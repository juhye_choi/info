package email.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;
import email.model.service.EmailService;

/**
 * Servlet implementation class DeleteEmailServlet
 */
@WebServlet("/deleteEmail.ad")
public class DeleteEmailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteEmailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int num = (int)request.getAttribute("num");
		
		int result = new EmailService().deleteEmail(num);
		
		String page="";
		
if(result > 0) {
			
				response.sendRedirect("/info/selectEmailList.ad");
			
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg","이메일 지우기 실패");
			
			request.getRequestDispatcher(page).forward(request, response);
			
		}
		
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
