package email.controller;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import member.model.service.MyAuthentication;

/**
 * Servlet implementation class EmailSendServlet
 */
@WebServlet("/EmailSend.ad")
public class EmailSendServlet extends HttpServlet {
   private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmailSendServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

   /**
    * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
    */
   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      String email = request.getParameter("email");
      String answer = request.getParameter("answer");
      int num = Integer.parseInt(request.getParameter("num"));
      

      
      
      Properties props = System.getProperties();
      props.put("mail.smtp.user", "itduck1004");
      props.put("mail.smtp.host","smtp.gmail.com");
      props.put("mail.smtp.port","465");
      props.put("mail.smtp.starttls.enable","true");
      props.put("mail.smtp.auth","true");
      props.put("mail.smtp.socketFactory.port","465");
      props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
      props.put("mail.smtp.socketFactory.fallback","false");
      
      Authenticator auth = new MyAuthentication();
      
      
      
      Session session = Session.getDefaultInstance(props,auth);
      MimeMessage msg = new MimeMessage(session);
      
      try {
         msg.setSentDate(new Date());
         
         
         
         InternetAddress from = new InternetAddress("itduck1004@gmail.com");
         
         msg.setFrom(from);
         
         InternetAddress to = new InternetAddress(email);
         msg.setRecipient(Message.RecipientType.TO, to);
         
         msg.setSubject("이메일 문의 답변","UTF-8");
         
         request.setAttribute("answer", answer);
         msg.setText(answer,"UTF-8");
         
         msg.setHeader("content-Type", "text/html");
         
         MimeMultipart multipart = new MimeMultipart("related");
        
         BodyPart messageBodyPart = new MimeBodyPart();
      
         
         
         
         String htmlText = "<img src=\"cid:my-image\" style='margin-left:auto; margin-right:auto;'><br><h1>이메일 문의 답변안내</h1><br><label>안녕하세요 잇덕입니다.<br> 항상 변함없는 애정과 신뢰로 잇덕을 이용해 주신 회원님께 깊은 감사를 드립니다.<br>보내주신 이메일 문의에 대한 답변을 드리겠습니다.<br></label> " + answer +"<br><br><br><label style='text-align:center; font-weight:600;'>본 메일은 '정보통신망 이용촉진 및 정보보호 등에 관한 법률' 제 30조 2에 의거하여 메일 수신동의와 상관없이 발송 되었습니다.\r\n" + 
         		"<br>궁금한 내용은 잇덕 고객지원센터에서 확인하실 수 있습니다.</label>";
            messageBodyPart.setContent(htmlText, "text/html;charset=" + "EUC-KR");
            multipart.addBodyPart(messageBodyPart);
    
          
            
            messageBodyPart = new MimeBodyPart();
            FileDataSource fds = new FileDataSource("C:/Users/황규환/git/info/web/images/logo.PNG");
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID", "<my-image>");
            multipart.addBodyPart(messageBodyPart);
            
      
            
            
            msg.setContent(multipart);
            
            
         
         javax.mail.Transport.send(msg);
         
         
      } catch (MessagingException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      
      request.setAttribute("num", num);
      RequestDispatcher rd = request.getRequestDispatcher("/deleteEmail.ad");
      rd.forward(request, response);
      
   }

   /**
    * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
    */
   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // TODO Auto-generated method stub
      doGet(request, response);
   }

}
