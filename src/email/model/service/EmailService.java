package email.model.service;

import email.model.dao.EmailDao;
import email.model.vo.Email;
import keyword.model.vo.PageInfo;
import member.model.dao.MemberDao;

import static common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import admin.model.dao.AdminDao;

public class EmailService {

	public int insertEmailQ(Email requestEmail) {
	
Connection con = getConnection();
		
		int result = new EmailDao().insertEmailQ(con,requestEmail);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		
		
		
		
		return result;
	}
	
	public int deleteEmail(int num) {
		Connection con = getConnection();

		int result = new EmailDao().deleteEmail(con,num);

		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}

		close(con);




		return result;
	}


	public int getEmailListCount() {
		Connection con = getConnection();
		int result = 0;

		result = new EmailDao().getEmailListCount(con);
		if(result != 0) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return result;
	}

	public ArrayList<Email> selectEmailList(PageInfo pi) {
Connection con = getConnection();
		
ArrayList<Email> list = new EmailDao().selectEamilList(con,pi);
		
		if(list != null) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return list;
	}

}
