package email.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import email.model.vo.Email;
import keyword.model.vo.PageInfo;
import member.model.dao.MemberDao;

import static common.JDBCTemplate.*;

public class EmailDao {
	
	private Properties prop = new Properties();
	
	public EmailDao() {
		
		String fileName = MemberDao.class.getResource("/sql/Email/email-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public int deleteEmail(Connection con, int num) {
		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("deleteEmail");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, num);

			result = pstmt.executeUpdate();


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {



			close(pstmt);
		}

		return result;
	}

	

	public int insertEmailQ(Connection con, Email requestEmail) {
PreparedStatement pstmt = null;
		
		int result = 0;
		
		String query = prop.getProperty("insertEmail");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, requestEmail.getUno());
			pstmt.setString(2, requestEmail.getQtitle());
			pstmt.setString(3, requestEmail.getQcontent());
			pstmt.setInt(4, requestEmail.getCategory());
			pstmt.setString(5, requestEmail.getQemail());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			
			close(pstmt);
			
		}
		
		
		
		
		
		
		
		
		return result;
	}


	public int getEmailListCount(Connection con) {

		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("getEmailListCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}

		return result;
	}


	public ArrayList<Email> selectEamilList(Connection con, PageInfo pi) {
		
		PreparedStatement pstmt = null;
		
		
		ResultSet rset = null;
		
		ArrayList<Email> list = null;
		Email e = null;
		
		
		String query = prop.getProperty("selectEamilListWithPaging");
		
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Email>();
			while(rset.next()) {
				
				e = new Email();
				
				e.setQid(rset.getInt("QID"));
				e.setUno(rset.getInt("UNO"));
				e.setQtitle(rset.getString("QTITLE"));
				e.setQcontent(rset.getString("QCONTENT"));
				e.setQdate(rset.getDate("QDATE"));
				e.setCategory(rset.getInt("CATEGORY"));
				e.setQemail(rset.getString("QEMAIL"));
				
				
				list.add(e);
				
			}
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}finally {
			
			close(pstmt);
			close(rset);
			
			
		}
		
		
		
		return list;
	}

}
