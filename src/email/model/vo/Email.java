package email.model.vo;

import java.io.Serializable;
import java.sql.Date;

public class Email implements Serializable{
	
	private int qid;
	private int uno;
	private String Qtitle;
	private String Qcontent;
	private Date Qdate;
	private int category;
	private String Qemail;
	private String qYn;
	
	public Email() {}
	public Email(int qid, int uno, String qtitle, String qcontent, Date qdate, int category, String qemail, String qYn) {
		super();
		this.qid = qid;
		this.uno = uno;
		Qtitle = qtitle;
		Qcontent = qcontent;
		Qdate = qdate;
		this.category = category;
		Qemail = qemail;
		this.qYn = qYn;
	}
	public int getQid() {
		return qid;
	}
	public void setQid(int qid) {
		this.qid = qid;
	}
	public int getUno() {
		return uno;
	}
	public void setUno(int uno) {
		this.uno = uno;
	}
	public String getQtitle() {
		return Qtitle;
	}
	public void setQtitle(String qtitle) {
		Qtitle = qtitle;
	}
	public String getQcontent() {
		return Qcontent;
	}
	public void setQcontent(String qcontent) {
		Qcontent = qcontent;
	}
	public Date getQdate() {
		return Qdate;
	}
	public void setQdate(Date qdate) {
		Qdate = qdate;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public String getQemail() {
		return Qemail;
	}
	public void setQemail(String qemail) {
		Qemail = qemail;
	}
	public void setqYn(String qYn) {
		this.qYn = qYn;
	}
	public String getqYn() {
		return qYn;
	}
	@Override
	public String toString() {
		return "Email [qid=" + qid + ", uno=" + uno + ", Qtitle=" + Qtitle + ", Qcontent=" + Qcontent + ", Qdate="
				+ Qdate + ", category=" + category + ", Qemail=" + Qemail + ", qYn=" + qYn + "]";
	}
	
	
	
	

}
