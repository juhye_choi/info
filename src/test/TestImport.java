package test;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/testImport")
public class TestImport extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public TestImport() {
        super();
    } 

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("result1");
		String buyerName = request.getParameter("result2");
		int price = Integer.parseInt(request.getParameter("result3"));
		String text = request.getParameter("result4");
		int recid = Integer.parseInt(request.getParameter("result5"));
		
		System.out.println("name : "+ name);
		System.out.println("buyerName : " + buyerName);
		System.out.println("price : " + price);
		System.out.println("text : " +text);
		System.out.println("recid : " + recid);
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
