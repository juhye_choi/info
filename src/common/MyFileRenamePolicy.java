package common;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.oreilly.servlet.multipart.FileRenamePolicy;

public class MyFileRenamePolicy implements FileRenamePolicy{

	@Override
	public File rename(File oldFile) {
		long currentTime = System.currentTimeMillis();
		int randomNumber = (int)(Math.random()*100000);
		SimpleDateFormat ft = new SimpleDateFormat("yyyyMMddHHmmss");
		
		//확장자명 가져오기
		String name = oldFile.getName();
		String body = "";
		String ext = "";
		int dot = name.lastIndexOf("."); //'.'의 위치를 찾지 못할경우 -1을 반환
		
		if(dot != -1) {
			body = name.substring(0, dot);
			ext = name.substring(dot);
		} else {
			body = name;
		}
		
		String fileName = ft.format(new Date(currentTime)) + randomNumber + ext;
		
		File newFile = new File(oldFile.getParent(), fileName); //getParent : 파일의 경로 가져오기
		
		return newFile;
	}

}
