package member.model.dao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import company.model.vo.Banner;
import company.model.vo.CPageInfo;
import company.model.vo.Company;
import company.model.vo.Payment;
import keyword.model.vo.PageInfo;
import member.model.vo.Member;
import person.model.vo.Attachment2;
import person.model.vo.Person;
import resume.model.vo.Attachment;

import static common.JDBCTemplate.*;

public class MemberDao {
	
	private Properties prop = new Properties();
	
	public MemberDao() {
		

		String fileName = MemberDao.class.getResource("/sql/member/member-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public int insertMember(Connection con, Member reuqestMember) {
	
		PreparedStatement pstmt = null;
		
		int result = 0;
		
		String query = prop.getProperty("insertMember");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, reuqestMember.getUserId());
			pstmt.setString(2, reuqestMember.getUserPwd());
			pstmt.setString(3, reuqestMember.getEmail());
			pstmt.setString(4, reuqestMember.getPhone());
			pstmt.setString(5, reuqestMember.getCategory());
			pstmt.setDate(6, reuqestMember.getRemoveDate());
			pstmt.setString(7, reuqestMember.getUserName());
			
			
			
			
			result = pstmt.executeUpdate();
			
			
			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			
			close(pstmt);
			
		}
		
		
		
		
		
		
		
		
		return result;
	}

	public int insertCMember(Connection con, Member reuqestMember) {


		PreparedStatement pstmt = null;
		
		int result = 0;
		
		String query = prop.getProperty("insertCMember");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, reuqestMember.getUserId());
			pstmt.setString(2, reuqestMember.getUserPwd());
			pstmt.setString(3, reuqestMember.getEmail());
			pstmt.setString(4, reuqestMember.getPhone());
			pstmt.setString(5, reuqestMember.getCategory());
			pstmt.setDate(6, reuqestMember.getRemoveDate());
			pstmt.setString(7, reuqestMember.getUserName());
			
		
	
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			
			close(pstmt);
			
		}
		
		
		
		
		
		
		
		
		return result;
	}

	public Member loginCheck(Connection con, Member requestMember) {

		PreparedStatement pstmt = null;
		
		ResultSet rset = null;
		
		Member loginUser = null;
		
		String query = prop.getProperty("loginSelect");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, requestMember.getUserId());
			pstmt.setString(2, requestMember.getUserPwd());
			
			rset = pstmt.executeQuery();
			
			
			if(rset.next()) {
				loginUser = new Member();
				
				loginUser.setUno(rset.getInt("UNO"));
				loginUser.setUserId(rset.getString("USER_ID"));
				loginUser.setUserPwd(rset.getString("USER_PWD"));
				loginUser.setUserName(rset.getString("NAME"));
				loginUser.setPhone(rset.getString("PHONE"));
				loginUser.setEmail(rset.getString("EMAIL"));
				loginUser.setRemoveStatus(rset.getString("STATUS"));
				loginUser.setCategory(rset.getString("CATEGORY"));
				
				
			}
			
			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			close(pstmt);
			close(rset);
			
			
		}
	
		
		
		return loginUser;
	}

	public Member CloginCheck(Connection con, Member requestMember) {
PreparedStatement pstmt = null;
		
		ResultSet rset = null;
		
		Member loginUser = null;
		
		String query = prop.getProperty("CloginSelect");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, requestMember.getUserId());
			pstmt.setString(2, requestMember.getUserPwd());
			
			rset = pstmt.executeQuery();
			
			
			if(rset.next()) {
				loginUser = new Member();
				
				loginUser.setUno(rset.getInt("UNO"));
				loginUser.setUserId(rset.getString("USER_ID"));
				loginUser.setUserPwd(rset.getString("USER_PWD"));
				loginUser.setUserName(rset.getString("NAME"));
				loginUser.setRemoveStatus(rset.getString("STATUS"));
				loginUser.setPhone(rset.getString("PHONE"));
				loginUser.setEmail(rset.getString("EMAIL"));
				loginUser.setCategory(rset.getString("CATEGORY"));
				loginUser.setApprovalYN(rset.getString("APPROVAL_YN"));
				
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return loginUser;
	}

	public int selectCurrval(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		

		int uno = 0;
		
		String query = prop.getProperty("selectCurrval");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				uno = rset.getInt("CURRVAL");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {

			close(stmt);
			close(rset);
		}
		
		return uno;
	}

	public int insertCompany(Connection con, Company requestCompany, int uno) {

		PreparedStatement pstmt = null;
		
		int result = 0;
		
		String query = prop.getProperty("insertCompany");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, uno);
			pstmt.setString(2, requestCompany.getRegistNum());
			pstmt.setString(3, requestCompany.getOwner());
			pstmt.setString(4, requestCompany.getHrManager());
			
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			
			close(pstmt);
			
			
		}
		
		
		return result;
	}

	public int idCheck(Connection con, String userId) {
int result = 0;
		
		PreparedStatement pstmt = null;
		
	ResultSet rset = null;
	
	String query = prop.getProperty("idCheck");
	
	try {
		pstmt= con.prepareStatement(query);
		
pstmt.setString(1, userId);
		
		rset = pstmt.executeQuery();
		
if(rset.next()) {
			
			result = rset.getInt(1);
			
		}
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally {
		close(pstmt);
		
		close(rset);
	}
	
	
	
		
		
		return result;
	}

	public int emailCheck(Connection con, String email) {
				int result = 0;
		
		PreparedStatement pstmt = null;
		
	ResultSet rset = null;
	
	String query = prop.getProperty("emailCheck");
	
	try {
		pstmt= con.prepareStatement(query);
		
		pstmt.setString(1, email);
		
		rset = pstmt.executeQuery();
		
		if(rset.next()) {
					
					result = rset.getInt(1);
					
				}
		
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally {
		close(pstmt);
		
		close(rset);
	}
	
	
	
		
		
		return result;										
	}

	

	public ArrayList<HashMap<String, Object>> selectPList(Connection con, PageInfo pi) {

	
		
		PreparedStatement pstmt = null;
		
		
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;	
		
		
		Member m = null;
		Person p = null;
		
		String query = prop.getProperty("selectPListWithPaging");
		
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				
				
				hmap.put("uno", rset.getInt("UNO"));
				hmap.put("name", rset.getString("NAME"));
				hmap.put("userId", rset.getString("USER_ID"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("nowPoint", rset.getInt("NOW_POINT"));
				
				
				
				list.add(hmap);
				
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			close(pstmt);
			
			close(rset);
		}
		

		
		
		return list;
	}

	public int PidSearch(Connection con, String personName, String personEmail) {
		
		
		
int result = 0;
		
		PreparedStatement pstmt = null;
		
	ResultSet rset = null;
	
	String query = prop.getProperty("PidSearch");
		
	try {
		pstmt= con.prepareStatement(query);
		
		pstmt.setString(1, personName);
		pstmt.setString(2, personEmail);
		
rset = pstmt.executeQuery();
		
		if(rset.next()) {
					
					result = rset.getInt(1);
					
				}
		
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally {
		close(pstmt);
		
		close(rset);
	}
	
	
	
		
		
		return result;
	}

	public Member idSearchFinish(Connection con, String personEmail) {
	
		PreparedStatement pstmt = null;
		
		ResultSet rset = null;
		
		Member m = null;
		
		String query = prop.getProperty("PidSearchFinish");
		
		
		try {
			pstmt= con.prepareStatement(query);
			pstmt.setString(1, personEmail);
			
			rset = pstmt.executeQuery();
			
			
			
			if(rset.next()) {
				m = new Member();
				
				m.setUno(rset.getInt("UNO"));
				m.setUserId(rset.getString("USER_ID"));
			
				m.setUserName(rset.getString("NAME"));
			
				m.setEmail(rset.getString("EMAIL"));
				m.setCategory(rset.getString("CATEGORY"));
				
				
			}
			
			
			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			close(rset);
			close(pstmt);
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		return m;
	}

	public int PpassSearch(Connection con, String personName, String personEmail, String personId) {
		
		
int result = 0;
		
		PreparedStatement pstmt = null;
		
	ResultSet rset = null;
	
	String query = prop.getProperty("PpassSearch");
		
	try {
		pstmt= con.prepareStatement(query);
		
		pstmt.setString(1, personName);
		pstmt.setString(2, personEmail);
		pstmt.setString(3, personId);
		
rset = pstmt.executeQuery();
		
		if(rset.next()) {
					
					result = rset.getInt(1);
					
				}
		
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally {
		close(pstmt);
		
		close(rset);
	}
	
	
	
		
		
		return result;
	}

	public int updatePass(Connection con, int uno, String password) {
		
PreparedStatement pstmt = null;
		
		int result = 0;
		
		String query = prop.getProperty("updatePass");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, password);
			pstmt.setInt(2, uno);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			close(pstmt);
			
			
			
		}
		
		
		
		
		return result;
	}

	public Member passSearchFinish(Connection con, String personId) {
PreparedStatement pstmt = null;
		
		ResultSet rset = null;
		
		Member m = null;
		
		String query = prop.getProperty("PpassSearchFinish");
		
		
		try {
			pstmt= con.prepareStatement(query);
			pstmt.setString(1, personId);
			
			rset = pstmt.executeQuery();
			
			
			
			if(rset.next()) {
				m = new Member();
				
				m.setUno(rset.getInt("UNO"));
				
				
			}
			
			
			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			close(rset);
			close(pstmt);
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		return m;
	}

	public Member CidSearchFinish(Connection con, String personEmail) {
PreparedStatement pstmt = null;
		
		ResultSet rset = null;
		
		Member m = null;
		
		String query = prop.getProperty("CidSearchFinish");
		
		
		try {
			pstmt= con.prepareStatement(query);
			pstmt.setString(1, personEmail);
			
			rset = pstmt.executeQuery();
			
			
			
			if(rset.next()) {
				m = new Member();
				
				m.setUno(rset.getInt("UNO"));
				m.setUserId(rset.getString("USER_ID"));
			
				m.setUserName(rset.getString("NAME"));
			
				m.setEmail(rset.getString("EMAIL"));
				m.setCategory(rset.getString("CATEGORY"));
				
				
			}
			
			
			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			close(rset);
			close(pstmt);
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		return m;
	}

	public int CidSearch(Connection con, String personName, String personEmail) {

		
		
int result = 0;
		
		PreparedStatement pstmt = null;
		
	ResultSet rset = null;
	
	String query = prop.getProperty("CidSearch");
		
	try {
		pstmt= con.prepareStatement(query);
		
		pstmt.setString(1, personName);
		pstmt.setString(2, personEmail);
		
rset = pstmt.executeQuery();
		
		if(rset.next()) {
					
					result = rset.getInt(1);
					
				}
		
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally {
		close(pstmt);
		
		close(rset);
	}
	
	
	
		
		
		return result;
	}

	public int CpassSearch(Connection con, String personName, String personEmail, String personId) {
		
		int result = 0;
				
				PreparedStatement pstmt = null;
				
			ResultSet rset = null;
			
			String query = prop.getProperty("CpassSearch");
				
			try {
				pstmt= con.prepareStatement(query);
				
				pstmt.setString(1, personName);
				pstmt.setString(2, personEmail);
				pstmt.setString(3, personId);
				
		rset = pstmt.executeQuery();
				
				if(rset.next()) {
							
							result = rset.getInt(1);
							
						}
				
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
				
				close(rset);
			}
			
			
			
				
				
				return result;
	}

	public Member CpassSearchFinish(Connection con, String personId) {
PreparedStatement pstmt = null;
		
		ResultSet rset = null;
		
		Member m = null;
		
		String query = prop.getProperty("CpassSearchFinish");
		
		
		try {
			pstmt= con.prepareStatement(query);
			pstmt.setString(1, personId);
			
			rset = pstmt.executeQuery();
			
			
			
			if(rset.next()) {
				m = new Member();
				
				m.setUno(rset.getInt("UNO"));
				
				
			}
			
			
			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			close(rset);
			close(pstmt);
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		return m;
	}

	public int CupdatePass(Connection con, String uno, String password) {
		
		PreparedStatement pstmt = null;
				
				int result = 0;
				
				String query = prop.getProperty("CupdatePass");
				
				try {
					pstmt = con.prepareStatement(query);
					pstmt.setString(1, password);
					pstmt.setString(2, uno);
					
					result = pstmt.executeUpdate();
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally {
					
					close(pstmt);
					
					
					
				}
				
				
				
				
				return result;
	}

	public int passCheck(Connection con, String userId, String password) {
int result = 0;
		
		PreparedStatement pstmt = null;
		
	ResultSet rset = null;
	
	String query = prop.getProperty("passwordCheck");
	
	try {
		pstmt= con.prepareStatement(query);
		
		pstmt.setString(1, userId);
		pstmt.setString(2, password);
		
		rset = pstmt.executeQuery();
		
		if(rset.next()) {
					
					result = rset.getInt(1);
					
				}
		
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally {
		close(pstmt);
		
		close(rset);
	}
	
	
	
		
		
		return result;
	}

	public int updateMember(Connection con, Member requestMember) {
PreparedStatement pstmt = null;
		
		int result = 0;
		
		String query = prop.getProperty("updateMember");
		
		try {
			pstmt = con.prepareStatement(query);
			
			
			pstmt.setString(1, requestMember.getPhone());
			
		
		
			pstmt.setInt(2, requestMember.getUno());
			
			
			result = pstmt.executeUpdate();
			
			
			
			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			
			close(pstmt);
			
		}
		
		
		
		
		return result;
	}

	public ArrayList<HashMap<String, Object>> selectCList(Connection con, CPageInfo pi) {

		
		PreparedStatement pstmt = null;
		
		
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;	
		
		
		Member m = null;
		Company c = null;
		
		String query = prop.getProperty("selectAppCListWithPaging");
		
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				
				
				hmap.put("uno", rset.getInt("UNO"));
				hmap.put("name", rset.getString("NAME"));
				hmap.put("userId", rset.getString("USER_ID"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("viewCount", rset.getString("VIEW_COUNT"));
			
				
				
				
				list.add(hmap);
				
			}
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			close(pstmt);
			
			close(rset);
		}
		

		
		
		return list;
	}

	public ArrayList<HashMap<String, Object>> selectLoginBannerImg(Connection con) {
		
		Statement stmt = null;
		ResultSet rset = null;
		
		ArrayList<HashMap<String, Object>> list = null;
		
		Attachment a = null;
		Banner b = null;
		Payment p = null;

		String query = prop.getProperty("selectLoginBannerImg");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			
			list = new ArrayList<HashMap<String, Object>>();
			while(rset.next()) {
				
			HashMap<String, Object> hmap = new 	HashMap<String, Object>();
				
				
			
			hmap.put("atId",rset.getInt("ATID") );
			hmap.put("changeName", rset.getString("CHANGE_NAME"));
			hmap.put("filePath",rset.getString("FILE_PATH") );
			hmap.put("payId", rset.getInt("PAYID"));
			hmap.put("bStart", rset.getDate("B_START"));
			hmap.put("bFinish", rset.getDate("B_FINISH"));
			hmap.put("pCode", rset.getInt("PCODE"));
			
			
			
			 list.add(hmap);
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		
		return list;
	}
	
	public int updateCompanyMember(Connection con, Member requestMember) {
PreparedStatement pstmt = null;
		
		int result = 0;
		
		String query = prop.getProperty("updateCompanyMember");
		
		try {
			pstmt = con.prepareStatement(query);
			
			
			pstmt.setString(1, requestMember.getPhone());
			
			pstmt.setInt(2, requestMember.getUno());
			
			
			result = pstmt.executeUpdate();
			
		
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
			
			close(pstmt);
			
		}
		
		
		
		
		return result;
	}

	public int companyNumCheck(Connection con, String companyNum) {
		int result = 0;
		
		PreparedStatement pstmt = null;
		
	ResultSet rset = null;
	
	String query = prop.getProperty("companyNumCheck");
	
	try {
		pstmt= con.prepareStatement(query);
		
		pstmt.setString(1, companyNum);
		
		rset = pstmt.executeQuery();
		
		if(rset.next()) {
					
					result = rset.getInt(1);
					
				}
		
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally {
		close(pstmt);
		
		close(rset);
	}
	
	
	
		
		
		return result;	
	}

	public String selectBanner(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		String fileName = null;
		
		String query = prop.getProperty("selectBanner");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				fileName = rset.getString("BANNER");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		return fileName;
	}

}
