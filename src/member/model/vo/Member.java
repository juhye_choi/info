package member.model.vo;

import java.io.Serializable;
import java.sql.Date;
public class Member implements Serializable{
	

	
	private int uno;
	
	private String userId;
	private String userPwd;
	private String userName;
	private String email;
	private String phone;
	private String category; // 개인, 기업, 관리자
	private String removeStatus; // 탈퇴여부 (기본값 : N)
	private Date removeDate; // 탈퇴날짜
	private String approvalYN;
	
	
	public Member() {}


	public Member(int uno, String userId, String userPwd, String userName, String email, String phone, String category,
			String removeStatus, Date removeDate, String approvalYN) {
		super();
		this.uno = uno;
		this.userId = userId;
		this.userPwd = userPwd;
		this.userName = userName;
		this.email = email;
		this.phone = phone;
		this.category = category;
		this.removeStatus = removeStatus;
		this.removeDate = removeDate;
		this.approvalYN = approvalYN;
	}


	public int getUno() {
		return uno;
	}


	public void setUno(int uno) {
		this.uno = uno;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getUserPwd() {
		return userPwd;
	}


	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getRemoveStatus() {
		return removeStatus;
	}


	public void setRemoveStatus(String removeStatus) {
		this.removeStatus = removeStatus;
	}


	public Date getRemoveDate() {
		return removeDate;
	}


	public void setRemoveDate(Date removeDate) {
		this.removeDate = removeDate;
	}
	
	public void setApprovalYN(String approvalYN) {
		this.approvalYN = approvalYN;
	}
	
	public String getApprovalYN() {
		return approvalYN;
	}


	@Override
	public String toString() {
		return "Member [uno=" + uno + ", userId=" + userId + ", userPwd=" + userPwd + ", userName=" + userName
				+ ", email=" + email + ", phone=" + phone + ", category=" + category + ", removeStatus=" + removeStatus
				+ ", removeDate=" + removeDate + ", approvalYN =" + approvalYN +"]";
	}
	
	
	
	
	
	
	
	
	
	

}
