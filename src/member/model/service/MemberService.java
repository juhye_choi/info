package member.model.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;

import company.model.vo.CPageInfo;
import company.model.vo.Company;
import keyword.model.vo.PageInfo;
import member.model.dao.MemberDao;
import member.model.vo.Member;
import person.model.dao.PersonDao;
import person.model.vo.Attachment2;
import person.model.vo.Person;

import static common.JDBCTemplate.*;

public class MemberService {

	public int insertMember(Member reuqestMember) {
		
		
		Connection con = getConnection();
		
		int result = new MemberDao().insertMember(con,reuqestMember);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		
		
		
		
		return result;
	}

	public int insertCMember(Member reuqestMember, Company requestCompany) {
		Connection con = getConnection();
		
		int result3 = 0;
		int uno=0;
		
		//기업회원 기본정보 insert
		int result = new MemberDao().insertCMember(con,reuqestMember);
		

		if(result > 0 ) {
			
			uno = new MemberDao().selectCurrval(con);
			
			
		}
		
		//기업회원 추가정보 insert
		int result2 = new MemberDao().insertCompany(con,requestCompany,uno);
		
		if (result > 0 && result2 > 0) {
			
			commit(con);
			result3 = 1;
			
		}else {

			rollback(con);
		}
		
		
		close(con);
		
		
		
		
		
		return result3;
	}

	public Member loginCheck(Member requestMember) {
		
		
		Connection con = getConnection();
		
		
		Member loginUser = new MemberDao().loginCheck(con,requestMember);
		
		
		
		close(con);
		
		
		
		
		
		
		return loginUser;
	}

	public Member CloginCheck(Member requestMember) {
		Connection con = getConnection();
		
		Member loginUser = new MemberDao().CloginCheck(con,requestMember);
close(con);
		
		
		
		
		
		
		return loginUser;
	}

	public int idCheck(String userId) {
Connection con = getConnection();
		
		int result = new MemberDao().idCheck(con, userId);
		
		close(con);
		
		
		
		
		
		
		return result;
	}

	public int emailCheck(String email) {
		Connection con = getConnection();
		
		int result = new MemberDao().emailCheck(con,email);
		
	close(con);
		
		
		
		
		
		
		return result;
	}

	public ArrayList<HashMap<String, Object>> selectPList(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new MemberDao().selectPList(con,pi);
		
		if(list != null) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return list;
	}

	public int PidSearch(String personName, String personEmail) {
		
		
		Connection con = getConnection();
		
		int result = new MemberDao().PidSearch(con,personName, personEmail);
		
		close(con);
			
			
			
			
			
			
			return result;
		
		
		
	}

	public Member idSearchFinish(String personEmail) {
	
		
		Connection con = getConnection();
		
		Member m = new MemberDao().idSearchFinish(con, personEmail);
		
		close(con);
		
		
		
		return m;
	}

	public int PpassSearch(String personName, String personEmail, String personId) {
Connection con = getConnection();
		
		int result = new MemberDao().PpassSearch(con,personName, personEmail, personId);
		
		close(con);
			
			
			
			
			
			
			return result;
	}

	public int updatePass(int uno, String password) {

		Connection con = getConnection();
		
		int result = new MemberDao().updatePass(con, uno, password);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		
		
		
		
		
		
		
		
		return result;
	}

	public Member passSearchFinish(String personId) {
Connection con = getConnection();
		
		Member m = new MemberDao().passSearchFinish(con, personId);
		
		close(con);
		
		
		
		return m;
	}

	public Member CidSearchFinish(String personEmail) {
Connection con = getConnection();
		
		Member m = new MemberDao().CidSearchFinish(con, personEmail);
		
		close(con);
		
		
		
		return m;
	}

	public int CidSearch(String personName, String personEmail) {
	Connection con = getConnection();
		
		int result = new MemberDao().CidSearch(con,personName, personEmail);
		
		close(con);
			
			
			
			
			
			
			return result;
		
	}

	public int CpassSearch(String personName, String personEmail, String personId) {
Connection con = getConnection();
		
		int result = new MemberDao().CpassSearch(con,personName, personEmail, personId);
		
		close(con);
			
			
			
			
			
			
			return result;
	}

	public Member CpassSearchFinish(String personId) {
Connection con = getConnection();
		
		Member m = new MemberDao().CpassSearchFinish(con, personId);
		
		close(con);
		
		
		
		return m;
	}

	public int CupdatePass(String uno, String password) {
Connection con = getConnection();
		
		int result = new MemberDao().CupdatePass(con, uno, password);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		
		
		
		
		
		
		
		
		return result;
	}

	public int passCheck(String userId, String password) {
Connection con = getConnection();
		
		int result = new MemberDao().passCheck(con,userId, password);
		
	close(con);
	
	return result;
		
	}

	public int updateMember(Member requestMember, Person requestPerson,ArrayList<Attachment2> fileList) {
		
		
		Connection con = getConnection();
		
		int result = 0;
		
		int result1 = new MemberDao().updateMember(con,requestMember);
		int currval = 	requestMember.getUno();
		if(result1 > 0) {
			
			int result2 = new PersonDao().updatePerson(con,requestPerson);
			 
			
			
			
			if(result2 > 0) {
				
				fileList.get(0).setProUno(currval);
				int result3 = new PersonDao().updateImg(con,fileList);
				
				if(result3 > 0) {
					
					
					result = 1;
					commit(con);
					
					
				}else {
					rollback(con);
				}
				
				
			}else {
				rollback(con);
				
			}
			
			
			
		} else {
			rollback(con);
		}
		
		
		
		
		
		
		
		
		close(con);
		
		return result;
		
		
		
		
		
	
	}

	public ArrayList<HashMap<String, Object>> selectCList(CPageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new MemberDao().selectCList(con,pi);
		
		if(list != null) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return list;
	}

	public ArrayList<HashMap<String, Object>> selectLoginBannerImg() {
		
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = null;
		
		list = new MemberDao().selectLoginBannerImg(con);
		close(con);
		
		
		return list;
	}

	public int companyNumCheck(String companyNum) {
		Connection con = getConnection();
		
		int result = new MemberDao().companyNumCheck(con,companyNum);
		
		close(con);
		
		
		
		
		
		
		return result;
	}

	public String selectBanner() {
		Connection con = getConnection();
		
		String fileName = new MemberDao().selectBanner(con);
		
		close(con);
		
		return fileName;
	}



}
