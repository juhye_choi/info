package member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.service.MemberService;
import member.model.vo.Member;

/**
 * Servlet implementation class MemberInsertServlet
 */
@WebServlet("/pInsertMember.me")
public class PMemberInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PMemberInsertServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String userId = request.getParameter("userId");
		String userPwd = request.getParameter("userPwd");
		String userName = request.getParameter("userName");
		String tel1 = request.getParameter("tel1");
		String tel2 = request.getParameter("tel2");
		String tel3 = request.getParameter("tel3");
		String phone = tel1 + "-"+tel2 + "-"+tel3;
		
		String email = request.getParameter("email");
		
		
		
		
		Member reuqestMember = new Member();
		
		reuqestMember.setUserId(userId);
		reuqestMember.setUserPwd(userPwd);
		reuqestMember.setUserName(userName);
		reuqestMember.setPhone(phone);
		reuqestMember.setEmail(email);
		reuqestMember.setCategory("P");
		reuqestMember.setRemoveDate(null);
	
		
		System.out.println("reuqestMember : " + reuqestMember);
		
		int result = new MemberService().insertMember(reuqestMember);
		
		
		String page="";
		
		if(result > 0) {
			
			page="loginBannerImg.me?type=1";
			
			response.sendRedirect(page);
			
			
			
		}else {
			
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg","개인회원 회원가입 실패!");
			
			request.getRequestDispatcher(page).forward(request, response);
			
			
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
