package member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.vo.Company;
import member.model.service.MemberService;
import member.model.vo.Member;

/**
 * Servlet implementation class CMemberInsertServlet
 */
@WebServlet("/cInsertMember.me")
public class CMemberInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CMemberInsertServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String userId = request.getParameter("userId");
		String password = request.getParameter("userPwd");
		String companyName = request.getParameter("companyName");
		String tel1 = request.getParameter("tel1");
		String tel2 = request.getParameter("tel2");
		String tel3 = request.getParameter("tel3");
		String phone = tel1 + "-"+tel2 + "-"+tel3;
		String email = request.getParameter("email");
		
	
		
		
		// 추가정보!!!!!!!!
		String companyNumber = request.getParameter("companyNumber");
		String ceoName = request.getParameter("ceoName");
		String managerName = request.getParameter("managerName");
		
		
		
		Company requestCompany = new Company();
		requestCompany.setRegistNum(companyNumber);
		requestCompany.setOwner(ceoName);
		requestCompany.setHrManager(managerName);
		
		
		
		
		
		
		
		
		
Member reuqestMember = new Member();
		
		reuqestMember.setUserId(userId);
		reuqestMember.setUserPwd(password);
		reuqestMember.setUserName(companyName);
		reuqestMember.setPhone(phone);
		reuqestMember.setEmail(email);
		reuqestMember.setCategory("C");
		reuqestMember.setRemoveDate(null);
		
		System.out.println("reuqestMember : " + reuqestMember);
		
		int result = new MemberService().insertCMember(reuqestMember,requestCompany);
		
		
		
String page="";
		
		if(result > 0) {
			
			page="loginBannerImg.me?type=2";
			
			response.sendRedirect(page);
			
			
		}else {
			
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg","기업회원 회원가입 실패!");
			
			request.getRequestDispatcher(page).forward(request, response);
			
			
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
