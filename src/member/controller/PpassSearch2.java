package member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.service.MemberService;
import member.model.vo.Member;

/**
 * Servlet implementation class PpassSearch2
 */
@WebServlet("/passSearch2.me")
public class PpassSearch2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PpassSearch2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String password = request.getParameter("userPwd");
	
		int uno = Integer.parseInt(request.getParameter("uno"));
		
		int type = Integer.parseInt(request.getParameter("type"));
		

		
		
		int result = new MemberService().updatePass(uno,password);
		
		
		
		String page="";
		
		if(result > 0) {
			
			
			
			//비밀번호 수정시
			if(type== 1) {
				
				response.sendRedirect("loginBannerImg.me?type=1");	
			}else if(type == 2) {
				Member loginUser = (Member)request.getSession().getAttribute("loginUser");
				loginUser.setUserPwd(password);
				
				request.getSession().setAttribute("loginUser", loginUser);
				response.sendRedirect("selectOne.po");
				
			}
			
			
		}else {
			
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg","개인회원 비밀번호 찾기 실패");
			
			request.getRequestDispatcher(page).forward(request, response);
			
		}
		
		
		
		
		
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
