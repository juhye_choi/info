package member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.service.MemberService;
import member.model.vo.Member;

/**
 * Servlet implementation class CLoginServlet
 */
@WebServlet("/cLogin.me")
public class CLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String userId = request.getParameter("userId");
		String password = request.getParameter("userPwd");
		

	
		
		Member requestMember = new Member();
		
		requestMember.setUserId(userId);
		requestMember.setUserPwd(password);
		
		Member loginUser = null;
		
		if(userId.equals("admin")) {
			 loginUser = new MemberService().CloginCheck(requestMember);
		}else {
			 loginUser = new MemberService().CloginCheck(requestMember);
		}
		
		
		
		
		String page="";
		
		
		if(loginUser != null) {
			
			if(userId.equals("admin")) {
request.getSession().setAttribute("loginUser", loginUser);
				
				response.sendRedirect("selectCount.me");
				
			}else if(loginUser.getApprovalYN().equals("N")) {
				request.setAttribute("msg", "아직 승인되지 않은 기업회원입니다.");
				
				request.getRequestDispatcher("views/common/CompanyneedLogin.jsp").forward(request, response);
				
			}else if(loginUser.getRemoveStatus().equals("Y")) {
				request.setAttribute("msg", "탈퇴처리된 회원입니다.");
				
				request.getRequestDispatcher("views/common/needLogin.jsp").forward(request, response);
				
			}else {
				request.getSession().setAttribute("loginUser", loginUser);
				response.sendRedirect("views/main/companyMain.jsp");
			}
			
			
			
		}else {
			
			
			
			request.setAttribute("msg", "아이디 및 비밀번호를 확인해주세요.");
			
			request.getRequestDispatcher("views/common/CompanyneedLogin.jsp").forward(request, response);
			
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
