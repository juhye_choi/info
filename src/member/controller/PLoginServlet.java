package member.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.oreilly.servlet.MultipartRequest;

import common.MyFileRenamePolicy;
import member.model.service.MemberService;
import member.model.vo.Member;
import person.model.service.PersonService;
import person.model.vo.Attachment2;

/**
 * Servlet implementation class PLoginServlet
 */
@WebServlet("/pLogin.me")
public class PLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		

			
		String userId = request.getParameter("userId");
		String password = request.getParameter("userPwd");
		
		System.out.println("로그인 아이디 : " + userId);	
		System.out.println("로그인 비밀번호 : " +password);	
		Member requestMember = new Member();
		
		requestMember.setUserId(userId);
		requestMember.setUserPwd(password);
		
		ArrayList<Attachment2> fileList = null;
		Member loginUser = null;
		
		if(userId.equals("admin")) {
			
			 loginUser = new MemberService().loginCheck(requestMember);
			
		}else {
			
		 loginUser = new MemberService().loginCheck(requestMember);
		 
		 if(loginUser != null) {
			 fileList = new PersonService().selectImg(loginUser.getUno()); 
		 }else {
			 fileList = null;
		 }
			
			
			
		}
		
	
	
		String page="";
	
		
		if(loginUser != null) {
			
			if(userId.equals("admin")) {
				
				request.getSession().setAttribute("loginUser", loginUser);
				
				response.sendRedirect("selectCount.me");
				
			}else if(loginUser.getRemoveStatus().equals("Y")) {
				request.setAttribute("msg", "탈퇴처리된 회원입니다.");
				
				request.getRequestDispatcher("views/common/needLogin.jsp").forward(request, response);
				
			}else {
				
				request.getSession().setAttribute("loginUser", loginUser);
				request.getSession().setAttribute("fileList", fileList);
				response.sendRedirect("index.jsp");
				
			}
			
			
			
				
				
			
			
		
			
		}else {
			
			
			request.setAttribute("msg", "아이디 및 비밀번호를 확인해주세요.");
		
			request.getRequestDispatcher("views/common/needLogin.jsp").forward(request, response);
			
		}
		
		
		}
		
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
