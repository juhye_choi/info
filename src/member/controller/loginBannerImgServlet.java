package member.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.service.MemberService;
import person.model.vo.Attachment2;

/**
 * Servlet implementation class loginBannerImgServlet
 */
@WebServlet("/loginBannerImg.me")
public class loginBannerImgServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public loginBannerImgServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int type= 1;
		if(request.getParameter("type") != null) {
				type=Integer.parseInt(request.getParameter("type"));
		}
		
		
	
		
		
		ArrayList<HashMap<String, Object>> list = new MemberService().selectLoginBannerImg();
		
		
		
		String page="";
		
		if(list != null) {
			
			if(type == 1) {
				page = "views/common/login.jsp";
				request.setAttribute("list", list);
				request.getRequestDispatcher(page).forward(request, response);
				
			}else if(type == 2) {
				page = "views/common/companyLogin.jsp";
				request.setAttribute("list", list);
				request.getRequestDispatcher(page).forward(request, response);
				
			}
			
			
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "아이디 화면 광고배너 추가 실패!");
			request.getRequestDispatcher(page).forward(request, response);
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
