package member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import email.model.service.EmailService;
import email.model.vo.Email;

/**
 * Servlet implementation class EmailQServlet
 */
@WebServlet("/emailQ.me")
public class EmailQServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmailQServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String email = request.getParameter("email");
		int uno = Integer.parseInt(request.getParameter("uno"));
		int category = Integer.parseInt(request.getParameter("cate1"));
		String title = request.getParameter("title");
		String content = request.getParameter("content");
	
		
		
		
		Email requestEmail = new Email();
		requestEmail.setQemail(email);
		requestEmail.setUno(uno);
		requestEmail.setCategory(category);
		requestEmail.setQtitle(title);
		requestEmail.setQcontent(content);
		
		int result = new EmailService().insertEmailQ(requestEmail);		
		
String page="";
		
		if(result > 0) {
			
			if(category == 1) {
				page="views/person/customerService.jsp";
				response.sendRedirect(page);
				
			}else if(category == 2){
				page="views/company/companyCustomerService.jsp";
				response.sendRedirect(page);
			}
			
		
			
			
			
		}else {
			
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg","이메일 문의 실패!");
			
			request.getRequestDispatcher(page).forward(request, response);
			
			
			
			
		}
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
