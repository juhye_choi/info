package company.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyPaymentService;
import company.model.vo.UsingAdverList;

@WebServlet("/testDatepicker")
public class TestDatePickerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public TestDatePickerServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int pcode = 7;
		ArrayList<UsingAdverList> list = new CompanyPaymentService().selectAdverList(pcode);
		
		String page = "";
		
		System.out.println(list);
		if(list != null) {
			page = "views/company/premium3.jsp";
			request.setAttribute("list", list);
			request.getRequestDispatcher(page).forward(request, response);
		}else {
			System.out.println("없음");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
