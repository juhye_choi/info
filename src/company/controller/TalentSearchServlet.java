package company.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyService;
import member.model.service.MemberService;
import member.model.vo.Member;
import person.model.vo.Person;
import person.model.vo.PersonalBlack;

/**
 * Servlet implementation class TalentSearchServlet
 */
@WebServlet("/search.cr")
public class TalentSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TalentSearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int uno = 0;
		String banner = null;
		
		if(request.getSession().getAttribute("loginUser") != null) {
			uno = ((Member) request.getSession().getAttribute("loginUser")).getUno();
		}
		
		banner = new MemberService().selectBanner();
		ArrayList<Person> list = new CompanyService().searchTalent(uno);
		
		String page ="";
		
		if(list != null) {
			ArrayList<Person> rlist = new CompanyService().searchRefTalent(uno);
			
			if(rlist != null) {
				page = "views/company/talentSearch.jsp";
				request.setAttribute("list", list);
				request.setAttribute("rlist", rlist);
				request.setAttribute("banner", banner);
			}else {
				page = "views/company/talentSearch.jsp";
				request.setAttribute("list", list);
				request.setAttribute("rlist", rlist);
				request.setAttribute("banner", banner);
			}
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "인재 검색 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
