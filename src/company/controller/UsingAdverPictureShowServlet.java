package company.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import company.model.service.CompanyPaymentService;

@WebServlet("/pictureShow")
public class UsingAdverPictureShowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UsingAdverPictureShowServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int payid = Integer.parseInt(request.getParameter("payid"));
		
		String path = new CompanyPaymentService().UsingAdverPicturePath(payid);
		System.out.println("path : " + path);
		
		response.getWriter().print(path);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
