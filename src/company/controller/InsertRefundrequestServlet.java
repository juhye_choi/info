package company.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyPaymentService;
import company.model.service.CompanyService;

@WebServlet("/insertRefundrequest")
public class InsertRefundrequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InsertRefundrequestServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int payid = Integer.parseInt(request.getParameter("payid"));
		
		System.out.println(payid);
		int result = new CompanyPaymentService().insertRefundrequest(payid);
		//location.href = "<%=request.getContextPath()%>/insertRefundrequest?payid="+payid;
		
		String page = "";
		if(result > 0) {
			page = "views/common/successPage.jsp?successCode=6";
			request.getRequestDispatcher(page).forward(request, response);
		}else {
			
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
