package company.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyService;
import company.model.vo.Company;
import company.model.vo.Recruit;
import member.model.vo.Member;

/**
 * Servlet implementation class CompanyIntro
 */
@WebServlet("/intro.atc")
public class CompanyIntroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CompanyIntroServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String page = "";
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		int uno = 0;
		if(loginUser != null) {
			uno = loginUser.getUno();
			Company company = new Company();
			
			company = new CompanyService().selectMember(uno);
			ArrayList<Recruit> recList = new CompanyService().selectIngRec(uno);
	
			if(company != null) {
				page="views/company/companyPage.jsp";
				request.setAttribute("company", company);
				request.setAttribute("recList", recList);
			} else {
				page = "views/common/companyErrorPage.jsp";
				request.setAttribute("msg", "오류!!");
			}
		} else {
			page = "views/common/companyErrorPage.jsp";
			request.setAttribute("msg", "로그인이 필요합니다!");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
