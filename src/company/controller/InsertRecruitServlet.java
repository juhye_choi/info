package company.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.oreilly.servlet.MultipartRequest;

import common.MyFileRenamePolicy;
import company.model.service.RecruitService;
import company.model.vo.Recruit;
import company.model.vo.RecruitAttachment;
import keyword.model.vo.Keyword;

@WebServlet("/insert.Rc")
public class InsertRecruitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InsertRecruitServlet() {
        super();
    }
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(ServletFileUpload.isMultipartContent(request)) {
			int maxSize = 1024 * 1024 * 100; // 100Mbyte로 용량제한
			
			String root = request.getSession().getServletContext().getRealPath("/");
			
			String savePath = root + "attachment_uploadFiles/";
			
			MultipartRequest multiRequest = new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			//변환된 파일의 이름을 저장할 arrayList
			ArrayList<String> saveFiles = new ArrayList<>();
			
			//원본파일의 이름 저장할 arrayList
			ArrayList<String> originFiles = new ArrayList<>();
			
			//파일이 전송된 input태그의 name을 반환
			Enumeration<String> files = multiRequest.getFileNames();
			
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
			}
			
			int uno = Integer.parseInt(multiRequest.getParameter("uno"));					//회원번호
			String rec_title = multiRequest.getParameter("post-title");						//제목
			int rec_type = Integer.parseInt(multiRequest.getParameter("employFrom"));		//고용형태
			String career_period = multiRequest.getParameter("career");						//경력
			int education = Integer.parseInt(multiRequest.getParameter("education"));						//학력
			int rec_pcount = Integer.parseInt(multiRequest.getParameter("personnel"));						//모집인원
			int salary_way = Integer.parseInt(multiRequest.getParameter("salary"));						//급여형태
			int min_salary = Integer.parseInt(multiRequest.getParameter("minSalary"));		//최소급여
			int max_salary = Integer.parseInt(multiRequest.getParameter("maxSalary"));		//최대급여
			String work_area = multiRequest.getParameter("workingArea");					//근무지역
			String job_position = multiRequest.getParameter("jobPosition");					//주요업무
			String eligibility = multiRequest.getParameter("eligibility");					//자격요건
			String spe_condition = multiRequest.getParameter("preferential");				//우대사항
			String benefits_welfare = multiRequest.getParameter("benWel");					//혜택 및 복지
			Date rec_start = Date.valueOf((String) multiRequest.getParameter("startDate"));						//시작일자
			Date rec_end = Date.valueOf((String) multiRequest.getParameter("endDate"));							//종료일자
			int howto_apply = Integer.parseInt(multiRequest.getParameter("reception"));					//접수방법
			String[] addResumeBtn = multiRequest.getParameterValues("addResumeBtn");
			String[] keywords = multiRequest.getParameterValues("keywords");
			int att = 0;
			
			if(addResumeBtn != null) {
				att = 1;
			}
			
			/*System.out.println("uno: " + uno);
			System.out.println("rec_title : " + rec_title);
			System.out.println("rec_type: " + rec_type);
			System.out.println("career_period: " + career_period);
			System.out.println("education: " + education);
			System.out.println("rec_pcount: " + rec_pcount);
			System.out.println("salary_way: " + salary_way);
			System.out.println("min_salary: " + min_salary);
			System.out.println("max_salary: " + max_salary);
			System.out.println("work_area: " + work_area);
			System.out.println("job_position: " + job_position);
			System.out.println("eligibility: " + eligibility);
			System.out.println("spe_condition: " + spe_condition);
			System.out.println("benefits_welfare: " + benefits_welfare);
			System.out.println("rec_start: " + rec_start);
			System.out.println("rec_end: " + rec_end);
			System.out.println("howto_apply: " + howto_apply);
			System.out.println(keywords.length);*/

			Recruit rec = new Recruit();
			rec.setUno(uno);
			rec.setRec_title(rec_title);
			rec.setRectype(rec_type);
			rec.setCareer_period(career_period);
			rec.setEducation(education);
			rec.setRec_pcount(rec_pcount);
			rec.setSalary_way(salary_way);
			rec.setMin_salary(min_salary);
			rec.setMax_salary(max_salary);
			rec.setWork_area(work_area);
			rec.setJob_position(job_position);
			rec.setEligibility(eligibility);
			rec.setSpe_condition(spe_condition);
			rec.setBenefits_welfare(benefits_welfare);
			rec.setRec_start(rec_start);
			rec.setRec_finish(rec_end);
			rec.setHowto_apply(howto_apply);
			
			if(keywords.length > 0) {
				ArrayList<Keyword> klist = new ArrayList<>();
				
				for(int i=0; i<keywords.length; i++) {
					Keyword k = new Keyword();
					k.setKcode(Integer.parseInt(keywords[i]));
					
					klist.add(k);
				}
				rec.setKlist(klist);
			}
			

			if(att > 0) {          //체크박스가 체크된 상태라면 파일을 첨부
				RecruitAttachment ra = new RecruitAttachment();
				ra.setFilePath(savePath);
				ra.setOriginName(originFiles.get(0));
				ra.setChangeName(saveFiles.get(0));
				rec.setRecruitAttachment(ra);
			}
			
			int result = new RecruitService().insertRecruit(rec, att);
			
			String page = "";
			
			//DB INSERT 성공
			if(result > 0) {
				page = "/selectList.rec";
			//DB INSERT 실패
			}else {
				//INSERT 실패시 파일 삭제
				for(int i=0; i<saveFiles.size(); i++) {
					File failedFile = new File(savePath +saveFiles.get(i));
					
					failedFile.delete();
				}
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "등록 실패!");
			}
			
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
