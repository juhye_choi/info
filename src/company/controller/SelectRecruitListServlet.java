package company.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyService;
import company.model.vo.Recruit;
import company.model.vo.RecruitCount;
import member.model.vo.Member;


@WebServlet("/selectList.rec")
public class SelectRecruitListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectRecruitListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String page = "";
		
		if(request.getSession().getAttribute("loginUser") != null) {
			Member loginUser = (Member) request.getSession().getAttribute("loginUser");
			int uno = loginUser.getUno();
			RecruitCount rc = new RecruitCount();
			int type= 0;
			if(request.getParameter("type") != null) {
				type = Integer.parseInt(request.getParameter("type"));
			}
			rc.setAllRecCount(new CompanyService().countUserAllRec(uno));
			rc.setIngRecCount(new CompanyService().countUserIngRec(uno));
			rc.setEndRecCount(new CompanyService().countUserEndRec(uno));
			
			ArrayList<Recruit> recAllList = new CompanyService().selectAllRec(uno);
			ArrayList<Recruit> recIngList = new CompanyService().selectIngRec(uno);
			ArrayList<Recruit> recEndList = new CompanyService().selectEndRec(uno);

			if(type==0) {
					page="views/company/companyJobOpening.jsp";
					request.setAttribute("Count", rc);
					request.setAttribute("recAllList", recAllList);
					request.setAttribute("recIngList", recIngList);
					request.setAttribute("recEndList", recEndList);
			} else {
					page = "views/company/applicantManagement.jsp";
					request.setAttribute("reclist", recAllList);
			}
				
			request.getRequestDispatcher(page).forward(request, response);
			
		} else {
			page="/loginBannerImg.me?type=2";
			request.setAttribute("msg", "로그인이 필요합니다.");
			request.getRequestDispatcher(page).forward(request, response);
		}

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
