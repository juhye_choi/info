package company.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.oreilly.servlet.MultipartRequest;

import common.MyFileRenamePolicy;
import company.model.service.CompanyPaymentService;
import company.model.vo.AdverAttachment;
import company.model.vo.CompanyPayment;
import member.model.vo.Member;
import resume.model.vo.Attachment;

@WebServlet("/paymentAdverServlet")
public class PaymentAdverServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PaymentAdverServlet() {
        super();
    } 
    //광고 배너 결제 servlet
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(ServletFileUpload.isMultipartContent(request)) {
			int maxSize = 1024 * 1024 * 10; // 10Mbyte로 용량제한
			
			String root = request.getSession().getServletContext().getRealPath("/");
			
			String savePath = root + "attachment_uploadFiles/";
			
			MultipartRequest multiRequest = new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			//변환된 파일의 이름을 저장할 arrayList
			ArrayList<String> saveFiles = new ArrayList<>();
			
			//원본파일의 이름 저장할 arrayList
			ArrayList<String> originFiles = new ArrayList<>();
			
			//파일이 전송된 input태그의 name을 반환
			Enumeration<String> files = multiRequest.getFileNames();
			
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
			}
			
			String AdverName = multiRequest.getParameter("AdverName");
			//System.out.println("AdverName : " + AdverName);
			int pcode = Integer.parseInt(multiRequest.getParameter("pcode"));
			String pstartDate = multiRequest.getParameter("startDate");
			Date startDate = null;
			if(pstartDate != null) {
				startDate = java.sql.Date.valueOf(pstartDate);
			}
			String pendDate = multiRequest.getParameter("endDate");
			Date endDate = null;
			if(pendDate != null) {
				endDate = java.sql.Date.valueOf(pendDate);
			}
			int category1 = Integer.parseInt(multiRequest.getParameter("category1"));
			//가격
			int paidAmount = Integer.parseInt(multiRequest.getParameter("paidAmount"));
			
			/*System.out.println("saveFiles : " + saveFiles);
			System.out.println("originFiles : " + originFiles);
			System.out.println("AdverName : " + AdverName);
			System.out.println("pcode : " + pcode);
			System.out.println("startDate :" + startDate);
			System.out.println("endDate : " + endDate);
			System.out.println("category : " + category1);
			System.out.println("paidAmount : " + paidAmount);
			*/
			//Attachment 객체 생성 : 첨부한 사진을 담는 용도
			//광고배너는 한개의 파일만 저장 arrayList를 사용할 필요없다.
			AdverAttachment at = new AdverAttachment();
			//경로
			at.setFilePath(savePath);
			//원본 이름
			at.setOriginName(originFiles.get(0));
			//바뀐이름
			at.setChangeName(saveFiles.get(0));
			//대표사진 = ? -> 0
			at.setFileLevel(0);
			
			//기업 결제내역 객체 생성
			CompanyPayment cp = new CompanyPayment();
			cp.setUno(((Member)request.getSession().getAttribute("loginUser")).getUno());
			cp.setStartDate(startDate);
			cp.setEndDate(endDate);
			cp.setCategory1(category1);
			cp.setProductCode(pcode);
			cp.setPaidAmount(paidAmount);
			cp.setAdverAttachment(at);
			
			//광고 결제 완료 후 DB INSERT  (결제내역 , 결제 상세내역 , 배너등록관리 , 첨부파일)
			int result = new CompanyPaymentService().PaymentAdverServlet(cp);
			//DB INSERT 성공
			PrintWriter out = response.getWriter();
			if(result > 0) {
				
				out.append("success");
				out.flush();
				out.close();
				
			//DB INSERT 실패
			}else {
				//INSERT 실패시 파일 삭제
				for(int i=0; i<saveFiles.size(); i++) {
					File failedFile = new File(savePath +saveFiles.get(i));
					
					failedFile.delete();
				}
				out.append("fail");
				
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
