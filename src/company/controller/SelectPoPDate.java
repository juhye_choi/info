package company.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.google.gson.internal.JsonReaderInternalAccess;

import company.model.service.CompanyPaymentService;
import company.model.vo.PayMentDates;
import member.model.vo.Member;

@WebServlet("/selectPoPDate")
public class SelectPoPDate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectPoPDate() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		int uno = loginUser.getUno();
		int payid = Integer.parseInt(request.getParameter("payid"));
		int pcode = Integer.parseInt(request.getParameter("pcode"));
		
		
		PayMentDates pd = new CompanyPaymentService().selectPoPDate(uno ,payid);
		
		
		long diff = pd.getStartDate().getTime() - pd.getSysDate().getTime();
		long diffDays = diff / (24*60*60*1000);
		

		//JSONObject payDetail = null;
		PrintWriter out = response.getWriter();
		if(diffDays >= 7 ) {
			//환불 가능 
			out.append("true");
		}else {
			//환불 불가능
			out.append("false");
		} 
		out.flush();
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
