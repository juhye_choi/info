package company.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import company.model.service.CompanyPaymentService;
import company.model.vo.UsingAdverList;

@WebServlet("/ChangeBanner")
public class ChangeBannerProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ChangeBannerProduct() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int pcode = Integer.parseInt(request.getParameter("pcode"));
		
		
		ArrayList<UsingAdverList> list = new CompanyPaymentService().selectAdverList(pcode);
		
		JSONArray result = new JSONArray();
		JSONObject change = null;
		
		for(UsingAdverList ulist : list) {
			change = new JSONObject();
			
			change.put("startDate" , ulist.getAdstartDate().toString());
			change.put("endDate", ulist.getAdendDate().toString());
			
			result.add(change);
		}
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.print(result.toJSONString());
		out.flush();
		out.close();
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
