package company.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyPaymentService;
import company.model.vo.UsingPersonList;
import member.model.vo.Member;

@WebServlet("/usingPerson")
public class UsingPersonProuctListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UsingPersonProuctListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//페이징 처리 전
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		
		
		if(loginUser == null) {
			request.getRequestDispatcher("loginBannerImg.me?type="+2).forward(request, response);
		}else {
			int uno = loginUser.getUno();
			ArrayList<UsingPersonList> list = new CompanyPaymentService().usingPersonList(uno);
			
			//System.out.println(list);
			
			
			String page = "";
			if(list !=null) {
				page = "views/company/premium5.jsp";
				request.setAttribute("list", list);
				request.getRequestDispatcher(page).forward(request, response);
			}else {
				page = "view/common/errorPage.jsp";
				request.setAttribute("msg", "PersonUsinglist불러오기 실패");
				request.getRequestDispatcher(page).forward(request, response);
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
