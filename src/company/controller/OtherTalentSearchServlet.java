package company.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyService;
import member.model.vo.Member;
import person.model.service.PersonService;
import person.model.vo.Attachment2;
import resume.model.service.ResumeService;
import resume.model.vo.Resume;

/**
 * Servlet implementation class OtherTalentSearchServlet
 */
@WebServlet("/talentSearch.cr")
public class OtherTalentSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OtherTalentSearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int uno = 0;
		int otherUno = 0;
		String type = null;
		String ChangeName = null;
		int view = 0;
		
		if(request.getSession().getAttribute("loginUser") != null){
			uno = ((Member) request.getSession().getAttribute("loginUser")).getUno();
			otherUno = Integer.parseInt(request.getParameter("otherUno"));
			type = ((Member) request.getSession().getAttribute("loginUser")).getCategory();
			
			if(type.equals("C")) {
				view = new CompanyService().checkPayHistory(uno, otherUno);
			}
		} else {
			request.setAttribute("msg", "로그인이 필요한 메뉴입니다.");
			request.getRequestDispatcher("views/common/CompanyneedLogin.jsp").forward(request, response);
		}
		
		ArrayList<Attachment2> atlist = new PersonService().selectImg(otherUno);
		ChangeName = atlist.get(0).getChangeName();
		
		String page = "";
		
		//이력서와 해당 사람의 정보를 가져오기!
		Resume resume = new ResumeService().selectResume(otherUno);
		HashMap<String, String> personInfo = new PersonService().selectPerson(otherUno);
		
		if(type != null) {
	 		if(personInfo != null || resume != null) {
				HashMap<String, Object> hmap = new HashMap<>();
				hmap.put("uno", uno);
				hmap.put("otherUno", otherUno);
				hmap.put("name", personInfo.get("name"));
				hmap.put("email", personInfo.get("email"));
				hmap.put("phone", personInfo.get("phone"));
				hmap.put("jobStatus", personInfo.get("jobStatus"));
				hmap.put("resume", resume);
				hmap.put("view", view);
				hmap.put("ChangeName", ChangeName);
				
				String infoOpen = new PersonService().selectInfoOpen(otherUno);
				
				 if(infoOpen != null) {
					 page = "views/company/otherTalentSearch.jsp";
					 request.setAttribute("hmap", hmap);
					 request.setAttribute("infoOpen", infoOpen);
					 request.setAttribute("type", type);
					 request.getRequestDispatcher(page).forward(request, response);
				 }else {
					 page = "views/common/errorPage.jsp";
					 request.setAttribute("msg", "정보 공개여부 조회 실패!");
					 request.getRequestDispatcher(page).forward(request, response);
				 }
			} else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "이력서 조회에 실패했습니다.");
				request.getRequestDispatcher(page).forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
