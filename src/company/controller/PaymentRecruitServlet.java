package company.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyPaymentService;
import company.model.vo.CompanyPayment;
import member.model.vo.Member;

@WebServlet("/paymentRecruit")
public class PaymentRecruitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PaymentRecruitServlet() {
        super();
    } 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		
		int uno = loginUser.getUno();
		//상품이름
		String orderName = request.getParameter("orderName");
		//상품 주문자 이름
		String buyerName = request.getParameter("buyerName");
		// 가격
		int paidAmount = Integer.parseInt(request.getParameter("paidAmount"));
		//선택한 공고
		String selectRecruit = request.getParameter("selectRecruit");
		//선택한 공고 id
		int recid = Integer.parseInt(request.getParameter("recid"));
		// 적극채용공고 시작 일자
		Date startDate = Date.valueOf(request.getParameter("startDate"));
		// 적극채용공고 종료 일자
		Date endDate = Date.valueOf(request.getParameter("endDate"));
		// 상품 코드 ( 적극채용공고 ) 
		int productCode = Integer.parseInt(request.getParameter("productCode"));
		// 결제 상세이력 구분 ( 요청 )
		int category1 = Integer.parseInt(request.getParameter("category1"));
		// 결제 상세이력 구분 ( 승인 ) 
		int category2 = Integer.parseInt(request.getParameter("category2"));
		
		System.out.println("recid : " + recid );
		System.out.println("startDate : " + startDate);
		System.out.println("endDate : " + endDate);
		System.out.println("productCode : " + productCode);
		System.out.println("category1 : " + category1+" ," + category2);
		
		CompanyPayment p = new CompanyPayment();
		
		p.setUno(uno);
		p.setPaidAmount(paidAmount);
		p.setRecid(recid);
		p.setStartDate(startDate);
		p.setEndDate(endDate);
		p.setProductCode(productCode);
		p.setCategory1(category1);
		p.setCategory2(category2);
		
		int result = new CompanyPaymentService().insertPayment(p);
		
		PrintWriter out = response.getWriter();
		
		if(result > 0 ) {
			out.append("success");
		}else {
			out.append("fail");
		}
		
		out.flush();
		out.close();
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
