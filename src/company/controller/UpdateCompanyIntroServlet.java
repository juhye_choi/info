package company.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.oreilly.servlet.MultipartRequest;

import common.MyFileRenamePolicy;
import company.model.service.CompanyService;
import company.model.vo.CompanyInfo;
import resume.model.vo.Attachment;

/**
 * Servlet implementation class UpdateCompanyIntroServlet
 */
@WebServlet("/updateIntro")
public class UpdateCompanyIntroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCompanyIntroServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(ServletFileUpload.isMultipartContent(request)) {
			int maxSize = 1024 * 1024 * 10; // 10Mbyte로 용량제한
			
			String root = request.getSession().getServletContext().getRealPath("/");
			
			String savePath = root + "attachment_uploadFiles/";
			
			MultipartRequest multiRequest = new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			//변환된 파일의 이름을 저장할 arrayList
			ArrayList<String> saveFiles = new ArrayList<>();
			
			//원본파일의 이름 저장할 arrayList
			ArrayList<String> originFiles = new ArrayList<>();
			
			//파일이 전송된 input태그의 name을 반환
			Enumeration<String> files = multiRequest.getFileNames();
			
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
			}
			
			/*System.out.println("파일개수 : " + originFiles.size());
			System.out.println("회원번호 : " + multiRequest.getParameter("uno"));
			System.out.println("설립일 : " + multiRequest.getParameter("birth"));
			System.out.println("주소 : " + multiRequest.getParameter("address"));
			System.out.println("주소 : " + multiRequest.getParameter("introText"));*/
			
			int uno = Integer.parseInt(multiRequest.getParameter("uno"));
			Date birth = Date.valueOf((String) multiRequest.getParameter("birth"));
			String address = multiRequest.getParameter("address");
			String introText = multiRequest.getParameter("introText");
			
			ArrayList<Attachment> atlist = new ArrayList<>();
			
			for(int i=0; i < originFiles.size(); i++) {
				if(originFiles.get(i) != null) {
					Attachment at = new Attachment();
					at.setFilePath(savePath);
					at.setOriginName(originFiles.get(i));
					at.setChangeName(saveFiles.get(i));
					atlist.add(at);
				};
			}
			CompanyInfo ci = new CompanyInfo();
			
			ci.setUno(uno);
			ci.setBirth(birth);
			ci.setAddress(address);
			ci.setIntro(introText);
			ci.setAtlist(atlist);
			
			int result = new CompanyService().updateCompanyInfo(uno, ci);
			
			String page = "";
			
			if(result > 0) {
				page = "/intro.atc";
			}else {
				for(int i=0; i<saveFiles.size(); i++) {
					File failedFile = new File(savePath +saveFiles.get(i));
					
					failedFile.delete();
				}
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "정보 수정 실패!");
			}
			
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
