package company.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.oreilly.servlet.MultipartRequest;

import common.MyFileRenamePolicy;
import company.model.service.CompanyService;
import company.model.vo.Company;
import company.model.vo.RecruitAttachment;

/**
 * Servlet implementation class UpdateCompanyServlet
 */
@WebServlet("/updateCompany.me")
public class UpdateCompanyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCompanyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(ServletFileUpload.isMultipartContent(request)) {
			int maxSize = 1024 * 1024 * 10; // 10Mbyte로 용량제한
			
			String root = request.getSession().getServletContext().getRealPath("/");
			
			String savePath = root + "attachment_uploadFiles/";
			
			MultipartRequest multiRequest = new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			//변환된 파일의 이름을 저장할 arrayList
			ArrayList<String> saveFiles = new ArrayList<>();
			
			//원본파일의 이름 저장할 arrayList
			ArrayList<String> originFiles = new ArrayList<>();
			
			//파일이 전송된 input태그의 name을 반환
			Enumeration<String> files = multiRequest.getFileNames();
			
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
			}
			Company c = new Company();
			c.setHrManager(multiRequest.getParameter("hrName"));
			c.setPhone(multiRequest.getParameter("phone"));
			c.setUno(Integer.parseInt(multiRequest.getParameter("uno1")));
			
			RecruitAttachment ra = new RecruitAttachment();
			ra.setFilePath(savePath);
			ra.setOriginName(originFiles.get(0));
			ra.setChangeName(saveFiles.get(0));
			
			int result = new CompanyService().updateProfile(ra, c);
			
			String page = "";
			
			//DB INSERT 성공
			if(result > 0) {
				page = "/views/company/companyProfile.jsp";
			//DB INSERT 실패
			}else {
				//INSERT 실패시 파일 삭제
				for(int i=0; i<saveFiles.size(); i++) {
					File failedFile = new File(savePath +saveFiles.get(i));
					
					failedFile.delete();
				}
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "기업회원정보 수정 실패!");
			}
			
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
