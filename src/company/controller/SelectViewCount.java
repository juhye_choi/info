package company.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyPaymentService;
import member.model.vo.Member;


@WebServlet("/selectViewCount")
public class SelectViewCount extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectViewCount() {
        super();
    } 

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		System.out.println(loginUser);
		int result = 9999;
		String page = "";
		if(loginUser != null) {
			int uno = loginUser.getUno();
			
			result = new CompanyPaymentService().selectViewCount(uno);
			
			
			page = "views/company/premium2.jsp";
			request.setAttribute("result", result);
			request.getRequestDispatcher(page).forward(request, response);
			
		}else if(loginUser==null){
			page ="views/company/premium2.jsp";
			request.setAttribute("result", result);
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
