package company.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyPaymentService;
import member.model.vo.Member;

@WebServlet("/SelectCount")
public class SelectCountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectCountServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		int uno = loginUser.getUno();
		
		int payid = Integer.parseInt(request.getParameter("payid"));
		int pcode = Integer.parseInt(request.getParameter("pcode"));
		
		int count = new CompanyPaymentService().selectCount(uno , payid);
		
		//System.out.println("pcode : " + pcode);
		//System.out.println("count : " + count);
		
		PrintWriter out = response.getWriter();
		
		//1000건
		if(pcode == 2) {
			if(count > 1000/2) {
				out.append("true");
			}else {
				out.append("false");
			}
		//500건
		}else if(pcode ==3) {
			if(count > 500/2) {
				out.append("true");
			}else {
				out.append("false");
			}
		//100건
		}else if(pcode == 4) {
			if(count > 100/2) {
				out.append("true");
			}else {
				out.append("false");
			}
		//50건
		}else if(pcode ==5){
			if(count > 50/2) {
				out.append("true");
			}else {
				out.append("false");
			}
		//10건
		}else if(pcode == 6) {
			if(count > 10/2) { 
				out.append("true");
			}else {
				out.append("false");
			}
		}
		out.flush();
		out.close();
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
