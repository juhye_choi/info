package company.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyService;
import keyword.model.vo.Keyword;
import member.model.vo.Member;
import person.model.service.PersonService;
import person.model.vo.Person;

/**
 * Servlet implementation class TalentKeyWordkSearchServlet
 */
@WebServlet("/talentKeySearch.crs")
public class TalentKeyWordkSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TalentKeyWordkSearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int uno = 0;
		String keyword = (String) request.getParameter("key");
		String searchKeyword = (String) request.getParameter("keyword");
		ArrayList<Person> list = null;
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		
		if(loginUser != null) {
			uno = loginUser.getUno();
		}
		
		if(uno > 0) {
			if(keyword != null) {
				list = new CompanyService().searchKeyTalent(keyword, uno);
			}else {
				list = new CompanyService().searchContainKeyTalent(searchKeyword, uno);
			}
		}else {
			if(keyword != null) {
				list = new CompanyService().nLoginSearchKeyTalent(keyword);
			}else {
				list = new PersonService().searchContainKeyResume(searchKeyword);
			}
		}
		
		
		String page = "";
		
		if(list != null) {
			
			ArrayList<Person> rlist = null;
			ArrayList<Keyword> klist = new CompanyService().selectKeyword(searchKeyword);
			
			if(uno > 0) {
				if(keyword != null) {
					rlist = new CompanyService().searchKeyRefTalent(keyword, uno);
				}else if(searchKeyword != null){
					rlist = new CompanyService().searchContainKeyRefTalent(searchKeyword, uno);
				}
			}else {
				if(keyword != null) {
					rlist = new CompanyService().nLoginSearchKeyRefTalent(keyword);
				}else if(searchKeyword != null) {
					rlist = new CompanyService().nLoginSearchContainKeyRefTalent(searchKeyword);
				}
				
			}
			
			if(rlist != null) {
				page = "views/company/talentKeywordSearch.jsp";
				request.setAttribute("list", list);
				request.setAttribute("rlist", rlist);
				request.setAttribute("keyword", keyword);
				request.setAttribute("klist", klist);
			}else {
				page = "views/company/talentKeywordSearch.jsp";
				request.setAttribute("list", list);
				request.setAttribute("rlist", rlist);
				request.setAttribute("keyword", keyword);
				request.setAttribute("klist", klist);
			}
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "인재 키워드 검색 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
