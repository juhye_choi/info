package company.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.JsonReaderInternalAccess;

import company.model.service.CompanyPaymentService;
import company.model.vo.UsingAdverList;

@WebServlet("/usingAdver")
public class UsingAdverProductListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UsingAdverProductListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int uno = Integer.parseInt(request.getParameter("uno"));
		
		//System.out.println(uno);
		//페이징 처리 전
		ArrayList<UsingAdverList> list = new CompanyPaymentService().usingAdverList(uno);
		
		//System.out.println(list);
		
		JSONArray result = new JSONArray();
		JSONObject usingAdver = null;
		
		for(UsingAdverList ulist : list) {
			usingAdver = new JSONObject();
			
			usingAdver.put("AdverstartDate", ulist.getAdstartDate().toString());
			usingAdver.put("AdendDate", ulist.getAdendDate().toString());
			usingAdver.put("AdFileName", URLEncoder.encode(ulist.getAdFileName() ,"UTF-8"));
			usingAdver.put("AdPayId", ulist.getAdPayId());
			if(ulist.getAdCategory().equals("1")) {
				ulist.setAdCategory("대기중");
			}else if(ulist.getAdCategory().equals("2")) {
				ulist.setAdCategory("이용중");
			}else {
				ulist.setAdCategory("종료");
			}
			
			usingAdver.put("AdCategory", URLEncoder.encode(ulist.getAdCategory(),"UTF-8"));
			result.add(usingAdver);
		}
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.print(result.toJSONString());
		out.flush();
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
