package company.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyService;
import member.model.vo.Member;
import resume.model.service.ResumeService;
import resume.model.vo.ProjectCareer;

/**
 * Servlet implementation class PayPersonRecruitServlet
 */
@WebServlet("/payCompany.pcr")
public class PayPersonRecruitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PayPersonRecruitServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int cUno = 0;
		int otherUno = 0;
		int pjtId = 0;
		String title = null;
		String changeName = null;
		
		if(request.getSession().getAttribute("loginUser") != null) {
			cUno = ((Member) request.getSession().getAttribute("loginUser")).getUno();
			otherUno = Integer.parseInt(request.getParameter("OtherUno"));
			pjtId = Integer.parseInt(request.getParameter("pjtId"));
			title = (String) request.getParameter("title");
			changeName = (String) request.getParameter("changeName");
		}else {
			request.setAttribute("msg", "잘못된 경로의 접근 방식입니다.");
			request.getRequestDispatcher("views/common/errorPage.jsp?errorCode=3").forward(request, response);
		}
		
		int count = new CompanyService().viewCount(cUno);
		
		String page = "";
		
		if(count > 0) {
			int result = new CompanyService().insertSeenParson(cUno, otherUno, pjtId, title);
			
			if(result > 0) {
				ProjectCareer pc = new ResumeService().selectProjectCareer(pjtId);
				
				long diff = pc.getPjtFinish().getTime() - pc.getPjtStart().getTime();
			    int diffDays = (int) (diff/(24*60*60*1000));
			    
			    page = "views/company/otherTalentRecruit.jsp";
				request.setAttribute("pc", pc);
				request.setAttribute("diffDays", diffDays);
				request.setAttribute("ChangeName", changeName);
			}else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "인재 열람 이력 실패");
			}
		}else {
			page = "/selectViewCount";
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
