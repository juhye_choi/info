package company.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.JsonReaderInternalAccess;

import company.model.service.CompanyPaymentService;
import company.model.vo.PaymentList;

@WebServlet("/paymentDetailList")
public class PaymentDetailList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PaymentDetailList() {
        super();
    }
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int uno = Integer.parseInt(request.getParameter("uno"));
		int payid = Integer.parseInt(request.getParameter("payid"));
		
		ArrayList<PaymentList> list = new CompanyPaymentService().selectDetailPaymentList(uno , payid);
		
		
		
		JSONArray result = new JSONArray();
		JSONObject payDetail = null;
		
			for(PaymentList plist : list) {
				payDetail = new JSONObject();
				
				payDetail.put("payDate", plist.getPaymentDate().toString());
				payDetail.put("pName", URLEncoder.encode(plist.getProductName(), "UTF-8"));
				payDetail.put("pMoney", plist.getPaymentPrice());
				
				if(plist.getPayhistroyCategory().equals("1")) {
					plist.setPayhistroyCategory("요청");
				}else if (plist.getPayhistroyCategory().equals("2")) {
					plist.setPayhistroyCategory("승인");
				}else if(plist.getPayhistroyCategory().equals("3")){
					plist.setPayhistroyCategory("환불");
				}else if(plist.getPayhistroyCategory().equals("4")) {
					plist.setPayhistroyCategory("환불요청");
				}else {
					plist.setPayhistroyCategory("환불완료");
				}
				
				//System.out.println(plist.getPayhistroyCategory());
				
				payDetail.put("cat", URLEncoder.encode(plist.getPayhistroyCategory(), "UTF-8"));
				
				
				payDetail.put("payid", plist.getPayid());
				payDetail.put("uno", uno);
				payDetail.put("pcode", plist.getPcode());
				
				
				result.add(payDetail);
			}
			
			response.setContentType("application/json");
			PrintWriter out = response.getWriter();
			out.print(result.toJSONString());
			out.flush();
			out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
