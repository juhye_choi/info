package company.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.vo.NowPopList;
import company.model.service.CompanyPaymentService;
import company.model.service.RecruitService;
import company.model.vo.Recruit;
import member.model.vo.Member;

@WebServlet("/recruitList")
public class RecruitListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RecruitListServlet() {
        super();
    } 

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//공고 List불러오기 (적극채용 공고 List)
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		
		int uno = 0;
		ArrayList<Recruit> list = null;
		
		
		ArrayList<NowPopList> nowPoplist = null; 
		if(loginUser != null) {
			uno = loginUser.getUno();
			list = new RecruitService().selectRecruitList(uno);
			
			//list가 null이 아니면 
			if(list != null) {
				//
				nowPoplist = new RecruitService().nowPoplist();
			}
		}
		System.out.println(nowPoplist);
		String page ="";
		
		if(list != null) {
			page = "views/company/premium1.jsp";
			request.setAttribute("list", list);
			request.setAttribute("nowPoplist", nowPoplist);
			request.getRequestDispatcher(page).forward(request, response);
		}else {
			page = "views/company/premium1.jsp";
			request.setAttribute("msg", "현재공고가 존재하지 않습니다.");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
