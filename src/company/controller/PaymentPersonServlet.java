package company.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyPaymentService;
import company.model.vo.CompanyPayment;
import member.model.vo.Member;

@WebServlet("/paymentPerson")
public class PaymentPersonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PaymentPersonServlet() {
        super();
    }
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		
		int uno = loginUser.getUno();
		//String orderName = request.getParameter("orderName");
		//String buyerName = request.getParameter("buyerName");
		int paidAmount = Integer.parseInt(request.getParameter("paidAmount"));
		int productCode = Integer.parseInt(request.getParameter("productCode"));
		int category1 = Integer.parseInt(request.getParameter("category1"));
		int category2 = Integer.parseInt(request.getParameter("category2"));
		//구매시 인재열람 갯수
		int num = Integer.parseInt(request.getParameter("num"));
		/*
		System.out.println("uno : " + uno);
		System.out.println("paidAmount :" + paidAmount);
		System.out.println("category1 : " + category1);
		System.out.println("category2 : " + category2);
		System.out.println("num : " + num);
		*/
		CompanyPayment cp = new CompanyPayment();
		
		cp.setUno(uno);
		cp.setProductCode(productCode);
		cp.setCategory1(category1);
		cp.setCategory2(category2);
		cp.setPaidAmount(paidAmount);
		cp.setCount(num);
		
		//인재열람결제시 insert
		int result = new CompanyPaymentService().insertPersonPayment(cp);
		
		PrintWriter out = response.getWriter();
		
		if(result > 0 ) {
			out.append("success");
		}else {
			out.append("fail");
		}
		
		out.flush();
		out.close();
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
