package company.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import company.model.service.CompanyPaymentService;
import company.model.vo.UsingPopRecruit;

@WebServlet("/usingPop")
public class UsingPopRecruitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UsingPopRecruitServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int uno = Integer.parseInt(request.getParameter("uno"));
		
		
		ArrayList<UsingPopRecruit> list = new CompanyPaymentService().usingPopRecruitList(uno);
		//System.out.println(list);
		JSONArray result = new JSONArray();
		JSONObject usingPopRecruit = null;
		
		for(UsingPopRecruit plist : list) {
			usingPopRecruit = new JSONObject();
			
			usingPopRecruit.put("PopRecruitstartDate", plist.getPopstartDate().toString());
			usingPopRecruit.put("PopRecruitendDate", plist.getPopendDate().toString());
			usingPopRecruit.put("PopRecruitName", URLEncoder.encode(plist.getPopTitle(),"UTF-8" ));
			if(plist.getCategory().equals("1")) {
				plist.setCategory("대기중");
			}else if(plist.getCategory().equals("2")) {
				plist.setCategory("이용중");
			}else {
				plist.setCategory("종료");
			}
			usingPopRecruit.put("PopRecruitCategory", URLEncoder.encode(plist.getCategory(),"UTF-8"));
			
			result.add(usingPopRecruit);
		}
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.print(result.toJSONString());
		out.flush();
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
