package company.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UsingPersonList {

	private Date openDate;		//이력서 열람한 날짜
	private String projectTitle;//열람한 이력서 제목
	private int count;			//열람한 기업의 인재열람권 갯수
	private int puno; 			//열람된 회원 ID
}
