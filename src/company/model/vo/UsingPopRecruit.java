package company.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UsingPopRecruit {
	
	private Date PopstartDate;	//결제완료한 적극채용공고 시작 날짜
	private Date PopendDate;	//결제완료한 적극채용공고 종료 날짜
	private String PopTitle;	//결제완료한 저극채용공고 제목
	private String category;	//결제완료한 적극채용공고의 상태
	private int payid;			//결제완료한 적극채용공고의 결제 ID
}
