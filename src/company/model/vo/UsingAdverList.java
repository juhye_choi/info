package company.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UsingAdverList {

	private Date AdstartDate;	//결제완료한 광고 시작 날짜
	private Date AdendDate;		//결제완료한 광고 종료 날짜
	private String AdFileName;	//결제완료한 광고 등록파일 이름
	private String AdCategory;	//결제완료한 광고상품의 현재 상태
	private int AdPayId;		//결제완료된 광고상품의 결제아이디
}
