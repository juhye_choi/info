package company.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import resume.model.vo.Attachment;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class CompanyPayment {
	private int uno;
	private int paidAmount;
	private int recid; 
	private Date startDate;
	private Date endDate;
	private int productCode;
	private int category1;
	private int category2;
	private int Count;
	private AdverAttachment adverAttachment;
}
