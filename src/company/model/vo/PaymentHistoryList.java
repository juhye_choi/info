package company.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PaymentHistoryList {

	private Date paymentDate;
	private String productName;
	private int paymentPrice;
	private String category;
	private Date startDate;
	private Date endDate;
	private String refund_why;
}
