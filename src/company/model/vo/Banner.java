package company.model.vo;

import java.io.Serializable;
import java.util.Date;

public class Banner implements Serializable{
	
	private int payId;
	private Date bStart;
	private Date bFinish;
	
	public Banner() {}

	public Banner(int payId, Date bStart, Date bFinish) {
		super();
		this.payId = payId;
		this.bStart = bStart;
		this.bFinish = bFinish;
	}

	public int getPayId() {
		return payId;
	}

	public void setPayId(int payId) {
		this.payId = payId;
	}

	public Date getbStart() {
		return bStart;
	}

	public void setbStart(Date bStart) {
		this.bStart = bStart;
	}

	public Date getbFinish() {
		return bFinish;
	}

	public void setbFinish(Date bFinish) {
		this.bFinish = bFinish;
	}

	@Override
	public String toString() {
		return "Banner [payId=" + payId + ", bStart=" + bStart + ", bFinish=" + bFinish + "]";
	}
	
	

}
