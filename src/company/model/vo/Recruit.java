package company.model.vo;

import java.sql.Date;
import java.util.ArrayList;

import keyword.model.vo.Keyword;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


public class Recruit {
	private int recid;					//채용공고 ID
	private int uno; 			 		//회원 번호 
	private String rec_title; 	  		//공고제목
	private int rectype; 		  		//고용형태 ( 1 : 정규직 , 2:계약직 , 3:인턴 , 4:기타)
	private String career_period; 		//경력 (NEW:신입 , OLD:경력  , N:무관)
	private int education; 		  		//학력   (1:고졸 , 2:초대졸 ,3:대졸 ,4:대학원졸 )
	private int rec_pcount;  	  		//모집인원 
	private int salary_way; 	  		//급여형태 (1:연봉 , 2:월급 ,3:시급 ,4:회사내규)
	private int min_salary;	  	  		//최소급여
	private int max_salary;				//최대급여
	private String work_area;			//근무지역
	private String job_position; 		//주요업무
	private String eligibility;	  		//자격조건
	private String spe_condition; 		//우대사항
	private String benefits_welfare;	//혜택 및 복지
	private Date rec_start;		  		//접수기간 시작
	private Date rec_finish;	  		//접수기간 종료
	private int howto_apply; 	  		//접수 방법 (1:잇덕접수 ,2:자사홈페이지 ,3:이메일접수) 접수방법은은 중복체크가 가능하게 되어있어서 ArrayList로 받아야될 수도 있음
	private Date rec_date;		  		//공고작성일
	private RecruitAttachment recruitAttachment;		//첨부파일
	private ArrayList<Keyword> klist;	//키워드
	private int applicant;					//지원자수
	
	public Recruit() {}

	public Recruit(int recid, int uno, String rec_title, int rectype, String career_period, int education,
			int rec_pcount, int salary_way, int min_salary, int max_salary, String work_area, String job_position,
			String eligibility, String spe_condition, String benefits_welfare, Date rec_start, Date rec_finish,
			int howto_apply, Date rec_date, RecruitAttachment recruitAttachment, ArrayList<Keyword> klist,
			int applicant) {
		super();
		this.recid = recid;
		this.uno = uno;
		this.rec_title = rec_title;
		this.rectype = rectype;
		this.career_period = career_period;
		this.education = education;
		this.rec_pcount = rec_pcount;
		this.salary_way = salary_way;
		this.min_salary = min_salary;
		this.max_salary = max_salary;
		this.work_area = work_area;
		this.job_position = job_position;
		this.eligibility = eligibility;
		this.spe_condition = spe_condition;
		this.benefits_welfare = benefits_welfare;
		this.rec_start = rec_start;
		this.rec_finish = rec_finish;
		this.howto_apply = howto_apply;
		this.rec_date = rec_date;
		this.recruitAttachment = recruitAttachment;
		this.klist = klist;
		this.applicant = applicant;
	}

	public int getRecid() {
		return recid;
	}

	public void setRecid(int recid) {
		this.recid = recid;
	}

	public int getUno() {
		return uno;
	}

	public void setUno(int uno) {
		this.uno = uno;
	}

	public String getRec_title() {
		return rec_title;
	}

	public void setRec_title(String rec_title) {
		this.rec_title = rec_title;
	}

	public int getRectype() {
		return rectype;
	}

	public void setRectype(int rectype) {
		this.rectype = rectype;
	}

	public String getCareer_period() {
		return career_period;
	}

	public void setCareer_period(String career_period) {
		this.career_period = career_period;
	}

	public int getEducation() {
		return education;
	}

	public void setEducation(int education) {
		this.education = education;
	}

	public int getRec_pcount() {
		return rec_pcount;
	}

	public void setRec_pcount(int rec_pcount) {
		this.rec_pcount = rec_pcount;
	}

	public int getSalary_way() {
		return salary_way;
	}

	public void setSalary_way(int salary_way) {
		this.salary_way = salary_way;
	}

	public int getMin_salary() {
		return min_salary;
	}

	public void setMin_salary(int min_salary) {
		this.min_salary = min_salary;
	}

	public int getMax_salary() {
		return max_salary;
	}

	public void setMax_salary(int max_salary) {
		this.max_salary = max_salary;
	}

	public String getWork_area() {
		return work_area;
	}

	public void setWork_area(String work_area) {
		this.work_area = work_area;
	}

	public String getJob_position() {
		return job_position;
	}

	public void setJob_position(String job_position) {
		this.job_position = job_position;
	}

	public String getEligibility() {
		return eligibility;
	}

	public void setEligibility(String eligibility) {
		this.eligibility = eligibility;
	}

	public String getSpe_condition() {
		return spe_condition;
	}

	public void setSpe_condition(String spe_condition) {
		this.spe_condition = spe_condition;
	}

	public String getBenefits_welfare() {
		return benefits_welfare;
	}

	public void setBenefits_welfare(String benefits_welfare) {
		this.benefits_welfare = benefits_welfare;
	}

	public Date getRec_start() {
		return rec_start;
	}

	public void setRec_start(Date rec_start) {
		this.rec_start = rec_start;
	}

	public Date getRec_finish() {
		return rec_finish;
	}

	public void setRec_finish(Date rec_finish) {
		this.rec_finish = rec_finish;
	}

	public int getHowto_apply() {
		return howto_apply;
	}

	public void setHowto_apply(int howto_apply) {
		this.howto_apply = howto_apply;
	}

	public Date getRec_date() {
		return rec_date;
	}

	public void setRec_date(Date rec_date) {
		this.rec_date = rec_date;
	}

	public RecruitAttachment getRecruitAttachment() {
		return recruitAttachment;
	}

	public void setRecruitAttachment(RecruitAttachment recruitAttachment) {
		this.recruitAttachment = recruitAttachment;
	}

	public ArrayList<Keyword> getKlist() {
		return klist;
	}

	public void setKlist(ArrayList<Keyword> klist) {
		this.klist = klist;
	}

	public int getApplicant() {
		return applicant;
	}

	public void setApplicant(int applicant) {
		this.applicant = applicant;
	}

	@Override
	public String toString() {
		return "Recruit [recid=" + recid + ", uno=" + uno + ", rec_title=" + rec_title + ", rectype=" + rectype
				+ ", career_period=" + career_period + ", education=" + education + ", rec_pcount=" + rec_pcount
				+ ", salary_way=" + salary_way + ", min_salary=" + min_salary + ", max_salary=" + max_salary
				+ ", work_area=" + work_area + ", job_position=" + job_position + ", eligibility=" + eligibility
				+ ", spe_condition=" + spe_condition + ", benefits_welfare=" + benefits_welfare + ", rec_start="
				+ rec_start + ", rec_finish=" + rec_finish + ", howto_apply=" + howto_apply + ", rec_date=" + rec_date
				+ ", recruitAttachment=" + recruitAttachment + ", klist=" + klist + ", applicant=" + applicant + "]";
	}

}
