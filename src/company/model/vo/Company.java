package company.model.vo;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;

import resume.model.vo.Attachment;

public class Company implements Serializable{
	
	private int uno;				//회원번호
	private String userId;			//회원 아이디
	private String companyName;		//이름
	private String phone;			//전화번호
	private String email;			//이메일
	private String registNum;		//사업자등록번호
	private String owner;			//대표
	private String hrManager;		//인사담당자
	private int extraRead;			//인재열람건수
	private String category;		//구분
	private String intro;			//회사소개
	private Date birth;				//설립일
	private String address;			//주소
	private String logo;			//로고
	private ArrayList<Attachment> atlist;  //사진들
	
	public Company() {}

	public Company(int uno, String userId, String companyName, String phone, String email, String registNum,
			String owner, String hrManager, int extraRead, String category, String intro, Date birth, String address,
			String logo, ArrayList<Attachment> atlist) {
		super();
		this.uno = uno;
		this.userId = userId;
		this.companyName = companyName;
		this.phone = phone;
		this.email = email;
		this.registNum = registNum;
		this.owner = owner;
		this.hrManager = hrManager;
		this.extraRead = extraRead;
		this.category = category;
		this.intro = intro;
		this.birth = birth;
		this.address = address;
		this.logo = logo;
		this.atlist = atlist;
	}

	public int getUno() {
		return uno;
	}

	public void setUno(int uno) {
		this.uno = uno;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRegistNum() {
		return registNum;
	}

	public void setRegistNum(String registNum) {
		this.registNum = registNum;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getHrManager() {
		return hrManager;
	}

	public void setHrManager(String hrManager) {
		this.hrManager = hrManager;
	}

	public int getExtraRead() {
		return extraRead;
	}

	public void setExtraRead(int extraRead) {
		this.extraRead = extraRead;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public ArrayList<Attachment> getAtlist() {
		return atlist;
	}

	public void setAtlist(ArrayList<Attachment> atlist) {
		this.atlist = atlist;
	}

	@Override
	public String toString() {
		return "Company [uno=" + uno + ", userId=" + userId + ", companyName=" + companyName + ", phone=" + phone
				+ ", email=" + email + ", registNum=" + registNum + ", owner=" + owner + ", hrManager=" + hrManager
				+ ", extraRead=" + extraRead + ", category=" + category + ", intro=" + intro + ", birth=" + birth
				+ ", address=" + address + ", logo=" + logo + ", atlist=" + atlist + "]";
	}


	
}
