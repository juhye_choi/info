package company.model.vo;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;

import resume.model.vo.Attachment;

public class CompanyInfo implements Serializable{
	
	private int uno;				//회원번호
	private String intro;			//회사소개
	private Date birth;				//설립일
	private String address;			//주소
	private ArrayList<Attachment> atlist;  //사진들
	
	public CompanyInfo() {}

	public CompanyInfo(int uno, String intro, Date birth, String address, ArrayList<Attachment> atlist) {
		super();
		this.uno = uno;
		this.intro = intro;
		this.birth = birth;
		this.address = address;
		this.atlist = atlist;
	}

	public int getUno() {
		return uno;
	}

	public void setUno(int uno) {
		this.uno = uno;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public ArrayList<Attachment> getAtlist() {
		return atlist;
	}

	public void setAtlist(ArrayList<Attachment> atlist) {
		this.atlist = atlist;
	}

	@Override
	public String toString() {
		return "CompanyInfo [uno=" + uno + ", intro=" + intro + ", birth=" + birth + ", address=" + address
				+ ", atlist=" + atlist + "]";
	}

	
	
	
}
