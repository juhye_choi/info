package company.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

public class RecruitAttachment {

	private int atId;
	private String originName;
	private String changeName;
	private String filePath;
	private Date atDate;
	private String removeYN;
	private Date removeDate;
	private int category;
	private int formRecid;
	
	public RecruitAttachment() {}

	public RecruitAttachment(int atId, String originName, String changeName, String filePath, Date atDate,
			String removeYN, Date removeDate, int category, int formRecid) {
		super();
		this.atId = atId;
		this.originName = originName;
		this.changeName = changeName;
		this.filePath = filePath;
		this.atDate = atDate;
		this.removeYN = removeYN;
		this.removeDate = removeDate;
		this.category = category;
		this.formRecid = formRecid;
	}

	public int getAtId() {
		return atId;
	}

	public void setAtId(int atId) {
		this.atId = atId;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getChangeName() {
		return changeName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getAtDate() {
		return atDate;
	}

	public void setAtDate(Date atDate) {
		this.atDate = atDate;
	}

	public String getRemoveYN() {
		return removeYN;
	}

	public void setRemoveYN(String removeYN) {
		this.removeYN = removeYN;
	}

	public Date getRemoveDate() {
		return removeDate;
	}

	public void setRemoveDate(Date removeDate) {
		this.removeDate = removeDate;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getFormRecid() {
		return formRecid;
	}

	public void setFormRecid(int formRecid) {
		this.formRecid = formRecid;
	}

	@Override
	public String toString() {
		return "RecruitAttachment [atId=" + atId + ", originName=" + originName + ", changeName=" + changeName
				+ ", filePath=" + filePath + ", atDate=" + atDate + ", removeYN=" + removeYN + ", removeDate="
				+ removeDate + ", category=" + category + ", formRecid=" + formRecid + "]";
	}
	
	
	
}
