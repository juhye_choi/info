package company.model.vo;

import java.io.Serializable;
import java.util.Date;

public class Payment implements Serializable {
	
	private int payId;
	private int pCode;
	private int pUno;
	private int aUno;
	private int recId;
	private Date payDate;
	
	
	public Payment() {}


	public Payment(int payId, int pCode, int pUno, int aUno, int recId, Date payDate) {
		super();
		this.payId = payId;
		this.pCode = pCode;
		this.pUno = pUno;
		this.aUno = aUno;
		this.recId = recId;
		this.payDate = payDate;
	}


	public int getPayId() {
		return payId;
	}


	public void setPayId(int payId) {
		this.payId = payId;
	}


	public int getpCode() {
		return pCode;
	}


	public void setpCode(int pCode) {
		this.pCode = pCode;
	}


	public int getpUno() {
		return pUno;
	}


	public void setpUno(int pUno) {
		this.pUno = pUno;
	}


	public int getaUno() {
		return aUno;
	}


	public void setaUno(int aUno) {
		this.aUno = aUno;
	}


	public int getRecId() {
		return recId;
	}


	public void setRecId(int recId) {
		this.recId = recId;
	}


	public Date getPayDate() {
		return payDate;
	}


	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}


	@Override
	public String toString() {
		return "Payment [payId=" + payId + ", pCode=" + pCode + ", pUno=" + pUno + ", aUno=" + aUno + ", recId=" + recId
				+ ", payDate=" + payDate + "]";
	}
	
	

}
