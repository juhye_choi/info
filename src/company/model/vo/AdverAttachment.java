package company.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import resume.model.vo.Attachment;


@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class AdverAttachment {

	private int atId;
	private String originName;
	private String changeName;
	private String filePath;
	private Date atDate;
	private String removeYN;
	private Date removeDate;
	private int category;
	private int fileLevel;
	
	private int payId;
}
