package company.model.vo;

import java.sql.Date;
import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PaymentList {
 
	private int payid;			//결제ID 
	private Date paymentDate;	//결제날자
	private String payhistroyCategory;  //결제 구분 ( 요청 , 승인 , 환불 ) 
	private int paymentPrice;   //결제가격
	private String productName; //상품명
	private int pcode;			//상품코드
	private int puno;			//인재열람uno
	private int auno;			//광고 uno
	private int recid;			//적극채용 공고 id
	
	private ArrayList<PaymentHistoryList> list;		//상세 내역 조회
	
}
