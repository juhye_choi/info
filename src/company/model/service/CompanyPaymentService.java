package company.model.service;

import static common.JDBCTemplate.close;
import static common.JDBCTemplate.commit;
import static common.JDBCTemplate.getConnection;
import static common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;

import company.model.dao.CompanyDao;
import company.model.dao.CompanyPaymentDao;
import company.model.vo.CompanyPayment;
import company.model.vo.PayMentDates;
import company.model.vo.PaymentList;
import company.model.vo.UsingAdverList;
import company.model.vo.UsingPersonList;
import company.model.vo.UsingPopRecruit;
import keyword.model.vo.PageInfo;
public class CompanyPaymentService {
	//적극채용공고 결제시 결제내역 , 결제상세내역 , 적극채용공고 관리 추가 service
	public int insertPayment(CompanyPayment p) {
		Connection con = getConnection();
		
		int resultOne = 0;
		int resultTwo = 0;
		int resultThree = 0;
		int resultFour = 0;
		int CurrentVal = 0;
		//결제 내역 insert
		resultOne = new CompanyPaymentDao().insertPayment(con, p);
		System.out.println("resultOne : " + resultOne);
		 
		if(resultOne > 0) {
			//CurrentVal 가져오기
			CurrentVal = new CompanyPaymentDao().selectCurrentVal(con);
			System.out.println("Curval : " + CurrentVal);
			//결제상세내역 요청 inseret
			resultTwo = new CompanyPaymentDao().insertPaymentHistory(con , p , CurrentVal);
			System.out.println("resultTwo : " + resultTwo);
			
			//결제상세내역 승인 insert
			resultThree = new CompanyPaymentDao().insertPaymentHistory2(con, p, CurrentVal);
			System.out.println("resultTwo : " + resultThree);
			
			if(resultTwo+resultThree>1) {
				//적극채용공고관리 insert
				resultFour = new CompanyPaymentDao().insertPOP_Recruit(con , p, CurrentVal);
				System.out.println("resultFour : " + resultFour);
				
				if(resultFour > 0) {
					//전체 성공시 COMMIT
					commit(con);
				}else {
					//전체 실패시 ROLLBACK
					rollback(con);
				}
			}else {
				rollback(con);
			}
		}else {
			rollback(con);
		}
		
		close(con);
		
		return resultFour;
	}
	//인재열람 결제 시 결제내역 , 결제 상세내역 insert , 기업회원 추가정보테이블 update service
	public int insertPersonPayment(CompanyPayment cp) {
		Connection con = getConnection();
		
		int resultOne = 0;
		int resultTwo = 0;
		int resultThree = 0;
		int resultFour = 0;
		int CurrentVal = 0;
		
		//결제내역 insert
		resultOne = new CompanyPaymentDao().insertPersonPayment(con, cp);
		System.out.println("resultOne : " + resultOne);
		
		if(resultOne > 0) {
			//CurrentVal 가져오기
			CurrentVal = new CompanyPaymentDao().selectCurrentVal(con);
			System.out.println("Curval : " + CurrentVal);
			
			resultTwo = new CompanyPaymentDao().insertPersonPaymentHistory(con, cp , CurrentVal);
			System.out.println("resultTwo : "  + resultTwo);
			
			resultThree = new CompanyPaymentDao().insertPersonPaymentHistory2(con, cp, CurrentVal);
			System.out.println("resultThree : " + resultThree);
			
			if(resultTwo+resultThree > 1) {
				//update 인재열람건수
				resultFour = new CompanyPaymentDao().updateCompanyPersonCount(con , cp , CurrentVal);
				System.out.println("resultFour : " + resultFour);
				
				if(resultFour > 0) {
					//전체성공시 commit
					commit(con);
				}else {
					rollback(con);
				}
			}else {
				rollback(con);
			}
		}else {
			rollback(con);
		}
		
		close(con);
		
		return resultFour;
	}
	
	//현 사용자의 인재열람 상품 갯수 Select
	public int selectViewCount(int uno) {
		Connection con = getConnection();
		
		int result = new CompanyPaymentDao().selectViewCount(uno , con);
		
		close(con);
		
		return result;
	}
	
	//광고배너 결제시 결제내역 , 결제상세내역 , 배너공고관리 , 첨부파일 테이블 insert
	public int PaymentAdverServlet(CompanyPayment cp) {
		Connection con = getConnection();
		
		int result = 0;
		int result1 = 0;
		int result2 = 0;
		int result3 = 0;
		int result4 = 0;
		int CurrentVal = 0;
		
		result1 = new CompanyPaymentDao().insertAdverPayment(con , cp);
		//결제내역에 Tabel insert 했을시 
		System.out.println("result1 : " + result1);
		if(result1 > 0) {
			CurrentVal = new CompanyPaymentDao().selectCurrentVal(con);
			//상세결제내역 Table insert ( 요청 ) 
			result2 = new CompanyPaymentDao().insertAdverPaymentHistory(con , cp , CurrentVal);
			System.out.println("result2 : " + result2);
			
			if(result2 > 0) {
				//배너등록관리 Table insert
				result3 = new CompanyPaymentDao().insertBanner(con , cp , CurrentVal);
				System.out.println("result3 : " + result3);
				if(result3 > 0) {
					// Attachment Table insert
					cp.getAdverAttachment().setPayId(CurrentVal);
					result4 = new CompanyPaymentDao().insertAttachment(con , cp , CurrentVal , 6);
					System.out.println("result4 : " + result4);
					if(result4 > 0) {
						result=1;
						commit(con);
					}else {
						rollback(con);
					}
				}else {
					rollback(con);
				}
			}else {
				rollback(con);
			}
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}
	
	//결제 내역 가져오기
	public ArrayList<PaymentList> SelectAllPaymentList(int uno, PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<PaymentList> list = new CompanyPaymentDao().SelectAllPaymentList(uno , con, pi);
		
		close(con);
		
		return list;
	}
	//조회 버튼 클릭시 상세 내역 가져오기
	public ArrayList<PaymentList> selectDetailPaymentList(int uno, int payid) {
		Connection con = getConnection();
		
		ArrayList<PaymentList> list = new CompanyPaymentDao().selectDetailPaymentList(uno , payid , con);
		
		close(con);
		
		return list;
	}
	//한개의 적극채용공고 결제에 따른 시작 날짜 조회
	public PayMentDates selectPoPDate(int uno, int payid) {
		Connection con = getConnection();
		
		PayMentDates pd = new CompanyPaymentDao().selectPoPDate(con , uno , payid);
		
		close(con);
		
		return pd;
	}
	
	//결제목록에서 선택한 결제목록의 view 카운트를 조회
	public int selectCount(int uno, int payid) {
		Connection con = getConnection();
		
		int count = new CompanyPaymentDao().selectCount(con, uno , payid);
		
		close(con);
		
		return count;
	}
	//결제목록에서 선택한 결제목록의 시작날자 조회 
	public PayMentDates selectAdverDate(int uno, int payid) {
		Connection con = getConnection();
		
		PayMentDates pd = new CompanyPaymentDao().selectAdverDate(con , uno , payid);
		
		close(con);
		return pd;
	}
	public int getListCount(int uno) {
		Connection con = getConnection();
		int result = 0;
		
		result = new CompanyPaymentDao().getListCount(con, uno);
		
		if(result != 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}
	
	//자신이 이용한 열람한 이용자들 리스트 (기업)
	public ArrayList<UsingPersonList> usingPersonList(int uno) {
		Connection con = getConnection();
		
		ArrayList<UsingPersonList> list = new CompanyPaymentDao().usingPersonList(con , uno);
		
		close(con);
		return list;
	}
	//자신이 이용한 광고 리스트 목록 ( 기업 ) 
	public ArrayList<UsingAdverList> usingAdverList(int uno) {
		Connection con = getConnection();
		
		ArrayList<UsingAdverList> list = new CompanyPaymentDao().usingAdverList(con , uno);
				
		close(con);
		
		return list;
	}
	//자신이 이용한 적극채용공고 리스트 ( 기업 ) 
	public ArrayList<UsingPopRecruit> usingPopRecruitList(int uno) {
		Connection con = getConnection();
		
		ArrayList<UsingPopRecruit> list = new CompanyPaymentDao().usingPopRecruitList(con , uno);
		
		close(con);
		
		return list;
	}
	//클릭한 사진 경로 받아오기
	public String UsingAdverPicturePath(int payid) {
		Connection con = getConnection();
		
		String path = new CompanyPaymentDao().UsingAdverPicturePath(payid , con);
		
		close(con);
		
		return path;
	}
	
	
	public ArrayList<Integer> selectPopRecruitDateCount() {
		Connection con = getConnection();
		
		ArrayList<Integer> list = new CompanyPaymentDao().selectPopRecruitDateCount(con);
		
		close(con);
		
		return list;
	}
	
	
	//전체 광고배너 startdate , enddate
	public ArrayList<UsingAdverList> selectAdverList(int pcode) {
		Connection con = getConnection();
		
		ArrayList<UsingAdverList> list = new CompanyPaymentDao().selectAdverList(con , pcode);
		
		close(con);
		
		return list;
	}
	//환불요청 버튼 클릭시 환불요청 inseret ( category ==4 ) 
	public int insertRefundrequest(int payid) {
		Connection con = getConnection();
		int result = 0;
		
		result = new CompanyPaymentDao().insertRefundrequest(payid , con);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		return result;
	}
	


}
