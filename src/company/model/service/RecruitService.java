package company.model.service;

import static common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;

import admin.model.vo.NowPopList;
import company.model.dao.RecruitDao;
import company.model.vo.Recruit;
public class RecruitService {
	// 기업 공고 게시물 조회  (uno , 공고가 진행중인 공고들 만 )
	public ArrayList<Recruit> selectRecruitList(int uno) {
		Connection con = getConnection();
		
		ArrayList<Recruit> list = new RecruitDao().selectRecruitList(con , uno);
		
		close(con);
		
		return list;
	}
	 
	//채용공고 등록용 게시물
	public int insertRecruit(Recruit rec, int addAtt) {
		Connection con = getConnection();
		
		int result = 0;
		int resultR = 0;
		int resultA = 0;
		int resultK = 0;
		int recId = 0;
		
		resultR = new RecruitDao().insertRecruit(con, rec);
		recId = new RecruitDao().selectCurrentVal(con);
		
		if(resultR > 0) {
			for(int i=0; i<rec.getKlist().size(); i++) {
				resultK += new RecruitDao().insertKeyword(con, recId, rec.getKlist().get(i));
			}
			if(resultK >= rec.getKlist().size()) {
				if(addAtt > 0) {
					resultA = new RecruitDao().insertAttachment(con, rec, recId);
					if(resultA > 0)
					{
						commit(con);
						result = 1;	
					} else {
						rollback(con);
					}
				} else {
					commit(con);
					result = 1;
				}
			} else {
				rollback(con);
			}
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}
	//현재 올라와있는 적극채용 공고 카운트와 가장 늦게끝나는 end날짜 가져오기(max) , 가장 빠른 start날짜 가져오기 (min)
	public ArrayList<NowPopList> nowPoplist() {
		Connection con = getConnection();
		
		ArrayList<NowPopList> nowPoplist = new RecruitDao().nowPopList(con);
		
		close(con);
		
		return nowPoplist;
	}

}
