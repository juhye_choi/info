package company.model.service;

import static common.JDBCTemplate.close;
import static common.JDBCTemplate.commit;
import static common.JDBCTemplate.getConnection;
import static common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;

import company.model.dao.CompanyDao;
import company.model.vo.Company;
import company.model.vo.CompanyInfo;
import company.model.vo.Recruit;
import company.model.vo.RecruitAttachment;
import keyword.model.vo.Keyword;
import person.model.dao.PersonDao;
import person.model.vo.Person;
import person.model.vo.PersonalBlack;
import resume.model.vo.Attachment;

public class CompanyService {
	
	public ArrayList<Person> searchTalent(int uno) {
		Connection con = getConnection();
	      
	    ArrayList<Person> list = new CompanyDao().searchTalent(con, uno);
	      
	    close(con);
	      
	    return list;
	}

	public ArrayList<Person> searchRefTalent(int uno) {
		Connection con = getConnection();
		
		String quarter = new CompanyDao().selectQuarter(con);
		ArrayList<Person> rlist = null;
		
		if(quarter != null) {
			 rlist = new CompanyDao().searchRefTalent(con, quarter, uno);
		}
		
		close(con);
		
		return rlist;
	}

	public int checkPayHistory(int uno, int otherUno) {
		Connection con = getConnection();
		
		int result = new CompanyDao().checkPayHistory(con, uno, otherUno);
		
		close(con);
		
		return result;
	}

	public int viewCount(int cUno) {
		Connection con = getConnection();
		
		int count = new CompanyDao().viewCount(con, cUno);
		
		close(con);
		
		return count;
	}

	public int insertSeenParson(int cUno, int otherUno, int pjtId, String title) {
		Connection con = getConnection();
		
		int result = new CompanyDao().insertSeenParson(con, cUno, otherUno, title);
		int result1 = 0;
		if(result > 0) {
			result1 = new CompanyDao().updateCompanyAdd(con, cUno);
			
			if(result1 > 0) {
				commit(con);
			}else {
				rollback(con);
			}
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result1;
	}

	public ArrayList<PersonalBlack> selectBlackList(int uno) {
		Connection con = getConnection();
		
		ArrayList<PersonalBlack> list = new CompanyDao().selectBlackList(con, uno);
		
		close(con);
		
		return list;
	}

	public ArrayList<Person> searchKeyTalent(String keyword, int uno) {
		Connection con = getConnection();
		
		ArrayList<Person> list = new CompanyDao().searchKeyTalent(con, keyword, uno);
		
		close(con);
		
		return list;
	}

	public ArrayList<Person> searchKeyRefTalent(String keyword, int uno) {
		Connection con = getConnection();
		
		String quarter = new PersonDao().selectQuarter(con);
		ArrayList<Person> rlist = null;
		
		if(quarter != null) {
			rlist = new CompanyDao().searchKeyRefTalent(con, keyword, quarter, uno);
		}
		
		close(con);
		
		return rlist;
	}

	public ArrayList<Keyword> selectKeyword(String searchKeyword) {
		Connection con = getConnection();
				
		ArrayList<Keyword> klist = new CompanyDao().selectKeywordList(con, searchKeyword);
		
		close(con);
		
		return klist;
	}
	//기업회원 정보 업데이트
	public int updateProfile(RecruitAttachment ra, Company c) {
		Connection con = getConnection();
		int resultC = 0;
		int resultCa = 0;
		int resultA = 0;
		int result = 0;

		resultCa = new CompanyDao().updateCompanyAddHR(con, c);
		if(resultCa > 0) {
			resultC = new CompanyDao().updateMemberC(con, c);
			if(resultC > 0) {
				System.out.println("ra : " + ra);
				if(ra.getChangeName() != null) {
					resultA = new CompanyDao().updateImg(con, ra, c.getUno());
					if(resultA > 0) {
						result = 1;
						commit(con);
					} else {
						rollback(con);
					}
				} else {
					result = 1;
					commit(con);
				}
			} else {
				rollback(con);
			}
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}
	//기업회원 정보 불러오기
	public Company selectMember(int uno) {
		Connection con = getConnection();
		Company c = new Company();
		
		c = new CompanyDao().selectMember(con, uno);
		c.setLogo(new CompanyDao().selectLogo(con, uno));
		c.setAtlist(new CompanyDao().selectAttachImg(con, uno));
		
		close(con);
		
		return c;
	}
	

	public ArrayList<Person> nLoginSearchKeyTalent(String keyword) {
		Connection con = getConnection();
		
		ArrayList<Person> list = new CompanyDao().nLoginSearchKeyTalent(con, keyword);
		
		close(con);
		
		return list;
	}

	public ArrayList<Person> searchContainKeyTalent(String searchKeyword, int uno) {
		Connection con = getConnection();
		
		ArrayList<Person> list = new CompanyDao().searchContainKeyTalent(con, searchKeyword, uno);
		
		close(con);
		
		return list;
	}

	public ArrayList<Person> nLoginSearchContainKeyTalent(String keyword) {
		Connection con = getConnection();
		
		ArrayList<Person> list = new CompanyDao().nLoginSearchContainKeyTalent(con, keyword);
		
		close(con);
		
		return list;
	}

	public ArrayList<Person> nLoginSearchKeyRefTalent(String keyword) {
		Connection con = getConnection();
		
		String quarter = new PersonDao().selectQuarter(con);
		ArrayList<Person> rlist = null;
		
		if(quarter != null) {
			rlist = new CompanyDao().nLoginSearchKeyRefTalent(con, keyword, quarter);
		}
		
		close(con);
		
		return rlist;
	}

	public ArrayList<Person> nLoginSearchContainKeyRefTalent(String searchKeyword) {
		Connection con = getConnection();
		
		String quarter = new PersonDao().selectQuarter(con);
		ArrayList<Person> rlist = null;
		
		if(quarter != null) {
			rlist = new CompanyDao().nLoginSearchContainKeyRefTalent(con, searchKeyword, quarter);
		}
		
		close(con);
		
		return rlist;
	}

	public ArrayList<Person> searchContainKeyRefTalent(String searchKeyword, int uno) {
		Connection con = getConnection();
		
		String quarter = new PersonDao().selectQuarter(con);
		ArrayList<Person> rlist = null;
		
		if(quarter != null) {
			rlist = new CompanyDao().searchContainKeyRefTalent(con, searchKeyword, quarter, uno);
		}
		
		close(con);
		
		return rlist;
	}

	public int countUserAllRec(int uno) {
		Connection con = getConnection();
		
		int count = 0;
		
		count = new CompanyDao().countUserAllRec(con, uno);
		
		close(con);
		
		return count;
	}
	
	public int countUserIngRec(int uno) {
		Connection con = getConnection();
		
		int count = 0;
		
		count = new CompanyDao().countUserIngRec(con, uno);
		
		close(con);
		
		return count;
	}
	//전체 채용공고
	public ArrayList<Recruit> selectAllRec(int uno) {
		Connection con = getConnection();
		ArrayList<Recruit> list = null;
		
		list = new CompanyDao().selectAllRec(con, uno);
		
		close(con);
		
		return list;
	}
	//진행중인 채용공고
	public ArrayList<Recruit> selectIngRec(int uno) {
		Connection con = getConnection();
		ArrayList<Recruit> list = null;
		
		list = new CompanyDao().selectIngRec(con, uno);
		
		close(con);
		
		return list;
	}
	//끝난 채용공고
	public ArrayList<Recruit> selectEndRec(int uno) {
		Connection con = getConnection();
		ArrayList<Recruit> list = null;
		
		list = new CompanyDao().selectEndRec(con, uno);
		
		close(con);
		
		return list;
	}
	
	//기업 소개 있는지 확인
		public int checkCompanyInfo(int uno) {
			Connection con = getConnection();
			int result = 0;
			int resultC = new CompanyDao().checkCompanyInfo(con, uno);
			
			if(resultC > 0) {
				result = 1;
			} else {
				int success = new CompanyDao().createCompanyInfo(con, uno);
				
				if(success > 0) {
					result = 1;
				}else {
					result = 0;
				}
			}
			
			close(con);
			
			return result;
		}
		//기업 소개 업데이트
		public int updateCompanyInfo(int uno, CompanyInfo ci) {
			Connection con = getConnection();
			int result = 0;
			int resultI = new CompanyDao().updateCompanyInfo(con, uno, ci);
			
			if(resultI > 0) {
				if(ci.getAtlist().size() > 0) {
					for(Attachment a : ci.getAtlist()) {
						result += new CompanyDao().insertImg(con, uno, a);
					}
					if(result != ci.getAtlist().size()) {
						rollback(con);
					}
					
				} else {
					result = 1;
				}
			}
			close(con);
			
			return result;
		}
		//끝난 채용공고 갯수조회
		public int countUserEndRec(int uno) {
			Connection con = getConnection();
			
			int count = 0;
			
			count = new CompanyDao().countUserEndRec(con, uno);
			
			close(con);
			
			return count;
		}
}
