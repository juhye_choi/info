package company.model.dao;

import static common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import admin.model.vo.NowPopList;
import company.model.vo.Recruit;
import keyword.model.vo.Keyword;

public class RecruitDao {
	private Properties prop = new Properties();
	
	public RecruitDao() {
		
		String fileName = RecruitDao.class.getResource("/sql/recruit/recruit-query.properties").getPath();
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	} 
	
	//기업 공고 리스트 불러오기
	public ArrayList<Recruit> selectRecruitList(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Recruit> list = null;
		
		String query = prop.getProperty("selectRecruitList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Recruit>();
			while(rset.next()) {
				Recruit r = new Recruit();
				r.setRecid(rset.getInt("RECID"));
				r.setUno(rset.getInt("UNO"));
				r.setRec_title(rset.getString("REC_TITLE"));
				r.setJob_position(rset.getString("JOB_POSITION"));
				r.setCareer_period(rset.getString("CAREER_PERIOD"));
				r.setRectype(rset.getInt("REC_TYPE"));
				r.setRec_pcount(rset.getInt("REC_PCOUNT"));
				r.setEducation(rset.getInt("EDUCATION"));
				r.setSpe_condition(rset.getString("SPE_CONDITION"));
				r.setSalary_way(rset.getInt("SALARY_WAY"));
				r.setMin_salary(rset.getInt("MIN_SALARY"));
				r.setMax_salary(rset.getInt("MAX_SALARY"));
				r.setWork_area(rset.getString("WORK_AREA"));
				r.setRec_start(rset.getDate("REC_START"));
				r.setRec_finish(rset.getDate("REC_FINISH"));
				r.setHowto_apply(rset.getInt("HOWTO_APPLY"));
				r.setRec_date(rset.getDate("REC_DATE"));
				
				list.add(r);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}

	public int insertRecruit(Connection con, Recruit rec) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertRecruit");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, rec.getUno());
			pstmt.setString(2, rec.getRec_title());
			pstmt.setString(3, rec.getJob_position());
			pstmt.setString(4, rec.getCareer_period());
			pstmt.setInt(5, rec.getRectype());
			pstmt.setInt(6, rec.getRec_pcount());
			pstmt.setInt(7, rec.getEducation());
			pstmt.setString(8, rec.getSpe_condition());
			pstmt.setInt(9,  rec.getSalary_way());
			pstmt.setInt(10, rec.getMin_salary());
			pstmt.setInt(11, rec.getMax_salary());
			pstmt.setString(12, rec.getWork_area());
			pstmt.setDate(13, rec.getRec_start());
			pstmt.setDate(14, rec.getRec_finish());
			pstmt.setInt(15, rec.getHowto_apply());
			pstmt.setString(16, rec.getEligibility());
			pstmt.setString(17, rec.getBenefits_welfare());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	
	public int selectCurrentVal(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("recruitCurrval");
		
		try {
			stmt = con.createStatement();
			
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
		}
		return result;
	}

	public int insertAttachment(Connection con, Recruit rec, int recId) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertAttachment");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, rec.getRecruitAttachment().getOriginName());
			pstmt.setString(2, rec.getRecruitAttachment().getChangeName());
			pstmt.setString(3, rec.getRecruitAttachment().getFilePath());
			pstmt.setInt(4, recId);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int insertKeyword(Connection con, int recId, Keyword keyword) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertKeyword");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, keyword.getKcode());
			pstmt.setInt(2, recId);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	//현재 올라와있는 적극채용 공고 카운트와 가장 늦게끝나는 end날짜 가져오기(max) , 가장 빠른 start날짜 가져오기 (min)	
	public ArrayList<NowPopList> nowPopList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<NowPopList> nowPopList = null;
		
		String query = prop.getProperty("nowPopList");
		
		try {
			stmt = con.createStatement();
			
			rset = stmt.executeQuery(query);
			
			nowPopList = new ArrayList<>();
			
			while(rset.next()) {
				NowPopList npl = new NowPopList();
				npl.setPstartDate(rset.getDate("MIN"));
				npl.setPendDate(rset.getDate("MAX"));
				npl.setCount(rset.getInt("COUNT"));
				
				nowPopList.add(npl);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		return nowPopList;
	}

}
