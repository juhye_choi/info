package company.model.dao;

import static common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import company.model.vo.Company;
import company.model.vo.CompanyInfo;
import company.model.vo.Recruit;
import company.model.vo.UsingAdverList;
import company.model.vo.RecruitAttachment;
import keyword.model.vo.Keyword;
import person.model.vo.Attachment2;
import person.model.vo.Person;
import person.model.vo.PersonalBlack;
import resume.model.vo.Attachment;

public class CompanyDao {
	
	private Properties prop = new Properties();
	
	public CompanyDao() {
		String fileName = CompanyDao.class.getResource("/sql/company/company-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<Person> searchTalent(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;
		
		String query = prop.getProperty("searchTalent");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));

				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public String selectQuarter(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		String result = null;
		
		String query = prop.getProperty("selectQuarter");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				result = rset.getString("QUARTER");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return result;
	}

	public ArrayList<Person> searchRefTalent(Connection con, String quarter, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;
		
		String query = prop.getProperty("searchRefTalent");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, quarter);
			pstmt.setInt(2,  uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public int checkPayHistory(Connection con, int uno, int otherUno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("checkPayHistory");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setInt(2, otherUno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}
	//기업회원 전화번호 업데이트
	public int updateMemberC(Connection con, Company c) {
		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("updateCompanyPhone");
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, c.getPhone());
			pstmt.setInt(2, c.getUno());

			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}
	//기업회원 인사담당자 업데이트
	public int updateCompanyAddHR(Connection con, Company c) {
		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("updateCompanyHR");
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, c.getHrManager());
			pstmt.setInt(2, c.getUno());

			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}
	//기업회원 로고 업데이트
	public int updateImg(Connection con, RecruitAttachment ra, int uno) {
		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("insertAttachmentCompany");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, ra.getOriginName());
			pstmt.setString(2, ra.getChangeName());
			pstmt.setString(3, ra.getFilePath());
			pstmt.setInt(4, 5);
			pstmt.setInt(5, uno);
			
			result = pstmt.executeUpdate();	
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}
	
	//회원정보 가져오기
	public Company selectMember(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Company c = null;
		
		
		String query = prop.getProperty("selectMember");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				c = new Company();
				
				c.setUno(rset.getInt("UNO"));
				c.setUserId(rset.getString("USER_ID"));
				c.setEmail(rset.getString("EMAIL"));
				c.setPhone(rset.getString("PHONE"));
				c.setCompanyName(rset.getString("NAME"));
				c.setRegistNum(rset.getString("REGIST_NUM"));
				c.setOwner(rset.getString("OWNER"));
				c.setHrManager(rset.getString("HR_NAME"));
				c.setIntro(rset.getString("CINTRO"));
				c.setBirth(rset.getDate("CBIRTH"));
				c.setAddress(rset.getString("CADDRESS"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return c;
	}
	//로고이름 가져오기
	public String selectLogo(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String logo = null;
		
		String query = prop.getProperty("selectLogo");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				logo = (rset.getString("CHANGE_NAME"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return logo;
	}
	
	public int viewCount(Connection con, int cUno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int count = 0;
		
		String query = prop.getProperty("viewCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cUno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				count = rset.getInt("COUNT");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return count;
	}

	public int insertSeenParson(Connection con, int cUno, int otherUno, String title) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertSeenParson");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cUno);
			pstmt.setInt(2, otherUno);
			pstmt.setString(3, title);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int updateCompanyAdd(Connection con, int cUno) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateCompanyAdd");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cUno);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
		
		return result;
	}

	public ArrayList<PersonalBlack> selectBlackList(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<PersonalBlack> list = null;
		
		String query = prop.getProperty("selectBlackList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<PersonalBlack>();
			
			while(rset.next()) {
				PersonalBlack b = new PersonalBlack();
				
				b.setpUno(rset.getInt(""));
				
				list.add(b);
			}
					
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<Person> searchKeyTalent(Connection con, String keyword, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;
		
		String query = prop.getProperty("searchKeyTalent");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, keyword);
			pstmt.setInt(2, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<Person> searchKeyRefTalent(Connection con, String keyword, String quarter, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;
		
		String query = prop.getProperty("searchKeyRefTalent");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, keyword);
			pstmt.setString(2, quarter);
			pstmt.setInt(3, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<Keyword> selectKeyword(Connection con, String searchKeyword) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Keyword> klist = null;
		
		String query = prop.getProperty("selectKeyword");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchKeyword);
			
			rset = pstmt.executeQuery();
			
			klist = new ArrayList<Keyword>();
			
			while(rset.next()) {
				Keyword k = new Keyword();
				
				k.setKname(rset.getString("KNAME"));
				
				klist.add(k);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return klist;
	}

	public ArrayList<Keyword> selectKeywordList(Connection con, String searchKeyword) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Keyword> klist = null;
		
		String query = prop.getProperty("selectKeywordList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchKeyword);
			
			rset = pstmt.executeQuery();
			
			klist = new ArrayList<>();
			
			while(rset.next()) {
				Keyword k = new Keyword();
				
				k.setKname(rset.getString("KNAME"));
				
				klist.add(k);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return klist;
	}


	public ArrayList<Person> nLoginSearchKeyTalent(Connection con, String keyword) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;
		
		String query = prop.getProperty("nLoginSearchKeyTalent");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, keyword);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<Person> searchContainKeyTalent(Connection con, String searchKeyword, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;
		
		String query = prop.getProperty("searchContainKeyTalent");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchKeyword);
			pstmt.setInt(2, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}

	public ArrayList<Person> nLoginSearchContainKeyTalent(Connection con, String keyword) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;
		
		String query = prop.getProperty("nLoginSearchContainKeyTalent");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, keyword);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			if(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}

	public ArrayList<Person> nLoginSearchKeyRefTalent(Connection con, String keyword, String quarter) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> rlist = null;
		
		String query = prop.getProperty("nLoginSearchKeyRefTalent");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, keyword);
			pstmt.setString(2, quarter);
			
			rset = pstmt.executeQuery();
			
			rlist = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				rlist.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return rlist;
	}

	public ArrayList<Person> nLoginSearchContainKeyRefTalent(Connection con, String searchKeyword, String quarter) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> rlist = null;
		
		String query = prop.getProperty("nLoginSearchKeyRefTalent");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchKeyword);
			pstmt.setString(2, quarter);
			
			rset = pstmt.executeQuery();
			
			rlist = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				rlist.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return rlist;
	}

	public ArrayList<Person> searchContainKeyRefTalent(Connection con, String searchKeyword, String quarter, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> rlist = null;
		
		String query = prop.getProperty("searchContainKeyRefTalent");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchKeyword);
			pstmt.setString(2, quarter);
			pstmt.setInt(3, uno);
			
			rset = pstmt.executeQuery();
			
			rlist = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				rlist.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return rlist;
	}

	//기업 회원(당사자)가 올린 공고 전체 갯수
	public int countUserAllRec(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int count = 0;
		
		String query = prop.getProperty("countAllRec");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				count = rset.getInt("COUNT");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return count;
	}
	//기업 회원(당사자)가 올린 공고 진행중 갯수
	public int countUserIngRec(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int count = 0;
		
		String query = prop.getProperty("countIngRec");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				count = rset.getInt("COUNT");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return count;
	}
	//기업 회원(당사자)가 올린 종료된 공고  갯수
	public int countUserEndRec(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int count = 0;
		
		String query = prop.getProperty("countEndRec");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				count = rset.getInt("COUNT");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return count;
	}
	//전체 채용공고
	public ArrayList<Recruit> selectAllRec(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Recruit> list = null;
		
		String query = prop.getProperty("selectAllRec");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Recruit>();
			
			while(rset.next()) {
				Recruit r = new Recruit();
				
				r.setRecid(rset.getInt("RECID"));
				r.setRec_title(rset.getString("REC_TITLE"));
				r.setRec_start(rset.getDate("REC_START"));
				r.setRec_finish(rset.getDate("REC_FINISH"));

				list.add(r);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}
	//진행중 채용공고
	public ArrayList<Recruit> selectIngRec(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Recruit> list = null;
		
		String query = prop.getProperty("selectIngRec");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Recruit>();
			
			while(rset.next()) {
				Recruit r = new Recruit();
				
				r.setRecid(rset.getInt("RECID"));
				r.setRec_title(rset.getString("REC_TITLE"));
				r.setRec_start(rset.getDate("REC_START"));
				r.setRec_finish(rset.getDate("REC_FINISH"));

				list.add(r);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}
	//종료된 채용공고
	public ArrayList<Recruit> selectEndRec(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Recruit> list = null;
		
		String query = prop.getProperty("selectEndRec");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Recruit>();
			
			while(rset.next()) {
				Recruit r = new Recruit();
				
				r.setRecid(rset.getInt("RECID"));
				r.setRec_title(rset.getString("REC_TITLE"));
				r.setRec_start(rset.getDate("REC_START"));
				r.setRec_finish(rset.getDate("REC_FINISH"));

				list.add(r);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}
	
	//기업 소개사진 갯수
	public ArrayList<Attachment> selectAttachImg(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Attachment> list = null;
		
		String query = prop.getProperty("selectAttach");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			while(rset.next()) {
				Attachment a = new Attachment();
				a.setAtId(rset.getInt("ATID"));
				a.setChangeName(rset.getString("CHANGE_NAME"));
				a.setOriginName(rset.getString("ORIGIN_NAME"));
				list.add(a);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}
	//기업소개 있는지 확인
	public int checkCompanyInfo(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int count = 0;
		
		String query = prop.getProperty("countCompanyInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				count = rset.getInt("COUNT");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return count;
	}
	//없으면 생성
	public int createCompanyInfo(Connection con, int uno) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("insertCompanyInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, uno);
			
			result = pstmt.executeUpdate();	
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}
	//기업소개 업데이트
	public int updateCompanyInfo(Connection con, int uno, CompanyInfo ci) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateCompanyInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, ci.getIntro());
			pstmt.setDate(2, ci.getBirth());
			pstmt.setString(3, ci.getAddress());
			pstmt.setInt(4, uno);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int insertImg(Connection con, int uno, Attachment a) {
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(a);
		String query = prop.getProperty("insertCompanyImg");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, a.getOriginName());
			pstmt.setString(2, a.getChangeName());
			pstmt.setString(3, a.getFilePath());
			pstmt.setInt(4, uno);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}


}
