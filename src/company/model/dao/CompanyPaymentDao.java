package company.model.dao;

import static common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import company.model.vo.CompanyPayment;
import company.model.vo.PayMentDates;
import company.model.vo.PaymentList;
import company.model.vo.UsingAdverList;
import company.model.vo.UsingPersonList;
import company.model.vo.UsingPopRecruit;
import keyword.model.vo.PageInfo;

public class CompanyPaymentDao {
	private Properties prop = new Properties();
	
	public CompanyPaymentDao() {
		String fileName = CompanyPaymentDao.class.getResource("/sql/companyPayment/payment-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	//적극채용공고 결제내역 insert
	public int insertPayment(Connection con, CompanyPayment p) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPayment");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, p.getProductCode());
			pstmt.setInt(2, p.getRecid());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}
	//CurrentVal가져오기
	public int selectCurrentVal(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("selectCurrval");
		
		try {
			stmt = con.createStatement();
			
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
		}
		return result;
	}
	//결제 상세내역 insert (요청)
	public int insertPaymentHistory(Connection con, CompanyPayment p, int currentVal) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPaymentHistory");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1,currentVal );
			pstmt.setInt(2, p.getCategory1());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}
	//결제 상세내역 insert(승인)
	public int insertPaymentHistory2(Connection con, CompanyPayment p, int currentVal) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPaymentHistory2");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, currentVal );
			pstmt.setInt(2, p.getCategory2());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}
	//적극 채용공고 관리 insert
	public int insertPOP_Recruit(Connection con, CompanyPayment p, int currentVal) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPOP_Recruit");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, currentVal);
			pstmt.setDate(2, p.getStartDate());
			pstmt.setDate(3, p.getEndDate());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}
	
	//인재열람상품 결제내역 INSERT
	public int insertPersonPayment(Connection con, CompanyPayment cp) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPersonPayment");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, cp.getProductCode());
			pstmt.setInt(2, cp.getUno());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}
	
	//인재열람 결제 후 상세 결제 내역에 insert ( 요청 )
	public int insertPersonPaymentHistory(Connection con, CompanyPayment cp, int currentVal) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPersonPaymentHistory1");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, currentVal );
			pstmt.setInt(2, cp.getCategory1());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}
	//인재열람 결제 후 상세 결제 내역에 insert ( 승인 )
	public int insertPersonPaymentHistory2(Connection con, CompanyPayment cp, int currentVal) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPersonPaymentHistory2");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, currentVal );
			pstmt.setInt(2, cp.getCategory2());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}
	//인재열람 갯수 update (인재열람 갯수 업데이트)
	public int updateCompanyPersonCount(Connection con, CompanyPayment cp, int currentVal) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateCompanyPersonCount");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, cp.getCount());
			pstmt.setInt(2, cp.getUno());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	//현 사용자의 인재열람 상품 갯수 Select
	public int selectViewCount(int uno, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("selectViewCount");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}
	//광고배너 결제시 결제내역 , 결제상세내역 , 배너공고관리 , 첨부파일 테이블 insert
	
	// 광고배너 결제내역 insert
	public int insertAdverPayment(Connection con, CompanyPayment cp) {
		PreparedStatement pstmt = null;
		int result = 0 ;
		
		String query = prop.getProperty("insertAdverPayment");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, cp.getProductCode());
			pstmt.setInt(2, cp.getUno());
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
		
	}
	//광고 배너 결제시 상세결재내역 insert (요청)
	public int insertAdverPaymentHistory(Connection con, CompanyPayment cp, int currentVal) {
		PreparedStatement pstmt = null;
		int result =0;
		
		String query = prop.getProperty("insertAdverPaymentHistory");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, currentVal);
			pstmt.setInt(2, cp.getCategory1());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result;
	}
	//Banner Tabel insert
	public int insertBanner(Connection con, CompanyPayment cp, int currentVal) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertBanner");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, currentVal);
			pstmt.setDate(2, cp.getStartDate());
			pstmt.setDate(3, cp.getEndDate());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int insertAttachment(Connection con, CompanyPayment cp, int currentVal, int i) {
		PreparedStatement pstmt = null;
		int result = 0 ;
		
		String query = prop.getProperty("insertAttachment");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, cp.getAdverAttachment().getOriginName());
			pstmt.setString(2, cp.getAdverAttachment().getChangeName());
			pstmt.setString(3, cp.getAdverAttachment().getFilePath());
			pstmt.setInt(4, i);
			pstmt.setInt(5, cp.getAdverAttachment().getPayId());
			pstmt.setInt(6, cp.getAdverAttachment().getFileLevel());
			
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	
	//결제 내역 Table ( 한 기업회원이 결제한 목록 조회 ) 
	public ArrayList<PaymentList> SelectAllPaymentList(int uno, Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset= null;
		ArrayList<PaymentList> list = null;
		
		String query = prop.getProperty("SelectAllPaymentList");
		
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, uno);
			pstmt.setInt(2, uno);
			pstmt.setInt(3, uno);
			pstmt.setInt(4, startRow);
			pstmt.setInt(5, endRow);
			
			rset  = pstmt.executeQuery();
			
			list = new ArrayList<PaymentList>();
			
			while(rset.next()) {
				PaymentList p = new PaymentList();
				
				p.setPayid(rset.getInt("PAYID"));
				p.setPaymentDate(rset.getDate("PAYDATE"));
				p.setPayhistroyCategory(rset.getString("CAT"));
				p.setPaymentPrice(rset.getInt("PMONEY"));
				p.setProductName(rset.getString("PNAME"));
				p.setPcode(rset.getInt("PCODE"));
				p.setPuno(rset.getInt("PUNO"));
				p.setAuno(rset.getInt("AUNO"));
				p.setRecid(rset.getInt("RECID"));
				
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		return list;
	}
	// 조회 버튼 클릭시 DetailList 가져오기
	public ArrayList<PaymentList> selectDetailPaymentList(int uno, int payid, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<PaymentList> list = null;
		
		String query = prop.getProperty("selectDetailPaymentList");

		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, uno);
			pstmt.setInt(2, payid);
			pstmt.setInt(3, uno);
			pstmt.setInt(4, payid);
			pstmt.setInt(5, uno);
			pstmt.setInt(6, payid);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<PaymentList>();
			while(rset.next()) {
				PaymentList p = new PaymentList();
				
				p.setPayid(rset.getInt("PAYID"));
				p.setPaymentDate(rset.getDate("PAYDATE"));
				p.setPayhistroyCategory(rset.getString("CAT"));
				p.setPaymentPrice(rset.getInt("PMONEY"));
				p.setProductName(rset.getString("PNAME"));
				p.setPcode(rset.getInt("PCODE"));
				p.setPuno(rset.getInt("PUNO"));
				p.setAuno(rset.getInt("AUNO"));
				p.setRecid(rset.getInt("RECID"));
				
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}
	//선택한 결제 공고의 시작날짜 , 현재날짜 리턴
	public PayMentDates selectPoPDate(Connection con, int uno, int payid) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		PayMentDates pd = null;
		
		String query = prop.getProperty("selectPoPDate");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, uno);
			pstmt.setInt(2, payid);
			pstmt.setInt(3, 1);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				pd = new PayMentDates();
				
				pd.setStartDate(rset.getDate("STARTDATE"));
				pd.setSysDate(rset.getDate("SYSDATE"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return pd;
	}
	//선택한 결제내역에 따른 인재열람 갯수 카운트
	public int selectCount(Connection con, int uno, int payid) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int count = 0;
		
		String query = prop.getProperty("selectCount");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, uno);
			pstmt.setInt(2, payid);
			pstmt.setInt(3, 1);
			rset = pstmt.executeQuery();
			if(rset.next()) {
				count = rset.getInt("COUNT");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return count;
	}
	//결제목록에서 선택한 결제목록의 시작날자 조회 
	public PayMentDates selectAdverDate(Connection con, int uno, int payid) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		PayMentDates pd = null;
		
		String query = prop.getProperty("selectAdverDate");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, uno);
			pstmt.setInt(2, payid);
			pstmt.setInt(3, 1);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				pd = new PayMentDates();
				
				pd.setStartDate(rset.getDate("STARTDATE"));
				pd.setSysDate(rset.getDate("SYSDATE"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		return pd;
	}
	//페지징처리를 위한 count 갯수 가져오기
	public int getListCount(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setInt(2, uno);
			pstmt.setInt(3, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return result;
	}
	//자신이 이용한 열람한 이용자들 리스트 (기업)	
	public ArrayList<UsingPersonList> usingPersonList(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<UsingPersonList> list = null;
		
		String query = prop.getProperty("usingPersonList");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<UsingPersonList>();
			
			while(rset.next()) {
				UsingPersonList ul = new UsingPersonList();
				
				ul.setOpenDate(rset.getDate("SPDATE"));
				ul.setProjectTitle(rset.getString("SP_TITLE"));
				ul.setCount(rset.getInt("VIEW_COUNT"));
				ul.setPuno(rset.getInt("PUNO"));
				list.add(ul);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}
	//자신이 이용한 광고 리스트 목록 ( 기업 )
	public ArrayList<UsingAdverList> usingAdverList(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<UsingAdverList> list = null;
		
		String query = prop.getProperty("usingAdverList");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<UsingAdverList>();
			while(rset.next()) {
				UsingAdverList ul = new UsingAdverList();
				
				ul.setAdstartDate(rset.getDate("B_START"));
				ul.setAdendDate(rset.getDate("B_FINISH"));
				ul.setAdFileName(rset.getString("ORIGIN_NAME"));
				ul.setAdCategory(rset.getString("CATEGORY"));
				ul.setAdPayId(rset.getInt("PAYID"));
				
				list.add(ul);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}
	//자신이 이용한 적극채용공고 리스트 ( 기업 ) 
	public ArrayList<UsingPopRecruit> usingPopRecruitList(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<UsingPopRecruit> list = null;
		
		String query = prop.getProperty("usingPopRecruitList");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			list = new ArrayList<UsingPopRecruit>();
			while(rset.next()) {
				UsingPopRecruit up = new UsingPopRecruit();
				
				up.setPopstartDate(rset.getDate("PR_START"));
				up.setPopendDate(rset.getDate("PR_FINISH"));
				up.setPopTitle(rset.getString("REC_TITLE"));
				up.setCategory(rset.getString("CATEGROY"));
				up.setPayid(rset.getInt("PAYID"));
				
				list.add(up);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}
	//클릭한 사진 경로 받아오기
	public String UsingAdverPicturePath(int payid, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String path = "";
		
		String query = prop.getProperty("PicturePath");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, payid);
			
			rset = pstmt.executeQuery();
			
			/*<%=request.getContextPath()%>/attachment_uploadFiles/<%=at.getChangeName()%>*/
			
			if(rset.next()) {
				path += "/info";
				path += "/attachment_uploadFiles/";
				path += rset.getString("CHANGE_NAME");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return path;
	}

	public ArrayList<Integer> selectPopRecruitDateCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<Integer> list = null;
		
		String query = prop.getProperty("selectPopRecruitDateCount");
		
		try {
			stmt = con.createStatement();
			
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<Integer>();
			while(rset.next()) {
				Integer li = new Integer(rset.getInt("count"));
				
				list.add(li);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		return list;
	}

	//전체 광고배너 startdate , enddate
	public ArrayList<UsingAdverList> selectAdverList(Connection con, int pcode) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<UsingAdverList> list = null;
		
		String query = prop.getProperty("selectAdverList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, pcode);
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()){
				UsingAdverList ual = new UsingAdverList();
				
				ual.setAdstartDate(rset.getDate("B_START"));
				ual.setAdendDate(rset.getDate("B_FINISH"));
				ual.setAdPayId(rset.getInt("PAYID"));
				
				list.add(ual);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}
	
	//환불요청 버튼 클릭시 환불요청 inseret ( category ==4 ) 
		public int insertRefundrequest(int payid, Connection con) {
			PreparedStatement pstmt = null;
			int result = 0;
			
			String query = prop.getProperty("insertRefundrequest");
			
			try {
				pstmt = con.prepareStatement(query);
				
				pstmt.setInt(1, payid);
				
				result = pstmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
			}
			return result;
		}


}
