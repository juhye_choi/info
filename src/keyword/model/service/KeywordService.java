package keyword.model.service;

import java.sql.Connection;
import java.util.ArrayList;

import keyword.model.dao.KeywordDao;
import keyword.model.vo.Keyword;
import keyword.model.vo.PageInfo;

import static common.JDBCTemplate.*;

public class KeywordService {

	public ArrayList<Keyword> selectList() {
		Connection con = getConnection();
		ArrayList<Keyword> klist = null;
		
		klist = new KeywordDao().selectList(con);
		
		close(con);
		
		return klist;
	}

	public int getListCount() {
		Connection con = getConnection();
		int result = 0;
		
		result = new KeywordDao().getListCount(con);
		
		close(con);
		
		return result;
	}

	public ArrayList<Keyword> selectListWithPaging(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<Keyword> klist = new KeywordDao().selectListWithPaging(con, pi);
		
		close(con);
		
		return klist;
	}

	public int insertKeyword(Keyword requestKeyword) {
		Connection con = getConnection();
		
		int result = new KeywordDao().insertKeyword(con, requestKeyword);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public int deleteKeyword(int kcode) {
		Connection con = getConnection();
		
		int result = new KeywordDao().deleteKeyword(con, kcode);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public int updateKeyword(Keyword requestKeyword) {
		Connection con = getConnection();
		
		int result = new KeywordDao().updateKeyword(con, requestKeyword);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public ArrayList<Keyword> searchKeywordCategory(int category, PageInfo pi) {
		Connection con = getConnection();
		ArrayList<Keyword> list = null;
		
		//System.out.println("Service Category : " + category);
		list = new  KeywordDao().searchKeywordCategory(con, category, pi);
		
		close(con);
		
		return list;
	}

	public ArrayList<Keyword> searchKeywordName(String keywordName, PageInfo pi) {
		Connection con = getConnection();
		ArrayList<Keyword> list = null;
		
		list = new KeywordDao().searchKeywordName(con, keywordName, pi);
		
		close(con);
		
		return list;
	}

	public int getListCount(int scategory) {
		Connection con = getConnection();
		int result = 0;
		
		result = new KeywordDao().getListCount(con, scategory);
		
		close(con);
		
		return result;
	}

	public int getListCount(String text) {
		Connection con = getConnection();
		int result = 0;
		
		result = new KeywordDao().getListCount(con, text);
		
		close(con);
		
		return result;
	}

}






