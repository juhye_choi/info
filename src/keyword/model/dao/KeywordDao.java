package keyword.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import keyword.model.vo.Keyword;
import keyword.model.vo.PageInfo;

import static common.JDBCTemplate.*;

public class KeywordDao {
	private Properties prop = new Properties();
	
	public KeywordDao() {
		
		String fileName = KeywordDao.class.getResource("/sql/keyword/keyword-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	

	public ArrayList<Keyword> selectList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<Keyword> klist = null;
		
		String query = prop.getProperty("selectList");
		
		try {
			stmt = con.createStatement();
			
			rset = stmt.executeQuery(query);
			
			klist = new ArrayList<>();
			while(rset.next()) {
				Keyword k = new Keyword();
				k.setKcode(rset.getInt("KCODE"));
				k.setCategory(rset.getString("CATEGORY"));
				k.setKname(rset.getString("KNAME"));
				
				klist.add(k);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return klist;
	}


	public int getListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getListCount");
		
		try {
			stmt = con.createStatement();
			
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
		}
		
		return result;
	}


	public ArrayList<Keyword> selectListWithPaging(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Keyword> klist = null;
		
		String query = prop.getProperty("selectListWithPaging");
		
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			klist = new ArrayList<Keyword>();
			while(rset.next()) {
				Keyword k = new Keyword();
				k.setKcode(rset.getInt("KCODE"));
				k.setCategory(rset.getString("CATEGORY"));
				k.setKname(rset.getString("KNAME"));
				
				klist.add(k);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return klist;
	}


	public int insertKeyword(Connection con, Keyword requestKeyword) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertKeyword");
		
		try {
			pstmt = con.prepareStatement(query);
			int category = Integer.parseInt(requestKeyword.getCategory());
			pstmt.setInt(1, category);
			pstmt.setString(2, requestKeyword.getKname());
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result;
	}


	public int deleteKeyword(Connection con, int kcode) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deleteKeyword");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, kcode);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}


	public int updateKeyword(Connection con, Keyword requestKeyword) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateKeyword");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestKeyword.getKname());
			pstmt.setInt(2, Integer.parseInt(requestKeyword.getCategory()));
			pstmt.setInt(3, requestKeyword.getKcode());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}


	public ArrayList<Keyword> searchKeywordName(Connection con, String keywordName, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Keyword> list = null;
		
		String query = prop.getProperty("searchKeywordName");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, keywordName);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Keyword>();
			while(rset.next()) {
				Keyword k = new Keyword();
				
				k.setKcode(rset.getInt("KCODE"));
				k.setCategory(rset.getString("CATEGORY"));
				k.setKname(rset.getString("KNAME"));
				
				list.add(k);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}


	public ArrayList<Keyword> searchKeywordCategory(Connection con, int category, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Keyword> list = null;
		
		String query = prop.getProperty("searchKeywordCategory");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, category);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Keyword>();
			while(rset.next()) {
				Keyword k = new Keyword();
				
				k.setKcode(rset.getInt("KCODE"));
				k.setCategory(rset.getString("CATEGORY"));
				k.setKname(rset.getString("KNAME"));
				
				list.add(k);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}


	public int getListCount(Connection con, int scategory) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getListCountCategory");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, scategory);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return result;
	}


	public int getListCount(Connection con, String text) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getListCountText");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, text);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return result;
	}

}







