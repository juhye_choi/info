package keyword.model.vo;

public class Keyword2 {
	private int kcode;
	private int category;
	private String kname;
	
	public Keyword2() {}

	public Keyword2(int kcode, int category, String kname) {
		super();
		this.kcode = kcode;
		this.category = category;
		this.kname = kname;
	}

	public int getKcode() {
		return kcode;
	}

	public void setKcode(int kcode) {
		this.kcode = kcode;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getKname() {
		return kname;
	}

	public void setKname(String kname) {
		this.kname = kname;
	}

}