package keyword.model.vo;

import java.io.Serializable;

public class Keyword implements Serializable{
	private int kcode;
	private String category;
	private String kname;
	
	public Keyword () {}

	public Keyword(int kcode, String category, String kname) {
		super();
		this.kcode = kcode;
		this.category = category;
		this.kname = kname;
	}

	public int getKcode() {
		return kcode;
	}

	public void setKcode(int kcode) {
		this.kcode = kcode;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getKname() {
		return kname;
	}

	public void setKname(String kname) {
		this.kname = kname;
	}

	@Override
	public String toString() {
		return "Keyword [kcode=" + kcode + ", category=" + category + ", kname=" + kname + "]";
	}
	
}
