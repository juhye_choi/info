package keyword.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import keyword.model.vo.Keyword;
import keyword.model.vo.PageInfo;
import keyword.model.service.KeywordService;


@WebServlet("/selectListPage.ke")
public class SelectKeywordListWithPaging extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public SelectKeywordListWithPaging() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//페이징 처리
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage=1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		limit = 10;
		
		int listCount = new KeywordService().getListCount();

		maxPage = (int)((double)listCount/limit + 0.9);
		startPage = (((int)((double) currentPage/limit+0.9))-1) * 10 + 1;
		
		endPage = startPage + 10 -1;
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		ArrayList<Keyword> klist = new KeywordService().selectListWithPaging(pi);
		
		String page ="";
		if(klist != null) {
			page = "views/admin/keywordMgt.jsp";
			request.setAttribute("klist", klist);
			request.setAttribute("pi", pi);
		} else {
			page="views/common/errorPgae.jsp";
			request.setAttribute("msg", "키워드 조회에 실패하였습니다.");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
