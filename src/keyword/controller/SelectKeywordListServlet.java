package keyword.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import keyword.model.service.KeywordService;
import keyword.model.vo.Keyword;

@WebServlet("/selectList.ke")
public class SelectKeywordListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectKeywordListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		int type = Integer.parseInt(request.getParameter("type"));
		ArrayList<Keyword> klist = new KeywordService().selectList();
		
		
		String page = "";
		if(klist != null) {
			
			request.setAttribute("klist", klist);
			
			if(type == 1) { // 프로젝트 경력기술서 작성하기 폼
				page = "views/person/projectDetailForm.jsp";
			} else if(type==2){ // 프로젝트 경력기술서 수정하기 폼으로
				String num = request.getParameter("num");
				String parameter = "type=2&num=" + num;
				page = "selectOne.pjt?" + parameter;
			} else if(type==3) {
				page = "views/company/jobPostingRegistration.jsp";
			}
			
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "키워드 불러오기에 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
