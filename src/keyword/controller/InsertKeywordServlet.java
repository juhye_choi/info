package keyword.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import keyword.model.service.KeywordService;
import keyword.model.vo.Keyword;


@WebServlet("/insert.ke")
public class InsertKeywordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InsertKeywordServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		int kcode = 0;
		Keyword requestKeyword = new Keyword();
		if(!request.getParameter("mkcode").equals("")) {
			kcode = Integer.parseInt(request.getParameter("mkcode"));
			requestKeyword.setKcode(kcode);
		}
		String kname = request.getParameter("kname");
		String kcategory = request.getParameter("kcategory");
		//System.out.println(kcode + "," + kname + "," + kcategory);
		
		requestKeyword.setKname(kname);
		requestKeyword.setCategory(kcategory);
		
		int result = 0;
		if(kcode == 0) { //인서트 하는 경우!
			//System.out.println("인서트!");
			result = new KeywordService().insertKeyword(requestKeyword);
		} else { // 업데이트 하는 경우!
			//System.out.println("업데이트!");
			result = new KeywordService().updateKeyword(requestKeyword);
		}
		
		
		String page = "";
		if(result>0) {
			page = "selectListPage.ke";
			response.sendRedirect(page);
			
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "키워드 수정/삽입에 실패했음!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
