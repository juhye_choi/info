package keyword.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import keyword.model.service.KeywordService;
import keyword.model.vo.Keyword;
import keyword.model.vo.PageInfo;

@WebServlet("/search.ke")
public class SearchKeywordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public SearchKeywordServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		int category = Integer.parseInt(request.getParameter("searchCategory"));
		String text = request.getParameter("searchText");
		int scategory = Integer.parseInt(request.getParameter("scategory"));
		
		//페이징 처리
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;

		currentPage=1;

		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		limit = 10;
		int listCount =0;
		if(category ==2) {
			listCount = new KeywordService().getListCount(scategory);
		} else {
			listCount = new KeywordService().getListCount(text);
		}

		maxPage = (int)((double)listCount/limit + 0.9);
		startPage = (((int)((double) currentPage/limit+0.9))-1) * 10 + 1;

		endPage = startPage + 10 -1;
		if(maxPage < endPage) {
			endPage = maxPage;
		}

		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);

		ArrayList<Keyword> list = null;
		if(category == 2) { // 키워드 분류 검색
			list = new KeywordService().searchKeywordCategory(scategory, pi);
		} else { //키워드명 검색
			list = new KeywordService().searchKeywordName(text, pi);
		}
		
		//System.out.println(pi);
		
		JSONObject keywordOjt = null;
		JSONObject piOjt = new JSONObject();
		JSONArray keywordlist = new JSONArray();
		
		if(list != null) {
			for(Keyword k : list) {
				keywordOjt = new JSONObject();
				
				keywordOjt.put("kcode", k.getKcode());
				keywordOjt.put("kname", URLEncoder.encode(k.getKname(), "UTF-8"));
				keywordOjt.put("category", Integer.parseInt(k.getCategory()));
				
				keywordlist.add(keywordOjt);
			}
			
			piOjt.put("keywordlist", keywordlist);
			
			piOjt.put("scurrentPage", pi.getCurrentPage());
			piOjt.put("slistCount", pi.getListCount());
			piOjt.put("slimit", pi.getLimit());
			piOjt.put("smaxPage", pi.getMaxPage());
			piOjt.put("sstartPage", pi.getStartPage());
			piOjt.put("sendPage", pi.getEndPage());
		} 
		//System.out.println(keywordlist);
		
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		//out.print(keywordlist.toJSONString());
		out.print(piOjt.toJSONString());
		
		
		out.flush();
		out.close();
		
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
