package keyword.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import keyword.model.service.KeywordService;


@WebServlet("/delete.ke")
public class DeleteKeywordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public DeleteKeywordServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		int kcode = Integer.parseInt(request.getParameter("num"));
		
		int result = new KeywordService().deleteKeyword(kcode);
		
		String page ="";
		if(result >0) {
			page = "selectListPage.ke";
			response.sendRedirect(page);
		} else {
			page = "veiws/common/errorPage.jsp";
			request.setAttribute("msg", "삭제 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
