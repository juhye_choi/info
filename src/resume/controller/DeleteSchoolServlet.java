package resume.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resume.model.service.ResumeService;


@WebServlet("/delete.sch")
public class DeleteSchoolServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public DeleteSchoolServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		int sid = Integer.parseInt(request.getParameter("num"));
		
		int result = new ResumeService().deleteSchool(sid);
		
		String page = "";
		if(result>0) {
			response.sendRedirect("views/common/successPage.jsp?successCode=2");
			
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "삭제 실패!!!");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
