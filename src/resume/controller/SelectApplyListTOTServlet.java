package resume.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import keyword.model.vo.PageInfo;
import member.model.vo.Member;
import resume.model.service.ResumeService;
import resume.model.vo.ApplySituation;

@WebServlet("/selectListTOT.papp")
public class SelectApplyListTOTServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectApplyListTOTServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int uno = 0;
		if(request.getSession().getAttribute("loginUser") == null) {
			request.setAttribute("msg", "로그인이 필요합니다.");
			request.getRequestDispatcher("views/common/needLogin.jsp").forward(request, response);
		} else {
			uno = ((Member) request.getSession().getAttribute("loginUser")).getUno();

			//페이징 처리
			int currentPage;
			int limit;
			int maxPage;
			int startPage;
			int endPage;

			currentPage=1;

			if(request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}
			
			limit = 10;
			
			String category = request.getParameter("category");
			int listCount = 0;
			if(category.equals("total")) { //전체보기일 때,
				listCount = new ResumeService().getApplyListCountTOT(uno);				
			} else { //종료된 공고 보기일 때,
				listCount = new ResumeService().getApplyListCountEND(uno);
			}

			maxPage = (int)((double)listCount/limit + 0.9);
			startPage = (((int)((double) currentPage/limit+0.9))-1) * 10 + 1;

			endPage = startPage + 10 -1;
			if(maxPage < endPage) {
				endPage = maxPage;
			}
			
			PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);

			
			ArrayList<ApplySituation> aslist = new ArrayList<>();
			
			if(category.equals("total")) { //전체보기일 때,
				aslist = new ResumeService().selectApplyLogListTOT(uno, pi);
			} else { //종료된 공고 보기일 때,
				aslist = new ResumeService().selectApplyLogListEND(uno, pi);
			}
			
			JSONObject applyOjt = null;
			JSONObject outerOjt = new JSONObject();
			JSONArray applyList = new JSONArray();
			
			if(aslist != null) {
				for(ApplySituation as : aslist) {
					applyOjt = new JSONObject();
					
					applyOjt.put("applyId", as.getApplyId());
					applyOjt.put("recId", as.getRecId());
					applyOjt.put("recTitle", URLEncoder.encode(as.getRecTitle(), "UTF-8"));
					applyOjt.put("applyDate", as.getApplyDate().toString());
					applyOjt.put("status", as.getStatus());
					applyOjt.put("recStart", as.getRecStart().toString());
					applyOjt.put("recFinish", as.getRecFinish().toString());
					
					applyList.add(applyOjt);
				}
				
				outerOjt.put("applyList", applyList);
				
				outerOjt.put("scurrentPage", pi.getCurrentPage());
				outerOjt.put("slistCount", pi.getListCount());
				outerOjt.put("slimit", pi.getLimit());
				outerOjt.put("smaxPage", pi.getMaxPage());
				outerOjt.put("sstartPage", pi.getStartPage());
				outerOjt.put("sendPage", pi.getEndPage());

			}
			
			response.setContentType("application/json");
			PrintWriter out = response.getWriter();
			out.print(outerOjt.toJSONString());
			
			out.flush();
			out.close();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
