package resume.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resume.model.service.ResumeService;

@WebServlet("/insertSeen.capp")
public class InsertSeenResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
    public InsertSeenResumeServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		//Insert아니고 Update!!!
		
		int appid = Integer.parseInt(request.getParameter("num"));
		System.out.println("appid" + appid);
		
		
		int result = new ResumeService().updateSeenResume(appid);
		
		if(result>0) {
			
			response.setContentType("application/json");
			PrintWriter out = response.getWriter();
			out.print(result);
			
			out.flush();
			out.close();
			
			
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
