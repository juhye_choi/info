package resume.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resume.model.service.ResumeService;

@WebServlet("/delete.pjt")
public class DeleteProjectCareerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DeleteProjectCareerServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int pjtId = Integer.parseInt(request.getParameter("num"));
		
		int result = new ResumeService().deleteProjectCareer(pjtId);
		
		String page ="";
		
		
		if(result>0) {
			response.sendRedirect(request.getContextPath() + "/selectOne.re");
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "삭제에 실패하였습니다.");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
