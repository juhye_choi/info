package resume.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resume.model.service.ResumeService;

@WebServlet("/update.pbl")
public class UpdatePBlackListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UpdatePBlackListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int pblHisId = Integer.parseInt(request.getParameter("pblId"));
		String reason = request.getParameter("blackReason");
		
		//System.out.println("pblHisId : " + pblHisId);
		//System.out.println("reason : " + reason);
		
		int result = new ResumeService().updatePBlackList(pblHisId, reason);
		
		String page = "";
		if(result > 0 ) {
			page = "selectList.pbl";
			response.sendRedirect(page);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "블랙리스트 업데이트 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
