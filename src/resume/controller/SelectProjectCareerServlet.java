package resume.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import resume.model.service.ResumeService;
import resume.model.vo.ProjectCareer;


@WebServlet("/selectOne.pjt")
public class SelectProjectCareerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public SelectProjectCareerServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String page = "";
		int pjtId = Integer.parseInt(request.getParameter("num"));
		int type = Integer.parseInt(request.getParameter("type"));
		int uno = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			uno = ((Member) request.getSession().getAttribute("loginUser")).getUno();			
		} else {
			page="views/common/needLogin.jsp";
			request.setAttribute("msg", "로그인이 필요합니다.");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
		//System.out.println("pjtId : " +pjtId);
		//System.out.println("type : " + type);
		//본인꺼인지 체크 
		int result = new ResumeService().checkProject(uno, pjtId);
		
		if(((Member) request.getSession().getAttribute("loginUser")).getCategory().equals("A")) { //관리자이면 1을 반환해서 볼수있게!
			result = 1;
		}
		
		if(result>0) {
			ProjectCareer pc = new ResumeService().selectProjectCareer(pjtId);
			
			
			long diff = pc.getPjtFinish().getTime() - pc.getPjtStart().getTime();
		      int diffDays = (int) (diff/(24*60*60*1000));
		      
		      
			if(type == 1) { //프로젝트 경력기술서 보기 (본인)
				page = "views/person/projectDetail.jsp";
			} else if(type == 2){ //프로젝트 경력기술서 수정하기 (본인)
				page= "views/person/projectDetailWrite.jsp";
			} else if(type == 3) { // 관리자가 회원의 경력기술서 보기
				page= "views/admin/memberPjtCareer.jsp";
			}
			request.setAttribute("pc", pc);
		    request.setAttribute("diffDays", diffDays);
			
		} else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "조회할 권한이 없습니다.");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
