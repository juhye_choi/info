package resume.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import resume.model.service.ResumeService;
import resume.model.vo.Education;
import resume.model.vo.School;

@WebServlet("/update.edu")
public class UpdateEducationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UpdateEducationServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		int eid = 0;
		int result = 0;
		if(request.getParameter("eid") != "") {
			eid = Integer.parseInt(request.getParameter("eid"));
		}
		
		String title = request.getParameter("title");
		String date1 = request.getParameter("date1");
		String content = request.getParameter("content");
		
		Education requestEdu = new Education();
		requestEdu.seteName(title);
		requestEdu.setEuContent(content);
		requestEdu.setUno(((Member) request.getSession().getAttribute("loginUser")).getUno());
		
		if(date1 != "") {
			java.sql.Date startDate = java.sql.Date.valueOf(date1);
			requestEdu.setEuStartDate(startDate);
		}
		
		if( request.getParameter("date2") != "") {
			String date2 = request.getParameter("date2");

			java.sql.Date finishDate = java.sql.Date.valueOf(date2);
			requestEdu.setEuFinishDate(finishDate);
		}
		
		if(eid == 0) { //insert하는 경우!!
			//System.out.println("인서트로직 발동!");
			result = new ResumeService().insertEducation(requestEdu);
			
		} else { //update하는 경우!!
			//System.out.println("수정로직 발동!");
			requestEdu.setEid(eid);
			result = new ResumeService().updateEducation(requestEdu);
		}
		
		String page = "";
		if(result>0) {
			response.sendRedirect(request.getContextPath() + "/selectOne.re");
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "경력 추가/수정에 실패하셨습니다.");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
