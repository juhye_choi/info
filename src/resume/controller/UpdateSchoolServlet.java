package resume.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import resume.model.service.ResumeService;
import resume.model.vo.Career;
import resume.model.vo.School;

@WebServlet("/update.sch")
public class UpdateSchoolServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public UpdateSchoolServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		int sid = 0;
		int result = 0;
		if(request.getParameter("sid") != "") {
			sid = Integer.parseInt(request.getParameter("sid"));
		}
		
		String title = request.getParameter("title");
		String date1 = request.getParameter("date1");
		String major = request.getParameter("major");
		//System.out.println(carid + " : " + title + date1 + date2 + content);
		School requestSchool = new School();
		requestSchool.setsName(title);
		requestSchool.setMajor(major);
		requestSchool.setUno(((Member) request.getSession().getAttribute("loginUser")).getUno());
		
		if(date1 != "") {
			java.sql.Date startDate = java.sql.Date.valueOf(date1);
			requestSchool.setEnterDate(startDate);
		}
		
		if( request.getParameter("date2") != "") {
			String date2 = request.getParameter("date2");

			java.sql.Date finishDate = java.sql.Date.valueOf(date2);
			requestSchool.setGraduDate(finishDate);
		}
		
		if(sid == 0) { //insert하는 경우!!
			//System.out.println("인서트로직 발동!");
			result = new ResumeService().insertSchool(requestSchool);
			
		} else { //update하는 경우!!
			//System.out.println("수정로직 발동!");
			requestSchool.setSid(sid);
			result = new ResumeService().updateSchool(requestSchool);
		}
		
		String page = "";
		if(result>0) {
			response.sendRedirect(request.getContextPath() + "/selectOne.re");
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "경력 추가/수정에 실패하셨습니다.");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
