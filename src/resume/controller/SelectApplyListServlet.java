package resume.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import keyword.model.vo.PageInfo;
import member.model.vo.Member;
import resume.model.service.ResumeService;
import resume.model.vo.ApplySituation;

@WebServlet("/selectList.papp")
public class SelectApplyListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
    public SelectApplyListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		int uno = 0;
		if(request.getSession().getAttribute("loginUser") == null) {
			request.setAttribute("msg", "로그인이 필요합니다.");
			request.getRequestDispatcher("views/common/needLogin.jsp").forward(request, response);
		} else {
			uno = ((Member) request.getSession().getAttribute("loginUser")).getUno();

			//페이징 처리
			int currentPage;
			int limit;
			int maxPage;
			int startPage;
			int endPage;

			currentPage=1;

			if(request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}

			limit = 10;

			int listCount = new ResumeService().getApplyListCount(uno);

			maxPage = (int)((double)listCount/limit + 0.9);
			startPage = (((int)((double) currentPage/limit+0.9))-1) * 10 + 1;

			endPage = startPage + 10 -1;
			if(maxPage < endPage) {
				endPage = maxPage;
			}

			PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);


			ArrayList<ApplySituation> aslist = new ResumeService().selectApplyLogList(uno, pi);

			String page = "";
			if(aslist != null) {
				page = "views/person/applyLog.jsp";
				request.setAttribute("aslist", aslist);
				request.setAttribute("pi", pi);
				request.getRequestDispatcher(page).forward(request, response);
			} else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "지원현황 조회에 실패하셨습니다.");
				request.getRequestDispatcher(page).forward(request, response);
			}	
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
