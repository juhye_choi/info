package resume.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resume.model.service.ResumeService;


@WebServlet("/update.in")
public class UpdateIntroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public UpdateIntroServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String intro = request.getParameter("intro");
		int uno = Integer.parseInt(request.getParameter("uno"));
		//System.out.println(intro);
		
		int result = new ResumeService().updateIntro(uno, intro);
		
		String page="";
		if(result>0) {
			page = "selectOne.re";
			response.sendRedirect(page);
		} else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "자기소개 수정 실패!!!!!!");
			request.getRequestDispatcher(page).forward(request,response);
		}
		
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
