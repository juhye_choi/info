package resume.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import resume.model.service.ResumeService;
import resume.model.vo.MorePageInfo;

@WebServlet("/selectList.seenr")
public class SelectSeenRecruitListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectSeenRecruitListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		int uno = 0;
		if(loginUser == null) {
			request.setAttribute("msg", "로그인이 필요한 메뉴입니다");
			request.getRequestDispatcher("views/common/needLogin.jsp").forward(request, response);
		} else {
			uno = loginUser.getUno();
			
			//페이징 처리
			int currentPage; //현재 페이지
			int limit; //한페이지에 몇개 보여줄지
			int maxPage; //최종(마지막) 페이지

			currentPage=1;

			if(request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}

			limit = 10;

			int listCount = new ResumeService().getSeenRecruitListCount(uno);
			maxPage = (int)((double)listCount/limit + 0.9);

			MorePageInfo mpi = new MorePageInfo(currentPage, listCount, limit, maxPage);
			
			
			ArrayList<HashMap<String, String>> list = new ResumeService().selectSeenRecruitList(uno, mpi);
			
			String page = "";
			if(list != null) {
				page = "views/person/watchingLog.jsp";
				request.setAttribute("list", list);
				request.setAttribute("mpi", mpi);
				request.getRequestDispatcher(page).forward(request, response);
			} else {
				page ="views/common/errorPage.jsp";
				request.setAttribute("msg", "내가본공고 조회 실패!");
			}
			
			
			
			
			
		}
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
