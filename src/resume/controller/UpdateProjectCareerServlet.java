package resume.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.oreilly.servlet.MultipartRequest;

import common.MyFileRenamePolicy;
import keyword.model.vo.Keyword;
import member.model.vo.Member;
import resume.model.service.ResumeService;
import resume.model.vo.Attachment;
import resume.model.vo.ProjectCareer;


@WebServlet("/update.pjt")
public class UpdateProjectCareerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public UpdateProjectCareerServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		//System.out.println("HELLO~");
		if(ServletFileUpload.isMultipartContent(request)) {
			int maxSize = 1024*1024 * 100; //10Mbyte로 용량제한
			
			
			String root = request.getSession().getServletContext().getRealPath("/");
			
			String savePath = root + "attachment_uploadFiles/";
			
			MultipartRequest multiRequest = new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			//변환된 파일의 이름을 저장할 arrayList
			ArrayList<String> saveFiles = new ArrayList<>();
			
			//원본파일의 이름 저장할 arrayList
			ArrayList<String> originFiles = new ArrayList<>();
			
			//파일이 전송된 input태그의 name을 반환
			Enumeration<String> files = multiRequest.getFileNames();
			
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
			}
			for(int i=0; i<originFiles.size(); i++) {
				System.out.println(originFiles.get(i));
			}
			int count = Integer.parseInt(multiRequest.getParameter("count"));
			
			//System.out.println("파일개수 : " + originFiles.size());
			//System.out.println("사진개수 : " + count);
			//System.out.println("프로젝트 이름 : " + multiRequest.getParameter("pjtName"));
			

			String pjtName = multiRequest.getParameter("pjtName");
			String myRole = multiRequest.getParameter("myRole");
			int pjtId = Integer.parseInt(multiRequest.getParameter("pjtId"));
			
			String pstartDate = null;
			Date pjtStart = null;
			if(!multiRequest.getParameter("pstartDate").equals("")) {
				pstartDate = multiRequest.getParameter("pstartDate");
				pjtStart = java.sql.Date.valueOf(pstartDate);
			}
			
			String pendDate = null;
			Date pjtFinish = null;
			if(!multiRequest.getParameter("pendDate").equals("")) {
				pendDate = multiRequest.getParameter("pendDate");
				pjtFinish = java.sql.Date.valueOf(pendDate);
			}
			
			String pjtContent = multiRequest.getParameter("pContent");
			String pjtLink = multiRequest.getParameter("relatedLink");
			
			
			//키워드를 가져옴
			ArrayList<Keyword> klist = new ArrayList<>();
			String[] karr = multiRequest.getParameterValues("keywords");
			
			for(int i=0; i<karr.length; i++) {
			if(!karr[i].equals("")) {
					Keyword k = new Keyword();
					k.setKcode(Integer.parseInt(karr[i]));
					
					klist.add(k);
				}
			}
			//System.out.println(klist);
			
			//원본첨부파일 목록을 불러옴!
			HashMap<String, Object> hmap = new ResumeService().selectAttachment(pjtId);
			ArrayList<Attachment> originPhoto = (ArrayList<Attachment>) hmap.get("originPhoto");
			Attachment originFile = (Attachment) hmap.get("originFile");
			
			//사진 list 담는 ArrayList
			ArrayList<Attachment> atlist = new ArrayList<>();
			Attachment atFile = null;
			if(originFiles.size() == 4) { //첨부파일이 없을 때,
				for(int i=3; i>=0; i--) {
					Attachment atPhoto = new Attachment();
					if(originFiles.get(i) != null) { //원본파일이 변경되었을 때,
						atPhoto.setFilePath(savePath);
						atPhoto.setOriginName(originFiles.get(i));
						atPhoto.setChangeName(saveFiles.get(i));
						atPhoto.setPhoPjtId(pjtId);
						if(i==3) {							
							atPhoto.setFileLevel(0);
						} else {
							atPhoto.setFileLevel(1);
						}
						
						atlist.add(atPhoto);
						
						File failedFile = new File(savePath + originPhoto.get(3-i).getChangeName());
						failedFile.delete(); //원본파일 삭제!
						
					} else { //원본파일이 변경되지 않았을 때,
						try {
							atlist.add(originPhoto.get(3-i));										
						} catch(IndexOutOfBoundsException e) {
							System.out.println("원본파일이 모자랍니다!");
						}
					}
				}
				if(originFile != null) {
					atFile = originFile;
					File failedFile = new File(savePath + originFile.getChangeName());
					failedFile.delete(); //원본파일 삭제!
				}
				
			} else { //첨부파일이 있을 때,
				//첨부파일을 담는 용도의 Attachment객체
				atFile = new Attachment();
				atFile.setFilePath(savePath);
				atFile.setOriginName(originFiles.get(0));
				atFile.setChangeName(saveFiles.get(0));
				atFile.setAttPjtId(pjtId);
				
				for(int i=4; i>=1; i--) {
					Attachment atPhoto = new Attachment();
					if(originFiles.get(i) != null) { //원본파일이 변경되었을 때,
						atPhoto.setFilePath(savePath);
						atPhoto.setOriginName(originFiles.get(i));
						atPhoto.setChangeName(saveFiles.get(i));
						atPhoto.setPhoPjtId(pjtId);
						if(i==4) {							
							atPhoto.setFileLevel(0);
						} else {
							atPhoto.setFileLevel(1);
						}
						
						atlist.add(atPhoto);
						
						File failedFile = new File(savePath + originPhoto.get(4-i).getChangeName());
						failedFile.delete(); //원본파일 삭제!
						
					} else { //원본파일이 변경되지 않았을 때,
						try {
							atlist.add(originPhoto.get(4-i));										
						} catch(IndexOutOfBoundsException e) {
							System.out.println("원본파일이 모자랍니다!");
						}
					}
				}
			}
			
			//System.out.println(originPhoto);
			//System.out.println(atlist);
			//System.out.println(atFile);
			
			//ProjectCareer객체 생성
			ProjectCareer pc = new ProjectCareer();
			pc.setUno(((Member)request.getSession().getAttribute("loginUser")).getUno());
			pc.setPjtId(pjtId);
			pc.setPjtName(pjtName);
			pc.setMyRole(myRole);
			pc.setPjtStart(pjtStart);
			pc.setPjtFinish(pjtFinish);
			pc.setPjtContent(pjtContent);
			pc.setPjtLink(pjtLink);
			pc.setAtlist(atlist);
			pc.setAttachmentFile(atFile);
			pc.setKlist(klist);
			
			int result = new ResumeService().updateProjectCareer(pc);
			
			String page = "";
			if(result>0) {
				page = "selectOne.pjt?type=1&num=" +pjtId;
				response.sendRedirect(page);
				
			} else {
				//실패했을 때, 파일 삭제
				for(int i=0; i<saveFiles.size(); i++) {
					if(saveFiles.get(i)!=null) {
						File failedFile = new File(savePath + saveFiles.get(i));
						failedFile.delete();
					}
					
				}
				page="views/common/errorPage.jsp";
				request.setAttribute("mgs", "업데이트에 실패!");
				request.getRequestDispatcher(page).forward(request, response);
			}
			
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
