package resume.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.oreilly.servlet.MultipartRequest;

import common.MyFileRenamePolicy;
import keyword.model.dao.KeywordDao;
import member.model.vo.Member;
import resume.model.service.ResumeService;
import resume.model.vo.Attachment;
import keyword.model.vo.Keyword;
import resume.model.vo.ProjectCareer;

@WebServlet("/insert.pjt")
public class InsertProjectCareerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InsertProjectCareerServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		//멀티파트폼데이터로 넘어오면 true, 아니면 false
		if(ServletFileUpload.isMultipartContent(request)) {
			int maxSize = 1024*1024 * 100; //10Mbyte로 용량제한
			
			//System.out.println("잘 넘어옴!");
			
			String root = request.getSession().getServletContext().getRealPath("/");
			//System.out.println(root);
			
			String savePath = root + "attachment_uploadFiles/";
			
			MultipartRequest multiRequest = new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			//변환된 파일의 이름을 저장할 arrayList
			ArrayList<String> saveFiles = new ArrayList<>();
			
			//원본파일의 이름 저장할 arrayList
			ArrayList<String> originFiles = new ArrayList<>();
			
			//파일이 전송된 input태그의 name을 반환
			Enumeration<String> files = multiRequest.getFileNames();
			
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
			}
			//System.out.println("saveFiles : " + saveFiles);
			//System.out.println("originFiles : " + originFiles);
			
			String pjtName = multiRequest.getParameter("pjtName");
			String myRole = multiRequest.getParameter("myRole");
			
			String pstartDate = null;
			Date pjtStart = null;
			if(!multiRequest.getParameter("pstartDate").equals("")) {
				pstartDate = multiRequest.getParameter("pstartDate");
				pjtStart = java.sql.Date.valueOf(pstartDate);
			}
			
			String pendDate = null;
			Date pjtFinish = null;
			if(!multiRequest.getParameter("pendDate").equals("")) {
				pendDate = multiRequest.getParameter("pendDate");
				pjtFinish = java.sql.Date.valueOf(pendDate);
			}
			
			String pjtContent = multiRequest.getParameter("pContent");
			String pjtLink = multiRequest.getParameter("relatedLink");
			
			int count = Integer.parseInt(multiRequest.getParameter("hiddenValue"));
			
			//System.out.println("사진개수 : " + count);
			//System.out.println("총 파일개수 : " + originFiles.size());
			
			//키워드를 가져옴
			ArrayList<Keyword> klist = new ArrayList<>();
			String[] karr = multiRequest.getParameterValues("keywords");
			
			for(int i=0; i<karr.length; i++) {
			if(!karr[i].equals("")) {
					Keyword k = new Keyword();
					k.setKcode(Integer.parseInt(karr[i]));
					
					klist.add(k);
				}
			}
			//Attachment 객체 생성 : 첨부한 사진을 담는 용도!
			ArrayList<Attachment> atlist = new ArrayList<>();
			Attachment attachmentFile = null;
			//System.out.println(originFiles.size());
			if(originFiles.size() == count) {	// 사진만 있는 경우,
				System.out.println("사진만 있음!");
				for(int i=originFiles.size()-1; i>=0; i--) {
					Attachment at = new Attachment();
					at.setFilePath(savePath);
					at.setOriginName(originFiles.get(i));
					at.setChangeName(saveFiles.get(i));

					if(i==originFiles.size()-1) {
						at.setFileLevel(0);
					} else {
						at.setFileLevel(1);
					}
					atlist.add(at);
				}
			} else { //첨부파일과 사진이 있는 경우,
				System.out.println("첨부도 있음!");
				for(int i=originFiles.size()-1; i>=1; i--) {
					Attachment at = new Attachment();
					at.setFilePath(savePath);
					at.setOriginName(originFiles.get(i));
					at.setChangeName(saveFiles.get(i));
					
					if(i==originFiles.size()-1) {
						at.setFileLevel(0);
					} else {
						at.setFileLevel(1);
					}
					
					atlist.add(at);
				}
				//첨부파일을 담는 용도의 Attachment객체
				attachmentFile = new Attachment();
				attachmentFile.setFilePath(savePath);
				attachmentFile.setOriginName(originFiles.get(0));
				attachmentFile.setChangeName(saveFiles.get(0));
			}

			//ProjectCareer객체 생성
			ProjectCareer pc = new ProjectCareer();
			pc.setUno(((Member)request.getSession().getAttribute("loginUser")).getUno());
			pc.setPjtName(pjtName);
			pc.setMyRole(myRole);
			pc.setPjtStart(pjtStart);
			pc.setPjtFinish(pjtFinish);
			pc.setPjtContent(pjtContent);
			pc.setPjtLink(pjtLink);
			pc.setAtlist(atlist);
			pc.setAttachmentFile(attachmentFile);
			pc.setKlist(klist);
			
			int result = new ResumeService().insertProjectCareer(pc);
			
			if(result>0) {
				response.sendRedirect(request.getContextPath() + "/selectOne.re");
			} else {
				//실패했을 때, 파일 삭제
				for(int i=0; i<saveFiles.size(); i++) {
					File failedFile = new File(savePath + saveFiles.get(i));
					
					failedFile.delete();
				}
				
				request.setAttribute("msg", "프로젝트 등록 실패하였습니다.");
				request.getRequestDispatcher("views/common/errorPage.jsp?errorCode=1").forward(request, response);
			}
			
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}









