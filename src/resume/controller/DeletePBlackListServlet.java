package resume.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resume.model.service.ResumeService;

@WebServlet("/delete.pbl")
public class DeletePBlackListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public DeletePBlackListServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int pblId = Integer.parseInt(request.getParameter("num"));
		
		System.out.println(pblId);
		
		int result = new ResumeService().deletePBlackList(pblId);
		
		String page = "";
		if(result > 0) {
			page = "selectList.pbl";
			response.sendRedirect(page);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "블랙리스트 업데이트 실패!");
			request.getRequestDispatcher(page).forward(request, response);
			
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
