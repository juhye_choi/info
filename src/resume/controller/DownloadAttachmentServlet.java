package resume.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resume.model.service.ResumeService;
import resume.model.vo.Attachment;


@WebServlet("/download.at")
public class DownloadAttachmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public DownloadAttachmentServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		int fileId = Integer.parseInt(request.getParameter("num"));
		
		System.out.println(fileId);
		
		Attachment file = new ResumeService().selectAttachFile(fileId);
		
		System.out.println(file);
		//폴더에서 파일을 읽을 스트림을 생성
		BufferedInputStream buf = null;

		//클라이언트로 내보낼 스트림 생성
		ServletOutputStream downOut = null;

		downOut = response.getOutputStream();

		//스트림으로 전송할 파일 객체 생성
		File downFile = new File(file.getFilePath() + file.getChangeName());

		response.setContentType("text/plain; charset=UTF-8");

		//한글 파일명에 대한 인코딩 처리
		response.setHeader("Content-Disposition", "attachment; filename=\"" + new String(file.getOriginName().getBytes("UTF-8"), "ISO-8859-1") + "\"");

		//전송하려는 파일의 길이 지정
		response.setContentLength((int) downFile.length());
		//어디서부터 어디까지 데이터를 합쳐서 줄지 알수있음!

		FileInputStream fin = new FileInputStream(downFile);
		buf = new BufferedInputStream(fin);

		int readBytes = 0;

		while((readBytes = buf.read()) != -1) { //파일의 끝을 만났을 때 반환하는 수 -1 (EOF)
			downOut.write(readBytes);
		}

		downOut.close();
		buf.close();
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
