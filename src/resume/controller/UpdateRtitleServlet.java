package resume.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resume.model.service.ResumeService;

@WebServlet("/updateRtitle.re")
public class UpdateRtitleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UpdateRtitleServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		int uno = Integer.parseInt(request.getParameter("uno"));
		String rtitle = request.getParameter("titleInput");
		
		int result = new ResumeService().updateRtitle(uno, rtitle);
		
		
		String page = "";
		if(result > 0) {
			page = "selectOne.re";
			response.sendRedirect(page);
			
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "타이틀 입력에 실패했습니다.");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
