package resume.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import resume.model.service.ResumeService;
import resume.model.vo.Career;


@WebServlet("/update.ca")
public class UpdateCareerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public UpdateCareerServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		//System.out.println("경력 업데이트/추가 서블릿!");
		
		int carid = 0;
		int result = 0;
		if(request.getParameter("carid") != null) {
			carid = Integer.parseInt(request.getParameter("carid"));
		}
		
		String title = request.getParameter("title");
		String date1 = request.getParameter("date1");
		String content = request.getParameter("content");
		//System.out.println(carid + " : " + title + date1 + date2 + content);
		Career requestCareer = new Career();
		requestCareer.setCarName(title);
		requestCareer.setUno(((Member)request.getSession().getAttribute("loginUser")).getUno());
		requestCareer.setCarContent(content);
		
		if(date1 != "") {
			java.sql.Date startDate = java.sql.Date.valueOf(date1);
			requestCareer.setCarStartDate(startDate);
		}
		
		if( request.getParameter("date2") != "") {
			String date2 = request.getParameter("date2");

			java.sql.Date finishDate = java.sql.Date.valueOf(date2);
			requestCareer.setCarFinishDate(finishDate);
		}
		
		if(carid == 0) { //insert하는 경우!!
			//System.out.println("인서트로직 발동!");
			result = new ResumeService().insertCareer(requestCareer);
			
		} else { //update하는 경우!!
			//System.out.println("수정로직 발동!");
			requestCareer.setCarId(carid);
			result = new ResumeService().updateCareer(requestCareer);
		}
		
		String page = "";
		if(result>0) {
			response.sendRedirect(request.getContextPath() + "/selectOne.re");
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "경력 추가/수정에 실패하셨습니다.");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
