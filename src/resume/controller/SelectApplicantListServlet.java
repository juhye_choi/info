package resume.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import company.model.vo.Recruit;
import person.model.service.RecruitInfoService;
import resume.model.service.ResumeService;
import resume.model.vo.ApplySituation;

@WebServlet("/selectList.capp")
public class SelectApplicantListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectApplicantListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		int recid = Integer.parseInt(request.getParameter("recid"));
		Recruit recruit = new RecruitInfoService().selectOneRecruit(recid);
		ArrayList<ApplySituation> aslist = new ResumeService().selectApplicantList(recid);
		
		JSONObject outerObj = new JSONObject();
		JSONArray applyList = new JSONArray();
		JSONObject recObj = null;
		
		if(aslist != null) {
			for(ApplySituation as : aslist) {
				recObj = new JSONObject();
				
				recObj.put("applyId", as.getApplyId());
				recObj.put("uno", as.getUno());
				recObj.put("name", URLEncoder.encode(as.getName(), "UTF-8"));
				recObj.put("recid", as.getRecId());
				recObj.put("applyDate", as.getApplyDate().toString());
				recObj.put("status", as.getStatus());
				if(as.getpBirth() != null) {
					recObj.put("pbirth", as.getpBirth().toString());
				} else {
					recObj.put("pbirth", "0000-00-00");
				}
				if(as.getAttachment().getFilePath() != null) {
					recObj.put("atId", as.getAttachment().getAtId());
					recObj.put("filePath", as.getAttachment().getFilePath());
					recObj.put("originName", URLEncoder.encode(as.getAttachment().getOriginName(),"UTF-8"));
					recObj.put("changeName", as.getAttachment().getChangeName());
				}
				
				applyList.add(recObj);
			}
			
			outerObj.put("applyList", applyList);
			outerObj.put("recTitle", URLEncoder.encode(recruit.getRec_title(), "UTF-8"));
			outerObj.put("recStart", recruit.getRec_start().toString());
			outerObj.put("recFinish", recruit.getRec_finish().toString());
			outerObj.put("recApplicant", recruit.getApplicant());
		}

		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.print(outerObj.toJSONString());
		
		out.flush();
		out.close();
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
