package resume.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resume.model.service.ResumeService;


@WebServlet("/delete.ca")
public class DeleteCareerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DeleteCareerServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		//System.out.println("삭제하기 서블릿!!");
		
		
		int carid = Integer.parseInt(request.getParameter("num"));
		
		int result = new ResumeService().deleteCareer(carid);
		
		String page = "";
		if(result>0) {
			response.sendRedirect("views/common/successPage.jsp?successCode=1");
			
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "삭제 실패!!!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
