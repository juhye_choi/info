package resume.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import person.model.service.PersonService;
import person.model.vo.Person;
import resume.model.service.ResumeService;
import resume.model.vo.Resume;


@WebServlet("/selectOne.re")
public class SelectResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectResumeServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//본인 이력서 보는 서블릿
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		
		
		int uno = 0;
		if(loginUser == null) {
			request.setAttribute("msg", "로그인이 필요한 메뉴입니다");
			request.getRequestDispatcher("views/common/needLogin.jsp").forward(request, response);
		} else {
			uno = loginUser.getUno();
			
			int type=0;
			//System.out.println(request.getParameter("type"));
			if(request.getParameter("type") != null) {
				type = Integer.parseInt(request.getParameter("type"));
			}
		
			Resume resume = null;
			
			//System.out.println("uno : " + uno);
			
			resume = new ResumeService().selectResume(uno);
			
			String page = "";
			if(resume != null) { 
				
				request.setAttribute("resume", resume);
				
				if(type ==1) {
					Person person = new PersonService().selectPersonInfo(uno);
					ArrayList<HashMap<String, String>> pdfklist = new ResumeService().selectpdfKeyword(uno);
					request.setAttribute("person", person);
					request.setAttribute("pdfklist", pdfklist);
					page = "views/person/resumeExport.jsp";
					request.getRequestDispatcher(page).forward(request, response);
					
				} else {
					page = "views/person/resume.jsp";
					request.getRequestDispatcher(page).forward(request, response);
				}
				
			} else { //이력서가 존재하지 않을때, 인서트함!
				
				int result = new ResumeService().insertResume(uno);
				
				if(result>0) {
					request.getRequestDispatcher("/selectOne.re").forward(request,response);
					
				} else {
					page = "views/common/errorPage.jsp";
					request.setAttribute("msg", "이력서 조회에 실패하였습니다.");
					request.getRequestDispatcher(page).forward(request, response);
				}	
			}
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
