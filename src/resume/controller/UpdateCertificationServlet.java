package resume.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import resume.model.service.ResumeService;
import resume.model.vo.Certification;
import resume.model.vo.Education;

@WebServlet("/update.cer")
public class UpdateCertificationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UpdateCertificationServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		int cerid = 0;
		int result = 0;
		if(request.getParameter("cerid") != "") {
			cerid = Integer.parseInt(request.getParameter("cerid"));
		}
		
		String cerName = request.getParameter("cerName");
		String date1 = request.getParameter("date1");
		
		Certification requestCer = new Certification();
		requestCer.setUno(((Member) request.getSession().getAttribute("loginUser")).getUno());
		requestCer.setCerName(cerName);
		
		if(date1 != "") {
			java.sql.Date startDate = java.sql.Date.valueOf(date1);
			requestCer.setCertifyDate(startDate);
		}
		
		
		if(cerid == 0) { //insert하는 경우!!
			//System.out.println("인서트로직 발동!");
			result = new ResumeService().insertCertification(requestCer);
			
		} else { //update하는 경우!!
			//System.out.println("수정로직 발동!");
			requestCer.setcerId(cerid);
			result = new ResumeService().updateCertification(requestCer);
		}
		
		String page = "";
		if(result>0) {
			response.sendRedirect(request.getContextPath() + "/selectOne.re");
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "경력 추가/수정에 실패하셨습니다.");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
