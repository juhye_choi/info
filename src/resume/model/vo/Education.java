package resume.model.vo;

import java.sql.Date;

public class Education implements java.io.Serializable{
	private int eid;
	private int uno;
	private String eName;
	private Date euStartDate;
	private Date euFinishDate;
	private String euContent;
	
	public Education() {}

	public Education(int eid, int uno, String eName, Date euStartDate, Date euFinishDate, String euContent) {
		super();
		this.eid = eid;
		this.uno = uno;
		this.eName = eName;
		this.euStartDate = euStartDate;
		this.euFinishDate = euFinishDate;
		this.euContent = euContent;
	}

	public int getEid() {
		return eid;
	}

	public void setEid(int eid) {
		this.eid = eid;
	}

	public int getUno() {
		return uno;
	}

	public void setUno(int uno) {
		this.uno = uno;
	}

	public String geteName() {
		return eName;
	}

	public void seteName(String eName) {
		this.eName = eName;
	}

	public Date getEuStartDate() {
		return euStartDate;
	}

	public void setEuStartDate(Date euStartDate) {
		this.euStartDate = euStartDate;
	}

	public Date getEuFinishDate() {
		return euFinishDate;
	}

	public void setEuFinishDate(Date euFinishDate) {
		this.euFinishDate = euFinishDate;
	}

	public String getEuContent() {
		return euContent;
	}

	public void setEuContent(String euContent) {
		this.euContent = euContent;
	}

	@Override
	public String toString() {
		return "Education [eid=" + eid + ", uno=" + uno + ", eName=" + eName + ", euStartDate=" + euStartDate
				+ ", euFinishDate=" + euFinishDate + ", euContent=" + euContent + "]";
	}
	
	
}
