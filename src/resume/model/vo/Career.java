package resume.model.vo;

import java.sql.Date;

public class Career implements java.io.Serializable{
	private int carId;
	private int uno;
	private String carName;
	private Date carStartDate;
	private Date carFinishDate;
	private String carContent;
	
	public Career() {}

	public Career(int carId, int uno, String carName, Date carStartDate, Date carFinishDate, String carContent) {
		super();
		this.carId = carId;
		this.uno = uno;
		this.carName = carName;
		this.carStartDate = carStartDate;
		this.carFinishDate = carFinishDate;
		this.carContent = carContent;
	}

	public int getCarId() {
		return carId;
	}

	public void setCarId(int carId) {
		this.carId = carId;
	}

	public int getUno() {
		return uno;
	}

	public void setUno(int uno) {
		this.uno = uno;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public Date getCarStartDate() {
		return carStartDate;
	}

	public void setCarStartDate(Date carStartDate) {
		this.carStartDate = carStartDate;
	}

	public Date getCarFinishDate() {
		return carFinishDate;
	}

	public void setCarFinishDate(Date carFinishDate) {
		this.carFinishDate = carFinishDate;
	}

	public String getCarContent() {
		return carContent;
	}

	public void setCarContent(String carContent) {
		this.carContent = carContent;
	}

	@Override
	public String toString() {
		return "Career [carId=" + carId + ", uno=" + uno + ", carName=" + carName + ", carStartDate=" + carStartDate
				+ ", carFinishDate=" + carFinishDate + ", carContent=" + carContent + "]";
	}
	
	
}
