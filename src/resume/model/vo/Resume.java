package resume.model.vo;

import java.util.ArrayList;
import java.util.HashMap;

public class Resume implements java.io.Serializable{
	private int uno;
	private String rTitle;
	private String rIntro;
	
	private ArrayList<ProjectCareer> plist;
	private ArrayList<School> slist;
	private ArrayList<Education> elist;
	private ArrayList<Certification> celist;
	private ArrayList<Career> calist;
	private ArrayList<HashMap<String, String>> khlist;
	
	public Resume() {}

	public Resume(int uno, String rTitle, String rIntro, ArrayList<ProjectCareer> plist, ArrayList<School> slist,
			ArrayList<Education> elist, ArrayList<Certification> celist, ArrayList<Career> calist,
			ArrayList<HashMap<String, String>> khlist) {
		super();
		this.uno = uno;
		this.rTitle = rTitle;
		this.rIntro = rIntro;
		this.plist = plist;
		this.slist = slist;
		this.elist = elist;
		this.celist = celist;
		this.calist = calist;
		this.khlist = khlist;
	}




	public int getUno() {
		return uno;
	}


	public void setUno(int uno) {
		this.uno = uno;
	}


	public String getrTitle() {
		return rTitle;
	}


	public void setrTitle(String rTitle) {
		this.rTitle = rTitle;
	}


	public String getrIntro() {
		return rIntro;
	}


	public void setrIntro(String rIntro) {
		this.rIntro = rIntro;
	}


	public ArrayList<ProjectCareer> getPlist() {
		return plist;
	}


	public void setPlist(ArrayList<ProjectCareer> plist) {
		this.plist = plist;
	}


	public ArrayList<School> getSlist() {
		return slist;
	}


	public void setSlist(ArrayList<School> slist) {
		this.slist = slist;
	}


	public ArrayList<Education> getElist() {
		return elist;
	}


	public void setElist(ArrayList<Education> elist) {
		this.elist = elist;
	}


	public ArrayList<Certification> getCelist() {
		return celist;
	}


	public void setCelist(ArrayList<Certification> celist) {
		this.celist = celist;
	}


	public ArrayList<Career> getCalist() {
		return calist;
	}


	public void setCalist(ArrayList<Career> calist) {
		this.calist = calist;
	}
	
	

	public ArrayList<HashMap<String, String>> getKhlist() {
		return khlist;
	}

	public void setKhlist(ArrayList<HashMap<String, String>> khlist) {
		this.khlist = khlist;
	}

	@Override
	public String toString() {
		return "Resume [uno=" + uno + ", rTitle=" + rTitle + ", rIntro=" + rIntro + ", plist=" + plist + ", slist="
				+ slist + ", elist=" + elist + ", celist=" + celist + ", calist=" + calist + ", khlist=" + khlist + "]";
	}

	
	
}
