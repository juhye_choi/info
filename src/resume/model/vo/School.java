package resume.model.vo;

import java.sql.Date;

public class School implements java.io.Serializable{
	private int sid;
	private int uno;
	private String sName;
	private Date enterDate;
	private Date graduDate;
	private String major;
	private String minor;
	
	public School() {}

	public School(int sid, int uno, String sName, Date enterDate, Date graduDate, String major, String minor) {
		super();
		this.sid = sid;
		this.uno = uno;
		this.sName = sName;
		this.enterDate = enterDate;
		this.graduDate = graduDate;
		this.major = major;
		this.minor = minor;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public int getUno() {
		return uno;
	}

	public void setUno(int uno) {
		this.uno = uno;
	}

	public String getsName() {
		return sName;
	}

	public void setsName(String sName) {
		this.sName = sName;
	}

	public Date getEnterDate() {
		return enterDate;
	}

	public void setEnterDate(Date enterDate) {
		this.enterDate = enterDate;
	}

	public Date getGraduDate() {
		return graduDate;
	}

	public void setGraduDate(Date graduDate) {
		this.graduDate = graduDate;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getMinor() {
		return minor;
	}

	public void setMinor(String minor) {
		this.minor = minor;
	}

	@Override
	public String toString() {
		return "School [sid=" + sid + ", uno=" + uno + ", sName=" + sName + ", enterDate=" + enterDate + ", graduDate="
				+ graduDate + ", major=" + major + ", minor=" + minor + "]";
	}
	
	
}
