package resume.model.vo;

import java.sql.Date;

public class Certification implements java.io.Serializable{
	private int cerId;
	private int uno;
	private String cerName;
	private Date certifyDate;
	
	public Certification() {}

	public Certification(int cerId, int uno, String cerName, Date certifyDate) {
		super();
		this.cerId = cerId;
		this.uno = uno;
		this.cerName = cerName;
		this.certifyDate = certifyDate;
	}

	public int getcerId() {
		return cerId;
	}

	public void setcerId(int cerId) {
		this.cerId = cerId;
	}

	public int getUno() {
		return uno;
	}

	public void setUno(int uno) {
		this.uno = uno;
	}

	public String getCerName() {
		return cerName;
	}

	public void setCerName(String cerName) {
		this.cerName = cerName;
	}

	public Date getCertifyDate() {
		return certifyDate;
	}

	public void setCertifyDate(Date certifyDate) {
		this.certifyDate = certifyDate;
	}

	@Override
	public String toString() {
		return "Certification [cerId=" + cerId + ", uno=" + uno + ", cerName=" + cerName + ", certifyDate="
				+ certifyDate + "]";
	}
	
	
}
