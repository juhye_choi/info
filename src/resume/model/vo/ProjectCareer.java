package resume.model.vo;

import java.sql.Date;
import java.util.ArrayList;

import keyword.model.vo.Keyword;

public class ProjectCareer implements java.io.Serializable{
	private int pjtId;
	private int uno;
	private String pjtName;
	private Date pjtStart;
	private Date pjtFinish;
	private String myRole;
	private String pjtContent;
	private String pjtLink;
	private String rTitle;
	private ArrayList<Attachment> atlist;
	private Attachment attachmentFile;
	private ArrayList<Keyword> klist;
	
	public ProjectCareer() {}

	public ProjectCareer(int pjtId, int uno, String pjtName, Date pjtStart, Date pjtFinish, String myRole,
			String pjtContent, String pjtLink, String rTitle, ArrayList<Attachment> atlist, Attachment attachmentFile,
			ArrayList<Keyword> klist) {
		super();
		this.pjtId = pjtId;
		this.uno = uno;
		this.pjtName = pjtName;
		this.pjtStart = pjtStart;
		this.pjtFinish = pjtFinish;
		this.myRole = myRole;
		this.pjtContent = pjtContent;
		this.pjtLink = pjtLink;
		this.rTitle = rTitle;
		this.atlist = atlist;
		this.attachmentFile = attachmentFile;
		this.klist = klist;
	}



	public int getPjtId() {
		return pjtId;
	}

	public void setPjtId(int pjtId) {
		this.pjtId = pjtId;
	}

	public int getUno() {
		return uno;
	}

	public void setUno(int uno) {
		this.uno = uno;
	}

	public String getPjtName() {
		return pjtName;
	}

	public void setPjtName(String pjtName) {
		this.pjtName = pjtName;
	}

	public Date getPjtStart() {
		return pjtStart;
	}

	public void setPjtStart(Date pjtStart) {
		this.pjtStart = pjtStart;
	}

	public Date getPjtFinish() {
		return pjtFinish;
	}

	public void setPjtFinish(Date pjtFinish) {
		this.pjtFinish = pjtFinish;
	}

	public String getMyRole() {
		return myRole;
	}

	public void setMyRole(String myRole) {
		this.myRole = myRole;
	}

	public String getPjtContent() {
		return pjtContent;
	}

	public void setPjtContent(String pjtContent) {
		this.pjtContent = pjtContent;
	}

	public String getPjtLink() {
		return pjtLink;
	}

	public void setPjtLink(String pjtLink) {
		this.pjtLink = pjtLink;
	}

	public String getrTitle() {
		return rTitle;
	}

	public void setrTitle(String rTitle) {
		this.rTitle = rTitle;
	}

	public ArrayList<Attachment> getAtlist() {
		return atlist;
	}

	public void setAtlist(ArrayList<Attachment> atlist) {
		this.atlist = atlist;
	}

	public Attachment getAttachmentFile() {
		return attachmentFile;
	}

	public void setAttachmentFile(Attachment attachmentFile) {
		this.attachmentFile = attachmentFile;
	}
	
	public ArrayList<Keyword> getKlist() {
		return klist;
	}

	public void setKlist(ArrayList<Keyword> klist) {
		this.klist = klist;
	}

	@Override
	public String toString() {
		return "ProjectCareer [pjtId=" + pjtId + ", uno=" + uno + ", pjtName=" + pjtName + ", pjtStart=" + pjtStart
				+ ", pjtFinish=" + pjtFinish + ", myRole=" + myRole + ", pjtContent=" + pjtContent + ", pjtLink="
				+ pjtLink + ", rTitle=" + rTitle + ", atlist=" + atlist + ", attachmentFile=" + attachmentFile
				+ ", klist=" + klist + "]";
	}
}
















