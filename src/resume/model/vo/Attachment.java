package resume.model.vo;

import java.io.Serializable;
import java.sql.Date;

public class Attachment implements Serializable{
	private int atId;
	private String originName;
	private String changeName;
	private String filePath;
	private Date atDate;
	private String removeYN;
	private Date removeDate;
	private int category;
	private int fileLevel;
	
	private int phoPjtId;
	private int attPjtId;
	
	public Attachment() {}

	


	public Attachment(int atId, String originName, String changeName, String filePath, Date atDate, String removeYN,
			Date removeDate, int category, int fileLevel, int phoPjtId, int attPjtId) {
		super();
		this.atId = atId;
		this.originName = originName;
		this.changeName = changeName;
		this.filePath = filePath;
		this.atDate = atDate;
		this.removeYN = removeYN;
		this.removeDate = removeDate;
		this.category = category;
		this.fileLevel = fileLevel;
		this.phoPjtId = phoPjtId;
		this.attPjtId = attPjtId;
	}




	public int getAtId() {
		return atId;
	}

	public void setAtId(int atId) {
		this.atId = atId;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getChangeName() {
		return changeName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getAtDate() {
		return atDate;
	}

	public void setAtDate(Date atDate) {
		this.atDate = atDate;
	}

	public String getRemoveYN() {
		return removeYN;
	}

	public void setRemoveYN(String removeYN) {
		this.removeYN = removeYN;
	}

	public Date getRemoveDate() {
		return removeDate;
	}

	public void setRemoveDate(Date removeDate) {
		this.removeDate = removeDate;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getPhoPjtId() {
		return phoPjtId;
	}

	public void setPhoPjtId(int phoPjtId) {
		this.phoPjtId = phoPjtId;
	}

	public int getAttPjtId() {
		return attPjtId;
	}

	public void setAttPjtId(int attPjtId) {
		this.attPjtId = attPjtId;
	}

	public int getFileLevel() {
		return fileLevel;
	}

	public void setFileLevel(int fileLevel) {
		this.fileLevel = fileLevel;
	}

	@Override
	public String toString() {
		return "Attachment [atId=" + atId + ", originName=" + originName + ", changeName=" + changeName + ", filePath="
				+ filePath + ", atDate=" + atDate + ", removeYN=" + removeYN + ", removeDate=" + removeDate
				+ ", category=" + category + ", fileLevel=" + fileLevel + ", phoPjtId=" + phoPjtId + ", attPjtId="
				+ attPjtId + "]";
	}

	
	
}
