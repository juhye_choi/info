package resume.model.vo;

import java.sql.Date;

public class ApplySituation implements java.io.Serializable{
	private int applyId;
	private int uno;
	private int recId;
	private String recTitle;
	private Date recStart;
	private Date recFinish;
	private Date applyDate;
	private String status;
	private String name;
	private Date pBirth;
	private Attachment attachment;
	
	public ApplySituation () {}

	

	public ApplySituation(int applyId, int uno, int recId, String recTitle, Date recStart, Date recFinish,
			Date applyDate, String status, String name, Date pBirth, Attachment attachment) {
		super();
		this.applyId = applyId;
		this.uno = uno;
		this.recId = recId;
		this.recTitle = recTitle;
		this.recStart = recStart;
		this.recFinish = recFinish;
		this.applyDate = applyDate;
		this.status = status;
		this.name = name;
		this.pBirth = pBirth;
		this.attachment = attachment;
	}



	public int getApplyId() {
		return applyId;
	}

	public void setApplyId(int applyId) {
		this.applyId = applyId;
	}

	public int getUno() {
		return uno;
	}

	public void setUno(int uno) {
		this.uno = uno;
	}

	public int getRecId() {
		return recId;
	}

	public void setRecId(int recId) {
		this.recId = recId;
	}

	public String getRecTitle() {
		return recTitle;
	}

	public void setRecTitle(String recTitle) {
		this.recTitle = recTitle;
	}

	public Date getRecStart() {
		return recStart;
	}

	public void setRecStart(Date recStart) {
		this.recStart = recStart;
	}

	public Date getRecFinish() {
		return recFinish;
	}

	public void setRecFinish(Date recFinish) {
		this.recFinish = recFinish;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	

	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public Date getpBirth() {
		return pBirth;
	}



	public void setpBirth(Date pBirth) {
		this.pBirth = pBirth;
	}



	public Attachment getAttachment() {
		return attachment;
	}



	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}



	@Override
	public String toString() {
		return "ApplySituation [applyId=" + applyId + ", uno=" + uno + ", recId=" + recId + ", recTitle=" + recTitle
				+ ", recStart=" + recStart + ", recFinish=" + recFinish + ", applyDate=" + applyDate + ", status="
				+ status + ", name=" + name + ", pBirth=" + pBirth + ", attachment=" + attachment + "]";
	}	

}
