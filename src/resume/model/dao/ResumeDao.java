package resume.model.dao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import keyword.model.vo.Keyword;
import keyword.model.vo.PageInfo;
import resume.model.vo.ApplySituation;
import resume.model.vo.Attachment;
import resume.model.vo.Career;
import resume.model.vo.Certification;
import resume.model.vo.Education;
import resume.model.vo.MorePageInfo;
import resume.model.vo.ProjectCareer;
import resume.model.vo.Resume;
import resume.model.vo.School;

import static common.JDBCTemplate.*;

public class ResumeDao {
	private Properties prop = new Properties();
	
	public ResumeDao() {
		
		String fileName = ResumeDao.class.getResource("/sql/resume/resume-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Resume selectResume(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Resume resume = null;
		
		String query = prop.getProperty("selectResume");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				resume = new Resume();
				resume.setUno(rset.getInt("UNO"));
				resume.setrTitle(rset.getString("RTITLE"));
				resume.setrIntro(rset.getString("RINTRO"));
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return resume;
	}

	public ArrayList<Career> selectCareer(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Career> list = null;
		
		String query = prop.getProperty("selectCareer");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			while(rset.next()) {
				Career c = new Career();
				
				c.setCarId(rset.getInt("CARID")); 
				c.setUno(rset.getInt("UNO"));
				c.setCarName(rset.getString("CARNAME"));
				c.setCarContent(rset.getString("CARCONTENT"));
				c.setCarStartDate(rset.getDate("CARSTART_DATE"));
				c.setCarFinishDate(rset.getDate("CAREND_DATE"));
				
				
				list.add(c);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}

	public ArrayList<Education> selectEducation(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Education> list = null;
		
		String query = prop.getProperty("selectEducation");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			while(rset.next()) {
				Education edu = new Education();
				
				edu.setEid(rset.getInt("EID"));
				edu.seteName(rset.getString("ENAME"));
				edu.setUno(rset.getInt("UNO"));
				edu.setEuContent(rset.getString("EUCONTENT"));
				edu.setEuStartDate(rset.getDate("EUSTART_DATE"));
				edu.setEuFinishDate(rset.getDate("EUEND_DATE"));
				
				list.add(edu);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<School> selectSchool(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<School> list = null;
		
		String query = prop.getProperty("selectSchool");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			while(rset.next()) {
				School sch = new School();
				
				sch.setSid(rset.getInt("SID"));
				sch.setUno(rset.getInt("UNO"));
				sch.setsName(rset.getString("SNAME"));
				sch.setEnterDate(rset.getDate("ENTER_DATE"));
				sch.setGraduDate(rset.getDate("GRADU_DATE"));
				sch.setMajor(rset.getString("MAJOR"));
				sch.setMinor(rset.getString("MINOR"));
				
				list.add(sch);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}

	public ArrayList<Certification> selectCertification(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Certification> list = null;
		
		String query = prop.getProperty("selectCertification");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			while(rset.next()) {
				Certification cer = new Certification();
				
				cer.setcerId(rset.getInt("CERID"));
				cer.setUno(rset.getInt("UNO"));
				cer.setCerName(rset.getString("CERNAME"));
				cer.setCertifyDate(rset.getDate("CERTIFY_DATE"));
				
				list.add(cer);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}

	public ArrayList<ProjectCareer> selectProejct(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<ProjectCareer> list = null;
		ArrayList<Attachment> atlist = null;
		
		String query = prop.getProperty("selectProject");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				ProjectCareer pjt = new ProjectCareer();
				Attachment att = new Attachment();
				atlist = new ArrayList<Attachment>();
				
				pjt.setPjtId(rset.getInt("PJTID"));
				pjt.setUno(rset.getInt("UNO"));
				pjt.setPjtName(rset.getString("PJTNAME"));
				pjt.setMyRole(rset.getString("MYROLE"));
				pjt.setPjtStart(rset.getDate("PJT_START"));
				pjt.setPjtFinish(rset.getDate("PJT_FINISH"));
				//pjt.setPjtContent(rset.getString("PJTCONTENT"));
				//pjt.setPjtLink(rset.getString("PJTLINK"));
				
				att.setAtId(rset.getInt("ATID"));
				att.setOriginName(rset.getString("ORIGIN_NAME"));
				att.setChangeName(rset.getString("CHANGE_NAME"));
				att.setFilePath(rset.getString("FILE_PATH"));
				att.setAtDate(rset.getDate("ATDATE"));
				
				atlist.add(att);
				pjt.setAtlist(atlist);
				list.add(pjt);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}

	public int checkProject(Connection con, int uno, int pjtId) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("checkProject");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setInt(2, pjtId);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		//System.out.println("result : " + result);
		return result;
	}

	public int insertResume(Connection con, int uno) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertResume");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setString(2, "타이틀을 입력해주세요.");
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result;
	}

	public ProjectCareer selectProjectDetail(Connection con, int pjtId) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ProjectCareer pc = null;
		
		String query = prop.getProperty("selectProjectDetail");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, pjtId);
			pstmt.setInt(2, pjtId);
			
			rset = pstmt.executeQuery();
			pc = new ProjectCareer();
			ArrayList<Attachment> atlist = new ArrayList<Attachment>();
			Attachment attachFile = new Attachment();
			while(rset.next()) {
				Attachment att = new Attachment();
				
				pc.setPjtId(rset.getInt("PJTID"));
				pc.setPjtName(rset.getString("PJTNAME"));
				pc.setUno(rset.getInt("UNO"));
				pc.setMyRole(rset.getString("MYROLE"));
				pc.setPjtStart(rset.getDate("PJT_START"));
				pc.setPjtFinish(rset.getDate("PJT_FINISH"));
				pc.setPjtContent(rset.getString("PJTCONTENT"));
				pc.setPjtLink(rset.getString("PJTLINK"));
				pc.setrTitle(rset.getString("RTITLE"));
				
				att.setAtId(rset.getInt("ATID"));
				att.setOriginName(rset.getString("ORIGIN_NAME"));
				att.setChangeName(rset.getString("CHANGE_NAME"));
				att.setFilePath(rset.getString("FILE_PATH"));
				att.setAtDate(rset.getDate("ATDATE"));
				att.setCategory(rset.getInt("CATEGORY"));
				
				if(att.getCategory() == 3) {
					attachFile.setAtId(att.getAtId());
					attachFile.setOriginName(att.getOriginName());
					attachFile.setChangeName(att.getChangeName());
					attachFile.setFilePath(att.getFilePath());
					attachFile.setAtDate(att.getAtDate());
					attachFile.setCategory(att.getCategory());
				} else {
					atlist.add(att);					
				}
			}
			
			//System.out.println(atlist);
			pc.setAtlist(atlist);
			pc.setAttachmentFile(attachFile);
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return pc;
	}

	public int insertProjectCareer(Connection con, ProjectCareer pc) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertProjectCareer");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, pc.getUno());
			pstmt.setString(2, pc.getPjtName());
			pstmt.setString(3, pc.getMyRole());
			pstmt.setDate(4, pc.getPjtStart());
			pstmt.setDate(5, pc.getPjtFinish());
			pstmt.setString(6, pc.getPjtContent());
			pstmt.setString(7, pc.getPjtLink());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int insertAttachment(Connection con, Attachment attachment, int i) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = "";
		if(i == 2) { //프로젝트 사진 insert
			query = prop.getProperty("insertAttachment2");
		} else { // i=3 일때 (프로젝트 첨부파일 insert)
			query = prop.getProperty("insertAttachment3");
		}
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, attachment.getOriginName());
			pstmt.setString(2, attachment.getChangeName());
			pstmt.setString(3, attachment.getFilePath());
			pstmt.setInt(4, i);
			
			if(i==2) { //프로젝트 사진 insert
				pstmt.setInt(5, attachment.getPhoPjtId());
				pstmt.setInt(6, attachment.getFileLevel());
			} else { // i=3 일때 (프로젝트 첨부파일 insert)
				pstmt.setInt(5, attachment.getAttPjtId());
			}
			
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int selectCurrval(Connection con) {
		Statement stmt = null;
		int result = 0;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectCurrval");
		
		try {
			stmt = con.createStatement();
			
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
		}
		return result;
	}

	public int insertCareer(Connection con, Career requestCareer) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertCareer");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, requestCareer.getUno());
			pstmt.setString(2, requestCareer.getCarName());
			pstmt.setString(3, requestCareer.getCarContent());
			pstmt.setDate(4, requestCareer.getCarStartDate());
			pstmt.setDate(5, requestCareer.getCarFinishDate());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int updateCareer(Connection con, Career requestCareer) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateCareer");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestCareer.getCarName());
			pstmt.setString(2, requestCareer.getCarContent());
			pstmt.setDate(3, requestCareer.getCarStartDate());
			pstmt.setDate(4, requestCareer.getCarFinishDate());
			pstmt.setInt(5, requestCareer.getCarId());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result;
	}

	public int deleteCareer(Connection con, int carid) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deleteCareer");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, carid);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int insertSchool(Connection con, School requestSchool) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertSchool");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, requestSchool.getUno());
			pstmt.setString(2, requestSchool.getsName());
			pstmt.setDate(3, requestSchool.getEnterDate());
			pstmt.setDate(4, requestSchool.getGraduDate());
			pstmt.setString(5, requestSchool.getMajor());
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int updateSchool(Connection con, School requestSchool) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateSchool");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestSchool.getsName());
			pstmt.setDate(2, requestSchool.getEnterDate());
			pstmt.setDate(3, requestSchool.getGraduDate());
			pstmt.setString(4, requestSchool.getMajor());
			pstmt.setInt(5, requestSchool.getSid());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int deleteSchool(Connection con, int sid) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deleteSchool");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, sid);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int insertEducation(Connection con, Education requestEdu) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertEducation");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, requestEdu.getUno());
			pstmt.setString(2, requestEdu.geteName());
			pstmt.setString(3, requestEdu.getEuContent());
			pstmt.setDate(4, requestEdu.getEuStartDate());
			pstmt.setDate(5, requestEdu.getEuFinishDate());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int updateEducation(Connection con, Education requestEdu) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateEducation");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestEdu.geteName());
			pstmt.setString(2, requestEdu.getEuContent());
			pstmt.setDate(3, requestEdu.getEuStartDate());
			pstmt.setDate(4, requestEdu.getEuFinishDate());
			pstmt.setInt(5, requestEdu.getEid());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int deleteEducation(Connection con, int eid) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deleteEducation");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, eid);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int insertCertification(Connection con, Certification requestCer) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertCertification");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, requestCer.getUno());
			pstmt.setString(2, requestCer.getCerName());
			pstmt.setDate(3, requestCer.getCertifyDate());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int updateCertification(Connection con, Certification requestCer) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateCertification");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestCer.getCerName());
			pstmt.setDate(2, requestCer.getCertifyDate());
			pstmt.setInt(3, requestCer.getcerId());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int deleteCertification(Connection con, int cerid) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deleteCertification");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cerid);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int insertKeyword(Connection con, int pjtId, Keyword k) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertKeyword");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, k.getKcode());
			pstmt.setInt(2, pjtId);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<Keyword> selectKeyword(Connection con, int pjtId) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Keyword> klist = null;
		
		String query = prop.getProperty("selectKeyword");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, pjtId);
			
			rset = pstmt.executeQuery();
			
			klist = new ArrayList<>();
			while(rset.next()) {
				Keyword k = new Keyword();
				
				k.setKcode(rset.getInt("KCODE"));
				k.setKname(rset.getString("KNAME"));
				k.setCategory(rset.getString("CATEGORY"));
				
				klist.add(k);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return klist;
	}

	public HashMap<String, Object> selectAttachment(Connection con, int pjtId) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		HashMap<String, Object> hmap = null;
		
		String query = prop.getProperty("selectAttachment");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, pjtId);
			pstmt.setInt(2, pjtId);
			
			rset = pstmt.executeQuery();
			
			hmap = new HashMap<>();
			ArrayList<Attachment> atlist = new ArrayList<>();
			Attachment originFile = null;
			while(rset.next()) {
				Attachment aTemp = new Attachment();
				aTemp.setAtId(rset.getInt("ATID"));
				aTemp.setFilePath(rset.getString("FILE_PATH"));
				aTemp.setOriginName(rset.getString("ORIGIN_NAME"));
				aTemp.setChangeName(rset.getString("CHANGE_NAME"));
				aTemp.setCategory(rset.getInt("CATEGORY"));
				aTemp.setPhoPjtId(rset.getInt("PHO_PJTID"));
				aTemp.setAttPjtId(rset.getInt("ATT_PJTID"));
				aTemp.setFileLevel(rset.getInt("FILE_LEVEL"));
				
				if(aTemp.getCategory() == 2) { //프로젝트 사진일 경우,
					atlist.add(aTemp);
				} else { //첨부파일일 경우,
					originFile = new Attachment();
					originFile.setAtId(aTemp.getAtId());
					originFile.setFilePath(aTemp.getFilePath());
					originFile.setOriginName(aTemp.getOriginName());
					originFile.setChangeName(aTemp.getChangeName());
					originFile.setAttPjtId(aTemp.getAttPjtId());
				}
			}
			hmap.put("originPhoto", atlist);
			hmap.put("originFile", originFile);
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return hmap;
	}

	public int updateProjectCareer(Connection con, ProjectCareer pc) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateProjectCareer");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, pc.getPjtName());
			pstmt.setString(2, pc.getMyRole());
			pstmt.setDate(3, pc.getPjtStart());
			pstmt.setDate(4, pc.getPjtFinish());
			pstmt.setString(5, pc.getPjtContent());
			pstmt.setString(6, pc.getPjtLink());
			pstmt.setInt(7, pc.getPjtId());
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result;
	}

	public int deleteAttachment(Connection con, int pjtId) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deleteAttachment");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, pjtId);
			pstmt.setInt(2, pjtId);
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int deleteKeyword(Connection con, int pjtId) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deleteKeyword");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, pjtId);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int deleteProjectCareer(Connection con, int pjtId) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deleteProjectCareer");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, pjtId);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int deleteAttachmentPermanent(Connection con, int pjtId) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deleteAttachmentPermanent");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, pjtId);
			pstmt.setInt(2, pjtId);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<HashMap<String, String>> selectKeywordList(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, String>> khlist = null;
		
		String query = prop.getProperty("selectKeywordList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			khlist = new ArrayList<HashMap<String,String>>();
			while(rset.next()) {
				HashMap<String, String>hmap = new HashMap<String, String>();

				hmap.put("kname", rset.getString("KNAME"));
				hmap.put("kcode", rset.getString("KCODE"));
				hmap.put("count", rset.getString("COUNT"));
				
				khlist.add(hmap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return khlist;
	}

	public Attachment selectAttachFile(Connection con, int fileId) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Attachment at = null;
		
		String query = prop.getProperty("selectAttachFile");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, fileId);
			
			rset = pstmt.executeQuery();
			
			at = new Attachment();
			if(rset.next()) {
				at.setAtId(rset.getInt("ATID"));
				at.setOriginName(rset.getString("ORIGIN_NAME"));
				at.setChangeName(rset.getString("CHANGE_NAME"));
				at.setFilePath(rset.getString("FILE_PATH"));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return at;
	}

	public int updateIntro(Connection con, int uno, String intro) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateIntro");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, intro);
			pstmt.setInt(2, uno);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<HashMap<String, String>> selectSeenRecruitList(Connection con, int uno, MorePageInfo mpi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, String>> list = null;
		
		String query = prop.getProperty("selectSeenRecruitList");
		
		int end = (mpi.getCurrentPage()-1) * mpi.getLimit() + 10;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setInt(2, end);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			while(rset.next()) {
				HashMap<String, String> seenMap = new HashMap<String, String>();
				
				seenMap.put("watchDate", rset.getDate("WATCH_DATE").toString());
				seenMap.put("recTitle", rset.getString("REC_TITLE"));
				seenMap.put("recStart", rset.getDate("REC_START").toString());
				seenMap.put("recFinish", rset.getDate("REC_FINISH").toString());
				seenMap.put("recId", rset.getString("RECID"));
				seenMap.put("status", rset.getString("STATUS"));
				
				list.add(seenMap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public int getSeenRecruitListCount(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getSeenRecruitListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return result;
	}

	public int getPBlackListCount(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getPBlackListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<HashMap<String, String>> selectPBlackList(Connection con, int uno, MorePageInfo mpi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, String>> list = null;
		
		String query = prop.getProperty("selectPBlackList");
		
		int end = (mpi.getCurrentPage()-1) * mpi.getLimit() + 10;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setInt(2, end);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			while(rset.next()) {
				HashMap<String, String> pblhmap = new HashMap<String, String>();
				
				pblhmap.put("pblHisId", rset.getString("PBLHIS_ID"));
				pblhmap.put("pblId", rset.getString("PBLID"));
				pblhmap.put("pblDate", rset.getDate("PBLIST_DATE").toString());
				pblhmap.put("reason", rset.getString("REASON"));
				pblhmap.put("category", rset.getString("CATEGORY"));
				pblhmap.put("cname", rset.getString("NAME"));
				
				list.add(pblhmap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public int updatePBlackList(Connection con, int pblHisId, String reason) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updatePBlackList");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, reason);
			pstmt.setInt(2, pblHisId);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}

	public int deletePBlackList(Connection con, int pblId) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deletePBlackList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, pblId);
			pstmt.setString(2, "해지");
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<HashMap<String, String>> selectpdfKeyword(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, String>> pdfklist = null;
		
		String query = prop.getProperty("selectpdfKeyword");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			pdfklist = new ArrayList<HashMap<String, String>>();
			while(rset.next()) {
				HashMap<String, String> pdfkmap = new HashMap<>();
				
				pdfkmap.put("kname", rset.getString("KNAME"));
				pdfkmap.put("kcode", rset.getString("KCODE"));
				pdfkmap.put("pjtid", rset.getString("PJTID"));
				
				pdfklist.add(pdfkmap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return pdfklist;
	}

	public int getApplyListCount(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getApplyListCount");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public ArrayList<ApplySituation> selectApplyLogList(Connection con, int uno, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<ApplySituation> aslist = null;

		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		String query = prop.getProperty("selectApplyLogList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			aslist = new ArrayList<ApplySituation>();
			while(rset.next()) {
				ApplySituation as = new ApplySituation();
				as.setApplyId(rset.getInt("APPID"));
				as.setUno(rset.getInt("UNO"));
				as.setRecId(rset.getInt("RECID"));
				as.setRecTitle(rset.getString("REC_TITLE"));
				as.setApplyDate(rset.getDate("APPLY_DATE"));
				as.setStatus(rset.getString("STATUS"));
				as.setRecStart(rset.getDate("REC_START"));
				as.setRecFinish(rset.getDate("REC_FINISH"));
				
				aslist.add(as);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return aslist;
	}

	public int getApplyListCountTOT(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getApplyListCountTOT");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public ArrayList<ApplySituation> selectApplyLogListTOT(Connection con, int uno, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<ApplySituation> aslist = null;

		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		String query = prop.getProperty("selectApplyLogListTOT");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			aslist = new ArrayList<ApplySituation>();
			while(rset.next()) {
				ApplySituation as = new ApplySituation();
				as.setApplyId(rset.getInt("APPID"));
				as.setUno(rset.getInt("UNO"));
				as.setRecId(rset.getInt("RECID"));
				as.setRecTitle(rset.getString("REC_TITLE"));
				as.setApplyDate(rset.getDate("APPLY_DATE"));
				as.setStatus(rset.getString("STATUS"));
				as.setRecStart(rset.getDate("REC_START"));
				as.setRecFinish(rset.getDate("REC_FINISH"));
				
				aslist.add(as);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return aslist;
	}

	public int getApplyListCountEND(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getApplyListCountEND");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public ArrayList<ApplySituation> selectApplyLogListEND(Connection con, int uno, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<ApplySituation> aslist = null;

		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		String query = prop.getProperty("selectApplyLogListEND");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			aslist = new ArrayList<ApplySituation>();
			while(rset.next()) {
				ApplySituation as = new ApplySituation();
				as.setApplyId(rset.getInt("APPID"));
				as.setUno(rset.getInt("UNO"));
				as.setRecId(rset.getInt("RECID"));
				as.setRecTitle(rset.getString("REC_TITLE"));
				as.setApplyDate(rset.getDate("APPLY_DATE"));
				as.setStatus(rset.getString("STATUS"));
				as.setRecStart(rset.getDate("REC_START"));
				as.setRecFinish(rset.getDate("REC_FINISH"));
				
				aslist.add(as);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return aslist;
	}

	public int updateRtitle(Connection con, int uno, String rtitle) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateRtitle");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, rtitle);
			pstmt.setInt(2, uno);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<ApplySituation> selectApplicantList(Connection con, int recid) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<ApplySituation> aslist = null;
		
		String query = prop.getProperty("selectApplicantList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, recid);
			
			rset = pstmt.executeQuery();
			aslist = new ArrayList<ApplySituation>();
			while(rset.next()) {
				ApplySituation as = new ApplySituation();
				Attachment at = new Attachment();
				
				as.setApplyId(rset.getInt("APPID"));
				as.setUno(rset.getInt("UNO"));
				as.setName(rset.getString("NAME"));
				as.setRecId(rset.getInt("RECID"));
				as.setApplyDate(rset.getDate("APPLY_DATE"));
				as.setStatus(rset.getString("STATUS"));
				as.setpBirth(rset.getDate("PBIRTH"));
				
				at.setAtId(rset.getInt("ATID"));
				at.setFilePath(rset.getString("FILE_PATH"));
				at.setOriginName(rset.getString("ORIGIN_NAME"));
				at.setChangeName(rset.getString("CHANGE_NAME"));
				as.setAttachment(at);
				
				aslist.add(as);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		
		return aslist;
	}
	
	public int updateSeenResume(Connection con, int appid) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateSeenResume");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, appid);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}

	
}







