package resume.model.service;

import resume.model.dao.ResumeDao;
import resume.model.vo.ApplySituation;
import resume.model.vo.Attachment;
import resume.model.vo.Career;
import resume.model.vo.Certification;
import resume.model.vo.Education;
import resume.model.vo.MorePageInfo;
import resume.model.vo.ProjectCareer;
import resume.model.vo.Resume;
import resume.model.vo.School;

import static common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import keyword.model.dao.KeywordDao;
import keyword.model.vo.Keyword;
import keyword.model.vo.PageInfo;

public class ResumeService {

	public Resume selectResume(int uno) {
		Connection con = getConnection();
		Resume resume = null;
		
		resume = new ResumeDao().selectResume(con, uno);
		if(resume != null) {
			resume.setCalist(new ResumeDao().selectCareer(con, uno));
			resume.setElist(new ResumeDao().selectEducation(con, uno));
			resume.setSlist(new ResumeDao().selectSchool(con, uno));
			resume.setCelist(new ResumeDao().selectCertification(con, uno));
			resume.setPlist(new ResumeDao().selectProejct(con, uno));
			resume.setKhlist(new ResumeDao().selectKeywordList(con, uno));
		}
		
		close(con);
		
		return resume;
	}

	public int checkProject(int uno, int pjtId) {
		Connection con = getConnection();
		int result = 0;
		
		//본인 프로젝트인지 확인하는 메소드
		int result1 = new ResumeDao().checkProject(con, uno, pjtId);
		if(result1 > 0) {
			result = 1;
		}
			
			//다시 한번 채크! 구매한 이력이 있으면 1을 반환!
			/*int result2 = new PointDao().checkUsePoint(con, uno, pjtId);
			if(result2 > 0) {
				result = 1;
			}*/
		close(con);
		
		return result;
	}

	public ProjectCareer selectProjectCareer(int pjtId) {
		Connection con = getConnection();
		ProjectCareer pc = null;
		ArrayList<Keyword> klist = null;
		pc = new ResumeDao().selectProjectDetail(con, pjtId);
		
		if(pc != null) {
			klist = new ResumeDao().selectKeyword(con, pjtId);
			if(klist != null) {
				pc.setKlist(klist);
			}
		}
		
		close(con);
		
		return pc;
	}

	public int insertResume(int uno) {
		Connection con = getConnection();
		
		int result = new ResumeDao().insertResume(con, uno);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}

	public int insertProjectCareer(ProjectCareer pc) {
		Connection con = getConnection();
		int result = 0;
		
		int result1 = new ResumeDao().insertProjectCareer(con, pc); //프로젝트 경력기술서 삽입
		
		if(result1>0) {
			int result2 = 0;
			
			int currval = new ResumeDao().selectCurrval(con); //경력기술서의 SEQ의 Current Value 조회
			
			
			for(int i=0; i<pc.getAtlist().size(); i++) {
				//사진파일을 등록하는 메소드 * 사진개수만큼!
				pc.getAtlist().get(i).setPhoPjtId(currval);
				result2 += new ResumeDao().insertAttachment(con, pc.getAtlist().get(i), 2); //첨부파일(사진) 삽입
			}
			if(result2 >0) {
				int result3 = 0;
				if(pc.getAttachmentFile() != null) {
					pc.getAttachmentFile().setAttPjtId(currval);
					result3 = new ResumeDao().insertAttachment(con, pc.getAttachmentFile(), 3); //첨부파일(파일) 삽입
				} else {
					result3 = 1;
				}
				int result4 = 0;
				if(result3>0) {
					for(int i=0; i<pc.getKlist().size(); i++) {
						result4 += new ResumeDao().insertKeyword(con, currval, pc.getKlist().get(i)); //키워드 삽입
					}
					
					if(result4>0) {
						result =1;
						commit(con);  //모두 성공했으면..!!
						
					} else {
						rollback(con); //키워드 삽입 실패시
					}
				} else {
					rollback(con); // 첨부파일 삽입 실패시
				}
				
			} else {
				rollback(con); // 첨부사진 삽입 실패시
			}
		} else {
			rollback(con); // 경력기술서 삽입 실패시
		}
		close(con);
		
		return result;
	}

	public int insertCareer(Career requestCareer) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().insertCareer(con, requestCareer);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}

	public int updateCareer(Career requestCareer) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().updateCareer(con, requestCareer);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}

	public int deleteCareer(int carid) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().deleteCareer(con, carid);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}

	public int insertSchool(School requestSchool) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().insertSchool(con, requestSchool);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		
		return result;
	}

	public int updateSchool(School requestSchool) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().updateSchool(con, requestSchool);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}

	public int deleteSchool(int sid) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().deleteSchool(con, sid);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}

	public int insertEducation(Education requestEdu) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().insertEducation(con, requestEdu);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}

	public int updateEducation(Education requestEdu) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().updateEducation(con, requestEdu);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}

	public int deleteEducation(int eid) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().deleteEducation(con, eid);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}

	public int insertCertification(Certification requestCer) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().insertCertification(con, requestCer);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}

	public int updateCertification(Certification requestCer) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().updateCertification(con, requestCer);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}

	public int deleteCertification(int cerid) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().deleteCertification(con, cerid);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}

	public HashMap<String, Object> selectAttachment(int pjtId) {
		Connection con = getConnection();
		HashMap<String, Object> hmap = null;
		
		hmap = new ResumeDao().selectAttachment(con, pjtId);
		
		close(con);
		
		return hmap;
	}

	public int updateProjectCareer(ProjectCareer pc) {
		Connection con = getConnection();
		int result = 0;
		
		int result1 = new ResumeDao().updateProjectCareer(con, pc); //pc정보를 업데이트!
		
		if(result1>0) {
			int result2 = new ResumeDao().deleteAttachment(con, pc.getPjtId()); //pjtId를 가진 모든 첨부파일을 삭제!
			
			for(int i=0; i<pc.getAtlist().size(); i++) {	//첨부파일(사진) 새로 삽입!
				result2 += new ResumeDao().insertAttachment(con, pc.getAtlist().get(i), 2);
			}
			
			if(result2>0) {
				int result3 = 0;
				if(pc.getAttachmentFile() != null) {
					result3 = new ResumeDao().insertAttachment(con, pc.getAttachmentFile(), 3); //첨부파일(파일) 삽입!					
				} else {
					result3 = 1;
				}
				
				if(result3>0) {
					
					int result4 = new ResumeDao().deleteKeyword(con, pc.getPjtId()); //기존 키워드 삭제!
					
					for(int i=0; i<pc.getKlist().size(); i++) {
						result4 += new ResumeDao().insertKeyword(con, pc.getPjtId(), pc.getKlist().get(i)); //키워드 삽입
					}
					
					if(result4>0) {
						commit(con);
						result = 1;
					} else {
						rollback(con);
					}
					
				} else {
					rollback(con);
				}
			} else {
				rollback(con);
			}
		} else {
			rollback(con);
		}
		close(con);
		
		return result;
	}

	public int deleteProjectCareer(int pjtId) {
		Connection con = getConnection();
		int result = 0;
		
		//키워드삭제
		int result1 = new ResumeDao().deleteKeyword(con, pjtId);
		
		if(result1>0) {
			//첨부파일 삭제
			int result2 = new ResumeDao().deleteAttachmentPermanent(con, pjtId);
			
			if(result2>0) {
				int result3 = new ResumeDao().deleteProjectCareer(con, pjtId);
				
				if(result3>0) {
					commit(con);
					result=1;
				}
				
			} else {
				rollback(con);
			}
		} else {
			rollback(con);
		}
		return result;
	}

	public Attachment selectAttachFile(int fileId) {
		Connection con = getConnection();
		
		Attachment at = new ResumeDao().selectAttachFile(con, fileId);
		
		close(con);
		
		return at;
	}

	public int updateIntro(int uno, String intro) {
		Connection con = getConnection();
		
		int result = new ResumeDao().updateIntro(con, uno, intro);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		close(con);
		
		return result;
	}

	public ArrayList<HashMap<String, String>> selectSeenRecruitList(int uno, MorePageInfo mpi) {
		Connection con = getConnection();
		ArrayList<HashMap<String, String>> list = null;
		
		list = new ResumeDao().selectSeenRecruitList(con, uno, mpi);
		
		close(con);
		
		return list;
	}

	public int getSeenRecruitListCount(int uno) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().getSeenRecruitListCount(con, uno);
		
		close(con);
		
		return result;
	}

	public int getPBlackListCount(int uno) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().getPBlackListCount(con, uno);
		
		close(con);
		
		return result;
	}

	public ArrayList<HashMap<String, String>> selectPBlackList(int uno, MorePageInfo mpi) {
		Connection con = getConnection();
		ArrayList<HashMap<String, String>> list = null;
		
		list = new ResumeDao().selectPBlackList(con, uno, mpi);
		
		close(con);
		
		return list;
	}

	public int updatePBlackList(int pblHisId, String reason) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().updatePBlackList(con, pblHisId, reason);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public int deletePBlackList(int pblId) {
		Connection con = getConnection();
		int result = 0;
		
		result = new ResumeDao().deletePBlackList(con, pblId);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		return result;
	}

	public ArrayList<HashMap<String, String>> selectpdfKeyword(int uno) {
		Connection con = getConnection();
		ArrayList<HashMap<String, String>> pdfklist = null;
		
		pdfklist = new ResumeDao().selectpdfKeyword(con, uno);
		
		close(con);
		
		return pdfklist;
	}

	public ArrayList<ApplySituation> selectApplyLogList(int uno, PageInfo pi) {
		Connection con = getConnection();
		ArrayList<ApplySituation> aslist = new ResumeDao().selectApplyLogList(con, uno, pi);
		
		close(con);
		
		return aslist;
	}

	public int getApplyListCount(int uno) {
		Connection con = getConnection();
		
		int result = new ResumeDao().getApplyListCount(con, uno);
		
		close(con);
		
		return result;
	}

	public int getApplyListCountTOT(int uno) {
		Connection con = getConnection();
		
		int result = new ResumeDao().getApplyListCountTOT(con, uno);
		
		close(con);
		
		return result;
	}

	public ArrayList<ApplySituation> selectApplyLogListTOT(int uno, PageInfo pi) {
		Connection con = getConnection();
		ArrayList<ApplySituation> aslist = new ResumeDao().selectApplyLogListTOT(con, uno, pi);
		
		close(con);
		
		return aslist;
	}

	public int getApplyListCountEND(int uno) {
		Connection con = getConnection();
		
		int result = new ResumeDao().getApplyListCountEND(con, uno);
		
		close(con);
		
		return result;
	}

	public ArrayList<ApplySituation> selectApplyLogListEND(int uno, PageInfo pi) {
		Connection con = getConnection();
		ArrayList<ApplySituation> aslist = new ResumeDao().selectApplyLogListEND(con, uno, pi);
		
		close(con);
		
		return aslist;
	}

	public int updateRtitle(int uno, String rtitle) {
		Connection con = getConnection();
		int result = new ResumeDao().updateRtitle(con, uno, rtitle);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public ArrayList<ApplySituation> selectApplicantList(int recid) {
		Connection con = getConnection();
		
		ArrayList<ApplySituation> atlist = new ResumeDao().selectApplicantList(con, recid);
		
		close(con);
		
		return atlist;
	}

	public int updateSeenResume(int appid) {
		Connection con = getConnection();
		
		int result = new ResumeDao().updateSeenResume(con, appid);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

}


















