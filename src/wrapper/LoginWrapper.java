package wrapper;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class LoginWrapper extends HttpServletRequestWrapper{

	public LoginWrapper(HttpServletRequest request) {
		super(request);
	
	}
	
	//원래있던 getParameter보다 얘가 먼저 동작하게 작성해주는것!
	@Override
	public String getParameter(String key) {
		
		
		String value="";
		
		//문자열같은거 비교할때 앞에 널이아니면서를 추가해주면 널포인트 예외를 처리해줄수있따!(앞에가 거짓이면 뒤에 실행하지않기때문에)
		if(key != null && key.equals("userPwd")) {
			
			value = getSha512(super.getParameter("userPwd"));
			
		}else {
			
			//원래가지고있는 파라미터에 userId넣어가지고 리턴해주는것! -> 똑같이 실행되는거다
			value = super.getParameter(key);
			
		}
		
		return value;
		
	}
	
	//암호화해주는 메소드!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	public static String getSha512(String pwd) {
		
		String encPwd = null;
		
		try {
			
			//SHA-512는 단방향 해시 암호화 알고리즘 -> 단방향은 한방향으로 암호화되서 다시 복호화 하지 못하고 해시는 다같은 길이로 암호화되는거!
			//암호화된 문장을 Digest라고 한다.
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			
			byte[] bytes= pwd.getBytes(Charset.forName("UTF-8"));
			
			md.update(bytes);
			
			//Base64는 인코딩 종류중 하나
			//digest() 바이트 배열형태로 반환해주고 그것을 문자형 형태로 돌려주는것! , 이떄 문자열로 돌려주는 인코딩 종류가 Base64
			encPwd = Base64.getEncoder().encodeToString(md.digest());
			
			
			
			
			
			
			
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return encPwd;
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
