package declaration.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import declaration.model.service.DeclarationService;


@WebServlet("/decPersonComp.adl")
public class DeclarationPersonCompanionSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public DeclarationPersonCompanionSevlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		int decid = Integer.parseInt(request.getParameter("decid"));
		int result = 0;
		//result = new DeclarationService().DecPersonCompanion(decid);
		
		if(result > 0) {
			response.sendRedirect("/info/declarationList.adl");
		}else {
			request.setAttribute("msg", "경고 처리 실패");
			request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
