package declaration.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import declaration.model.service.DeclarationService;
import declaration.model.vo.DeclarationList;
import member.model.vo.Member;

/**
 * Servlet implementation class WarningListServlet
 */
@WebServlet("/warningList.awl")
public class WarningListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WarningListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		
		String page = "";
		
		if(loginUser != null) {
			if(loginUser.getUno() == 1) {
				ArrayList<DeclarationList> list = new DeclarationService().selectWarningList();

				if(list != null) {
					page = "views/admin/warningPersonList.jsp";
					request.setAttribute("list", list);
				}else {
					page = "views/common/errorPage.jsp";
					request.setAttribute("msg", "경고처리 리스트 조회 실패");
				}
			}else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "잘못된 방법의 접근 방법입니다.");
			}
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "잘못된 방법의 접근 방법입니다.");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
