package declaration.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import declaration.model.service.DeclarationService;

/**
 * Servlet implementation class PersonDeclartionServlet
 */
@WebServlet("/personsingo.re")
public class PersonDeclarationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PersonDeclarationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId = (String) request.getParameter("userId");
		String otherUserId = (String) request.getParameter("otherUserId");
		String text = (String) request.getParameter("text");
		String OtherUserId = "";
		for(int i = 0; i < otherUserId.length(); i++) {
			if(i != 0) {
				OtherUserId += otherUserId.charAt(i);
			}
		}
		
		int uno = new DeclarationService().selectUno(userId);
		int otherUno = new DeclarationService().selectOtherUno(OtherUserId);
		
		String page = "";
		
		if(uno > 0 && otherUno > 0) {
			
			int declare = new DeclarationService().insertPersonDeclaration(uno, otherUno, text);
			
			if(declare > 0) {
				String type = new DeclarationService().selectType(uno);
				
				if(type.equals("P")) {
					response.sendRedirect("/info/searchResume.sr");
				}else {
					response.sendRedirect("/info/search.cr");
				}
				
			}else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "개인 신고 추가 실패");
				request.getRequestDispatcher(page).forward(request, response);
			}
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "회원 정보 조회 실패");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
