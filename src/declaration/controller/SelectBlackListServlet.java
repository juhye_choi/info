package declaration.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import declaration.model.service.DeclarationService;
import declaration.model.vo.DeclarationList;
import member.model.vo.Member;

/**
 * Servlet implementation class SelectBlackListServlet
 */
@WebServlet("/blackList.sab")
public class SelectBlackListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectBlackListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		
		if(loginUser.getUno() != 1) {
			request.setAttribute("msg", "로그인이 필요한 메뉴입니다");
			request.getRequestDispatcher("").forward(request, response);
		}
			ArrayList<DeclarationList> plist = new DeclarationService().selectPBList();
			
			String page = "";
			
			if(plist != null) {
				ArrayList<DeclarationList> clist = new DeclarationService().selectCBList();
				
				if(clist != null) {
					page = "views/admin/blackList.jsp";
					request.setAttribute("plist", plist);
					request.setAttribute("clist", clist);
				}else {
					page = "views/common/errorPage.jsp";
					request.setAttribute("msg", "기업 블래리스트 조회 실패");
				}
			}else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "개인 블래리스트 조회 실패");
			}
			request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
