package declaration.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import declaration.model.service.DeclarationService;
import declaration.model.vo.DeclarationList;


@WebServlet("/search.adl")
public class SearchDeclareServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
    public SearchDeclareServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		int type = Integer.parseInt(request.getParameter("type"));
		//System.out.println(type);
		
		ArrayList<DeclarationList> delist = null;
		if(type==1) { //전체 검색일 경우
			delist = new DeclarationService().searchAllDecList();
		} else if(type ==2){
			delist = new DeclarationService().searchCompleteDecList();
		} else {
			delist = new DeclarationService().selectDeclarationPersonList();
		}
		
		JSONObject decObj = null;
		JSONArray decjArr = new JSONArray();
		
		if(delist != null) {
			for(DeclarationList d : delist) {
				decObj = new JSONObject();
				
				decObj.put("decId", d.getdId());
				decObj.put("decDate", d.getDecDate().toString());
				decObj.put("gUno", d.getgUno());
				decObj.put("gName", URLEncoder.encode(d.getgName(), "UTF-8"));
				decObj.put("gUserId", d.getgUserid());
				decObj.put("rUno", d.getrUno());
				decObj.put("rName", URLEncoder.encode(d.getrName(), "UTF-8"));
				decObj.put("rUserId", d.getrUserId());
				decObj.put("email", d.getEmail());
				decObj.put("content", URLEncoder.encode(d.getContent(), "UTF-8"));
				decObj.put("category", d.getCategory());
				
				decjArr.add(decObj);
				
				
			}
		}
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.print(decjArr.toJSONString());
		
		out.flush();
		out.close();
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
