package declaration.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import declaration.model.service.DeclarationService;

/**
 * Servlet implementation class PlusBlackListTypeCheckServlet
 */
@WebServlet("/checkPCId.apc")
public class PlusBlackListTypeCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PlusBlackListTypeCheckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String type = (String) request.getParameter("category");
		String EmailorNum = (String) request.getParameter("EmailorNum");
		String id = (String) request.getParameter("id");
		
		int result = new DeclarationService().checkType(type, id, EmailorNum);
		
		PrintWriter out = response.getWriter();
		JSONObject checkResult = null;
		
		if(result > 0) {
			checkResult = new JSONObject();
			/*checkResult.put(key, value)*/
		}else {
			
		}
		
		response.setContentType("application/json");
		out.flush();
		out.close();
	} 

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
