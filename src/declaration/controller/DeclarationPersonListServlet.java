package declaration.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import declaration.model.service.DeclarationService;
import declaration.model.vo.DeclarationList;
import member.model.vo.Member;

@WebServlet("/declarationList.adl")
public class DeclarationPersonListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public DeclarationPersonListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		
		String page = "";
		
		if(loginUser != null) {
			if(loginUser.getUno() == 1) {
				ArrayList<DeclarationList> list = new DeclarationService().selectDeclarationPersonList();
				
				if(list != null) {
					page = "views/admin/personDeclaration.jsp";
					request.setAttribute("list", list);
				}
			}else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "잘못된 방법의 접근 방법입니다.");
			}
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "잘못된 방법의 접근 방법입니다.");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
