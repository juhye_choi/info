package declaration.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import declaration.model.service.DeclarationService;

/**
 * Servlet implementation class CancelBlackListServlet
 */
@WebServlet("/cancelBlack.acb")
public class CancelBlackListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CancelBlackListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String num = (String) request.getParameter("delCNum");
		String email = (String) request.getParameter("delContent");
		
		int result = 0; 
		
		if(num != null && email == null) {
			result = new DeclarationService().cancelCBlack(num);
		}else if(num == null && email != null) {
			result = new DeclarationService().cancelPBlack(email);
		}
 		
		if(result > 0) {
			response.sendRedirect("/info/blackList.sab");
		}else {
			String page = "";
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "블랙리스트 취소 실패");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
