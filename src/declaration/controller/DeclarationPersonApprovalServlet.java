package declaration.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import declaration.model.service.DeclarationService;

@WebServlet("/decPersonApp.adl")
public class DeclarationPersonApprovalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public DeclarationPersonApprovalServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//int bid = Integer.parseInt(request.getParameter("Bid"));
		//int result = new DeclarationService().DecPersonApproval(bid);
		int decid = Integer.parseInt(request.getParameter("decid"));
		String content = request.getParameter("content");
		int type = Integer.parseInt(request.getParameter("type"));
		
		
		int result = 0;
		if(type==1) {
			result = new DeclarationService().DecPersonApproval(decid, content);			
		} else {
			result = new DeclarationService().DecPersonCompanion(decid, content);
		}
		
		if(result > 0) {
			response.sendRedirect("/info/declarationList.adl");
		}else {
			request.setAttribute("msg", "경고 처리 실패");
			request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
