package declaration.model.dao;

import static common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import company.model.vo.Company;
import declaration.model.vo.DeclarationList;
import member.model.vo.Member;

public class DeclarationDao {
	
	private Properties prop = new Properties();
	
	public DeclarationDao() {
		String fileName = DeclarationDao.class.getResource("/sql/declaration/declaration-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int selectUno(Connection con, String userId) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int uno = 0;
		
		String query = prop.getProperty("selectUno");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, userId);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				uno = rset.getInt("UNO");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return uno;
	}

	public int selectOtherUno(Connection con, String otherUserId) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int otherUno = 0;
		
		String query = prop.getProperty("selectOtherUno");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, otherUserId);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				otherUno = rset.getInt("UNO");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return otherUno;
	}

	public int insertPersonDeclaration(Connection con, int uno, int otherUno) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPersonDeclaration");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setInt(2, otherUno);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public int selectPersonDeclaration(Connection con){
		Statement stmt = null;
		ResultSet rset = null;
		int decId = 0;
		
		String query = prop.getProperty("selectPersonDeclaration");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				decId = rset.getInt("CURRVAL");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return decId;
	}

	public int insertPersonDeclarationHis(Connection con, int decId, String text) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPersonDeclarationHis");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, decId);
			pstmt.setString(2, text);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<DeclarationList> selectDeclarationPersonList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<DeclarationList> list = null;
		
		String query = prop.getProperty("selectDeclarationPersonList");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<DeclarationList>();
			
			while(rset.next()) {
				DeclarationList d = new DeclarationList();
				
				d.setdId(rset.getInt("DECID"));
				d.setDecDate(rset.getDate("DDATE"));
				d.setgUno(rset.getInt("GUNO"));
				d.setgName(rset.getString("GNAME"));
				d.setgUserid(rset.getString("GID"));
				
				d.setrUno(rset.getInt("RUNO"));
				d.setrName(rset.getString("RNAME"));
				d.setrUserId(rset.getString("RID"));
				
				d.setEmail(rset.getString("EMAIL"));
				d.setContent(rset.getString("CONTENT"));
				d.setCategory(rset.getInt("CATEGORY"));
				
				list.add(d);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return list;
	}
	
	public DeclarationList selectDeclaration(Connection con, int bid) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		DeclarationList d = null;
		
		String query = prop.getProperty("selectDeclaration");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, bid);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				d = new DeclarationList();
				
				d.setdId(rset.getInt("DECID"));
				d.setDecDate(rset.getDate("DDATE"));
				d.setrUno(rset.getInt("GUNO"));
				d.setrName(rset.getString("GNAME"));
				d.setgUno(rset.getInt("RUNO"));
				d.setgName(rset.getString("RNAME"));
				d.setEmail(rset.getString("EMAIL"));
				d.setContent(rset.getString("CONTENT"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return d;
	}
	
	public int DecPersonApproval(Connection con, int decid, String content) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("DecPersonApproval");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, decid);
			pstmt.setString(2, content);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int DecPersonCompanion(Connection con, int decid, String content) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("DecPersonCompanion");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, decid);
			pstmt.setString(2, content);

			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<DeclarationList> selectWarningList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<DeclarationList> list = null;
		
		String query = prop.getProperty("selectWarningList2");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				DeclarationList d = new DeclarationList();
				
				d.setgUserid(rset.getString("USER_ID"));
				d.setgName(rset.getString("NAME"));
				d.setEmail(rset.getString("EMAIL"));
				d.setCount(rset.getInt("COUNT"));
				
				list.add(d);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}

		return list;
	}

	public int plusBlackPerson(Connection con, String id, Member m, String content, String email) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("plusBlackPerson");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, m.getUserName());
			pstmt.setString(2, content);
			pstmt.setString(3, email);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<DeclarationList> selectAllDecList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<DeclarationList> list = null;
		
		String query = prop.getProperty("selectAllDecList");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<DeclarationList>();
			
			while(rset.next()) {
				DeclarationList d = new DeclarationList();
				
				d.setdId(rset.getInt("DECID"));
				d.setDecDate(rset.getDate("DDATE"));
				d.setgUno(rset.getInt("GUNO"));
				d.setgName(rset.getString("GNAME"));
				d.setgUserid(rset.getString("GID"));
				
				d.setrUno(rset.getInt("RUNO"));
				d.setrName(rset.getString("RNAME"));
				d.setrUserId(rset.getString("RID"));
				
				d.setEmail(rset.getString("EMAIL"));
				d.setContent(rset.getString("CONTENT"));
				d.setCategory(rset.getInt("CATEGORY"));
				
				list.add(d);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<DeclarationList> searchCompleteDecList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<DeclarationList> list = null;
		
		String query = prop.getProperty("searchCompleteDecList");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<DeclarationList>();
			
			while(rset.next()) {
				DeclarationList d = new DeclarationList();
				
				d.setdId(rset.getInt("DECID"));
				d.setDecDate(rset.getDate("DDATE"));
				d.setgUno(rset.getInt("GUNO"));
				d.setgName(rset.getString("GNAME"));
				d.setgUserid(rset.getString("GID"));
				
				d.setrUno(rset.getInt("RUNO"));
				d.setrName(rset.getString("RNAME"));
				d.setrUserId(rset.getString("RID"));
				
				d.setEmail(rset.getString("EMAIL"));
				d.setContent(rset.getString("CONTENT"));
				d.setCategory(rset.getInt("CATEGORY"));
				
				list.add(d);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return list;
	}

	public String memberCheck(Connection con, String id) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String memberCheck = null;
		
		String query = prop.getProperty("memberCheck");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, id);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				memberCheck = rset.getString("CATEGORY");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return memberCheck;
	}

	public Member personInfo(Connection con, String id) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Member m = null;
		
		String query = prop.getProperty("personInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, id);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				m = new Member();
				
				m.setUserId(rset.getString("USER_ID"));
				m.setUserName(rset.getString("NAME"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return m;
	}

	public Company companyInfo(Connection con, String id) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Company c = null;
		
		String query = prop.getProperty("companyInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, id);
			
			rset = pstmt.executeQuery();
			
			c = new Company();
			
			if(rset.next()) {
				c.setRegistNum(rset.getString("REGIST_NUM"));
				c.setUserId(rset.getString("USER_ID"));
				c.setCompanyName(rset.getString("NAME"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return c;
	}

	public int plusBlackCompany(Connection con, String id, Company c, String content, String email) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("plusBlackCompany");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, c.getCompanyName());
			pstmt.setString(2, content);
			pstmt.setString(3, email);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}

	public int updateMember(Connection con, String id) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateMember");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, id);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}

	public ArrayList<DeclarationList> selectPBList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<DeclarationList> plist = null;
		
		String query = prop.getProperty("selectPBList");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			plist = new ArrayList<DeclarationList>();
			
			while(rset.next()) {
				DeclarationList d = new DeclarationList();
				
				d.setCategory(rset.getInt("CATEGORY"));
				d.setrName(rset.getString("ABNAME"));
				d.setContent(rset.getString("ABWHY"));
				d.setDecDate(rset.getDate("ABDATE"));
				d.setEmail(rset.getString("PEMAIL"));
				
				plist.add(d);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
				
		return plist;
	}

	public ArrayList<DeclarationList> selectCBList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<DeclarationList> clist = null;
		
		String query = prop.getProperty("selectCBList");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			clist = new ArrayList<DeclarationList>();
			
			while(rset.next()) {
				DeclarationList d = new DeclarationList();
				
				d.setCategory(rset.getInt("CATEGORY"));
				d.setrName(rset.getString("ABNAME"));
				d.setContent(rset.getString("ABWHY"));
				d.setDecDate(rset.getDate("ABDATE"));
				d.setcNum(rset.getString("CREGI_NUM"));
				
				clist.add(d);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		return clist;
	}

	public int memberCNumCheck(Connection con, String num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("memberCNumCheck");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, num);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt("UNO");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return result;
	}

	public int memberEmailCheck(Connection con, String email) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("memberEmailCheck");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, email);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt("UNO");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return result;
	}

	public int cancelCBlack(Connection con, String num) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("cancelCBlack");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, num);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int cancelPBlack(Connection con, String email) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("cancelPBlack");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, email);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int updateMemberStatus(Connection con, int memberCheck) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateMemberStatus");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberCheck);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}

	public String selectType(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String type = null;
		
		String query = prop.getProperty("selectType");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				type = rset.getString("CATEGORY");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return type;
	}

	public int checkPType(Connection con, String id, String emailorNum) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("checkPType");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, id);
			pstmt.setString(2, emailorNum);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt("COUNT");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public int checkCType(Connection con, String id, String emailorNum) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("checkCType");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, id);
			pstmt.setString(2, emailorNum);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt("COUNT");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

}