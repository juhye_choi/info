package declaration.model.service;

import static common.JDBCTemplate.close;
import static common.JDBCTemplate.commit;
import static common.JDBCTemplate.getConnection;
import static common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;

import company.model.vo.Company;
import declaration.model.dao.DeclarationDao;
import declaration.model.vo.DeclarationList;
import member.model.vo.Member;

public class DeclarationService {

	public int selectUno(String userId) {
		Connection con = getConnection();
		
		int uno = new DeclarationDao().selectUno(con, userId);
		
		close(con);
		
		return uno;
	}

	public int selectOtherUno(String otherUserId) {
		Connection con = getConnection();
		
		int otherUno = new DeclarationDao().selectOtherUno(con, otherUserId);
		
		close(con);
		
		return otherUno;
	}

	public int insertPersonDeclaration(int uno, int otherUno, String text) {
		Connection con = getConnection();
		
		int result = 0;
		int result1 = new DeclarationDao().insertPersonDeclaration(con, uno, otherUno);
		
		if(result1 > 0) {
			int decId = new DeclarationDao().selectPersonDeclaration(con);
			
			if(decId > 0) {
				result = new DeclarationDao().insertPersonDeclarationHis(con, decId, text);
			}
		}
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		close(con);
		
		return result;
	}

	public ArrayList<DeclarationList> selectDeclarationPersonList() {
		Connection con = getConnection();
		
		ArrayList<DeclarationList> list = new DeclarationDao().selectDeclarationPersonList(con);
		
		close(con);
		
		return list;
	}

	public int DecPersonApproval(int decid, String content) {
		Connection con = getConnection();
		
		//DeclarationList d = new DeclarationDao().selectDeclaration(con, bid);
		//int result = 0;

		int result = new DeclarationDao().DecPersonApproval(con, decid, content);

		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}
	
	public int DecPersonCompanion(int decid, String content) {
		Connection con = getConnection();
		
		int result = new DeclarationDao().DecPersonCompanion(con, decid, content);

		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public ArrayList<DeclarationList> selectWarningList() {
		Connection con = getConnection();
		
		ArrayList<DeclarationList> list = new DeclarationDao().selectWarningList(con);
		
		close(con);
		
		return list;
	}

	public int plusBlackPerson(String id, String email, String content) {
		Connection con = getConnection();
		
		String memberCheck = new DeclarationDao().memberCheck(con, id);
		
		int result = 0; 
		
		if(memberCheck.equals("P")) {
			Member m = new DeclarationDao().personInfo(con, id);
			
			if(m != null) {
				int subResult = new DeclarationDao().plusBlackPerson(con, id, m, content, email);
				
				if(subResult > 0) {
					result = new DeclarationDao().updateMember(con, id);
				}
			}
		}else if(memberCheck.equals("C")){
			Company c = new DeclarationDao().companyInfo(con, id);
			
			if(c != null) {
				int subResult = new DeclarationDao().plusBlackCompany(con, id, c, content, email);

				if(subResult > 0) {
					result = new DeclarationDao().updateMember(con, id);
				}
			}
		}
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public ArrayList<DeclarationList> searchAllDecList() {
		Connection con = getConnection();
		
		ArrayList<DeclarationList> delist = new DeclarationDao().selectAllDecList(con);
		
		close(con);
		
		return delist;
	}

	public ArrayList<DeclarationList> searchCompleteDecList() {
		Connection con = getConnection();
		
		ArrayList<DeclarationList> delist = new DeclarationDao().searchCompleteDecList(con);
		
		close(con);
		
		return delist;
	}

	public ArrayList<DeclarationList> selectPBList() {
		Connection con = getConnection();
		
		ArrayList<DeclarationList> plist = new DeclarationDao().selectPBList(con);
		
		close(con);
		
		return plist;
	}

	public ArrayList<DeclarationList> selectCBList() {
		Connection con = getConnection();
		
		ArrayList<DeclarationList> clist = new DeclarationDao().selectCBList(con);
		
		close(con);
		
		return clist;
	}

	public int cancelCBlack(String num) {
		Connection con = getConnection();
		
		 int result = 0;
		int memberCheck = new DeclarationDao().memberCNumCheck(con, num); 
		
		if(memberCheck > 0) {
			int result1 = new DeclarationDao().cancelCBlack(con, num);
			
			if(result1 > 0) {
				result = new DeclarationDao().updateMemberStatus(con, memberCheck);
			}
		}

		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		return result;
	}

	public int cancelPBlack(String email) {
		Connection con = getConnection();
		
		int memberCheck = new DeclarationDao().memberEmailCheck(con, email); 
		int result = 0;
		
		if(memberCheck > 0) {
			int result1 = new DeclarationDao().cancelPBlack(con, email);
			
			if(result1 > 0) {
				result = new DeclarationDao().updateMemberStatus(con, memberCheck);
			}
		}
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		return result;
	}

	public String selectType(int uno) {
		Connection con = getConnection();
		
		String Type = new DeclarationDao().selectType(con, uno);
		
		close(con);
		
		return Type;
	}

	public int checkType(String type, String id, String emailorNum) {
		Connection con = getConnection();
		int result = 0;
		
		if(type.equals("기업")) {
			result = new DeclarationDao().checkPType(con, id, emailorNum);
		}else if(type.equals("개인")) {
			result = new DeclarationDao().checkCType(con, id, emailorNum);
		}
		
		close(con);
		
		return result;
	}
	
}
