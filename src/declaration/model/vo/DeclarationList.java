package declaration.model.vo;

import java.io.Serializable;
import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


public class DeclarationList implements Serializable {
	private int dId;
	private Date decDate;
	private int rUno;
	private String rUserId;
	private String rName;
	private int gUno;
	private String gUserid;
	private String gName;
	private String Email;
	private String content;
	private int category;
	private int count;
	private String cNum;
	
	public DeclarationList() {}

	
	public DeclarationList(int dId, Date decDate, int rUno, String rUserId, String rName, int gUno, String gUserid,
			String gName, String email, String content, int category, int count) {
		super();
		this.dId = dId;
		this.decDate = decDate;
		this.rUno = rUno;
		this.rUserId = rUserId;
		this.rName = rName;
		this.gUno = gUno;
		this.gUserid = gUserid;
		this.gName = gName;
		this.Email = email;
		this.content = content;
		this.category = category;
		this.count = count;
	}

	public String getcNum() {
		return cNum;
	}


	public void setcNum(String cNum) {
		this.cNum = cNum;
	}


	public int getCount() {
		return count;
	}


	public void setCount(int count) {
		this.count = count;
	}



	public int getdId() {
		return dId;
	}

	public void setdId(int dId) {
		this.dId = dId;
	}

	public Date getDecDate() {
		return decDate;
	}

	public void setDecDate(Date decDate) {
		this.decDate = decDate;
	}

	public int getrUno() {
		return rUno;
	}

	public void setrUno(int rUno) {
		this.rUno = rUno;
	}

	public String getrUserId() {
		return rUserId;
	}

	public void setrUserId(String rUserId) {
		this.rUserId = rUserId;
	}

	public String getrName() {
		return rName;
	}

	public void setrName(String rName) {
		this.rName = rName;
	}

	public int getgUno() {
		return gUno;
	}

	public void setgUno(int gUno) {
		this.gUno = gUno;
	}

	public String getgUserid() {
		return gUserid;
	}

	public void setgUserid(String gUserid) {
		this.gUserid = gUserid;
	}

	public String getgName() {
		return gName;
	}

	public void setgName(String gName) {
		this.gName = gName;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "DeclarationList [dId=" + dId + ", decDate=" + decDate + ", rUno=" + rUno + ", rUserId=" + rUserId
				+ ", rName=" + rName + ", gUno=" + gUno + ", gUserid=" + gUserid + ", gName=" + gName + ", Email="
				+ Email + ", content=" + content + ", category=" + category + "]";
	}

}