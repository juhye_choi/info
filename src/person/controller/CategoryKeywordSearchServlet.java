package person.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import keyword.model.vo.Keyword;
import member.model.service.MemberService;
import member.model.vo.Member;
import person.model.service.PersonService;
import person.model.vo.Person;

/**
 * Servlet implementation class CategoryKeywordSearchServlet
 */
@WebServlet("/catekeywordSearch.re")
public class CategoryKeywordSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CategoryKeywordSearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String keyword = (String) request.getParameter("key");
		String searchKeyword = (String) request.getParameter("keyword");
		ArrayList<Person> list = null;
		int uno = 0;
		String banner = null;
		
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		banner = new MemberService().selectBanner();
		
		if(loginUser != null) {
			uno = loginUser.getUno();
		}
		
		if(uno > 0) {
			if(keyword != null) {
				list = new PersonService().pSearchKeyResume(keyword, uno);
			}else if(searchKeyword != null) {
				list = new PersonService().pSearchContainKeyResume(searchKeyword, uno);
			}
		}else {
			if(keyword != null) {
				list = new PersonService().searchKeyResume(keyword);
			}else if(searchKeyword != null) {
				list = new PersonService().searchContainKeyResume(searchKeyword);
			}
		}

		String page = ""; 
		if(list != null) {
			ArrayList<Person> rlist = null;
			ArrayList<Keyword> klist =  new PersonService().selectKeyword(searchKeyword);
			
			if(uno > 0) {
				if(keyword != null) {
					rlist = new PersonService().pSearchKeyRefResume(keyword);
				}else if(searchKeyword != null) {
					rlist = new PersonService().pSearchContainKeyRefResume(searchKeyword);
				}
			}else {
				if(keyword != null) {
					rlist = new PersonService().searchKeyRefResume(keyword);
				}else if(searchKeyword != null) {
					rlist = new PersonService().searchContainKeyRefResume(searchKeyword);
				}
			}
			
			if(rlist != null) {
				page = "views/person/keywordResumeSearch.jsp";
				request.setAttribute("list", list);
				request.setAttribute("rlist", rlist);
				request.setAttribute("keyword", keyword);
				request.setAttribute("klist", klist);
				request.setAttribute("banner", banner);
			}else {
				page = "views/person/keywordResumeSearch.jsp";
				request.setAttribute("list", list);
				request.setAttribute("rlist", rlist);
				request.setAttribute("keyword", keyword);
				request.setAttribute("klist", klist);
				request.setAttribute("banner", banner);
			}
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "이력서 리스트 조회 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
