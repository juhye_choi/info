package person.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import person.model.service.PointService;


@WebServlet("/selectOne.po")
public class SelectMyPageInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public SelectMyPageInfoServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		//request.setCharacterEncoding("UTF-8");

		int uno = 0;
		if(request.getSession().getAttribute("loginUser") == null){
			request.setAttribute("msg", "로그인이 필요합니다.");
			request.getRequestDispatcher("views/common/needLogin.jsp").forward(request, response);
		}else {
		//회원 번호 가져오기
		uno = ((Member) request.getSession().getAttribute("loginUser")).getUno();
		//System.out.println(uno);
		
		
		
		ArrayList<Integer> list = new PointService().selectOneInfo(uno);
		
		if(list != null) {
			if(list.size() == 8) {
				HashMap<String,Integer> ihmap = new HashMap<>();
				//System.out.println(list);
				//1.기본정보 다 들어갔는지 체크! 맞으면1, 틀리면0
				ihmap.put("basic", list.get(0));
				//2. 프로젝트 개수
				ihmap.put("pjtNum", list.get(1));
				//3. 경력 개수
				ihmap.put("carNum", list.get(2));
				//4. 학력+교육+자격증 수
				ihmap.put("eduNum", list.get(3));
				//5. 포인트 수
				ihmap.put("point", list.get(4));
				//6. 추천포폴 신청 여부(신청 개수)
				ihmap.put("refReq", list.get(5));
				//7. 뱃지(추천포폴 등록 허가 수) 수
				ihmap.put("refAcc", list.get(6));
				//8. 출첵 여부
				ihmap.put("dailyCheck", list.get(7));
				
				request.setAttribute("ihmap", ihmap);
				request.getRequestDispatcher("views/person/myPage.jsp").forward(request, response);
			} else {
				request.setAttribute("msg", "이력서/기본정보를 업데이트 해주세요!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);

			}
		} else {
			request.setAttribute("msg", "회원정보를 가져올 수 없습니다.");
			request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
			
		}
		}
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
