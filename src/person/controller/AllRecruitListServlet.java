package person.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import person.model.service.RecruitInfoService;
import person.model.vo.PageInfo;
import person.model.vo.RecruitInfo;

@WebServlet("/allRecruit")
public class AllRecruitListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AllRecruitListServlet() {
        super();
        // TODO Auto-generated  constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int currentPage;	//현재 페이지를 표시할 변수
		int limit;			//한 페이지에 게시글이 몇 개 보여질 것인지
		int maxPage;		//전체 페이지에서 가장 마지막 페이지
		
		currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		//한 페이지에 보여질 목록 갯수
		limit = 8;
		
		int listCount = new RecruitInfoService().getListCount();
		//System.out.println("전체 공고 갯수(서블릿) : " + listCount);
		
		maxPage = (int) Math.ceil((double)listCount / (double)limit);
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage);
		
		ArrayList<RecruitInfo> list = new RecruitInfoService().selectListWithPaging(currentPage, limit);
		
		String page = "";
		if(list != null) {
			page = "views/person/recruitList.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "게시판 조회 실패!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
