package person.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import person.model.service.PointService;


@WebServlet("/insertDaily.po")
public class InsertDailyCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public InsertDailyCheckServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		int uno = ((Member) request.getSession().getAttribute("loginUser")).getUno();
		
		int result = new PointService().insertDailyCheck(uno);
		
		response.getWriter().print(result);
		
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
