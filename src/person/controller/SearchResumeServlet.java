package person.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.service.MemberService;
import member.model.vo.Member;
import person.model.service.PersonService;
import person.model.vo.Person;

@WebServlet("/searchResume.sr")
public class SearchResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SearchResumeServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		Person p = null;
		int uno = 0;
		
		String banner = null;
		banner = new MemberService().selectBanner();
		
		if(loginUser != null) {
			uno = loginUser.getUno();
			p = new PersonService().myResume(uno);
		}
		
		String page = "";
		if(p != null){
			ArrayList<Person> list = new PersonService().searchResume(uno);
			
			if(list != null) {
				ArrayList<Person> rlist = new PersonService().searchRefResume();
				
				if(rlist != null) {
					page = "views/person/resumeSearch.jsp";
					request.setAttribute("list", list);
					request.setAttribute("p", p);
					request.setAttribute("rlist", rlist);
					request.setAttribute("banner", banner);
				}else {
					page = "views/common/errorPage.jsp";
					request.setAttribute("msg", "추천 이력서 리스트 조회 실패!");
				}
			}else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "이력서조회실패!");
			}
		}else {
			ArrayList<Person> list = new PersonService().searchResume(uno);
			
			if(list != null) {
				ArrayList<Person> rlist = new PersonService().searchRefResume();
				
				if(rlist != null) {
						page = "views/person/resumeSearch.jsp";
						request.setAttribute("list", list);
						request.setAttribute("p", p);
						request.setAttribute("rlist", rlist);
						request.setAttribute("banner", banner);
				}else {
					page = "views/common/errorPage.jsp";
					request.setAttribute("msg", "추천 이력서 리스트 조회 실패!");
				}
			}else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "이력서조회실패!");
			}
		}
		request.getRequestDispatcher(page).forward(request, response);
	} 

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
