package person.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import person.model.service.PersonService;

@WebServlet("/addBlack.ps")
public class AddPersonBlacklistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AddPersonBlacklistServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		int cuno = Integer.parseInt(request.getParameter("CUNO"));
		int uno = loginUser.getUno();
		int result = 0;
		result = new PersonService().addBlacklist(uno, cuno);
		
		String page = "";

		if(result > 0) {
			page="/intro.prs?CUNO=" + cuno;
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "블랙리스트 등록 실패!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
