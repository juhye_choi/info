package person.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import person.model.service.PersonService;
import resume.model.service.ResumeService;
import resume.model.vo.ProjectCareer;

/**
 * Servlet implementation class PersonResumePayServlet
 */
@WebServlet("/personPay.re")
public class PersonResumePayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PersonResumePayServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int uno = 0;
		int otherUno = 0;
		int pjtId = 0;
		
		if(request.getSession().getAttribute("loginUser") != null) {
			uno = ((Member) request.getSession().getAttribute("loginUser")).getUno();
			otherUno = Integer.parseInt(request.getParameter("OtherUno"));
			pjtId = Integer.parseInt(request.getParameter("pjtId"));
		}else {
			request.setAttribute("msg", "잘못된 경로로 접근하셨습니다.");
			request.getRequestDispatcher("views/common/needLogin.jsp").forward(request, response);
		}
		
		int point = new PersonService().selectPersonPoint(uno);
		
		String page = "";
		
		if(point > 0) {
			int result = new PersonService().payPoint(uno, otherUno);
			
			if(result > 0) {
				ProjectCareer pc = new ResumeService().selectProjectCareer(pjtId);
				
				long diff = pc.getPjtFinish().getTime() - pc.getPjtStart().getTime();
			    int diffDays = (int) (diff/(24*60*60*1000));
			    
			    page = "views/person/otherRecruit.jsp";
				request.setAttribute("pc", pc);
				request.setAttribute("diffDays", diffDays);
			}else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "포인트 결제 실패");
			}
		}else {
			page = "/selectList.po";
			request.setAttribute("msg", "회원 포인트 조회 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
