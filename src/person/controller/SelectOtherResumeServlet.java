package person.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyService;
import member.model.vo.Member;
import person.model.service.PersonService;
import person.model.vo.Attachment2;
import resume.model.service.ResumeService;
import resume.model.vo.Resume;


@WebServlet("/selectOther.re")
public class SelectOtherResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public SelectOtherResumeServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		int uno = 0;
		int otherUno = 0;
		String type = null;
		String ChangeName = null;
		
		if(request.getSession().getAttribute("loginUser") != null){
			uno = ((Member) request.getSession().getAttribute("loginUser")).getUno();
			otherUno = Integer.parseInt(request.getParameter("otherUno"));
			ChangeName = (String) request.getParameter("ChangeName");
			type = ((Member) request.getSession().getAttribute("loginUser")).getCategory();
		} else {
			request.setAttribute("msg", "로그인이 필요한 메뉴입니다.");
			request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		}
		
		// 기업인지 개인인지 확인
		int view = 0;
		
		//1. 구매이력이 있는지 확인 : view=1이면 구매이력O, view=0이면 구매이력X
		view = new PersonService().checkPointHistory(uno, otherUno);
		
		if(uno == otherUno) {
			view = 1;
		}
		
		ArrayList<Attachment2> atlist = new PersonService().selectImg(otherUno);
		ChangeName = atlist.get(0).getChangeName();

		
		String page = "";
		
		//이력서와 해당 사람의 정보를 가져오기!
		Resume resume = new ResumeService().selectResume(otherUno);
		HashMap<String, String> personInfo = new PersonService().selectPerson(otherUno);
		
		if(personInfo != null || resume != null) {
			HashMap<String, Object> hmap = new HashMap<>();
			hmap.put("uno", uno);
			hmap.put("otherUno", otherUno);
			hmap.put("name", personInfo.get("name"));
			hmap.put("email", personInfo.get("email"));
			hmap.put("phone", personInfo.get("phone"));
			hmap.put("jobStatus", personInfo.get("jobStatus"));
			hmap.put("resume", resume);
			hmap.put("view", view);
			hmap.put("ChangeName", ChangeName);
			
			String infoOpen = new PersonService().selectInfoOpen(otherUno);
			
			 if(infoOpen != null) {
				 page = "views/person/otherResume.jsp";
				 request.setAttribute("hmap", hmap);
				 request.setAttribute("infoOpen", infoOpen);
				 request.setAttribute("type", type);
				 request.getRequestDispatcher(page).forward(request, response);
			 }else {
				 page = "views/common/errorPage.jsp";
				 request.setAttribute("msg", "정보 공개여부 조회 실패!");
				 request.getRequestDispatcher(page).forward(request, response);
			 }
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "이력서 조회에 실패했습니다.");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
