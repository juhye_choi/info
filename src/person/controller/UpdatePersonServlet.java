package person.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.oreilly.servlet.MultipartRequest;

import common.MyFileRenamePolicy;
import member.model.service.MemberService;
import member.model.vo.Member;
import person.model.service.PersonService;
import person.model.vo.Attachment2;
import person.model.vo.Person;

/**
 * Servlet implementation class UpdatePersonServlet
 */
@WebServlet("/updatePerson.me")
public class UpdatePersonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdatePersonServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(ServletFileUpload.isMultipartContent(request)) {
			
			int maxSize = 1024 * 1024 * 10;
			String root = request.getSession().getServletContext().getRealPath("/");
			
			
			String savePath = root + "thumbnail_uploadFiles/";
			
			MultipartRequest multiRequest = new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
		
			ArrayList<String> saveFiles = new ArrayList<String>();
			ArrayList<String> originFiles = new ArrayList<String>();
			Enumeration<String> files = multiRequest.getFileNames();
			
		
			
			while(files.hasMoreElements()) {
				
				
				String name = files.nextElement();
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
				
			}
			
			ArrayList<Attachment2> fileList = new ArrayList<>();
			ArrayList<Attachment2> list = (ArrayList<Attachment2>) request.getSession().getAttribute("fileList");
			
			
			
for(int i = originFiles.size() -1; i >=0; i--) {
				
	
				
				//첨부한 사진 insert
				Attachment2 at = new Attachment2();
				at.setFilePath(savePath);
				at.setOriginName(originFiles.get(i));
				at.setChangeName(saveFiles.get(i));
				
				
				
				fileList.add(at);
				
				
				
				
				// 사진 업데이트 안했을때 세션에 저장된 사진 insert하게
				if(fileList.get(0).getOriginName() == null) {
			
					
					at.setFilePath(list.get(0).getFilePath());
					at.setOriginName(list.get(0).getOriginName());
					at.setChangeName(list.get(0).getChangeName());
					
					fileList.add(at);
					
				}
			}




			
			
			
		int uno = Integer.parseInt(multiRequest.getParameter("uno"));
			
			
		
		
		//기본정보 테이블에 들어갈 데이터
		String tel1 = multiRequest.getParameter("tel1");
		String tel2 = multiRequest.getParameter("tel2");
		String tel3 = multiRequest.getParameter("tel3");
		String phone = tel1 + "-"+tel2 + "-"+tel3;
		

		
		Member requestMember = new Member();
		
		
Member loginUser = (Member)request.getSession().getAttribute("loginUser");
Person person = (Person)request.getSession().getAttribute("person");


		
		requestMember.setUno(uno);
		if(!phone.equals("--")) {
		requestMember.setPhone(phone);
		loginUser.setPhone(phone);
		}else {
			requestMember.setPhone(loginUser.getPhone());
			
		}
	
		
		///////////////////////////////////////////////////Member테이블 업데이트 데이터
	
		String birth1 = multiRequest.getParameter("birth1");
		
		String birth2 = multiRequest.getParameter("birth2");
		String birth3 = multiRequest.getParameter("birth3");
		String birth = birth1 + "-"+birth2 + "-"+birth3;
		

	
		String gender = multiRequest.getParameter("gender");
		
		
		String publicInfo = multiRequest.getParameter("publicInfo");
	
		String JobStatus = multiRequest.getParameter("JobStatus");
	
		String github = multiRequest.getParameter("github");
		
		String blog = multiRequest.getParameter("blog");
		
		String kosa = multiRequest.getParameter("kosa");
		
		
		
		Person requestPerson = new Person();
		
		requestPerson.setUno(uno);
		if(!birth.equals("--")) {
		requestPerson.setBirth(Date.valueOf(birth));
		}else {
			requestPerson.setBirth(person.getBirth());
		}
		
		if(gender != null) {
		requestPerson.setGendder(gender);
		}else {
			requestPerson.setGendder(person.getGendder());
		}
		
		if(publicInfo != null) {
		requestPerson.setInfoOpen(publicInfo);
		} else {
			requestPerson.setInfoOpen(person.getInfoOpen());
		}
		
		if(JobStatus != null) {
		requestPerson.setJobStatus(JobStatus);
		}else {
			requestPerson.setJobStatus(person.getJobStatus());
			}
		
		if(github != null) {
		requestPerson.setGithub(github);
		} else {
			requestPerson.setGithub(person.getGithub());
		}
		if(blog != null) {
		requestPerson.setBlog(blog);
		} else {
			requestPerson.setBlog(person.getBlog());
		}
		if(kosa != null) {
		requestPerson.setKosaYN(kosa);
		} else {
			requestPerson.setKosaYN(person.getKosaYN());
		}
//////////////////////////////////////////////PERSON_ADD 테이블에 업데이트 데이터
		
		int result1 = new MemberService().updateMember(requestMember,requestPerson,fileList);
		
	
		
		
		String page="";
		
		
		
		if(result1 > 0) {
			
			request.getSession().setAttribute("loginUser", loginUser);
			request.getSession().setAttribute("fileList", fileList);
		
			response.sendRedirect("selectOne.po");
			
			
			
		}else {
			
			for(int i=0; i<saveFiles.size(); i++) {
				File failedFile = new File(savePath +saveFiles.get(i));
				
				failedFile.delete();
			}
			
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "개인회원 추가정보 업데이트 실패");
			RequestDispatcher view = request.getRequestDispatcher(page);
			view.forward(request, response);
			
			
		}
		
		
		
		
		
		}
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
