package person.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import person.model.service.PointService;
import person.model.vo.PointHistory;

@WebServlet("/insertPay.po")
public class InsertPointPayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
    public InsertPointPayServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String payName = request.getParameter("payname");
		String userName= request.getParameter("userName");
		int uno = Integer.parseInt(request.getParameter("uno"));
		int amount = Integer.parseInt(request.getParameter("amount"));
		
//		System.out.println("payName : " + payName);
//		System.out.println("userName : " + userName);
//		System.out.println("uno : " + uno);
		System.out.println("amount : " + amount);
		
		PointHistory ph = new PointHistory();
		ph.setUno(uno);
		ph.setPayPoint(amount);
		ph.setPoint(amount/100);
		ph.setPoContent(payName + amount);
		
		int result = new PointService().insertPointPay(ph);
		
		String page = "";
		if(result>0) {
			page = "selectList.po";
			response.sendRedirect(page);
					
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "결제에 실패하셨습니다.");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
