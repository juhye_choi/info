package person.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import person.model.service.PersonService;
import person.model.vo.Person;

/**
 * Servlet implementation class PersonInfo
 */
@WebServlet("/personInfo.me")
public class PersonInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PersonInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	
		
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		
		int uno = loginUser.getUno();
		
		Person person = null;
		
		person = new PersonService().selectPersonInfo(uno);
		
		String page="";
		
		//추가정보가 있는 사람(널이 아닌사람)은 바로 원래있던 정보 띄어주고
		if(person != null) {
			
			page = "views/person/personInformation2.jsp";
			request.setAttribute("person", person);
		
			request.getSession().setAttribute("person", person);
			request.getRequestDispatcher(page).forward(request, response);
			
			
		}else {
			//처음들어온사람(추가정보없는사람)은 널값으로 insert한번해주자!
			int result = new PersonService().insertPersonInfo(uno);
			
			if(result > 0) {
			
				request.getRequestDispatcher("/personInfo.me").forward(request,response);
			}else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "개인정보 조회에 실패하였습니다.");
				request.getRequestDispatcher(page).forward(request, response);
				
				
			}
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
