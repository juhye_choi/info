package person.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import person.model.service.PersonService;

/**
 * Servlet implementation class SelectRefResumeServlet
 */
@WebServlet("/selectResume.rg")
public class SelectRefResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectRefResumeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		
		int uno = 0;
		//로그인 했을 때
		if(loginUser != null) {
			uno = loginUser.getUno();
		
			String quarter = new PersonService().selectquarter();
		
			String page ="";
			if(quarter != null) {
				String result = new PersonService().RefSumeYN(uno, quarter);
				System.out.println(result);
				//이미 신청했을 때
				page = "views/person/resumeGuide.jsp";
				request.setAttribute("result", result);
				
			}else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "분기 조회실패");
			}
			
			request.getRequestDispatcher(page).forward(request, response);
		// 비로그인 일 때	
		}else {
			String quarter = new PersonService().selectquarter();
			String page ="";
			if(quarter != null) {
				page = "views/person/resumeGuide.jsp";
				request.setAttribute("quarter", quarter);
				request.setAttribute("result", "NotLogin");
			}else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "분기 조회실패");
			}
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
