package person.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import keyword.model.vo.Keyword2;
import person.model.service.PersonService;

/**
 * Servlet implementation class DetailSearchKeyworkServlet
 */
@WebServlet("/detailSearch.rs")
public class DetailSearchKeyworkServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailSearchKeyworkServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int buttonNum = Integer.parseInt(request.getParameter("button"));
		ArrayList<Keyword2> list = null;
		
		list = new PersonService().selectDetailKeyword(buttonNum);
		
		PrintWriter out = response.getWriter();
		JSONObject kname = null;
		JSONObject result = new JSONObject();
		JSONArray keyArray = new JSONArray();
		
		if(list != null) {
			for(Keyword2 k2 : list) {
				kname = new JSONObject();
				kname.put("KNAME", URLEncoder.encode(k2.getKname(), "UTF-8"));
				
				keyArray.add(kname);
			}
		}	
		response.setContentType("application/json");
		out.print(keyArray.toJSONString());
		out.flush();
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
