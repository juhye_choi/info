package person.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.vo.Company;
import company.model.vo.Recruit;
import member.model.vo.Member;
import person.model.service.RecruitInfoService;

/**
 * Servlet implementation class SelectOneRecruit
 */
@WebServlet("/recruit.so")
public class SelectOneRecruit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectOneRecruit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		int uno = 0;
		int result = 0;
		if(loginUser != null) {
			uno = loginUser.getUno();
		}
		
		
		int recid = Integer.parseInt(request.getParameter("RECID"));
		Recruit recruit = new RecruitInfoService().selectOneRecruit(recid);
		Company company = new RecruitInfoService().selectOneCompany(recruit.getUno());
		
		int type = Integer.parseInt(request.getParameter("type"));
		
		
		String page = "";
		if(company != null) {
			
			if(type==1) {
				if(uno > 0) {
					result = new RecruitInfoService().readRecruit(uno, recid);
				}
				page = "views/person/recruit.jsp";
				request.setAttribute("recruit", recruit);
				request.setAttribute("company", company);
				request.setAttribute("check", result);
			}else if(type==2) {
				page = "views/admin/recruitOne.jsp";
				request.setAttribute("recruit", recruit);
				request.setAttribute("company", company);
			}
			
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "채용공고 조회 실패!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
