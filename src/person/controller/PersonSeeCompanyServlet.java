package person.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import company.model.service.CompanyService;
import company.model.vo.Company;
import company.model.vo.Recruit;
import member.model.vo.Member;
import person.model.service.PersonService;

@WebServlet("/intro.prs")
public class PersonSeeCompanyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PersonSeeCompanyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String page = "";
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		int uno = 0;
		if(loginUser != null) {
			uno = loginUser.getUno();
		}
		int cuno = Integer.parseInt(request.getParameter("CUNO"));
		int result = 0;
		Company company = new Company();
		
		company = new CompanyService().selectMember(cuno);
		ArrayList<Recruit> recList = new CompanyService().selectIngRec(cuno);
		result = new PersonService().selectBlacklist(uno, cuno);
		if(company != null) {
			page="views/person/CompanyInfoP.jsp";
			request.setAttribute("company", company);
			request.setAttribute("recList", recList);
			request.setAttribute("blacklist", result);
		} else {
			page = "views/common/companyErrorPage.jsp";
			request.setAttribute("msg", "오류!!");
		}
			
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
