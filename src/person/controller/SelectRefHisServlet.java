package person.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import person.model.service.PersonService;
import person.model.vo.RecommendResume;

/**
 * Servlet implementation class SelectRefHisServlet
 */
@WebServlet("/selectRecommendResume.rh")
public class SelectRefHisServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectRefHisServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		
		if(loginUser == null) {
			request.setAttribute("msg", "로그인이 필요한 메뉴입니다");
			request.getRequestDispatcher("views/common/needLogin.jsp").forward(request, response);
		}
		
		int uno = loginUser.getUno();
		
		ArrayList<RecommendResume> list = new PersonService().selectRefHis(uno);
		String page ="";
		if(list != null) {
			int result = new PersonService().selectAppCount(uno);
			String quarter = new PersonService().selectquarter();
			String Result = new PersonService().RefSumeYN(uno, quarter);
			
			String count = result + "";
			
			page = "views/person/recommendedResume.jsp";
			request.setAttribute("count", count);
			request.setAttribute("list", list);
			request.setAttribute("Result", Result);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "추천이력서 내역 조회 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
