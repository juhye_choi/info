package person.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import resume.model.service.ResumeService;
import resume.model.vo.ProjectCareer;

/**
 * Servlet implementation class SelectOtherRecruitServlet
 */
@WebServlet("/otherRecruit.re")
public class SelectOtherRecruitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectOtherRecruitServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int uno = 0;
		String ChangeName = null;
		int otherUno = 0;
		int pjtId = 0;
		
		if(request.getSession().getAttribute("loginUser") != null) {
			uno = ((Member) request.getSession().getAttribute("loginUser")).getUno();
			otherUno = Integer.parseInt(request.getParameter("otherUno"));
			pjtId = Integer.parseInt(request.getParameter("PjtId"));
			ChangeName = (String) request.getParameter("ChangeName");
		}else {
			request.setAttribute("msg", "로그인이 필요한 메뉴입니다.");
			request.getRequestDispatcher("views/common/needLogin.jsp").forward(request, response);
		}
		
		String page = "";
		
		int result = new ResumeService().checkProject(uno, pjtId);

		if(result > 0) {
			ProjectCareer pc = new ResumeService().selectProjectCareer(pjtId);
			
			long diff = pc.getPjtFinish().getTime() - pc.getPjtStart().getTime();
		    int diffDays = (int) (diff/(24*60*60*1000));
		    
			page = "views/person/otherRecruit.jsp";
			request.setAttribute("pc", pc);
			request.setAttribute("diffDays", diffDays);
			request.setAttribute("ChangeName", ChangeName);
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "프로젝트 아이디 조회 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
