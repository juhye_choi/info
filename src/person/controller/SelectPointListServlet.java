package person.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import keyword.model.vo.PageInfo;
import member.model.vo.Member;
import person.model.service.PointService;
import person.model.vo.PointHistory;

@WebServlet("/selectList.po")
public class SelectPointListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public SelectPointListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		int uno = 0;
		if(request.getSession().getAttribute("loginUser") != null) {
			uno = ((Member) request.getSession().getAttribute("loginUser")).getUno();
		
		
		//페이징 처리
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;

		currentPage=1;

		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		limit = 10;

		int listCount = new PointService().getListCount(uno);

		maxPage = (int)((double)listCount/limit + 0.9);
		startPage = (((int)((double) currentPage/limit+0.9))-1) * 10 + 1;

		endPage = startPage + 10 -1;
		if(maxPage < endPage) {
			endPage = maxPage;
		}

		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		ArrayList<PointHistory> polist = new PointService().selectPointList(uno, pi);
		
		String page = "";
		if(polist != null) {
			page = "views/person/pointPay.jsp";
			request.setAttribute("polist", polist);
			request.setAttribute("pi", pi);

		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "조회에 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
		} else {
			request.setAttribute("msg", "로그인이 필요합니다.");
			request.getRequestDispatcher("views/common/needLogin.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
