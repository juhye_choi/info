package person.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import person.model.service.PersonService;

/**
 * Servlet implementation class InsertRefResumeServlet
 */
@WebServlet("/insertRef.rg")
public class InsertRefResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertRefResumeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		
		int uno = loginUser.getUno();
		String quarter = new PersonService().selectquarter();
		String result = new PersonService().RefSumeYN(uno, quarter);
		
		String page = "";
			
			int resultResume = new PersonService().intsertRefResume(uno, quarter);
			
			if(resultResume > 0) {
				response.sendRedirect("/info/selectResume.rg");
			}else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "추천이력서 신청 실패!");
				request.getRequestDispatcher(page).forward(request, response);
			}
			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
