package person.model.vo;

import java.io.Serializable;
import java.sql.Date;

public class Attachment2 implements Serializable{
	
	private int atId;
	private String originName;
	private String changeName;
	private String filePath;
	private Date atDate;
	private String removeYN;
	private Date removeDate;
	private int category;
	private int fileLevel;
	
	private int proUno;
	
	
	public  Attachment2() {}

	public Attachment2(int atId, String originName, String changeName, String filePath, Date atDate, String removeYN,
			Date removeDate, int category, int fileLevel, int proUno) {
		super();
		this.atId = atId;
		this.originName = originName;
		this.changeName = changeName;
		this.filePath = filePath;
		this.atDate = atDate;
		this.removeYN = removeYN;
		this.removeDate = removeDate;
		this.category = category;
		this.fileLevel = fileLevel;
		this.proUno = proUno;
		
	}

	public int getAtId() {
		return atId;
	}

	public void setAtId(int atId) {
		this.atId = atId;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getChangeName() {
		return changeName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getAtDate() {
		return atDate;
	}

	public void setAtDate(Date atDate) {
		this.atDate = atDate;
	}

	public String getRemoveYN() {
		return removeYN;
	}

	public void setRemoveYN(String removeYN) {
		this.removeYN = removeYN;
	}

	public Date getRemoveDate() {
		return removeDate;
	}

	public void setRemoveDate(Date removeDate) {
		this.removeDate = removeDate;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getFileLevel() {
		return fileLevel;
	}

	public void setFileLevel(int fileLevel) {
		this.fileLevel = fileLevel;
	}

	public int getProUno() {
		return proUno;
	}

	public void setProUno(int proUno) {
		this.proUno = proUno;
	}

	@Override
	public String toString() {
		return "Attachment2 [atId=" + atId + ", originName=" + originName + ", changeName=" + changeName + ", filePath="
				+ filePath + ", atDate=" + atDate + ", removeYN=" + removeYN + ", removeDate=" + removeDate
				+ ", category=" + category + ", fileLevel=" + fileLevel + ", proUno=" + proUno + "]";
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
