package person.model.vo;

import java.sql.Date;

import resume.model.vo.Attachment;

public class Person {
	private int uno;
	private Date birth;
	private String gendder;
	private int point;
	private String infoOpen;
	private String jobStatus;
	private String github;
	private String blog;
	private String kosaYN;
	private String userid;
	private String name;
	private String count;
	private Attachment picture;
	
	public Person() {}
	
	public Person(int uno, String userid, String name, String count) {
		super();
		this.uno = uno;
		this.userid = userid;
		this.name = name;
		this.count = count;
	}

	
	public Person(int uno, Date birth, String gendder, int point, String infoOpen, String jobStatus, String github,
			String blog, String kosaYN, String userid, String name, String count) {
		super();
		this.uno = uno;
		this.birth = birth;
		this.gendder = gendder;
		this.point = point;
		this.infoOpen = infoOpen;
		this.jobStatus = jobStatus;
		this.github = github;
		this.blog = blog;
		this.kosaYN = kosaYN;
		this.userid = userid;
		this.name = name;
		this.count = count;
	}

	public Attachment getPicture() {
		return picture;
	}

	public void setPicture(Attachment picture) {
		this.picture = picture;
	}

	public String getKosaYN() {
		return kosaYN;
	}

	public void setKosaYN(String kosaYN) {
		this.kosaYN = kosaYN;
	}



	public int getUno() {
		return uno;
	}



	public void setUno(int uno) {
		this.uno = uno;
	}



	public Date getBirth() {
		return birth;
	}



	public void setBirth(Date birth) {
		this.birth = birth;
	}



	public String getGendder() {
		return gendder;
	}



	public void setGendder(String gendder) {
		this.gendder = gendder;
	}



	public int getPoint() {
		return point;
	}



	public void setPoint(int point) {
		this.point = point;
	}



	public String getInfoOpen() {
		return infoOpen;
	}



	public void setInfoOpen(String infoOpen) {
		this.infoOpen = infoOpen;
	}



	public String getJobStatus() {
		return jobStatus;
	}



	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}



	public String getGithub() {
		return github;
	}



	public void setGithub(String github) {
		this.github = github;
	}



	public String getBlog() {
		return blog;
	}



	public void setBlog(String blog) {
		this.blog = blog;
	}



	public String getUserid() {
		return userid;
	}



	public void setUserid(String userid) {
		this.userid = userid;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getCount() {
		return count;
	}



	public void setCount(String count) {
		this.count = count;
	}



	@Override
	public String toString() {
		return "Person [uno=" + uno + ", birth=" + birth + ", gendder=" + gendder + ", point=" + point + ", infoOpen="
				+ infoOpen + ", jobStatus=" + jobStatus + ", KOSA_YN=" + kosaYN +  ", github=" + github + ", blog=" + blog + ", userid=" + userid
				+ ", name=" + name + ", count=" + count + "]";
	}
	
}