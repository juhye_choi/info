package person.model.vo;

public class RePageInfo {
	
	private int reCurrentPage;
	private int reListCount;
	private int limit;
	private int reMaxPage;
	private int reStartPage;
	private int reEndPage;
	
	public RePageInfo() {}

	public RePageInfo(int reCurrentPage, int reListCount, int limit, int reMaxPage, int reStartPage, int reEndPage) {
		super();
		this.reCurrentPage = reCurrentPage;
		this.reListCount = reListCount;
		this.limit = limit;
		this.reMaxPage = reMaxPage;
		this.reStartPage = reStartPage;
		this.reEndPage = reEndPage;
	}

	public int getReCurrentPage() {
		return reCurrentPage;
	}

	public void setReCurrentPage(int reCurrentPage) {
		this.reCurrentPage = reCurrentPage;
	}

	public int getReListCount() {
		return reListCount;
	}

	public void setReListCount(int reListCount) {
		this.reListCount = reListCount;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getReMaxPage() {
		return reMaxPage;
	}

	public void setReMaxPage(int reMaxPage) {
		this.reMaxPage = reMaxPage;
	}

	public int getReStartPage() {
		return reStartPage;
	}

	public void setReStartPage(int reStartPage) {
		this.reStartPage = reStartPage;
	}

	public int getReEndPage() {
		return reEndPage;
	}

	public void setReEndPage(int reEndPage) {
		this.reEndPage = reEndPage;
	}

}