package person.model.vo;

import java.sql.Date;

public class PointHistory implements java.io.Serializable{
	private int pointId;
	private String category;
	private Date poDate;
	private String poContent;
	private int payUno;
	private String payName;
	private int payPoint;
	private int point;
	private int uno;
	
	public PointHistory() {}

	

	public PointHistory(int pointId, String category, Date poDate, String poContent, int payUno, String payName,
			int payPoint, int point, int uno) {
		super();
		this.pointId = pointId;
		this.category = category;
		this.poDate = poDate;
		this.poContent = poContent;
		this.payUno = payUno;
		this.payName = payName;
		this.payPoint = payPoint;
		this.point = point;
		this.uno = uno;
	}



	public int getPointId() {
		return pointId;
	}

	public void setPointId(int pointId) {
		this.pointId = pointId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getPoDate() {
		return poDate;
	}

	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}

	public String getPoContent() {
		return poContent;
	}

	public void setPoContent(String poContent) {
		this.poContent = poContent;
	}

	public int getPayUno() {
		return payUno;
	}

	public void setPayUno(int payUno) {
		this.payUno = payUno;
	}

	public int getPayPoint() {
		return payPoint;
	}

	public void setPayPoint(int payPoint) {
		this.payPoint = payPoint;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public int getUno() {
		return uno;
	}

	public void setUno(int uno) {
		this.uno = uno;
	}

	
	
	public String getPayName() {
		return payName;
	}



	public void setPayName(String payName) {
		this.payName = payName;
	}



	@Override
	public String toString() {
		return "PointHistory [pointId=" + pointId + ", category=" + category + ", poDate=" + poDate + ", poContent="
				+ poContent + ", payUno=" + payUno + ", payName=" + payName + ", payPoint=" + payPoint + ", point="
				+ point + ", uno=" + uno + "]";
	}

	
}
