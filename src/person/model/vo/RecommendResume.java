package person.model.vo;

import java.sql.Date;
import java.util.ArrayList;

import member.model.vo.Member;

public class RecommendResume {
	private int refhisId;
	private int refId;
	private String refhisContent;
	private String category;
	private Date refhidDate;
	private String quarter;
	private ArrayList<Member> memberList;
	private Member member;
	
	public RecommendResume() {}
	

	public RecommendResume(int refhisId, int refId, String refhisContent, String category, Date refhidDate,
			String quarter, ArrayList<Member> memberList, Member member) {
		super();
		this.refhisId = refhisId;
		this.refId = refId;
		this.refhisContent = refhisContent;
		this.category = category;
		this.refhidDate = refhidDate;
		this.quarter = quarter;
		this.memberList = memberList;
		this.member = member;
	}
	
	
	public ArrayList<Member> getMemberList() {
		return memberList;
	}

	public void setMemberList(ArrayList<Member> memberList) {
		this.memberList = memberList;
	}

	public int getRefhisId() {
		return refhisId;
	}

	public void setRefhisId(int refhisId) {
		this.refhisId = refhisId;
	}

	public int getRefId() {
		return refId;
	}

	public void setRefId(int refId) {
		this.refId = refId;
	}

	public String getRefhisContent() {
		return refhisContent;
	}

	public void setRefhisContent(String refhisContent) {
		this.refhisContent = refhisContent;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getRefhidDate() {
		return refhidDate;
	}

	public void setRefhidDate(Date refhidDate) {
		this.refhidDate = refhidDate;
	}



	public Member getMember() {
		return member;
	}



	public void setMember(Member member) {
		this.member = member;
	}

	


	public String getQuarter() {
		return quarter;
	}


	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}


	@Override
	public String toString() {
		return "RecommendResume [refhisId=" + refhisId + ", refId=" + refId + ", refhisContent=" + refhisContent
				+ ", category=" + category + ", refhidDate=" + refhidDate + ", quarter=" + quarter + ", memberList="
				+ memberList + ", member=" + member + "]";
	}

	
}