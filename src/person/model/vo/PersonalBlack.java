package person.model.vo;

public class PersonalBlack {
	private int pblid;
	private int pUno;
	private int cUno;
	
	public PersonalBlack() {}

	public PersonalBlack(int pblid, int pUno, int cUno) {
		super();
		this.pblid = pblid;
		this.pUno = pUno;
		this.cUno = cUno;
	}

	public int getPblid() {
		return pblid;
	}

	public void setPblid(int pblid) {
		this.pblid = pblid;
	}

	public int getpUno() {
		return pUno;
	}

	public void setpUno(int pUno) {
		this.pUno = pUno;
	}

	public int getcUno() {
		return cUno;
	}

	public void setcUno(int cUno) {
		this.cUno = cUno;
	}

	@Override
	public String toString() {
		return "PersonalBlack [pblid=" + pblid + ", pUno=" + pUno + ", cUno=" + cUno + "]";
	}
	
}