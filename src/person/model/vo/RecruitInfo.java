package person.model.vo;

import java.util.ArrayList;

import keyword.model.vo.Keyword;

public class RecruitInfo {
	private int recid;					//채용공고 ID
	private int uno; 			 		//회원 번호 
	private String rec_title; 	  		//공고제목
	private String company_name;		//회사명
	private String logo;	 			//로고
	private ArrayList<Keyword> klist;	//키워드
	
	public RecruitInfo() {}
	
	public RecruitInfo(int recid, int uno, String rec_title, String company_name, String logo,
			ArrayList<Keyword> klist) {
		super();
		this.recid = recid;
		this.uno = uno;
		this.rec_title = rec_title;
		this.company_name = company_name;
		this.logo = logo;
		this.klist = klist;
	}

	public int getRecid() {
		return recid;
	}

	public void setRecid(int recid) {
		this.recid = recid;
	}

	public int getUno() {
		return uno;
	}

	public void setUno(int uno) {
		this.uno = uno;
	}

	public String getRec_title() {
		return rec_title;
	}

	public void setRec_title(String rec_title) {
		this.rec_title = rec_title;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public ArrayList<Keyword> getKlist() {
		return klist;
	}

	public void setKlist(ArrayList<Keyword> klist) {
		this.klist = klist;
	}

	@Override
	public String toString() {
		return "RecruitInfo [recid=" + recid + ", uno=" + uno + ", rec_title=" + rec_title + ", company_name="
				+ company_name + ", logo=" + logo + ", klist=" + klist + "]";
	}
	
	
	
	
	
	
}
