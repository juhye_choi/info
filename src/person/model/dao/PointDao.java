package person.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import keyword.model.vo.PageInfo;
import person.model.vo.PointHistory;

import static common.JDBCTemplate.*;

public class PointDao {
	private Properties prop = new Properties();
	
	public PointDao() {
		String fileName = PointDao.class.getResource("/sql/person/point-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int checkDaily(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("checkDaily");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public ArrayList<Integer> selectOneInfo(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Integer> numlist = null;
		
		String query = prop.getProperty("selectOneInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setInt(2, uno);
			pstmt.setInt(3, uno);
			pstmt.setInt(4, uno);
			pstmt.setInt(5, uno);
			pstmt.setInt(6, uno);
			pstmt.setInt(7, uno);
			pstmt.setInt(8, uno);
			pstmt.setInt(9, uno);
			
			rset = pstmt.executeQuery();
			numlist = new ArrayList<Integer>();
			while(rset.next()) {
				numlist.add(rset.getInt("BASIC"));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return numlist;
	}

	public int insertDailyCheck(Connection con, int uno) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertDailyCheck");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, "출석체크");
			pstmt.setInt(2, uno);
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<PointHistory> selectPointList(Connection con, int uno, PageInfo pi) {
		PreparedStatement pstmt = null;
		ArrayList<PointHistory> polist = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectPointList");
		
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			polist = new ArrayList<>();
			while(rset.next()) {
				PointHistory ph = new PointHistory();
				ph.setCategory(rset.getString("CATEGORY"));
				ph.setPointId(rset.getInt("POINTID"));
				ph.setPoDate(rset.getDate("PODATE"));
				ph.setPoint(rset.getInt("POINT"));
				ph.setPayUno(rset.getInt("PAY_UNO"));
				ph.setPayName(rset.getString("NAME"));
				ph.setPayPoint(rset.getInt("PAYPOINT"));
				ph.setPoContent(rset.getString("POCONTENT"));
				
				polist.add(ph);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return polist;
	}

	public int getListCount(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return result;
	}

	public int insertPointPay(Connection con, PointHistory ph) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPointPay");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, ph.getPoContent());
			pstmt.setInt(2, ph.getPayPoint());
			pstmt.setInt(3, ph.getPoint());
			pstmt.setInt(4, ph.getUno());
			
			result = pstmt.executeUpdate();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

}









