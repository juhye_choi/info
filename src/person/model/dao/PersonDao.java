package person.model.dao;

import static common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import keyword.model.vo.Keyword;
import keyword.model.vo.Keyword2;
import person.model.vo.Attachment2;
import person.model.vo.Person;
import person.model.vo.RecommendResume;
import resume.model.vo.Attachment;

public class PersonDao {

	private Properties prop = new Properties();

	public PersonDao() {

		String fileName = PersonDao.class.getResource("/sql/person/person-query.properties").getPath();

		try {
			prop.load(new FileReader(fileName));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Person myResume(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Person p = null;
		String query = prop.getProperty("selectMyResume");

		try {
			pstmt= con.prepareStatement(query);
			pstmt.setInt(1, uno);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return p;
	}

	public ArrayList<Person> searchResume(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;

		String query = prop.getProperty("selectSearchResume");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();

			list = new ArrayList<>();
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));

				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	public int getreListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int reListCount = 0;

		String query = prop.getProperty("reListCount");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if(rset.next()) {
				reListCount = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}

		return reListCount;
	}
	
	public String selectQuarter(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		String result = null;

		String query = prop.getProperty("selectQuarter");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if(rset.next()) {
				result = rset.getString("QUARTER");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}

		return result;
	}

	public int selectRefResume(Connection con, int uno, String quarter) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("selectRefResume");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setString(2, quarter);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt("REFID");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public int intsertRefResume(Connection con, int uno, String quarter) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("intsertRefResume");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setString(2, quarter);

			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	public int selectCurrval(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int refId = 0;

		String query = prop.getProperty("selectCurrval");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if(rset.next()) {
				refId = rset.getInt("CURRVAL");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}

		return refId;
	}

	public int insertRefHis(Connection con, int refId) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("insertRefHis");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, refId);

			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	public HashMap<String, String> selectPserson(Connection con, int otherUno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		HashMap<String, String> personInfo = null;

		String query = prop.getProperty("selectOtherPerson");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, otherUno);

			rset = pstmt.executeQuery();

			personInfo = new HashMap<>();
			if(rset.next()) {
				personInfo.put("name", rset.getString("NAME"));
				personInfo.put("phone", rset.getString("PHONE"));
				personInfo.put("email", rset.getString("EMAIL"));
				personInfo.put("jobStatus", rset.getString("JOB_STATUS"));
				personInfo.put("infoOpen", rset.getString("INFO_OPEN"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return personInfo;
	}

	public int checkPointHistory(Connection con, int uno, int otherUno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("checkPointHistory");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, otherUno);
			pstmt.setInt(2, uno);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}

		return result;
	}

	public ArrayList<RecommendResume> selectRefHis(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<RecommendResume> list = null;

		String query = prop.getProperty("selectRefHis");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);

			rset = pstmt.executeQuery();

			list = new ArrayList<>();
			while(rset.next()) {
				RecommendResume rr = new RecommendResume();

				rr.setRefhidDate(rset.getDate("REFHIS_DATE"));
				rr.setCategory(rset.getString("CATEGORY"));
				rr.setRefhisContent(rset.getString("CONTENT"));

				list.add(rr);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	public int selectAppCount(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("selectAppCount");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt("COUNT");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return result;
	}

	public Person selectPersonInfo(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Person person = null;

		String query = prop.getProperty("selectPersonInfo");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				person = new Person();

				person.setUno(rset.getInt("UNO"));
				person.setBirth(rset.getDate("PBIRTH"));
				person.setGendder(rset.getString("GENDER"));
				person.setPoint(rset.getInt("NOW_POINT"));
				person.setInfoOpen(rset.getString("INFO_OPEN"));
				person.setJobStatus(rset.getString("JOB_STATUS"));
				person.setGithub(rset.getString("GITHUB"));
				person.setBlog(rset.getString("BLOG"));
				person.setKosaYN(rset.getString("KOSA_YN"));

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}

		return person;
	}

	public int insertPerson(Connection con, int uno) {

		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("insertPerson");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);

			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}

	public int updatePerson(Connection con, Person requestPerson) {
		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("updatePerson");
		try {
			pstmt = con.prepareStatement(query);

			pstmt.setDate(1, requestPerson.getBirth());
			pstmt.setString(2, requestPerson.getGendder());
			pstmt.setString(3, requestPerson.getInfoOpen());
			pstmt.setString(4, requestPerson.getJobStatus());
			pstmt.setString(5, requestPerson.getGithub());
			pstmt.setString(6, requestPerson.getBlog());
			pstmt.setString(7, requestPerson.getKosaYN());
			pstmt.setInt(8, requestPerson.getUno());

			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}

	public String selectCategory(Connection con, int uno, String quarter) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String result = null;

		String query = prop.getProperty("selectCategory");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setString(2, quarter);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getString("CATEGORY");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return result;
	}

	public int updateImg(Connection con, ArrayList<Attachment2> fileList) {
		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("insertAttachmentPerson");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setString(1, fileList.get(0).getOriginName());
			pstmt.setString(2, fileList.get(0).getChangeName());
			pstmt.setString(3, fileList.get(0).getFilePath());
			pstmt.setInt(4, 4);
			pstmt.setInt(5, fileList.get(0).getProUno());

			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}

	public ArrayList<Attachment2> selectImg(Connection con, int uno) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Attachment2> list = null;

		String query = prop.getProperty("selectImg");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, uno);
			pstmt.setInt(2, uno);

			rset = pstmt.executeQuery();

			list = new ArrayList<Attachment2>();
			while(rset.next()) {

				Attachment2 att = new  Attachment2();

				att.setChangeName(rset.getString("CHANGE_NAME"));
				att.setOriginName(rset.getString("ORIGIN_NAME"));
				att.setFilePath(rset.getString("FILE_PATH"));
				att.setAtDate(rset.getDate("ATDATE"));
				att.setRemoveYN(rset.getString("REMOVE_YN"));
				att.setCategory(rset.getInt("CATEGORY"));

				list.add(att);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}finally {

			close(pstmt);
			close(rset);

		}

		return list;
	}

	public ArrayList<Person> searchRefResume(Connection con, String quarter) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;
		
		String query = prop.getProperty("searchRefResume");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, quarter);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<Keyword2> selectDetailKeyword(Connection con, int buttonNum) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Keyword2> list = null;
		
		String query = prop.getProperty("selectDetailKeyword");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, buttonNum);
			
			rset = pstmt.executeQuery();
			
			list =new ArrayList<>();
			
			while(rset.next()) {
				Keyword2 k = new Keyword2();
				
				k.setKname(rset.getString("KNAME"));
				
				list.add(k);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	public String selectInfoOpen(Connection con, int otherUno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String infoOpen = null;
		
		String query = prop.getProperty("selectInfoOpen");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, otherUno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				infoOpen = rset.getString("INFO_OPEN");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return infoOpen;
	}

	public int selectPersonPoint(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int point = 0;
		
		String query = prop.getProperty("selectPersonPoint");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				point = rset.getInt("POINT");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return point;
	}

	public int insetPayPointHis(Connection con, int uno, int otherUno) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insetPayPointHis");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, otherUno);
			pstmt.setInt(2, uno);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<Person> searchKeyResume(Connection con, String keyword) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;

		String query = prop.getProperty("searchKeyResume");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, keyword);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setName(rset.getString("NAME"));
				p.setUserid(rset.getString("USER_ID"));
				p.setCount(rset.getString("COUNT"));

				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<Person> searchKeyRefResume(Connection con, String keyword, String quarter) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;
		
		String query = prop.getProperty("searchKeyRefResume");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, keyword);
			pstmt.setString(2, quarter);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}

	public ArrayList<Person> searchContainKeyResume(Connection con, String searchKeyword) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;

		String query = prop.getProperty("searchContainKeyResume");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchKeyword);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<Person> pSearchKeyResume(Connection con, String keyword, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;
		
		String query = prop.getProperty("pSearchKeyResume");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, keyword);
			pstmt.setInt(2, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));

				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<Person> pSearchContainKeyResume(Connection con, String searchKeyword, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;

		String query = prop.getProperty("pSearchContainKeyResume");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchKeyword);
			pstmt.setInt(2, uno);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));

				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<Person> searchContainKeyRefResume(Connection con, String keyword, String quarter) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;
		
		String query = prop.getProperty("searchContainKeyRefResume");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, keyword);
			pstmt.setString(2, quarter);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<Person> pSearchKeyRefResume(Connection con, String keyword, String quarter) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;
		
		String query = prop.getProperty("pSearchKeyRefResume");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, keyword);
			pstmt.setString(2, quarter);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));

				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<Person> pSearchContainKeyRefResume(Connection con, String quarter, String keyword) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Person> list = null;
		
		String query = prop.getProperty("pSearchContainKeyRefResume");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, keyword);
			pstmt.setString(2, quarter);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Person>();
			
			while(rset.next()) {
				Person p = new Person();
				Attachment a = new Attachment();
				
				p.setUno(rset.getInt("UNO"));
				p.setUserid(rset.getString("USER_ID"));
				p.setName(rset.getString("NAME"));
				p.setCount(rset.getString("COUNT"));
				
				a.setChangeName(rset.getString("PICTURE"));
				
				p.setPicture(a);
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<Keyword> selectKeyword(Connection con, String searchKeyword) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Keyword> klist = null;
		
		String query = prop.getProperty("selectKeyword");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchKeyword);
			
			rset = pstmt.executeQuery();
			
			klist = new ArrayList<Keyword>();
			
			while(rset.next()) {
				Keyword k = new Keyword();
				
				k.setKname(rset.getString("KNAME"));
				
				klist.add(k);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return klist;
	}
	//블랙리스트 등록했는지
	public int selectBlacklist(Connection con, int uno, int cuno) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("checkBlacklist");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setInt(2, cuno);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int addBlacklist(Connection con, int uno, int cuno) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertBlacklist");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setInt(2, cuno);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

}