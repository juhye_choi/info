package person.model.dao;

import static common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import company.model.vo.Company;
import company.model.vo.PaymentList;
import company.model.vo.Recruit;
import company.model.vo.RecruitAttachment;
import keyword.model.vo.Keyword;
import person.model.vo.RecruitInfo;
import resume.model.vo.Attachment;

public class RecruitInfoDao {
	
	private Properties prop = new Properties();
	
	public RecruitInfoDao() {
		String fileName = PersonDao.class.getResource("/sql/recruit/recruit-query.properties").getPath();

		try {
			prop.load(new FileReader(fileName));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<PaymentList> PremiumList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<PaymentList> list = null;
		
		String query = prop.getProperty("selectPremiumRecruit");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				PaymentList pl = new PaymentList();
				
				pl.setRecid(rset.getInt("RECID"));
				
				list.add(pl);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return list;
	}

	public RecruitInfo selectAll(Connection con, int recId) {
		PreparedStatement pstmt = null;
		RecruitInfo ri = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("PremiumRecruitList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, recId);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				ri = new RecruitInfo();
				ri.setRecid(recId);
				ri.setLogo(rset.getString("CHANGE_NAME"));
				ri.setRec_title(rset.getString("REC_TITLE"));
				ri.setCompany_name(rset.getString("NAME"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return ri;
	}
	//키워드 3개
	public ArrayList<Keyword> selectKeywordList(Connection con, int recid) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Keyword> keywordList = null;
		
		String query = prop.getProperty("selectKeyword");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, recid);
			
			rset = pstmt.executeQuery();
			
			keywordList = new ArrayList<>();
			while(rset.next()) {
				Keyword k = new Keyword();
				k.setKname(rset.getString("KNAME"));
				
				keywordList.add(k);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return keywordList;
	}

	// 공고 기간이 유효한 전체 게시물 조회용
	public int getListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int listCount = 0;
		
		String query = prop.getProperty("countList");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;
	}
	

	public ArrayList<RecruitInfo> selectListWithPaging(Connection con, int currentPage, int limit) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<RecruitInfo> list = null;
		
		String query = prop.getProperty("recruitingListWithPaging");
		
		int startRow = (currentPage - 1) * limit + 1;
		int endRow = startRow + limit - 1;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while (rset.next()) {
				RecruitInfo ri = new RecruitInfo();
				
				ri.setRecid(rset.getInt("RECID"));
				ri.setLogo(rset.getString("CHANGE_NAME"));
				ri.setRec_title(rset.getString("REC_TITLE"));
				ri.setCompany_name(rset.getString("NAME"));	
				
				list.add(ri);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}

	public Recruit selectOneRecruit(Connection con, int recid) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Recruit list = null;
		
		String query = prop.getProperty("selectRecruitContents");
		
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, recid);
				
				rset = pstmt.executeQuery();
				
				if(rset.next()) {
					list = new Recruit();
					list.setRecid(rset.getInt("RECID"));
					list.setUno(rset.getInt("UNO"));
					list.setRec_title(rset.getString("REC_TITLE"));
					list.setRectype(rset.getInt("REC_TYPE"));
					list.setCareer_period(rset.getString("CAREER_PERIOD"));
					list.setEducation(rset.getInt("EDUCATION"));
					list.setRec_pcount(rset.getInt("REC_PCOUNT"));
					list.setSalary_way(rset.getInt("SALARY_WAY"));
					list.setMin_salary(rset.getInt("MIN_SALARY"));
					list.setMax_salary(rset.getInt("MAX_SALARY"));
					list.setWork_area(rset.getString("WORK_AREA"));
					list.setJob_position(rset.getString("JOB_POSITION"));
					list.setEligibility(rset.getString("ELIGIBILITY"));
					list.setSpe_condition(rset.getString("SPE_CONDITION"));
					list.setBenefits_welfare(rset.getString("BENEFITS_WELFARE"));
					list.setRec_start(rset.getDate("REC_START"));
					list.setRec_finish(rset.getDate("REC_FINISH"));
					list.setHowto_apply(rset.getInt("HOWTO_APPLY"));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
				close(rset);
			}
		
		return list;
	}

	public Company selectOneCompany(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Company list = null;
		
		String query = prop.getProperty("selectCompanyContents");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				list = new Company();
				list.setEmail(rset.getString("EMAIL"));
				list.setPhone(rset.getString("PHONE"));
				list.setCompanyName(rset.getString("NAME"));
				list.setOwner(rset.getString("OWNER"));
				list.setHrManager(rset.getString("HR_NAME"));
				list.setIntro(rset.getString("CINTRO"));
				list.setBirth(rset.getDate("CBIRTH"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}
	//모든 키워드
	public ArrayList<Keyword> selectAllKeywordList(Connection con, int recid) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Keyword> keywordList = null;
		
		String query = prop.getProperty("selectAllKeyword");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, recid);
			
			rset = pstmt.executeQuery();
			
			keywordList = new ArrayList<>();
			while(rset.next()) {
				Keyword k = new Keyword();
				k.setKname(rset.getString("KNAME"));
				
				keywordList.add(k);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return keywordList;
	}

	public RecruitAttachment selectOneAttachment(Connection con, int recid) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		RecruitAttachment ra = null;
		
		String query = prop.getProperty("selectAttachment");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, recid);
			
			rset = pstmt.executeQuery();
			
			ra = new RecruitAttachment();
			
			if(rset.next()) {
				ra.setOriginName(rset.getString("ORIGIN_NAME"));
				ra.setChangeName(rset.getString("CHANGE_NAME"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return ra;		
	}
	
	//지원자수 조회
	public int selectCount(Connection con, int recid) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int count = 0;
		
		String query = prop.getProperty("countApplicant");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, recid);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				count = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return count;	
	}
	//공고 지원
	public int applyRecruit(Connection con, int uno, int recid) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertApply");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, uno);
			pstmt.setInt(2, recid);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	//파일첨부
	public int insertAttachment(Connection con, RecruitAttachment ra, int appId) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertAttachmentApply");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, ra.getOriginName());
			pstmt.setString(2, ra.getChangeName());
			pstmt.setString(3, ra.getFilePath());
			pstmt.setInt(4, appId);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public int applyCurrentVal(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("applyCurrval");
		
		try {
			stmt = con.createStatement();
			
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
		}
		return result;
	}

	public int readRecruit(Connection con, int uno, int recid) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("readRecruit");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, uno);
			pstmt.setInt(2, recid);

			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<Attachment> selectCompanyImg(Connection con, int uno) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Attachment> list = null;
		
		String query = prop.getProperty("selectCompanyImg");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			
			rset = pstmt.executeQuery();

			list = new ArrayList<>();
			while(rset.next()) {
				Attachment a = new Attachment();
				
				a.setChangeName(rset.getString("CHANGE_NAME"));
				
				list.add(a);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public int checkReadRecruit(Connection con, int uno) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("checkReadRecruit");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, uno);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	//지원 기록
	public int supportRecord(Connection con, int uno, int recid) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("supportRecord");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, recid);
			pstmt.setInt(2, uno);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
}
