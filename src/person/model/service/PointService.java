package person.model.service;

import static common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;

import keyword.model.vo.PageInfo;
import person.model.dao.PointDao;
import person.model.vo.PointHistory;

public class PointService {

	public int checkDaily(int uno) {
		Connection con = getConnection();
		int result = 0;
		
		result = new PointDao().checkDaily(con, uno);
		
		close(con);
		
		return result;
	}

	public ArrayList<Integer> selectOneInfo(int uno) {
		Connection con = getConnection();
		ArrayList<Integer> numlist = null;
		
		numlist = new PointDao().selectOneInfo(con, uno);
		
		close(con);
		
		return numlist;
	}

	public int insertDailyCheck(int uno) {
		Connection con = getConnection();
		int result = 0;
		
		result = new PointDao().insertDailyCheck(con, uno);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public ArrayList<PointHistory> selectPointList(int uno, PageInfo pi) {
		Connection con = getConnection();
		ArrayList<PointHistory> polist = null;
		
		polist = new PointDao().selectPointList(con, uno, pi);
		
		close(con);
		
		return polist;
	}

	public int getListCount(int uno) {
		Connection con = getConnection();
		int result = 0;
		
		result = new PointDao().getListCount(con, uno);
		
		close(con);
		
		return result;
	}

	public int insertPointPay(PointHistory ph) {
		Connection con = getConnection();
		int result = 0;
		
		result = new PointDao().insertPointPay(con, ph);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

}
