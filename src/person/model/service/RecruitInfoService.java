package person.model.service;

import static common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;

import company.model.vo.Company;
import company.model.vo.PaymentList;
import company.model.vo.Recruit;
import company.model.vo.RecruitAttachment;
import person.model.dao.RecruitInfoDao;
import person.model.vo.RecruitInfo;

public class RecruitInfoService {
	// 적극채용 사용중인 채용공고ID들
	public ArrayList<PaymentList> PremiumList() {
		Connection con = getConnection();
		
		ArrayList<PaymentList> plist = new RecruitInfoDao().PremiumList(con);
		
		close(con);
		
		return plist;
	}
	
	// 적극채용공고들의 소개배너
	public ArrayList<RecruitInfo> selectAll(ArrayList<PaymentList> paymentList) {
		Connection con = getConnection();
		ArrayList<RecruitInfo> list = null;
		RecruitInfo oneRec = null;
		
		list = new ArrayList<>();
		
		for(PaymentList pl : paymentList) {
			oneRec = new RecruitInfoDao().selectAll(con, pl.getRecid());
			oneRec.setKlist(new RecruitInfoDao().selectKeywordList(con, pl.getRecid()));
			list.add(oneRec);
		}
		
		close(con);
		
		return list;
	}

	// 공고 기간이 유효한 전체 게시물 조회용
	public int getListCount() {
		Connection con = getConnection();
		
		int listCount = new RecruitInfoDao().getListCount(con);
		
		close(con);
		
		return listCount;
	}

	// 페이징 처리 후 게시물 목록 조회용
	public ArrayList<RecruitInfo> selectListWithPaging(int currentPage, int limit) {
		Connection con = getConnection();
		ArrayList<RecruitInfo> mergeList = null;
		
		ArrayList<RecruitInfo> list = new RecruitInfoDao().selectListWithPaging(con, currentPage, limit);

		mergeList = new ArrayList<>();
		for(RecruitInfo ri : list) {
			RecruitInfo oneRec = new RecruitInfo();
			
			oneRec.setRecid(ri.getRecid());
			oneRec.setRec_title(ri.getRec_title());
			oneRec.setLogo(ri.getLogo());
			oneRec.setCompany_name(ri.getCompany_name());
			oneRec.setKlist(new RecruitInfoDao().selectKeywordList(con, ri.getRecid()));
			mergeList.add(oneRec);
		}
		
		close(con);

		return mergeList;
	}
	
	//선택한 채용공고의 내용
	public Recruit selectOneRecruit(int recid) {
		Connection con = getConnection();
		
		Recruit list = new RecruitInfoDao().selectOneRecruit(con, recid);
		list.setKlist(new RecruitInfoDao().selectAllKeywordList(con, recid));
		list.setRecruitAttachment(new RecruitInfoDao().selectOneAttachment(con, recid));
		list.setApplicant(new RecruitInfoDao().selectCount(con, recid));
		close(con);
		
		return list;
	}
	//선택한 채용공고의 회사
	public Company selectOneCompany(int uno) {
		Connection con = getConnection();
		
		Company list = new RecruitInfoDao().selectOneCompany(con, uno);
		
		list.setAtlist(new RecruitInfoDao().selectCompanyImg(con, uno));
		
		close(con);
		
		return list;
	}
	//공고 지원
	public int applyRecruit(RecruitAttachment ra, int uno, int recid) {
		Connection con = getConnection();
		int result = 0;
		int resultR = 0;
		int resultA = 0;
		
		resultR = new RecruitInfoDao().applyRecruit(con, uno, recid);
		
		//System.out.println("resultR : " + resultR);
		int appId = new RecruitInfoDao().applyCurrentVal(con);
		//System.out.println("appId : " + appId);
		if(resultR > 0) {
			resultA = new RecruitInfoDao().insertAttachment(con, ra, appId);
			//System.out.println("resultA : " + resultA);
			if(resultA > 0) {
				commit(con);
				result = 1;
			} else {
				rollback(con);
			}
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public int readRecruit(int uno, int recid) {
		Connection con = getConnection();
		int result = 0;
		int resultI = 0;
		int resultR = 0;
		
		resultR = new RecruitInfoDao().checkReadRecruit(con, uno);
		if(resultR == 0) {
			resultI = new RecruitInfoDao().readRecruit(con, uno, recid);
			if(resultI > 0) {
				commit(con);
			}else {
				rollback(con);
			}
		}
		
		result = new RecruitInfoDao().supportRecord(con, uno, recid);
		
		close(con);
		
		return result;
	}

}
