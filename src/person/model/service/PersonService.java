package person.model.service;

import static common.JDBCTemplate.close;
import static common.JDBCTemplate.commit;
import static common.JDBCTemplate.getConnection;
import static common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import keyword.model.vo.Keyword;
import keyword.model.vo.Keyword2;
import person.model.dao.PersonDao;
import person.model.vo.Attachment2;
import person.model.vo.Person;
import person.model.vo.RecommendResume;

public class PersonService {

   public Person myResume(int uno) {
      Connection con = getConnection();
      
      Person p = new PersonDao().myResume(con, uno);
      
      close(con);
      
      return p;
   }

   public ArrayList<Person> searchResume(int uno) {
      Connection con = getConnection();
      
      ArrayList<Person> list = new PersonDao().searchResume(con, uno);
      
      close(con);
      
      return list;
   }

   public int getreListCount() {
      Connection con = getConnection();
      
      int reListCount = new PersonDao().getreListCount(con);
      
      close(con);
      
      return reListCount;
   }

   public String selectquarter() {
      Connection con = getConnection();
      
      String quarter = new PersonDao().selectQuarter(con);
      
      close(con);
      
      return quarter;
   }

   public String RefSumeYN(int uno, String quarter) {
      Connection con = getConnection();
      
      int refId = new PersonDao().selectRefResume(con, uno, quarter);
      String result = null;
      
      if(refId > 0) {
    	  result = new PersonDao().selectCategory(con, uno, quarter);
      }
      
      close(con);
      
      return result;
   }
   
   public int intsertRefResume(int uno, String quarter) {
      Connection con = getConnection();
      
      int result = 0;
      
      int result1 = new PersonDao().intsertRefResume(con, uno, quarter);
      int result2 = 0;
      
      if(result1 > 0) {
         int refId = new PersonDao().selectCurrval(con);
         
         if(refId > 0) {
            result2 = new PersonDao().insertRefHis(con, refId);
         }
      }
      
      if(result1 > 0 && result2 > 0) {
         result = 1;
         commit(con);
      }else {
         rollback(con);
      }
      close(con);
      
      return result;
   }

   public HashMap<String, String> selectPerson(int otherUno) {
      Connection con = getConnection();
      
      HashMap<String, String> personInfo = new PersonDao().selectPserson(con, otherUno);
      
      close(con);
      
      return personInfo;
   }

   public int checkPointHistory(int uno, int otherUno) {
      Connection con = getConnection();
      
      int result = new PersonDao().checkPointHistory(con, uno, otherUno);
      
      close(con);
      
      return result;
   }

   public ArrayList<RecommendResume> selectRefHis(int uno) {
      Connection con = getConnection();
      
      ArrayList<RecommendResume> list = new PersonDao().selectRefHis(con, uno);
      
      close(con);
      
      return list;
   }

   public int selectAppCount(int uno) {
      Connection con = getConnection();
      
      int result = new PersonDao().selectAppCount(con, uno);
      
      close(con);
      
      return result;
   }
   
   public Person selectPersonInfo(int uno) {
         
         Connection con = getConnection();
         Person person = null;
         
         person = new PersonDao().selectPersonInfo(con,uno);
         
         close(con);
         
         return person;
      }

      public int insertPersonInfo(int uno) {
         Connection con = getConnection();
         
         int result = new PersonDao().insertPerson(con, uno);
         
         if(result>0) {
            commit(con);
         } else {
            rollback(con);
         }
         
         return result;
      }

	public ArrayList<Attachment2> selectImg(int uno) {
	
		 Connection con = getConnection();
		
		ArrayList<Attachment2> list = null;
		
		list = new PersonDao().selectImg(con,uno);
		
		
		close(con);
		
		
		return list;
	}

	public ArrayList<Person> searchRefResume() {
		Connection con = getConnection();
		
		String quarter = null;
		ArrayList<Person> list = null;
		
		quarter = new PersonDao().selectQuarter(con);

		if(quarter != null) {
			list = new PersonDao().searchRefResume(con, quarter);
		}
		
		close(con);
		
		return list;
	}

	public ArrayList<Keyword2> selectDetailKeyword(int buttonNum) {
		Connection con = getConnection();
		
		ArrayList<Keyword2> list = new PersonDao().selectDetailKeyword(con, buttonNum);
		
		close(con);
		
		return list;
	}

	public String selectInfoOpen(int otherUno) {
		Connection con = getConnection();
		
		String infoOpen = new PersonDao().selectInfoOpen(con, otherUno);
		
		close(con);
		
		return infoOpen;
	}

	public int selectPersonPoint(int uno) {
		Connection con = getConnection();
		
		int point = new PersonDao().selectPersonPoint(con, uno);
		
		close(con);
		
		return point;
	}

	public int payPoint(int uno, int otherUno) {
		Connection con = getConnection();
		
		int result = new PersonDao().insetPayPointHis(con, uno, otherUno);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public ArrayList<Person> searchKeyResume(String keyword) {
		Connection con = getConnection();
		
		ArrayList<Person> list = new PersonDao().searchKeyResume(con, keyword);
		
		close(con);
		
		return list;
	}

	public ArrayList<Person> searchKeyRefResume(String keyword) {
		Connection con = getConnection();
		
		String quarter = new PersonDao().selectQuarter(con);
		ArrayList<Person> list = null;
		
		if(quarter != null) {
			list = new PersonDao().searchKeyRefResume(con, keyword, quarter);
		}
		
		close(con);
		
		return list;
	}

	public ArrayList<Person> searchContainKeyResume(String searchKeyword) {
		Connection con = getConnection();
		
		ArrayList<Person> list = new PersonDao().searchContainKeyResume(con, searchKeyword);
		
		close(con);
		
		return list;
	}

	public ArrayList<Person> pSearchKeyResume(String keyword, int uno) {
		Connection con = getConnection();
		
		ArrayList<Person> list = new PersonDao().pSearchKeyResume(con, keyword, uno);
		
		close(con);
		
		return list;
	}

	public ArrayList<Person> pSearchContainKeyResume(String searchKeyword, int uno) {
		Connection con = getConnection();
		
		ArrayList<Person> list = new PersonDao().pSearchContainKeyResume(con, searchKeyword, uno);
		
		close(con);
		
		return list;
	}
	
	public ArrayList<Person> searchContainKeyRefResume(String keyword) {
		Connection con = getConnection();
		
		String quarter = new PersonDao().selectQuarter(con);
		ArrayList<Person> list = null;
		
		if(quarter != null) {
			list = new PersonDao().searchContainKeyRefResume(con, keyword, quarter);
		}
		
		close(con);
		
		return list;
	}
	public ArrayList<Person> pSearchKeyRefResume(String keyword) {
		Connection con = getConnection();
		
		String quarter = new PersonDao().selectQuarter(con);
		ArrayList<Person> list = null;
		
		if(quarter != null) {
			list = new PersonDao().pSearchKeyRefResume(con, keyword, quarter);
		}
		
		close(con);
		
		return list;
	}

	public ArrayList<Person> pSearchContainKeyRefResume(String keyword) {
		Connection con = getConnection();
		
		String quarter = new PersonDao().selectQuarter(con);
		ArrayList<Person> list = null;
		
		if(quarter != null) {
			list = new PersonDao().pSearchContainKeyRefResume(con, quarter, keyword);
		}
		
		close(con);
		
		return list;
	}

	public ArrayList<Keyword> selectKeyword(String searchKeyword) {
		Connection con = getConnection();
		
		ArrayList<Keyword> klist = new PersonDao().selectKeyword(con, searchKeyword);
		
		close(con);
		
		return klist;
	}

	public int selectBlacklist(int uno, int cuno) {
		Connection con = getConnection();
		int result = 0;
		
		result = new PersonDao().selectBlacklist(con, uno, cuno);
		
		close(con);
		
		return result;
	}
	//블랙리스트 등록
	public int addBlacklist(int uno, int cuno) {
		Connection con = getConnection();
		int result = 0;
		
		result = new PersonDao().addBlacklist(con, uno, cuno);
		
		close(con);
		
		return result;
	}

}