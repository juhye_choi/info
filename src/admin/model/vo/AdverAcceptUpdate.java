package admin.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class AdverAcceptUpdate {
	
	private int pcode;
	private String cName;
	private Date sDate;
	private Date eDate;
	private int pMoney;
	private String changeName;
	private String originName;
}
