package admin.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class AdverAccept {
	
	private Date pDate;			//결제날짜
	private Date sDate;			//광고 시작날짜
	private Date eDate;			//광고 종료날짜
	private String cName;		//결제한 기업 이름
	private String pName;		//결제한 상품 이름
	private int pMoney;			//결제한 금액
	private String changeName;	//광고 파일 바뀐이름
	private int phCategory;		//결제 이력 구분
								//1:요청 , 2:승인 ,3:환불
	private int payid;			//payid
}
