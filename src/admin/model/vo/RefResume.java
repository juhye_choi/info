package admin.model.vo;

import java.io.Serializable;
import java.sql.Date;

public class RefResume implements Serializable{
	private int refId;
	private int refHisId;
	private String category;
	private String quarter;
	private Date refHisDate;
	private String userId;
	private String name;
	private int uno;
	private String refHisContent;
	
	public RefResume() {}

	public RefResume(int refId, int refHisId, String category, String quarter, Date refHisDate, String userId,
			String name, int uno, String refHisContent) {
		super();
		this.refId = refId;
		this.refHisId = refHisId;
		this.category = category;
		this.quarter = quarter;
		this.refHisDate = refHisDate;
		this.userId = userId;
		this.name = name;
		this.uno = uno;
		this.refHisContent = refHisContent;
	}

	public int getRefId() {
		return refId;
	}

	public void setRefId(int refId) {
		this.refId = refId;
	}

	public int getRefHisId() {
		return refHisId;
	}

	public void setRefHisId(int refHisId) {
		this.refHisId = refHisId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	public Date getRefHisDate() {
		return refHisDate;
	}

	public void setRefHisDate(Date refHisDate) {
		this.refHisDate = refHisDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRefHisContent() {
		return refHisContent;
	}

	public void setRefHisContent(String refHisContent) {
		this.refHisContent = refHisContent;
	}

	public int getUno() {
		return uno;
	}

	public void setUno(int uno) {
		this.uno = uno;
	}

	@Override
	public String toString() {
		return "RefResume [refId=" + refId + ", refHisId=" + refHisId + ", category=" + category + ", quarter="
				+ quarter + ", refHisDate=" + refHisDate + ", userId=" + userId + ", name=" + name + ", uno=" + uno
				+ ", refHisContent=" + refHisContent + "]";
	}

}
