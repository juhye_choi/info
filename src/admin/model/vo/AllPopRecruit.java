package admin.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class AllPopRecruit {
	
	private Date payDate;
	private Date sDate;
	private Date eDate;
	private String name;
	private int pMoney;
	private String rTitle;
	private int recid;
	private int status;
}
