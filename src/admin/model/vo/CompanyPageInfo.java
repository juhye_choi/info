package admin.model.vo;

import java.io.Serializable;

public class CompanyPageInfo implements Serializable{
	private int currentPage;
	private int CompanylistCount;
	private int limit;
	private int maxPage;
	private int startPage;
	private int endPage;
	
	public CompanyPageInfo() {}

	public CompanyPageInfo(int currentPage, int CompanylistCount, int limit, int maxPage, int startPage, int endPage) {
		super();
		this.currentPage = currentPage;
		this.CompanylistCount = CompanylistCount;
		this.limit = limit;
		this.maxPage = maxPage;
		this.startPage = startPage;
		this.endPage = endPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getListCount() {
		return CompanylistCount;
	}

	public void setListCount(int CompanylistCount) {
		this.CompanylistCount = CompanylistCount;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(int maxPage) {
		this.maxPage = maxPage;
	}

	public int getStartPage() {
		return startPage;
	}

	public void setStartPage(int startPage) {
		this.startPage = startPage;
	}

	public int getEndPage() {
		return endPage;
	}

	public void setEndPage(int endPage) {
		this.endPage = endPage;
	}

	@Override
	public String toString() {
		return "CompanyPageInfo [currentPage=" + currentPage + ", CompanylistCount=" + CompanylistCount + ", limit=" + limit + ", maxPage="
				+ maxPage + ", startPage=" + startPage + ", endPage=" + endPage + "]";
	}
}
