package admin.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class AllPayment {
	
	private String mcat;	//개인 : p , 기업 : c
	private int pcat;		//1 : 요청 , 2 :승인  ,3 : 환불
	private String userid;	//결제한 아이디
	private String pname;	//결제 상품이럼
	private int pmoney;		//결제 금액
	private Date pDate;		//결제날짜
	private int payid;
}	
	
