package admin.model.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import admin.model.dao.AdminDao;
import admin.model.vo.AdverAccept;
import admin.model.vo.AdverAcceptUpdate;
import admin.model.vo.AllPayment;
import admin.model.vo.AllPopRecruit;
import admin.model.vo.CompanyPageInfo;
import company.model.dao.CompanyPaymentDao;
import company.model.vo.AdverAttachment;
import keyword.model.dao.KeywordDao;
import keyword.model.vo.Keyword;
import keyword.model.vo.PageInfo;
import member.model.dao.MemberDao;
import member.model.vo.Member;
import person.model.vo.RecommendResume;
import static common.JDBCTemplate.*;

public class AdminService {

	public ArrayList<RecommendResume> selectRefHisList() {
		Connection con = getConnection();

		ArrayList<RecommendResume> list = new AdminDao().selectRefHisList(con);

		close(con);

		return list;
	}

	public int EnrollmentRefResume(int refid) {
		Connection con = getConnection();

//		String quarter = new AdminDao().selectQuarter(con);
//		int result = 0;
//		int result2 = 0; 
//		int refId = 0;
//
//		if(quarter != null) {
//			refId = new AdminDao().selectRefId(con, uno, quarter);
//
//			if(refId > 0) {
//				result2 = new AdminDao().updateEnrollmentRefResume(con, refId);
//			}
//		}
		
		int result = 0;
		int result1 = new AdminDao().updateEnrollmentRefResume(con, refid);
		if(result1>0) {
			int result2 = new AdminDao().insertPointforApproval(con, refid);
			if(result2>0) {
				result = 1;
			} else {
				result = 0;
			}
		}
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}

		close(con);

		return result;
	}

	public int CompanionRef(int refId, String content) {
		Connection con = getConnection();

		
		int result = new AdminDao().CompanionRef(con, refId, content);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return result;
	}

	public int selectCountCompany() {
		Connection con = getConnection();
		int result = 0;

		result = new AdminDao().selectCountCompany(con);

		close(con);

		return result;
	}

	public int selectCountPerson() {
		Connection con = getConnection();
		int result = 0;

		result = new AdminDao().selectCountPerson(con);

		close(con);

		return result;
	}

	public int selectCountRef() {
		Connection con = getConnection();
		int result = 0;

		result = new AdminDao().selectCountRef(con);

		close(con);

		return result;
	}

	public ArrayList<HashMap<String, Object>> SearchPersonOne(int num) {


		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdminDao().SearchPersonOne(con,num);


		close(con);

		return list;




	}

	public int deletePerson(int uno) {
		Connection con = getConnection();

		int result = new AdminDao().deletePerson(con,uno);

		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}

		close(con);




		return result;
	}

	public int getListCount() {
		Connection con = getConnection();
		int result = 0;

		result = new AdminDao().getListCount(con);
		if(result != 0) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return result;
	}

	public int getCompanylistCount() {
		Connection con = getConnection();
		int result = 0;

		result = new AdminDao().getCompanylistCount(con);
		if(result != 0) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return result;
	}

	public ArrayList<HashMap<String, Object>> selelctCList(CompanyPageInfo pi) {
		Connection con = getConnection();

		ArrayList<HashMap<String, Object>> list = new AdminDao().selectCList(con,pi);

		if(list != null) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return list;
	}

	public int ApprovalCompany(int num) {
		Connection con = getConnection();

		int result = new AdminDao().ApprovalCompany(con,num);

		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}

		close(con);




		return result;
	}

	public int deleteCompany(int uno) {
		Connection con = getConnection();

		int result = new AdminDao().deleteCompany(con,uno);

		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}

		close(con);




		return result;
	}

	public ArrayList<HashMap<String, String>> SearchCompanyOne(int num) {
		Connection con = getConnection();

		ArrayList<HashMap<String, String>> list = new AdminDao().SearchCompanyOne(con,num);


		close(con);

		return list;

	}

	public int getCListCount() {

		Connection con = getConnection();
		int result = 0;

		result = new AdminDao().getClistCount(con);
		if(result != 0) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return result;

	}

	public ArrayList<RecommendResume> selectRefHisDetailList() {
		Connection con = getConnection();

		ArrayList<RecommendResume> list = new AdminDao().selectRefHisDetailList(con);

		close(con);

		return list;
	}
	//관리자 정산내역 - 전체 결제 내역 가져오기 (결제날짜에 따른 DESC) 
	public ArrayList<AllPayment> selectAllPayment(PageInfo pi) {
		Connection con = getConnection();

		ArrayList<AllPayment> list = new AdminDao().selectAllPayment(con , pi);

		close(con);

		return list;
	}
	//전체 게시물 갯수 가져오기
	public int getAllPaymentListCount() {
		Connection con = getConnection();

		int count = new AdminDao().getAllPaymentListCount(con);

		close(con);

		return count;
	}
	//검색에 대한 갯수 카운트
	public int getListCount(String text, int select, String radio) {
		Connection con = getConnection();

		int result = 0;
		String radio2 = "";

		if(radio.equals("person")) {
			radio = "P";
			//개인이면서 구매자정보를 검색하는 경우
			if(select == 1) {
				result = new AdminDao().getListCountPersonUserid(text, select , radio , con);
				//개인이면서 결제내용을 검색하는 경우
			}else if(select ==2 ) {
				result = new AdminDao().getListCountPersonPName(text, select , radio , con);

			}
		}else if(radio.equals("company")) {
			radio = "C";
			//기업이면서 구매자정보를 검색하는 경우
			if(select ==1 ) {
				result = new AdminDao().getListCountCompanyUserid(text , select , radio , con);
				//기업이면서 결제내용을 검색하는 경우
			}else if(select ==2) {
				result = new AdminDao().getListCountCompanyPName(text,select,radio,con);

			}
		}else {
			radio2 = "P";
			radio = "C";
			//기업이면서 구매자정보를 검색하는 경우
			if(select ==1) {
				//기업이면서 결제내용을 검색하는 경우
				result = new AdminDao().getListCountAllUserid(text,select,radio,radio2,con);
			}else if (select==2) {
				result = new AdminDao().getListCountAllPName(text,select ,radio,radio2,con);
			}
		}

		close(con);

		return result;
	}
	//갯수가아닌 ArrayList가져오기
	public ArrayList<AllPayment> searchPaymentList(String text, int select, String radio, PageInfo pi) {
		Connection con = getConnection();
		ArrayList<AllPayment> list = null;
		String radio2 = "";

		if(radio.equals("person")) {
			radio = "P";
			//개인이면서 구매자정보를 검색하는 경우
			if(select == 1) {
				list = new AdminDao().searchPersonUserid(con , text , radio , pi);
				//개인이면서 결제내용을 검색하는 경우
			}else if(select ==2 ) {
				list = new AdminDao().searchPersonPName(con , text , radio , pi);
			}
		}else if(radio.equals("company")) {
			radio = "C";
			//기업이면서 구매자정보를 검색하는 경우
			if(select ==1 ) {
				list = new AdminDao().searchCompanyUserid(con , text , radio , pi);
				//기업이면서 결제내용을 검색하는 경우
			}else if(select ==2) {
				list = new AdminDao().searchCompanyPName(con , text, radio , pi);
			}
		}else {
			radio2 = "P";
			radio = "C";
			//기업이면서 구매자정보를 검색하는 경우
			if(select ==1) {
				list = new AdminDao().searchAllUserid(con , text , radio , radio2 , pi);
				//기업이면서 결제내용을 검색하는 경우
			}else if (select==2) {
				list = new AdminDao().searchAllPName(con , text , radio , radio2 , pi);
			}
		}
		close(con);

		return list;
	}
	//광고배너 페이징을 위한 카운팅
	public int getAllAdverAcceptListCount() {
		Connection con = getConnection();

		int count = new AdminDao().getAllAdverAcceptListCount(con);

		close(con);

		return count;
	}
	//광고배너 리스트 불러오기 (페이징 처리)
	public ArrayList<AdverAccept> selectAllAdverList(PageInfo pi) {
		Connection con = getConnection();

		ArrayList<AdverAccept> list = new AdminDao().selectAllAdverList(pi , con);

		close(con);

		return list;
	}
	//관리자 광고 승인을 통한 paymentHistory insert
	public int insertYesAdver(int payid) {
		Connection con = getConnection();

		int result = new AdminDao().insertYesAdver(payid , con);

		if(result>0) {
			commit(con);
		}else {
			rollback(con);
		}

		close(con);

		return result;
	}
	//관리자 광고 거절을 통한 paymentHistory insert
	public int insertNoAdver(int payid, String text) {
		Connection con = getConnection();

		int result = new AdminDao().insertNoAdver(con , payid , text);

		if(result >0) {
			commit(con);
		}else {
			rollback(con);
		}

		return result;
	}

	public int getListNameCount(String text) {
		Connection con = getConnection();
		int result = 0;

		result = new AdminDao().getListNameCount(con,text);


		close(con);

		return result;
	}

	public int getListIdCount(String text) {
		Connection con = getConnection();
		int result = 0;

		result = new AdminDao().getListIdCount(con,text);


		close(con);

		return result;
	}

	public int getListEmailCount(String text) {
		Connection con = getConnection();
		int result = 0;

		result = new AdminDao().getListEmailCount(con,text);


		close(con);

		return result;
	}

	public ArrayList<HashMap<String, Object>> searchPersonName(String text, PageInfo pi) {
		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list = null;

		list = new AdminDao().searchPersonName(con, text, pi);

		close(con);

		return list;
	}

	public ArrayList<HashMap<String, Object>> searchPersonId(String text, PageInfo pi) {
		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list = null;

		list = new AdminDao().searchPersonId(con, text, pi);

		close(con);

		return list;
	}

	public ArrayList<HashMap<String, Object>> searchPersonEmail(String text, PageInfo pi) {
		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list = null;

		list = new AdminDao().searchPersonEmail(con, text, pi);

		close(con);

		return list;
	}
	//선택시 adverlist객체 가져오기
	public AdverAcceptUpdate AdverAcceptUpdate(int payid) {
		Connection con = getConnection();
		
		AdverAcceptUpdate auu = new AdminDao().AdverAcceptUpdate(payid , con);
		
		close(con);
		
		return auu;
	}
	//해당 사진의 경로  , 파일 이름 , 변경된 파일 이름 전달 ( update ) 
	public int UpdateAdverImg(AdverAttachment at, int payid) {
		Connection con = getConnection();
		
		int result = new AdminDao().updateAdverImg(con , at , payid);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public String selectNoAdverReason(int payid) {
		Connection con = getConnection();
		
		String reason = new AdminDao().selectNoAdverReason(payid , con);
		
		close(con);
		
		return reason;
	}

	public int getAllPopRecruitListCount() {
		Connection con = getConnection();
		
		int count = new AdminDao().getAllPopRecruitListCount(con);
				
		close(con);
		return count;
		
	}
	//전체 적극채용 공고 리스트 불러오기
	public ArrayList<AllPopRecruit> selectAllPopRecruitList(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<AllPopRecruit> list = new AdminDao().selectAllPopRecruitList(pi , con);
		
		close(con);
		
		return list;
	}

	public int getListRecruitCount() {
		Connection con = getConnection();
		int result = 0;

		result = new AdminDao().getListRecruitCount(con);
		if(result != 0) {
			commit(con);
		} else {
			rollback(con);
		}

		close(con);

		return result;
	}

	public ArrayList<HashMap<String, Object>> selectRecruitList(PageInfo pi) {
Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new AdminDao().selectRecruitList(con,pi);
		
		if(list != null) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return list;
	}

	public int deleteRecruit(int recid) {
		Connection con = getConnection();

		int result = new AdminDao().deleteRecruit(con,recid);

		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}

		close(con);




		return result;
	}
	//1. payid만가지고있으므로 pcode를 읽어온다.
	//읽어온 pcode가 1이면 a dao 처리  2 ~ 6이면  b 다오처리
	// 7~9이면 c dao 처리
	public int updateRefundCompleted(int payid) {
		Connection con = getConnection();
		//상품코드 가져오기
		int pcode = new AdminDao().updateRefundCompleted(payid , con);
		
		System.out.println("pcode : " + pcode);
		int resultOne = 0;
		int resultTwo = 0;
		int resultThree = 0;
		if(pcode == 1) {
//			System.out.println("적극채용공고");
			resultOne = new AdminDao().updatePopRecruit(payid, pcode , con);
			System.out.println("resultOne(결제상세이력 update여부) : " + resultOne);
			if(resultOne > 0) {
				resultTwo = new AdminDao().updatePopRecruit2(payid ,con);
				System.out.println("resultTwo(적극채용공고List 시작 , 종료날짜 update여부) : " + resultTwo);
				if(resultTwo > 0) {
					resultThree = resultTwo;
					commit(con);
				}else {
					rollback(con);
				}
			}else {
				rollback(con);
			}
		}else if(pcode >=2 && pcode <= 6){
			//System.out.println("인재열람");
			resultOne = new AdminDao().updatePopRecruit(payid , pcode , con);
			System.out.println("resultOne(결제상세이력 update여부) : " + resultOne );
			if(resultOne > 0) {
				resultTwo = new AdminDao().selectUno(payid , con);
				// uno 가져오기 ( 인재열람 ) 
				System.out.println("resultTwo(인재열람 Uno 가져오기) : " + resultTwo);
				if(resultTwo > 0) {
					resultThree = new AdminDao().updateViewCount(resultTwo , con);
					
					System.out.println("resultThree(인재열람Count 갯수 초기화)여부 : " + resultThree);
					if(resultThree > 0) {
						commit(con);
					}
				}else {
					rollback(con);
				}
			}else {
				rollback(con);
			}
		}else if(pcode >=7 && pcode <=9){
//			System.out.println("광고배너");
			resultOne = new AdminDao().updatePopRecruit(payid, pcode , con);
			System.out.println("resultOne(결제상세이력 update여부) : " + resultOne);
			
			if(resultOne > 0) {
				resultTwo = new AdminDao().updateBannerDate(payid , con);
				System.out.println("resultTwo (배너List 시작 , 종료날짜 update여부 ) : " + resultTwo);
				if(resultTwo > 0) {
					//resultThree return;
					resultThree = resultTwo;
					commit(con);
				}else {
					rollback(con);
				}
			}else {
				rollback(con);
			}
		}
		close(con);
		return resultThree;
	}
	//전체 거래갯수
	public int totalPaymentCount() {
		Connection con = getConnection();
		
		int result = new AdminDao().totalPaymentCount(con);
		
		close(con);
		
		return result;
	}
	//전체 매출
	public int totalPaymentMoney() {
		Connection con = getConnection();
		
		int result = new AdminDao().totalPaymentMoney(con);
		
		close(con);
		return result;
	}
	//오늘 매출
	public int todayPaymentMoney() {
		Connection con = getConnection();
		
		int result = new AdminDao().todayPaymentMoney(con);
		
		close(con);
		
		return result;
	}


}
