package admin.model.service;

import java.sql.Connection;
import java.util.ArrayList;

import admin.model.dao.RefResumeDao;
import admin.model.vo.RefResume;
import keyword.model.vo.PageInfo;

import static common.JDBCTemplate.*;

public class RefResumeService {

	public ArrayList<RefResume> selectRefResumeList(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<RefResume> reflist = new RefResumeDao().selectRefResumeList(con, pi);
		
		close(con);
		
		return reflist;
	}

	public int getListCount() {
		Connection con = getConnection();
		
		int result = new RefResumeDao().getListCount(con);
		
		close(con);
		
		return result;
	}

	public int getSearchListCountId(String text, String status2) {
		Connection con = getConnection();
		
		int result = new RefResumeDao().getSearchListCountId(con, text, status2);
		
		close(con);
		
		return result;
	}

	public ArrayList<RefResume> searchRefResumeId(String text, String status2, PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<RefResume> reflist = new RefResumeDao().searchRefResumeId(con, text,status2, pi);
		
		close(con);
		
		return reflist;
	}

	public int getSearchListCountQuarter(String text, String status2) {
		Connection con = getConnection();
		
		int result = new RefResumeDao().getSearchListCountQuarter(con, text, status2);
		
		close(con);
		
		return result;
	}

	public ArrayList<RefResume> searchRefResumeQuarter(String text, String status2, PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<RefResume> reflist = new RefResumeDao().searchRefResumeQuarter(con, text,status2, pi);
		
		close(con);
		
		return reflist;
	}

	public int insertRefDeny(int refid) {
		Connection con = getConnection();
		
		int result = new RefResumeDao().insertRefDeny(con, refid);
		
		if(result>0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}

}
