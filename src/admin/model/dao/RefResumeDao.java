package admin.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import admin.model.vo.RefResume;
import keyword.model.vo.PageInfo;
import static common.JDBCTemplate.*;

public class RefResumeDao {
	private Properties prop = new Properties();
	
	public RefResumeDao() {
		String fileName = RefResumeDao.class.getResource("/sql/admin/refResume-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<RefResume> selectRefResumeList(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<RefResume> reflist = null;
		
		String query = prop.getProperty("selectRefResumeList");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, "ACC");
			pstmt.setString(2, "");
			pstmt.setInt(3, startRow);
			pstmt.setInt(4, endRow);
			
			rset = pstmt.executeQuery();
			
			reflist = new ArrayList<>();
			while(rset.next()) {
				RefResume ref = new RefResume();
				ref.setRefId(rset.getInt("REFID"));
				ref.setRefHisId(rset.getInt("REFHIS_ID"));
				ref.setCategory(rset.getString("CATEGORY"));
				ref.setQuarter(rset.getString("QUARTER"));
				ref.setRefHisDate(rset.getDate("REFHIS_DATE"));
				ref.setUserId(rset.getString("USER_ID"));
				ref.setName(rset.getString("NAME"));
				ref.setRefHisContent(rset.getString("REFHIS_CONTENT"));
				ref.setUno(rset.getInt("UNO"));
				
				reflist.add(ref);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return reflist;
	}

	public int getListCount(Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, "ACC");
			pstmt.setString(2, "");
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public int getSearchListCountId(Connection con, String text, String status2) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getSearchListCountId");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, status2);
			pstmt.setString(2, text);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public ArrayList<RefResume> searchRefResumeId(Connection con, String text, String status2, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<RefResume> reflist = null;
		
		String query = prop.getProperty("searchRefResumeId");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, status2);
			pstmt.setString(2, text);
			pstmt.setInt(3, startRow);
			pstmt.setInt(4, endRow);
			
			rset = pstmt.executeQuery();
			
			reflist = new ArrayList<>();
			while(rset.next()) {
				RefResume ref = new RefResume();
				ref.setRefId(rset.getInt("REFID"));
				ref.setRefHisId(rset.getInt("REFHIS_ID"));
				ref.setCategory(rset.getString("CATEGORY"));
				ref.setQuarter(rset.getString("QUARTER"));
				ref.setRefHisDate(rset.getDate("REFHIS_DATE"));
				ref.setUserId(rset.getString("USER_ID"));
				ref.setName(rset.getString("NAME"));
				ref.setRefHisContent(rset.getString("REFHIS_CONTENT"));
				ref.setUno(rset.getInt("UNO"));

				
				reflist.add(ref);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return reflist;
	}

	public int getSearchListCountQuarter(Connection con, String text, String status2) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getSearchListCountQuarter");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, status2);
			pstmt.setString(2, text);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public ArrayList<RefResume> searchRefResumeQuarter(Connection con, String text, String status2, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<RefResume> reflist = null;
		
		String query = prop.getProperty("searchRefResumeQuarter");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, status2);
			pstmt.setString(2, text);
			pstmt.setInt(3, startRow);
			pstmt.setInt(4, endRow);
			
			rset = pstmt.executeQuery();
			
			reflist = new ArrayList<>();
			while(rset.next()) {
				RefResume ref = new RefResume();
				ref.setRefId(rset.getInt("REFID"));
				ref.setRefHisId(rset.getInt("REFHIS_ID"));
				ref.setCategory(rset.getString("CATEGORY"));
				ref.setQuarter(rset.getString("QUARTER"));
				ref.setRefHisDate(rset.getDate("REFHIS_DATE"));
				ref.setUserId(rset.getString("USER_ID"));
				ref.setName(rset.getString("NAME"));
				ref.setRefHisContent(rset.getString("REFHIS_CONTENT"));
				ref.setUno(rset.getInt("UNO"));
				
				reflist.add(ref);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return reflist;
	}

	public int insertRefDeny(Connection con, int refid) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertRefDeny");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, refid);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result;
	}
	
	
	
	

}






