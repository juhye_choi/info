package admin.model.dao;

import static common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import admin.model.vo.AdverAccept;
import admin.model.vo.AdverAcceptUpdate;
import admin.model.vo.AllPayment;
import admin.model.vo.AllPopRecruit;
import admin.model.vo.CompanyPageInfo;
import company.model.vo.AdverAttachment;
import company.model.vo.Company;
import keyword.model.vo.Keyword;
import keyword.model.vo.PageInfo;
import member.model.vo.Member;
import person.model.vo.Person;
import person.model.vo.RecommendResume;
import person.model.vo.RecruitInfo;
public class AdminDao {

	Properties prop = new Properties();

	public AdminDao() {

		String fileName = AdminDao.class.getResource("/sql/admin/admin-query.properties").getPath();

		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public ArrayList<RecommendResume> selectRefHisList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<RecommendResume> list = null;
		//ArrayList<Member> mlist = null;

		String query = prop.getProperty("selectRefHisList");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			list = new ArrayList<RecommendResume>();

			while(rset.next()) {
				RecommendResume rr = new RecommendResume();
				Member m = new Member();
				//mlist = new ArrayList<Member>();

				rr.setRefhidDate(rset.getDate("REFHIS_DATE"));
				rr.setCategory(rset.getString("CATEGORY"));
				rr.setRefhisContent(rset.getString("REFHIS_CONTENT"));
				rr.setRefId(rset.getInt("REFID"));
				rr.setRefhisId(rset.getInt("REFHIS_ID"));
				rr.setQuarter(rset.getString("QUARTER"));
				
				m.setUno(rset.getInt("UNO"));
				m.setUserName(rset.getString("NAME"));
				m.setUserId(rset.getString("USER_ID"));
				rr.setMember(m);
				
				list.add(rr);
				
				/*
				mlist.add(m);
				rr.setMemberList(mlist);
				list.add(rr);*/
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}

		return list;
	}

	public String selectQuarter(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		String result = null;

		String query = prop.getProperty("selectQuarter");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if(rset.next()) {
				result = rset.getString("QUARTER");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}

		return result;
	}

	public int selectRefId(Connection con, int uno, String quarter) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("selectRefId");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, uno);
			pstmt.setString(2, quarter);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt("REFID");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return result;
	}

	public int updateEnrollmentRefResume(Connection con, int refId) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("updateEnrollmentRefResume");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, refId);

			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	public int selectUno(Connection con, String userId) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("selectUno");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, userId);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt("UNO");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return result;
	}

	public int CompanionRef(Connection con, int refId, String content) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("CompanionRef");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, refId);
			pstmt.setString(2, content);

			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	public int selectCountCompany(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("selectCountCompany");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if(rset.next()) {
				result = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return result;
	}

	public int selectCountPerson(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("selectCountPerson");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if(rset.next()) {
				result = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return result;
	}

	public int selectCountRef(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("selectCountRef");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if(rset.next()) {
				result = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return result;
	}

	public ArrayList<HashMap<String, Object>> SearchPersonOne(Connection con, int num) {


		PreparedStatement pstmt = null;

		ResultSet rset = null;

		ArrayList<HashMap<String, Object>> list = null;
		Member m = null;
		Person p = null;

		String query = prop.getProperty("searchPersonOne");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, num);
			pstmt.setInt(2, num);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();


			if(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				hmap.put("name", rset.getString("NAME"));
				hmap.put("birth", rset.getDate("PBIRTH"));
				hmap.put("phone", rset.getString("PHONE"));
				hmap.put("gender",rset.getString("GENDER"));
				hmap.put("jobStatus", rset.getString("JOB_STATUS"));
				hmap.put("github", rset.getString("GITHUB"));
				hmap.put("blog", rset.getString("BLOG"));
				hmap.put("infoOpen",rset.getString("INFO_OPEN"));
				hmap.put("kosaYn", rset.getString("KOSA_YN"));
				hmap.put("userId", rset.getString("USER_ID"));
				hmap.put("changeName", rset.getString("CHANGE_NAME"));


				list.add(hmap);

			}









		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {

			close(pstmt);

			close(rset);
		}





		return list;
	}

	public int deletePerson(Connection con, int uno) {
		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("deletePerson");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, uno);

			result = pstmt.executeUpdate();


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {



			close(pstmt);
		}

		return result;
	}

	public int getListCount(Connection con) {



		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;


		String query = prop.getProperty("getListCount");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if(rset.next()) {
				result = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}

		return result;
	}

	public int getCompanylistCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("getCompanylistCount");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if(rset.next()) {
				result = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}

		return result;
	}

	public ArrayList<HashMap<String, Object>> selectCList(Connection con, CompanyPageInfo pi) {

		PreparedStatement pstmt = null;


		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;



		Member m = null;
		Company c = null;


		String query = prop.getProperty("selectCListWithPaging");

		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;


		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);


			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();

			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				hmap.put("uno", rset.getInt("UNO"));
				hmap.put("name", rset.getString("NAME"));
				hmap.put("userId", rset.getString("USER_ID"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("phone", rset.getString("PHONE"));

				hmap.put("registNum", rset.getString("REGIST_NUM"));
				hmap.put("owner", rset.getString("OWNER"));


				list.add(hmap);

			}


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {

			close(pstmt);

			close(rset);
		}




		return list;
	}

	public int ApprovalCompany(Connection con, int num) {
		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("ApprovalCompany");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);

			result = pstmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {



			close(pstmt);
		}

		return result;
	}

	public int deleteCompany(Connection con, int uno) {
		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("deleteCompany");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, uno);

			result = pstmt.executeUpdate();


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {



			close(pstmt);
		}

		return result;
	}

	public ArrayList<HashMap<String, String>> SearchCompanyOne(Connection con, int num) {

		PreparedStatement pstmt = null;

		ResultSet rset = null;

		ArrayList<HashMap<String, String>> list = null;
		Member m = null;
		Company p = null;

		String query = prop.getProperty("searchCompanyOne");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, num);


			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, String>>();


			if(rset.next()) {
				HashMap<String, String> hmap = new HashMap<String, String>();

				hmap.put("name", rset.getString("NAME"));

				hmap.put("phone", rset.getString("PHONE"));


				hmap.put("registNum", rset.getString("REGIST_NUM"));
				hmap.put("owner", rset.getString("OWNER"));


				list.add(hmap);

			}









		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {

			close(pstmt);

			close(rset);
		}





		return list;
	}

	public int getClistCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("getClistCount");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if(rset.next()) {
				result = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}

		return result;
	}
	//관리자 정산내역 - 전체 결제 내역 가져오기 (결제날짜에 따른 DESC) 
	public ArrayList<AllPayment> selectAllPayment(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<AllPayment> list = null;

		String query = prop.getProperty("selectAllPayment");

		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<AllPayment>();
			while(rset.next()) {
				AllPayment ap = new AllPayment();

				ap.setPayid(rset.getInt("PAYID"));
				ap.setMcat(rset.getString("MCAT"));
				ap.setPcat(rset.getInt("PCAT"));
				ap.setUserid(rset.getString("USERID"));
				ap.setPname(rset.getString("PNAME"));
				ap.setPmoney(rset.getInt("PMONEY"));
				ap.setPDate(rset.getDate("PDATE"));

				list.add(ap);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}

	public ArrayList<RecommendResume> selectRefHisDetailList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<RecommendResume> list = null;
		ArrayList<Member> mlist = null;

		String query = prop.getProperty("selectRefHisDetailList");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			list = new ArrayList<>();

			while(rset.next()) {
				RecommendResume rr = new RecommendResume();
				Member m = new Member();
				mlist = new ArrayList<Member>();

				rr.setRefhidDate(rset.getDate("REFHIS_DATE"));
				rr.setCategory(rset.getString("CATEGORY"));
				rr.setRefhisContent(rset.getString("CONTENT"));
				rr.setRefId(rset.getInt("REFID"));

				m.setUno(rset.getInt("UNO"));
				m.setUserName(rset.getString("NAME"));
				m.setUserId(rset.getString("USER_ID"));

				mlist.add(m);
				rr.setMemberList(mlist);
				list.add(rr);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}

		return list;
	}
	//전체 게시물 갯수 가져오기
	public int getAllPaymentListCount(Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0 ;

		String query = prop.getProperty("selectAllPaymentListCount");

		try {
			pstmt = con.prepareStatement(query);
			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return result;
	}

	//개인으로 선택하고 구매자정보를 검색하는 경우
	public int getListCountPersonUserid(String text, int select, String radio, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("CountPersonUserid");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setString(1, text);
			pstmt.setString(2, radio);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		return result;
	}
	//개인으로 선택하고 상품내용을 검색하는 경우
	public int getListCountPersonPName(String text, int select, String radio, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("CountPersonPName");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setString(1, text);
			pstmt.setString(2, radio);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return result;
	}

	//기업으로 선택하고 구매자정보를 검색하는 경우
	public int getListCountCompanyUserid(String text, int select, String radio, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0 ;

		String query = prop.getProperty("CountCompanyUserid");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setString(1, text);
			pstmt.setString(2, radio);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return result;
	}
	//기업으로 선택하고 상품내용을 검색하는 경우
	public int getListCountCompanyPName(String text, int select, String radio, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0 ;

		String query = prop.getProperty("CountCompanyPName");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setString(1, text);
			pstmt.setString(2, radio);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return result;
	}

	//전체로 선택하고 구매자정보를 검색하는 경우
	public int getListCountAllUserid(String text, int select, String radio, String radio2, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("CountAllUserid");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setString(1, text);
			pstmt.setString(2, radio);
			pstmt.setString(3, radio2);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return result;
	}
	//전체로 선택하고 상품내용을 검색하는 경우
	public int getListCountAllPName(String text, int select, String radio, String radio2, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("CountAllPName");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setString(1, text);
			pstmt.setString(2, radio);
			pstmt.setString(3, radio2);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return result;
	}
	//개인이면서 구매자정보를 검색하는 경우 (ArrayList)
	public ArrayList<AllPayment> searchPersonUserid(Connection con, String text, String radio, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<AllPayment> list = null;

		String query = prop.getProperty("searchPersonUserid");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		try {
			pstmt = con.prepareStatement(query);

			pstmt.setString(1, text);
			pstmt.setString(2, radio);
			pstmt.setInt(3, startRow);
			pstmt.setInt(4, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<AllPayment>();
			while(rset.next()) {
				AllPayment ap = new AllPayment();

				ap.setMcat(rset.getString("MCAT"));
				ap.setUserid(rset.getString("USERID"));
				ap.setPname(rset.getString("PNAME"));
				ap.setPmoney(rset.getInt("PMONEY"));
				ap.setPDate(rset.getDate("PDATE"));

				list.add(ap);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		return list;
	}
	//개인이면서 상품내용을 검색하는 경우 (ArrayList)
	public ArrayList<AllPayment> searchPersonPName(Connection con, String text, String radio, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<AllPayment> list = null;

		String query = prop.getProperty("searchPersonPName");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, text);
			pstmt.setString(2, radio);
			pstmt.setInt(3, startRow);
			pstmt.setInt(4, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<AllPayment>();
			while(rset.next()) {
				AllPayment ap = new AllPayment();

				ap.setMcat(rset.getString("MCAT"));
				ap.setUserid(rset.getString("USERID"));
				ap.setPname(rset.getString("PNAME"));
				ap.setPmoney(rset.getInt("PMONEY"));
				ap.setPDate(rset.getDate("PDATE"));

				list.add(ap);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}


		return list;
	}
	//기업으로 선택하고 구매자정보를 검색하는 경우
	public ArrayList<AllPayment> searchCompanyUserid(Connection con, String text, String radio, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<AllPayment> list = null;

		String query = prop.getProperty("searchCompanyUserid");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, text);
			pstmt.setString(2, radio);
			pstmt.setInt(3, startRow);
			pstmt.setInt(4, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<AllPayment>();

			while(rset.next()) {
				AllPayment ap = new AllPayment();

				ap.setMcat(rset.getString("MCAT"));
				ap.setUserid(rset.getString("USERID"));
				ap.setPname(rset.getString("PNAME"));
				ap.setPmoney(rset.getInt("PMONEY"));
				ap.setPDate(rset.getDate("PDATE"));

				list.add(ap);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}

		return list;
	}
	//기업으로 선택하고 상품내용을 검색하는 경우
	public ArrayList<AllPayment> searchCompanyPName(Connection con, String text, String radio, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<AllPayment> list = null;

		String query = prop.getProperty("searchCompanyPName");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, text);
			pstmt.setString(2, radio);
			pstmt.setInt(3, startRow);
			pstmt.setInt(4, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<AllPayment>();

			while(rset.next()) {
				AllPayment ap = new AllPayment();

				ap.setMcat(rset.getString("MCAT"));
				ap.setUserid(rset.getString("USERID"));
				ap.setPname(rset.getString("PNAME"));
				ap.setPmoney(rset.getInt("PMONEY"));
				ap.setPDate(rset.getDate("PDATE"));

				list.add(ap);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}



		return list;
	}
	//전체로 선택하고 구매자정보를 검색하는 경우
	public ArrayList<AllPayment> searchAllUserid(Connection con, String text, String radio, String radio2,
			PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<AllPayment> list = null;

		String query = prop.getProperty("searchAllUserid");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		try {
			pstmt = con.prepareStatement(query);

			pstmt.setString(1, text);
			pstmt.setString(2, radio2);
			pstmt.setString(3, radio);
			pstmt.setInt(4, startRow);
			pstmt.setInt(5, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<AllPayment>();

			while(rset.next()) {
				AllPayment ap = new AllPayment();

				ap.setMcat(rset.getString("MCAT"));
				ap.setUserid(rset.getString("USERID"));
				ap.setPname(rset.getString("PNAME"));
				ap.setPmoney(rset.getInt("PMONEY"));
				ap.setPDate(rset.getDate("PDATE"));

				list.add(ap);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}
	//전체로 선택하고 상품내용을 검색하는 경우
	public ArrayList<AllPayment> searchAllPName(Connection con, String text, String radio, String radio2, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<AllPayment> list = null;

		String query = prop.getProperty("searchAllPName");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setString(1, text);
			pstmt.setString(2, radio2);
			pstmt.setString(3, radio);
			pstmt.setInt(4, startRow);
			pstmt.setInt(5, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<AllPayment>();

			while(rset.next()) {
				AllPayment ap = new AllPayment();

				ap.setMcat(rset.getString("MCAT"));
				ap.setUserid(rset.getString("USERID"));
				ap.setPname(rset.getString("PNAME"));
				ap.setPmoney(rset.getInt("PMONEY"));
				ap.setPDate(rset.getDate("PDATE"));

				list.add(ap);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}


		return list;
	}
	//광고배너 페이징을 위한 카운팅	
	public int getAllAdverAcceptListCount(Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int count = 0;

		String query = prop.getProperty("AdverAcceptListCount");

		try {
			pstmt = con.prepareStatement(query);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				count = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return count;
	}
	//광고 배너 승인 + 페이징
	public ArrayList<AdverAccept> selectAllAdverList(PageInfo pi, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<AdverAccept> list = null;

		String query = prop.getProperty("selectAllAdverList");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<AdverAccept>();

			while(rset.next()) {
				AdverAccept aa = new AdverAccept();

				aa.setPDate(rset.getDate("PDATE"));
				aa.setSDate(rset.getDate("STARTDATE"));
				aa.setEDate(rset.getDate("ENDDATE"));
				aa.setCName(rset.getString("COMPANYNAME"));
				aa.setPName(rset.getString("PNAME"));
				aa.setPMoney(rset.getInt("PMONEY"));
				aa.setChangeName(rset.getString("CHANGENAME"));
				aa.setPhCategory(rset.getInt("PHCATEGORY"));
				aa.setPayid(rset.getInt("PAYID"));

				list.add(aa);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}
	//관리자 광고 승인을 통한 paymentHistory insert
	public int insertYesAdver(int payid, Connection con) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("insertYesAdver");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, payid);

			result = pstmt.executeUpdate();
		} catch (SQLException e1) {
			e1.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}
	//관리자 광고 거절을 통한 paymentHistory insert
	public int insertNoAdver(Connection con, int payid, String text) {
		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("insertNoAdver");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, payid);
			pstmt.setString(2, text);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}



	public int getListNameCount(Connection con, String text) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("getListNameCountText");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, text);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}

		return result;
	}

	public int getListIdCount(Connection con, String text) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("getListIdCountText");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, text);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}

		return result;
	}

	public int getListEmailCount(Connection con, String text) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;

		String query = prop.getProperty("getListEmailCountText");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, text);

			rset = pstmt.executeQuery();

			if(rset.next()) {
				result = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(pstmt);
		}

		return result;
	}

	public ArrayList<HashMap<String, Object>> searchPersonName(Connection con, String text, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		Member m = null;
		Person p = null;


		String query = prop.getProperty("searchPersonName");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, text);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				hmap.put("uno", rset.getInt("UNO"));
				hmap.put("name", rset.getString("NAME"));
				hmap.put("userId", rset.getString("USER_ID"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("nowPoint", rset.getInt("NOW_POINT"));


				list.add(hmap);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	public ArrayList<HashMap<String, Object>> searchPersonId(Connection con, String text, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		Member m = null;
		Person p = null;


		String query = prop.getProperty("searchPersonId");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, text);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				hmap.put("uno", rset.getInt("UNO"));
				hmap.put("name", rset.getString("NAME"));
				hmap.put("userId", rset.getString("USER_ID"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("nowPoint", rset.getInt("NOW_POINT"));


				list.add(hmap);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}

	public ArrayList<HashMap<String, Object>> searchPersonEmail(Connection con, String text, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;

		Member m = null;
		Person p = null;


		String query = prop.getProperty("searchPersonEmail");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, text);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);

			rset = pstmt.executeQuery();

			list = new ArrayList<HashMap<String, Object>>();
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();

				hmap.put("uno", rset.getInt("UNO"));
				hmap.put("name", rset.getString("NAME"));
				hmap.put("userId", rset.getString("USER_ID"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("nowPoint", rset.getInt("NOW_POINT"));


				list.add(hmap);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}

		return list;
	}
	//선택시 adverlist객체 가져오기
	public AdverAcceptUpdate AdverAcceptUpdate(int payid, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		AdverAcceptUpdate aau = null;
		
		String query = prop.getProperty("AdverUpdate");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, payid);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				aau = new AdverAcceptUpdate();
				
				aau.setPcode(rset.getInt("PCODE"));
				aau.setCName(rset.getString("NAME"));
				aau.setSDate(rset.getDate("B_START"));
				aau.setEDate(rset.getDate("B_FINISH"));
				aau.setPMoney(rset.getInt("PMONEY"));
				aau.setChangeName(rset.getString("CHANGE_NAME"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return aau;
	}
	//해당 사진의 경로  , 파일 이름 , 변경된 파일 이름 전달 ( update ) 
	public int updateAdverImg(Connection con, AdverAttachment at, int payid) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("updateAdverImg");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, at.getOriginName());
			pstmt.setString(2, at.getChangeName());
			pstmt.setString(3, at.getFilePath());
			pstmt.setInt(4, payid);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public String selectNoAdverReason(int payid, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String reason = "";
		
		String query = prop.getProperty("NoAdverReason");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, payid);
			pstmt.setInt(2, 3);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				reason = rset.getString(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return reason;
	}
	//적극채용중인 리스트 전체 불러오기
	public int getAllPopRecruitListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getAllPopRecruitListCount");
		
		try {
			stmt = con.createStatement();
			
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		
		return result;
	}
	//전체 적극채용 공고 리스트 불러오기
	public ArrayList<AllPopRecruit> selectAllPopRecruitList(PageInfo pi, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<AllPopRecruit> list = null;
		
		String query = prop.getProperty("selectAllPopRecruitList");
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			while(rset.next()) {
				AllPopRecruit apr = new AllPopRecruit();
				
				apr.setPayDate(rset.getDate("PAY_DATE"));
				apr.setSDate(rset.getDate("PR_START"));
				apr.setEDate(rset.getDate("PR_FINISH"));
				apr.setName(rset.getString("NAME"));
				apr.setPMoney(rset.getInt("PMONEY"));
				apr.setRecid(rset.getInt("RECID"));
				apr.setRTitle(rset.getString("REC_TITLE"));
				apr.setStatus(rset.getInt("STATUS"));
				
				list.add(apr);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return list;
	}

	public int getListRecruitCount(Connection con) {


		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;


		String query = prop.getProperty("getListRecruitCount");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			if(rset.next()) {
				result = rset.getInt(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
		}

		return result;
	}

	public ArrayList<HashMap<String, Object>> selectRecruitList(Connection con, PageInfo pi) {
		

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;	
		
		Member m = null;
		RecruitInfo r = null;
		
		String query = prop.getProperty("selectRecruitWithPaging");
		
		int startRow = (pi.getCurrentPage()-1) * pi.getLimit() +1;
		int endRow = startRow + 9;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				HashMap<String, Object> hmap = new HashMap<String, Object>();
				
				hmap.put("recId", rset.getInt("RECID"));
				hmap.put("name", rset.getString("NAME"));
				hmap.put("userId", rset.getString("USER_ID"));
				hmap.put("email", rset.getString("EMAIL"));
				hmap.put("recStart", rset.getDate("REC_START"));
				hmap.put("recFinish", rset.getDate("REC_Finish"));
				
				list.add(hmap);
				
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			
			close(pstmt);
			
			close(rset);
		}
		

		
		
		return list;
	}

	public int deleteRecruit(Connection con, int recid) {
		PreparedStatement pstmt = null;

		int result = 0;

		String query = prop.getProperty("deleteRecruit");

		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, recid);

			result = pstmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}

		return result;
	}

	//1. payid만가지고있으므로 pcode를 읽어온다.
	//읽어온 pcode가 1이면 a dao 처리  2 ~ 6이면  b 다오처리
	// 7~9이면 c dao 처리
	public int updateRefundCompleted(int payid, Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int pcode = 0;
		
		String query = prop.getProperty("selectPcode");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, payid);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				pcode = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return pcode;
	}
	// 적극채용공고 환불완료update (결제내역 ( history ) )
	// 인재열람 환불 update  (결제내역 ( history ) )
	// 광고배너 환불 update  (결제내역 ( history ) )   -> 전부 payid로 처리
	public int updatePopRecruit(int payid, int pcode, Connection con) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPaymentHistoryfromRefund");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, payid);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}
	// 적극채용공고 환불완료update (poplist 시작날짜 , 종료날짜 update)
	public int updatePopRecruit2(int payid, Connection con) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updatePopRecruitList");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, payid);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	// view_count update를 위한 selectuno
	public int selectUno(int payid , Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int uno = 0;
		
		String query = prop.getProperty("selectUno");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, payid);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				uno = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return uno;
	}
	//가져온 UNO를 바탕으로 UPDATE 여부 
	public int updateViewCount(int resultTwo, Connection con) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateViewCount");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, resultTwo);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	
	// 배너광고 update dates
	public int updateBannerDate(int payid, Connection con) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateBannerList");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, payid);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}
	//전체 거래갯수
	public int totalPaymentCount(Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("totalPaymentCount");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, 1);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}
	//전체 매출
	public int totalPaymentMoney(Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("totalPaymentMoney");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, 1);
			pstmt.setInt(2, 5);
			pstmt.setInt(3, 0);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}
	//오늘 매출
	public int todayPaymentMoney(Connection con) {
		PreparedStatement pstmt = null;
		ResultSet rset=  null;
		int result = 0;
		
		String query = prop.getProperty("todayPaymentMoney");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, 0);
			pstmt.setInt(2, 1);
			pstmt.setInt(3, 0);
			pstmt.setInt(4, 5);
			pstmt.setInt(5, 0);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public int insertPointforApproval(Connection con, int refid) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertPointforApproval");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, "추천이력서 등록 축하포인트 지급");
			pstmt.setInt(2, refid);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	
}