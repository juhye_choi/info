package admin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import member.model.vo.Member;
import person.model.service.PersonService;
import person.model.vo.Attachment2;
import resume.model.service.ResumeService;
import resume.model.vo.Resume;

@WebServlet("/selectOne.mre")
public class SelectMemberResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectMemberResumeServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		if(request.getSession().getAttribute("loginUser") != null){
			String category = ((Member) request.getSession().getAttribute("loginUser")).getCategory();
			
			if(category.equals("A")) {
				
				int uno = Integer.parseInt(request.getParameter("num"));
				//System.out.println("uno : " + uno);
				
				Resume resume = new ResumeService().selectResume(uno);
				HashMap<String, String> personInfo = new PersonService().selectPerson(uno);
				
				
				String page = "";
				if(resume != null) { 
					ArrayList<Attachment2> atlist = new PersonService().selectImg(uno);
					
					request.setAttribute("resume", resume);
					request.setAttribute("personInfo", personInfo);
					request.setAttribute("atlist", atlist);
					request.getRequestDispatcher("views/admin/memberResume.jsp").forward(request, response);
				
				} else { //이력서가 없을때! (NULL)
					request.setAttribute("msg", "이력서가 없습니다.");
					request.getRequestDispatcher("views/common/errorPage.jsp?errorCode=2").forward(request, response);
				}
				
				
			} else { //관리자가 아닐때!
				request.setAttribute("msg", "잘못된 접근입니다.");
				request.getRequestDispatcher("views/common/needLogin.jsp").forward(request, response);
			}
			
		} else {
			request.setAttribute("msg", "로그인이 필요한 메뉴입니다.");
			request.getRequestDispatcher("views/common/needLogin.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
