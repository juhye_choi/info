package admin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;

/**
 * Servlet implementation class SearchPersonOne
 */
@WebServlet("/SearchPersonOne.ad")
public class SearchPersonOne extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchPersonOne() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		
		int num = Integer.parseInt(request.getParameter("num"));
		
		ArrayList<HashMap<String, Object>> list  = new AdminService().SearchPersonOne(num);
		
		System.out.println("list 확인하자! " + list);
		
		String page = "";
		
		
		if(list != null) {
			
			
			page="views/admin/SearchPersonOne.jsp";
			request.setAttribute("list", list);
			
		}else {
			
page="views/common/errorPage.jsp";
			
			request.setAttribute("msg", "관리자 개인회원 개별조회 실패!");
			
		}
		
		request.getRequestDispatcher(page).forward(request, response);
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
