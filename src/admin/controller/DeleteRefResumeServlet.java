package admin.controller;



import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.RefResumeService;

@WebServlet("/delete.refall")
public class DeleteRefResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DeleteRefResumeServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int refid = Integer.parseInt(request.getParameter("num"));
		
		int result = new RefResumeService().insertRefDeny(refid);
		
		String page ="";
		if(result >0) {
			page = "selectList.refall";
			response.sendRedirect(page);
			
		} else {
			page = "views/common/errorPage.jsp?errorCode=2";
			request.setAttribute("msg", "등록취소에 실패하셨습니다!");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
