package admin.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.RefResumeService;
import admin.model.vo.RefResume;
import keyword.model.vo.PageInfo;

@WebServlet("/selectList.refall")
public class SelectRefResumeListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
    public SelectRefResumeListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//페이징 처리
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage=1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		limit = 10;
		
		int listCount = new RefResumeService().getListCount();

		maxPage = (int)((double)listCount/limit + 0.9);
		startPage = (((int)((double) currentPage/limit+0.9))-1) * 10 + 1;
		
		endPage = startPage + 10 -1;
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		
		ArrayList<RefResume> reflist = new RefResumeService().selectRefResumeList(pi);
		
		
		
		String page = "";
		if(reflist != null) {
			page = "views/admin/refResumeMgt.jsp";
			request.setAttribute("reflist", reflist);
			request.setAttribute("pi", pi);
			request.getRequestDispatcher(page).forward(request, response);
		} else {
			page = "views/common/errorPage.jsp?errorCode=2";
			request.setAttribute("msg", "추천이력서 이력 조회 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
