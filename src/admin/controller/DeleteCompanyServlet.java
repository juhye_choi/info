package admin.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;

/**
 * Servlet implementation class DeleteCompanyServlet
 */
@WebServlet("/deleteCompany.ad")
public class DeleteCompanyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteCompanyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int uno = Integer.parseInt(request.getParameter("num"));
		String type= request.getParameter("type");
		
		
		
		int result = new AdminService().deleteCompany(uno);
		
		
		
String page="";
		
		if(result > 0) {
			
			
			
			
		if(type.equals("1")) {
			response.sendRedirect("/info/SelectCompanyApproval.ad");
		}else if(type.equals("2")) {
			response.sendRedirect("/info/SelectCompanyListPage.ad");
			
		}
				
		
				
			
		
			
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg","탈퇴 실패");
			
			request.getRequestDispatcher(page).forward(request, response);
			
		}
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
