package admin.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;

/**
 * Servlet implementation class CompanionRefResumeServlet
 */
@WebServlet("/updateRefCom.rr")
public class CompanionRefResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CompanionRefResumeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//String userId = request.getParameter("userId");
		int refId = Integer.parseInt(request.getParameter("selectRefId"));
		String content = request.getParameter("content");
		
		//System.out.println(userId);
		//System.out.println(content);
		int result = new AdminService().CompanionRef(refId, content);
		
		if(result > 0) {
			response.sendRedirect("/info/selectRefList.rhl");
		}else {
			String page = "";
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "반려 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
