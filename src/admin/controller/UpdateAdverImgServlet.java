package admin.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;
import admin.model.vo.AdverAcceptUpdate;

@WebServlet("/updateAdverImg")
public class UpdateAdverImgServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UpdateAdverImgServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int payid = Integer.parseInt(request.getParameter("payid"));
		
		//System.out.println(payid);
		
		AdverAcceptUpdate aau = new AdminService().AdverAcceptUpdate(payid);
		
		//System.out.println(aau);
		
		String page = "";
		if(aau != null) {
			page = "views/admin/adverManage.jsp";
			request.setAttribute("aau", aau);
			request.setAttribute("payid", payid);
			request.getRequestDispatcher(page).forward(request, response);
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "선택한 광고 세부정보 불러오기 실패");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
