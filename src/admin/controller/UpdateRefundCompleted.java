package admin.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;

@WebServlet("/updateRefundcompleted")
public class UpdateRefundCompleted extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UpdateRefundCompleted() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int payid = Integer.parseInt(request.getParameter("payid"));
		//System.out.println(payid);
		
		//1. payid만가지고있으므로 pcode를 읽어온다.
		//읽어온 pcode가 1이면 a dao 처리  2 ~ 6이면  b 다오처리
		// 7~9이면 c dao 처리
		// a ( 적극채용공고 ) -> 해당 payid의 category를 5번 (환불완료) 로 처리
		// 해당 pop_recruit 에서 시작날짜 , 종료날짜 변경 (오늘의 하루전날로 변경 ) 
		int result = new AdminService().updateRefundCompleted(payid);
		
		System.out.println("result : " + result);
		String page = "";
		if(result > 0) {
			page = "views/common/successPage.jsp?successCode=7";
			request.getRequestDispatcher(page).forward(request, response);
		}else {
			
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
