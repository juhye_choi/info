package admin.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;

@WebServlet("/insertYesAdver")
public class InsertYesAdverServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InsertYesAdverServlet() {
        super(); 
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int payid = Integer.parseInt(request.getParameter("payid"));
		
		//System.out.println("payid : " + payid);
		
		int result = new AdminService().insertYesAdver(payid);
		
		System.out.println("result : " + result);
		
		
		String page = "";
		if(result > 0) {
			page = "/info/selectAdverAcc";
			response.sendRedirect(page);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "승인 실패!!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
