package admin.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;
import person.model.vo.RecommendResume;


@WebServlet("/selectRefList.rhl")
public class SelectRefHisListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public SelectRefHisListServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<RecommendResume> list = new AdminService().selectRefHisList();
		
		String page = "";
		if(list != null) {
//			ArrayList<RecommendResume> detailList = new AdminService().selectRefHisDetailList();
//			
//			if(detailList != null) {
				page="views/admin/requestRefResumeMgt.jsp";
				request.setAttribute("list", list);
				//request.setAttribute("detailList", detailList);
//			}else {
//				page="views/common/errorPage";
//				request.setAttribute("msg", "추천이력서 신청 간편 보기 조회 실패!");
//			}
			
		}else {
			page="views/common/errorPage";
			request.setAttribute("msg", "추천이력서 신청 모두 보기 조회 실패!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
