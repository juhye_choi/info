package admin.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;
import admin.model.vo.AllPayment;
import company.model.service.CompanyPaymentService;
import keyword.model.vo.PageInfo;
import member.model.vo.Member;

@WebServlet("/adminallPaymentList")
public class SelectAllPaymentListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public SelectAllPaymentListServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		//로그인이 있을 때
		if(loginUser!=null) {
				//페이징 처리
				int currentPage;
				int limit;
				int maxPage;
				int startPage;
				int endPage;

				currentPage=1;

				if(request.getParameter("currentPage") != null) {
					currentPage = Integer.parseInt(request.getParameter("currentPage"));
				}

				limit = 10;

				int listCount = new AdminService().getAllPaymentListCount();
				System.out.println("list : " + listCount);
				maxPage = (int)((double)listCount/limit + 0.9);
				startPage = (((int)((double) currentPage/limit+0.9))-1) * 10 + 1;

				endPage = startPage + 10 -1;
				if(maxPage < endPage) {
					endPage = maxPage;
				}

				PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);

				ArrayList<AllPayment> list = new AdminService().selectAllPayment(pi);
				
				int totalPaymentCount = new AdminService().totalPaymentCount();
				
				int totalPaymentMoney = new AdminService().totalPaymentMoney();
				
				int todayPaymentMoney = new AdminService().todayPaymentMoney();
				
				
				System.out.println("totalPaymentCount : " + totalPaymentCount );
				System.out.println("totalPaymentMoney : " + totalPaymentMoney);
				System.out.println("todayPaymentMoney : " + todayPaymentMoney);
				
				if(totalPaymentCount!=0 || totalPaymentMoney!=0 || todayPaymentMoney!=0) {
					
					String page = "";
					if(list !=null) {
						page = "views/admin/moneyInfo.jsp";
						request.setAttribute("list", list);
						request.setAttribute("pi", pi);
						request.setAttribute("totalPaymentCount", totalPaymentCount);
						request.setAttribute("totalPaymentMoney", totalPaymentMoney);
						request.setAttribute("todayPaymentMoney", todayPaymentMoney);
						request.getRequestDispatcher(page).forward(request, response);
					}else {
						page = "views/common/errorPage.jsp";
						request.setAttribute("msg", "list불러오기 실패");
						request.getRequestDispatcher(page).forward(request, response);
					}
				}
				
		//로그인이 없을때
		}else {
			System.out.println("??");
			request.getRequestDispatcher("views/common/companyLogin.jsp").forward(request, response);
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
