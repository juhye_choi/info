package admin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import admin.model.service.AdminService;
import admin.model.vo.AllPayment;
import keyword.model.vo.PageInfo;

@WebServlet("/searchAllPaymentList")
public class SearchAllPaymentListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SearchAllPaymentListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//선택한 라디오 버튼 ( 개인 , 기업 , 전체 value ) person 개인 , company 기업  , all 전체
		String radio = request.getParameter("searchRadio");
		//선택한 드롭박스   1 : 결제여부 , 2 : 구매자정보 , 3:결제내용
		int select = Integer.parseInt(request.getParameter("searchSelect"));
		// 작성한 text
		String text = request.getParameter("searchText");
		//System.out.println(radio);
		//System.out.println(select);
		//System.out.println(text);
		//페이징 처리
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage=1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		limit = 10;
		int listCount =0;
		System.out.println("??");
		//검색 갯수 Count
		listCount = new AdminService().getListCount(text , select , radio);
		
		System.out.println("listCount : " + listCount);
		
		maxPage = (int)((double)listCount/limit + 0.9);
		startPage = (((int)((double) currentPage/limit+0.9))-1) * 10 + 1;

		endPage = startPage + 10 -1;
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		ArrayList<AllPayment> list = new AdminService().searchPaymentList(text , select , radio , pi);
		
		JSONObject paymentListOjt = null;
		JSONObject piOjt = new JSONObject();
		JSONArray paymentList = new JSONArray();
		
		System.out.println("list : " + list);
		if(list!=null) {
			for(AllPayment ap : list) {
				paymentListOjt = new JSONObject();
				
				paymentListOjt.put("MCAT", ap.getMcat());
				paymentListOjt.put("USERID", ap.getUserid());
				paymentListOjt.put("PNAME", URLEncoder.encode(ap.getPname() , "UTF-8"));
				paymentListOjt.put("PMONEY", ap.getPmoney());
				paymentListOjt.put("PDATE", ap.getPDate().toString());
				
				paymentList.add(paymentListOjt);
			}
			
			System.out.println("paymentList : " + paymentList);
			
			piOjt.put("paymentList", paymentList);
			
			piOjt.put("scurrentPage", pi.getCurrentPage());
			piOjt.put("slistCount", pi.getListCount());
			piOjt.put("slimit", pi.getLimit());
			piOjt.put("smaxPage", pi.getMaxPage());
			piOjt.put("sstartPage", pi.getStartPage());
			piOjt.put("sendPage", pi.getEndPage());
			
			System.out.println("piOjt : " + piOjt);
		}
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		//out.print(keywordlist.toJSONString());
		out.print(piOjt.toJSONString());
		
		out.flush();
		out.close();
		System.out.println("out : " + out);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
