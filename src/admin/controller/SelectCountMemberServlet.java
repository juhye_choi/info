package admin.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;


@WebServlet("/selectCount.me")
public class SelectCountMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public SelectCountMemberServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		int countCompany = new AdminService().selectCountCompany();
		
		int countPerson = new AdminService().selectCountPerson();
		
		int countReference = new AdminService().selectCountRef();
		
		//System.out.println(countCompany + ", " + countPerson + ", " + countReference);
		
		if(countCompany!=0 || countPerson!=0 || countReference!=0) {
			request.setAttribute("countCompany", countCompany);
			request.setAttribute("countPerson", countPerson);
			request.setAttribute("countReference", countReference);
			
			request.getRequestDispatcher("views/main/adminMain.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		}
		
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
