package admin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;
import company.model.vo.CPageInfo;
import keyword.model.vo.PageInfo;
import member.model.service.MemberService;

/**
 * Servlet implementation class SelectCompanyListWithPaging
 */
@WebServlet("/SelectCompanyListPage.ad")
public class SelectCompanyListWithPaging extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectCompanyListWithPaging() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage=1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		limit = 10;
		
		int ClistCount = new AdminService().getCListCount();
		
		maxPage = (int)((double)ClistCount/limit + 0.9);
		startPage = (((int)((double) currentPage/limit+0.9))-1) * 10 + 1;
		
		endPage = startPage + 10 -1;
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		CPageInfo pi = new CPageInfo(currentPage, ClistCount, limit, maxPage, startPage, endPage);
		
		ArrayList<HashMap<String, Object>> list = new  MemberService().selectCList(pi);
		
		
		
		String page="";
		
		if(list != null) {
			page="views/admin/cCustomInfo.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
			
		}else {
			
			page="views/common/errorPgae.jsp";
			request.setAttribute("msg", "관리자 기업회원리스트 불러오기 실패");
			
			
		}
		request.getRequestDispatcher(page).forward(request, response);
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
