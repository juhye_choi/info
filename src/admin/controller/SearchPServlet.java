package admin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import admin.model.service.AdminService;
import keyword.model.service.KeywordService;
import keyword.model.vo.Keyword;
import keyword.model.vo.PageInfo;
import member.model.vo.Member;

/**
 * Servlet implementation class SearchPServlet
 */
@WebServlet("/searchP.ad")
public class SearchPServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchPServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int category = Integer.parseInt(request.getParameter("searchCategory"));
		String text = request.getParameter("searchText");
		
	
		
		//페이징 처리
				int currentPage;
				int limit;
				int maxPage;
				int startPage;
				int endPage;

				currentPage=1;
				
				if(request.getParameter("currentPage") != null) {
					currentPage = Integer.parseInt(request.getParameter("currentPage"));
				}
				limit = 10;
				int listCount =0;
				if(category == 1) {
					listCount = new AdminService().getListNameCount(text);
				}else if(category == 2) {
					listCount = new AdminService().getListIdCount(text);
				}else if(category == 3) {
					listCount = new AdminService().getListEmailCount(text);
				}
				
				
				maxPage = (int)((double)listCount/limit + 0.9);
				startPage = (((int)((double) currentPage/limit+0.9))-1) * 10 + 1;
				
				endPage = startPage + 10 -1;
				if(maxPage < endPage) {
					endPage = maxPage;
				}
				
				PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);

				ArrayList<HashMap<String, Object>> list = null;
				
				if(category == 1) {
					list = new AdminService().searchPersonName(text, pi);
				}else if(category == 2) {
					list = new AdminService().searchPersonId(text, pi);
				}else if(category == 3) {
					list = new AdminService().searchPersonEmail(text, pi);
				}
				
				
				
				JSONObject MemberOjt = null;
				JSONObject piOjt = new JSONObject();
				JSONArray Memberlist = new JSONArray();
				
				if(list != null) {
					
					for(HashMap<String, Object> hmap : list) {
					 MemberOjt = new JSONObject();
						
					 MemberOjt.put("uno", hmap.get("uno"));	
					 MemberOjt.put("name", URLEncoder.encode((String)hmap.get("name"),"UTF-8"));	
					 MemberOjt.put("userId", hmap.get("userId"));	
					 MemberOjt.put("email", hmap.get("email"));	
					 MemberOjt.put("nowPoint", hmap.get("nowPoint"));	
						
					 Memberlist.add(MemberOjt);
					}
					
					piOjt.put("Memberlist", Memberlist);
					
					piOjt.put("scurrentPage", pi.getCurrentPage());
					piOjt.put("slistCount", pi.getListCount());
					piOjt.put("slimit", pi.getLimit());
					piOjt.put("smaxPage", pi.getMaxPage());
					piOjt.put("sstartPage", pi.getStartPage());
					piOjt.put("sendPage", pi.getEndPage());
					
				}
				
					
					response.setContentType("application/json");
					PrintWriter out = response.getWriter();
					//한뭉탱이로 넘거야대니까 
					out.print(piOjt.toJSONString());
					
					
					
					out.flush();
					out.close();
					
				}
				
				
		
		
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
