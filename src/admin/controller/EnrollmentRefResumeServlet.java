package admin.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;


@WebServlet("/update.ref")
public class EnrollmentRefResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
    public EnrollmentRefResumeServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int refid = Integer.parseInt(request.getParameter("refid"));
		
		int result = new AdminService().EnrollmentRefResume(refid);
		
		if(result > 0) {
			response.sendRedirect("/info/selectRefList.rhl");
		}else {
			String page ="";
			page = "views/common/errorPage";
			request.setAttribute("msg", "추천 이력서 등록 처리 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
