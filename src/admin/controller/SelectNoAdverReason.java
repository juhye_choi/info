package admin.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;

@WebServlet("/selectNoAdverReason")
public class SelectNoAdverReason extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectNoAdverReason() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int payid = Integer.parseInt(request.getParameter("payid"));
		
		//System.out.println(payid);
		String reason = "";
		reason = new AdminService().selectNoAdverReason(payid);
		
		System.out.println(reason);
		
		response.setCharacterEncoding("UTF-8");
		
		response.getWriter().print(reason);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
