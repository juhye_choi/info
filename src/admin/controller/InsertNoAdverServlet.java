package admin.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;

@WebServlet("/insertNoAdver")
public class InsertNoAdverServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InsertNoAdverServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int payid = Integer.parseInt(request.getParameter("payid"));
		String text = request.getParameter("text");
		//System.out.println(payid);
		System.out.println(text);
		
		int result = new AdminService().insertNoAdver(payid , text);
		
		String page = "";
		if(result > 0) {
			page= "/info/selectAdverAcc";
			response.sendRedirect(page);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "거절 실패!!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
