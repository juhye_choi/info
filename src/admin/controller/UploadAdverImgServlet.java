package admin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.oreilly.servlet.MultipartRequest;

import admin.model.service.AdminService;
import common.MyFileRenamePolicy;
import company.model.vo.AdverAttachment;

@WebServlet("/updateAdverImg.up")
public class UploadAdverImgServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    public UploadAdverImgServlet() {
        super();
    }

    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(ServletFileUpload.isMultipartContent(request)) {
			
			int maxSize = 1024 * 1024 * 10; // 10Mbyte로 용량제한
			
			String root = request.getSession().getServletContext().getRealPath("/");
			
			String savePath = root + "attachment_uploadFiles/";
			
			MultipartRequest multiRequest = new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			//변환된 파일의 이름을 저장할 arrayList
			ArrayList<String> saveFiles = new ArrayList<>();
			
			//원본파일의 이름 저장할 arrayList
			ArrayList<String> originFiles = new ArrayList<>();
			
			//파일이 전송된 input태그의 name을 반환
			Enumeration<String> files = multiRequest.getFileNames();
			
			
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
			}
			
			String fileName = multiRequest.getParameter("file");
			int payid = Integer.parseInt(multiRequest.getParameter("payid"));
			
			AdverAttachment at = new AdverAttachment();
			
			at.setOriginName(originFiles.get(0));
			at.setChangeName(saveFiles.get(0));
			at.setFilePath(savePath);
			//해당 사진의 경로  , 파일 이름 , 변겨오딘 파일 이름 전달 ( update ) 
			int result = new AdminService().UpdateAdverImg(at , payid);
			
			PrintWriter out = response.getWriter();
			if(result > 0) {
				out.append("success");
				out.flush();
				out.close();
				
			//DB INSERT 실패
			}else {
				request.setAttribute("msg", "실패!!");
				request.getRequestDispatcher("views/commom/errorPage.jsp").forward(request, response);
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
