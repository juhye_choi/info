package admin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;
import keyword.model.vo.PageInfo;
import member.model.service.MemberService;

/**
 * Servlet implementation class SelectPersonListWithPaging
 */
@WebServlet("/SelectPersonListPage.ad")
public class SelectPersonListWithPaging extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectPersonListWithPaging() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		//페이징 처리
				int currentPage;
				int limit;
				int maxPage;
				int startPage;
				int endPage;
				
				currentPage=1;
		
				if(request.getParameter("currentPage") != null) {
					currentPage = Integer.parseInt(request.getParameter("currentPage"));
				}
		
				limit = 10;
		
				int listCount = new AdminService().getListCount();
		
				maxPage = (int)((double)listCount/limit + 0.9);
				startPage = (((int)((double) currentPage/limit+0.9))-1) * 10 + 1;
				
				endPage = startPage + 10 -1;
				if(maxPage < endPage) {
					endPage = maxPage;
				}
				
				PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
				
		
				ArrayList<HashMap<String, Object>> list = new  MemberService().selectPList(pi);
		
				
				
		String page="";
		
		if(list != null) {
			page="views/admin/pCustomInfo.jsp";
			request.setAttribute("list", list);
			request.setAttribute("pi", pi);
			
		}else {
			
			page="views/common/errorPgae.jsp";
			request.setAttribute("msg", "관리자 개인회원리스트 불러오기 실패");
			
			
		}
		request.getRequestDispatcher(page).forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
