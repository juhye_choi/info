package admin.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;

/**
 * Servlet implementation class DeleteRecruitServlet
 */
@WebServlet("/DeleteRecruit.ad")
public class DeleteRecruitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteRecruitServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		int recid = Integer.parseInt(request.getParameter("RECID"));
		int type = 0;
		type = Integer.parseInt(request.getParameter("type"));
		
		int result = new AdminService().deleteRecruit(recid);
		String page="";
		
		if(result > 0) {
			if(type == 1) {
				response.sendRedirect("/info/selectList.rec");
			}else {
				response.sendRedirect("/info/SelectCompanyRecruitList.ad?currentPage=1");
			}
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg","관리자 채용공고 삭제 실패");
			
			request.getRequestDispatcher(page).forward(request, response);
			
		}
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
