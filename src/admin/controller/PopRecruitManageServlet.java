package admin.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import admin.model.service.AdminService;
import admin.model.vo.AllPopRecruit;
import keyword.model.vo.PageInfo;
import member.model.vo.Member;

@WebServlet("/popRecruitListManage")
public class PopRecruitManageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PopRecruitManageServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		
		if(loginUser!=null) {
			
			//페이징 처리
			int currentPage;
			int limit;
			int maxPage;
			int startPage;
			int endPage;

			currentPage=1;

			if(request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}

			limit = 10;
			
			int listCount = new AdminService().getAllPopRecruitListCount();
			
			System.out.println(listCount);
			
			maxPage = (int)((double)listCount/limit + 0.9);
			startPage = (((int)((double) currentPage/limit+0.9))-1) * 10 + 1;

			endPage = startPage + 10 -1;
			if(maxPage < endPage) {
				endPage = maxPage;
			}

			PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);

			ArrayList<AllPopRecruit> list = new AdminService().selectAllPopRecruitList(pi);
			
			//System.out.println(list);
			
			String page = "";
			if(list!=null) {
				page = "views/admin/jobAdverManage.jsp";
				request.setAttribute("list", list);
				request.setAttribute("pi", pi);
				request.getRequestDispatcher(page).forward(request, response);
			}else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "불러오기 실패");
				request.getRequestDispatcher(page).forward(request, response);
			}
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
