package admin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import admin.model.service.RefResumeService;
import admin.model.vo.RefResume;
import keyword.model.vo.PageInfo;

@WebServlet("/search.refall")
public class SearchRefResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SearchRefResumeServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		int category = Integer.parseInt(request.getParameter("searchCategory"));
		String text = request.getParameter("searchText");
		String status = request.getParameter("status");
		
		//System.out.println("text -->" + text);
		
		//페이징 처리
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;

		currentPage=1;

		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		String status2 = "";
		switch(status) {
			case "conf" : status2 = "ACC"; break;
			case "apply" : status2 = "REQ"; break;
			case "rej" : status2 = "DEN"; break;
			case "fin" : status2 = "END"; break;
			case "all" : status2 = ""; break;
		}
		
		limit = 10;
		int listCount = 0;
		if(category == 1) { //아이디 검색!
			listCount = new RefResumeService().getSearchListCountId(text, status2);
			
		} else { // 해당 분기검색!
			listCount = new RefResumeService().getSearchListCountQuarter(text, status2);
		}
		
		maxPage = (int)((double)listCount/limit + 0.9);
		startPage = (((int)((double) currentPage/limit+0.9))-1) * 10 + 1;

		endPage = startPage + 10 -1;
		if(maxPage < endPage) {
			endPage = maxPage;
		}

		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		ArrayList<RefResume> reflist = null;
		if(category == 1) { //아이디 검색!
			reflist = new RefResumeService().searchRefResumeId(text, status2, pi);
		} else {
			reflist = new RefResumeService().searchRefResumeQuarter(text, status2, pi);
		}
		
		//System.out.println(reflist);
		
		JSONObject refResumeObj = null;
		JSONObject outerObj = new JSONObject();
		JSONArray refResumelist = new JSONArray();
		
		if(reflist != null) {
			for(RefResume ref : reflist) {
				refResumeObj = new JSONObject();
				
				refResumeObj.put("refId", ref.getRefId());
				refResumeObj.put("refHisId", ref.getRefHisId());
				refResumeObj.put("status", ref.getCategory());
				refResumeObj.put("quarter", ref.getQuarter());
				refResumeObj.put("refHisDate", ref.getRefHisDate().toString());
				refResumeObj.put("userId", ref.getUserId());
				refResumeObj.put("uno", ref.getUno());
				refResumeObj.put("name", URLEncoder.encode(ref.getName(), "UTF-8")); //인코딩!
				if(ref.getRefHisContent() != null) {
					refResumeObj.put("content", URLEncoder.encode(ref.getRefHisContent(), "UTF-8")); //인코딩!					
				} else {
					refResumeObj.put("content", ref.getRefHisContent());
				}
				
				refResumelist.add(refResumeObj);
			}
			
			outerObj.put("refResumelist", refResumelist);
			
			outerObj.put("scurrentPage", pi.getCurrentPage());
			outerObj.put("slistCount", pi.getListCount());
			outerObj.put("slimit", pi.getLimit());
			outerObj.put("smaxPage", pi.getMaxPage());
			outerObj.put("sstartPage", pi.getStartPage());
			outerObj.put("sendPage", pi.getEndPage());
		}
		
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.print(outerObj.toJSONString());
		
		out.flush();
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
