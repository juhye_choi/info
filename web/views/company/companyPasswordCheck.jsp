<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<%@ include file="/views/common/import.html" %>

<style>
body{
		text-align:center;
		padding-top:5%;
		margin:auto;
	}
	
	.memberChange{
		width:15%;
		height:40px;
		margin:3px;
		background:white;
		border:2px solid black;
		font-size:17px;
	}
	.memberChange:hover{
	cursor:pointer;
}
	
	#passwordCheck {
		background:#CC33FF;
		color:white;
	}
	
		#allDiv {
	margin-left: 13%;
	margin-right: 8%;
	margin-top: 25px;
}

		#div {
	margin-left: 10%;
	margin-right: 8%;
	margin-top: 50px;
}

		#div2 {
	margin-left: 2%;
	margin-right: 8%;
	margin-top: 20px;
}




td {
	vertical-align: top;
	padding-top: 10px;
}



</style>
</head>
<body>

<%@ include file="../common/companyMenu.jsp" %>


<h1>기업회원 비밀번호 찾기</h1>

<div style="margin-top:30px;">
<a href="<%= request.getContextPath() %>/views/company/companyIdCheck.jsp"><button class="memberChange" id="idCheck">아이디 찾기</button></a>
	<a href="<%= request.getContextPath() %>/views/company/companyPasswordCheck.jsp"><button class="memberChange" id="passwordCheck">비밀번호 찾기</button></a>
	<br>

</div>

<div id="div">
<h4>회원정보에 등록된 정보로 비밀번호를 찾을 수 있습니다.</h4>

</div>

<div id="allDiv">


<form id="passSearchForm" action="<%=request.getContextPath()%>/CpassSearchFinish.me" method="post">
<table align="center">

<tr>
<td text-align="center">아이디</td>
<td><div class="ui input"><input type="text" id="personId" name="personId"placeholder=""></div></td>
<td></td>
</tr>

<tr>
<td text-align="center">회사명</td>
<td><div class="ui input"><input type="text" id="personName" name="personName"placeholder=""></div></td>
<td></td>
</tr>

<tr>
<td text-align="center">이메일</td>
<td><div class="ui input"><input type="email" name="personEmail" id="personEmail"></div></td>
<td  style="padding-left:15px;"><input type="button" class="ui button" id="emailBtn" value="인증번호 발송" style="width: 130px;"></td>
</tr>

<tr>
<td text-align="center">인증번호</td>
<td><div class="ui input"><input type="text" name="checkNum" id="checkNum" placeholder=""></div></td>
<td style="padding-left:15px;"><input type="button" id="emailSend" class="ui button" style="width: 130px;"
value="확인" > </td>

<td><div class="countdown" style="color:red; padding-top:10px;" id="countdown"></div></td>
</tr>

<tr>
					
					<td><input type="hidden" readonly="readonly" name="code_check" id="code_check" value="<%= getRandom()%>"></td>
					
					</tr>

</table>

<div id="div2">

<input type="button"  class="ui secondary button" value="인증확인" id="PassSearch">
</div>



</div>




</form>


<%! public int getRandom() {
		int random = 0;
		random = (int)Math.floor((Math.random() * (99999-10000+1))) + 10000;
		return random;
		
		}%>

<script>





$("#emailBtn").click(function(){
	
	
	
	
	var personName = $("#personName").val();
	var personEmail = $("#personEmail").val();
	var personId = $("#personId").val();
	 var code = $("#code_check").val();
	
	$.ajax({
		url : "/info/CpassSearch.me",
		type : "post",
		data : {
			personName : personName,
			personEmail : personEmail,
			personId : personId
		},
		success : function(data) {
			if(data === "search") {
				
				alert("해당 이메일로 인증 발송");
				
				$.ajax({
					 url : "/info/pJoinEmailSend",	
					 type : "post",
					 data : {
						  code : code,
						  email : personEmail
						},
success : function(data) {
							
							
							
							
							
						},
						error : function() {
							console.log("실패!");
						}
					 
					
				})
			
		
				
				
				
				
				
				
				
				
				$("#checkNum").attr("disabled",false);
				 $("#emailSend").attr("disabled",false);
				 
				 var timer2 = "3:00";
					var interval = setInterval(function() {


					  var timer = timer2.split(':');
					  //by parsing integer, I avoid all extra string processing
					  var minutes = parseInt(timer[0], 10);
					  var seconds = parseInt(timer[1], 10);
					  --seconds;
					  minutes = (seconds < 0) ? --minutes : minutes;
					  if (minutes < 0) clearInterval(interval);
					  seconds = (seconds < 0) ? 59 : seconds;
					  seconds = (seconds < 10) ? '0' + seconds : seconds;
					 
					  
					  if(minutes==0 && seconds==00) {
						  
						  $("#checkNum").val("");
						  $("#checkNum").attr("disabled",true);
						
						
						 $("#emailBtn").attr("value","재발송");
						
						 clearInterval(interval);
						 
					  }
					  
				
					  $('.countdown').html(minutes + ':' + seconds);
					  timer2 = minutes + ':' + seconds;
					  
					 
					  
					  
					}, 1000);
				
			}else {
				
				alert("입력정보를 다시 확인해주세요");
				
			}
			
			
			
		},
		error : function() {
			
			
		}
		
		
	})
	
	
	
})

$(function(){


$("#checkNum").attr("disabled",true);
$("#emailSend").attr("disabled",true);

})

$("#emailSend").click(function(){
	
	var code = $("#code_check").val();
	
	var checkNum = $("#checkNum").val();
	
	if(code == checkNum) {
		
		
		alert("이메일 인증이 완료되었습니다.");
		 $("#checkNum").attr("disabled",true);
		 $('.countdown').remove();
		 
		 $("#PassSearch").click(function(){
			 
		$("#passSearchForm").submit();
			 
			 
		 })
		 
		
	}else {
		
		alert("인증번호가 틀렸습니다.");
		
		
		
	}

	
	
	
	
})














</script>



</body>
</html>