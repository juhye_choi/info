<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/semantic/semantic.min.css">
<script src="<%=request.getContextPath()%>/semantic/semantic.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
body {
	text-align: center;
	padding-top: 5%;
	margin: auto;
}

.memberChange {
	width: 15%;
	height: 8%;
	margin: 3px;
	background: white;
	border: 2px solid black;
	font-size: 17px;
}

.memberChange:hover {
	cursor: pointer;
}

#enterprise {
	background: #CC33FF;
	color: white;
}

/* table {
	margin-top: 30px;
} */

#allDiv {
	margin-left: 8%;
	margin-right: 8%;
	margin-top: 50px;
}

td {
	vertical-align: top;
	padding-top: 10px;
}

.searchSection {
	margin-top: 70px;
	padding-right: 20%;
	padding-left: 20%;
	/* padding-bottom:20px; */
	/*  border:1px solid black;  */
	/* border-top:2px solid black;  */
	text-align: center;
}

#profileImgArea {
	width:200px;
	height:150px;
}

#profileImg {
	 width:150px;
	 height:150px;
	 /* margin:50px; */
	 border:1px solid black;
}

#profileImgArea:hover {
cursor:pointer;


}
</style>
</head>
<body>


	<a href="<%=request.getContextPath()%>/index.jsp"><img
		src="<%=request.getContextPath()%>/images/logo.PNG" alt="메인로고"
		width="40%"></a>
	<br>
	<a href="<%=request.getContextPath()%>/views/person/personjoinForm.jsp"><button
			class="memberChange" id="personal">개인</button></a>
	<a
		href="<%=request.getContextPath()%>/views/company/companyjoinForm.jsp"><button
			class="memberChange" id="enterprise">기업</button></a>
	<br>

	<div>
	
	<div style="margin-top: 10px; color: red;">*은 필수 항목 표시입니다.</div>




			<hr>
	
	
	<form id="frm" enctype="multipart/form-data" method="post">

<table align="center" style="margin-bottom:30px;">
	
	
	
	 <tr style="margin-left: 15px;">
	<td ><label> 사업자등록증 사진</label></td>
	<td ><div id="profileImgArea">
					
						
						<img id="profileImg" src="" >
						
					</div></td>
	<td ><input type="button" id="uploadbutton" class="ui button" value="첨부하기" style="width: 130px;  margin-left:100px;"></td>
	</tr> 

<tr>

</tr>

</table>

<div id="fileArea">
			<input type="file" id="file" name="file" style="width: 300px;" onchange="loadImg(this)">
			
			</div>


</form>

		<form id="joinForm"
			action="<%=request.getContextPath()%>/cInsertMember.me" method="post">


			
			<table align="center" style="margin-top:10px;">

<tr >

					<td><label>* 사업자 등록번호</label></td>
					<td style="width:300px;" ><div class="ui input">
							<input type="text" id="companyNumber" name="companyNumber"
								placeholder="" maxlength="10" style="width:250px;">
						</div></td>
					<td style="padding-left: 15px;"><input type="button" class="ui button"
							style="width: 130px;"   id="companyNumberCheck" value="확인"></td>

				</tr>


				<tr>

					<td><label>* 아이디</label></td>
					<td style="width:300px;"><div class="ui input">
							<input type="text" name="userId" placeholder="" id="userId" style="width:250px;">
						</div></td>
					<td style="padding-left: 15px;"><label id="id-success"
						style="width: 150px; color: green;">사용가능한 아이디</label> <label
						id="id-danger" style="width: 150px; color: red;">사용불가 아이디</label></td>

				</tr>
				
				<tr>
				<td colspan="3"><span id="idNum" style="color:red; padding-right:30px;">6~12자의 소문자,숫자로만 사용가능합니다.</span></td>
				
				</tr>

				<tr>

					<td ><label>* 비밀번호</label></td>
					<td style="width:300px;"><div class="ui input">
							<input type="password" name="userPwd" id="userPwd"style="width:250px;"
								placeholder="">
						</div></td>
					<td></td>

				</tr>
				
				

				<tr>

					<td><label>* 비밀번호확인</label></td>
					<td style="width:300px;"><div class="ui input">
							<input type="password" name="userPwd2" id="userPwd2"
								placeholder="" style="width:250px;">
						</div></td>
					<td style="padding-left: 15px;"><label id="alert-success"
						style="width: 150px; color: green;">비밀번호 사용가능</label> <label
						id="alert-danger" style="width: 150px; color: red;">비밀번호
							사용불가능</label></td>

				</tr>
				
<tr>
				<td colspan="3"><span id="passNum" style="color:red; padding-left:70px;">6~20자에 숫자,특수문자 1개이상 포함 후 사용가능합니다.</span></td>
				
				</tr>



				<tr>

					<td><label>* 이메일</label></td>
					<td style="width:300px;"><div class="ui input">
							<input type="email" name="email" placeholder="" id="email" style="width:250px;">
						</div></td>

					<td style="padding-left: 15px;"><label id="email-success"
						style="width: 150px; color: green;">사용가능한 이메일</label> <label
						id="email-danger" style="width: 150px; color: red;">사용불가
							이메일</label></td>

				</tr>
				<tr>

					<td></td>
					<td style="width:300px;"><div class="ui input">
							<input type="text" name="emailCheck" placeholder="인증번호 확인" style="width:250px;"
								id="emailCheck">
						</div></td>
					<td style="padding-left: 15px;"><input type="button"
						id="emailBtn" class="ui button" style="width: 130px;"
						value="인증번호 발송"> <input type="button" id="emailSend"
						class="ui button" style="width: 130px; display: none" value="확인"></td>

					<td><div class="countdown"
							style="color: red; padding-top: 10px;" id="countdown"></div></td>

				</tr>










				<tr>

					<td><label>* 회사명</label></td>
					<td style="width:300px;"><div class="ui input">
							<input type="text" id="companyName" name="companyName"
								placeholder="" style="width:250px;">
						</div></td>
					<td></td>

				</tr>

				<tr>

					<td><label>* 대표자명</label></td>
					<td style="width:300px;"><div class="ui input">
							<input type="text" id="ceoName" name="ceoName" placeholder="" style="width:250px;">
						</div></td>
					<td></td>

				</tr>

				<tr>

					<td><label>* 인사 담당자명</label></td>
					<td style="width:300px;"><div class="ui input">
							<input type="text" id="managerName" name="managerName" style="width:250px;"
								placeholder="">
						</div></td>
					<td></td>

				</tr>




				<tr>
					<td>연락처</td>
					<td><div class="ui input">
							<input type="text" maxlength="3" name="tel1" size="2" id="tel1" style="width:75px;">
						</div>-
						<div class="ui input">
							<input type="text" maxlength="4" name="tel2" size="2" id="tel2" style="width:78px;">
						</div>-
						<div class="ui input">
							<input type="text" maxlength="4" name="tel3" size="2" id="tel3" style="width:78px;">
						</div></td>
					<td></td>
				</tr>


				<tr>

					<td><input type="hidden" readonly="readonly" name="code_check"
						id="code_check" value="<%=getRandom()%>"></td>

				</tr>



				





			</table>

			<div class="searchSection">

				<h3>기업 회원 약관 (개정 및 적용 2019.12.04)</h3>



				<div
					style="overflow: scroll; overflow-x: hidden; width: 100%; height: 300px; text-align: left;">

				
<br>
제1조 (목적)<br>
본 약관은 ㈜잇덕(이하 "회사")이 운영하는 웹사이트(이하 “사이트”)를 통해 인터넷 관련 서비스를 제공함에 있어, 회사가 제공하는 서비스와 관련하여, 이를 이용하는 가입자(이하 “회원” 또는 “기업회원”)의 이용조건 및 제반 절차, 기타 필요한 사항을 규정함을 목적으로 한다.<br>
<br><br>
제2조 (용어의 정의)<br>
본 약관에서 사용하는 용어의 정의는 아래와 같습니다.<br>
① "사이트"라 함은 회사가 서비스를 이용자에게 제공하기 위하여 제작, 운영하는 사이트를 말합니다. 현재 회사가 운영하는 사이트의 접속 주소는 아래와 같습니다.<br>
- www.ITDuck.co.kr<br>
② "서비스"라 함은 채용정보, 이력서 및 기업정보 제공 기타의 서비스를 통하여 구직·채용시장에서 구직자와 기업의 연결을 돕는 플랫폼 서비스입니다. 구체적으로는 회사가 기업 또는 구직자가 구인, 구직과 교육을 목적으로 등록하는 각종 자료를 DB화하여 각각의 목적에 맞게 분류 가공, 집계하여 정보를 제공하는 서비스 및 기타 구인 및 구직이 원활히 이루어지도록 하기 위하여 사이트 등에서 제공하는 모든 서비스를 말합니다. 일부 서비스는 유료로 제공될 수 있습니다. <br>
③ "이용자"라 함은 사이트 등에 접속하여 본 약관에 따라 회사가 제공하는 서비스를 이용하는 회원(기업회원 및 개인회원을 포함) 및 비회원을 말합니다.<br>
④ "기업회원"이라 함은 채용을 희망하는 기업으로서 본 서비스를 이용하기 위하여 본 약관에 동의하고 회사와 이용계약을 체결하여 기업회원 ID를 부여받은 이용자를 말합니다. 회사는 유료 서비스 등에 있어서 기업회원의 분류에 따라 권리의무를 다르게 설정할 수 있습니다.<br>
⑤ "ID" 또는 "기업회원 ID"라 함은 기업회원의 식별 및 서비스 이용을 위하여 기업회원이 선정하고 회사가 부여하는 문자와 숫자의 조합을 말합니다.<br>
⑥ "비밀번호"라 함은 회사의 서비스를 이용하려는 사람이 기업회원 ID를 부여 받은 자와 동일인임을 확인하고 기업회원의 권익을 보호하기 위하여 기업회원이 선정한 문자와 숫자의 조합을 말합니다.<br>
⑦ "꽥포인트"라 함은 회사의 서비스를 이용할 경우 회사가 정한 포인트 정책에 따라 기업회원에게 적립되어 결제수단으로 이용 가능한 포인트입니다.<br>
<br><br>
제3조 (약관의 명시와 개정)<br>
① 회사는 약관의 규제 등에 관한 법률, 정보통신망 이용 촉진 및 정보 보호 등에 관한 법률, 개인정보 보호법, 전기통신기본법, 전기통신사업법 등 관련 법령을 위배하지 않는 범위에서 본 약관을 개정할 수 있습니다.<br>
② 만약, 기업회원이 변경 약관 적용에 대한 거부 의사를 표시하지 않고 서비스를 계속 이용하는 경우, 회사는 기업회원이 변경 약관 적용에 동의하는 것으로 간주합니다.<br>
<br><br>
제4조 (약관 외 준칙)<br>
① 본 약관에서 규정하지 않은 사항은 약관의 규제 등에 관한 법률, 정보통신망 이용촉진 및 정보보호 등에 관한 법률, 개인정보 보호법, 전기통신기본법, 전기통신사업법 등의 관계법령에 따라 규율됩니다.<br>
② 기업회원이 유료 서비스를 구입하는 경우 유료 서비스 이용과 관련하여 본 약관에 우선하여 유료 서비스 판매약관(이하 “판매약관”이라 합니다)이 적용됩니다. 그 밖에 회사가 운영하는 개별 서비스 이용약관이 별도로 있는 경우 해당 서비스 이용약관이 본 약관에 우선하여 적용됩니다.<br>
<br><br>
제5조 (서비스 이용계약의 성립)<br>
① 서비스 이용계약은 기업회원 서비스를 이용하고자 하는 자(이하 “이용희망자”라 합니다)의 본 약관과 개인정보 처리방침의 내용에 대한 동의 및 회원 가입 신청에 대하여 회사가 승낙함으로써 성립합니다.<br>
② 이용희망자가 기업회원 가입 신청시 사이트 등의 기업회원 가입 화면 중 "동의함" 버튼을 누르면 본 약관 및 개인정보 처리방침을 충분히 읽고 동의한 것으로 간주됩니다.<br>
<br><br>
제6조 (회원 가입 신청)<br>
① 이용희망자는 기업회원 가입 신청시 본 약관 및 회사의 개인정보 처리방침에서 정한 필수 수집항목 등에 기재된 정보를 제공하여야 합니다.<br>
② 이용희망자는 기업회원 가입 신청시 반드시 회원 인증을 거쳐야 하며, 회원 인증시 정확한 회사명과 사업자등록번호를 제공하여야만 서비스를 이용할 수 있습니다. <br>
<br><br>
제7조 (기업회원 가입 신청의 승낙과 제한)<br>
① 회사는 기업회원 가입 신청을 한 이용희망자에 대하여 다음 각 호의 사유를 모두 충족할 경우 원칙적으로 접수 순으로 기업회원 가입을 승낙합니다.<br>
1. 회사의 업무수행 및 서비스 제공을 위한 설비의 여유·기술상 지장이 없는 경우<br>
2. 본 약관에서 정하는 승낙 제한 또는 거절, 보류 사유에 해당하지 않는 경우<br>
② 다음 각 호 중 어느 사유에 해당하는 경우, 회사는 이용자의 기업회원 가입 신청을 승낙하지 아니합니다. 기업회원 가입 후 해당 사실이 밝혀진 경우, 회사는 해당 기업회원 ID를 삭제하거나 기업회원을 강제탈퇴 조치하며, 필요할 경우 관계법령에 따른 형사처벌이나 행정제재를 위한 법률절차를 진행할 수 있습니다.<br>
1. 기업회원 가입 신청 시에 타인의 명의를 도용한 경우<br>
2. 기업회원 가입 신청 시에 회사명 또는 사업자등록번호를 실제와 다르게 기재하거나 기타 기업회원 정보를 허위로 기재한 경우<br>
3. 회사가 증빙 자료 제출을 요구하였음에도 불구하고 합리적 이유 없이 이에 응하지 아니한 경우<br>
4. 증빙 자료를 제출함에 있어 허위의 내용이 기재된 자료, 위조 또는 변조된 자료를 제출한 경우<br>
5. 서비스 이용 정지 또는 강제 탈퇴 등의 제재조치가 있었던 경우<br>
6. 회사가 서비스 부정이용 행위에 대한 제재조치를 취하기 전 이를 회피하기 위해 탈퇴하였던 경우<br>
③ 다음 각 호 중 어느 사유에 해당하는 경우, 회사는 그 사유가 해소될 때까지 이용자의 기업회원 가입 신청에 대한 승낙을 유보할 수 있습니다. 다만, 회사는 기업회원 가입 신청을 한 이용자에 대해 그 사유를 통지할 수 있습니다.<br>
1. 업무수행 및 서비스 제공을 위한 설비의 여유가 없는 경우<br>
2. 업무수행 및 서비스 제공을 위한 기술상 지장이 있는 경우<br>
3. 기타 회사의 사정(회사의 귀책사유 있는 경우를 포함)으로 이용 승낙이 곤란한 경우<br>
<br><br>
제8조 (서비스의 내용)<br>
① 회사가 제공하는 서비스의 내용은 다음 각 호와 같습니다.<br>
1. 인재열람 서비스<br>
2. 채용광고 서비스<br>
3. 배너광고 서비스<br>
<br><br>
제9조 (기업정보 및 채용공고의 등록, 이력서 검색)<br>
① 기업회원은 구직을 원하는 이용자가 채용공고를 열람한 후 해당 기업에 대하여 정확히 판단할 수 있도록 기업정보를 사실과 다름없이 정확하게 기재하여야 합니다. 기업정보를 사실과 다르게 기재한 기업회원은 이로 인하여 발생한 모든 문제에 대하여 전적인 책임을 부담하여야 합니다.<br>
② 기업회원은 채용절차 공정화에 관한 법률 제4조, 직업안정법 제34조 및 동법 시행령 제34조에 의하여 금지되는 거짓 구인광고 또는 거짓 채용공고(이하 “거짓 채용공고”라 합니다)를 등록하여서는 아니됩니다. 거짓 채용공고를 등록하거나 거짓 구인 조건을 제시한 기업회원은 채용절차의 공정화에 관한 법률 제16조에 의해 5년 이하의 징역 또는 2천만원 이하의 벌금형을 받거나 직업안정법 제47조에 의해 5년 이하의 징역 또는 5천만 원 이하의 벌금형을 받을 수 있습니다. 회사는 거짓 채용공고를 등록한 기업회원을 직권으로 고발할 수 있습니다.<br>
③ 제2항에 따라 금지되는 거짓 채용공고는 다음 각 호와 같습니다.<br>
1. 구인 또는 채용을 가장하여 아이디어 수집, 사업장 홍보, 물품 판매, 유학 알선, 수강생 모집, 직업소개, 부업알선, 자금 모집 등을 행하는 공고<br>
2. 작성자가 제시한 직종, 업무내용, 근로조건 등이 실제와 현저히 다른 공고<br>
3. 회사명, 담당자 성명, 사무실 연락처 등 구인업체의 중요 정보를 정확하게 기입하지 않았을 경우<br>
4. 기타 채용공고의 중요 내용이 사실과 다른 공고<br>
5. 제1호 내지 제4호의 거짓 채용공고를 목적으로 구인자의 신원(업체명 또는 성명)을 표시하지 아니하는 공고<br>
④ 기업회원은 남녀 고용 평등과 일, 가정 양립 지원에 관한 법률 제7조에 의거하여 근로자의 모집 및 채용에 있어서 남녀를 차별하여서는 아니됩니다.<br>
⑤ 기업회원은 채용공고 등록시 서비스 부정이용행위를 하여서는 아니됩니다.<br>
⑥ 기업회원이 등록한 기업정보 및 채용공고가 위반한 것으로 인정될 경우, 회사는 해당 공고의 게시를 중단하거나 삭제하는 등의 조치를 취할 수 있습니다. 또한, 기업회원은 회사 및 이용자에 대한 관계에서 발생할 수 있는 민·형사상 책임을 전적으로 부담합니다.<br>
⑦ 회사는 기업회원이 등록한 기업정보 및 채용공고를 회사가 정한 방법에 따라 노출시킬 수 있으며, 지정된 마감기간이 지난 채용공고를 임의로 마감시킬 수 있습니다.<br>
⑧ 기업회원은 유·무료로 사람인 개인회원이 등록한 이력서를 검색할 수 있으며, 회사가 제시하는 별도의 커뮤니케이션 수단을 통하여 개인회원에게 연락을 취할 수 있습니다. 이 때, 이력서상 기재된 개인회원의 연락처의 열람 및 연락의 목적은 채용활동에 국한되어야 하며, 기업의 영업·마케팅을 위하여 활용하거나 제3자에 대한 개인정보 제공 시에는 정보통신망 이용 촉진 및 정보 보호 등 관련 법률, 개인정보 보호법, 직업안정법 위반에 따른 법적 책임을 전적으로 부담합니다.<br>
⑨ 회사는 기업회원이 등록된 개인회원의 이력서를 검색하고, 개인회원에게 연락할 수 있는 권한에 적정한 제한을 둘 수 있으며, 기업회원이 적절하지 않은 방법이나 채용활동 이외의 목적으로 개인회원에게 연락하거나 서비스 이용에 불편을 끼친 경우, 기업회원의 채용공고 및 기업정보를 임의로 수정/삭제 등의 조치를 취할 수 있습니다.<br>
<br><br>
제10조 (이용요금의 환불)<br>
① 회사는 다음 각 호에 경우에는 이용요금을 환불합니다. <br>
1. 채용광고 서비스<br>
광고중인 건은 환불 불가능하나 대기중인 것은 환불 가능합니다.
일주일 전까지 전액환불하고 그 이후에는 불가능합니다.<br>
2. 인재열람 서비스<br>
1포인트당 금액 동일하게 환불이 가능합니다.
구매 후 30일이 지났거나 구매 후 30%이상 사용했거나 하면 환불 불가합니다.<br>
3. 배너광고 서비스<br>
결제하고 승인 거절되면 전액 환불가능합니다.<br>

<br><br>
제11조 (꽥포인트의 적립 및 이용 등)<br>
① 꽥포인트는 유료 광고·공고상품 또는 유료 서비스의 구매 사유로 적립됩니다. <br>
② 기업고객이 적립한 꽥포인트는 사이트 등에서 유료 광고·공고상품이나 유료 서비스 결제시 1포인트 당 1원의 가치로 현금처럼 사용 가능하며, 향후 회사 정책에 따라 그 사용처가 확장될 수 있습니다. 다만, 기업회원이 적립한 꽥포인트는 현금 기타 현금성 지급수단으로 전환되거나 출금될 수 없습니다.<br>
<br><br>
제12조 (서비스 이용시간 및 제공 중지)<br>
① 회사는 특별한 사유가 없는 한 연중무휴, 1일 24시간 서비스를 제공합니다. 다만, 일부 서비스의 경우 그 종류나 성질을 고려하여 별도로 이용시간을 정할 수 있으며, 회사는 그 이용 시간을 기업회원에게 공지합니다.<br>
② 회사는 다음 각 호에 해당하는 경우 서비스의 제공을 중지할 수 있습니다.<br>
1. 설비의 보수 등 회사의 필요에 의해 사전에 기업회원들에게 공지한 경우<br>
2. 기간통신사업자가 전기통신서비스 제공을 중지하는 경우<br>
3. 기타 불가항력적인 사유에 의해 서비스 제공이 객관적으로 불가능한 경우<br>
<br><br>
제13조 (게시물 작성과 회사의 수정·삭제 권한)<br>
① 게시물이란 기업회원이 등록한 기업정보 및 채용공고와 기타 사이트 등에 게시한 일체의 게시물을 의미합니다.<br>
② 기업회원은 게시물 작성시 서비스 제공 목적에 부합하게 정확한 사실에 근거하여 성실하게 그 내용을 작성하여야 하며, 작성된 게시물의 내용이 사실이 아니거나 부정확한 경우 발생하는 모든 문제에 대하여 전적인 책임을 부담합니다.<br>
③ 모든 게시물의 작성 및 관리는 기업회원 본인이 하는 것이 원칙이며, 제3자를 통해 위탁 또는 대행 관리를 하더라도 게시물 작성 및 관리에 관련된 일체의 책임은 기업회원에게 귀속됩니다. 기업회원은 주기적으로 작성된 게시물을 확인하여 정확한 내용이 포함될 수 있도록 수정·변경하는 등 일체의 노력을 다하여야 합니다.<br>
④ 기업회원이 등록한 게시물 내용에 오·탈자가 있을 경우, 회사는 사전 고지 없이 이를 수정할 수 있습니다.<br>
⑤ 기업회원이 등록한 게시물에 대하여 제3자로부터 허위사실 및 명예훼손 등으로 삭제요청이 접수된 경우, 회사는 해당 게시물을 직권으로 삭제할 수 있으며, 기업회원에게 해당 게시물의 삭제 사실 및 사유를 사후 통지합니다.<br>
⑥ 사이트 등에 채용공고를 등록한 기업회원에 대하여 고용노동부 및 유관기관, 법원 등 게시 중단을 요청할 수 있는 정당한 권한을 가진 기관으로부터 게시 중단 공문이나 명령 등이 접수된 경우, 회사는 사전 고지 없이 해당 기업회원의 채용공고 게시를 중단할 수 있으며, 채용공고 게시 중단으로 인하여 기업회원에게 발생한 손해를 배상하지 않습니다.<br>

<br><br>
제14조 (기업회원의 의무)<br>
① 기업회원은 관계법령과 본 약관의 규정, 회사의 서비스 운영정책 기타 고지된 서비스 이용상의 유의 사항을 준수하여야 하며, 기타 회사의 업무에 지장을 초래하는 행위를 하여서는 아니됩니다.<br>
② 기업회원이 신청한 유료 광고·공고 또는 유료 서비스는 등록 또는 신청과 동시에 회사와 채권, 채무 관계가 발생합니다.<br>
③ 기업회원이 결제수단으로 신용카드를 사용할 경우 비밀번호 등 결제정보 유실 방지는 기업회원 스스로 관리해야 합니다. 다만, 사이트의 결함에 따른 결제정보 유실의 발생에 대한 책임은 기업회원이 부담하지 않습니다.<br>
④ 기업회원은 서비스를 이용하여 얻은 정보를 회사의 사전 동의 없이 복사, 복제, 번역, 출판, 방송 기타의 방법으로 사용하거나 이를 타인에게 제공할 수 없습니다.<br>
⑤ 기업회원은 본 약관에서 정한 바와 다른 목적으로 사용할 수 없습니다.<br>
⑥ 기업회원은 사이트 등을 통해 열람한 이력서 정보를 회사 및 당사자의 허락 없이 재배포할 수 없으며, 본 정보에 대한 출력 및 복사 등의 관리 책임은 전적으로 기업회원에게 있습니다.<br>
<br><br>
제15조 (서비스의 부정이용에 대한 제재조치 등)<br>
① 기업회원이 서비스 부정이용행위를 하였을 경우, 회사는 해당 기업회원에게 사전 통지한 후 채용공고의 게시 중단 또는 삭제 조치, 기업회원 서비스 이용 중지, ID 삭제조치, 기업회원 강제 탈퇴 및 재가입 제한 조치(이하 “제재조치”라 합니다)를 취할 수 있습니다. 다만, 서비스 정상화 또는 민원처리 등 긴급한 사정이 있을 경우, 회사는 제재조치를 취한 후 사후적으로 해당 기업회원에게 통지할 수 있습니다.<br>
<br><br>
제16조 (서비스 이용계약 해지 및 자동탈퇴)<br>
① 기업회원이 서비스 이용계약을 해지하고자 할 경우, 고객센터 또는 "기업회원 탈퇴” 메뉴를 이용해 회사에 대한 해지 신청을 합니다. 기업회원은 서비스의 개별 또는 일괄 탈퇴 여부를 선택할 수 있습니다.<br>

				</div>

				<div class="ui checkbox" style="margin-top: 50px;">
					<input type="checkbox" id="agree" name="agreeCheck" value="동의">
					<label for="agree">약관내용에 모두 동의합니다</label>
				</div>
				
				




			</div>

			<input type="button" style="margin-top: 20px;"
				class="ui secondary button" id="insertMember" value="회원가입">

		</form>

		<%!public int getRandom() {
		int random = 0;
		random = (int) Math.floor((Math.random() * (99999 - 10000 + 1))) + 10000;
		return random;

	}%>

	</div>


	<script>
	
	function loadImg(value) {
		
		if(value.files && value.files[0]) {
			
			var reader = new FileReader();
			
			reader.onload = function(e) {
				
				$("#profileImg").attr("src",e.target.result);
				
				
				
				
			}
			reader.readAsDataURL(value.files[0]);
			
			
		}
		
		
		
		
		
	}
	
	$("#emailSend").click(function(){
		
		var code = $("#code_check").val();
		
		var emailCheck = $("#emailCheck").val();
		
		if(code == emailCheck) {
			
			
			alert("이메일 인증이 완료되었습니다.");
			 $("#emailCheck").attr("disabled",true);
			 $('.countdown').remove();
			
			 
		}else {
			
			alert("인증번호가 틀렸습니다.");
			
			
			
		}
	
		
		
		
		
	})
	
	
	

		$(function() {
			
			//이메일 검사(adfg@dfds.dsf)
			//4글자 이상이 나오고 
	  		//@가 나오고 1글자 이상 주소 . 글자 1~3
			 var emailRegExp = /^\w{4,}@\w{1,}\.\w{3}$/;
			 
			//아이디 검사
		 	//첫글자는 반드시 영문 소문자, 총 4~12자로 이루어지고
		 	//숫자가 반드시 하나 이상 포함되어야 함
		 	//영문 소문자와 숫자로 이루어져야 한다.
		    var idRegExp = /[a-z]([a-z]|[0-9]{1,}){3,11}/;
		    
		    var userId = document.getElementById('userId').value;
			 var pass = document.getElementById('userPwd').value;
			 var pass1 = document.getElementById('userPwd2').value;
			
	         var email = document.getElementById('email').value;
			
		    
			$("#ceoName").attr("disabled",false);
			 $("#companyName").attr("disabled",false);
		 $("#companyNumber").attr("disabled",false);
			
		 
		 $("#fileArea").hide();
			$("#idNum").hide();
			
			$("#passNum").hide();
			
			$("#profileImgArea").click(function(){
				
				
				$("#file").click();
				
				
				
				
				
			})
			$("#emailCheck").attr("disabled", true);
			$("#emailBtn").attr("disabled", true);
			
			$("#agree").attr("disabled", true);
			
			
			$("#id-success").hide();
			$("#id-danger").hide();
			$("#alert-success").hide();
			$("#alert-danger").hide();
			$("#email-success").hide();
			$("#email-danger").hide();
			
			$("#email").focusout(function() {

				var email = $("#email").val();

				$.ajax({

					url : "/info/emailCheck.me",
					type : "post",
					data : {
						email : email
					},
					success : function(data) {

						if (data === "fail") {

							$("#email-success").hide();
							$("#email-danger").show();
							$("#emailCheck").attr("disabled", true);
							$("#emailBtn").attr("disabled", true);
							 document.getElementById('email').select();
							

						} else {

							if (email == "") {
								$("#email-success").hide();
								$("#email-danger").hide();

							} else {
								
								//중복되진않지만 유효성검사에 걸리는 이메일
								if(!emailRegExp.test(email)){
									 $("#email-success").hide();
										$("#email-danger").show();
						             document.getElementById('email').select();
						            
						          }else {
						        	  $("#email-success").show();
										$("#email-danger").hide();
										 $("#emailCheck").attr("disabled",false);
										 $("#emailBtn").attr("disabled",false);	  
						          }

							}

						}

					},
					error : function() {

						console.log("실패!");

					}

				});

			});
			
			$("#userId").keyup(function(){
				
				$("#idNum").show();
	
				
			})
			
			
			$("#userId").focusout(function() {

				var userId = $("#userId").val();

				$.ajax({

					url : "/info/idCheck.me",
					type : "post",
					data : {
						userId : userId
					},
					success : function(data) {

						if (data === "fail") {
							$("#id-success").hide();
							$("#id-danger").show();
							document.getElementById('userId').select();
						} else {

							if (userId == "") {
								$("#id-success").hide();
								$("#id-danger").hide();

							} else {
								//중복되진않는데 유효성검사 걸리면!
								if(!idRegExp.test(userId)){
									$("#id-success").hide();
									$("#id-danger").show();
						             document.getElementById('userId').select();
						            //중복되지도않고 유효성검사도 안걸리고! 
						          }else {
						        	  $("#id-success").show();
										$("#id-danger").hide();	  
						          }
							}

						}

					},
					error : function() {

						console.log("실패!");

					}

				});

			});
			
			
			$("#emailBtn").click(function() {
				 var code = $("#code_check").val();
				 var email = $("#email").val();
				 
				 
				$("#emailCheck").attr("disabled",false);
				
				$("#emailBtn").css("display","none");
				$("#emailSend").css("display","block");
				$.ajax({
				
					
					 url : "/info/pJoinEmailSend",
					 type : "post",
					 data : {
						  code : code,
						  email : email
						},
						success : function(data) {
							
							
							
							
							
						},
						error : function() {
							console.log("실패!");
						}
					
				});
				
				
				
				

				var timer2 = "3:00";
				var interval = setInterval(function() {


				  var timer = timer2.split(':');
				  //by parsing integer, I avoid all extra string processing
				  var minutes = parseInt(timer[0], 10);
				  var seconds = parseInt(timer[1], 10);
				  --seconds;
				  minutes = (seconds < 0) ? --minutes : minutes;
				  if (minutes < 0) clearInterval(interval);
				  seconds = (seconds < 0) ? 59 : seconds;
				  seconds = (seconds < 10) ? '0' + seconds : seconds;
				 
				  
				  if(minutes==0 && seconds==00) {
					  
					  $("#emailCheck").val("");
					 $("#emailCheck").attr("disabled",true);
					 $("#emailSend").css("display","none");
					 $("#emailBtn").css("display","block");
					 $("#emailBtn").attr("value","재발송");
					
					 
					 
					 
					 clearInterval(interval);
					 
				  }
				  
			
				  $('.countdown').html(minutes + ':' + seconds);
				  timer2 = minutes + ':' + seconds;
				  
				 
				  
				  
				}, 1000);
				 

				

			});

			$("#insertMember").click(function() {
				//이름 검사
		  		//2글자 이상, 한글만
				var ceoNameRegExp = /^[가-힣]{2,}$/g;
				var managerNameRegExp = /^[가-힣]{2,}$/g;
				
				
				//전화번호 검사
		  		//전화번호 앞자리는 2~3자리 숫자
		  		//두번째자리는 3~4자리 숫자
		  		//세번째 자리는 4자리 숫자
		         var tel1RegExp = /[0-9]{2,3}/;
		         var tel2RegExp = /[0-9]{3,4}/;
		         var tel3RegExp = /[0-9]{4}/;
		         
		         var tel1 = document.getElementById('tel1').value;
		         var tel2 = document.getElementById('tel2').value;
		         var tel3 = document.getElementById('tel3').value;
		         
		      
				
						var userId = $("#userId").val();
						var userPwd = $("#userPwd").val();
						var userPwd2 = $("#userPwd2").val();
						var email = $("#email").val();
						var emailCheck = $("#emailCheck").val();
						var companyNumber = $("#companyNumber").val();
						var companyName = $("#companyName").val();
						var ceoName = $("#ceoName").val();
						var managerName = $("#managerName").val();

						var chk1 = $("#agree").is(":checked");
						
						
						
					

						if (userId == "" || userPwd == "" || userPwd2 == ""
								|| email == "" || emailCheck == ""
								|| companyNumber == "" || chk1 == false
								|| companyName == "" || ceoName == ""
								|| managerName == "") {

							alert("필수정보를 모두 입력해주세요.");

						} else {
							
							if(!ceoNameRegExp.test(ceoName)){
					             alert("올바르지 않은 대표자명입니다.");
					             document.getElementById('ceoName').select();
					            
					          }else if(!managerNameRegExp.test(managerName)) {
					        	  alert("올바르지 않은 인사담당자명입니다.");
						             document.getElementById('managerName').select();
					        	 
					          } else if(!tel1RegExp.test(tel1)) {
					        	  alert("올바르지 않은 연락처입니다.");
					              document.getElementById('tel1').select();
					          }else if(!tel2RegExp.test(tel2)) {
					        	  alert("올바르지 않은 연락처입니다.")
					              document.getElementById('tel2').select();
					          }else if(!tel3RegExp.test(tel3)) {
					        	  alert("올바르지 않은 연락처입니다.")
					              document.getElementById('tel3').select();
					          }else {

									console.log('제출');
									$("#joinForm").submit();  
					          }
						}

					});

			

			$("input[type=password]").keyup(function() {
				
				  var passRegExp =  /^(?=.*[a-zA-Z])((?=.*\d)|(?=.*\W)).{6,20}$/;

				  $("#passNum").show();
			       
				var pwd1 = $("#userPwd").val();
				var pwd2 = $("#userPwd2").val();
				
				

				if (pwd1 != "" || pwd2 != "") {

					if (pwd1 == pwd2) {
						
						if(!passRegExp.test(pwd1)) {
							$("#alert-success").hide();
							$("#alert-danger").show();
							
						}else {
							$("#alert-success").show();
							$("#alert-danger").hide();	
						}
						

					} else {

						$("#alert-success").hide();
						$("#alert-danger").show();
						

					}

				}

			})
			
			
			
		$("#companyNumberCheck").click(function(){
			

			var id = $("#companyNumber").val();
			
			
			   $.ajax({
				   
				   type : 'get',
			        url : 'https://business.api.friday24.com/closedown/' +id,
			        dataType : 'html', 
			        beforeSend : function(xhr){
			            xhr.setRequestHeader("Authorization", "Bearer mC4tfLjaV6tnucCHGfDa");
			           
			        },
			        error: function(xhr, status, error){
			        	
			        	
			        	
			            alert("사업자등록번호를 확인해주세요.");
			            document.getElementById('companyNumber').select();
			          
			            
			            
			        },
			        success : function(xml){
			           /*  alert(xml); */
			           
			           
			           if(xml.substring(95,101) == "normal") {
			        	   
			        	   alert("사업자등록번호 인증 완료");
			        	   
			        	   $("#agree").attr("disabled", false);
			        	   
			        	   
			           }else if(xml.substring(95,101) == "down") {
			        	   
			        	   alert("휴업");
			           }else if(xml.substring(95,101) == "close") {
			        	   
			        	   alert("폐업");
			           }else if(xml.substring(95,101) == "unregistered") {
			        	   
			        	   alert("미등록");
			           }
			        	  
			           
			            
			           
			           
			           
			           
			        }
				   
				   
				   
				   
				   
			   });
			
			
			
			
		})
			
			
			
			
			
			
			

		})
		
		
		  $("#uploadbutton").click(function(){
			  
			
	       
	         var formData = new FormData();
	         formData.append('file',$("#file")[0].files[0]);
	      
	              $.ajax({
	                url: 'https://ocr.api.friday24.com/business-license',
	                enctype: 'multipart/form-data',
	                processData: false,
	                    contentType: false,
	                data: formData,
	                type: 'post',
	                
	                beforeSend : function(xhr){
	                    xhr.setRequestHeader("Authorization", "Bearer MzzUPEqNIpdzorFZTZgx");
	                  /*   xhr.setRequestHeader("Content-Type", "multipart/form-data"); */
	                },
	                success: function(xml){
	                
	                	  var state =  xml;
	                	
	                	  //사진에서 잘라온 사업자등록번호 10자리가 모두 숫자이면(isNaN의 부정)
	               if(  !isNaN(JSON.stringify(state[Object.keys(state)[0]]).substring(11,21))) {
	            	   
	            	   var companyNum = JSON.stringify(state[Object.keys(state)[0]]).substring(11,21);
	            	   
	            	   $.ajax({
	            		   
	            		   url: "/info/companyNumCheck.me",
	            		   type: "post",
	            		   data : {
	            			   companyNum : companyNum
	            			   
	            		   },
	            		   success : function(data) {
	            			   //사업자 사진 올렸는데 번호 중복됬을때
	            			   if(data === "fail") {
	            				  
	            				   alert("중복된 사업자등록증 사진입니다.");
	            				   $("#profileImg").attr("src","");
	        	            	   $("#ceoName").val("");
	        	                	$("#companyName").val("");
	        	                	$("#companyNumber").val("");
	        	                	 $("#agree").attr("disabled", true);
	            				   
	            			   }else {
	            				   
	        	            	   alert("사업자등록증 사진 인증 완료");
	        	            	 
	        	            	   $("#ceoName").val(JSON.stringify(state[Object.keys(state)[0]]).substring(78,81));
	        	                	 $("#ceoName").attr("readonly",true);
	        	                	$("#companyName").val(JSON.stringify(state[Object.keys(state)[0]]).substring(61,65));
	        	                	 $("#companyName").attr("readonly",true);
	        	                	$("#companyNumber").val(JSON.stringify(state[Object.keys(state)[0]]).substring(11,21));
	        	                	 $("#companyNumber").attr("readonly",true);
	        	                	 $("#agree").attr("disabled", false);
	        	                	
	        	                	
	        	                    console.log(JSON.stringify(state[Object.keys(state)[0]]));
	        	                    console.log(JSON.stringify(state[Object.keys(state)[1]]));
	        	                  
	        	                    
	        	                    
	        	                    //사업자등록번호 가져옴
	        	                    console.log( JSON.stringify(state[Object.keys(state)[0]]).substring(11,21));
	        	                    
	        	                    //회사이름 가져옴
	        	                     console.log( JSON.stringify(state[Object.keys(state)[0]]).substring(61,65));
	        	                    
	        	                    //대표자명 가져옴
	        	                    console.log( JSON.stringify(state[Object.keys(state)[0]]).substring(78,81));
	        	                    
	        	                    //휴폐업상태 가져옴
	        	                    console.log( JSON.stringify(state[Object.keys(state)[1]]).substring(95,101));
	            				   
	            			   }
	            			   
	            			   
	            		   },
	            		   error : function () {
	            				console.log("실패!");
	            			   
	            		   }
	            		   
	            		   
	            		   
	            	   })
	            	   
	            	   
	            	   
	            	   
	            	 
	            	   
	               }else {
	            	   
	            	   alert("사업자등록증 사진을 확인해주세요.");
	            	   $("#profileImg").attr("src","");
	            	   $("#ceoName").val("");
	                	$("#companyName").val("");
	                	$("#companyNumber").val("");
	                	 $("#agree").attr("disabled", true);
	            	   
	               }                		
	             
	                },
	                error:function(request,status,error){
				        alert("사업자등록증 사진을 확인해주세요."); 
				        
				        $("#ceoName").val("");
	                	$("#companyName").val("");
	                	$("#companyNumber").val("");
	                	 $("#agree").attr("disabled", true);
				        
				        
				        
				        
				        
				        
				        
				        
				        
				    }


	         }); 
	})
		
		
		
		
		
	</script>









</body>
</html>