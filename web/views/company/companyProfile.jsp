<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>기업 개인정보</title>
<style>
#mainDiv {
	margin-left:auto;
	margin-right:auto;
	margin-top:70px;
	width:1100px;
}

#userCheckDiv {
		text-align:center;
		padding-top:5%;
		margin:auto;
		height:400px;
		color:gray;
}

td {
	vertical-align: top;
	padding-top: 10px;
}
</style>
<%@ include file="/views/common/import.html" %>
</head>
<body>
	<%@ include file="../common/companyMenu.jsp" %>  <!-- 메뉴바 -->
	<% if(loginUser != null){ %>
	<div id="mainDiv">
		<div id="userCheckDiv">
			<h3>기본정보 열람 및 수정하시려면 비밀번호를 입력해주세요.</h3>
		<form id="companyInfoForm" action="<%=request.getContextPath()%>/CompanyInfo.me" method="post">
			<table align="center" style="text-align:center;">
				<tr>
					<td>아이디</td>
					<td><div class="ui input"><input type="text" value="<%=loginUser.getUserId() %>" readonly disabled></div></td>
				</tr>
				<tr>
					<td>비밀번호</td>
					<td><div class="ui input"><input type="password" name="userPwd" id="userPwd"></div></td>
				</tr>
			</table>
			</form>
			<br>
			<button class="ui secondary button"  id="companyInfoCheck">확인</button>
			<a href="<%= request.getContextPath() %>/views/company/companyPage.jsp"><button class="ui button">취소</button></a>
		</div>
	</div>
	<%@ include file="../common/ourfooter.jsp" %>		<!-- 하단 -->
	<script>
	$("#companyInfoCheck").click(function(){

		var userId = "<%=loginUser.getUserId() %>";
		var userPwd = $("#userPwd").val();
	
		$.ajax({
			url : "/info/passwordCheck.me",
			type : "post",
			data : {
				userId : userId,
				userPwd : userPwd
			}, success : function(data) {
				if(data === "fail") {
					$("#companyInfoForm").submit();
				} else {					
					alert("비밀번호를 확인해주세요");	
				}
			}, error : function() {}
		});		
	});
	</script>
	<% } else { %>
	<script>
	swal ({
		title : "로그인이 필요한 서비스입니다!",
		icon : "error"
	}).then((value) => {
			document.location.href="<%=request.getContextPath()%>/loginBannerImg.me?type=2"; 
	});
	</script>
	<% } %>
</body>
</html>