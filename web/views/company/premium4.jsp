<%@page import="company.model.vo.PaymentList"%>
<%@page import="java.util.ArrayList, keyword.model.vo.PageInfo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%	
	ArrayList<PaymentList> list = (ArrayList<PaymentList>) request.getAttribute("list");
	System.out.println(list);
	
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="/views/common/import.html"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
.maintitle {
	margin-right: auto;
	margin-left: auto;
	width: 70%;
	height: 60px;
	text-align: center;
	font-size: 2.5em;
	font-weight: bold;
	color: black;
	padding-top: 20px;
}
.first {
	margin-right: auto;
	margin-left: auto;
	width: 70%;
	margin-top: 25px;
}
table td {
	height: 40px;
}

table tr {
	text-align: center;
}

	.pagingArea{
		text-align:center;
		padding:20px;
		margin-bottom:50px;
		margin-top:30px;
	}
	.pagingbtn{
		height:30px;
		width:30px;
		display:inline-block;
		vertical-align:top;
		overflow:hidden;
		background:white;
		border:1px solid lightgray;
		border-radius:5px;
	}
	.pagingbtn:hover{
		cursor:pointer;
	}
	.pagingbtn:disabled{
		color:gray;
	}
</style>
</head>
<body>
	<%@ include file="../common/companyMenu.jsp"%>
	<div style="height: 60px;"></div>
	<div class="maintitle">결제 내역</div>
	<br>
	<div class="first">
		<h1>프리미엄 상품 결제내역</h1>
		<br>
		<hr>
		<br>
		<table class="ui sortable celled table" id="list">
			<thead>
				<tr align="center">
					<th>번호</th>
					<th>결제날짜</th>
					<th>상품명</th>
					<th>결제 금액</th>
					<th>구분</th>
					<th>조회</th>
				</tr>
			</thead>
			<tbody>
				<%for (int i=0; i<list.size(); i++){ %>
				<tr>
					<td><%=i+1%></td>
					<td><%=list.get(i).getPaymentDate()%></td>
					<td><%=list.get(i).getProductName()%></td>
					<td><%=list.get(i).getPaymentPrice()%>원</td>
					<td><%  
						String category = "";
						if(Integer.parseInt(list.get(i).getPayhistroyCategory()) == 1){
							category = "요청"	;							
						}else if (Integer.parseInt(list.get(i).getPayhistroyCategory()) == 2){
							category = "승인";
						}else if (Integer.parseInt(list.get(i).getPayhistroyCategory()) == 3){
							category ="환불";
						}else if(Integer.parseInt(list.get(i).getPayhistroyCategory()) == 4){
							category ="환불요청";
						}else{
							category ="환불완료";
						}
						%>
						<%=category %>
					</td>
					<td><button class="ui primary button" onclick="inquiry(this);">조회</button>
						<input type="hidden" value="<%=list.get(i).getPayid()%>">
					</td>
				</tr>
				<%} %>
		</table>
		
		<div class="pagingArea">
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/allPaymentList?currentPage=1'"><<</button>
			<%if(currentPage <=1) {%>
				<button class="pagingbtn" disabled></button>
			<%} else{ %>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/allPaymentList?currentPage=<%=currentPage-1%>'"><</button>
			<%} %>
			
			<%for(int p=startPage; p<=endPage; p++){
				if(p == currentPage){ %>
					<button class="pagingbtn" disabled><%=p %></button>
				<%} else{ %>
					<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/allPaymentList?currentPage=<%=p%>'"><%=p %></button>
				<%}
			  } %>
			
			<%if(currentPage >= maxPage){ %>
				<button class="pagingbtn" disabled></button>
			<%} else {%>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/allPaymentList?currentPage=<%=currentPage+1%>'">></button>
			<%} %>
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/allPaymentList?currentPage=<%=maxPage%>'">>></button>
		</div>
		<hr style="widows: 90%;">
			<div class="explain" style="font-size:small;">
			<div style="height: 20px;"></div>
			<strong>[프리미엄 상품 환불정책 안내]</strong>
			<br>
			<br>
			<strong>[적극채용공고]</strong>
			<ul>
				<li>적극채용공고로 등록된 공고가 부적절하다고 판단되면 관리자에 의해 해당 공고가 삭제될 수 있습니다. (환불 불가)</li>
				<li>결제는 일주일단위로만 결제가 가능하며 일단위로는 결제가 불가능합니다.</li>
				<li>결제 후 공고 시작일로부터 일주일 전까지만 환불이 가능합니다. 공고 시작 일주일 전에는 환불이 불가능합니다.</li>
				<li>같은 회원이 여러개의 공고를 등록할 수 있습니다.</li>
			</ul>
			<br><br>
			<strong>[인재열람]</strong>
			<ul>
				<li>비공개된 구직자의 이력서는 확인 할 수 없습니다.</li>
				<li>인재열람 상품의 기간 제한은 없으나 관리자에 의해 탈퇴조치 시 모든 상품은 소멸됩니다.</li>
				<li>열람한 인재가 비공개전환 또는 탈퇴 시 확인할 수 없습니다.</li>
				<li>현재 결제 상품이 남아있는 경우 모든 열람권을 사용하기 전까지 추가 결제가 불가능합니다.</li>
				<li>구매한 갯수의 50%이상 남아있는 경우 환불이 가능하며 환불 금액은 개당 390원으로 환불이 이루어집니다.</li>
			</ul>
			<br><br>
			<strong>[광고배너]</strong>
			<ul>
				<li>첨부된 배너가 부적절한 배너일 경우 광고 심의에 의해 거절 조치되고 결제 금액은 환불됩니다.</li>
				<li>부적절한 광고배너를 지속적으로 신청할 경우 해당 회원에 대해서 불이익 조치가 진행됩니다.</li>
				<li>광고배너는 예약제로 기간연장은 불가능합니다.</li>
			</ul>
			</div>	
	</div>
	
	<div class="ui modal" id="detail">
		<i class="close icon"></i>
		<div class="header">결제 내역 상세</div>
		<div class="image content">
			<div class="description">
				<div class="ui header">상세 결제 내역</div>
				<hr>
				<table class="ui celled table" id="detailTable">
					<thead>
						<tr>
							<th>상품명</th>
							<th>결제날짜</th>
							<th>결제금액</th>
							<th>구분</th>
							<th>기타</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		<div class="actions">
			<div class="ui positive right labeled icon button">
				확인<i class="checkmark icon"></i>
			</div>
		</div>
		
	</div>
<%@ include file="../common/ourfooter.jsp"%>
	<script>
		var dohunlength = 0;
		$(function() {
			$("#list").tablesort();
		});
		function inquiry(par) {
			var payid = $(par).parent().find("input[type=hidden]").val();
			var uno = <%=loginUser.getUno()%>;
			var result  = {
					uno : <%=loginUser.getUno()%>, 
					payid : payid 
			}
			$.ajax({
				url : "paymentDetailList",
				data : result,
				type: "POST",
				dataType: "json",
				success:function(data){
					$tableBody = $("#detailTable tbody");
					$tableBody.html('');
					
					dohunlength = data.length;
					$.each(data , function(index , value){
						var payid = value.payid;
						var uno = value.uno;
						var pcode = value.pcode;
						
						//지금 현 공고가 환불 조건에 맞는지 아닌지 판단 ajax
						//1.적극채용공고
						//- 공고 시작일 일주일 전에는 환불 불가
						//2.인재열람
						//- 결제한 상품의 50%이상 사용시 환불 불가
						//- 50%이상 남아있어도 개당 390원으로 환불 가능
						//3. 광고배너
						//- 광고 시작일 일주일 전에는 환불 불가
						//payBack() 버튼을 눌렀을 때에는 환불로직이 돌아가며 , 
						//환불정책에 위배되면 버튼을 비활성화 시킨다. or alert창으로 결제가 불가능하게 만든다.
						
						//pcode가 1일때  (적극채용 공고일때) 환불 제한 logic
						if(pcode==1){
								var recresult ={
									payid:payid,
									pcode:pcode
								}
								//console.log("적극채용공고"+payid);
								$.ajax({
									url:"selectPoPDate",
									data : recresult,
									type:"POST",
									dataType: "json",
									success:function(data){
										//console.log(data);
										if(data === true && (decodeURIComponent(value.cat)==="승인" || decodeURIComponent(value.cat)==="요청")){
											//환불 가능
											var $tr = $("<tr>");
											var $name = $("<td>").text(decodeURIComponent(value.pName));
											var $date = $("<td>").text(value.payDate);
											var $money = $("<td>").text(value.pMoney);
											var $cat = $("<td>").text(decodeURIComponent(value.cat));
											var $non = $("<td>");
											//결과에 따라 환불 제한                    
											var $button = null;
											if(dohunlength == 1){
												$button = $("<td id='payid'>").append("<button class='ui red button payBack' id = 'back'>환불</button>");
											}else if(dohunlength ==2){
												console.log(decodeURIComponent(value.cat));
												if(decodeURIComponent(value.cat) == "승인" || decodeURIComponent(value.cat) == "요청"){
													$button = $("<td id='payid'>").append("<button class='ui red button payBack' id = 'back'>환불</button>");
												}else if(decodeURIComponent(value.cat) == "환불요청" || decodeURIComponent(value.cat) == "요청"){
													$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불요청</button>");
												}
											}else if(dohunlength == 3){
												if(decodeURIComponent(value.cat) == "요청"  || decodeURIComponent(value.cat) == "승인"  ||decodeURIComponent(value.cat) == "환불요청" ){
													$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불요청</button>");
												}else if(decodeURIComponent(value.cat) == "승인" ||decodeURIComponent(value.cat) == "환불요청" ||decodeURIComponent(value.cat) == "환불완료" ){
													$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불완료</button>");
												}
											}else if(dohunlength == 4){
												$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불완료</button>");
											}
											var $hidden = $("<input type='hidden'>").val(payid);
												$tr.append($name);
											$tr.append($date);
											$tr.append($money);
											$tr.append($cat);
											$button.append($hidden);
											//마지막 인덱스에 button 추가
											if(index == dohunlength-1){
												$tr.append($button);
												$button.append($hidden);
											}else{
												$tr.append($non);
											}
											//테이블에 tr 추가로 표 완성
											$tableBody.append($tr);
											
										}else{
											//환불 불가능
											var $tr = $("<tr>");
											var $name = $("<td>").text(decodeURIComponent(value.pName));
											var $date = $("<td>").text(value.payDate);
											var $money = $("<td>").text(value.pMoney);
											var $cat = $("<td>").text(decodeURIComponent(value.cat));
											var $non = $("<td>");
											//결과에 따라 환불 제한
											var $button = $("<td>").append("<button class='ui red button' disabled >환불</button>");
											$tr.append($name);
											$tr.append($date);
											$tr.append($money);
											$tr.append($cat);
											//마지막 인덱스에 button 추가
											if(index == 0){
												$tr.append($button);
											}else{
												$tr.append($non);
											}
											//테이블에 tr 추가로 표 완성
											$tableBody.append($tr);
										}
									}
								}) /* recajax end */
							
						//pcode가 2보다크고 6보다 작을때  (인재열람상품일때) 환불 제한 logic
						}else if(pcode>=2 && pcode <=6){
							var personresult ={
									payid:payid,
									pcode:pcode
							}
							//console.log("인재열람 : "+payid)
							$.ajax({
								url:"SelectCount",
								data : personresult,
								type:"POST",
								dataType: "json",
								success:function(data){
									
									if(data === true){
										//환불 가능
										//console.log("트루트루");
										//console.log(payid);
										var $tr = $("<tr>");
										
										var $name = $("<td>").text(decodeURIComponent(value.pName));
										var $date = $("<td>").text(value.payDate);
										var $money = $("<td>").text(value.pMoney);
										var $cat = $("<td>").text(decodeURIComponent(value.cat));
										var $non = $("<td>");
										var $button = null;
										var count = 0;
										console.log(index);
										//console.log(decodeURIComponent(value.cat))
										/* if(dohunlength ==1){
											if((decodeURIComponent(value.cat)==="승인" || decodeURIComponent(value.cat)==="요청")){
												$button = $("<td id='payid'>").append("<button class='ui red button payBack' id = 'back'>환불</button>");
											}else if(decodeURIComponent(value.cat)==="환불요청"){
												$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불요청</button>");
											}
										} */
										if(dohunlength == 1){
											$button = $("<td id='payid'>").append("<button class='ui red button payBack' id = 'back'>환불</button>");
										}else if(dohunlength ==2){
											console.log(decodeURIComponent(value.cat));
											if(decodeURIComponent(value.cat) == "승인" || decodeURIComponent(value.cat) == "요청"){
												$button = $("<td id='payid'>").append("<button class='ui red button payBack' id = 'back'>환불</button>");
											}else if(decodeURIComponent(value.cat) == "환불요청" || decodeURIComponent(value.cat) == "요청"){
												$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불요청</button>");
											}
										}else if(dohunlength == 3){
											if(decodeURIComponent(value.cat) == "요청"  || decodeURIComponent(value.cat) == "승인"  ||decodeURIComponent(value.cat) == "환불요청" ){
												$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불요청</button>");
											}else if(decodeURIComponent(value.cat) == "승인" ||decodeURIComponent(value.cat) == "환불요청" ||decodeURIComponent(value.cat) == "환불완료" ){
												$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불완료</button>");
											}
										}else if(dohunlength == 4){
											$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불완료</button>");
										}
										var $hidden = $("<input type='hidden'>").val(payid);
										$tr.append($name);
										$tr.append($date);
										$tr.append($money);
										$tr.append($cat);
										
										if(index == dohunlength-1){
											$tr.append($button);
											$button.append($hidden);
										}else{
											$tr.append($non);
										}
										//테이블에 tr 추가로 표 완성
										$tableBody.append($tr);
									}else if(data === true){
										
									}else{
										//환불 불가	
										//console.log("펄스펄스");
										var $tr = $("<tr>");
										var $name = $("<td>").text(decodeURIComponent(value.pName));
										var $date = $("<td>").text(value.payDate);
										var $money = $("<td>").text(value.pMoney);
										var $cat = $("<td>").text(decodeURIComponent(value.cat));
										var $non = $("<td>");
										var $button = $("<td>").append("<button class='ui red button' disabled >환불</button>");
										$tr.append($name);
										$tr.append($date);
										$tr.append($money);
										$tr.append($cat);
										//마지막 인덱스에 button 추가
										if(index == 0){
											$tr.append($button);
										}else{
											$tr.append($non);
										}
										//테이블에 tr 추가로 표 완성
										$tableBody.append($tr);
									}
								}
							})/* personajax end */
							
						}else if(pcode >=7 && pcode <= 9){
							var adverresult ={
									payid:payid,
									pcode:pcode
							}
							//console.log("payid : " + payid)
							$.ajax({
								url:"selectAdverDate",
								data : adverresult,
								type:"POST",
								dataType: "json",
								success:function(data){
									if(data === true){
										//console.log(payid);
										var $tr = $("<tr>");
										var $name = $("<td>").text(decodeURIComponent(value.pName));
										var $date = $("<td>").text(value.payDate);
										var $money = $("<td>").text(value.pMoney);
										var $cat = $("<td>").text(decodeURIComponent(value.cat));
										var $non = $("<td>");
										
										console.log(dohunlength);
										var $button = null;
										if(dohunlength == 1){
											$button = $("<td id='payid'>").append("<button class='ui red button payBack' id = 'back'>환불</button>");
										}else if(dohunlength ==2){
											console.log(decodeURIComponent(value.cat));
											if(decodeURIComponent(value.cat) == "승인" || decodeURIComponent(value.cat) == "요청"){
												$button = $("<td id='payid'>").append("<button class='ui red button payBack' id = 'back'>환불</button>");
											}else if(decodeURIComponent(value.cat) == "환불요청" || decodeURIComponent(value.cat) == "요청"){
												$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불요청</button>");
											}else if(decodeURIComponent(value.cat) == "환불완료" || decodeURIComponent(value.cat) == "요청"){
												$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불완료</button>");
											}else{
												$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불</button>");
											}
										}else if(dohunlength == 3){
											if(decodeURIComponent(value.cat) == "요청"  || decodeURIComponent(value.cat) == "승인"  ||decodeURIComponent(value.cat) == "환불요청" ){
												$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불요청</button>");
											}else if(decodeURIComponent(value.cat) == "승인" ||decodeURIComponent(value.cat) == "환불요청" ||decodeURIComponent(value.cat) == "환불완료" ){
												$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불완료</button>");
											}
										}else if(dohunlength == 4){
											$button = $("<td id='payid'>").append("<button class='ui red button' disabled >환불완료</button>");
										}
										var $hidden = $("<input type='hidden'>").val(payid);
										$tr.append($name);
										$tr.append($date);
										$tr.append($money);
										$tr.append($cat);
										$button.append($hidden);
										//마지막 인덱스에 button 추가
										if(index == dohunlength-1){
											$tr.append($button);
											$button.append($hidden);
										}else{
											$tr.append($non);
										}
										//테이블에 tr 추가로 표 완성
										$tableBody.append($tr);
									}else{
										var $tr = $("<tr>");
										var $name = $("<td>").text(decodeURIComponent(value.pName));
										var $date = $("<td>").text(value.payDate);
										var $money = $("<td>").text(value.pMoney);
										var $cat = $("<td>").text(decodeURIComponent(value.cat));
										var $non = $("<td>");
										var $button = $("<td>").append("<button class='ui red button payBack' disabled >환불</button>");
										$tr.append($name);
										$tr.append($date);
										$tr.append($money);
										$tr.append($cat);
										//마지막 인덱스에 button 추가
										if(index == 0){
											$tr.append($button);
										}else{
											$tr.append($non);
										}
										//테이블에 tr 추가로 표 완성
										$tableBody.append($tr);

									}
								}
							})/* adverajax end */
						}
					});
					
					$('#detail').modal('show');
				},
				error:function(request,status,error){
			        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			    }
			});/*paymentDetailList end */
		}
		$(document).on('click', '.payBack', function(){
			var payid = $(this).parent().find("input[type=hidden]").val();
			console.log("payid : " + payid);
			
			if(confirm("환불 요청을 하시겠습니까?")){
				
				//$(this).attr('disabled', true);
				
				location.href = "<%=request.getContextPath()%>/insertRefundrequest?payid="+payid;
			}
			
		})
		
	</script>
</body>
</html>