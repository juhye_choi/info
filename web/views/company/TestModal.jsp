<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="/views/common/import.html"%>
</head>
<style>
.file_input label {
    position:relative;
    cursor:pointer;
    display:inline-block;
    vertical-align:middle;
    overflow:hidden;
    width:100px;
    height:30px;
    background:#777;
    color:#fff;
    text-align:center;
    line-height:30px;
}
.file_input label input {
    position:absolute;
    width:0;
    height:0;
    overflow:hidden;
}
.file_input input[type=text] {
    vertical-align:middle;
    display:inline-block;
    width:400px;
    height:28px;
    line-height:28px;
    font-size:11px;
    padding:0;
    border:0;
    border:1px solid #777;
}


</style>
<body>

<div class="file_input">
    <label>
        File Attach
        <input type="file" onchange="javascript:document.getElementById('file_route').value=this.value">
    </label>
    <input type="text" readonly="readonly" title="File Route" id="file_route">
</div>




	<button onclick="on()">클릭</button>
	<div class="ui modal">
		<i class="close icon"></i>
		<div class="header">Profile Picture</div>
		<div class="image content">
			<div class="ui medium image">
				<div class="ui calendar" id="disable1">
					<div class="ui input left icon">
						<i class="calendar icon"></i> <input type="text" placeholder="Date" class="checkStartDate">
					</div>
				</div>
			</div>
			<div class="description">
				<div class="ui header">We've auto-chosen a profile image for
					you.</div>
				<p>
					We've grabbed the following image from the <a
						href="https://www.gravatar.com" target="_blank">gravatar</a> image
					associated with your registered e-mail address.
				</p>
				<p>Is it okay to use this photo?</p>
			</div>
		</div>
		<div class="actions">
			<div class="ui black deny button">Nope</div>
			<div class="ui positive right labeled icon button">
				Yep, that's me <i class="checkmark icon"></i>
			</div>
		</div>
	</div>
</body>
<script>
	

	
	
	function on() {
		$('.ui.modal').modal({
			onShow : function(value){
				 var listDate = [];
				 var today = new Date();
				$('#disable1').calendar({
					  type : 'date',
					  minDays : 7,
					  maxDays : 7,
					  text:{
						  days :['월','화','수','목','금','토','일'],
						  months:['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
					  },
					  minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
					
					  formatter : {
							date : function(date, settings) {
								if (!date)
									return '';
								var day = date.getDate();
								var month = date.getMonth() + 1;
								var year = date.getFullYear();
								return year + '-' + month + '-' + day;
							}
						},
				      initialDate: new Date(),
				      disabledDates: listDate  ,
					  endCalendar : $('#disable2')
				  });
				  
			}
		}).modal('show');

	}
	
	
	
	/* $("#jobModal").modal({
		onShow : function(value){
			$('#rangestart').calendar({
				type: 'date',
				formatter : {
					date: function(date, settings){
						console.log("hello!?");
						if(!date) return '';
						var day = date.getDate();
						var month = date.getMonth() +1;
						var year = date.getFullYear();
						return year + '-' + month + '-' + day;
					}
				},
				endCalendar: $('#rangeend')
			});
			$('#rangeend').calendar({
				type: 'date',
				formatter : {
					date: function(date, settings){
						if(!date) return '';
						var day = date.getDate();
						var month = date.getMonth() +1;
						var year = date.getFullYear();
						return year + '-' + month + '-' + day;
					}
				},
				startCalendar: $('#rangestart')
			});
		}
	}).modal('show'); */
</script>
</html>