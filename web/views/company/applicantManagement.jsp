<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, company.model.vo.*"%>
<%
	ArrayList<Recruit> reclist = (ArrayList<Recruit>) request.getAttribute("reclist");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>기업페이지</title>
<%@ include file="/views/common/import.html" %>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
.date {
	font-size:20px;
	margin:10px;
}
.number {
	text-align:center;
	font-size:50px;
	font-weight:900;
	height:60px;
}
th {
	width:16%;
}
#title {
	font-size:30px;
	font-weight:900;
	height:50px;
	margin-top:20px;
}

#allDiv{
	margin-bottom:250px;
}
.attach:hover{
	cursor:pointer;
	text-decoration:underline;
}

<%@ include file="/css/companyBodyStyle.css" %>
</style>
</head>
<body>
	<%@ include file="../common/companyMenu.jsp"%>
	<div id="allDiv">
		<select name="selectRegistration" id="selectRegistration"  style="width:200px;height:30px;">
			<option value="0">공고 선택</option>
			<%if(reclist != null){ 
				for(Recruit r : reclist){%>
				<option value="<%=r.getRecid()%>"><%= r.getRec_title()%></option>
			<%} } %>
		</select>
		<br>
		<div id="title"><p>공고를 선택해주세요.</p></div>
		<div class="date"><span id="recDate">0000.00.00 ~ 0000.00.00</span> <label style="color:gray">ㅣ</label><label style="color:blue" id="recStatus">채용상태</label></div>
		<hr>
		<table style="width:100%;">
		<tr>
			<th>전체 지원자</th>
			<th>미열람</th>
			<th>열람완료</th>
		</tr>
		<tr id="howmany">
			<td class="number">0</td>
			<td class="number">0</td>
			<td class="number">0</td>
		</tr>
		</table>
		<hr>
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead>
					<tr align="center">
						<th>이름</th>
						<th>생년월일</th>
						<th>지원일</th>
						<th>열람상태</th>
						<th>이력서조회</th>
					</tr>
				</thead>
				<tbody align="center">
					
				</tbody>
			</table>
		</div>
		<script>
		function replaceAll(sValue, param1, param2) { // replaceAll 이라는 변수 선언.
			   return sValue.split(param1).join(param2);
		}
		$(function(){
			$("#logTable").tablesort();
			
			
			$("#selectRegistration").change(function(){
				//console.log("눌렸음!");
				//console.log();
				
				/* <div class="ui active dimmer">
    			<div class="ui loader"></div>
  				</div> */
				
				$tableBody = $("#logTable tbody");
  				$tableBody.html("<div class='ui active inverted dimmer'><div class='ui loader'></div></div>");
				
				var recid = $("#selectRegistration option:selected").val()
				$.ajax({
					url : "selectList.capp",
					data : {
						recid : recid
					},
					type : "get",
					success:function(data){
						//console.log(data);
						var res = decodeURIComponent(data.recTitle);
						res = replaceAll(res, "+", " ");
						$("#title").children().text(res);
						$("#recDate").text(data.recStart + " ~ " + data.recFinish);
						
						var now = new Date();
						var recStart = new Date(data.recStart);
						var recFinish = new Date(data.recFinish);
						if(now >= recStart && now <= recFinish){
							//console.log("채용중!");
							$("#recStatus").text("채용중").css("color", "blue");
							
						} else if(now > recFinish){
							//console.log("아님!");
							$("#recStatus").text("채용종료").css("color", "black");
						} else{
							$("#recStatus").text("채용예정").css("color", "gray");
						}
						
						//$tableBody = $("#logTable tbody");
						
						$tableBody.html('');
						var cnt = 0;
						$.each(data.applyList, function(index, value){
							$tr = $("<tr align='center'>");
							$nameTd = $("<td>").text(decodeURIComponent(value.name));
							$birthTd = $("<td>").text(value.pbirth);
							$appDateTd = $("<td>").text(value.applyDate);
							
							var seeStatus = "";
							if(value.status == 'Y'){
								cnt++;
								seeStatus = "열람완료";
								$statusTd = $("<td>").text(seeStatus).css("color", "gray");
							} else{
								seeStatus = "미열람";
								$statusTd = $("<td>").text(seeStatus).css("color", "red");
							}
							
							
							$originFileTd = $("<td class='attach'>").text(decodeURIComponent(value.originName));
							$attachTd = $("<input type='hidden'>").val(value.atId);
							$appIdTd = $("<input type='text' style='display:none'>").val(value.applyId);
							$tr.append($nameTd);
							$tr.append($birthTd);
							$tr.append($appDateTd);
							$tr.append($statusTd);
							$originFileTd.append($attachTd);
							$originFileTd.append($appIdTd);
							$tr.append($originFileTd);
							
							$tableBody.append($tr);
							
						});
						
						$("#howmany").children().eq(0).text(data.recApplicant);
						$("#howmany").children().eq(1).text(data.recApplicant - cnt);
						$("#howmany").children().eq(2).text(cnt);
						
						$(".attach").click(function(){
							var atid = $(this).parent().find("input[type=hidden]").val();
							var appid = $(this).parent().find("input[type=text]").val();
							var thisComponent = $(this);
							
							location.href='<%=request.getContextPath()%>/download.at?num='+atid;
							
							
							$.ajax({
								url : "insertSeen.capp",
								data : {
									num : appid
								},
								type : "get",
								success:function(data){
									if(data>0){
										//console.log("성공!!!");
										thisComponent.parent().children().eq(3).text("열람완료").css("color","gray");
										
										$("#howmany").children().eq(1).text($("#howmany").children().eq(1).text()-1);
										$("#howmany").children().eq(2).text(Number($("#howmany").children().eq(2).text())+1);
									}
								},
								error:function(request,status,error){
							        alert("code = "+ request.status + " message = " + request.responseText + " error = " + error); // 실패 시 처리
								}
							});
							
							
							
							
						});
					},
					error:function(request,status,error){
				        alert("code = "+ request.status + " message = " + request.responseText + " error = " + error); // 실패 시 처리
				    }
				});
				
			});
		});
	</script>
	</div>
	<%@ include file="../common/ourfooter.jsp" %>		<!-- 하단 -->
</body>
</html>
