<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="company.model.vo.*"%>
<%
    Company c = (Company)request.getAttribute("company");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>기업 개인정보</title>
<style>
#mainDiv {
	margin-left:auto;
	margin-right:auto;
	margin-top:70px;
	width:900px;
}

#logoImgArea {
	width:300px;
	height:300px;
}

#logoImg {
	 width:200px;
	 height:200px;
	 margin:50px;
	 border:1px solid black;
}

#logoImgArea:hover {
cursor:pointer;

}

td {
	padding-top: 30px;
	font-size:15px;
	font-weight:bold;
}

</style>
<%@ include file="/views/common/import.html" %>
</head>
<body>
	<%@ include file="../common/companyMenu.jsp" %>  <!-- 메뉴바 -->
	<form id="modifyForm" action="<%=request.getContextPath()%>/updateCompany.me" method="post" encType="multipart/form-data">
	<div id="mainDiv">
		<h3>기업회원정보</h3>
		<table align="center" style="border-top:1px solid gray;">
			<tr>
				<td width="800px">
					<table>
						<tr>
							<td width="130px">아이디</td>
							<td width="370px"><div class="ui fluid input"><input type="text" value="<%=loginUser.getUserId()%>" disabled></div></td>
						</tr>
						<tr>
							<td>비밀번호</td>
							<td><div class="ui fluid input"><input type="password" placeholder="********" readonly disabled></div></td>
							<td width="170px"><label class="ui button blue" style="margin-left:20px;" onclick="modal()">비밀번호 변경</label></td>
						</tr>
						<tr>
							<td>인사담당자</td>
							<td>
								<% if(c.getHrManager() != null){ %>
								<div class="ui fluid input"><input type="text" id="hrName" name="hrName" value="<%=c.getHrManager()%>"></div>
								<% } else { %>
								<div class="ui fluid input"><input type="text" id="hrName" name="hrName" value="미입력"></div>
								<% } %>
							</td>
						</tr>
						<tr>
							<td>담당자 연락처</td>
							<td><div class="ui fluid input"><input type="text" id="phone" name="phone" value="<%= c.getPhone() %>"></div></td>
						</tr>
						<tr>
							<td>담당자 이메일</td>
							<td><div class="ui fluid input"><input type="text" id="email" name="email" value="<%= loginUser.getEmail() %>" disabled></div></td>
							 <td><input type="hidden" name="uno1" id="uno1" value="<%= loginUser.getUno() %>"></td>
						</tr>
					</table>
				</td>
				<td width="300px" align="center">
					<div id="logoImgArea">
						<% if(c.getLogo() != null) { %>
							<img src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=c.getLogo() %>" id="logoImg">
						<% } else { %>
							<img src="<%=request.getContextPath()%>/images/profilePhoto.jpg" id="logoImg">
						<% } %>
					</div>
					<input type="button" id="logoBtn" class="ui button gray" value="사진 업로드">
				</td>
			</tr>
		</table>
		<div id="fileArea">
			<input type="file" id="logoFile" name="logoFile" onchange="loadImg(this)" accept="image/*">
			
			</div>
		
		<div align="center" style="padding-top:50px;">
			 <label class="ui button" onclick="goBack()">취소</label>
			 <label class="ui button black" onclick="Change()">제출하기</label>
		</div>
	</div>
	</form>
	<div class="ui modal" style="width:600px; height:300px; text-align:center; vertical-align:middle; padding:20px;">
		<i class="close icon"></i>
		<div class="description">
			<div align="center">
				<h3>비밀번호 변경</h3>
				<form id="passForm" action="<%=request.getContextPath()%>/CpassSearch2.me" method="post">
				<input type="hidden" name="uno2" id="uno2" value="<%=loginUser.getUno() %>">
				<table>
					<tr>	
						<td width="170px">변경할 비밀번호</td>
						<td width="300px"><div class="ui fluid input"><input type="password" id="password" name="password"></div></td>
					</tr>
					<tr>
						<td>변경할 비밀번호 확인</td>
						<td><div class="ui fluid input"><input type="password" id="password2" name="password2"></div></td>
					</tr>
					
				</table>
				</form>
			</div><br>
			<div align="right" style="margin:20px;">
				<label class="ui button gray" style="margin-left:10px;">취소</label>
				<label class="ui secondary button" onclick="changePass()">변경</label>
			</div>
		</div>
	</div>
	
	
	<script>
	function Change(){
		swal ({
			title : "등록되었습니다.",
			icon: "success"
		}).then((value) => {
			$("#modifyForm").submit();
		});
	}
	
	function goBack(){
		alert("다 하고나서 링크");
	}
	
	function changePass(){
		swal ({
			title : "변경되었습니다.",
			icon: "success"
		}).then((value) => {
			$("#passForm").submit();
		});
	}
	
	function loadImg(value) {
		
		if(value.files && value.files[0]) {
			
			var reader = new FileReader();
			
			reader.onload = function(e) {
				
				$("#logoImg").attr("src",e.target.result);
					
			}
			reader.readAsDataURL(value.files[0]);
			
		}
		
	}

	$(function(){
		$("#fileArea").hide();
		$("#logoImgArea").click(function(){
			$("#logoFile").click();	
		});
		
		$("#logoBtn").click(function(){
			$("#logoFile").click();	
		});
	});	

	
	function modal(){
		$('.ui.modal')
			 .modal('show');
	}
	
	function changePass(){

		var pwd1 = $("#password").val();
		var pwd2 = $("#password2").val();

		if(pwd1 != "" || pwd2 != "") {
			if(pwd1 == pwd2) {
				$("#passSearch").click(function(){
					$("#passSearchForm").submit(); 	
				});
			} else {
				swal ({
					title : "오류",
					text : "비밀번호가 일치하지 않습니다.",
					icon: "success"
				}).then((value) => {
					$("#passSearchForm").submit();
				});
			};
		};
	};

	</script>
	
	<%@ include file="../common/ourfooter.jsp" %>		<!-- 하단 -->
</body>
</html>