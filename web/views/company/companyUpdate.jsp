<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,resume.model.vo.* , company.model.vo.*"%>
<%
	Company c = (Company) request.getAttribute("company");
%>    
<!DOCTYPE html>
<html>
<head>
<style>
#mainDiv {
	margin-left:auto;
	margin-right:auto;
	margin-top:70px;
	width:1100px;
	align:center;
}

.title {
	font-size:50px;
	font-weight:bold;
	width:850px;
}

#topTitle {
	border-bottom:2px solid gray;
}
.logoImg {
	width:100px;
	height:100px;
}
.subText {
	font-size:16px;
	font-weight:600;
	padding-bottom:5px;
}

.infoImg{
	border:1px solid lightgray;
	display:inline-block;
	vertical-align:middle;
	width:200px;
	height:200px;
	color:white;
	border-radius:10px;
	margin:17px;
	margin-bottom:20px;	
}

.infoImg > img{
	width:200px;
	height:200px;
	border-radius:10px;
	border:1px doted black;
}

.infoImg:hover{
	cursor:pointer;
	box-shadow: 0px 0px 2px 2px lightgray;
}
</style>
<meta charset="UTF-8">
<title>기업정보 수정</title>
<%@ include file="/views/common/import.html" %>
</head>
<body>
	<%@ include file="../common/companyMenu.jsp" %>
	<script src="<%=request.getContextPath()%>/semantic/components/calendar.js"></script>
	<form id="updateForm" action="<%=request.getContextPath()%>/updateIntro" method="post" encType="multipart/form-data">
	<div id="mainDiv">
	<input type="hidden" name="uno" value="<%=loginUser.getUno() %>">
		<table id="topTitle" align="center">
			<tr>
				<td width="100px;">
				<% if(c.getLogo() != null) { %>
					<img class="logoImg" src="<%= request.getContextPath() %>/attachment_uploadFiles/<%=c.getLogo() %>">
				<% } else { %>
					<img class="logoImg" src="<%= request.getContextPath() %>/images/companyDefault.jpeg">
				<% } %>
				</td>
				<td class="title"><%=c.getCompanyName() %></td>
				<td><label class="ui button blue" onclick="doUpdate()">수정완료</label></td>
			</tr>
		</table>
		<table style="width:1000px; padding:20px;" align="center">
			<tr>
				<td class="subText">대표자명</td>
				<td class="subText">사업자등록번호</td>
				<td class="subText">설립일</td>
			</tr>
			<tr>
				<td><div class="ui big disabled input" style="width:280px; margin-right:50px;"><input type="text" value="<%=c.getOwner() %>"></div></td>
				<td><div class="ui big disabled input" style="width:280px; margin-right:50px;"><input type="text" value="<%=c.getRegistNum() %>"></div></td>
				<td><div class="ui big calendar" id="date_calendar"><div class="ui big input left icon" style="width:280px;"><i class="calendar icon"></i><input type="text" placeholder="Date" name="birth" value="<%=c.getBirth() %>" readOnly></div></div></td>
			</tr>
			<tr>
				<td class="subText" style="padding-top:30px;">주소</td>
			</tr>
			<tr>
				<td colspan="3"><div class="ui big input"><input type="text" name="address" size="100" value="<%=c.getAddress() %>"></div></td>
			</tr>
			<tr>
				<td class="subText" style="padding-top:30px;">기업소개</td>
			</tr>
			<tr>
				<td colspan="3"><div class="ui big input form field"><textarea cols="100" rows="15" name="introText" style="resize: none;"><%=c.getIntro() %></textarea></div></td>
			</tr>
		</table>
		<table style="width:1000px; padding:20px;" align="center">	
			<tr>
				<td class="subText">사진</td>
			</tr>
			<tr>
				<td>
					<div class="infoImg" id="contentArea1">
					<% if(c.getAtlist().size() > 0) { %>
						<img id="contentPhoto1" src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=c.getAtlist().get(0).getChangeName()%>">
					<% } else { %>
						<img id="contentPhoto1">
					<% } %>
					</div>
					
					<div class="infoImg" id="contentArea2">
					<% if(c.getAtlist().size() > 1) { %>
						<img id="contentPhoto2" src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=c.getAtlist().get(1).getChangeName()%>">
					<% } else { %>
						<img id="contentPhoto2">
					<% } %>
					</div>
				
					<div class="infoImg" id="contentArea3">
					<% if(c.getAtlist().size() > 2) { %>
						<img id="contentPhoto3" src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=c.getAtlist().get(2).getChangeName()%>">
					<% } else { %>
						<img id="contentPhoto3">
					<% } %>
					</div>
					
					<div class="infoImg" id="contentArea4">
					<% if(c.getAtlist().size() > 3) { %>
						<img id="contentPhoto4" src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=c.getAtlist().get(3).getChangeName()%>">
					<% } else { %>
						<img id="contentPhoto4">
					<% } %>
					</div>
					
					<input type="hidden" name="hiddenValue" id="hiddenValue">
				</td>
			</tr>
			<tr>
				<td id="fileArea">
					<input type="file" id="fileImg1" name="fileImg1" onchange="loadImg(this, 1);">
					<input type="file" id="fileImg2" name="fileImg2" onchange="loadImg(this, 2);">
					<input type="file" id="fileImg3" name="fileImg3" onchange="loadImg(this, 3);">
					<input type="file" id="fileImg4" name="fileImg4" onchange="loadImg(this, 4);">
				</td>
			</tr>
		</table>
	</div>
	</form>
	<%@ include file="../common/ourfooter.jsp" %>		<!-- 하단 -->
	<script>
	var used = [false, false, false, false];
	
	function loadImg(value, num){
		if(value.files && value.files[0]){
			var reader = new FileReader();
			
			fileNm = value.value;
			if (fileNm != "") {
			    var ext = fileNm.slice(fileNm.lastIndexOf(".") + 1).toLowerCase();
			    if (!(ext == "gif" || ext == "jpg" || ext == "png" || ext == "PNG" || ext == "JPG" || ext == "jpeg" || ext == "JPEG")) {
			        alert("이미지파일 (.jpg, .png, .gif ) 만 업로드 가능합니다.");
			        return false;
			    }
			}

			reader.onload = function(e){
				
				switch(num){
					case 1 : $("#contentPhoto1").attr("src", e.target.result); used[0] = true; break;
					case 2 : $("#contentPhoto2").attr("src", e.target.result); used[1] = true; break;
					case 3 : $("#contentPhoto3").attr("src", e.target.result); used[2] = true; break;
					case 4 : $("#contentPhoto4").attr("src", e.target.result); used[3] = true; break;
				}
			}
			reader.readAsDataURL(value.files[0]);
		}
	}
	
	$(function(){
		var more = 1;
		
		$("#fileArea").hide();
		//사진영역 클릭했을 때, 파일이 로드되는 것!
		$("#contentArea1").click(function(){
			$("#fileImg1").click();
		});
		$("#contentArea2").click(function(){
			$("#fileImg2").click();
		});
		$("#contentArea3").click(function(){
			$("#fileImg3").click();
		});
		$("#contentArea4").click(function(){
			$("#fileImg4").click();
			
		
		});
	});
	
	function doUpdate() {
		swal ({
			   title : "수정되었습니다.",
			   icon : "success"
		}).then((value) => {
			$("#updateForm").submit();
		});
	}
	
	$('#date_calendar')
	  .calendar({
	    type: 'date'
	  })
	;
	</script>
</body>
</html>
