<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*, keyword.model.vo.*"%>
<%
	ArrayList<Keyword> klist = (ArrayList<Keyword>) request.getAttribute("klist");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>기업페이지</title>
<%@ include file="/views/common/import.html" %>
<script src="<%=request.getContextPath()%>/semantic/components/calendar.js"></script>
<style>
#allDiv {
	margin-left: auto;
	margin-right: auto;
	margin-top: 100px;
	width:900px;
}

.subTitle {
	color: gray;
	font-size: 15px;
	font-weight: bold;
	margin-left: 10px;
}

.explain {
	font-size: 20px;
	font-weight: bolder;
}

.explainDiv {
	width:150px;
	vertical-align:top;
}

.necessary {
	font-size: 15px;
	color: red;
}

.radio {
	font-size: 20px;
	font-weight: 500;
	padding-top:30px;
	padding-bottom:30px;
}

.input {
	height: 30px;
	width: 500px;
	padding-left : 10px;
}

.sort {
	margin : 30px;
}

.salary {
	width:170px;
	height:40px;
	font-size:15px;
	margin-left:10px;
	margin-right:10px;
	text-align:right;
	padding-right:10px;
}

.space {
	margin-left:10px;
	margin-right:10px;
}

.check-text	{
	font-size: 20px;
	font-weight: 500;
	margin-right: 20px;
}


#inwon {
  width:50px;
  font-size:20px;
}

hr {
	margin-top: 10px;
}

input {
	font-size : 20px;
}

input[type=radio] {
	visibility:hidden;
	margin:0;
	height:0;
	width:0;
}

input[type=date] {
	width:200px;
	height:30px;
}

.ui.action.input input[type="file"] {
  display: none;
}

.reception {
	visibility:hidden;
	margin:0;
	height:0;
	width:0;
}
.keywordArea{
	height:46px;
	width:100%;
	padding:5px;
	text-align:center;
	overflow:hidden;
}
.kbtnArea{
	display:inline-block;
	margin:2px;
}
	
	.more{
		color:gray;
		border:1px solid #e7e6e1;
		border-bottom-left-radius:8px;
		border-bottom-right-radius:8px;
		text-align:center;
		margin-bottom:80px;
	}
	.more:hover{
		cursor:pointer;
		color:black;
		/* background:#e7e6e1; */
	}

</style>
</head>
<body>
	<%@ include file="../common/companyMenu.jsp"%>
	<% if(loginUser != null && loginUser.getCategory().equals("C")) { %>
	<form id="allContents" action="<%=request.getContextPath()%>/insert.Rc" method="post" enctype="multipart/form-data">
	<input type="hidden" name="uno" value="<%= loginUser.getUno() %>">
	<div id="allDiv">
	
	<label class="subTitle">접수기간 및 방법</label>
	<hr>
	
	<!-- 공고제목 -->
	<div align="center">
	<table class="sort">
		<tr>
		<td>
			<div class = "explainDiv">
			<label class="explain">공고제목</label>
			<label class="necessary">*</label>
			</div>
		</td>
		<td>
			<div class="ui input">
				<input type="text" class="input" id="post-title" name="post-title">
			</div>
		</td>
		</tr>
	</table>
	</div>

	<label class="subTitle">고용형태 및 지원자격</label>
	<hr>
	<!-- 고용형태 -->
	<div align="center">
	<table class="sort">
		<tr>
			<td>
				<div class="explainDiv">
					<label class="explain">고용형태</label>
					<label class="necessary">*</label>
				</div>
			</td>
			<td class="radio" width="500px;">
				<label for="regular" class="ui big button gray"><input type="radio" name="employFrom" value="1" id="regular">정규직</label>
				<label for="contract" class="ui big button gray"><input type="radio" name="employFrom" value="2" id="contract">계약직</label>
				<label for="intern" class="ui big button gray"><input type="radio" name="employFrom" value="3" id="intern">인턴</label>
				<label for="dispatch" class="ui big button gray"><input type="radio" name="employFrom" value="4" id="dispatch">파견직</label>
			</td>
		</tr>
		<tr>
			<td>
				<div class="explainDiv">
				<label class="explain">경력</label>
				<label class="necessary">*</label>
				</div>
			</td>
			<td class="radio">
				<label for="newEmployee" class="ui big button gray"><input type="radio" name="career" value="NEW" id="newEmployee">신입</label>
				<label for="oldEmployee" class="ui big button gray"><input type="radio" name="career" value="OLD" id="oldEmployee">경력</label>
				<label for="noEmployee" class="ui big button gray"><input type="radio" name="career" value="N" id="noEmployee">경력무관</label>
			</td>
		</tr>
		<tr>
			<td>
				<div class="explainDiv">
				<label class="explain">학력</label>
				<label class="necessary">*</label>
				</div>
			</td>
			<td style="padding-top:30px; padding-bottom:30px;" colspan="3">
				<select name="education" id="education" class="ui dropdown" style="width:200px;">
					<option class="item" value="5">학력무관</option>
					<option class="item" value="1">고등학교 졸업</option>
					<option class="item" value="2">대학 졸업(2,3년)</option>
					<option class="item" value="3">대학 졸업(4년)</option>
					<option class="item" value="4">대학원 졸업</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div class="explainDiv">
				<label class="explain">모집인원</label>
				<label class="necessary">*</label>
				</div>
			</td>
			<td class="radio">
			<label for="Omyeong" class="ui big button gray"><input type="radio" name="personnel" value="1" id="Omyeong">O명</label>
			<label for="OOmyeong" class="ui big button gray"><input type="radio" name="personnel" value="2" id="OOmyeong">OO명</label>
			<label for="OOOmyeong" class="ui big button gray"><input type="radio" name="personnel" value="3" id="OOOmyeong">OOO명</label>
			</td>
		</tr>
	</table>
	</div>
	
		
	<br> <label class="subTitle">근무조건</label>
	<hr>
	
	<!-- 급여 -->
	<div align="center">
	<table class="sort">
		<tr>
		<td>
			<div class="explainDiv">
			<label class="explain">급여</label>
			<label class="necessary">*</label>
			</div>
		</td>
		<td style="width:500px;">
			<select name="salary" id="salary"  class="ui dropdown" style="width:100px;">
				<option value="1">연봉</option>
				<option value="2">월급</option>
				<option value="3">시급</option>
				<option value="4">회사내규</option>
			</select>
			<div class="ui input salary">
				<input type="text" class="salary" name="minSalary" id="minSalary" placeholder="최소급여(만원)">
			</div>
			<label>~</label>
			<div class="ui input salary">
				<input type="text" class="salary" name="maxSalary" id="maxSalary" placeholder="최대급여(만원)">
			</div>
		</td>
		</tr>
	</table>
	</div>
	
	<!-- 근무지역 -->
	<div align="center">
	<table class="sort">
		<tr>
		<td>
			<div class="explainDiv">
			<label class="explain">근무지역</label>
			<label class="necessary">*</label>
			</div>
		</td>
		<td>
			<div class="ui input">
				<input type="text" class="input" id="workingArea" name="workingArea" placeholder="ex) 서울특별시 강남구 강남구 테헤란로14길 6">
			</div>
		</td>
		</tr>
	</table>
	</div>
	
	<label class="subTitle">입력사항</label>
	<hr>
	<div align="center">
		<table>
			<tr>
				<td style="padding-top:30px; padding-bottom:20px;"><label class="explain">주요업무</label></td>
			</tr>
			<tr>
				<td><div class="ui form field" style="width:650px;"><textarea name="jobPosition" rows="8" style="resize: none;"></textarea></div></td>
			</tr>
			<tr>
				<td style="padding-top:30px; padding-bottom:20px;"><label class="explain">자격요건</label></td>
			</tr>
			<tr>
				<td><div class="ui form field" style="width:650px;"><textarea name="eligibility" rows="8" style="resize: none;"></textarea></div></td>
			</tr>
			<tr>
				<td style="padding-top:30px; padding-bottom:20px;"><label class="explain">우대사항</label></td>
			</tr>
			<tr>
				<td><div class="ui form field" style="width:650px;"><textarea name="preferential" rows="8" style="resize: none;"></textarea></div></td>
			</tr>
			<tr>
				<td style="padding-top:30px; padding-bottom:20px;"><label class="explain">혜택 및 복지</label></td>
			</tr>
			<tr>
				<td style="padding-bottom:50px;"><div class="ui form field" style="width:650px;"><textarea name="benWel" rows="8" style="resize: none;"></textarea></div></td>
			</tr>
		</table>
	</div>




	<label class="subTitle">접수기간 및 방법</label>
	<hr>

	<!-- 접수기간 -->
	<div align="center">
	<table class="sort">
		<tr>
		<td>
			<div class="explainDiv">
			<label class="explain">접수시간</label>
			<label class="necessary">*</label>
			</div>
		</td>
		<td style="width:500px;">
			<div class="ui form">
			  <div class="two fields">
			    <div class="field">
			      <label>시작일</label>
			      <div class="ui calendar" id="rangestart">
			        <div class="ui input left icon" style="padding:0">
			          <i class="calendar icon"></i>
			          <input type="text" name="startDate" id="startDate" placeholder="시작일" readOnly>
			        </div>
			      </div>
			    </div>
			    <div class="field">
			      <label>종료일</label>
			      <div class="ui calendar" id="rangeend">
			        <div class="ui input left icon" style="padding:0">
			          <i class="calendar icon"></i>
			          <input type="text" name="endDate" id="endDate" placeholder="종료일" readOnly>
			        </div>
			      </div>
			    </div>
			  </div>
			</div>
		</td>
		</tr>
	</table>
	</div>
	
	<!-- 접수방법 -->
	<div align="center">
	<table class="sort">
		<tr>
		<td>
			<div class="explainDiv">
			<label class="explain">접수방법</label>
			<label class="necessary">*</label>
			</div>
		</td>
		<td style="width:500px;">
			<label for="byITDuck" class="ui big button gray"><input type="radio" name="reception" value="1" id="byITDuck">잇덕 접수</label>
     		<label for="byMail" class="ui big button gray"><input type="radio" name="reception" value="2" id="byMail">이메일</label>
     		<!-- <label for="byCompany" class="ui big button gray"><input type="radio" name="reception" value="3" id="byCompany">기업 홈페이지</label> -->
		</td>
		</tr>
	</table>
	</div>
	
	<label class="subTitle">회사 이력서 양식</label>
	<hr>

	<!-- 양식추가 -->
	<div align="center">
	<table class="sort">
		<tr>
			<td>
				<div class="explainDiv">
				<label class="explain">이력서 양식</label>
				</div>
			</td>
			<td style="width:500px;">
				<div class="ui toggle checkbox">
		 			<input type="checkbox" name="addResumeBtn" id="addResumeBtn" value="on">
					<label for="addResumeBtn">추가</label>
				</div>
			</td>
		</tr>
	</table>
	</div>

	<!-- 파일 추가 -->
	<div align="center">
	<table class="sort">
		<tr>
			<td>
				<div class="explainDiv">
				<label class="explain">파일 첨부</label>
				</div>
			</td>
			<td>
				<div class="ui action input disabled" id="fileDiv">
				  <input type="text" id="fileBtn1" placeholder="파일명" readonly>
				  <input type="file" name="addResume">
				  <div class="ui icon button disabled" id="fileBtn">
				    <i class="attach icon"></i>
				  </div>
				</div>
			</td>
		</tr>
	</table>
	</div>
	
	<label class="subTitle">공고 키워드</label><label style="font-size:10px; color:lightgray;"> (5개)</label>
	<hr>
	<div class="keywordArea" align="center">
	<%  int i = 1;
		for(Keyword k : klist){ %>
		<div class="kbtnArea">
		<label class="ui button kbtn"><input type="checkbox" name="keywords" value="<%=k.getKcode()%>" style="visibility:hidden; margin:0; padding:0; width:0;"><%=k.getKname() %></label>
		</div>
		<% if(i%6==0){%> <br> <% } i++;
		} %>
	</div>
	
	
	<div class="more"><i class="chevron down icon"></i></div>

	<div style="margin-bottom:100px; text-align:right;">
		<input class="ui button red" type="reset" value="취소">
		<label class="ui button blue" onclick="insertRecruit()">등록</label>
	</div>
	</div>

	</form>
	
	<%@ include file="../common/ourfooter.jsp" %>

	<script>
	var today = new Date();
	var more = 1;	

	$('#rangestart').calendar({
		minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
		type: 'date',
		endCalendar: $('#rangeend')
	});
	$('#rangeend').calendar({
		minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
		type: 'date',
		startCalendar: $('#rangestart')
	});
	
	$(document).ready(function() {
		$("input:radio[name='employFrom']").click( function() {
			if ($(this).prop('checked') == true) {
				$("input:radio[name='employFrom']").parent().removeClass("blue");
				$("input:radio[name='employFrom']").parent().addClass("gray");
				$(this).parent().removeClass("gray");
				$(this).parent().addClass("blue");
			}
		});
		
		$("input:radio[name='career']").click( function() {
			if ($(this).prop('checked') == true) {
				$("input:radio[name='career']").parent().removeClass("blue");
				$("input:radio[name='career']").parent().addClass("gray");
				$(this).parent().removeClass("gray");
				$(this).parent().addClass("blue");
			}
		}); 
		
		$("input:radio[name='personnel']").click( function() {
			if ($(this).prop('checked') == true) {
				$("input:radio[name='personnel']").parent().removeClass("blue");
				$("input:radio[name='personnel']").parent().addClass("gray");
				$(this).parent().removeClass("gray");
				$(this).parent().addClass("blue");
			}
		});
		
		$("input:radio[name='reception']").click( function() {
			if ($(this).prop('checked') == true) {
				$("input:radio[name='reception']").parent().removeClass("blue");
				$("input:radio[name='reception']").parent().addClass("gray");
				$(this).parent().removeClass("gray");
				$(this).parent().addClass("blue");
			}
		}); 
		
		$("input:checkBox[name='addResumeBtn']").click( function() {
			if ($(this).prop('checked') == true) {
				$("#fileDiv").removeClass("disabled");
				$("#fileBtn").removeClass("disabled");
			} else {
				$("#fileDiv").addClass("disabled");
				$("#fileBtn").addClass("disabled");
			}
		}); 
		
		$("#fileBtn1").click(function() {
			$(this).parent().find("input:file").click();
		});
		
		$("#fileBtn").click(function() {
			$(this).parent().find("input:file").click();
		});


		$('input:file', '.ui.action.input').on('change', function(e) {
			var name = e.target.files[0].name;
			$('input:text', $(e.target).parent()).val(name);
		});

	});
	
	function insertRecruit(){
		var title = document.getElementById("post-title").value;
		var minSal = document.getElementById("minSalary").value;
		var maxSal = document.getElementById("maxSalary").value;
		var workingArea = document.getElementById("workingArea").value;
		var strDate = document.getElementById("startDate").value;
		var endDate = document.getElementById("endDate").value;
	     
		if($.trim(title) == ""){
			swal ("등록실패", "채용 공고 제목을 입력해주세요!", "error");
			var offset = $("#post-title").offset();  
			$('html').animate({scrollTop : offset.top / 3}, 0);
		} else {
			if($(':radio[name="employFrom"]:checked').length < 1){
				swal ("등록실패", "고용형태를 선택해주세요!", "error");
				var offset = $("#regular").offset();  
				$('html').animate({scrollTop : offset.top / 3}, 0);
		   	} else {
		   		if($(':radio[name="career"]:checked').length < 1){
					swal ("등록실패", "경력을 선택해주세요!", "error");
					var offset = $("#newEmployee").offset();  
					$('html').animate({scrollTop : offset.top / 3}, 0);
			   	} else {
			   		if($(':radio[name="personnel"]:checked').length < 1){
						swal ("등록실패", "모집인원 선택해주세요!", "error");
						var offset = $("#Omyeong").offset();  
						$('html').animate({scrollTop : offset.top / 3}, 0);
				   	} else {
				   		if($.trim(minSal) == ""){
				   			$("#minSalary").focus();
				   			swal ({
								title : "등록실패",
								text : "최소급여를 입력해주세요!",
								icon: "error"
							}).then((value) => {
								$("#minSalary").focus();
							});							
						} else {
							if($.trim(maxSal) == ""){
								swal ("등록실패", "최대급여를 입력해주세요!", "error");
								$("#minSalary").focus();
							} else {
								if($.trim(workingArea) == ""){
									swal ("등록실패", "근무지역을 입력해주세요!", "error");
									$("#workingArea").focus();
								} else {
									if($.trim(strDate) == ""){
										swal ("등록실패", "공고 시작일을 입력해주세요!", "error");
										$("#startDate").focus();
									} else {
										if($.trim(minSal) == ""){
											swal ("등록실패", "공고 종료일을 입력해주세요!", "error");
											$("#endDate").focus();
										} else {
											if($(':radio[name="reception"]:checked').length < 1){
												swal ("등록실패", "접수방법을 선택해주세요!", "error");
											} else {
												if($(':checkBox[name="keywords"]:checked').length < 1){
													swal ("등록실패", "키워드를 선택해주세요!", "error");
												} else {
													swal ({
														title : "등록되었습니다.",
														icon: "success"
													}).then((value) => {
														$("#allContents").submit();
													});
												}
											}
										}
									}
								}
							}
						}
				   	}
			   	}
		   	}
		}
	}
	
	$(':text[name="minSalary"]').on('keyup', function() {
	    $(this).val($(this).val().replace(/[^0-9]/g,""));
	});
	
	$(':text[name="maxSalary"]').on('keyup', function() {
	    $(this).val($(this).val().replace(/[^0-9]/g,""));
	});

	$('.more').click(function(){
		if(more == 1){
			$('.keywordArea').animate({
				'height':'500px',
				'duration':'1s'
			});
			$(this).html('<i class="chevron up icon"></i>');
			more = 0;
		} else{
			$('.keywordArea').animate({
				'height':'46px',
				'duration':'1s'
			})
			$(this).html('<i class="chevron down icon"></i>');
			more = 1;
			
		}
	});
	
	$("input:checkBox[name='keywords']").click(function(){
		if($(':checkBox[name="keywords"]:checked').length > 5) {
			$(this).prop('checked', false);
			swal ("키워드 갯수 초과", "키워드는 최대 5개까지 가능합니다.", "error");
		} else if($(this).prop('checked') == true) {
			$(this).parent().addClass("orange");
		} else {
			$(this).parent().removeClass("orange");
		}
	});

	</script>
	<% } else if(loginUser != null) { %>
	<script>
	swal ({
		title : "기업회원이 아닙니다!",
		icon : "error"
	}).then((value) => {
		document.location.href="<%=request.getContextPath()%>/index.jsp"; 
	});
	</script>	
	<% } else { %>
	<script>
	swal ({
		title : "로그인이 필요한 서비스입니다!",
		icon : "error"
	}).then((value) => {
			document.location.href="<%=request.getContextPath()%>/loginBannerImg.me?type=2"; 
	});
	</script>
	<% } %>
</body>
</html>