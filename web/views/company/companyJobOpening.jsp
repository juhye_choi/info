<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, company.model.vo.*"%>
<%
	RecruitCount rc = (RecruitCount) request.getAttribute("Count");
	ArrayList<Recruit> ra = (ArrayList<Recruit>) request.getAttribute("recAllList");
	ArrayList<Recruit> ri = (ArrayList<Recruit>) request.getAttribute("recIngList");
	ArrayList<Recruit> re = (ArrayList<Recruit>) request.getAttribute("recEndList");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>기업페이지</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<%@ include file="/views/common/import.html" %>
<style>
.mainBtn{
	width:33%;
	height:80px;
	margin:-2px;
	margin-bottom:50px;
	padding-top:30px;
	background:#e7e6e1;
	border:1px solid black;
	display:inline-block;
	text-align:center;
	vertical-align:middle;
	font-size:25px;
}

.mainBtn:hover{
	cursor:pointer;
}

.recList:hover{
	cursor:pointer;
	color:blue;
	font-weight:900;
}

.btnArea{
	text-align:center;	
}

#warning {
	width:100%;
	height:500px;
	text-align:center;
	padding-top:100px;
}

th {
	color:gray;
}
<%@ include file="/css/companyBodyStyle.css" %>

</style>
</head>
<body>
	<%@ include file="../common/companyMenu.jsp" %>
	<div id="allDiv">
		<div class="btnArea">
			<div class="mainBtn" id="recruitING">채용중 
				<% if(rc.getIngRecCount() == 0) { %>
					0
				<% } else { %>
					<%=rc.getIngRecCount() %>
				<% } %>	
			</div>
			<div class="mainBtn" id="recruitEND">채용종료
				<% if(rc.getEndRecCount() == 0) { %>
					0
				<% } else { %>
					<%=rc.getEndRecCount() %>
				<% } %>	
			</div>
			<div class="mainBtn" id="recruitALL">전체 
				<% if(rc.getAllRecCount() == 0) { %>
					0
				<% } else { %>
					<%=rc.getAllRecCount() %>
				<% } %>	
			</div>
		</div>
		
		<div id="divIng">
		<% if(rc.getIngRecCount() == 0) { %>
			<table style="margin-left:auto; margin-right:auto; margin-top:100px; margin-bottom:200px;">
				<tr>
					<td rowspan="2" style="margin:auto;">
						<img src="<%=request.getContextPath()%>/images/warning.PNG">
					</td>
					<td>
					<h2>등록된 공고가 없습니다.</h2>
					</td>
				</tr>
				<tr>
					<td>
					<a href="<%=request.getContextPath()%>/selectList.ke?type=3">채용공고 등록하기 ></a>
					</td>
				</tr>
			</table>
		<% } else { %>
			<table class="ui sortable celled table" id="ingTable" style="margin-bottom:200px">
				<thead align="center">
					<tr style="text-align:center;">
						<th width="100px">순번</th>
						<th>제목</th>
						<th width="200px">기간</th>
						<th width="100px">종료</th>
					</tr>
				</thead>
				<tbody align="center">
				<%  int i = ri.size();
					for(Recruit r : ri) { 
				%>
					<tr>
						<td style="text-align:center;"><%=i %></td>
						<td><label class="recList" onclick="window.open('<%=request.getContextPath()%>/recruit.so?RECID=<%=r.getRecid() %>&type=2', '채용공고', 'location=0, resizable=no, menubar=no, status=no, toolbar=no'); "><%=r.getRec_title() %></label></td>
						<td style="text-align:center;"><%=r.getRec_start() %> ~ <%=r.getRec_finish() %></td>
						<td style="text-align:center;"><button class="ui red button mini buttonStyle" onClick="location.href='<%=request.getContextPath()%>/DeleteRecruit.ad?RECID=<%=r.getRecid() %>&type=1'">종료</button></td>
					</tr>
				<% i--; } %>
				</tbody>
			</table>
		<% } %>
		</div>
			
			
		<div id="divEnd" class="tableSection" style="display:none;">
			<% if(rc.getEndRecCount() == 0) { %>
			<table style="margin-left:auto; margin-right:auto; margin-top:100px; margin-bottom:200px;">
				<tr>
					<td rowspan="2" style="margin:auto;">
						<img src="<%=request.getContextPath()%>/images/warning.PNG">
					</td>
					<td>
					<h2>종료된 공고가 없습니다.</h2>
					</td>
				</tr>
				<tr>
					<td>
				
					</td>
				</tr>
			</table>
		<% } else { %>
			<table class="ui sortable celled table" id="endTable" style="margin-bottom:200px">
				<thead align="center">
					<tr style="text-align:center;">
						<th width="100px">순번</th>
						<th>제목</th>
						<th width="200px">기간</th>
						<th width="100px">종료</th>
					</tr>
				</thead>
				<tbody align="center">
				<% 	int j = re.size(); 
					for(Recruit r : re) { 
				%>
					<tr>
						<td style="text-align:center;"><%=j %></td>
						<td><label class="recList" onclick=" window.open('<%=request.getContextPath()%>/recruit.so?RECID=<%=r.getRecid() %>&type=2', '채용공고', 'location=0, resizable=no, menubar=no, status=no, toolbar=no'); "><%=r.getRec_title() %></label></td>
						<td style="text-align:center;"><%=r.getRec_start() %> ~ <%=r.getRec_finish() %></td>
						<td><button class="ui button mini buttonStyle disabled">종료</button></td>
					</tr>
				<% j--; } %>
				</tbody>
			</table>
		<% } %>
		</div>
		
		
		<div id="divAll" class="tableSection" style="display:none;">
			<% if(rc.getAllRecCount() == 0) { %>
			<table style="margin-left:auto; margin-right:auto; margin-top:100px; margin-bottom:200px;">
				<tr>
					<td rowspan="2" style="margin:auto;">
						<img src="<%=request.getContextPath()%>/images/warning.PNG">
					</td>
					<td>
					<h2>등록된 공고가 없습니다.</h2>
					</td>
				</tr>
				<tr>
					<td>
					<a href="<%=request.getContextPath()%>/selectList.ke?type=3">채용공고 등록하기 ></a>
					</td>
				</tr>
			</table>
		<% } else { %>
			<table class="ui sortable celled table" id="allTable" style="margin-bottom:200px">
				<thead align="center">
					<tr style="text-align:center;">
						<th width="100px">순번</th>
						<th>제목</th>
						<th width="200px">기간</th>
						<th width="100px">종료</th>
					</tr>
				</thead>
				<tbody align="center">
				<% 	int z = ra.size();
					for(Recruit r : ra) { 	
				%>
					<tr>
						<td style="text-align:center;"><%=z %></td>
						<td><label class="recList" onclick=" window.open('<%=request.getContextPath()%>/recruit.so?RECID=<%=r.getRecid() %>&type=2', '채용공고', 'location=0, resizable=no, menubar=no, status=no, toolbar=no'); "><%=r.getRec_title() %></label></td>
						<td style="text-align:center;"><%=r.getRec_start() %> ~ <%=r.getRec_finish() %></td>
						<td style="text-align:center;">
						<%  Date today = new Date();
							if(today.compareTo(r.getRec_finish()) < 0){ %>
						<button class="ui red button mini buttonStyle" onClick="location.href='<%=request.getContextPath()%>/DeleteRecruit.ad?RECID=<%=r.getRecid() %>&type=1'">종료</button>
						<% }else { %>
						<button class="ui button mini buttonStyle disabled">종료</button>
						<% } %>
						</td>
					</tr>
				<% z--; } %>
				</tbody>
			</table>
		<% } %>
	</div>
</div>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
	<script>
		$(function(){
			//기본값
			$("#recruitING").css({"border-bottom":"white", "background":"white"});
			$("#recruitEND").css({"color":"gray", "border":"1px solid gray"});
			$("#recruitTOT").css({"color":"gray", "border":"1px solid gray"});
			
			$("#ingTable").tablesort();	
			$("#endTable").tablesort();
			$("#allTable").tablesort();	
			
			$("#recruitING").click(function(){
				$(this).css({"border":"1px solid black", "border-bottom":"white", "background":"white", "color":"black"});
				$("#recruitEND").css({"border":"1px solid gray", "border-bottom":"1px solid black", "background":"#e7e6e1", "color":"gray"});
				$("#recruitALL").css({"border":"1px solid gray", "border-bottom":"1px solid black", "background":"#e7e6e1", "color":"gray"});
				
				$("#divIng").show();
				$("#divEnd").hide();
				$("#divAll").hide();
			});
			$("#recruitEND").click(function(){
				$(this).css({"border":"1px solid black", "border-bottom":"white", "background":"white", "color":"black"});
				$("#recruitING").css({"border":"1px solid gray", "border-bottom":"1px solid black", "background":"#e7e6e1", "color":"gray"});
				$("#recruitALL").css({"border":"1px solid gray", "border-bottom":"1px solid black", "background":"#e7e6e1", "color":"gray"});

				$("#divIng").hide();
				$("#divEnd").show();
				$("#divAll").hide();
			});
			$("#recruitALL").click(function(){
				$(this).css({"border":"1px solid black", "border-bottom":"white", "background":"white", "color":"black"});
				$("#recruitING").css({"border":"1px solid gray", "border-bottom":"1px solid black", "background":"#e7e6e1", "color":"gray"});
				$("#recruitEND").css({"border":"1px solid gray", "border-bottom":"1px solid black", "background":"#e7e6e1", "color":"gray"});
				

				$("#divIng").hide();
				$("#divEnd").hide();
				$("#divAll").show();
			});
			
			
		});
		

	</script>
	<%@ include file="../common/ourfooter.jsp" %>		<!-- 하단 -->
</body>
</html>