<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	int result = (Integer) request.getAttribute("result");
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>인재열람</title>
 
<script type="text/javascript"src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript"src="https://cdn.iamport.kr/js/iamport.payment-1.1.5.js"></script>
<%@ include file="/views/common/import.html"%>
<style>
#premium2title {
	font-family: "문체부 제목 돋음체";
	text-align: center;
	font-size: 2em;
	margin-bottom: 30px;
}

.main {
	border: 1px solid #A3AFBA;
	width: 60%;
	margin-left: auto;
	margin-right: auto;
	display: block;
	padding-top: 30px;
}

.title {
	width: 250px !important;
	height: 200px;
	margin: 20px;
	margin-left: 27px;
	padding-left: 20px;
	padding-top: 15px;
	display: inline-block;
	overflow: hidden;
}
#beaf{
	margin-right:20px;
	margin-left:20px;
	padding-top:30px;
	border: 1px solid #A3AFBA;
}
.example {
	width: 35%;
	height: 400px;
	margin-left: 30px;
	padding-left: 70px;
	padding-top: 15px;
	display: inline-block;
	overflow: hidden;
}

#smalltitle {
	font-size: 1.5em;
	text-decoration: underline;
	text-underline-position: under;
}

#smalltitle2 {
	font-size: 1.2em;
}

#bebutton {
	color: #7A8288;
	background-color: #D2D8DD;
	padding: 5px;
	font-family: "배달의민족 주아";
	text-align: center;
	font-size: 1.6em;
	width: 100px;
}

#betitle {
	font-family: "배달의민족 주아";
	font-size: 2em;
	display: inline-block;
}

#beimg {
	display: inline-block;
}

#betext {
	color: #7A8288;
}

.example2 {
	width: 55%;
	height: 400px;
	margin-left:50px;
	padding-top: 15px;
	display: inline-block;
	overflow: hidden;
}

.afsession {
	display: inline-block;
}

#afbutton {
	color: white;
	background-color: red;
	padding: 5px;
	font-family: "배달의민족 주아";
	text-align: center;
	font-size: 1.6em;
	width: 100px;
}

#afuserimg img {
	width: 120px;
	height: 120px;
	border-radius: 75px;
	margin-bottom: 50px;
}

#afsession {
	margin-left: 20px;
}

#aftitle {
	font-family: "배달의민족 주아";
	font-size: 2em;
	display: inline-block;
}

#aftext {
	
}

#afimg {
	display: inline-block;
}
#option{
	text-align: center;
}
.select:hover{background-color:#e7e6e1; cursor:pointer;}
.select{
	width:15%;
	height:50px;
	margin-left : 20px;
	border: 1px solid black;
	display: inline-block;
	padding-top : 12px;
	font-size:1.2em;
	font-weight: bold;
} 
.selectoption{
	padding-top : 30px;
	padding-left : 30px;
	padding-bottom: 50px;
}
#select1style{
	margin-left : 70%;
}
.selectclass{
	display:inline-block;
}
#price{
	font-size: 2em;
	color:red;
}
#pricecomment{
	font-size: x-small;	
	display:inline-block;
}
.explain{
	margin:30px;
}
</style>  
</head>
<body>
	<%@ include file="../common/companyMenu.jsp"%>
	<div style="height: 85px"></div>
	<div class="main">
		<p id="premium2title">인재열람상품 이용 방법</p>
		<div class="title">
			<p id="smalltitle">첫째.</p>
			<p id="smalltitle2">
				<strong>인재검색에서 이력서 검색</strong>
			</p>
			<br>
			<p>
				<strong>경력 , 스킬 , 능력</strong>에 관한<br> 키워드로 원하는 맞춤 인재검색
			</p>
		</div>
		<div class="title">
			<p id="smalltitle">둘째.</p>
			<p id="smalltitle2">
				<strong>이력서 확인</strong>
			</p>
			<br>
			<p>
				관심이 가튼 인재의 이력서를<br> <strong>[자세히 보기]</strong> 버튼을 통해 확인
			</p>
		</div>
		<div class="title">
			<p id="smalltitle">셋째.</p>
			<p id="smalltitle2">
				<strong>해당 인재에게 입사 제의</strong>
			</p>
			<br>
			<p>
				연락처 확인 후 구직자에게<br> 별도로 입사제의 연락
			</p>
		</div>
		<div id="beaf">
			<div class="example">
				<div id="bebutton">
					<p>적용전</p>
				</div>
				<br> <br>
				<div>
					<p id="betitle">잇O</p>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;****&nbsp;&nbsp;.**&nbsp;&nbsp;.**
					&nbsp;&nbsp;&nbsp;&nbsp;| &nbsp;&nbsp;구직중
				</div>
				<br>
				<div>
					<div id="beimg">
						<img alt="" src="<%=request.getContextPath()%>/images/email.png">
					</div>
					<div id="beimg">
						<p id="betext">&nbsp;&nbsp;&nbsp;****&nbsp;@&nbsp;****&nbsp;.&nbsp;*</p>
					</div>
				</div>
				<br>
				<div>
					<div id="beimg">
						<img alt="" src="<%=request.getContextPath()%>/images/phone.png">
					</div>
					<div id="beimg">
						<p id="betext">&nbsp;&nbsp;&nbsp;***&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;****&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;**</p>
					</div>
				</div>
				<br>
				<div>
					<div id="beimg">
						<img alt="" src="<%=request.getContextPath()%>/images/school.png">
					</div>
					<div id="beimg">
						<p id="betext">&nbsp;&nbsp;&nbsp;****&nbsp;&nbsp;학교&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*****&nbsp;&nbsp;&nbsp;전공</p>
					</div>
				</div>
			</div>
			<div class="example2">
				<div id="afbutton">
					<p>적용후</p>
				</div>
				<br> <br>

				<div>
					<div class="afsession" id="afuserimg">
						<img alt="" src="<%=request.getContextPath()%>/images/ITDUCKduck.png">
					</div>
					<div class="afsession" id="afsession">
						<div>
							<p id="aftitle">잇덕</p>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1995&nbsp;&nbsp;.12&nbsp;&nbsp;.02&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;구직중
						</div>
						<br>
						<div>
							<div id="afimg">
								<img alt="" src="<%=request.getContextPath()%>/images/email.png">
							</div>
							<div id="afimg">
								<p id="aftext">&nbsp;&nbsp;&nbsp;ITDUCK&nbsp;@&nbsp;korea&nbsp;.&nbsp;com</p>
							</div>
						</div>
						<br>
						<div>
							<div id="afimg">
								<img alt="" src="<%=request.getContextPath()%>/images/phone.png">
							</div>
							<div id="afimg">
								<p id="betext">&nbsp;&nbsp;&nbsp;010&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;1234&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;5678</p>
							</div>
						</div>
						<br>
						<div>
							<div id="afimg">
								<img alt="" src="<%=request.getContextPath()%>/images/school.png">
							</div>
							<div id="afimg">
								<p id="betext">&nbsp;&nbsp;&nbsp;한국&nbsp;&nbsp;대학교&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ITDUCK&nbsp;&nbsp;&nbsp;전공</p>
							</div>
						</div>
						<br>
					</div>
				</div>
			</div>
		</div>
		<br>
		<hr style="width : 95%">
		<br>
		<div id = "option">
			<div class="select" id = "selectone"><p>인재 1,000건</p></div>
			<div class="select" id = "selecttwo"><p>인재 500건</p></div>
			<div class="select" id = "selectthree"><p>인재 100건</p></div>
			<div class="select" id = "selectfour"><p>인재 50건</p></div>
			<div class="select" id = "selectfive"><p>인재 10건</p></div>
			<br>
			<br>
			<hr style="width : 95%">
			<br>
		</div>
			<div class = "selectoption" style="">
			
				<div id = "select1" style="display:none">
					<p style="font-size: 2em; font-weight: bold;" >인재 1,000건<p>
					<p>총 1,000 명의 인재의 이력서를 열람하고 입사제의를 할 수 있습니다. <br>
					상품 유효기간은 없으며 단 모든 열람권을 이용 후 추가 결제가 가능합니다.
					</p>
					<div id = "select1style">
						<p class="selectclass"><strong>가격</strong></p> &nbsp;&nbsp;&nbsp;
						<p class="selectclass" id ="price">490,000</p>&nbsp;&nbsp;&nbsp;
						<p class="selectclass" id = "pricecoment">원 / 건당 490원</p>&nbsp;&nbsp;&nbsp;&nbsp;
						<br>
						<button id="p1000"class="ui red button" style="margin-left:50%;" onclick="importStart(1000)">결제하기</button>
					</div>
					<br>
					<hr style="width : 95%">
				</div>
			
			
				<div id = "select2" style="display:none">
					<p style="font-size: 2em; font-weight: bold;" >인재 500건<p>
					<p>총 500 명의 인재의 이력서를 열람하고 입사제의를 할 수 있습니다. <br>
					상품 유효기간은 없으며 단 모든 열람권을 이용 후 추가 결제가 가능합니다.
					</p>
					<div id = "select1style">
						<p class="selectclass"><strong>가격</strong></p> &nbsp;&nbsp;&nbsp;
						<p class="selectclass" id ="price">245,000</p>&nbsp;&nbsp;&nbsp;
						<p class="selectclass" id = "pricecoment">원 / 건당 490원</p>&nbsp;&nbsp;&nbsp;&nbsp;
						<br>
						<button class="ui red button" style="margin-left:50%;" onclick="importStart(500)" value=500>결제하기</button>
					</div>
					<br>
					<hr style="width : 95%">
				</div>
			
			
				<div id = "select3" style="display:none">
					<p style="font-size: 2em; font-weight: bold;" >인재 100건<p>
					<p>총 100 명의 인재의 이력서를 열람하고 입사제의를 할 수 있습니다. <br>
					상품 유효기간은 없으며 단 모든 열람권을 이용 후 추가 결제가 가능합니다.
					</p>
					<div id = "select1style">
						<p class="selectclass"><strong>가격</strong></p> &nbsp;&nbsp;&nbsp;
						<p class="selectclass" id ="price">49,000</p>&nbsp;&nbsp;&nbsp;
						<p class="selectclass" id = "pricecoment">원 / 건당 490원</p>&nbsp;&nbsp;&nbsp;&nbsp;
						<br>
						<button class="ui red button" style="margin-left:50%;" onclick ="importStart(100)">결제하기</button>
					</div>
					<br>
					<hr style="width : 95%">
				</div>
			
			
				<div id = "select4" style="display:none">
					<p style="font-size: 2em; font-weight: bold;" >인재 50건<p>
					<p>총 50 명의 인재의 이력서를 열람하고 입사제의를 할 수 있습니다. <br>
					상품 유효기간은 없으며 단 모든 열람권을 이용 후 추가 결제가 가능합니다.
					</p>
					<div id = "select1style">
						<p class="selectclass"><strong>가격</strong></p> &nbsp;&nbsp;&nbsp;
						<p class="selectclass" id ="price">24500</p>&nbsp;&nbsp;&nbsp;
						<p class="selectclass" id = "pricecoment">원 / 건당 490원</p>&nbsp;&nbsp;&nbsp;&nbsp;
						<br>
						<button class="ui red button" style="margin-left:50%;" onclick ="importStart(50)">결제하기</button>
					</div>
					<br>
					<hr style="width : 95%">
				</div>
		
				<div id = "select5" style="display:none">
					<p style="font-size: 2em; font-weight: bold;" >인재 10건<p>
					<p>총 10명의 인재의 이력서를 열람하고 입사제의를 할 수 있습니다. <br>
					상품 유효기간은 없으며 단 모든 열람권을 이용 후 추가 결제가 가능합니다.
					</p>
					<div id = "select1style">
						<p class="selectclass"><strong>가격</strong></p> &nbsp;&nbsp;&nbsp;
						<p class="selectclass" id ="price">4900</p>&nbsp;&nbsp;&nbsp;
						<p class="selectclass" id = "pricecoment">원 / 건당 490원</p>&nbsp;&nbsp;&nbsp;&nbsp;
						<br>
						<button class="ui red button" style="margin-left:50%;"onclick ="importStart(10)">결제하기</button>
					</div>
					<br>
					<hr style="width : 95%">
				</div>
			</div>
			
			
		<div class="explain" style="font-size:small;">
			<strong>[인재열람 상품 이용 안내]</strong>
			<br>
			<ul>
				<li>비공개된 구직자의 이력서는 확인 할 수 없습니다.</li>
				<li>인재열람 상품의 기간 제한은 없으나 관리자에 의해 탈퇴조치 시 모든 상품은 소멸됩니다.</li>
				<li>열람한 인재가 비공개전환 또는 탈퇴 시 확인할 수 없습니다.</li>
				<li>현재 결제 상품이 남아있는 경우 모든 열람권을 사용하기 전까지 추가 결제가 불가능합니다.</li>
			</ul>
		</div>
	</div>
		<script>
				
				$(function(){
					$("#selectone").click(function(){
						$("#select1").toggle();
						$("#select2").hide();
						$("#select3").hide();
						$("#select4").hide();
						$("#select5").hide();
					});
					$("#selecttwo").click(function(){
						$("#select1").hide();
						$("#select2").toggle();
						$("#select3").hide();
						$("#select4").hide();
						$("#select5").hide();
					});
					$("#selectthree").click(function(){
						$("#select1").hide();
						$("#select2").hide();
						$("#select3").toggle();
						$("#select4").hide();
						$("#select5").hide();
					});
					$("#selectfour").click(function(){
						$("#select1").hide();
						$("#select2").hide();
						$("#select3").hide();
						$("#select4").toggle();
						$("#select5").hide();
					});
					$("#selectfive").click(function(){
						$("#select1").hide();
						$("#select2").hide();
						$("#select3").hide();
						$("#select4").hide();
						$("#select5").toggle();
					});
				})
				function importStart(num){
					//초기화할 변수들
					// 상품 갸격
					var pamount = 0;
					// 상품코드
					var pcode = 0;
					
					if(num==1000){
						pamount= 100;
						pcode = 2;
					}else if(num==500){
						pamount=245000;
						pcode=3;
					}else if(num==100){
						pamount=100;
						pcode=4;
					}else if(num==50){
						pamount=24500;
						pcode=5;
					}else{
						pamount=4900;
						pcode=6;
					}
					console.log(pamount);
					console.log(pcode);
					<%if(loginUser != null){%>
						<%if(result == 0){%>
						
						var IMP = window.IMP;
						IMP.init('imp79349410');
						
						IMP.request_pay({
							pg : 'inicis', // version 1.1.0부터 지원.
							pay_method : 'card',
							merchant_uid : 'merchant_' + new Date().getTime(),
							name : '인재검색 ' + num + '건(㈜ITDUCK)',
							amount : pamount, //판매 가격
							buyer_email : 'itduck1004@gmail.com',
							buyer_name : '<%=loginUser.getUserName()%>' ,
							buyer_tel : '<%=loginUser.getPhone()%>',
							app_scheme : '<%=request.getContextPath()%>/index.jsp'
							/* buyer_addr : '서울특별시 강남구 삼성동', // 주소
							buyer_postcode : '123-456', //우편번호 */
						}, function(rsp) {
							if (rsp.success) { //결제 성고했을때의 로직
								var msg = '결제가 완료되었습니다.\n';
								msg += '고유ID : ' + rsp.imp_uid+"\n";
								msg += '상점 거래ID : ' + rsp.merchant_uid+"\n";
								msg += '결제 금액 : ' + rsp.paid_amount+"\n";
								msg += '주문명 : ' + rsp.name+"\n";
								msg += '결제상태 : ' + rsp.status+"\n";
								msg += '카드 승인번호 : ' + rsp.apply_num+"\n"; // 승인번호 (신용카드결제 한에서)
								msg += '주문자이름 : ' + rsp.buyer_name+"\n";
								msg += '주문자email : ' + rsp.buyer_email+"\n";
								msg += '주문자 연락처 : ' + rsp.buyer_tel+"\n";
								msg += '결제승인시각  : ' + rsp.paid_at+"\n";
								msg += '거래 매출전표 : ' + rsp.receipt_url+"\n";
				
								// 적극채용공고 상품코드 (1)
								var productCode = pcode;
								// 상세 결제 이력 (구분 [요청:1])
								var category1 = 1;
								var category2 = 2; 
							
								var result = { 
										orderName:rsp.name, //주문명
										buyerName:rsp.buyer_name, //주문자 이름
										paidAmount:rsp.paid_amount, // 가격
										productCode:productCode, //상품코드 (적극채용공고 :1)
										category1:category1, // 카테고리 ( 요청 )
										category2:category2,  // 카테고리 ( 승인 )
										num : num // 갯수
								};
								
								console.log(result);	
								
								$.ajax({
									url:"<%=request.getContextPath()%>/paymentPerson",
									data :result,
									type: "get",
									success:function(data){
										if(data === "success"){
											//alert("데이터베이스 insert 성공");
											swal ({
											   title : "결제가완료되었습니다.",
											   icon : "success"
											}).then((value) => {
												location.href = "<%=request.getContextPath()%>/allPaymentList";
											  // location.href=path; 
											});
										}else{
											//alert("데이터베이스 insert 실패");
											location.href = "<%=request.getContextPath()%>/views/company/premium2.jsp";
										}
									},
									error:function(data){
										console.log("실패!");
									}
								});
							} else { //결제 실패시 로직
								var msg = '결제에 실패하였습니다.';
								msg += '에러내용 : ' + rsp.error_msg;
								alert(msg);
							}
							//alert(msg);
							
						});
						<%}else{%>
							swal ({
								  title : "인재열람 건수가 " + <%=result%>+ "건 남았습니다. \n 모두 사용 후 추가 결제가 가능합니다.",
							 	  icon : "error"
							}).then((value) => {
								location.href ="<%=request.getContextPath()%>/selectViewCount";
							});
						<%}%>
						<%}else{%>
							swal ({
							   title : "로그인이 필요합니다.",
							   icon : "error"
							}).then((value) => {
								location.href = "<%=request.getContextPath()%>/loginBannerImg.me?type="+2;
							});
							//로그인창으로 이동하게 나중에 바꿔야됨!!
						<%}%>
					}
		</script>

	<!-- footer -->
	<%@ include file="../common/ourfooter.jsp"%>
</body>
</html>