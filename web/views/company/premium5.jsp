<%@page import="company.model.vo.UsingPersonList"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	ArrayList<UsingPersonList> list = (ArrayList<UsingPersonList>) request.getAttribute("list");
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>이용 내역</title>
<%@ include file="/views/common/import.html"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
.premiumlist {
	width: 65%; 
	margin-left: auto;
	margin-right: auto;
}
#p {
	font-family: 'Gothic A1', sans-serif;
	text-align: center;
	font-size: 3em;
	margin-bottom: 30px;
}
.tablelist {
	margin-left: auto;
	margin-right: auto;
	width: 65%;
}
.modalOuter{
		margin-left:auto;
		margin-right:auto;
		text-align:center;
	}
</style>
</head>
<body>
	<%@ include file="../common/companyMenu.jsp"%>
	<div style="height: 50px;"></div>
	<p id="p">이용내역</p>
	<div class="premiumlist">
		<div class="ui three item menu">
			<a class="item" id="frist">인재열람</a> 
			<a class="item" id="second" onclick="adverUsing()">광고배너</a>
			<a class="item" id="third" onclick="pop_rec()">적극채용공고</a>
		</div>
	</div>
	<div class="tablelist" id="firsttable" >
		<table class="ui sortable celled table" id="listPerson">
			<thead>
				<tr align="center">
					<th>번호</th>
					<th>날짜</th>
					<th>열람이력서제목</th>
					<th>남은갯수</th>
				</tr>
			</thead>
			<tbody align="center">
				<%for(int i=0; i < list.size(); i++){ %>
				<tr style="text-align: center;">
					<td><%=i+1%></td>
					<td><%=list.get(i).getOpenDate() %></td>
					<td class="projectName">
						<%=list.get(i).getProjectTitle()%>
						<input type ="hidden"  id ="hid" value ="<%=list.get(i).getPuno()%>">
					</td>
					<td>
						<%if(i == 0) {%>
							<%=list.get(i).getCount()%>
						<%}else{ %>
						
						<%} %>
					</td>
				</tr>
				<%} %>
			</tbody>
		</table>
		<script>
			$(".projectName").hover(function(){
				$(this).css("background","lightgray");
				$(this).css("text-decoration","underline");
			}, function(){
				$(this).css("background","white");
				$(this).css("text-decoration","none");
			});
			$(".projectName").hover(function(){
				$(this).css("cursor","pointer");
			});
			$(".projectName").click(function(){
				var num = $('#hid').val();
				location.href = "<%=request.getContextPath()%>/talentSearch.cr?otherUno="+num;
			});
		</script>
	</div>
	<div class="tablelist" id="secondtable" style="display: none">
		<table class="ui sortable celled table" id="listAdver">
			<thead>
				<tr align="center">
					<th>번호</th>
					<th>시작날짜</th>
					<th>종료날짜</th>
					<th>등록파일</th>
					<th>구분</th>
				</tr>
			</thead>
			<tbody align="center">
				
			</tbody>
		</table>
	</div>
	<div class="tablelist" id="thirdtable" style="display: none">
		<table class="ui sortable celled table" id="listRec">
			<thead>
				<tr>
					<th>번호</th>
					<th>시작날짜</th>
					<th>종료날짜</th>
					<th>이용 공고</th>
					<th>구분</th>
				</tr>	
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>

	<div class="ui basic modal" id="introModal">
			<div class="modalOuter">
				<div class="buttonArea">
					<div id="ibtn" onclick="$('#introModal').modal('hide')" style="float:right;">X</div>
				</div>
				<div class="bigPhotoArea">
					<img id="modalImg" style="width:300px;" src="">
				</div>
			</div>
	</div> <!-- 사진 띄우는 모달 끝 -->	
	<div style="height:50px"></div>
	
	<%@ include file="../common/ourfooter.jsp"%>
	<script>
		
		$(function(){
			$("#frist").click(function(){
				$("#firsttable").toggle();
				$("#secondtable").hide();
				$("#thirdtable").hide();
			})
			$("#second").click(function(){
				$("#firsttable").hide();
				$("#secondtable").toggle();
				$("#thirdtable").hide();
			})
			$("#third").click(function(){
				$("#firsttable").hide();
				$("#secondtable").hide();
				$("#thirdtable").toggle();
			})
		});
		function adverUsing(){
			var result = {
				uno : <%=loginUser.getUno()%>
			}
			$.ajax({
				url :"usingAdver",
				data : result,
				type: "POST",
				dataType : "json",
				success:function(data){
					$tableBody = $("#listAdver tbody");
					$tableBody.html('');
					$.each(data , function(index , value){
						var payid = value.AdPayId;
						console.log(payid);
						
						var $input = $("<input type ='hidden'>").val(value.AdPayId);
						
						var $tr = $("<tr style = 'text-align:center;'>");
						var $no = $("<td>").text(index+1);
						var $AdStartDate = $("<td>").text(value.AdverstartDate);
						var $AdendDate = $("<td>").text(value.AdendDate);
						var $AdFileName = $("<td class='fileName' name='payid'> ").text(decodeURIComponent(value.AdFileName)).append($input);
						var $AdCategory = $("<td>").text(decodeURIComponent(value.AdCategory));
						
						
						$tr.append($no);
						$tr.append($AdStartDate);
						$tr.append($AdendDate);
						$tr.append($AdFileName);
						$tr.append($AdCategory);
						
						$tableBody.append($tr);
					})
					$(".fileName").hover(function(){
						$(this).css("background","lightgray");
						$(this).css("text-decoration","underline");
					}, function(){
						$(this).css("background","white");
						$(this).css("text-decoration","none");
					});
					$(".fileName").hover(function(){
						$(this).css("cursor","pointer");
					});
					
					//fileName 에 따른 사진 보여주기 function
					$(".fileName").click(function(){
						var payid  = $(this).find("input[type=hidden]").val();
						var result ={
							payid : payid
						}
						$.ajax({
							url: "pictureShow",
							data : result,
							type: "POST",
							success:function(data){
								$("#modalImg").attr("src", data);
								$("#introModal").modal('show');
							}
						})
					});
				},
				error:function(request,status,error){
			        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			    }
			})
		}
		
		function pop_rec(){
			var result = {
				uno : <%=loginUser.getUno()%>
			}
			$.ajax({
				url :"usingPop",
				data : result,
				type: "POST",
				dataType : "json",
				success:function(data){
					$tableBody = $("#listRec tbody");
					$tableBody.html('');
					$.each(data,function(index , value){
						
						var str = replaceAll(decodeURIComponent(value.PopRecruitName),"+" ," ");
						var $tr = $("<tr stlye='text-align:center'>");
						var $no = $("<td>").text(index+1);
						var $PopRecruitStartDate = $("<td>").text(value.PopRecruitstartDate);
						var $PopRecruitendDate = $("<td>").text(value.PopRecruitendDate);
						var $PopRecruitTitle = $("<td>").text(str);
						var $PopRecruitCategory = $("<td>").text(decodeURIComponent(value.PopRecruitCategory));
						
						$tr.append($no);
						$tr.append($PopRecruitStartDate);
						$tr.append($PopRecruitendDate);
						$tr.append($PopRecruitTitle);
						$tr.append($PopRecruitCategory);
						
						$tableBody.append($tr);
						
						
					})
				},
				error:function(request,status,error){
			        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			    }
			})
				
		}
		
		
		function replaceAll(sValue, param1, param2) {
			// replaceAll 이라는 변수 선언.
			return sValue.split(param1).join(param2);
		}
	</script>
</body>
</html>