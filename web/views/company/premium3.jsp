<%@page import="company.model.vo.UsingAdverList"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%
		ArrayList<UsingAdverList> list = (ArrayList<UsingAdverList>) request.getAttribute("list");
	%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="https://fonts.googleapis.com/css?family=Nunito&display=swap"
	rel="stylesheet">
<title>배너광고</title>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <!--DatePicker  --> 
 
<!-- Import 결제 -->
<script type="text/javascript"src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript"src="https://cdn.iamport.kr/js/iamport.payment-1.1.5.js"></script>
<%@ include file="/views/common/import.html"%>
<style>
#maintitle {
	text-align: center;
	width: 65%;
	margin-left: auto;
	margin-right: auto;
	font-size: 3em;
	font-weight: bold;
	margin-top: 30px;
	margin-bottom: 55px;
	font-family: 'Nunito', sans-serif;
}

.adver {
	width: 65%;
	margin-left: auto;
	margin-right: auto;
	height: 400px;
	padding-left: 20px;
	padding-top: 20px;
	overflow: scroll;
}

.adver2 {
	width: 65%;
	margin-left: auto;
	margin-right: auto;
	height: 345px;
	padding-left: 20px;
	padding-top: 10px;
	overflow: scroll;
}

#adver22 {
	height: 300px;
}

.adver3 {
	width: 65%;
	margin-left: auto;
	margin-right: auto;
	height: 345px;
	padding-left: 20px;
	padding-top: 10px;
}

.adver4 {
	width: 65%;
	margin-left: auto;
	margin-right: auto;
	height: 200px;
}

.adver::-webkit-scrollbar {
	display: none;
}

.adver2::-webkit-scrollbar {
	display: none;
}

.type {
	margin-left: 30px;
	display: inline-block;
	width: 400px;
	height: 280px;
	overflow: hidden;
}

#typetext {
	padding-top: 70px;
	margin-left: 50px;
}

#typetext2 {
	padding-top: 60px;
	margin-left: 50px;
}

.type img {
	width: 370px;
	height: 280px;
	padding-top: 30px;
}

#advertitle {
	display: inline-block;
}

#tabletext {
	text-align: center;
	width: 220px;
	height: 150px;
}

#tabletexttitle {
	font-size: 1.5em;
	font-weight: bold;
}

#tabletextexplain {
	padding-top: 10px;
	font-size: x-small;
}

#button {
	width: 300px;
	height: 100px;
	font-size: 2.5em;
	margin-left: 70%;
}
	#resultOne{
	width : 40%;
	heigth: 50px;
	border: 1px solid lightgray;
	text-align: center;
	font-size:1.3em;
	border-radius: 5px;
}
	#resultTwo{
	width : 40%;
	heigth: 50px;
	border: 1px solid lightgray;
	text-align: center;
	font-size:1.3em;
	border-radius: 5px;
}
.dateResult {
	display: inline-block;
}

.explain {
	margin-left: 30px;
	margin-top: 20px;
}
.file_input label {
    position:relative;
    cursor:pointer;
    display:inline-block;
    vertical-align:middle;
    overflow:hidden;
    width:100px;
    height:30px;
    background:#777;
    color:#fff;
    text-align:center;
    line-height:30px;
    border-radius : 3px;
}
.file_input label input {
	border-radius : 10px;
    position:absolute;
    width:0;
    height:0;
    overflow:hidden;
}
.file_input input[type=text] {
    vertical-align:middle;
    display:inline-block;
    width:65%;
    height:28px;
    line-height:28px;
    font-size:11px;
    padding:0;
    border:0;
    border:1px solid #777;
}
.dateResult {
	display: inline-block;
}
.calendar{
	display: inline-block;
}

@media screen and (max-width:768px) {
	.adver {
		width: 65%;
		margin-left: auto;
		margin-right: auto;
		height: 400px;
		padding-left: 20px;
		padding-top: 20px;
		overflow: scroll;
	}
	.adver2 {
		width: 65%;
		margin-left: auto;
		margin-right: auto;
		height: 345px;
		padding-left: 20px;
		padding-top: 10px;
		overflow: scroll;
	}
	#adver22 {
		height: 300px;
	}
	.adver3 {
		width: 65%;
		margin-left: auto;
		margin-right: auto;
		height: 345px;
		padding-left: 20px;
		padding-top: 10px;
	}
	.adver::-webkit-scrollbar {
		display: none;
	}
	.adver2::-webkit-scrollbar {
		display: none;
	}
	.type {
		margin-left: 30px;
		display: inline-block;
		width: 400px;
		height: 280px;
		overflow: hidden;
	}
	#typetext {
		padding-top: 70px;
		margin-left: 50px;
	}
	#typetext2 {
		padding-top: 60px;
		margin-left: 50px;
	}
	.type img {
		width: 370px;
		height: 280px;
		padding-top: 30px;
	}
	#advertitle {
		display: inline-block;
	}
	#tabletext {
		text-align: center;
		width: 220px;
		height: 150px;
	}
	#button {
		width: 100px;
		height: 40px;
		font-size: small;
		margin-left: 80%;
	}
	#tabletexttitle {
		font-size: medium;
		font-weight: bold;
	}
	#tabletextexplain {
		font-size: xx-small;
	}

}
</style>
</head>
<body>
	<%@ include file="../common/companyMenu.jsp"%>
	<div style="height: 60px;"></div>
	<div id="maintitle">배너광고안내</div>
	<div class="adver">
		<div>
			<div id="advertitle" style="color: blue; font-size: 2.5em">|</div>
			<div id="advertitle" style="font-size: 1.5em; font-weight: bold;">포트폴리오
				페이지</div>
		</div>
		<br>
		<div class="type">
			<img style="height: 250px" alt="" src="<%=request.getContextPath()%>/images/adver1.png">
		</div>
		<div class="type" id="typetext">
			<div style="font-weight: bold; font-size: 2em;">포트폴리오 배너</div>
			<br>
			<hr>
			<br>
			<div>
				- 가격&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<div style="color: red; font-size: 1.5em; display: inline-block;">550,000원</div>
				<div style="color:red; font-size:0.8em; display:inline-block;">(주)</div>
			</div>
			<br>
			<div>- 사이즈&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1200px X 500px</div>
		</div>
	</div>

	<div class="adver2">
		<div>
			<div id="advertitle" style="color: blue; font-size: 2.5em">|</div>
			<div id="advertitle" style="font-size: 1.5em; font-weight: bold;">로그인
				페이지</div>
		</div>
		<br>
		<div class="type">
			<img alt="" src="<%=request.getContextPath()%>/images/adver2.png">
		</div>
		<div class="type" id="typetext2">
			<div style="font-weight: bold; font-size: 2em;">로그인 좌측 배너</div>
			<br>
			<hr>
			<br>
			<div>
				- 가격&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<div style="color: red; font-size: 1.5em; display: inline-block;">550,000원</div>
				<div style="color:red; font-size:0.8em; display:inline-block;">(주)</div>
			</div>
			<br>
			<div>- 사이즈&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1200px X 500px</div>
		</div>
	</div>

	<div class="adver2" id="adver22">
		<div class="type">
			<img alt="" src="<%=request.getContextPath()%>/images/adver3.png">
		</div>
		<div class="type" id="typetext2">
			<div style="font-weight: bold; font-size: 2em;">로그인 우측 배너</div>
			<br>
			<hr>
			<br>
			<div>
				- 가격&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<div style="color: red; font-size: 1.5em; display: inline-block;">550,000원</div>
				<div style="color:red; font-size:0.8em; display:inline-block;">(주)</div>
			</div>
			<br>
			<div>- 사이즈&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1200px X 500px</div>
		</div>
	</div>
	<div class="adver3">
		<div class="util">
			<p style="font-size: 2em; font-weight: bold;">이용방법</p>
		</div>
		<hr>
		<div class="utilexp">
			<table class="utiltable">
				<tr>
					<td id="tabletext">
						<div id="tabletexttitle">날짜 선택</div>
						<div id="tabletextexplain">원하는 날짜와 기간을 선택</div>
					</td>
					<td id="tableimg"><img alt="" src="<%=request.getContextPath()%>/images/select.png">
					</td>
					<td id="tabletext">
						<div id="tabletexttitle">파일 첨부</div>
						<div id="tabletextexplain">
							사용할 배너 이미지를 <br>양식에 맞게 첨부
						</div>
					</td>
					<td id="tableimg"><img alt="" src="<%=request.getContextPath()%>/images/select.png">
					</td>
					<td id="tabletext">
						<div id="tabletexttitle">결제</div>
						<div id="tabletextexplain">기간에 맞게 대금 결제</div>
					</td>
					<td id="tableimg"><img alt="" src="<%=request.getContextPath()%>/images/select.png">
					</td>
					<td id="tabletext">
						<div id="tabletexttitle">심사</div>
						<div id="tabletextexplain">첨부된 배너의 관리자 심사</div>
					</td>
					<td id="tableimg"><img alt="" src="<%=request.getContextPath()%>/images/select.png">
					</td>
					<td id="tabletext">
						<div id="tabletexttitle">광고 게제</div>
						<div id="tabletextexplain">선택된 날짜에 광고 게제</div>
					</td>
				</tr>
			</table>

			<button class="ui red button" id="button" onclick="payment();">결제하기</button>
			
			<!-- 결제모달 -->
			<div class="ui modal tiny">
				<i class="close icon"></i>
				<div class="header">옵션 선택</div>
				<div class="image content">
					<div class="description" style="width: 90%;text-align: center;">
						<div class="ui header">
							<select class="ui dropdown" id="recruitSelect" style="width: 85%; height: 50px;" onchange="chageSelect()">
								<option value =7>포트폴리오 배너</option>
								<option value =8>로그인 좌측 배너</option>
								<option value =9>로그인 우측 배너</option>
							</select>
						</div>
						<br>
						<div class="ui calendar" id="disable1">
							<div class="ui input left icon">
								<i class="calendar icon"></i><input type="text"
									placeholder="Date" class="checkStartDate">
							</div>
						</div>
						~
						<div class="ui calendar" id="disable2">
							<div class="ui input left icon">
								<i class="calendar icon"></i> <input type="text"
									placeholder="Date" class="checkendDate">
							</div>
						</div>
						<div style="height:30px; "></div>
						<!-- 파일 업로드 form -->
						<form action="upload" id="uploadForm" method="post"
							enctype="multipart/form-data">
							<div class="file_input">
								<label> File Attach <input id="fileData" type="file" onchange="javascript:document.getElementById('file_route').value=this.value">
								</label> <input type="text" readonly="readonly" title="File Route"
									id="file_route">
							</div>
						</form>
						<div style="height:20px;"></div>
					</div>
				</div>
				<div class="actions">
					<div class="ui black deny button">취소</div>
					<div class="ui positive right labeled icon button" onclick ="import1()">결제하기<i class="checkmark icon"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="adver4">
		<br>
		<hr>
		<br>
		<div class="explain" style="font-size: small;">
			<strong>[배너광고 상품 이용안내]</strong> <br>
			<ul>
				<li>첨부된 배너가 부적절한 배너일 경우 광고 심의에 의해 거절 조치되고 결제 금액은 환불됩니다.</li>
				<li>부적절한 광고배너를 지속적으로 신청할 경우 해당 회원에 대해서 불이익 조치가 진행됩니다.</li>
				<li>광고배너는 예약제로 기간연장은 불가능합니다.</li>
			</ul>
		</div>
	</div>
	<script>
	/* -------------------------------------------------날짜 막기 function start ------------------------------------ */
	   var listDate = [];
		/* 받은 startDate , endDate 를 막는다. */
		 <%for(int i=0; i<list.size(); i++){%>
		 	getDateRange("<%=list.get(i).getAdstartDate()%>", "<%=list.get(i).getAdendDate()%>" , listDate)
		 <%}%>
		 /* 날짜 막는 함수 */
		function getDateRange(startDate, endDate, listDate) {
	        var dateMove = new Date(startDate);
	        var strDate = startDate;

	        if (startDate == endDate){
	            var strDate = dateMove.toISOString().slice(0,10);
	            listDate.push(strDate);
	        }
	        else{
				while (strDate < endDate){
	                var strDate = dateMove.toISOString().slice(0, 10);
	                listDate.push(strDate);
	                dateMove.setDate(dateMove.getDate() + 1);
	            }
	        }
	        return listDate;
	    };
 /* -------------------------------------------------날짜 막기 function end ------------------------------------ */
	
		/* 결제버튼 클릭 -> 모달창 on */
		function payment() {
			<%if(loginUser == null){%>
			swal ({
				   title : "로그인이 필요합니다.",
				   icon : "error"
				}).then((value) => {
					location.href = "<%=request.getContextPath()%>/loginBannerImg.me?type="+2;
				});
		<%}else{%>
			$('.ui.modal').modal({
				onShow : function(value){
					var today = new Date();
					$('#disable1').calendar({
						  type : 'date',
						  minDays : 7,
						  maxDays : 7,
						  text:{
							  days :['월','화','수','목','금','토','일'],
							  months:['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
						  },
						  minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
						
						  formatter : {
								date : function(date, settings) {
									if (!date)
										return '';
									var day = date.getDate();
									var month = date.getMonth() + 1;
									var year = date.getFullYear();
									return year + '-' + month + '-' + day;
								}
							},
					      initialDate: new Date(),
					      disabledDates: listDate  ,
						  endCalendar : $('#disable2'),
						  
					  });
						$('#disable2').calendar({
							  type : 'date',
							  minDays : 7,
							  maxDays : 7,
							  text:{
								  days :['월','화','수','목','금','토','일'],
								  months:['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
							  },
							  formatter : {
									date : function(date, settings) {
										if (!date)
											return '';
										var day = date.getDate();
										var month = date.getMonth() + 1;
										var year = date.getFullYear();
										return year + '-' + month + '-' + day;
									}
								},
						      initialDate : new Date(),
						      disabledDates: listDate,
						      startCalendar : $('#disable1')
						  });
				}
			}).modal('show');
		<%}%>
		}
		function chageSelect(){
			var selectoption = $("#recruitSelect option:selected").val();
			
			if(selectoption == 7){
				//console.log("7번이네!!")
				var firstlist = [];
				var result = {
						pcode : selectoption
					};
				$.ajax({
					url:"<%=request.getContextPath()%>/ChangeBanner",
					data : result,
					type:"POST",
					success:function(data){
						//console.log(data);
						$.each(data , function(index , value){
							getDateRange(value.startDate , value.endDate , firstlist);
							console.log(firstlist);
						});
						var today = new Date();
						$('#disable1').calendar({
							  type : 'date',
							  minDays : 7,
							  maxDays : 7,
							  text:{
								  days :['월','화','수','목','금','토','일'],
								  months:['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
							  },
							  minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
							
							  formatter : {
									date : function(date, settings) {
										if (!date)
											return '';
										var day = date.getDate();
										var month = date.getMonth() + 1;
										var year = date.getFullYear();
										return year + '-' + month + '-' + day;
									}
								},
						      initialDate: new Date(),
						      disabledDates: firstlist  ,
							  endCalendar : $('#disable2'),
							  
						  });
						$('#disable2').calendar({
							  type : 'date',
							  minDays : 7,
							  maxDays : 7,
							  text:{
								  days :['월','화','수','목','금','토','일'],
								  months:['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
							  },
							  formatter : {
									date : function(date, settings) {
										if (!date)
											return '';
										var day = date.getDate();
										var month = date.getMonth() + 1;
										var year = date.getFullYear();
										return year + '-' + month + '-' + day;
									}
								},
						    // initialDate: new Date(new Date ($("#startDate").val()).getDate() + 7)  ,
						      initialDate : new Date(),
						      disabledDates: firstlist,
						      startCalendar : $('#disable1')
						  });
					}
					
				})
				
			}else if(selectoption == 8){
				//console.log("8번이네!")	;
				var secondlist = [];
				var result = {
						pcode : selectoption
					};
				console.log(selectoption);
				$.ajax({
					url:"<%=request.getContextPath()%>/ChangeBanner",
					data : result,
					type:"POST",
					success:function(data){
						$.each(data , function(index , value){
							getDateRange(value.startDate , value.endDate , secondlist);
							console.log(secondlist);
						});
						var today = new Date();
						$('#disable1').calendar({
							  type : 'date',
							  minDays : 7,
							  maxDays : 7,
							  text:{
								  days :['화','화','수','목','금','토','일'],
								  months:['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
							  },
							  minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
							
							  formatter : {
									date : function(date, settings) {
										if (!date)
											return '';
										var day = date.getDate();
										var month = date.getMonth() + 1;
										var year = date.getFullYear();
										return year + '-' + month + '-' + day;
									}
								},
						      initialDate: new Date(),
						      disabledDates: secondlist  ,
							  endCalendar : $('#disable2'),
						  });
						$('#disable2').calendar({
							  type : 'date',
							  minDays : 7,
							  maxDays : 7,
							  text:{
								  days :['화','화','화','목','금','토','일'],
								  months:['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
							  },
							  formatter : {
									date : function(date, settings) {
										if (!date)
											return '';
										var day = date.getDate();
										var month = date.getMonth() + 1;
										var year = date.getFullYear();
										return year + '-' + month + '-' + day;
									}
								},
						    // initialDate: new Date(new Date ($("#startDate").val()).getDate() + 7)  ,
						      initialDate : new Date(),
						      disabledDates: secondlist,
						      startCalendar : $('#disable1')
						  });
					}
				});
			}else{
				//console.log("9번이네!")	;
				var thirdlist = [];
				var result = {
						pcode : selectoption
					};
				console.log(selectoption);
				$.ajax({
					url:"<%=request.getContextPath()%>/ChangeBanner",
					data : result,
					type:"POST",
					success:function(data){
						$.each(data ,function (index, value){
							getDateRange(value.startDate , value.endDate , thirdlist);
							console.log(thirdlist);
						});
						var today = new Date();
						$('#disable1').calendar({
							  type : 'date',
							  minDays : 7,
							  maxDays : 7,
							  text:{
								  days :['화','화','수','목','금','토','일'],
								  months:['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
							  },
							  minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
							
							  formatter : {
									date : function(date, settings) {
										if (!date)
											return '';
										var day = date.getDate();
										var month = date.getMonth() + 1;
										var year = date.getFullYear();
										return year + '-' + month + '-' + day;
									}
								},
						      initialDate: new Date(),
						      disabledDates: thirdlist  ,
							  endCalendar : $('#disable2'),
						  });
						$('#disable2').calendar({
							  type : 'date',
							  minDays : 7,
							  maxDays : 7,
							  text:{
								  days :['화','화','화','목','금','토','일'],
								  months:['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
							  },
							  formatter : {
									date : function(date, settings) {
										if (!date)
											return '';
										var day = date.getDate();
										var month = date.getMonth() + 1;
										var year = date.getFullYear();
										return year + '-' + month + '-' + day;
									}
								},
						    // initialDate: new Date(new Date ($("#startDate").val()).getDate() + 7)  ,
						      initialDate : new Date(),
						      disabledDates: thirdlist,
						      startCalendar : $('#disable1')
						  });
					}
				});
			}
		}
		
		/* -------------------------------------------------아임포트 function start ------------------------------------ */
		function import1(){ 
			/* 두 날짜를 뺀 날짜가 일주일이내이면 -> 나중에 수정 */
			/* var sss = $(".checkStartDate").val();
			var ddd = $(".checkendDate").val();
			console.log("sss : " + sss);
			console.log("ddd : " + ddd);
			console.log("빼기"+ ddd - sss); */
			
			var AdverName = "";
			var code = 0;
			//선택한 값의 value가져오기
			var select = $("#recruitSelect option:selected").val();
				
			if(select==7){
				AdverName= "포트폴리오";
				code = 7;
			}else if(select==8){
				AdverName="로그인 좌측";
				code=8;
			}else{
				AdverName="로그인 우측";
				code=9;
			}
			var form = $('#uploadForm')[0];
			//form-data형식 가져오기
			var formData = new FormData(form);
			//filedata 입력
			formData.append('file' , $('#fileData')[0].files[0]);
			//광고 배너 위치 이름
			formData.append('AdverName' , AdverName);
			//PCODE (상품코드)
			formData.append('pcode' , code);
			
			
			//imgfile 파일만 등록하기 
			var fileNm = $("#fileData").val();
			console.log(fileNm);
			if (fileNm != "") {
			    var ext = fileNm.slice(fileNm.lastIndexOf(".") + 1).toLowerCase();
			    if (!(ext == "gif" || ext == "jpg" || ext == "png")) {
			        alert("이미지파일 (.jpg, .png, .gif ) 만 업로드 가능합니다.");
			        return false;
			    }
			}/* end imgfile*/
				
			var IMP = window.IMP;
			IMP.init('imp79349410');
			<%if(loginUser != null){%>
		
				IMP.request_pay({
					pg : 'inicis', // version 1.1.0부터 지원.
					pay_method : 'card',
					merchant_uid : 'merchant_' + new Date().getTime(),
					name : AdverName +'광고배너 결제',
					amount : '100', //판매 가격
					buyer_email : 'itduck1004@gmail.com',
					buyer_name : '<%=loginUser.getUserName()%>' ,
					buyer_tel : '<%=loginUser.getPhone()%>'
				}, function(rsp) {
					if (rsp.success) {
						var msg = '결제가 완료되었습니다.\n';
						msg += '고유ID : ' + rsp.imp_uid+"\n";
						msg += '상점 거래ID : ' + rsp.merchant_uid+"\n";
						msg += '결제 금액 : ' + rsp.paid_amount+"\n";
						msg += '주문명 : ' + rsp.name+"\n";
						msg += '결제상태 : ' + rsp.status+"\n";
						msg += '카드 승인번호 : ' + rsp.apply_num+"\n"; // 승인번호 (신용카드결제 한에서)
						msg += '주문자이름 : ' + rsp.buyer_name+"\n";
						msg += '주문자email : ' + rsp.buyer_email+"\n";
						msg += '주문자 연락처 : ' + rsp.buyer_tel+"\n";
						msg += '결제승인시각  : ' + rsp.paid_at+"\n";
						msg += '거래 매출전표 : ' + rsp.receipt_url+"\n";
		
						//선택한 시작 , 종료 날짜 
						var startDate = $(".checkStartDate").val();
						var endDate = $(".checkendDate").val();
						
						// 상세 결제 이력 (구분 [요청:1])
						var category1 = 1;
						
						
						//선택한 시작 , 종료 날짜 
						formData.append('startDate' , startDate);
						formData.append('endDate' , endDate);
						// 상세 결제 이력 (구분 [요청:1])
						formData.append('category1' , category1);
						formData.append('paidAmount' , rsp.paid_amount);
						
						
						$.ajax({
							url:"<%=request.getContextPath()%>/paymentAdverServlet",
							enctype: 'multipart/form-data',
							data : formData,
							processData:false,
							contentType:false,
							type: "POST",
							success:function(data){
								if(data === "success"){
									//alert("데이터베이스 insert 성공");
									swal ({
									   title : "결제가완료되었습니다.",
									   icon : "success"
									}).then((value) => {
										location.href = "<%=request.getContextPath()%>/allPaymentList";
									  // location.href=path; 
									});
								}else{
									//alert("데이터베이스 insert 실패");
									location.href = "<%=request.getContextPath()%>/views/company/premium3.jsp";
								}
							},
							error:function(data){
								console.log("실패!");
							}
						});
					} else { //결제 실패시 로직-
						var msg = '결제에 실패하였습니다.';
						msg += '에러내용 : ' + rsp.error_msg;
						alert(msg);
					}
					//alert(msg);
					
				});
			<%}else{%>
				alert("로그인이 필요합니다.");
				//로그인창으로 이동하게 나중에 바꿔야됨!!
				location.href = "<%=request.getContextPath()%>/views/common/companyLogin.jsp";
			<%}%>
		}
		/* -------------------------------------------------아임포트 function end ------------------------------------ */
	</script>
	<%@ include file="../common/ourfooter.jsp"%>
</body>
</html>