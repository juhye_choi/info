<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%
    Member m = (Member)request.getAttribute("m");
    
    %>
    
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<%@ include file="/views/common/import.html" %>

<style>
body{
		text-align:center;
		padding-top:5%;
		margin:auto;
	}
	
	.memberChange{
		width:15%;
		height:40px;
		margin:3px;
		background:white;
		border:2px solid black;
		font-size:17px;
	}
	.memberChange:hover{
	cursor:pointer;
}
	
	#idCheck {
		background:#CC33FF;
		color:white;
	}
	
		#allDiv {
	margin-left: 13%;
	margin-right: 8%;
	margin-top: 25px;
}

		#div {
	margin-left: 10%;
	margin-right: 8%;
	margin-top: 50px;
}

		#div2 {
	margin-left: 2%;
	margin-right: 8%;
	margin-top: 20px;
}




td {
	vertical-align: top;
	padding-top: 10px;
}



</style>
</head>
<body>

<%@ include file="../common/companyMenu.jsp" %>



<h1 style="margin-right: 55%;">기업회원 비밀번호 찾기</h1>





<div id="allDiv">

<hr>

<h4 style="text-align:left;">회원정보의 비밀번호를 재설정해주세요.</h4>
<br>
<form id="passSearchForm" action="<%=request.getContextPath()%>/CpassSearch2.me" method="post">
<table align="center" class="ui definition table" bolder="1"  style="width:80%; margin:auto;">

 
  
  <tbody>
    <tr>
      <td style="text-align:center">비밀번호</td>
      <td><div class="ui input"><input type="password" id="userPwd" name="userPwd" style="width:300px;"></div></td>
     <td> <td><input type="hidden" id="type" name="type" value="1"></td></td>
    </tr>
    
    <tr>
      <td style="text-align:center">비밀번호 확인</td>
      <td><div class="ui input"><input type="password" id="userPwd2" name="userPwd2" style="width:300px;"></div></td>
      <td><input type="hidden" name="uno" value="<%= m.getUno() %>"></td>
     
    </tr>
    </tbody>



</table>

</form>

<div id="div2">

<input type="button"  class="ui secondary button" value="확인" id="passSearch">





</div>


</div>



<script>



$(function(){
	
	$("#userPwd2").focusout(function() {

		var pwd1 = $("#userPwd").val();
		var pwd2 = $("#userPwd2").val();

		if (pwd1 != "" || pwd2 != "") {

			if (pwd1 == pwd2) {
				$("#passSearch").click(function(){
					
					$("#passSearchForm").submit();
					
				})

			} else {

				alert("비밀번호가 동일한지 확인해주세요.");

			}

		}

	})

	
})






</script>











</body>
</html>