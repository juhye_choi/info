<%@page import="admin.model.vo.NowPopList"%>
<%@page import="company.model.vo.Recruit"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	ArrayList<Recruit> list = (ArrayList<Recruit>) request.getAttribute("list");
	ArrayList<NowPopList> nowlist = (ArrayList<NowPopList>) request.getAttribute("nowPoplist");
%>
<!DOCTYPE html> 
<html>
<head>
<meta charset="UTF-8">
<title>프리미엄</title>
<link href="https://fonts.googleapis.com/css?family=Gothic+A1|Indie+Flower&display=swap" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- Import 결제 -->
<script type="text/javascript"src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript"src="https://cdn.iamport.kr/js/iamport.payment-1.1.5.js"></script>
<%@ include file="/views/common/import.html" %>
</head>
<style>
#premium1 {
	border: 1px solid black;
	width: 60%;
	margin-left: auto;
	margin-right: auto;
	display: block;
	padding-top: 20px;
	padding-left: 20px;
	padding-right: 20px;
}

.first {
	display: inline-block;
}

.money {
	display: inline-block;
	margin-top: 40px;
}

#premium1price {
	float: right;
	color: red;
	word-break: break-all;
}

.second {
	width: 200px;
	height: 330px;
	display: inline-block;
	margin-left: 140px;
	margin-right: 10px;
	overflow: hidden;
}

#explanation {
	padding: 10px;
	padding-top: 60px;
	word-break: break-all;
}

.info {
	width: 380px;
	height: 230px;
	display: inline-block;
	margin-left: 40px;
	margin-right: 10px;
	overflow: hidden;
}

#infotext {
	padding: 10px;
	padding-top: 60px;
	padding-right: 30px;
	word-break: break-all;
	border-right: 1px solid #A3AFBA;
}

.info img {
	width: 350px;
	height: 200px
}

#p {
	font-family: 'Gothic A1', sans-serif;
	text-align: center;
	font-size: 3em;
	margin-bottom: 30px;
}
.ui .dropdown{
	opacity: 0.5;
	font-size: 0.8em;
}

.dateResult {
	display: inline-block;
}
#resultOne{
	width : 40%;
	heigth: 50px;
	border: 1px solid lightgray;
	text-align: center;
	font-size:1.3em;
	border-radius: 5px;
}
#resultTwo{
	width : 40%;
	heigth: 50px;
	border: 1px solid lightgray;
	text-align: center;
	font-size:1.3em;
	border-radius: 5px;
}
.explain{
	margin:30px;
	
}
.calendar{
	display: inline-block;
}
</style>
<body>
	<%@include file="../common/companyMenu.jsp"%>
	<div style="height: 50px"></div>
	<p id="p">채용광고</p>
	<div id="premium1">
		<div class="first">
			<h1>적극채용광고</h1>
			<br> 
			<p style="opacity: 0.6">기업 로고와 함께 노출 되어 주목도가 높은 상품</p>
		</div>
		<div id="premium1price" class="first">
			<h1 class="money">99,000</h1>
			<h3 class="money">원/일</h3> 
			<br>
			<button class="ui red button" onclick="payment()">결제하기</button>
		</div>
		<Br>
		<br> <br>
		<hr>
		<br>
		<p style="opacity: 0.6">#눈에 띄는 공고 &nbsp;&nbsp; #상단 &nbsp;&nbsp;
			#강조효과</p>
		<br>
		<div style="border: 1px solid black; padding: 20px 10px;">
			<div class="second" id ="explanation">
				<h3>프리미엄</h3>
				<p>주목도가 높은 상단 페이지에 기업 로고와 함께 노출되어 효과가 좋은 상품입니다.</p>
				<br>
				<p>결제</p>
				<p style="opacity: 0.6">주 단위로 결제되며 결제 취소 (환불)은 대기상태일 때만 가능합니다.</p>
			</div>
			<div class="second">
				<img alt="" src="<%=request.getContextPath()%>/images/premium.png">
			</div>
		</div>
		<br>
		<hr>
		<br>
		<div>
		<h2>&nbsp;&nbsp;이용 절차</h2>
		<div class="">
			<div  class = "info" id = "infotext" >
				<h3>첫 번째</h3>			
				<br>
				<p>현재 사용 중인 기간을 제외하고 해당 달력에서 원하는 기간을 선택합니다.</p>
			</div>
			<div  class = "info" ><img alt="" src="<%=request.getContextPath()%>/images/date1.png"></div>
		</div>
		<br>
		
		<br>
		<div class="">
			<div class = "info" id = "infotext">
				<h3>두 번째</h3>
				<br>
				<p>기업에서 현재 올라온 공고 중 적극채용공고로 등록할 공고를 선택합니다.</p>
			</div>
			<div class = "info"><img alt="" src="<%=request.getContextPath()%>/images/date2.png"></div>
		</div>
		<br>
		<br>
		<div class="">
			<div class = "info" id = "infotext">
				<h3>세 번째</h3>
				<br>
				<p>선택한 기간과 적극채용공고로 등록한 공고를 확인하고 결제를 통해 <br><br>
				홍보효과가 뛰어난 적극채용 공고 카테고리를 이용해보세요!!</p>
			</div>
			<div class = "info"><img alt="" src="<%=request.getContextPath()%>/images/date3.png"></div>
		</div>
		<Br><br><br>
		<hr style="widows: 90%;">
			<div class="explain" style="font-size:small;">
			<strong>[적극채용공고 상품 이용안내]</strong>
			<br>
			<ul>
				<li>적극채용공고로 등록된 공고가 부적절하다고 판단되면 관리자에 의해 해당 공고가 삭제될 수 있습니다. (환불 불가)</li>
				<li>결제는 일주일단위로만 결제가 가능하며 일단위로는 결제가 불가능합니다.</li>
				<li>결제 후 공고 시작일로부터 일주일 전까지만 환불이 가능합니다. 공고 시작 일주일 전에는 환불이 불가능합니다.</li>
				<li>같은 회원이 여러개의 공고를 등록할 수 있습니다.</li>
				<li>적극채용공고는 일주일 단위로 결제가 가능하며 월 ~일 기간의 결제가 이루어집니다.</li>
			</ul>
			</div>	
		</div>
	</div>
	<br>
	<button class="ui red button" style="margin-left:74%;" onclick ="payment()">결제하기</button>
			<!-- 결제모달 -->
			<div class="ui modal tiny">
				<i class="close icon"></i>
				<div class="header">옵션 선택</div>
				<div class="image content">
					<div class="description" style="width: 90%; text-align: center;">
						<div class="ui header">
							<select class="ui dropdown" id="recruitSelect" style="width: 90%; height: 50px;">
								<%if(list !=null){ 
									for(Recruit r : list){ %>
									<option value ="<%=r.getRecid()%>"><%=r.getRec_title() %></option>
									<%} 
								}%>
							</select>
						</div>
						<br>
							<div class="ui calendar" id="disable1">
							<div class="ui input left icon">
								<i class="calendar icon"></i><input type="text"
									placeholder="Date" class="checkStartDate">
							</div>
						</div>
						~
						<div class="ui calendar" id="disable2">
							<div class="ui input left icon">
								<i class="calendar icon"></i> <input type="text"
									placeholder="Date" class="checkendDate">
							</div>
						</div>
				</div>
				</div>
				<div class="actions">
					<div class="ui black deny button">취소</div>
					<div class="ui positive right labeled icon button" onclick ="import1()">결제하기<i class="checkmark icon"></i>
					</div>
				</div>
			</div>
			
		
	<%@ include file="../common/ourfooter.jsp" %>	
	
	<script>
	/* -------------------------------------------------날짜 막기 function start ------------------------------------ */
	   var listDate = [];
		/* 받은 startDate , endDate 를 막는다. */
		<%if(nowlist.get(0).getCount() == 10){%>
			console.log(<%=nowlist.get(0).getCount()%>);
			getDateRange("<%=nowlist.get(0).getPstartDate()%>", "<%=nowlist.get(0).getPendDate()%>" ,listDate);
		<%}else{%>		
				
		<%}%>		
		console.log(listDate);
		 /* 날짜 막는 함수 */
		function getDateRange(startDate, endDate, listDate) {
	        var dateMove = new Date(startDate);
	        var strDate = startDate;

	        if (startDate == endDate){
	            var strDate = dateMove.toISOString().slice(0,10);
	            listDate.push(strDate);
	        }
	        else{
				while (strDate < endDate){
	                var strDate = dateMove.toISOString().slice(0, 10);
	                listDate.push(strDate);
	                dateMove.setDate(dateMove.getDate() + 1);
	            }
	        }
	        return listDate;
	    };
/* -------------------------------------------------날짜 막기 function end ------------------------------------ */
		/* 로그인이 안되어있을 시 결제 불가 */
		function payment() {
			<%if(loginUser == null){%>
				swal ({
					   title : "로그인이 필요합니다.",
					   icon : "error"
					}).then((value) => {
						location.href = "<%=request.getContextPath()%>/loginBannerImg.me?type="+2;
					});
			<%}else{%>
				$('.ui.modal').modal({
					onShow : function(value){
						var today = new Date();
						$('#disable1').calendar({
							  type : 'date',
							  minDays : 7,
							  maxDays : 7,
							  text:{
								  days :['월','화','수','목','금','토','일'],
								  months:['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
							  },
							  minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
							
							  formatter : {
									date : function(date, settings) {
										if (!date)
											return '';
										var day = date.getDate();
										var month = date.getMonth() + 1;
										var year = date.getFullYear();
										return year + '-' + month + '-' + day;
									}
								},
						      initialDate: new Date(),
						      disabledDates: listDate  ,
							  endCalendar : $('#disable2'),
							  
						  });
							$('#disable2').calendar({
								  type : 'date',
								  minDays : 7,
								  maxDays : 7,
								  text:{
									  days :['월','화','수','목','금','토','일'],
									  months:['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
								  },
								  formatter : {
										date : function(date, settings) {
											if (!date)
												return '';
											var day = date.getDate();
											var month = date.getMonth() + 1;
											var year = date.getFullYear();
											return year + '-' + month + '-' + day;
										}
									},
							      initialDate : new Date(),
							      disabledDates: listDate,
							      startCalendar : $('#disable1')
							  });
					}
				}).modal('show');
			<%}%>
		};
		
		function import1(){
			<%if(loginUser != null){%>
			
			var IMP = window.IMP;
			IMP.init('imp79349410');
			
			IMP.request_pay({
				pg : 'inicis', // version 1.1.0부터 지원.
				pay_method : 'card',
				merchant_uid : 'merchant_' + new Date().getTime(),
				name : '적극채용공고 결제',
				amount : '100', //판매 가격
				buyer_email : 'itduck1004@gmail.com',
				buyer_name : '<%=loginUser.getUserName()%>' ,
				buyer_tel : '<%=loginUser.getPhone()%>',
				app_scheme : '<%=request.getContextPath()%>/index.jsp'
				/* buyer_addr : '서울특별시 강남구 삼성동', // 주소
				buyer_postcode : '123-456', //우편번호 */
			}, function(rsp) {
				if (rsp.success) { //결제 성고했을때의 로직
					var msg = '결제가 완료되었습니다.\n';
					msg += '고유ID : ' + rsp.imp_uid+"\n";
					msg += '상점 거래ID : ' + rsp.merchant_uid+"\n";
					msg += '결제 금액 : ' + rsp.paid_amount+"\n";
					msg += '주문명 : ' + rsp.name+"\n";
					msg += '결제상태 : ' + rsp.status+"\n";
					msg += '카드 승인번호 : ' + rsp.apply_num+"\n"; // 승인번호 (신용카드결제 한에서)
					msg += '주문자이름 : ' + rsp.buyer_name+"\n";
					msg += '주문자email : ' + rsp.buyer_email+"\n";
					msg += '주문자 연락처 : ' + rsp.buyer_tel+"\n";
					msg += '결제승인시각  : ' + rsp.paid_at+"\n";
					msg += '거래 매출전표 : ' + rsp.receipt_url+"\n";
	
					//선택한 공고 text
					var select = $("#recruitSelect option:selected").text();
					//선택한 공고의 ID (RECID)
					var recid = $("#recruitSelect option:selected").val();
					//선택한 시작 , 종료 날짜 
					var startDate = $(".checkStartDate").val();
					var endDate = $(".checkendDate").val();
					// 적극채용공고 상품코드 (1)
					var productCode = 1;
					// 상세 결제 이력 (구분 [요청:1])
					var category1 = 1;
					var category2 = 2; 
					
					console.log(select);
					console.log(recid)
					console.log(startDate);
					console.log(endDate);
					
					console.log(typeof(endDate));
					console.log(typeof(startDate));
					
					var result = { 
							orderName:rsp.name, //주문명
							buyerName:rsp.buyer_name, //주문자 이름
							paidAmount:rsp.paid_amount, // 가격
							selectRecruit:select, //선택한 공고이름
							recid:recid,		// 선택한 공고 id
							startDate:startDate, //선택한 스타트 날짜
							endDate:endDate,	 //선택한 end 날짜
							productCode:productCode, //상품코드 (적극채용공고 :1)
							category1:category1, // 카테고리 ( 요청 )
							category2:category2  // 카테고리 ( 승인 )
					};
					
					console.log(result);	
					
					$.ajax({
						url:"<%=request.getContextPath()%>/paymentRecruit",
						data :result,
						type: "get",
						success:function(data){
							if(data === "success"){
								//alert("데이터베이스 insert 성공");
									swal ({
									   title : "결제가 완료되었습니다.",
					 				  icon : "success"
									}).then((value) => {
					 					  //location.href=path; 
										location.href = "<%=request.getContextPath()%>/allPaymentList";
									});
							}else{
								//alert("데이터베이스 insert 실패");
								location.href = "<%=request.getContextPath()%>/views/company/premium2.jsp";
							}
						},
						error:function(data){
							console.log("실패!");
						}
					});
				} else { //결제 실패시 로직
					var msg = '결제에 실패하였습니다.';
					msg += '에러내용 : ' + rsp.error_msg;
					alert(msg);
				}
				//alert(msg);
			
			});
			<%}%>
		}
		
	</script>
</body>
</html>
