<%@page import="company.model.vo.UsingAdverList"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%
		ArrayList<UsingAdverList> list = (ArrayList<UsingAdverList>) request.getAttribute("list");
	%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="/views/common/import.html"%>
</head>
<style>
.file_input label {
    position:relative;
    cursor:pointer;
    display:inline-block;
    vertical-align:middle;
    overflow:hidden;
    width:100px;
    height:30px;
    background:#777;
    color:#fff;
    text-align:center;
    line-height:30px;
    border-radius : 3px;
}
.file_input label input {
	border-radius : 10px;
    position:absolute;
    width:0;
    height:0;
    overflow:hidden;
}
.file_input input[type=text] {
    vertical-align:middle;
    display:inline-block;
    width:65%;
    height:28px;
    line-height:28px;
    font-size:11px;
    padding:0;
    border:0;
    border:1px solid #777;
}


.dateResult {
	display: inline-block;
}

#resultOne {
	width: 40%;
	heigth: 50px;
	border: 1px solid lightgray;
	text-align: center;
	font-size: 1.3em;
	border-radius: 5px;
}

#resultTwo {
	width: 40%;
	heigth: 50px;
	border: 1px solid lightgray;
	text-align: center;
	font-size: 1.3em;
	border-radius: 5px;
}
.calendar{
	display: inline-block;
}
</style>


<body>

	<div class="ui modal tiny">
		<i class="close icon"></i>
		<div class="header">옵션 선택</div>
		<div class="image content">
			<div class="description" style="width: 90%; text-align: center;" >
				<div class="ui calendar" id="disable1">
						<div class="ui input left icon">
							<i class="calendar icon"></i><input type="text" placeholder="Date" class="checkStartDate">
						</div>
				</div>
				~
				<div class="ui calendar" id="disable2">
					<div class="ui input left icon">
						<i class="calendar icon"></i> <input type="text" placeholder="Date"class="checkendDate">
					</div>
				</div>
				<div class="ui header">
					<select class="ui dropdown" id="recruitSelect"
						style="width: 85%; height: 50px;">
						<option value=7>포트폴리오 배너</option>
						<option value=8>로그인 좌측 배너</option>
						<option value=9>로그인 우측 배너</option>
					</select>
				</div>
				<div style="height: 20px;"></div>
				<!-- 파일 업로드 form -->
				<form action="upload" id="uploadForm" method="post" enctype="multipart/form-data">
				<div class="file_input">
					<label> File Attach 
						<input type="file" onchange="javascript:document.getElementById('file_route').value=this.value">
					</label> 
					<input type="text" readonly="readonly" title="File Route" id="file_route">
				</div>
				</form>
				<div style="height: 20px;"></div>
			</div>
		</div>
		<div class="actions">
			<div class="ui black deny button">취소</div>
			<div class="ui positive right labeled icon button">결제하기<i class="checkmark icon"></i>
			</div>
		</div>
	</div>

	<button onclick="on()">모달 on</button>
	<script>
	function on(){
		$('.ui.modal').modal({
			onShow : function(value){
				var today = new Date();
				$('#disable1').calendar({
					  type : 'date',
					  minDays : 7,
					  maxDays : 7,
					  text:{
						  days :['월','화','수','목','금','토','일'],
						  months:['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
					  },
					  minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
					
					  formatter : {
							date : function(date, settings) {
								if (!date)
									return '';
								var day = date.getDate();
								var month = date.getMonth() + 1;
								var year = date.getFullYear();
								return year + '-' + month + '-' + day;
							}
						},
				      initialDate: new Date(),
				      disabledDates: listDate  ,
					  endCalendar : $('#disable2'),
					  
				  });
				  
					$('#disable2').calendar({
						  type : 'date',
						  minDays : 7,
						  maxDays : 7,
						  text:{
							  days :['월','화','수','목','금','토','일'],
							  months:['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
						  },
						  formatter : {
								date : function(date, settings) {
									if (!date)
										return '';
									var day = date.getDate();
									var month = date.getMonth() + 1;
									var year = date.getFullYear();
									return year + '-' + month + '-' + day;
								}
							},
					    // initialDate: new Date(new Date ($("#startDate").val()).getDate() + 7)  ,
					      initialDate : new Date(),
					      disabledDates: listDate,
					      startCalendar : $('#disable1')
					  });
			}
		}).modal('show');
		
	}
	 var listDate = [];
	/* 받은 startDate , endDate 를 막는다. */
	 <%for(int i=0; i<list.size(); i++){%>
	 	getDateRange("<%=list.get(i).getAdstartDate()%>", "<%=list.get(i).getAdendDate()%>" , listDate)
	 <%}%>
	 /* 날짜 막는 함수 */
	function getDateRange(startDate, endDate, listDate) {
        var dateMove = new Date(startDate);
        var strDate = startDate;

        if (startDate == endDate){
            var strDate = dateMove.toISOString().slice(0,10);
            listDate.push(strDate);
        }
        else{
			while (strDate < endDate){
                var strDate = dateMove.toISOString().slice(0, 10);
                listDate.push(strDate);
                dateMove.setDate(dateMove.getDate() + 1);
            }
        }
        return listDate;
    };
		
	</script>
</body>
</html>