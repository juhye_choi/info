<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
     <%
    
    Member m = (Member) request.getAttribute("m");
    
    
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<%@ include file="/views/common/import.html" %>

<style>
body{
		text-align:center;
		padding-top:5%;
		margin:auto;
	}
	
	.memberChange{
		width:15%;
		height:40px;
		margin:3px;
		background:white;
		border:2px solid black;
		font-size:17px;
	}
	.memberChange:hover{
	cursor:pointer;
}
	
	#idCheck {
		background:#CC33FF;
		color:white;
	}
	
		#allDiv {
	margin-left: 13%;
	margin-right: 8%;
	margin-top: 25px;
}

		#div {
	margin-left: 10%;
	margin-right: 8%;
	margin-top: 50px;
}

		#div2 {
	margin-left: 2%;
	margin-right: 8%;
	margin-top: 20px;
}




td {
	vertical-align: top;
	padding-top: 10px;
}



</style>
</head>
<body>

<%@ include file="../common/companyMenu.jsp" %>



<h1 style="margin-right: 55%;">기업회원 아이디 찾기</h1>





<div id="allDiv">

<hr>

<h4 style="text-align:left;"><%= m.getUserName() %>님의 아이디가<span style="color:red"> 1건</span> 검색되었습니다.</h4>
<br>

<table align="center" class="ui celled table" bolder="1">

  <thead>
    <tr>
      <th>구분</th>
      <th>아이디</th>
      <th>가입일</th>
    </tr>
  </thead>
  
  <tbody>
    <tr>
      <td>기업회원</td>
      <td><%= masking(m.getUserId()) %></td>
      <td>2019-12-23</td>
    </tr>
    </tbody>



</table>



<div id="div2">

<a href="<%= request.getContextPath() %>/views/common/companyLogin.jsp"> <button type="submit"  class="ui secondary button">로그인</button></a>

<a href="<%= request.getContextPath() %>/views/company/companyPasswordCheck.jsp"> <button type="submit"  class="ui button">비밀번호 찾기</button></a>


</div>


</div>



<%!
public String masking(String userId) {
	
	if(userId == "") {
		return "";
	}
	
	userId = userId.replaceAll("(?<=.{3}).","*");

	return userId;
}

%>









</body>
</html>