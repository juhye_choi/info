<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="resume.model.vo.*, java.util.*"%>
<%
	Resume myResume = (Resume) request.getAttribute("resume");
	
	if(myResume.getrTitle() == null){
		myResume.setrTitle("타이틀을 입력해주세요.");
	}
	if(myResume.getrIntro() == null){
		myResume.setrIntro("자기소개를 입력해주세요.");
	}
	
	
	ArrayList<Education> elist = myResume.getElist();
	ArrayList<School> slist = myResume.getSlist();
	ArrayList<Certification> celist = myResume.getCelist();
	ArrayList<Career> calist = myResume.getCalist();
	ArrayList<ProjectCareer> plist = myResume.getPlist();
	ArrayList<HashMap<String, String>> khlist = myResume.getKhlist();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>이력서</title>
<style>
	.first{
		margin-right:auto;
		margin-left:auto;
		width:70%;
	}
	.detailInfo{
		/* height:600px; */
		margin:10px;
		overflow:hidden;
		
	}
	.eduInfo{
		width:20%;
		height:100%;
		padding:10px;
		display:inline-block;
		background:#e7e6e1;
		overflow:hidden;
		vertical-align:top;
	}
	
	.projectInfo{
		width:65%;
		height:100%;
		padding:10px;
		display:inline-block;
		/* border:1px solid gray; */
		overflow:hidden;
		vertical-align:top;
	}
	.keywordInfo{
		width:14%;
		height:360px;
		padding:10px;
		display:inline-block;
		border:1px solid #e7e6e1;
		text-align:center;
		overflow:hidden;
		vertical-align:top;
		border-radius:10px;
	}
	.keywordArea{
		height:30px;
		margin-left:10px;
	}
	.keyword{
		/* display:inline-block; */
		height:20px;
		overflow:hidden;
		border:1px solid purple;
		border-radius:5px;
		color:purple;
		width:90%;
	}
	.keywordCnt{
		/* display:inline-block; */
		border-radius:10px;
		background:#e75a5a;
		width:18px;
		color:white;
		position:relative;
		top:11px;
		left:80%;
	}
	
	h4 {
		color:gray;
	}
	
	.leftDetail{
		margin:10px;
		margin-bottom:30px;
	}
	
	@media screen and (max-width: 768px){
		.first{
			width:100%;
		}
		.detailInfo{
			height:1500px;
		}
		.projectInfo{
			display:block;
			width:100%;
			height:40%;
		}
		.eduInfo{
			display:block;
			width:100%;
			height:30%;
			overflow:scroll;
		}
		.eduInfo::-webkit-scrollbar{
			display:none;
		}
		.keywordInfo{
			display:block;
			width:100%;
			height:20%;
			overflow:scroll;
		}
		.keywordInfo::-webkit-scrollbar{
			display:none;
		}
		
		.sentence{
			display:none;
		}
		.name{
			font-size:2em;
		}
		.basic2>table{
			width:100%; text-align:center; margin-top:0;
		}
		.basic2 td:last-of-type{
			display:none;
		}
		#transPDF{
			display:none;
		}
	}
	
	.editBtn{
		float:right;
		background:#e7e6e1;
		border:1px solid #e7e6e1;
		cursor:pointer;
		border-radius:5px;
		width:30px;
	
	}
	.editBtn:hover{
		background-color:lightgray;
		border-radius:10px;
	}
	
	.pjtDetail{
		width:95%;
		height:100px;
		border:1px solid #e7e6e1;
		border-radius:10px;
		margin:20px;
		padding:5px;
	}
	.pjtAdd{
		width:95%;
		height:30px;
		border:1px solid #e7e6e1;
		border-radius:10px;
		margin:20px;
		text-align:center;
		padding-top:3px;
		color:white;
		background: lightgray;
	}
	
	.pjtDetail:hover, .pjtAdd:hover{ /* background-color:#e7e6e1; */ cursor:pointer; box-shadow: 3px 3px 20px #dddddd;}
	.date{ font-size:.8em; padding-left:10px; }
	.title{ font-weight:bold; }
	.title:hover{ text-decoration:underline; cursor:pointer; }
	.explain{ font-size:.9em; color:gray; padding-left:10px; margin-bottom:10px; }
	
	.modalOuter{
		margin-left:auto;
		margin-right:auto;
		width:500px;
		height:200px;
		background:white;
		color:black;
		padding:20px;
		text-align:center;
	}
	
	.modalOuter2{
		margin-left:auto;
		margin-right:auto;
		width:500px;
		height:320px;
		background:white;
		color:black;
		padding:20px;
		text-align:center;
	}
	.modalOuter3{
		margin-left:auto;
		margin-right:auto;
		width:400px;
		height:280px;
		background:white;
		color:black;
		padding:20px;
		text-align:center;
	}
	textarea{
		border:1px solid lightgray;
		border-radius:6px;
	}
	.buttonArea{
		padding:10px;
	}
	.modalOuter10{
		margin-left:auto;
		margin-right:auto;
		width:500px;
		height:320px;
		background:white;
		color:black;
		padding:20px;
		text-align:center;
		
	}
	.pdfTable{
		width:400px;
		height:200px;
	}
	.pintro{
		display:inline-block;
		overflow:hidden;
		vertical-align:top;
	}
	.ptitle{
		padding-left:2px;
		font-size:1.2em;
		font-weight:bold;

	}
	.pdate{
		color:gray;
		padding:5px;

	}
	.pcontent{
		color:lightgray;
		padding-left:5px;
		overflow:hidden;
		/* width:450px; */
		/* height:60px; */

	}
	.pimg{
		width:88px;
		height:88px;
		border-radius:5px;
		border:1px solid gray;
		display:inline-block;
		overflow:hidden;
	}
	
</style>
<%@ include file="/views/common/import.html" %>
</head>
<body>
<%@ include file="../common/mainMenu.jsp" %>
	<%if(loginUser == null){
		request.setAttribute("msg", "로그인이 필요한 메뉴입니다");
		request.getRequestDispatcher("../common/needLogin.jsp").forward(request, response);
	} %>
	<!-- <h1 align="center">이력서</h1> -->
	<div class="first">
		<%@ include file="resumeCommon.jsp" %>
		<div class="detailInfo">
			<div class="eduInfo">
				<div class="leftDetail">
					<button class="editBtn" id="modifyIntro"><i class="icon purple edit"></i></button>
					<h4>자기소개</h4>
					<div><pre><%=myResume.getrIntro() %></pre></div>
				</div>
				<div class="leftDetail">
						<button class="editBtn" id="modifyJob"><i class="icon purple plus circle"></i></button>
						<h4>경력</h4>
					<%if(calist != null){
						for(Career c : calist){%>
							<div>
								<input type="hidden" name="career" value="<%=c.getCarId()%>" />
								<div id="title" class="title mtitle"><%=c.getCarName() %></div>
								<%if(c.getCarFinishDate() != null){ %>
									<div id="date" class="date"><%=c.getCarStartDate() %> ~ <%=c.getCarFinishDate()%></div>
								<%} else{ %>
									<div id="date" class="date"><%=c.getCarStartDate() %> ~ now</div>
								<%} 
								if(c.getCarContent() != null){ %>
									<div id="explain" class="explain"><%=c.getCarContent() %></div>
								<%} else{%>
									<div id="explain" class="explain">-</div>
								<%} %>
							</div>
					<%}} %>
				</div>
				<div class="leftDetail">
						<button class="editBtn" id="modifySch"><i class="icon purple plus circle"></i></button>
						<h4>학력</h4>
					<%if(slist != null){
						for(School s : slist){ %>
						<div>
							<input type="hidden" name="school" value="<%=s.getSid()%>" />
							<div id="title" class="title stitle"><%=s.getsName() %></div>
							<%if(s.getGraduDate() != null){ %>
								<div id="date" class="date"><%=s.getEnterDate() %> ~ <%=s.getGraduDate() %></div>
							<%} else{ %>
								<div id="date" class="date"><%=s.getEnterDate() %> ~ now</div>
							<%}
							if(s.getMajor() != null){%>
								<div id="explain" class="explain"><%=s.getMajor() %></div>
							<%} else{%>
								<div id="explain" class="explain">-</div>
							<%} %>
						</div>
					<%}} %>
				</div>
				<div class="leftDetail">
						<button class="editBtn" id="modifyEdu"><i class="icon purple plus circle"></i></button>
						<h4>교육</h4>
					<%if(elist != null){
						for(Education e : elist){%>
						<div>
							<input type="hidden" name="education" value="<%=e.getEid()%>" />
							<div id="title" class="title etitle"><%=e.geteName() %></div>
							<%if(e.getEuFinishDate() != null){ %>
								<div id="date" class="date"><%=e.getEuStartDate() %> ~ <%=e.getEuFinishDate() %></div>
							<%} else{%>
								<div id="date" class="date"><%=e.getEuStartDate() %> ~ now</div>
							<%} %>
							<div id="explain" class="explain"><%=e.getEuContent() %></div>
						</div>
					<%}} %>
				</div>
				<div class="leftDetail">
					<button class="editBtn" id="modifyCer"><i class="icon purple plus circle"></i></button>
					<h4>자격증</h4>
					<%if(celist != null){
					for(Certification ce : celist){ %>
					<div>
						<input type="hidden" name="certification" value="<%=ce.getcerId()%>"/>
						<div class="title ctitle"><%=ce.getCerName() %></div>
						<div id="date" class="date"><%=ce.getCertifyDate() %></div>
						<div id="explain" class="explain"></div>
					</div>
					<%}} %>
				</div>
			</div>
			
			

			<div class="projectInfo">
				<h4>프로젝트 경력기술서</h4>
				<%if(plist != null){ 
					for(ProjectCareer pc : plist){
					%>
					<div class="pjtDetail" id="project1">
						<input type="hidden" name="project" value="<%=pc.getPjtId()%>">
						<%if(pc.getAtlist().get(0).getChangeName() != null){ %>
						<div class="pimg"><img src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=(pc.getAtlist()).get(0).getChangeName()%>" width="88px" height="88px"></div>
						<%} else{ %>
						<div class="pimg"></div>
						<%} %>
						<div class="pintro">
							<div class="ptitle"><%=pc.getPjtName() %></div>
							<div class="pdate"><%=pc.getPjtStart() %> ~ <%=pc.getPjtFinish()%> </div>
							<div class="pcontent"><%=pc.getMyRole() %></div>
						</div>
					</div>
				<%}} %>
					<div class="pjtAdd" id="pjtAdd">경력기술서 추가하기</div>
			</div>
			

			<div class="keywordInfo">
				<h4 style="margin:0px;">연관키워드</h4>
				<%if(khlist != null){
					int cnt = 10;
					if(cnt > khlist.size()){
						cnt = khlist.size();
					}
					for(int i=0; i<cnt; i++){%>
					<div class="keywordArea">
						<div class="keywordCnt"><%=khlist.get(i).get("count") %></div>
						<div class="keyword"><%=khlist.get(i).get("kname")%></div>
					</div>
				<%}} %>
				
			</div> <!-- 키워드 영역 끝! -->
		</div>
	</div> <!-- first Area End -->

	<%-- 모달 영역! --%>
	<div class="ui basic modal" id="introModal">
		<div class="modalOuter">
			<h4>자기소개</h4>
			<hr>
			<form action="<%=request.getContextPath() %>/update.in" method="post">
				<textarea cols="60" rows="5" style="resize:none;" name="intro"><%=myResume.getrIntro() %></textarea>
				<input type="hidden" name="uno" value="<%=myResume.getUno()%>">
				<div class="buttonArea">
					<button type="submit" id="ibtn1" class="ui purple button mini">수정</button>
					<button type="reset" id="ibtn2" class="ui button mini">취소</button>
				</div>
			</form>
		</div>
	</div> <!-- 자기소개 모달 끝 -->
	
	<div class="ui basic modal" id="jobModal">
		<div class="modalOuter2">
			<h4>경력사항</h4>
			<hr>
			<form action="<%=request.getContextPath() %>/update.ca" id="careerForm" method="post">
				<table align="center" style="height:200px;">
					<tr>
						<td><label>회사명</label></td>
						<td colspan="3"><div class="ui input"><input id="mCareer1" name="title" type="text" size="48" readonly></div></td>
					</tr>
					<tr>
						<td><label>근무기간</label></td>
						<td><div class="ui calendar" id="rangestart"><div class="ui input left icon">
								<i class="calendar icon"></i><input type="text" name="date1" id="mCareer2" placeholder="Start Date" readonly size="16"></div></div></td>
						<td width="10">~</td><td><div class="ui calendar" id="rangeend"><div class="ui input left icon">
								<i class="calendar icon"></i><input type="text" name="date2" id="mCareer3" placeholder="End Date" readonly size="16"></div></div>
						</td>
						<!-- <td><div class="ui input"><input type="date" name="date1" id="mCareer2" readonly></div>
							~ <div class="ui input"><input type="date" name="date2" id="mCareer3" readonly></div>
						</td> -->
					</tr>
					<tr>
						<td><label>직무/직책</label></td>
						<td  colspan="3"><div class="ui input"><input type="text" name="content" size="48" id="mCareer4" readonly></div>
							<input type="hidden" name="carid" id="mCareer5">
						</td>
					</tr>
				</table>
				<div class="buttonArea">
					<button type="button" id="jbtn1" class="ui purple button mini" style="display:none">추가하기</button>
					<input type="button" id="jbtn2" class="ui purple button mini" value="수정하기">
					<button type="button" id="jbtn3" class="ui purple button mini" style="display:none">수정</button>
					<input type="button" id="jbtn4" class="ui black button mini" style="display:none" value="삭제">
					<button type="reset" id="jbtn5" class="ui button mini">취소</button>
				</div>
			</form>
		</div>
	</div> <!-- 경력 모달 끝 -->
	<div class="ui basic modal" id="schModal">
		<div class="modalOuter2">
			<h4>학력사항</h4>
			<hr>
			<form action="<%=request.getContextPath() %>/update.sch" method="post" id="schoolForm">
				<table align="center" style="height:200px;">
					<tr>
						<td><label>학교명</label></td>
						<td colspan="3"><div class="ui input"><input name="title" id="mSchool1" type="text" size="48"></div></td>
					</tr>
					<tr>
						<td><label>재학기간</label></td>
						<td><div class="ui calendar" id="rangestart2"><div class="ui input left icon">
								<i class="calendar icon"></i><input type="text" name="date1" id="mSchool2" placeholder="Start Date" readonly size="16"></div></div></td>
						<td width="10">~</td><td><div class="ui calendar" id="rangeend2"><div class="ui input left icon">
								<i class="calendar icon"></i><input type="text" name="date2" id="mSchool3" placeholder="End Date" readonly size="16"></div></div>
						</td>
						<!-- <td><div class="ui input"><input type="date" name="date1" id="mSchool2"></div>
							~ <div class="ui input"><input type="date" name="date2" id="mSchool3"></div>
						</td> -->
					</tr>
					<tr>
						<td><label>전공</label></td>
						<td colspan="3"><div class="ui input"><input type="text" name="major" size="48" id="mSchool4"></div>
							<input type="hidden" name="sid" id="mSchool5">
						</td>
					</tr>
				</table>
				<div class="buttonArea">
					<button type="button" id="sbtn1" class="ui purple button mini" style="display:none">추가하기</button>
					<input type="button" id="sbtn2" class="ui purple button mini" value="수정하기">
					<button type="button" id="sbtn3" class="ui purple button mini" style="display:none">수정</button>
					<input type="button" id="sbtn4" class="ui black button mini" style="display:none" value="삭제">
					<button type="reset" id="sbtn5" class="ui button mini">취소</button>
				</div>
			</form>
		</div>
	</div> <!-- 학력 모달 끝 -->
	<div class="ui basic modal" id="eduModal">
		<div class="modalOuter2">
			<h4>교육사항</h4>
			<hr>
			<form action="<%=request.getContextPath() %>/update.edu" method="post" id="educationForm">
				<table align="center" style="height:200px;">
					<tr>
						<td><label>교육기관명</label></td>
						<td colspan="3"><div class="ui input"><input name="title" id="mEdu1" type="text" size="48"></div></td>
					</tr>
					<tr>
						<td><label>교육기간</label></td>
						<td><div class="ui calendar" id="rangestart3"><div class="ui input left icon">
								<i class="calendar icon"></i><input type="text" name="date1" id="mEdu2" placeholder="Start Date" readonly size="16"></div></div></td>
						<td width="10">~</td><td><div class="ui calendar" id="rangeend3"><div class="ui input left icon">
								<i class="calendar icon"></i><input type="text" name="date2" id="mEdu3" placeholder="End Date" readonly size="16"></div></div>
						</td>
						<!-- <td><div class="ui input"><input type="date" name="date1" id="mEdu2"></div>
							~ <div class="ui input"><input type="date" name="date2" id="mEdu3"></div>
						</td> -->
					</tr>
					<tr>
						<td><label>교육내용</label></td>
						<td colspan="3"><div class="ui input"><input type="text" name="content" size="48" id="mEdu4"></div>
							<input type="hidden" id="mEdu5" name="eid">
						</td>
					</tr>
				</table>
				<div class="buttonArea">
					<button type="button" id="ebtn1" class="ui purple button mini" style="display:none">추가하기</button>
					<input type="button" id="ebtn2" class="ui purple button mini" value="수정하기">
					<button type="button" id="ebtn3" class="ui purple button mini" style="display:none">수정</button>
					<input type="button" id="ebtn4" class="ui black button mini" style="display:none" value="삭제">
					<button type="reset" id="ebtn5" class="ui button mini">취소</button>
				</div>
			</form>
		</div>
	</div> <!-- 교육 모달 끝 -->
	<div class="ui basic modal" id="cerModal">
		<div class="modalOuter3">
			<h4>자격증 사항</h4>
			<hr>
			<form action="<%=request.getContextPath() %>/update.cer" method="post" id="certificationForm">
				<table align="center" style="height:150px;">
					<tr>
						<td><label>자격증명</label></td>
						<td><div class="ui input"><input name="cerName" id="mCer1" type="text" size="20"></div></td>
					</tr>
					<tr>
						<td><label>취득일</label></td>
						<td><div class="ui input"><input name="date1" type="date" id="mCer2"></div>
							<input type="hidden" id="mCer3" name="cerid">
						</td>
					</tr>
				</table>
				<div class="buttonArea">
					<button type="button" id="cbtn1" class="ui purple button mini" style="display:none">추가하기</button>
					<input type="button" id="cbtn2" class="ui purple button mini" value="수정하기">
					<button type="button" id="cbtn3" class="ui purple button mini" style="display:none">수정</button>
					<input type="button" id="cbtn4" class="ui black button mini" style="display:none" value="삭제">
					<button type="reset" id="cbtn5" class="ui button mini">취소</button>
				</div>
			</form>
		</div>
	</div> <!-- 자격증 모달 끝 -->
	
	<div class="ui basic modal" id="pdfmodal">
		<div class="modalOuter10">
			<h4>PDF로 변환할 정보를 선택하세요.</h4>
			<hr>
			<table class="pdfTable">
				<tr>
					<td><label>기본정보 : </label></td>
					<td><input type="checkbox" id="emailM" value="email" checked><label for="emailM">이메일</label></td>
					<td><input type="checkbox" id="phoneM" value="phone" checked><label for="phoneM">연락처</label></td>
					<td><input type="checkbox" id="photoM" value="photo" checked><label for="photoM">사진</label></td>
				</tr>
				<tr>
					<td><label>경력 : </label></td>
					<td colspan="3"><input type="checkbox" id="job1" value="job1" checked><label for="job1">가가오뱅크</label>
					<input type="checkbox" id="job2" value="job2" checked><label for="job2">KH아이티</label>
					<input type="checkbox" id="job3" value="job3" checked><label for="job3">네이뻐</label>
					</td>
				</tr>
				<tr>
					<td><label>학력 : </label></td>
					<td colspan="3"><input type="checkbox" id="sch1" value="sch1" checked><label for="sch1">유명대학교</label>
					</td>
				</tr>
				<tr>
					<td><label>교육 : </label></td>
					<td colspan="3"><input type="checkbox" id="edu1" value="edu1" checked><label for="edu1">KH정보교육원</label>
					</td>
				</tr>
				<tr>
					<td><label>자격증 : </label></td>
					<td colspan="3"><input type="checkbox" id="cer1" value="cer1" checked><label for="cer1">정보처리기사</label>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<script>
	
	
		$(function(){
			$(".pjtDetail").click(function(){
				var num = $(this).find("input[type=hidden]").val();
				location.href="<%=request.getContextPath()%>/selectOne.pjt?type=1&num=" + num;
			});
			
			//
			$("#modifyIntro").click(function(){
				$('#introModal').modal('show');
			});
			
			//추가하기 눌렀을 떄!
			$("#modifyJob").click(function(){
				$("#jbtn1").css("display", "inline");
				$("#jbtn2").css("display", "none");
				$("#jbtn3").css("display", "none");
				$("#jbtn4").css("display", "none");
				$("#mCareer1").attr("readonly", false);
				/* $("#mCareer2").attr("readonly", false);
				$("#mCareer3").attr("readonly", false); */
				$("#mCareer4").attr("readonly", false);
				$("#mCareer1").val("");
				$("#mCareer2").val("");
				$("#mCareer3").val("");
				$("#mCareer4").val("");
				$("#mCareer5").val(0);
				$("#jobModal").modal({
					onShow : function(value){
						$('#rangestart').calendar({
							type: 'date',
							formatter : {
								date: function(date, settings){
									if(!date) return '';
									var day = date.getDate();
									var month = date.getMonth() +1;
									var year = date.getFullYear();
									return year + '-' + month + '-' + day;
								}
							},
							endCalendar: $('#rangeend')
						});
						$('#rangeend').calendar({
							type: 'date',
							formatter : {
								date: function(date, settings){
									if(!date) return '';
									var day = date.getDate();
									var month = date.getMonth() +1;
									var year = date.getFullYear();
									return year + '-' + month + '-' + day;
								}
							},
							startCalendar: $('#rangestart')
						});
					}
				}).modal('show');
			});

			$("#modifySch").click(function(){
				$("#sbtn1").css("display", "inline");
				$("#sbtn2").css("display", "none");
				$("#sbtn3").css("display", "none");
				$("#sbtn4").css("display", "none");
				$("#mSchool1").attr("readonly", false);
				/* $("#mSchool2").attr("readonly", false);
				$("#mSchool3").attr("readonly", false); */
				$("#mSchool4").attr("readonly", false);
				$("#mSchool1").val("");
				$("#mSchool2").val("");
				$("#mSchool3").val("");
				$("#mSchool4").val("");
				$("#mSchool5").val(0);
				$("#schModal").modal({
					onShow : function(value){
						$('#rangestart2').calendar({
							type: 'date',
							formatter : {
								date: function(date, settings){
									if(!date) return '';
									var day = date.getDate();
									var month = date.getMonth() +1;
									var year = date.getFullYear();
									return year + '-' + month + '-' + day;
								}
							},
							endCalendar: $('#rangeend2')
						});
						$('#rangeend2').calendar({
							type: 'date',
							formatter : {
								date: function(date, settings){
									if(!date) return '';
									var day = date.getDate();
									var month = date.getMonth() +1;
									var year = date.getFullYear();
									return year + '-' + month + '-' + day;
								}
							},
							startCalendar: $('#rangestart2')
						});
					}
				}).modal('show');
			});

			$("#modifyEdu").click(function(){
				$("#ebtn1").css("display", "inline");
				$("#ebtn2").css("display", "none");
				$("#ebtn3").css("display", "none");
				$("#ebtn4").css("display", "none");
				$("#mEdu1").attr("readonly", false);
				/* $("#mEdu2").attr("readonly", false);
				$("#mEdu3").attr("readonly", false); */
				$("#mEdu4").attr("readonly", false);
				$("#mEdu1").val("");
				$("#mEdu2").val("");
				$("#mEdu3").val("");
				$("#mEdu4").val("");
				$("#mEdu5").val(0);
				$("#eduModal").modal({
					onShow : function(value){
						$('#rangestart3').calendar({
							type: 'date',
							formatter : {
								date: function(date, settings){
									if(!date) return '';
									var day = date.getDate();
									var month = date.getMonth() +1;
									var year = date.getFullYear();
									return year + '-' + month + '-' + day;
								}
							},
							endCalendar: $('#rangeend3')
						});
						$('#rangeend3').calendar({
							type: 'date',
							formatter : {
								date: function(date, settings){
									if(!date) return '';
									var day = date.getDate();
									var month = date.getMonth() +1;
									var year = date.getFullYear();
									return year + '-' + month + '-' + day;
								}
							},
							startCalendar: $('#rangestart3')
						});
					}
				}).modal('show');
			});

			$("#modifyCer").click(function(){
				$("#cbtn1").css("display", "inline");
				$("#cbtn2").css("display", "none");
				$("#cbtn3").css("display", "none");
				$("#cbtn4").css("display", "none");
				$("#mCer1").attr("readonly", false);
				/* $("#mCer2").attr("readonly", false); */
				$("#mCer1").val("");
				$("#mCer2").val("");
				$("#mCer3").val(0);
				$("#cerModal").modal('show');
			});
			
			
			
			//수정하기 눌렀을 때, 동작!
			$(".title.mtitle").click(function(){
				$("#jbtn1").css("display", "none");
				$("#jbtn2").css("display", "inline");
				$("#jbtn3").css("display", "none");
				$("#jbtn4").css("display", "none");
				$("#mCareer1").attr("readonly", true);
				/* $("#mCareer2").attr("readonly", true);
				$("#mCareer3").attr("readonly", true); */
				$("#mCareer4").attr("readonly", true);
				var date = $(this).parent().children().eq(2).text();
				var day = date.split(" ~ ");
				
				
				$("#mCareer1").val($(this).text());
				$("#mCareer2").val(day[0]);
				if(day[1] != "now"){
					$("#mCareer3").val(day[1]);
				}
				$("#mCareer4").val($(this).parent().children().eq(3).text());
				$("#mCareer5").val($(this).parent().find("input[type=hidden]").val());
				
				$("#jobModal").modal({
					onShow : function(value){
						$('#rangestart').calendar({
							type: 'date',
							formatter : {
								date: function(date, settings){
									if(!date) return '';
									var day = date.getDate();
									var month = date.getMonth() +1;
									var year = date.getFullYear();
									return year + '-' + month + '-' + day;
								}
							},
							endCalendar: $('#rangeend')
						});
						$('#rangeend').calendar({
							type: 'date',
							formatter : {
								date: function(date, settings){
									if(!date) return '';
									var day = date.getDate();
									var month = date.getMonth() +1;
									var year = date.getFullYear();
									return year + '-' + month + '-' + day;
								}
							},
							startCalendar: $('#rangestart')
						});
					}
				}).modal('show');
			});
			
			$(".title.stitle").click(function(){
				$("#sbtn1").css("display", "none");
				$("#sbtn2").css("display", "inline");
				$("#sbtn3").css("display", "none");
				$("#sbtn4").css("display", "none");
				$("#mSchool1").attr("readonly", true);
				/* $("#mSchool2").attr("readonly", true);
				$("#mSchool3").attr("readonly", true); */
				$("#mSchool4").attr("readonly", true);
				var date = $(this).parent().children().eq(2).text();
				var day = date.split(" ~ ");
				
				
				$("#mSchool1").val($(this).text());
				$("#mSchool2").val(day[0]);
				if(day[1] != "now"){
					$("#mSchool3").val(day[1]);					
				}
				$("#mSchool4").val($(this).parent().children().eq(3).text());
				$("#mSchool5").val($(this).parent().find("input[type=hidden]").val());
				$("#schModal").modal('show');
			});
			
			$(".title.etitle").click(function(){
				$("#ebtn1").css("display", "none");
				$("#ebtn2").css("display", "inline");
				$("#ebtn3").css("display", "none");
				$("#ebtn4").css("display", "none");
				$("#mEdu1").attr("readonly", true);
				/* $("#mEdu2").attr("readonly", true);
				$("#mEdu3").attr("readonly", true); */
				$("#mEdu4").attr("readonly", true);
				var date = $(this).parent().children().eq(2).text();
				var day = date.split(" ~ ");
				
				$("#mEdu1").val($(this).text());
				$("#mEdu2").val(day[0]);
				if(day[1] != 'now'){
					$("#mEdu3").val(day[1]);
				}
				$("#mEdu4").val($(this).parent().children().eq(3).text());
				$("#mEdu5").val($(this).parent().find("input[type=hidden]").val());
				$("#eduModal").modal('show');
			});
			$(".title.ctitle").click(function(){
				$("#cbtn1").css("display", "none");
				$("#cbtn2").css("display", "inline");
				$("#cbtn3").css("display", "none");
				$("#cbtn4").css("display", "none");
				$("#mCer1").attr("readonly", true);
				/* $("#mCer2").attr("readonly", true); */
				
				$("#mCer1").val($(this).text());
				$("#mCer2").val($(this).parent().children().eq(2).text());
				$("#mCer3").val($(this).parent().find("input[type=hidden]").val());
				$("#cerModal").modal('show');
			});
			
			//모달의 버튼영역!
			$("#ibtn1").click(function(){ //수정하기 버튼!!!!
			});
			$("#ibtn2").click(function(){ // 취소 & 모달 닫기
				$("#introModal").modal('hide');
			});
			
			$("#jbtn1").click(function(){ // 추가하기 버튼!!!!
				//console.log("제출!"); 
				if($("#mCareer1").val() == ""){
					alert("회사명 빈칸은 불가합니다.");
					$("#mCareer1").parent().addClass("error");
					$("#mCareer1").focus();
				} else if($("#mCareer2").val() == ""){
					alert("시작날짜는 빈칸이 불가합니다.");
					$("#mCareer2").parent().addClass("error");
					$("#mCareer2").focus();	
				} else{
					$("#careerForm").submit();
					
				}
			});
			$("#jbtn2").click(function(){ //수정을 가능하게 하는 버튼!
				$(this).css("display", "none");
				$("#jbtn3").css("display", "inline");
				$("#jbtn4").css("display", "inline");
				$("#mCareer1").attr("readonly", false);
				/* $("#mCareer2").attr("readonly", false);
				$("#mCareer3").attr("readonly", false); */
				$("#mCareer4").attr("readonly", false);
				
			});
			$("#jbtn3").click(function(){ // 수정하기 버튼!!
				if($("#mCareer1").val() == ""){
					alert("회사명 빈칸은 불가합니다.");
					$("#mCareer1").parent().addClass("error");
					$("#mCareer1").focus();
				} else if($("#mCareer2").val() == ""){
					alert("시작날짜는 빈칸이 불가합니다.");
					$("#mCareer2").parent().addClass("error");
					$("#mCareer2").focus();	
				} else{
					$("#careerForm").submit();
				}
			});
			$("#jbtn4").click(function(){ // 삭제하기 버튼!!
				var num = $(this).parent().parent().find("input[type=hidden]").val();
				location.href="<%=request.getContextPath()%>/delete.ca?num=" + num;
			});
			$("#jbtn5").click(function(){ //취소 & 모달 닫기
				$("#jobModal").modal('hide');
			});
			
			$("#sbtn1").click(function(){ //학력 추가하기 버튼!!
				if($("#mSchool1").val() == ""){
					alert("학교명 빈칸은 불가합니다.");
					$("#mSchool1").parent().addClass("error");
					$("#mSchool1").focus();
				} else if($("#mSchool2").val() == ""){
					alert("시작날짜는 빈칸이 불가합니다.");
					$("#mSchool2").parent().addClass("error");
					$("#mSchool2").focus();	
				} else{
					$("#schoolForm").submit();
				}
			});
			$("#sbtn2").click(function(){ //수정을 가능하게 하는 버튼!
				$(this).css("display", "none");
				$("#sbtn3").css("display", "inline");
				$("#sbtn4").css("display", "inline");
				$("#mSchool1").attr("readonly", false);
				/* $("#mSchool2").attr("readonly", false);
				$("#mSchool3").attr("readonly", false); */
				$("#mSchool4").attr("readonly", false);
				
			});
			$("#sbtn3").click(function(){ // 수정하기 버튼!!
				if($("#mSchool1").val() == ""){
					alert("학교명 빈칸은 불가합니다.");
					$("#mSchool1").parent().addClass("error");
					$("#mSchool1").focus();
				} else if($("#mSchool2").val() == ""){
					alert("시작날짜는 빈칸이 불가합니다.");
					$("#mSchool2").parent().addClass("error");
					$("#mSchool2").focus();	
				} else{
					$("#schoolForm").submit();
				}
			});
			$("#sbtn4").click(function(){ // 삭제하기 버튼!!
				var num = $(this).parent().parent().find("input[type=hidden]").val();
				location.href="<%=request.getContextPath()%>/delete.sch?num=" + num;
			});
			$("#sbtn5").click(function(){ //취소 & 모달 닫기
				$("#schModal").modal('hide');
			});
				
			$("#ebtn1").click(function(){ //교육 추가하기 버튼!!
				if($("#mEdu1").val() == ""){
					alert("교육명 빈칸은 불가합니다.");
					$("#mEdu1").parent().addClass("error");
					$("#mEdu1").focus();
				} else if($("#mEdu2").val() == ""){
					alert("시작날짜는 빈칸이 불가합니다.");
					$("#mEdu2").parent().addClass("error");
					$("#mEdu2").focus();	
				} else if($("#mEdu4").val() == ""){
					alert("교육내용은 빈칸이 불가합니다.");
					$("#mEdu4").parent().addClass("error");
					$("#mEdu4").focus();	
				} else{
					$("#educationForm").submit();
				}
			});
			$("#ebtn2").click(function(){ //수정을 가능하게 하는 버튼!
				$(this).css("display", "none");
				$("#ebtn3").css("display", "inline");
				$("#ebtn4").css("display", "inline");
				$("#mEdu1").attr("readonly", false);
				/* $("#mEdu2").attr("readonly", false);
				$("#mEdu3").attr("readonly", false); */
				$("#mEdu4").attr("readonly", false);
				
			});
			$("#ebtn3").click(function(){ // 수정하기 버튼!!
				if($("#mEdu1").val() == ""){
					alert("교육명 빈칸은 불가합니다.");
					$("#mEdu1").parent().addClass("error");
					$("#mEdu1").focus();
				} else if($("#mEdu2").val() == ""){
					alert("시작날짜는 빈칸이 불가합니다.");
					$("#mEdu2").parent().addClass("error");
					$("#mEdu2").focus();	
				} else if($("#mEdu4").val() == ""){
					alert("교육내용은 빈칸이 불가합니다.");
					$("#mEdu4").parent().addClass("error");
					$("#mEdu4").focus();	
				} else{
					$("#educationForm").submit();
				}
			});
			$("#ebtn4").click(function(){ // 삭제하기 버튼!!
				var num = $(this).parent().parent().find("input[type=hidden]").val();
				location.href="<%=request.getContextPath()%>/delete.edu?num=" + num;
			});
			$("#ebtn5").click(function(){ //취소 & 모달 닫기
				$("#eduModal").modal('hide');
			});
			
			
			$("#cbtn1").click(function(){ //자격증 추가하기 버튼!!
				if($("#mCer1").val() == ""){
					alert("자격증명 빈칸은 불가합니다.");
					$("#mCer1").parent().addClass("error");
					$("#mCer1").focus();
				} else if($("#mCer2").val() == ""){
					alert("취득일 빈칸이 불가합니다.");
					$("#mCer2").parent().addClass("error");
					$("#mCer2").focus();	
				} else{
					$("#certificationForm").submit();
				}	
			});
			$("#cbtn2").click(function(){ //수정을 가능하게 하는 버튼!
				$(this).css("display", "none");
				$("#cbtn3").css("display", "inline");
				$("#cbtn4").css("display", "inline");
				$("#mCer1").attr("readonly", false);
				/* $("#mCer2").attr("readonly", false); */
				
			});
			$("#cbtn3").click(function(){ // 수정하기 버튼!!
				if($("#cbtn1").val() == ""){
					alert("자격증명 빈칸은 불가합니다.");
					$("#cbtn1").parent().addClass("error");
					$("#cbtn1").focus();
				} else if($("#cbtn2").val() == ""){
					alert("취득일 빈칸이 불가합니다.");
					$("#cbtn2").parent().addClass("error");
					$("#cbtn2").focus();	
				} else{
					$("#certificationForm").submit();
				}	
			});
			$("#cbtn4").click(function(){ // 삭제하기 버튼!!
				var num = $(this).parent().parent().find("input[type=hidden]").val();
				console.log(num);
				location.href="<%=request.getContextPath()%>/delete.cer?num=" + num;
			});
			$("#cbtn5").click(function(){ //취소 & 모달 닫기
				$("#cerModal").modal('hide');
			});
			
			
			
			$("#finish").css("visibility","visible");
			
			$("#finish").click(function(){
				var num = <%=loginUser.getUno()%>;
				window.location.href="<%=request.getContextPath()%>/selectOther.re?otherUno=" + num;
			});
			
			$("#transPDF").css("visibility","visible");
			
			$("#transPDF").click(function(){
				//$("#pdfmodal").modal('show');
				location.href = "<%=request.getContextPath()%>/selectOne.re?type=1";
			});
			
			$("#pjtAdd").click(function(){
				location.href="<%=request.getContextPath()%>/selectList.ke?type=1";
			});
			
			$("#title").click(function(){
				//console.log("타이틀 수정!");
				$("#title").hide();
				$("#titleInput").show();
				$("#titleModify").show();
				$("#titleModifyCancel").show();
			});
			
			$("#titleModifyCancel").click(function(){
				$("#title").show();
				$("#titleInput").hide();
				$("#titleModify").hide();
				$("#titleModifyCancel").hide();
			});
			$("#titleModify").click(function(){
				$("#unoInput").val("<%=loginUser.getUno()%>");
				
				$("#rtitleForm").submit();
			});
			
			//기본정보 출력
			$("#name").text("<%=loginUser.getUserName()%>");
			$("#title").text("<%=myResume.getrTitle()%>");
			$("#titleInput").val("<%=myResume.getrTitle()%>");
			$("#emailAddress").text("<%=loginUser.getEmail()%>");
			<%if(loginUser.getPhone() != null){%>
				$("#phoneNumber").text("<%=loginUser.getPhone()%>");
			<%} else{%>
				$("#phoneNumber").text("-");
			<%}%>
		});
	
	</script>
	
	
	
	<%@ include file="../common/ourfooter.jsp" %>
</body>
</html>