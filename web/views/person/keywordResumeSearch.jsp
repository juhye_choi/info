<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, person.model.vo.*, keyword.model.vo.*, keyword.model.vo.*"%>
<% 
	ArrayList<Person> list = (ArrayList<Person>) request.getAttribute("list");
	ArrayList<Person> rlist = (ArrayList<Person>) request.getAttribute("rlist");
	String keyword = (String) request.getAttribute("keyword");
	ArrayList<Keyword> klist = (ArrayList<Keyword>) request.getAttribute("klist");
%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/css/swiper.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.min.js"></script>
<script type="text/javascript"src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<%@ include file="/views/common/import.html"%>
<style>
#category {
	background: rgb(52, 152, 219);
	color: white;
	margin: 10px;
	height: 40;
	width: 100;
	border-radius: 5px;
}

.resume {
	padding-top: 50px;
}

form {
	border-radius: 10px;
	border: 1px solid black;
	width: 197;
	height: 246;
	display: inline-block;
	text-align: center;
}

#resumeCategory {
	align: left;
	margin: 20px;
	width:40%;
	font-weight: bold;
	font-size: 15px;
	width:200px;
	text-align:left;
}

#detailSearch {
	width: 80%;
	padding: 10px;
	display: none;
	border: 1px solid lightgray;
	margin-left: auto;
	margin-right: auto;
}

#detailSearch button {
	background: white;
	border: 0;
	font-size: 20px;
}

#detailmenu {
	width: 90%;
	height: 60px;
	display: none;
	margin: 30px;
}

#menu1, #menu2, #menu3 {
	width: 80%;
	height: 50px;
	margin: 20px;
}

table p {
	margin: 10px;
	font-size: 15px;
	font-weight: bold;
}

#profile {
	width: 60px;
	height: 60px;
	border-radius: 100%;
	text-align: center;
	position: relative;
	background: url(/info/images/profilePhoto.jpg) no-repeat 50%;
	background-size: cover;
	margin-left: auto;
	margin-right: auto;
	z-index: 1;
}

.swiper-container {
	width: 90%;
	height: 40%;
	padding:0;
}

.swiper-slide {
	text-align: center;
	display: flex; /* 내용을 중앙정렬 하기위해 flex 사용 */
	align-items: center; /* 위아래 기준 중앙정렬 */
	justify-content: center; /* 좌우 기준 중앙정렬 */
}
#detailKeywordArea input {
	margin:10px;
	font-size:13px;
	width:130px;
	text-align:center;
	justify-content: center;
	align-items: center;
}
</style>
</head>
<body>
	<%@ include file="../common/mainMenu.jsp"%>
	<div id="resume" class="resume" style="padding-top:50px; margin-left:auto; margin-right:auto; width: 80%;">
		<table>
			<tr>
				<td>
					<div class="ui icon input">
  						<input type="text" placeholder="검색" class="searchKeyword">
  						<i class="circular search link icon" id="searchKeyword1"></i>
					</div>
				</td>
				<td>
					<input value="검색" id="searchKeyword2" class="ui button" style="width:120px;">
				</td>
				<td colspan="5">
					<input value="상세검색" id="detailCategory" class="ui button" style="width:150px;">
				</td>
				<% if(keyword != null) { %>
					<td>
						<input type="text" class="ui button" value="검색 키워드 :  <%= keyword %>" style="margin-left:30px; background:white;" readonly>
					</td>
				<%}else if(klist != null){ %>
					<td class="ui button" style="background:white;">
						<%for(int k = 0; k < klist.size(); k++) {%>
							<% if(k == 0) { %>
									검색 키워드 : <%= klist.get(k).getKname() %>  
							<% }else { %>
									/  <%= klist.get(k).getKname() %>	 
							<% }}%>
					</td>
				<%}%>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	<div id="detailSearch">
		<table align="center">
			<tr>
				<td>
					<div class="ui icon input">
						<input type="button" name="CategoryDetail" value="언어" class="ui button" style="width:140px; border:none; text-align:center; padding:10px; font-size:16px;">
					</div>
				</td>
				<td>
					<div class="ui icon input">
						<input type="button" name="CategoryDetail" value="모바일/OS" class="ui button" style="width:140px; border:none; text-align:center; padding:10px; font-size:16px;">
					</div>
				</td>
				<td>
					<div class="ui icon input">
						<input type="button" name="CategoryDetail" value="웹개발" class="ui button" style="width:140px; border:none; text-align:center; padding:10px; font-size:16px;">
					</div>
				</td>
				<td>
					<div class="ui icon input">
						<input type="button" name="CategoryDetail" value="프레임워크" class="ui button" style="width:140px; border:none; text-align:center; padding:10px; font-size:16px;">
					</div>
				</td>
				<td>
					<div class="ui icon input">
						<input type="button" name="CategoryDetail" value="라이브러리" class="ui button" style="width:140px; border:none; text-align:center; padding:10px; font-size:16px;">
					</div>
				</td>
				<td>
					<div class="ui icon input">
						<input type="button" name="CategoryDetail" value="DB" class="ui button" style="width:140px; border:none; text-align:center; padding:10px; font-size:16px;">
					</div>
				</td>
				<td>
					<div class="ui icon input">
						<input type="button" name="CategoryDetail" id="field" value="업무분야" class="ui button" style="width:140px; border:none; text-align:center; padding:10px; font-size:16px;">
					</div>
				</td>
				<td>
					<div class="ui icon input">
						<input type="button" name="CategoryDetail" id="tool" value="개발환경" class="ui button" style="width:150px; border:none; text-align:center; padding:10px; font-size:16px;">
					</div>
				</td>
			</tr>
		</table>
		<div id="detailKeywordArea"></div>
	</div>
	<div align="center" id="Search" style="margin-left: auto; margin-right: auto; margin-top: 10px;">
		<br>
		<div class="resume">
		<% if(rlist != null) { %>
			<div align="left" style="width:90%">
				<div>
					<p id="resumeCategory">추천 이력서</p>
				</div>
			</div>
			<div>
				<div class="swiper-container">
					<div class="swiper-wrapper">
						<%
							for (int i = 0; i < rlist.size(); i++) {
						%>
						<div class="swiper-slide" style="margin:0px; width:210px;">
							<form style="display: inline-block; width:200px; height:245px;">
								<div style="height: 15%">
									<% if(loginUser != null) { 
										if(loginUser.getUno() != rlist.get(i).getUno()) {%>
											<img class="singo" alt="" src="<%=request.getContextPath()%>/images/declaration.png" style="width:25px; height:30px; position:block; float:left; margin:5px;" onclick="singo('<%= rlist.get(i).getUserid() %>')">
										<% } %>
									<% }else { %>
											<img alt="" src="<%=request.getContextPath()%>/images/declaration.png" style="width: 25px; height: 30px; position: block; float: left; margin: 5px;" >
									<% } %>
								</div>
								<div>
									<% if(rlist.get(i).getPicture().getChangeName() == null) { %>
                             			<div id="profile" style="background-image: url(/info/images/profilePhoto.jpg);"></div>
                          			<% }else { %>
                             			<div id="profile" style="background-image: url(<%= request.getContextPath()%>/thumbnail_uploadFiles/<%=rlist.get(i).getPicture().getChangeName() %>);"></div> 
                         			<% } %>
								</div>
								<div align="center" style="height: 40%">
									<div class="name" style="margin-top: 3%; font-weight: bold;">
										<% for(int j = 0; j < rlist.get(i).getName().length(); j++) {
											if(j == 0) { %>
												<%= rlist.get(i).getName().charAt(j) %>
											<% }else { %>
												O
										 <% }} %>
									</div>
									<div class="title" style="font-size: x-small;">@<%=rlist.get(i).getUserid()%></div>
									<br>
									<div class="project" style="font-weight: bold"><%=rlist.get(i).getCount()%>개의 프로젝트</div>
									<hr style="background: lightgray; width: 60%;">
								</div>
								<div>
									<input type="button" value="더보기" class="ui button mini" style="position: block; background: #e0e1e2; color: rgba(0, 0, 0, .6); vertical-align: bottom;" onclick="location.href='<%= request.getContextPath()%>/selectOther.re?otherUno=<%= rlist.get(i).getUno()%>&ChangeName=<%= rlist.get(i).getPicture().getChangeName() %>'">
								</div>
							</form>
						</div>
						<% } %>
					</div>
					<div class="swiper-button-next"></div>
					<div class="swiper-button-prev"></div>
				</div>
			</div>
			<br>
			<% } %>
			<div class="resume">
				<div align="left" style="width:90%">
					<div>
						<p id="resumeCategory">다른 이력서</p>
					</div>
				</div>
				<div>
					<div class="swiper-container">
						<div class="swiper-wrapper">
							<%
								for (int k = 0; k < list.size(); k++) {
							%>
							<div class="swiper-slide" style="margin:0px; width:210px;">
								<form style="display: inline-block; width:200px; height:245px;">
									<div style="height: 15%">
										<% if(loginUser != null) { %>
											<img class="singo" alt="" src="<%=request.getContextPath()%>/images/declaration.png" style="width:25px; height:30px; position:block; float:left; margin:5px;" onclick="singo('<%= list.get(k).getUserid() %>')">
										<% }else { %>
											<img alt="" src="<%=request.getContextPath()%>/images/declaration.png" style="width: 25px; height: 30px; position: block; float: left; margin: 5px;" >
										<% } %>
									</div>
									<div>
										<% if(list.get(k).getPicture().getChangeName() == null) { %>
                             				<div id="profile" style="background-image: url(/info/images/profilePhoto.jpg);"></div>
                          				<% }else { %>
                           					<div id="profile" style="background-image: url(<%= request.getContextPath()%>/thumbnail_uploadFiles/<%=list.get(k).getPicture().getChangeName() %>);"></div> 
                           				<% } %>
									</div>
									<div align="center" style="height: 40%">
										<div class="name" style="margin-top: 3%; font-weight: bold;">
											<% for(int l = 0; l < list.get(k).getName().length(); l++) {
												if(l == 0) { %>
													<%= list.get(k).getName().charAt(l) %>
												<% }else { %>
													O
											 <% }} %>
										</div>
										<div class="title" style="font-size: x-small;">@<%=list.get(k).getUserid()%></div>
										<br>
										<div class="project" style="font-weight: bold"><%=list.get(k).getCount()%>개의 프로젝트</div>
										<hr style="background: lightgray; width: 60%;">
									</div>
									<div>
										<input type="button" value="더보기" class="ui button mini" style="position: block; background: #e0e1e2; color: rgba(0, 0, 0, .6); vertical-align: bottom;" onclick="location.href='<%= request.getContextPath()%>/selectOther.re?otherUno=<%= list.get(k).getUno()%>&ChangeName=<%= list.get(k).getPicture().getChangeName() %>'">
									</div>
								</form>
							</div>
							<% } %>
						</div>
						<div class="swiper-button-next"></div>
						<div class="swiper-button-prev"></div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="ui modal" style="width: 300px; height: 400px; text-align: center; vertical-align: middle;">
			<i class="close icon"></i>
			<div class="description">
				<table style="margin: 10px; margin-left: auto; margin-right: auto;">
					<tr align="left">
						<td><h2 style="color:red; margin-bottom:10px;">신고</h2></td>
					</tr>
					<tr align="left">
						<td><label style="font-weight: bold;">회원 아이디</label></td>
					</tr>
					<tr>
						<td align="center"><input type="text" name="userId" size="30" id="userId" style="margin-bottom:10px; border:none; font-size:16px;" disabled="disabled"></td>
					</tr>
					<tr align="left">
						<td><label style="font-weight: bold;">신고할 회원</label> <br></td>
					</tr>
					<tr style="margin-bottom:10px;">
						<td align="center"><input type="text" size="30" id="otherUserId" style="margin-bottom:10px; border:none; font-size:16px;" disabled="disabled"></td>
					</tr>
					<tr align="left">
						<td><label style="font-weight: bold;">제목 / 내용</label> <br></td>
					</tr>
					<tr style="margin-bottom:10px;">
						<td align="center"><textarea rows="8" id="textArea" cols="33" placeholder="신고 내용을 입력해주세요" style="margin-bottom:10px; resize:none;"></textarea></td>
					</tr>
				</table>
				<div align="center">
					<button class="ui button" style="font-size: 15px;" onclick="singoSubmit($('#userId').val(), $('#otherUserId').val(), $('#textArea').val())">등록하기</button>
					<button class="ui button" style="font-size: 15px;" onclick="close();">취소하기</button>
				</div>
			</div>
		</div>
	</div>
	
	<script>
		var num = 0;
			$(function() {
				$("#detailCategory").click(function() {
					$("#detailSearch").toggle();
				});
				
				$("input[name=CategoryDetail]").click(function(){
					
					if($(this).val() === '언어'){
						num = 1;
					}
					if($(this).val() === '모바일/OS'){
						num = 2;
					}
					if($(this).val() === '웹개발'){
						num = 3;
					}
					if($(this).val() === '프레임워크'){
						num = 4;
					}
					if($(this).val() === '라이브러리'){
						num = 5;
					}
					if($(this).val() === 'DB'){
						num = 6;
					}
					if($(this).val() === '업무분야'){
						num = 7;
					}
					if($(this).val() === 'Tool'){
						num = 8;
					}
					
					$.ajax({
						url : "/info/detailSearch.rs",
						type : "get",
						
						data : {
							button : num
						},
						success : function(data) {
							$detailKeywordArea = $("#detailKeywordArea");
							
							$detailKeywordArea.html('');
							var $table = $("<table>");
							var $tr = $("<tr>");
							var $td = $("<td>");
							$.each(data, function(index, value) {
								
								var $input = $("<input type='button' id='keyword' class='ui button' style='width:140px; border:none; text-align:center; padding:10px; font-size:13px;'>");
								$input.val(decodeURIComponent(value.KNAME));
								$td.append($input);
								
							});
							
							$tr.append($td);
							$table.append($tr);
							$detailKeywordArea.append($table);
						},
						error : function() {
							console.log("실패!");
						}
					});
				});
				
				$(document).on('click', "#keyword", function(){
					var key = $(this).val();
					location.href = "<%=request.getContextPath()%>/catekeywordSearch.re?key="+key;
				});
				
				$("#searchKeyword1").click(function(){
					var keyword = $(".searchKeyword").val();
					
					if(keyword == "") {
						alert("검색창에 검색어를 입력한 후 검색 하실 수 있습니다.");
					}else {
						location.href = "<%=request.getContextPath()%>/catekeywordSearch.re?keyword="+keyword;
					}
				});
				
				$("#searchKeyword2").click(function(){
					var keyword = $(".searchKeyword").val();
					
					if(keyword == "") {
						alert("검색창에 검색어를 입력한 후 검색 하실 수 있습니다.");
					}else {
						location.href = "<%=request.getContextPath()%>/catekeywordSearch.re?keyword="+keyword;
					}
				});
				
			});
			
			function singo(UserId) {
				console.log("신고");
				var id;
				<% if(loginUser != null) { %>
					id= '<%= loginUser.getUserId() %>';
				<% } %>
				var otherUserId = '@' + UserId;
				$("#userId").val(id);
				$("#otherUserId").val(otherUserId);
				$('.ui.modal').modal('show');
			}
			
			function close() {
				$('.ui.modal').modal('hide');
			}
			
			function singoSubmit(userId, otherUserId, textArea) {
				if(textArea == "") {
					alert('신고 내용을 입력해야 신고 등록이 가능합니다.');
				}else {
					location.href="<%= request.getContextPath()%>/personsingo.re?userId="+userId+"&otherUserId="+otherUserId+"&text="+textArea; 
				}
			}

			new Swiper('.swiper-container', {

				slidesPerView : 5, // 동시에 보여줄 슬라이드 갯수
				spaceBetween : 5, // 슬라이드간 간격
				slidesPerGroup : 5, // 그룹으로 묶을 수, slidesPerView 와 같은 값을 지정하는게 좋음

				// 그룹수가 맞지 않을 경우 빈칸으로 메우기
				// 3개가 나와야 되는데 1개만 있다면 2개는 빈칸으로 채워서 3개를 만듬
				loopFillGroupWithBlank : true,

				loop : true, // 무한 반복

				pagination : { // 페이징
					el : '.swiper-pagination',
					clickable : true, // 페이징을 클릭하면 해당 영역으로 이동, 필요시 지정해 줘야 기능 작동
				},
				navigation : { // 네비게이션
					nextEl : '.swiper-button-next', // 다음 버튼 클래스명
					prevEl : '.swiper-button-prev', // 이번 버튼 클래스명
				},
			});
		</script>
	<%@ include file="../common/ourfooter.jsp"%>
</body>
</html>