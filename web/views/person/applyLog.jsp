<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, resume.model.vo.*, keyword.model.vo.PageInfo"%>
<%
	ArrayList<ApplySituation> aslist = (ArrayList<ApplySituation>) request.getAttribute("aslist");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>지원현황</title>
<%@ include file="../common/import.html" %>
	<style>
		.outline{
			width:1000px;
			height:650px;
			margin-right:auto;
			margin-left:auto;
			/* border:1px solid black; */
		}
		.title{
			margin:30px;
			margin-left:20px;
			font-size:1.8em;
		}
		.mainBtn{
			width:30%;
			height:50px;
			margin:-2px;
			padding-top:12px;
			background:#e7e6e1;
			border:1px solid black;
			display:inline-block;
			text-align:center;
			vertical-align:middle;
			font-size:1.2em;
		}
		.mainBtn:hover{
			cursor:pointer;
		}
		.btnArea{
			text-align:center;
			
		}
		.tableArea{
			text-align:center;
			width:100%;
			margin-top:20px;
			height:500px;
			/* border:1px solid black; */
		}
		table{
			width:85%;
		}
		.tableArea th{
			font-size:1.5em;
			border-bottom:1px solid #e7e6e1;
		}
		th, td{
			padding:10px;
			font-size:1.2em;
		}
		td:nth-of-type(2):hover{
			text-decoration:underline;
			cursor:pointer;
		}
		
		.pagingArea{
			text-align:center;
			padding:20px;
			margin-bottom:20px;
			margin-top:10px;
		}
		.pagingbtn{
			height:30px;
			width:30px;
			display:inline-block;
			vertical-align:top;
			overflow:hidden;
			background:white;
			border:1px solid lightgray;
			border-radius:5px;
		}
		.pagingbtn:hover{
			cursor:pointer;
		}
		.pagingbtn:disabled{
			color:gray;
		}
	
	</style>
</head>
<body>
	<%@ include file="../common/mainMenu.jsp" %>
	
	<div class="outline">
		<div class="title">지원현황</div>
		<div class="btnArea">
			<div class="mainBtn" id="recruitTOT" onclick="recruitTOTFunction('search')">전체</div>
			<div class="mainBtn" id="recruitING">채용중</div>
			<div class="mainBtn" id="recruitEND" onclick="recruitENDFunction('search')">채용종료</div>
		</div>
		
		<div class="tableArea">
			<table align="center" id="ingTable">
				<tr>
					<th>지원날짜</th>
					<th style="width:300px;">제목</th>
					<th>기간</th>
					<th>열람여부</th>
				</tr>
				<%for(ApplySituation as : aslist){ %>
					<tr>
						<td><%=as.getApplyDate() %></td>
						<td class="goRec"><%=as.getRecTitle() %> <input type="hidden" id="recId" name="recId" value="<%=as.getRecId()%>"></td>
						<td><%=as.getRecStart() %> ~ <%=as.getRecFinish() %></td>
						<td><%=as.getStatus() %></td>
					</tr>
				<%} %>
			</table>
			<table align="center" id="endTable" style="display:none;">
				<thead>
					<tr>
						<th>지원날짜</th>
						<th style="width:300px;">제목</th>
						<th>기간</th>
						<th>열람여부</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			<table align="center" id="totTable" style="display:none;">
				<thead>
					<tr>
						<th>지원날짜</th>
						<th style="width:300px;">제목</th>
						<th>기간</th>
						<th>열람여부</th>
					</tr>
				</thead>
				<tbody>
				
				</tbody>
			</table>
		</div> <!-- tableArea End -->
		<div class="pagingArea" id="pagingArea1">
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.papp?currentPage=1'"><<</button>
			<%if(currentPage <=1) {%>
				<button class="pagingbtn" disabled></button>
			<%} else{ %>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.papp?currentPage=<%=currentPage-1%>'"><</button>
			<%} %>
			
			<%for(int p=startPage; p<=endPage; p++){
				if(p == currentPage){ %>
					<button class="pagingbtn" disabled><%=p %></button>
				<%} else{ %>
					<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.papp?currentPage=<%=p%>'"><%=p %></button>
				<%}
			  } %>
			
			<%if(currentPage >= maxPage){ %>
				<button class="pagingbtn" disabled></button>
			<%} else {%>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.papp?currentPage=<%=currentPage+1%>'">></button>
			<%} %>
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.papp?currentPage=<%=maxPage%>'">>></button>
		</div>
		<div class="pagingArea" id="pagingArea"></div>
	</div>
	
	<%@ include file="../common/ourfooter.jsp" %>
	<script>
		$(function(){
			//기본값
			$("#recruitING").css({"border-bottom":"white", "background":"white"});
			$("#recruitEND").css({"color":"gray", "border":"1px solid gray"});
			$("#recruitTOT").css({"color":"gray", "border":"1px solid gray"});
			
			
			$("#recruitING").click(function(){
				$(this).css({"border":"1px solid black", "border-bottom":"white", "background":"white", "color":"black"});
				$("#recruitEND").css({"border":"1px solid gray", "border-bottom":"1px solid black", "background":"#e7e6e1", "color":"gray"});
				$("#recruitTOT").css({"border":"1px solid gray", "border-bottom":"1px solid black", "background":"#e7e6e1", "color":"gray"});

				$("#pagingArea1").show();
				$("#pagingArea").hide();
				
				$("#ingTable").show();
				$("#endTable").hide();
				$("#totTable").hide();
				
			});
			
			$(".goRec").click(function(){
				var num = $(this).find("input[type=hidden]").val();
				
				location.href="<%=request.getContextPath()%>/recruit.so?type=1&RECID=" + num;
			});
		});
		function replaceAll(sValue, param1, param2) { // replaceAll 이라는 변수 선언.

			   return sValue.split(param1).join(param2);

		}
		
		
		function recruitENDFunction(num){
			var scurrentPage = 1;
			
			if(num == 'search'){
				scurrentPage=1;
			} else{
				if(num.innerText == '>>'){
					scurrentPage = $("#lastPage").val();
				} else{
					scurrentPage = num.innerText;
				}
			}
			
			$.ajax({
				url: "selectListTOT.papp",
				data : {
					currentPage:scurrentPage,
					category:"end"
				},
				type:"get",
				success:function(data){
					//console.log(data);
					
					$tableBody = $("#endTable tbody");
					$pagingArea = $("#pagingArea").html('');
					
					$tableBody.html('');
					
					var applyList = data.applyList;

					var scurrentPage = data.scurrentPage;
					var slistCount = data.slistCount;
					var smaxPage = data.smaxPage;
					var sstartPage = data.sstartPage;
					var sendPage = data.sendPage;
					
					$.each(applyList, function(index, value){
						var $tr = $("<tr align='center'>");
						var $applyDateTd = $("<td>").text(value.applyDate);
						var res = decodeURIComponent(value.recTitle);
						res = replaceAll(res, "+", " ");
						var $recTitleTd = $("<td class='goRec'>").text(res);
						var $recPeriodTd = $("<td>").text(value.recStart + " ~ " + value.recFinish);
						var $statusTd = $("<td>").text(value.status);
						var $recIdInput = $("<input type='hidden'>").val(value.recId);
						
						$recTitleTd.append($recIdInput);
						$tr.append($applyDateTd);
						$tr.append($recTitleTd);
						$tr.append($recPeriodTd);
						$tr.append($statusTd);
						
						$tableBody.append($tr);
					});
					
					//페이징 처리!
					var $pageDiv = new Array();
					var $button = new Array();
					var $input2 = new Array();
					
					if(scurrentPage != 1){
						$pageDiv1 = $("<div class='eachArea'>");
						$button1 = $("<button class='pagingbtn' onclick='recruitENDFunction(1)'>").text('<<');
						$pageDiv1.append($button1);
						$pagingArea.append($pageDiv1);
					} 
					
					for(var p=sstartPage; p<=sendPage; p++){
						if(p==scurrentPage){
							$pageDiv[p] = $("<div class='eachArea'>");
							$button[p] = $("<button class='pagingbtn' disabled>").text(p);
								
						} else{
							$pageDiv[p] = $("<div class='eachArea'>");
							$button[p] = $("<button class='pagingbtn' onclick='recruitENDFunction(this)'>").text(p);
							//$input2[p] = $("<input type='hidden' id=''>").val(p);
						}
						$pageDiv[p].append($button[p]).append($input2[p]);
						$pagingArea.append($pageDiv[p]);
					} 
					
					
					if(scurrentPage != sendPage){
						$pageDiv2 = $("<div class='eachArea'>");
						$button2 = $("<button class='pagingbtn' onclick='recruitENDFunction(this)'>").text('>>');
						$input3 = $("<input type='hidden' id='lastPage'>").val(sendPage);
						$pageDiv2.append($button2);
						$pageDiv2.append($input3);
						$pagingArea.append($pageDiv2);
					}
					
					
					
					
					$("#recruitEND").css({"border":"1px solid black", "border-bottom":"white", "background":"white", "color":"black"});
					$("#recruitING").css({"border":"1px solid gray", "border-bottom":"1px solid black", "background":"#e7e6e1", "color":"gray"});
					$("#recruitTOT").css({"border":"1px solid gray", "border-bottom":"1px solid black", "background":"#e7e6e1", "color":"gray"});

					$("#ingTable").hide();
					$("#endTable").show();
					$("#totTable").hide();

					$("#pagingArea1").hide();
					$("#pagingArea").show();
					
					$(".goRec").click(function(){
						var num = $(this).find("input[type=hidden]").val();
						
						location.href="<%=request.getContextPath()%>/recruit.so?type=1&RECID=" + num;
					});
				},
				error:function(data){
					console.log("에러!");
				}
			});
		}
		
		function recruitTOTFunction(num){
			var scurrentPage = 1;
			
			if(num == 'search'){
				scurrentPage=1;
			} else{
				if(num.innerText == '>>'){
					scurrentPage = $("#lastPage").val();
				} else{
					scurrentPage = num.innerText;
				}
			}
			
			$.ajax({
				url : "selectListTOT.papp",
				data : {
					currentPage:scurrentPage,
					category:"total"
				},
				type:"get",
				success:function(data){
					//console.log(data);
					
					$tableBody = $("#totTable tbody");
					$pagingArea = $("#pagingArea").html('');
					
					$tableBody.html('');
					
					var applyList = data.applyList;

					var scurrentPage = data.scurrentPage;
					var slistCount = data.slistCount;
					var smaxPage = data.smaxPage;
					var sstartPage = data.sstartPage;
					var sendPage = data.sendPage;
					
					$.each(applyList, function(index, value){
						var $tr = $("<tr align='center'>");
						var $applyDateTd = $("<td>").text(value.applyDate);
						var res = decodeURIComponent(value.recTitle);
						res = replaceAll(res, "+", " ");
						var $recTitleTd = $("<td>").text(res);
						var $recPeriodTd = $("<td>").text(value.recStart + " ~ " + value.recFinish);
						var $statusTd = $("<td>").text(value.status);
						var $recIdInput = $("<input type='hidden'>").val(value.recId);
						
						$recTitleTd.append($recIdInput);
						$tr.append($applyDateTd);
						$tr.append($recTitleTd);
						$tr.append($recPeriodTd);
						$tr.append($statusTd);
						
						$tableBody.append($tr);
						
						
					});
					
					//페이징 처리!
					var $pageDiv = new Array();
					var $button = new Array();
					var $input2 = new Array();
					
					if(scurrentPage != 1){
						$pageDiv1 = $("<div class='eachArea'>");
						$button1 = $("<button class='pagingbtn' onclick='recruitTOTFunction(1)'>").text('<<');
						$pageDiv1.append($button1);
						$pagingArea.append($pageDiv1);
					} 
					
					for(var p=sstartPage; p<=sendPage; p++){
						if(p==scurrentPage){
							$pageDiv[p] = $("<div class='eachArea'>");
							$button[p] = $("<button class='pagingbtn' disabled>").text(p);
								
						} else{
							$pageDiv[p] = $("<div class='eachArea'>");
							$button[p] = $("<button class='pagingbtn' onclick='recruitTOTFunction(this)'>").text(p);
							//$input2[p] = $("<input type='hidden' id=''>").val(p);
						}
						$pageDiv[p].append($button[p]).append($input2[p]);
						$pagingArea.append($pageDiv[p]);
					} 
					
					
					if(scurrentPage != sendPage){
						$pageDiv2 = $("<div class='eachArea'>");
						$button2 = $("<button class='pagingbtn' onclick='recruitTOTFunction(this)'>").text('>>');
						$input3 = $("<input type='hidden' id='lastPage'>").val(sendPage);
						$pageDiv2.append($button2);
						$pageDiv2.append($input3);
						$pagingArea.append($pageDiv2);
					}
					
					$("#recruitTOT").css({"border":"1px solid black", "border-bottom":"white", "background":"white", "color":"black"});
					$("#recruitING").css({"border":"1px solid gray", "border-bottom":"1px solid black", "background":"#e7e6e1", "color":"gray"});
					$("#recruitEND").css({"border":"1px solid gray", "border-bottom":"1px solid black", "background":"#e7e6e1", "color":"gray"});
					

					$("#ingTable").hide();
					$("#endTable").hide();
					$("#totTable").show();
					
					$("#pagingArea1").hide();
					$("#pagingArea").show();
					
				},
				error:function(request,status,error){
			        alert("code = "+ request.status + " message = " + request.responseText + " error = " + error); // 실패 시 처리
			    }
			});
			
		}
	</script>
</body>
</html>







