<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Fomantic Test</title>
<%@ include file="/views/common/import.html" %>
<style>
	.first{
		margin-right:auto;
		margin-left:auto;
		width:70%;
	}
	.detailInfo{
		margin:20px;
	}
	.pjt {
		color:gray;
		font-weight:bold;
		font-size:1.1em;
	}
	label{
		font-weight:bold;
		font-size:1.2em;
	}
	.tableArea td{
		height:60px;
		text-align:center;
	}
	.tableArea td:first-of-type{
		text-align:right;
		padding-right:10px;
	}
	.contentLabel{
		vertical-align:top;
		padding-top:10px;
	}
	textarea{
		border:1px solid lightgray;
		border-radius:6px;
	}
	textarea:active{
		border:1px solid skyblue;
	}
	
	@media screen and (max-width: 768px){
		.first{
			width:100%;
		}
		.sentence{
			display:none;
		}
		.name{
			font-size:2em;
		}
		.basic2>table{
			width:100%; text-align:center; margin-top:0;
		}
		.basic2 td:last-of-type{
			display:none;
		}
	}
	
	.pjt-photo{
		border:1px solid lightgray;
		display:inline-block;
		vertical-align:middle;
		width:20%;
		height:100px;
		color:white;
		border-radius:10px;
		margin-right:10px;
		margin-left:10px;
	}
	.pjt-photo:hover{
		cursor:pointer;
		background:lightgray;
	}
	.keywordArea{
		height:46px;
		width:100%;
		padding:5px;
		overflow:hidden;
	}
	.kbtnArea{
		display:inline-block;
		margin:2px;
	}
	
	.more{
		color:gray;
		border:1px solid #e7e6e1;
		border-bottom-left-radius:8px;
		border-bottom-right-radius:8px;
	}
	.more:hover{
		cursor:pointer;
		color:black;
		/* background:#e7e6e1; */
	}
	
	#thumbnailPhoto, #contentPhoto1, #contentPhoto2, #contentPhoto3{
		width:145px;
		height:98px;
		border-radius:10px;
		border:1px doted black;
	}
	
</style>
</head>
<body>
	<%@ include file="../common/mainMenu.jsp" %>

		<div class="two fields">
			<div class="field">
				<label>Start date</label>
				<div class="ui calendar" id="rangestart">
					<div class="ui input left icon">
						<i class="calendar icon"></i> <input type="text" placeholder="Start">
					</div>
				</div>
			</div>
			<div class="field">
				<label>End date</label>
				<div class="ui calendar" id="rangeend">
					<div class="ui input left icon">
						<i class="calendar icon"></i> <input type="text" placeholder="End">
					</div>
				</div>
			</div>
		</div>
	
	<script>
		$('#rangestart').calendar({
			  type: 'date',
			  endCalendar: $('#rangeend')
			});
			$('#rangeend').calendar({
			  type: 'date',
			  startCalendar: $('#rangestart')
		});
	</script>
</body>
</html>