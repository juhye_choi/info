<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, resume.model.vo.*"%>
<%
	HashMap<String, Object> hmap = (HashMap<String, Object>) request.getAttribute("hmap");
		int uno = (Integer) hmap.get("uno");
		int otherUno = (Integer) hmap.get("otherUno");
		String name = (String) hmap.get("name");
		String email = (String) hmap.get("email");
		String phone = (String) hmap.get("phone");
		String jobStatus = (String) hmap.get("jobStatus");
		Resume resume = (Resume) hmap.get("resume");
		int view = (Integer) hmap.get("view");
		String ChangeName = (String) hmap.get("ChangeName");
		String infoOpen = (String) request.getAttribute("infoOpen");
		String type = (String) request.getAttribute("type");
		
		ArrayList<Education> elist = resume.getElist();
		ArrayList<School> slist = resume.getSlist();
		ArrayList<Certification> celist = resume.getCelist();
		ArrayList<Career> calist = resume.getCalist();
		ArrayList<ProjectCareer> plist = resume.getPlist();
		ArrayList<HashMap<String, String>> khlist = resume.getKhlist();
		
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>이력서</title>
<style>
	.first{
		margin-right:auto;
		margin-left:auto;
		width:70%;
	}
	
	.space{
		height:180px;
		background:white;
	}
	.basicInfo{
		height:180px;
		width:70%;
		margin-right:auto;
		margin-left:auto;
		border-bottom:1px solid lightgray;
		padding:10px;
		padding-bottom:5px;
		padding-top:20px;
		position:fixed;
		background:white;
		z-index:80;
		
	}
	.basic1>img{
		width: 140px; height: 140px;
    	object-fit: cover;
   		object-position: top;
    	border-radius: 50%;
    	
	}
	.basic1{
		width:25%;
		height:100%;
		display:inline-block;
		text-align:right;
		overflow:hidden;
		
	}
	.basic23{
		display:inline-block;
		overflow:hidden;
		width:70%;
		height:100%;
	}
	.basic2{
		width:100%;
		height:60%;
		padding:10px;
	}
	.basic3{
		width:100%;
		height:30%;
		vertical-align:top;
		text-align:right;
	}
	.basic2>table{
		width:100%; text-align:center; margin-top:15px;	
	}
	.name{
		font-size:2.5em;
		font-weight:bold;
		margin:10px;
	}
	.sentence{
		color:gray;
		margin:10px;
	}
	.basicTable{
		width:600px;
	}
	.detailInfo{
		/* height:600px; */
		margin:10px;
		overflow:hidden;
		
	}
	.eduInfo{
		width:20%;
		height:100%;
		padding:10px;
		display:inline-block;
		background:#e7e6e1;
		overflow:hidden;
		vertical-align:top;
	}
	
	.projectInfo{
		width:65%;
		height:100%;
		padding:10px;
		display:inline-block;
		/* border:1px solid gray; */
		overflow:hidden;
		vertical-align:top;
	}
	.keywordInfo{
		width:14%;
		height:360px;
		padding:10px;
		display:inline-block;
		border:1px solid #e7e6e1;
		text-align:center;
		overflow:hidden;
		vertical-align:top;
		border-radius:10px;
	}
	.keywordArea{
		height:30px;
		margin-left:10px;
	}
	.keyword{
		/* display:inline-block; */
		height:20px;
		overflow:hidden;
		border:1px solid purple;
		border-radius:5px;
		color:purple;
		width:90%;
	}
	.keywordCnt{
		/* display:inline-block; */
		border-radius:10px;
		background:#e75a5a;
		width:18px;
		color:white;
		position:relative;
		top:11px;
		left:80%;
	}
	
	h4 {
		color:gray;
	}
	
	.leftDetail{
		margin:10px;
		margin-bottom:30px;
	}
	
	@media screen and (max-width: 768px){
		.first{
			width:100%;
		}
		.detailInfo{
			height:1500px;
		}
		.projectInfo{
			display:block;
			width:100%;
			height:40%;
		}
		.eduInfo{
			display:block;
			width:100%;
			height:30%;
			overflow:scroll;
		}
		.eduInfo::-webkit-scrollbar{
			display:none;
		}
		.keywordInfo{
			display:block;
			width:100%;
			height:20%;
			overflow:scroll;
		}
		.keywordInfo::-webkit-scrollbar{
			display:none;
		}
		
		.sentence{
			display:none;
		}
		.name{
			font-size:2em;
		}
		.basic2>table{
			width:100%; text-align:center; margin-top:0;
		}
		.basic2 td:last-of-type{
			display:none;
		}
		#transPDF{
			display:none;
		}
	}
	
	.pjtDetail{
		width:95%;
		height:100px;
		border:1px solid #e7e6e1;
		border-radius:10px;
		margin:20px;
		padding:5px;
	}
	.pjtAdd{
		width:95%;
		height:30px;
		border:1px solid #e7e6e1;
		border-radius:10px;
		margin:20px;
		text-align:center;
		padding-top:3px;
		color:white;
		background: lightgray;
	}
	
	.pjtDetail:hover, .pjtAdd:hover{ cursor:pointer; box-shadow: 3px 3px 20px #dddddd; }
	.date{ font-size:.8em; padding-left:10px; }
	.title{ font-weight:bold; }
	.explain{ font-size:.9em; color:gray; padding-left:10px; margin-bottom:10px; }
	
	.modalOuter{
		margin-left:auto;
		margin-right:auto;
		width:500px;
		height:200px;
		background:white;
		color:black;
		padding:20px;
		text-align:center;
	}
	
	.modalOuter2{
		margin-left:auto;
		margin-right:auto;
		width:500px;
		height:320px;
		background:white;
		color:black;
		padding:20px;
		text-align:center;
	}
	textarea{
		border:1px solid lightgray;
		border-radius:6px;
	}
	.buttonArea{
		padding:10px;
	}
	.pdfTable{
		width:400px;
		height:200px;
	}
	.pintro{
		display:inline-block;
		overflow:hidden;
		vertical-align:top;
	}
	.ptitle{
		padding-left:2px;
		font-size:1.2em;
		font-weight:bold;

	}
	.pdate{
		color:gray;
		padding:5px;

	}
	.pcontent{
		color:lightgray;
		padding-left:5px;
		overflow:hidden;
	}
	.pimg{
		width:88px;
		height:88px;
		border-radius:5px;
		border:1px solid gray;
		display:inline-block;
		overflow:hidden;
	}
	
</style>
<%@ include file="/views/common/import.html" %>
</head>
<body>
	<%@ include file="../common/mainMenu.jsp" %>
	<%if(loginUser == null){
		request.setAttribute("msg", "로그인이 필요한 메뉴입니다");
		request.getRequestDispatcher("../common/needLogin.jsp").forward(request, response);
	} %>
	<div class="first">
		<div class="basicInfo">
			<div class="basic1" id="photo">
				<% if(ChangeName == null) { %>
					<img src="<%=request.getContextPath()%>/images/profilePhoto.jpg">
				<% }else { %>
					<img src="<%= request.getContextPath()%>/thumbnail_uploadFiles/<%= ChangeName%>">
				<% } %>
			</div>
			<div class="basic23">
				<div class="basic3" id="buttonSection"></div>
				<div class="basic2" id="contact">
					<span class="name" id="name">
					<% for(int i = 0; i < name.length(); i++) {
						
						if(i == 0) { %>
							<%= name.charAt(i) %>
						<% }else { %>
							O
					 <%} } %>
					</span><span class="sentence" id="title"><%= resume.getrTitle() %></span>
					<table class="basicTable">
						<tr>
							<% if(infoOpen.equals("ALL")) { %> 
								<td><i class="icon mail"></i><span id="emailAddress"><%= email %></span></td>
								<td><i class="icon mobile alternate"></i><span id="phoneNumber">010-****-****</span></td>
								<td><i class="icon bookmark"></i><span id="status"><%= jobStatus %></span></td>
							<% }else if(infoOpen.equals("PART")) {  %>
								<td><i class="icon mail"></i><span id="emailAddress"><%= email %></span></td>
								<td><i class="icon mobile alternate"></i><span id="phoneNumber">010-****-****</span></td>
								<td><i class="icon bookmark"></i><span id="status">비공개</span></td>
							<% }else { %>
								<td><i class="icon mail"></i><span id="emailAddress">비공개</span></td>
								<td><i class="icon mobile alternate"></i><span id="phoneNumber">010-****-****</span></td>
								<td><i class="icon bookmark"></i><span id="status">비공개</span></td>
							<% }%>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="space"></div>
		<div class="detailInfo">
			<div class="eduInfo">
				<div class="leftDetail">
					<h4>자기소개</h4>
					<div><%=resume.getrIntro() %></div>
				</div>
				<div class="leftDetail">
						<h4>경력</h4>
					<%if(calist != null){
						for(Career c : calist){%>
							<div>
								<input type="hidden" name="career" value="<%=c.getCarId()%>" />
								<div id="title" class="title mtitle"><%=c.getCarName() %></div>
								<div id="date" class="date"><%=c.getCarStartDate() %> ~ <%=c.getCarFinishDate()%></div>
								<div id="explain" class="explain">
									<% if(c.getCarContent() != null) { %>
										<%=c.getCarContent() %>
									<% }else { %>
										미작성
									<% } %>
								</div>
							</div>
					<%}} %>
				</div>
				<div class="leftDetail">
						<h4>학력</h4>
					<%if(slist != null){
						for(School s : slist){ %>
						<div>
							<input type="hidden" name="school" value="<%=s.getSid()%>" />
							<div id="title" class="title stitle"><%=s.getsName() %></div>
							<div id="date" class="date"><%=s.getEnterDate() %> ~ <%=s.getGraduDate() %></div>
							<div id="explain" class="explain"><%=s.getMajor() %> 전공</div>
						</div>
					<%}} %>
				</div>
				<div class="leftDetail">
						<h4>교육</h4>
					<%if(elist != null){
						for(Education e : elist){%>
						<div>
							<input type="hidden" name="education" value="<%=e.getEid()%>" />
							<div id="title" class="title etitle"><%=e.geteName() %></div>
							<div id="date" class="date"><%=e.getEuStartDate() %> ~ <%=e.getEuFinishDate() %></div>
							<div id="explain" class="explain">
								<% if(e.getEuContent() != null) { %>
									<%=e.getEuContent() %>
								<% }else { %>
									미작성
								<% } %>
							</div>
						</div>
					<%}} %>
				</div>
				<div class="leftDetail">
					<h4>자격증</h4>
					<%if(celist != null){
					for(Certification ce : celist){ %>
					<div>
						<input type="hidden" name="certification" value="<%=ce.getcerId()%>"/>
						<div class="title ctitle"><%=ce.getCerName() %></div>
						<div id="date" class="date"><%=ce.getCertifyDate() %></div>
					</div>
					<%}} %>
				</div>
			</div>
			
			<div class="projectInfo">
				<h4>프로젝트 경력기술서</h4>
				<%if(plist != null){ 
					for(ProjectCareer pc : plist){%>
						<div class="pjtDetail" id="project1" onclick="pjtDetail('<%=otherUno%>', '<%= pc.getPjtId()%>', '<%= ChangeName%>')">
						<input type="hidden" name="project" value="<%=pc.getPjtId()%>">
						<%if(pc.getAtlist().get(0).getChangeName() != null){ %>
						<div class="pimg"><img src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=(pc.getAtlist()).get(0).getChangeName()%>" width="88px" height="88px"></div>
						<%} else{ %>
						<div class="pimg"></div>
						<%} %>
						<div class="pintro">
							<div class="ptitle"><%=pc.getPjtName() %></div>
							<div class="pdate"><%=pc.getPjtStart() %> ~ <%=pc.getPjtFinish()%> </div>
							<div class="pcontent"><%=pc.getMyRole() %></div>
						</div>
					</div>
				<%}} %>
			</div>
			
			<div class="ui modal v1" style="width:350px; height:170px; text-align:center; vertical-align:middle; padding:20px;">
				<i class="close icon"></i>
				<div class="description">
					<div align="center">
						<h3>결제 되지 않은 이력서 입니다.</h3>
						<h3>포인트 결제 후에 경력기술서를 볼 수 있습니다.</h3>
					</div><br>
					<div align="center">
						<button id="OK"style="width:140px; height:40px; background:red; color:white; font-size:15px; border:none;">확인</button>
					</div>
				</div>
			</div>
			
			<div class="ui modal v2" style="width:350px; height:170px; text-align:center; vertical-align:middle; padding:20px;">
				<i class="close icon"></i>
				<div class="description">
					<div align="center">
						<h3>이력서를 결제 하시겠습니까?</h3>
						<h4>(결제 시 포인트가 감소합니다)</h4>
					</div><br>
					<div align="center">
						<button id="pay" style="width:140px; height:40px; background:red; color:white; font-size:15px; border:none;">결제</button>
						<button style="width:140px; height:40px; background:blue; color:white; font-size:15px; border:none;" class="close">취소</button>
					</div>
				</div>
			</div>
	
			<div class="keywordInfo">
				<h4 style="margin:0px;">연관키워드</h4>
				<%if(khlist != null){
					int cnt = 10;
					if(cnt > khlist.size()){
						cnt = khlist.size();
					}
					
					for(int i=0; i < cnt; i++){%>
					<div class="keywordArea">
						<div class="keywordCnt"><%=khlist.get(i).get("count") %></div>
						<div class="keyword"><%=khlist.get(i).get("kname")%></div>
					</div>
				<%}} %>
				
			</div> <!-- 키워드 영역 끝! -->
		</div>
	</div> <!-- first Area End -->
	<script>
	var uno = 0;
	var OtherUno = 0;
	var pjtId = 0;
	
		function pjtDetail(otherUno, PjtId, ChangeName) {
			<%if(view==0){%>
				$('.ui.modal.v1').modal('show');
				OtherUno = otherUno;
				pjtId = PjtId;
			<%}else {%>
				location.href="<%=request.getContextPath()%>/otherRecruit.re?otherUno="+otherUno+"&PjtId="+PjtId+"&ChangeName="+ChangeName;
			<% } %>
		}
		
		$(function(){
			$("#OK").click(function(){
				$('.ui.modal.v1').modal('hide');
				$('.ui.modal.v2').modal('show');
			});
			
			$(".close").click(function(){
				$('.ui.modal.v2').modal('hide');
			});
			
			$("#pay").click(function(){
				location.href="<%=request.getContextPath()%>/personPay.re?OtherUno="+OtherUno+"&pjtId="+pjtId;
			});
		});
	</script>
	<%@ include file="../common/ourfooter.jsp" %>
	
</body>
</html>
