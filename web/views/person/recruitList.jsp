<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.ArrayList, keyword.model.vo.*, person.model.vo.*"%>
<%
	ArrayList<RecruitInfo> list = (ArrayList<RecruitInfo>) request.getAttribute("list");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
.rImg {
	width:280px;
	height:250px;
	border-radius:2%;
}

.rTitle {
	font-weight:900;
	font-size:20px;
	white-space: nowrap;
	padding:10px;
	padding-bottom:0;
	width:255px;
	height:40px;
	text-overflow:ellipsis;
	overflow:hidden;
}

.rCompanyName {
	color:gray;
	font-size:15px;
	padding-left:3px;
	padding-right:3px;
}

.rCategory {
	text-align:right;
	font-size:8px;
	color:gray;
	padding-left:3px;
	padding-right:3px;
}


</style>
</head>
<body>
<% if(list != null){ %>
<div style="margin-left:auto; margin-right:auto; width:1240px;">
<table>
	<tr>
			<% int i = 1;
				   for(RecruitInfo ri : list) { 
				   //System.out.println("이것은 : "+ ri);
				   %>
				<td width="300px" height="350px">
					<table class="pArea" onClick="location.href='<%=request.getContextPath()%>/recruit.so?RECID=<%=ri.getRecid() %>' +'&type=1'">
						<tr>	<!-- 공고 이미지 -->
							<td width="220px">
							<% if(ri.getLogo() != null) {%>
								<img src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=ri.getLogo() %>" class="rImg">
							<% } else { %>
								<img src="<%=request.getContextPath()%>/images/companyDefault.jpeg" class="rImg">
							<% } %>
							</td>
						</tr>
						<tr style="margin-top:10px">	<!-- 공고 제목 -->
							<td height="50px"><div  class="rTitle"><%=ri.getRec_title() %></div></td>
						</tr>
						<tr>	<!-- 회사명 -->
							<td class="rCompanyName"><div style="margin:10px; margin-bottom:30px;"><%=ri.getCompany_name() %></div></td>
						</tr>
						<tr>	<!-- 카테고리 -->
							<td class="rCategory">
								<div style="margin:3px;">
								<% if(ri.getKlist() != null){ for(Keyword k : ri.getKlist()) { %>
									<label class="ui gray mini label">&nbsp;<%=k.getKname()%></label>
								<% }
								} else { %> 
									<label class="ui gray mini label">키워드없음</label>
								<% } %> 
								</div>
							</td>
						</tr>
					</table>
				</td> <% if(i % 4 == 0) { %> </tr><tr> <% }  i++;
				} %>
	</tr>
</table>
</div>
<% } %>

</body>
</html>