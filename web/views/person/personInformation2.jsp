<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="person.model.vo.*, java.util.* , java.text.SimpleDateFormat"%>
	
	<%
	
	
	
	
	Person person = (Person)request.getAttribute("person");
	ArrayList<Attachment2> fileList = (ArrayList<Attachment2>) session.getAttribute("fileList");

	
	Date Birth = person.getBirth();
	
	String birthY="";
	String birthM = "";
	String birthD = "";
	
	if(Birth != null) {
	
	
	SimpleDateFormat afterFormat = new SimpleDateFormat("yyyyMMdd");
	String birth = afterFormat.format(Birth);
	
	 birthY = birth.substring(0,4);
	 birthM = birth.substring(4,6);
	 birthD = birth.substring(6,8);
	
	} else if(Birth == null){
		birthY = "";	
		 birthM = "";
		 birthD = "";
		
	} 
	
	
	
	
	%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>개인페이지</title>
<%@ include file="/views/common/import.html" %>
<style>
#allDiv {
	margin-left: 16%;
	margin-right: 16%;
	margin-top: 100px;
}

#subDiv1 {

display:inline-block;
width:300px;


}

#subDiv0 {

display:inline-block;


}


.subTitle {
	color: gray;
	font-size: 15px;
	font-weight: bold;
	margin-left: 10px;
}

.explain {
	font-size: 20px;
	font-weight: bolder;
}

.explainDiv {
	width:150px;
	vertical-align:top;
}



.sort {
	margin : 30px;
}


#td-vertical {
	vertical-align: top;
	padding-top: 5px;
}



hr {
	margin-top: 10px;
}

.inline {

display:inline-block;
}

#profileImgArea {
	width:300px;
	height:300px;
}

#profileImg {
	 width:200px;
	 height:200px;
	 margin:50px;
	 border:1px solid black;
}

#profileImgArea:hover {
cursor:pointer;

}


</style>
</head>
<body>
	<%@ include file="../common/mainMenu.jsp" %>
	<%
	
	String[] tel =new String[3];
	
	
	
	String phone = loginUser.getPhone();
	
	if(phone == null) {
		tel[0]="";
		tel[1]="";
		tel[2]="";
		
	}
	
	else if(phone.equals("--")) {
		tel[0]="";
		tel[1]="";
		tel[2]="";
		
		
	}else {
		 tel = phone.split("-");	
		
	}
	
	
	

	
	
	
	
	
	%>
	<form id="modifyForm" action="<%=request.getContextPath()%>/updatePerson.me" method="post" encType="multipart/form-data">
	

	<div id="allDiv">
	
	   <script>
   $(document).ready(function() {
      $("input:checkBox[name='wlsWk']").click( function() {
         if ($(this).prop('checked') == true) {
            $("#xkfxhl").removeClass("disabled");
            $("#xkfxhl").addClass("red");
            
            $("#xkfxhl").click(function(){
            	
            var check =	confirm("정말 탈퇴하시겠습니까?");
            	if(check == true) {
            		location.href = "<%=request.getContextPath()%>/deletePerson.ad?num=" + <%= loginUser.getUno() %>+ "&type=1";	
            		
            	}else if(check == false) {
            		
            		$("input:checkBox[name='wlsWk']").prop("checked",false);
            		$("#xkfxhl").addClass("disabled");
                    $("#xkfxhl").removeClass("red");
            		
            	}
            	
            	
            	
            	
            })
            
            
         } else {
            $("#xkfxhl").addClass("disabled");
            $("#xkfxhl").removeClass("red");
         }
      }); 
   });
   </script>
	
	
	<br>
	
	
	<label class="subTitle">개인정보</label>
	<hr>
	

	<div id="subDiv0" style="margin-bottom:100px;">
	
	<table class="sort">
	<tr>
	<td ><label class="explain">이름</label></td>
	<td style="padding-left:50px;"><div class="ui input"><input type="text" style="width:250px; color:black; background:lightgray;" id="name" name="name" value="<%=loginUser.getUserName()%>" readonly></div></td>
	<td><input type="hidden" name="uno" value="<%= loginUser.getUno() %>"></td>
	
	</tr>
	
	<tr>
						<td style="padding-top:20px;"><label class="explain">생년월일</label></td>
						<td style="padding-left:50px; padding-top:20px;"><div class="ui input">
								<input type="text" maxlength="4" name="birth1" size="4" id="birth1" style="width:70px; margin-right:5px;" value=<%=birthY %>>
							</div>/
							<div class="ui input"> 
								<input type="text" maxlength="2" name="birth2" size="2" id="birth2"  style="width:70px; margin-left:5px; margin-right:5px;" value=<%=birthM %>>
							</div>/
							<div class="ui input">
								<input type="text" maxlength="2" name="birth3" size="2" id="birth3"  style="width:70px; margin-left:5px;" value=<%=birthD %>>
							</div></td>
						<td></td>
					</tr>
	
	<tr >
	
	
	
	<tr>
						<td style="padding-top:20px;"><label class="explain">연락처</label></td>
						<td style="padding-left:50px; padding-top:20px;"><div class="ui input">
								<input type="text" maxlength="3" name="tel1" size="2" id="tel1" style="width:70px; margin-right:5px; "value=<%=tel[0] %>>
							</div>-
							<div class="ui input"> 
								<input type="text" maxlength="4" name="tel2" size="2" id="tel2"  style="width:70px; margin-left:5px; margin-right:5px;"value=<%=tel[1] %>>
							</div>-
							<div class="ui input">
								<input type="text" maxlength="4" name="tel3" size="2" id="tel3"  style="width:70px; margin-left:5px;"value=<%=tel[2] %>>
							</div></td>
						<td></td>
					</tr>
	
	<tr >
	<td style="padding-top:20px;"><label class="explain">성별</label></td>
	
	
	
	
	<%if(person.getGendder() == null){ %>
	<td style="padding-left:50px; padding-top:20px; width:100px;"><div class="ui radio checkbox"><input type="radio" name="gender" id="M" value="M" > <label for="M">남자</label></div>
	<div class="ui radio checkbox" style="padding-left:50px;"><input type="radio" name="gender" id="F" value="F" > <label for="F">여자</label></div></td>
<%}else if(person.getGendder().equals("F")){ %>
<td style="padding-left:50px; padding-top:20px; width:100px;"><div class="ui radio checkbox"><input type="radio" name="gender" id="M" value="M" > <label for="M">남자</label></div>
	<div class="ui radio checkbox" style="padding-left:50px;"><input type="radio" name="gender" id="F" value="F" checked > <label for="F">여자</label></div></td>


	<%} else if(person.getGendder().equals("M")){%>
	<td style="padding-left:50px; padding-top:20px; width:100px;"><div class="ui radio checkbox"><input type="radio" name="gender" id="M" value="M" checked> <label for="M">남자</label></div>
	<div class="ui radio checkbox" style="padding-left:50px;"><input type="radio" name="gender" id="F" value="F" > <label for="F">여자</label></div></td>
	
	<%} %>
	</tr>
	
	
		
	</table>
	
	
	
	</div>
	
	<div id="subDiv1" style="margin-left:100px;">

	<table  style="padding-bottom:100px;" >
	<tr>
	<td width="300px" align="center">
					<div id="profileImgArea">
					<%if(fileList.size() == 0) {%>
						<img id="profileImg" src="<%=request.getContextPath()%>/images/profilePhoto.jpg">
						<%}else { %>
						<img id="profileImg" src="<%=request.getContextPath()%>/thumbnail_uploadFiles/<%=fileList.get(0).getChangeName() %>">
						<%} %>
					</div>
					<input type="button" id="titleBtn" class="ui button gray" value="사진 업로드">
				</td>
	
	</tr>
	
	

	<tr>
	
	</tr>
	
	</table>
	
	
	</div>
	<br>
	
	
	
	<div id="subDiv2" class="inline">
	<label class="subTitle">정보 공개</label>
	<hr style="width:95%">
	
	
	<table class="sort">
	
	<tr>
	<td ><label class="explain">구직상태</label></td>
	<td style="padding-left:50px;">
			
			<%if(person.getJobStatus()==null){ %>
			<select class="ui dropdown" name="JobStatus" id="JobStatus" style="width:250px;">
  				<option value="EDU">재학중</option>
  				<option value="N">구직중</option>
  				<option value="Y">재직중</option>
  				
			</select>
			<%} else if(person.getJobStatus().equals("N")){ %>
			<select class="ui dropdown" name="JobStatus" id="JobStatus" style="width:250px;">
  				<option value="N">구직중</option>
  				<option value="EDU">재학중</option>
  				
  				<option value="Y">재직중</option>
  				
			</select>
			<%} else if(person.getJobStatus().equals("Y")) {%>
			<select class="ui dropdown" name="JobStatus" id="JobStatus" style="width:250px;">
  				<option value="Y">재직중</option>
  				<option value="N">구직중</option>
  				<option value="EDU">재학중</option>
  				
  				
  				
			</select>
			
			<%} else if(person.getJobStatus().equals("EDU")) {%>
			<select class="ui dropdown" name="JobStatus" id="JobStatus" style="width:250px;">
  				<option value="EDU">재학중</option>
  				<option value="N">구직중</option>
  				<option value="Y">재직중</option>
  				
			</select>
			
			
			<%} %>
			
</td>


	<td></td>
	
	</tr>
	
	<tr>
	<td style="padding-top:20px;"><label class="explain">GitHub</label></td>
	<td style="padding-left:50px; padding-top:20px;"">
			<div class="ui labeled input">
  				<div class="ui label">
    				http://
  				</div>
  				
  				<%if(person.getGithub() == null) { %>
  				<input type="text" name="github" id="github" value="">
  				<%} else {%>
  				<input type="text" name="github" id="github" value=<%=person.getGithub() %>>
  				
  				<%} %>
			</div>
	
	</td>
	
	</tr>
	
	<tr>
	<td style="padding-top:20px;"><label class="explain">블로그</label></td>
	<td style="padding-left:50px; padding-top:20px;"">
			<div class="ui labeled input">
  				<div class="ui label">
    				http://
  				</div>
  				<%if(person.getBlog() == null){ %>
  				<input type="text" name="blog" id="blog" value="">
  				<%}else { %>
  				<input type="text" name="blog" id="blog" value=<%= person.getBlog() %>>
  				<%} %>
			</div>
	
	</td>
	
	</tr>
	
	<tr>
	<td style="padding-top:20px;"><label class="explain">정보공개</label></td>
	<td style="padding-left:50px; padding-top:20px;">
			
			
	
	
	<% if(person.getInfoOpen()==null){ %>
	<select class="ui dropdown" id="publicInfo" name="publicInfo" style="width:250px;">
  				<option value="ALL">전부공개</option>
  				<option value="PART">일부공개</option>
  				<option value="NO">비공개</option>
			</select>
			
			<%}else if(person.getInfoOpen().equals("PART")){ %>
			
			<select class="ui dropdown" id="publicInfo" name="publicInfo" style="width:250px;">
  				<option value="PART">일부공개</option>
  				<option value="ALL">전부공개</option>
  				
  				<option value="NO">비공개</option>
			</select>
			
			
			
			
			<%} else if(person.getInfoOpen().equals("NO")) { %>
			<select class="ui dropdown" id="publicInfo" name="publicInfo" style="width:250px;">
  				<option value="NO">비공개</option>
  				<option value="PART">일부공개</option>
  				<option value="ALL">전부공개</option>
  				
  				
			</select>
			
			<%} else if(person.getInfoOpen().equals("ALL")) {%>
			<select class="ui dropdown" id="publicInfo" name="publicInfo" style="width:250px;">
  				<option value="ALL">전부공개</option>
  				<option value="PART">일부공개</option>
  				<option value="NO">비공개</option>
			</select>
			
			
			<%} %>
			</td>
	
	</tr>
	
	<tr >
	<td style="padding-top:20px;"><label class="explain">KOSA 보유여부</label></td>
	
	
	<%if(person.getKosaYN() ==null){ %>
	<td style="padding-left:50px; padding-top:20px; width:100px;"><div class="ui radio checkbox"><input type="radio" name="kosa" id="Y" value="Y"> <label for="Y">보유</label></div>
	<div class="ui radio checkbox" style="padding-left:50px;"><input type="radio" name="kosa" id="N" value="N" > <label for="N">미보유</label></div></td>
<%}else if(person.getKosaYN().equals("N")){ %>
	<td style="padding-left:50px; padding-top:20px; width:100px;"><div class="ui radio checkbox"><input type="radio" name="kosa" id="Y" value="Y" > <label for="Y">보유</label></div>
	<div class="ui radio checkbox" style="padding-left:50px;"><input type="radio" name="kosa" id="N" value="N" checked> <label for="N">미보유</label></div></td>
	
	<%} else if(person.getKosaYN().equals("Y")) {%>
	<td style="padding-left:50px; padding-top:20px; width:100px;"><div class="ui radio checkbox"><input type="radio" name="kosa" id="Y" value="Y" checked> <label for="Y">보유</label></div>
	<div class="ui radio checkbox" style="padding-left:50px;"><input type="radio" name="kosa" id="N" value="N" > <label for="N">미보유</label></div></td>
	
	<%} %>
	</tr>
	
	
	

	
	
		
	</table>
	</div>
	
	
	
	<div id="subDiv3" class="inline">
	
	
	<label class="subTitle">가입정보</label>
	<hr style="width:95%">
	<table class="sort">
	
	<tr>
	<td ><label class="explain">아이디</label></td>
	<td style="padding-left:50px;"><div class="ui input" style="color:black;"><input type="text" style="width:250px; color:black; background:lightgray;" value="<%= loginUser.getUserId() %>" readonly></div></td>
	<td></td>
	
	</tr>
	
	<tr>
	<td style="padding-top:20px;"><label class="explain">비밀번호 수정</label></td>
	<td style="padding-left:30px; padding-top:20px;"><div class="ui input"><input type="button" class="ui button blue" style="margin-left:20px;" onclick="modal()" value="비밀번호 변경"></div></td>
	<td></td>
	
	</tr>
	
	<tr>
		<td style="padding-top:20px;"><label class="explain">회원탈퇴</label></td>   
		<td style="padding-left:30px; padding-top:20px;"><input type="button" id="xkfxhl" class="ui disabled button" style="margin-left:20px;" value="탈퇴"></td>
	</tr>
	
	<tr>
		<td colspan="2">
			<label for="wlsWK">탈퇴하시겠습니까?<input type="checkbox" name="wlsWk" id="wlsWk" style="margin-left:10px;">네</label>
		</td>
	</tr>

	
	
		
	</table>
	</div>
	

	

	</div>
	
	
	
	
	<div class="btn" align="center" style="margin-top:50px;">
				 <input type="button" class="ui secondary button" onclick="submit();" value="제출하기">

				<input type="button" class="ui button" id="cancle" value="취소">
				
			</div>
			
			<div id="fileArea">
			<input type="file" id="thumbnailImg1" name="thumbnailImg1" onchange="loadImg(this)">
			
			</div>
	
	
	

	</form>
	
	 
	<div class="ui modal" style="width:600px; height:300px; text-align:center; vertical-align:middle; padding:20px;">
	
		<i class="close icon"></i>
		<div class="description">
			<div align="center">
				<h3>비밀번호 변경</h3>
				<form id="passSearchForm" action="<%=request.getContextPath()%>/passSearch2.me" method="post">
				<table style="margin-top:50px;">
					<tr>
						<td width="170px">변경할 비밀번호</td>
						<td width="300px"><div class="ui fluid input"><input type="password" id="userPwd" name="userPwd"></div></td>
					</tr>
					<tr >
						<td style="margin-top:20px;">변경할 비밀번호 확인</td>
						<td><div class="ui fluid input"><input type="password" id="userPwd2" name="userPwd2"></div></td>
					</tr>
					
					<tr>
					 <td><input type="hidden" id="type" name="type" value="2"></td>
					  <td><input type="hidden" name="uno" value="<%= loginUser.getUno() %>"></td>
					 
					</tr>
				</table>
					</form>
			</div><br>
			<div align="right" style="margin:20px;">
				<input type="button"  class="ui secondary button"  id="passSearch" value="변경">
				<input type="button" class="ui button gray" style="margin-left:10px;" value="취소" id="cancle">
			</div>
		</div>
	
	</div>
	
	
	
		

<script>

$("#cancle").click(function(){
	
	
	location.href="<%= request.getContextPath()%>/selectOne.po";
	
	
})

function loadImg(value) {
	
	if(value.files && value.files[0]) {
		
		var reader = new FileReader();
		
		reader.onload = function(e) {
			
			$("#profileImg").attr("src",e.target.result);
			
			
			
			
		}
		reader.readAsDataURL(value.files[0]);
		
		
	}
	
	
	
	
	
}




function modal(){
	$('.ui.modal').modal('show');
}


$("#userPwd2").focusout(function() {

	var pwd1 = $("#userPwd").val();
	var pwd2 = $("#userPwd2").val();

	if (pwd1 != "" || pwd2 != "") {

		if (pwd1 == pwd2) {
			$("#passSearch").click(function(){
				
				$("#passSearchForm").submit();
				
			})

		} else {

			alert("비밀번호가 동일한지 확인해주세요.");

		}

	}

})

$(function(){
	$("#fileArea").hide();
	
	
	
	
	$("#profileImgArea").click(function(){
		
		
		$("#thumbnailImg1").click();
		
		
		
		
		
	})
	
	$("#titleBtn").click(function(){
		
		
		
		$("#thumbnailImg1").click();
		
		
	})
	
	
	
	
	
	
})



</script>






	<br><br><br><br><br><br><br><br>

</body>
</html>