<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.ArrayList, keyword.model.vo.*, person.model.vo.*"%>
<%
	ArrayList<RecruitInfo> list = (ArrayList<RecruitInfo>) request.getAttribute("list");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>채용검색</title>
<%@ include file="/views/common/import.html" %>
<style>
.searchArea {
	padding-top:50px;
	padding-bottom:20px;
	padding-left:10%;
}

.slid {
	width:20px;
	height:80px;
	font-size:15px;
	font-weight:900;
}

.premiumArea {
	padding-top:10px;
	padding-bottom:30px;
}

.pArea {
	border-radius:2%;
	border:1px solid gray;
	margin:10px;
}

.pArea:hover {
	cursor:pointer;
}

.pImg {
	width:220px;
	height:200px;
	border-radius:2%;
}

.pTitle {
	font-weight:900;
	font-size:20px;
	margin-top:10px;
	white-space: nowrap;
	padding:5px;
	padding-bottom:0;
	width:220px;
	height:40px;
	text-overflow:ellipsis;
	overflow:hidden;
}

.pCompanyName {
	margin-top:10px;
	color:gray;
	font-size:13px;
}

.pCategory {
	text-align:right;
	font-size:6px;
	color:gray;
}

.rArea {
	border-radius:2%;
	border:1px solid gray;
	margin:20px;
}

.rImg {
	width:240px;
	height:190px;
	border-radius:2%;
}

.rLocation {
	color:gray;
	font-size:12px;
	padding-left:5px;
	padding-right:5px;
}

.rCategory {
	text-align:right;
	font-size:8px;
	color:gray;
	padding-left:5px;
	padding-right:5px;
}

.pageInfo {
	text-align:center;
	padding-top:50px;
	padding-bottom:100px;
}

#mainRecruit {
	margin-bottom:300px;
}

</style>
</head>
<body>
	<%@ include file="../common/mainMenu.jsp" %>
	<div class="searchArea">
		<table>
			<tr>
				<td>
					<div class="ui icon input">
  						<input type="text" placeholder="검색">
  						<i class="circular search link icon"></i>
					</div>
				</td>
				<td colspan="5">
					<input value="상세검색" class="ui button" style="width:150px;">
				</td>
			</tr>
		</table>
	</div>   <!-- 검색창 div -->
	
	<div class="premiumArea">
		
		<table style="margin-left:auto; margin-right:auto; width:1250px;">
			<tr>
				<td><h3>적극채용중인 기업</h3></td>
			</tr>
		</table>
		
		<table style="margin-left:auto; margin-right:auto;">
			<tr>
				<% int i = 1;
				   for(RecruitInfo ri : list) { %>
				<td width="220px">
					<table class="pArea" onClick="location.href='<%=request.getContextPath()%>/recruit.so?RECID=<%=ri.getRecid() %>&type=1'">
						<tr>	<!-- 공고 이미지 -->
							<td width="220px">
							<% if(ri.getLogo() != null) {%>
								<img src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=ri.getLogo() %>" class="pImg">
							<% } else { %>
								<img src="<%=request.getContextPath()%>/images/companyDefault.jpeg" class="pImg">
							<% } %>
							</td>
						</tr>
						<tr style="margin-top:10px">	<!-- 공고 제목 -->
							<td><div class="pTitle"><%=ri.getRec_title() %></div></td>
						</tr>
						<tr>	<!-- 회사명 -->
							<td class="pCompanyName"><div style="margin:10px; margin-bottom:30px;"><%=ri.getCompany_name() %></div></td>
						</tr>
						<tr>	<!-- 카테고리 -->
							<td class="pCategory">
								<div style="margin:3px;">
								<% for(Keyword k : ri.getKlist()) { %>
									<label class="ui gray mini label">&nbsp;<%=k.getKname()%></label>
								<% } %> 
								</div>
							</td>
						</tr>
					</table>
				</td> <% if(i % 5 == 0) { %> </tr><tr> <% }  i++;
				} %>
			</tr>
		</table>
	</div>	<!-- 적극채용 div -->
	
	<div id="mainRecruit">
	<table style="margin-left:auto; margin-right:auto; width:1250px;">
		<tr>
			<td><h3>전체 채용공고</h3></td>
		</tr>
	</table>
	</div>	<!-- 채용공고리스트 div -->
	<script>
	$(document).ready(function() {
		$.post("<%=request.getContextPath()%>/allRecruit?currentPage=1",
	            function(data)
	            {
	                if (data != "")
	                {
	               		$('#mainRecruit').append(data);
	                }
	            }
	        );
	});
	var pageLoaded = 2;
	$(window).scroll(function()
	{
		console.log("스크롤 : " + $(window).scrollTop());
		console.log("높이 : " + $(document).height());
		console.log("창 : " + $(window).height() * 2);
		
	    if($(window).scrollTop() == $(document).height() - $(window).height())
	    {
	        $.post("<%=request.getContextPath()%>/allRecruit?currentPage="+pageLoaded,
	            function(data)
	            {
	                if (data != "")
	                {
	               		$('#mainRecruit').append(data);
				        pageLoaded = pageLoaded+1;
	                }
	            }
	        );
	    }
	});
	</script>
</body>
</html>