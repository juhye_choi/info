<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/semantic/semantic.min.css">
<script src="<%=request.getContextPath()%>/semantic/semantic.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
body {
	text-align: center;
	padding-top: 5%;
	margin: auto;
}

.memberChange {
	width: 15%;
	height: 8%;
	margin: 3px;
	background: white;
	border: 2px solid black;
	font-size: 17px;
}

.memberChange:hover {
	cursor: pointer;
}

#personal {
	background: #CC33FF;
	color: white;
}

table {
	margin-top: 30px;
}

#allDiv {
	margin-left: 8%;
	margin-right: 8%;
	margin-top: 50px;
}

td {
	vertical-align: top;
	padding-top: 10px;
}

.searchSection {
	margin-top: 70px;
	padding-right: 20%;
	padding-left: 20%;
	/* padding-bottom:20px; */
	/*  border:1px solid black;  */
	/* border-top:2px solid black;  */
	text-align: center;
}
</style>
</head>
<body>


	<a href="<%=request.getContextPath()%>/index.jsp"><img
		src="<%=request.getContextPath()%>/images/logo.PNG" alt="메인로고"
		width="40%"></a>
	<br>
	<a
		href="<%=request.getContextPath()%>/views/person/personjoinForm.jsp"><button
			class="memberChange" id="personal">개인</button></a>
	<a
		href="<%=request.getContextPath()%>/views/company/companyjoinForm.jsp"><button
			class="memberChange" id="enterprise">기업</button></a>
	<br>

	<div>

		<form id="joinForm"
			action="<%=request.getContextPath()%>/pInsertMember.me" method="post" >


			<div style="margin-top: 10px; color: red;">*은 필수 항목 표시입니다.</div>


			<div id="allDiv">

				<hr>

				<table align="center">




					<tr>

						<td><label>* 아이디</label></td>
						<td style="width:300px;"><div class="ui input">
								<input type="text" name="userId" placeholder="" id="userId" style="width:250px;">
							</div></td>
						<td style="padding-left: 15px;"><label id="id-success"
							style="width: 150px; color: green;">사용가능한 아이디</label> <label
							id="id-danger" style="width: 150px; color: red;">사용불가 아이디</label></td>

					</tr>
					<tr>
				<td colspan="2"><span style="color:red; padding-left:100px;" id="idNum">6~12자의 소문자,숫자로만 사용가능합니다.</span></td>
				
				</tr>

					<tr>

						<td><label>* 비밀번호</label></td>
						<td style="width:300px;"><div class="ui input">
								<input type="password" id="userPwd" name="userPwd"
									placeholder="" style="width:250px;">
							</div></td>
						<td></td>

					</tr>

					<tr>

						<td><label>* 비밀번호확인</label></td>
						<td style="width:300px;"><div class="ui input">


								<input type="password" id="userPwd2" name="userPwd2"
									placeholder="" style="width:250px;">
							</div></td>

						<td style="padding-left: 15px;"><label id="alert-success"
							style="width: 150px; color: green;">비밀번호 일치</label> <label
							id="alert-danger" style="width: 150px; color: red;">비밀번호
								불일치</label></td>

					</tr>




					<tr>

						<td><label>* 이름</label></td>
						<td style="width:300px;"><div class="ui input">
								<input type="text" name="userName" placeholder="" id="userName"style="width:250px;">
							</div></td>
						<td></td>

					</tr>


					<!-- <tr>

				<td><label> 생년월일</label></td>
				<td><div class="ui input">
  					<input type="number" name="date" placeholder=""></div></td>
				<td></td>

			</tr> -->


					<tr>
						<td>연락처</td>
						<td><div class="ui input">
								<input type="text" maxlength="3" name="tel1" size="2" id="tel1" style="width:75px;">
							</div>-
							<div class="ui input"> 
								<input type="text" maxlength="4" name="tel2" size="2" id="tel2" style="width:78px;">
							</div>-
							<div class="ui input">
								<input type="text" maxlength="4" name="tel3" size="2" id="tel3" style="width:78px;">
							</div></td>
						<td></td>
					</tr>



					<tr>

						<td><label>* 이메일</label></td>
						<td style="width:300px;"><div class="ui input">
								<input type="email" name="email" placeholder="" id="email" style="width:250px;">
							</div></td>
							
						<td style="padding-left: 15px;"><label id="email-success"
							style="width: 150px; color: green;">사용가능한 이메일</label> <label
							id="email-danger" style="width: 150px; color: red;">사용불가 이메일</label></td>

					</tr>



					<tr>

						<td></td>
						<td style="width:300px;"><div class="ui input">
								<input type="text" name="emailCheck" placeholder="인증번호 확인" id="emailCheck" style="width:250px;" >
							</div></td>
							<td style="padding-left: 15px;"><input type="button"
							id="emailBtn" class="ui button" style="width: 130px;"
							value="인증번호 발송">
							
							<input type="button"
							id="emailSend" class="ui button" style="width: 130px; display:none"
							value="확인" >
							
							
							</td>
						<td><div class="countdown" style="color:red; padding-top:10px;" id="countdown"></div></td>


					</tr>
					
					<tr>
					
					<td><input type="hidden" readonly="readonly" name="code_check" id="code_check" value="<%= getRandom()%>"></td>
					
					</tr>
				

					<!-- 	<tr>
				<td><label>성별</label></td>
				<td><input type="radio" name="gender" value="M" id="male">
					<label for="male">남</label> <input type="radio" name="gender"
					value="F" id="female"> <label for="female">여</label></td>
			
			
			</tr> -->







				</table>


				<div class="searchSection">

					<h3>개인 회원 약관 (개정 및 적용 2019.12.04)</h3>



					<div
						style="overflow: scroll; overflow-x: hidden; width: 100%; height: 300px; text-align: left;">



						<br> <br> 제1조 (목적)<br>
						<br> 본 약관은 ㈜잇덕(이하 "회사")이 운영하는 웹사이트(이하 “사이트”)를 통해 인터넷 관련 서비스를
						제공함에 있어, 회사가 제공하는 서비스와 관련하여, 이를 이용하는 가입자(이하 “회원” 또는 “개인회원”)의 이용조건
						및 제반 절차, 기타 필요한 사항을 규정함을 목적으로 한다.<br> <br> 제2조 (개인회원 정보,
						이력서 노출)<br>
						<br> ① 개인회원의 이력서는 개인이 회원가입 또는 이력서 작성 및 수정시 희망한 형태로 이력서를 노출한다.<br>
						② 회사는 개인회원이 이력서의 공개/비공개 지정, 이력서상의 연락처 공개/비공개를 자유롭게 선택할 수 있도록 하여야
						한다.<br> ③ 회사는 개인회원이 이력서를 인재정보에 등록하기를 희망했을 경우 유료이력서 검색을 신청한
						기업회원이 열람 가능하도록 할 수 있다. 다만, 연락처 각 항목이 비공개로 지정된 경우 해당 항목별 연락처를 노출할 수
						없다.<br> <br> 제3조 (서비스의 요금)<br>
						<br> ① 개인회원 가입과 이력서 등록은 무료이다. <br> ② 회사는 유료서비스를 제공할 경우
						사이트에 요금에 대해서 공지를 하여야 한다.<br> ③ 회사는 유료서비스 이용금액이 변경되는 경우 변경된 금액
						적용일 최소 7일전(금액의 변경이 소비자에게 불리한 경우에는 30일전)부터 웹사이트 초기화면 공지사항 또는 이메일을
						통해 고지한다. 다만, 변경 이전에 적용 또는 계약한 금액은 소급하여 적용하지 아니한다.<br> <br>
						제4조 (서비스 이용시간) <br>
						<br> ① 회사는 특별한 사유가 없는 한 연중무휴, 1일 24시간 서비스를 제공한다. 다만, 회사는 서비스의
						종류나 성질에 따라 제공하는 서비스 중 일부에 대해서는 별도로 이용시간을 정할 수 있으며, 이 경우 회사는 그
						이용시간을 사전에 회원에게 공지 또는 통지하여야 한다.<br> ② 회사는 자료의 가공과 갱신을 위한 시스템
						작업시간, 장애해결을 위한 보수작업 시간, 정기 PM작업, 시스템 교체작업, 회선 장애 등이 발생한 경우 일시적으로
						서비스를 중단할 수 있으며 계획된 작업의 경우 이메일을 통해 서비스 중단 시간과 작업 내용을 알려야 한다.<br>
						<br> 제5조 (자료 내용의 책임과 회사의 정보 수정 권한)<br>
						<br> ① 자료내용은 회원이 등록한 개인정보 및 이력서와 사이트에 게시한 게시물을 말한다.<br>
						② 회원은 자료 내용 및 게시물을 사실에 근거하여 성실하게 작성해야 하며, 만일 자료의 내용이 사실이 아니거나
						부정확하게 작성되어 발생하는 모든 책임은 회원에게 있다.<br> ③ 모든 자료내용의 관리와 작성은 회원 본인이
						하는 것이 원칙이나 사정상 위탁 또는 대행관리를 하더라도 자료내용의 책임은 회원에게 있으며 회원은 주기적으로 자신의
						자료를 확인하여 항상 정확하게 관리가 되도록 노력해야 한다.<br> ④ 회사는 개인회원이 등록한 자료 내용에
						오자, 탈자 또는 사회적 통념에 어긋나는 문구가 있을 경우 이를 언제든지 수정할 수 있다.<br> ⑤
						개인회원이 등록한 자료로 인해 타인(또는 타법인)으로부터 허위사실 및 명예훼손 등으로 삭제 요청이 접수된 경우 회사는
						개인회원에게 사전 통지 없이 본 자료를 삭제할 수 있으며 삭제 후 메일 등의 방법으로 통지할 수 있다.<br>
						<br> 제6조 (회사의 의무)<br>
						<br> ① 회사는 본 약관에서 정한 바에 따라 계속적, 안정적으로 서비스를 제공할 수 있도록 최선의 노력을
						다해야 한다.<br> ② 회사는 서비스와 관련한 회원의 불만사항이 접수되는 경우 이를 즉시 처리하여야 하며,
						즉시 처리가 곤란한 경우에는 그 사유와 처리일정을 서비스 화면 또는 기타 방법을 통해 동 회원에게 통지하여야 한다.<br>
						③ 회사는 유료 결제와 관련한 결제 사항 정보를 상법 등 관련법령의 규정에 의하여 5년 이상 보존한다.<br>
						④ 천재지변 등 예측하지 못한 일이 발생하거나 시스템의 장애가 발생하여 서비스가 중단될 경우 이에 대한 손해에 대해서는
						회사가 책임을 지지 않는다. 다만 자료의 복구나 정상적인 서비스 지원이 되도록 최선을 다할 의무를 진다.<br>
						⑤ 회원의 자료를 본 서비스 이외의 목적으로 제3자에게 제공하거나 열람시킬 경우 반드시 회원의 동의를 얻어야 한다.<br>
						⑥ 회원이 등록한 자료로 인하여 사이트의 원활한 운영에 영향을 미친다고 판단될 시, 등록된 모든 자료를 회원의 사전
						동의 없이 삭제할 수 있다.<br> <br> 제 7조(회원의 의무)<br>
						<br> ① 회원은 관계법령과 본 약관의 규정 및 기타 회사가 통지하는 사항을 준수하여야 하며, 기타 회사의
						업무에 방해되는 행위를 해서는 안 된다.<br> ② 회원이 신청한 유료서비스는 등록 또는 신청과 동시에 회사와
						채권, 채무 관계가 발생하며, 회원은 이에 대한 요금을 지정한 기일 내에 납부하여야 한다.<br> ③ 회원이
						결제 수단으로 신용카드를 사용할 경우 비밀번호 등 정보 유실 방지는 회원 스스로 관리해야 한다. 단, 사이트의 결함에
						따른 정보유실의 발생에 대한 책임은 회원의 의무에 해당하지 아니한다.<br> ④ 회원은 서비스를 이용하여 얻은
						정보를 회사의 사전동의 없이 복사, 복제, 번역, 출판, 방송 기타의 방법으로 사용하거나 이를 타인에게 제공할 수
						없다.<br> ⑤ 회원은 본 서비스를 건전한 구인 구직 이외의 목적으로 사용해서는 안되며 이용 중 다음 각
						호의 행위를 해서는 안된다.<br> (1) 다른 회원의 아이디를 부정 사용하는 행위<br>
						<br> (2) 범죄행위를 목적으로 하거나 기타 범죄행위와 관련된 행위<br> (3) 타인의 명예를
						훼손하거나 모욕하는 행위<br> (4) 타인의 지적재산권 등의 권리를 침해하는 행위<br> (5)
						해킹행위 또는 바이러스의 유포 행위<br> (6) 타인의 의사에 반하여 광고성 정보 등 일정한 내용을
						계속적으로 전송하는 행위<br> (7) 서비스의 안정적인 운영에 지장을 주거나 줄 우려가 있다고 판단되는 행위<br>
						(8) 사이트의 정보 및 서비스를 이용한 영리 행위<br> (9) 그밖에 선량한 풍속, 기타 사회질서를
						해하거나 관계법령에 위반하는 행위<br> <br> 제 8조 (회원의 개인정보보호)<br>
						<br> 회사는 이용자의 개인정보보호를 위하여 노력해야 한다. 이용자의 개인정보보호에 관해서는
						정보통신망이용촉진 및 정보보호 등에 관한 법률에 따르고, 사이트에 "개인정보처리방침"을 고지한다.<br> <br>
						제9조 (신용정보의 제공 활용 동의)<br>
						<br> 회사가 회원가입과 관련하여 취득한 회원의 개인신용정보를 타인에게 제공하거나 활용하고자 할 때에는
						신용정보의 이용 및 보호에 관한 법률 제32조의 규정에 따라 사전에 그 사유 및 해당기관 또는 업체명 등을 밝히고 해당
						회원의 동의를 얻어야 한다.<br> <br> 제 10조(분쟁의 해결)<br>
						<br> ① 회사와 회원은 서비스와 관련하여 발생한 분쟁을 원만하게 해결하기 위하여 필요한 모든 노력을 하여야
						한다.<br> ② 전항의 노력에도 불구하고, 회사와 회원간에 발생한 전자거래 분쟁에 관한 소송은 제소 당시의
						회원의 주소에 의하고, 주소가 없는 경우에는 거소를 관할하는 지방 법원의 전속 관할로 한다. 다만, 제소 당시 회원의
						주소 또는 거소가 분명하지 아니 하거나, 외국 거주자의 경우에는 민사소송법상의 관할법원에 제기한다.<br>



					</div>


					<div class="ui checkbox" style="margin-top: 50px;">
						<input type="checkbox" id="agree" name="agreeCheck" value="동의" >
						<label for="agree">약관내용에 모두 동의합니다</label>
					</div>











				</div>



				<input type="button" class="ui secondary button" id="insertMember" style="margin-top: 20px;" value="회원가입">
					
					

			</div>





		</form>





<%! public int getRandom() {
		int random = 0;
		random = (int)Math.floor((Math.random() * (99999-10000+1))) + 10000;
		return random;
		
		}%>





	</div>
	
	


	<script>
	
	

		$("#emailSend").click(function(){
			
			var code = $("#code_check").val();
			
			var emailCheck = $("#emailCheck").val();
			
			if(code == emailCheck) {
				
				
				alert("이메일 인증이 완료되었습니다.");
				 $("#emailCheck").attr("disabled",true);
				 $('.countdown').remove();
				 
				
			}else {
				
				alert("인증번호가 틀렸습니다.");
				
				
				
			}
		
			
			
			
			
		})
		
		
		
	
	
	
	
	
		

		

		

		$(function() {
			
			//이메일 검사(adfg@dfds.dsf)
			//4글자 이상이 나오고 
	  		//@가 나오고 1글자 이상 주소 . 글자 1~3
			 var emailRegExp = /^\w{4,}@\w{1,}\.\w{3}$/;
			 
			//아이디 검사
		 	//첫글자는 반드시 영문 소문자, 총 6~12자로 이루어지고
		 	//숫자가 반드시 하나 이상 포함되어야 함
		 	//영문 소문자와 숫자로 이루어져야 한다.
		    var idRegExp = /[a-z]([a-z]|[0-9]{1,}){5,11}/;
			 
		  	
			 
			 var userId = document.getElementById('userId').value;
			 var pass = document.getElementById('userPwd').value;
			 var pass1 = document.getElementById('userPwd2').value;
			
	         var email = document.getElementById('email').value;
			
			 $("#emailCheck").attr("disabled",true);
			 $("#emailBtn").attr("disabled",true);
			
			 $("#idNum").hide();
			
			 
			$("#alert-success").hide();
			$("#alert-danger").hide();
			$("#id-success").hide();
			$("#id-danger").hide();
			$("#email-success").hide();
			$("#email-danger").hide();
			
			$("#userId").keyup(function(){
				
				 $("#idNum").show();
				
			})
			
			$("#userId").focusout(function() {

				var userId = $("#userId").val();

				$.ajax({

					url : "/info/idCheck.me",
					type : "post",
					data : {
						userId : userId
					},
					success : function(data) {

						if (data === "fail") {

							$("#id-success").hide();
							$("#id-danger").show();
							document.getElementById('userId').select();
						} else {

							if (userId == "") {
								$("#id-success").hide();
								$("#id-danger").hide();

							} else {
								//중복되진않는데 유효성검사 걸리면!
								if(!idRegExp.test(userId)){
									$("#id-success").hide();
									$("#id-danger").show();
						             document.getElementById('userId').select();
						            //중복되지도않고 유효성검사도 안걸리고! 
						          }else {
						        	  $("#id-success").show();
										$("#id-danger").hide();	  
						          }
								
							}

						}

					},
					error : function() {

						console.log("실패!");

					}

				});

			});
			
			
			
			
			
			$("#email").focusout(function() {

				var email = $("#email").val();
				

				$.ajax({

					url : "/info/emailCheck.me",
					type : "post",
					data : {
						email : email
					},
					success : function(data) {

						if (data === "fail") {

							$("#email-success").hide();
							$("#email-danger").show();
							 $("#emailCheck").attr("disabled",true);
							 $("#emailBtn").attr("disabled",true);
							  document.getElementById('email').select();
							
						} else {

							if (email == "") {
								$("#email-success").hide();
								$("#email-danger").hide();

							} else {
								
								//중복되진않지만 유효성검사에 걸리는 이메일
								if(!emailRegExp.test(email)){
									 $("#email-success").hide();
										$("#email-danger").show();
						             document.getElementById('email').select();
						            
						          }else {
						        	  $("#email-success").show();
										$("#email-danger").hide();
										 $("#emailCheck").attr("disabled",false);
										 $("#emailBtn").attr("disabled",false);	  
						          }
								
								
							
							}

						}

					},
					error : function() {

						console.log("실패!");

					}

				});

			});
			
		
			
			$("#emailBtn").click(function() {
				 var code = $("#code_check").val();
				 var email = $("#email").val();
				 
				 
				$("#emailCheck").attr("disabled",false);
				
				$("#emailBtn").css("display","none");
				$("#emailSend").css("display","block");
				$.ajax({
				
					
					 url : "/info/pJoinEmailSend",
					 type : "post",
					 data : {
						  code : code,
						  email : email
						},
						success : function(data) {
							
							
							
							
							
						},
						error : function() {
							console.log("실패!");
						}
					
				});
				
				/* $.ajax({
					  
					  var code = $("#code_check").val();
					  
					  url : "/info/send",
					  type : "post",
					  data : {
						  code : code
						},
						success : function(data) {
							
						},
						error : function() {
							console.log("실패!");
						}
					  
					  
				  }); */
				
				

				var timer2 = "3:00";
				var interval = setInterval(function() {


				  var timer = timer2.split(':');
				  //by parsing integer, I avoid all extra string processing
				  var minutes = parseInt(timer[0], 10);
				  var seconds = parseInt(timer[1], 10);
				  --seconds;
				  minutes = (seconds < 0) ? --minutes : minutes;
				  if (minutes < 0) clearInterval(interval);
				  seconds = (seconds < 0) ? 59 : seconds;
				  seconds = (seconds < 10) ? '0' + seconds : seconds;
				 
				  
				  if(minutes==0 && seconds==00) {
					  
					  $("#emailCheck").val("");
					 $("#emailCheck").attr("disabled",true);
					 $("#emailSend").css("display","none");
					 $("#emailBtn").css("display","block");
					 $("#emailBtn").attr("value","재발송");
					
					 clearInterval(interval);
					 
				  }
				  
			
				  $('.countdown').html(minutes + ':' + seconds);
				  timer2 = minutes + ':' + seconds;
				  
				 
				  
				  
				}, 1000);
				 

				

			});
			
			
			
		
			
			
			$("#insertMember").click(function(){
				console.log('실행');
				
				//이름 검사
		  		//2글자 이상, 한글만
		        var nameRegExp = /^[가-힣]{2,}$/g;
		        
		      	//전화번호 검사
		  		//전화번호 앞자리는 2~3자리 숫자
		  		//두번째자리는 3~4자리 숫자
		  		//세번째 자리는 4자리 숫자
		         var tel1RegExp = /[0-9]{2,3}/;
		         var tel2RegExp = /[0-9]{3,4}/;
		         var tel3RegExp = /[0-9]{4}/;
		         
		       
				 var tel1 = document.getElementById('tel1').value;
		         var tel2 = document.getElementById('tel2').value;
		         var tel3 = document.getElementById('tel3').value;

				
				var userId = $("#userId").val();
				var userPwd = $("#userPwd").val();
				var userPwd2 = $("#userPwd2").val();
				var userName = $("#userName").val();
				
				var email = $("#email").val();
				var emailCheck = $("#emailCheck").val();
				
				var chk1 = $("#agree").is(":checked");
				
				
				if(userId=="" || userPwd=="" ||userPwd2=="" ||  userName=="" || email=="" || emailCheck=="" || chk1== false) {
					
					alert("필수정보를 모두 입력해주세요.");
					
				}else {
					
					if(!nameRegExp.test(userName)){
			             alert("올바르지 않은 이름입니다.");
			             document.getElementById('userName').select();
			            
			          }else if(!tel1RegExp.test(tel1)) {
			        	  alert("올바르지 않은 연락처입니다.");
			              document.getElementById('tel1').select();
			          }else if(!tel2RegExp.test(tel2)) {
			        	  alert("올바르지 않은 연락처입니다.")
			              document.getElementById('tel2').select();
			          }else if(!tel3RegExp.test(tel3)) {
			        	  alert("올바르지 않은 연락처입니다.")
			              document.getElementById('tel3').select();
			          }else {

							console.log('제출');
							$("#joinForm").submit();  
			          }
			        
			          
					
				}
			});

			

			$("input[type=password]").keyup(function() {

				var pwd1 = $("#userPwd").val();
				var pwd2 = $("#userPwd2").val();

				if (pwd1 != "" || pwd2 != "") {

					if (pwd1 == pwd2) {
						$("#alert-success").show();

						$("#alert-danger").hide();

					} else {

						$("#alert-success").hide();
						$("#alert-danger").show();

					}

				}

			})
			
			
			
			
			
			
			
			
			
			
			
			
			

		})
		

		
		
		
		
		
	</script>








</body>
</html>