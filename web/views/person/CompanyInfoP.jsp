<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,resume.model.vo.* , company.model.vo.*"%>
<%
	Company c = (Company) request.getAttribute("company");
	ArrayList<Recruit> rList = (ArrayList<Recruit>) request.getAttribute("recList");
	int blacklist = (int) request.getAttribute("blacklist");
%>    
<!DOCTYPE html>
<html>
<head>
<style>
#mainDiv {
	margin-left:auto;
	margin-right:auto;
	margin-top:70px;
	width:1100px;
}

#topTitle {
	border-bottom:2px solid gray;
}

.title {
	font-size:50px;
	font-weight:bold;
	width:1000px;
}

.subTitle {
	font-size:30px;
	font-weight:bold;
	margin-bottom:20px;
}

.position {
	width:300px;
	height:200px;
}

.position:hover {
	cursor:pointer;
}

.logoImg {
	width:100px;
	height:100px;
}

.infoImg {
	width:200px;
	height:150px;
}

.intro {
	padding:10px;
}
pre {
	white-space: pre-line;
}
</style>
<meta charset="UTF-8">
<title>기업정보</title>
<%@ include file="/views/common/import.html" %>
</head>
<body>
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=158fc96dbe3058f905dcd07613a77957&libraries=services,clusterer,drawing"></script>
	<%@ include file="../common/mainMenu.jsp" %>
	<div id="mainDiv">
		<table id="topTitle">
			<tr>
				<td colspan = 2 align="right">
				<% if(loginUser != null) { 
					if(blacklist > 0) { %>
						<button class="ui button disabled">블랙리스트 추가</button>
					<% } else { %>
						<button class="ui button red" onclick="location.href='<%= request.getContextPath()%>/addBlack.ps?CUNO=<%=c.getUno() %>'">블랙리스트 추가</button>
					<% } %>
				<% } else { %>
					<button class="ui button disabled">블랙리스트 추가</button>
				<% } %>
				</td>
			</tr>
			<tr>
				<td width="100px;">
				<% if(c.getLogo() != null) { %>
					<img class="logoImg" src="<%= request.getContextPath() %>/attachment_uploadFiles/<%=c.getLogo() %>">
				<% } %>
				
				</td>
				<td class="title"><%=c.getCompanyName() %></td>
			</tr>
		</table>
		<table style="margin:30px;">
			<tr>
				<td width="700px;" style="vertical-align:top;">  					<!-- 채용중인 포지션 시작 -->
					<p class="subTitle">채용중인 공고</p>
					<table>
						<tr>
						<% 	int i = 0;
							if(rList.size() > 0) {
							for(Recruit r : rList) { %>
							<td>
								<div class="ui segment position" onClick="location.href='<%= request.getContextPath()%>/recruit.so?RECID=<%=r.getRecid() %>&type=1'">
 									<h3 class="ui floated header"><%=r.getRec_title() %></h3>
			 						<div class="ui clearing divider"></div>
			  						<p><%=r.getRec_finish() %> 까지</p>
								</div>
							</td>
						<% i++;
							if(i % 2 == 0) {%>
								</tr><tr>
							<% 	}
							} 	
						} %>
						</tr>
					</table>
				
				
				</td>									<!-- 채용중인 포지션 끝 -->
				
				<td width="300px" style="vertical-align: top;">						<!-- 우측 시작 -->
					<p class="subTitle">회사 정보</p>
					<table class="ui inverted grey table" style="text-align:center; font-size:17px; font-weight:bold;">
						<tr>
							<td width="130px" height="70px">기업대표</td>
							<td width="170px">
							<%if(c.getOwner() != null) { %>
								<%=c.getOwner() %> 
							<% } else { %>미입력<% } %></td>
						</tr>
					</table>
					<table class="ui inverted grey table" style="text-align:center; font-size:17px; font-weight:bold;">
						<tr>
							<td width="130px" height="70px">인사담당자</td>
							<td width="170px">
							<%if(c.getHrManager() != null) { %>
								<%=c.getHrManager() %> 
							<% } else { %>미입력<% } %></td>
						</tr>
					</table>
					<table class="ui inverted grey table" style="text-align:center; font-size:17px; font-weight:bold; margin-bottom:50px;">
						<tr>
							<td width="130px" height="70px">설립일</td>
							<td width="170px">
							<%if(c.getBirth() != null) { %>
								<%=c.getBirth() %> 
							<% } else { %>미입력<% } %></td>
						</tr>
					</table>
					
					<table style="font-weight:bold; color:gray;">
						<tr>
							<td>연락처</td>
							<td width="100px">
							<%if(c.getPhone() != null) { %>
								<%=c.getPhone() %> 
							<% } else { %>미입력<% } %>
							</td>
						</tr>
						<tr>
							<td>이메일</td>
							<td><%=c.getEmail() %></td>
						</tr>
					</table>
				</td>									<!-- 우측 끝 -->
			</tr>
			<tr>
				<td colspan="2">
					<table width="840px">
						<tr>
							<td style="padding-bottom:15px"><p class="subTitle">회사 소개</p></td>
						</tr>
						<% if(c.getAtlist() != null){ %>
						<tr>
							<% for(Attachment a : c.getAtlist()) { %>
							<td width="200px;"><img src="<%= request.getContextPath() %>/attachment_uploadFiles/<%=a.getChangeName() %>" class="infoImg"></td>
							<% } %>
						</tr>
						<% } %>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="intro">
					<div style="width:820px;">
						<pre><%if(c.getIntro() != null) { %>
								 <%=c.getIntro() %> 
							 <% } else { %>
							 	 미입력 
							 <% } %>
						</pre>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2"><div id="map" style="width:800px;height:400px;margin-top:20px"></div></td>
			</tr>
			<tr>
				<td><strong>주소 : </strong><%=c.getAddress() %></td>
			</tr>
		</table>
			
			<script>
			var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
		    mapOption = {
		        center: new kakao.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
		        level: 3 // 지도의 확대 레벨
		    };  

			// 지도를 생성합니다    
			var map = new kakao.maps.Map(mapContainer, mapOption); 
	
			// 주소-좌표 변환 객체를 생성합니다
			var geocoder = new kakao.maps.services.Geocoder();
	
			// 주소로 좌표를 검색합니다
			geocoder.addressSearch('<%=c.getAddress() %>', function(result, status) {
	
			    // 정상적으로 검색이 완료됐으면 
			     if (status === kakao.maps.services.Status.OK) {
	
			        var coords = new kakao.maps.LatLng(result[0].y, result[0].x);
	
			        // 결과값으로 받은 위치를 마커로 표시합니다
			        var marker = new kakao.maps.Marker({
			            map: map,
			            position: coords
			        });
	
			        // 인포윈도우로 장소에 대한 설명을 표시합니다
			        var infowindow = new kakao.maps.InfoWindow({
			            content: '<div style="width:150px;text-align:center;padding:6px 0;"><%=c.getCompanyName() %></div>'
			        });
			        infowindow.open(map, marker);
	
			        map.setCenter(coords);
			    } 
			});    
			</script>
	</div>
	<%@ include file="../common/ourfooter.jsp" %>		<!-- 하단 -->
				
</body>
</html>