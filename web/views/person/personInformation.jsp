<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<%@ include file="/views/common/import.html" %>

<style>
body{
		text-align:center;
		padding-top:5%;
		margin:auto;
	}
	
	.memberChange{
		width:15%;
		height:40px;
		margin:3px;
		background:white;
		border:2px solid black;
		font-size:17px;
	}
	
	#idCheck {
		background:#CC33FF;
		color:white;
	}
	
		#allDiv {
	margin-left: 6%;
	margin-right: 8%;
	margin-top: 30px;
}

		#div {
align:center;
margin-top: 50px;
	
	color:gray;
}

		#div2 {
	margin-left: 12%;
	margin-right: 8%;
	margin-top: 20px;
}




td {
	vertical-align: top;
	padding-top: 10px;
}



</style>
</head>
<body>

<%@ include file="../common/mainMenu.jsp" %>







<div id="div" >
<h3>기본정보 열람 및 수정하시려면 비밀번호를 입력해주세요.</h3>

</div>


<div id="allDiv">


<form id="personInfoForm" action="<%=request.getContextPath()%>/personInfo.me" method="post">

<table align="center">

<tr>
<td text-align="center">아이디</td>
<td><div class="ui input"><input type="text" id="userId" name="" value="<%=loginUser.getUserId() %>" style="background:lightgray;" readonly></div></td>

</tr>

<tr>
<td text-align="center">비밀번호</td>
<td><div class="ui input"><input type="password" name="userPwd" id="userPwd"></div></td>

</tr>


</table>

</form>

<div id="div2">

 
 <input type="button" class="ui secondary button" value="확인" id="personInfoCheck">


<a href="<%= request.getContextPath() %>/selectOne.po"> <button class="ui button">취소</button></a>



</div>


</div>


<script>



$("#personInfoCheck").click(function(){
	
	var userId = "<%=loginUser.getUserId() %>";
	var userPwd = $("#userPwd").val();
	
	
	
	$.ajax({
		
		url : "/info/passwordCheck.me",
		type : "post",
		data : {
			userId : userId,
			userPwd : userPwd
		},
		success : function(data) {
			if(data === "fail") {
				
				$("#personInfoForm").submit();
				
			}else {
				
				
				alert("비밀번호를 확인해주세요");
				
			}
			
			
		},
		error : function() {
			
		}
		
		
	})
	
	
	
	
	
	
	
	
})











</script>










</body>
</html>