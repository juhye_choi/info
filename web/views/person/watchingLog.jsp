<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, resume.model.vo.*"%>
<%
	ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>>) request.getAttribute("list");

	MorePageInfo mpi = (MorePageInfo) request.getAttribute("mpi");
	int listCount = mpi.getListCount();
	int currentPage = mpi.getCurrentPage();
	int maxPage = mpi.getMaxPage();
	
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>내가 본 공고</title>
<%@ include file="../common/import.html" %>
	<style>
		.outline{
			width:1000px;
			/* height:650px; */
			margin-right:auto;
			margin-left:auto;
		}
		.title{
			margin:30px;
			margin-left:20px;
			font-size:1.8em;
		}
		.tableArea{
			text-align:center;
			width:100%;
			height:450px;
			margin-top:20px;
			/* border:1px solid black; */
		}
		table{
			width:85%;
		}
		.tableArea th{
			font-size:1.5em;
			border-bottom:1px solid #e7e6e1;
		}
		th, td{
			padding:10px;
			font-size:1.2em;
		}
		td:nth-of-type(2):hover{
			text-decoration:underline;
			cursor:pointer;
		}
		
		.pagingArea{
			text-align:center;
			padding:20px;
			margin-bottom:10px;
		}
		.moreSee1{
			height:40px;
			color:gray;
			border:1px solid lightgray;
			padding:10px;
		}
		.moreSee2{
			height:40px;
			color:gray;
			border:1px solid gray;
			padding:10px;
		}
		.moreSee2:hover{
			cursor:pointer;
			color:black;
		}
	
	</style>
</head>
<body>
	<%@ include file="../common/mainMenu.jsp" %>

	<div class="outline">
		<div class="title">내가 본 공고</div>
		
		<div class="tableArea">
			<table align="center" id="ingTable">
				<tr>
					<th>본날짜</th>
					<th>제목</th>
					<th>채용기간</th>
					<th>상태</th>
				</tr>
				<%for(HashMap<String, String> hmap : list){ %>
				<tr>
					<td><%=hmap.get("watchDate") %></td>
					<td class="goRec"><%=hmap.get("recTitle") %>
						<input type="hidden" value='<%=hmap.get("recId") %>'>
					</td>
					<td><%=hmap.get("recStart") %> ~ <%=hmap.get("recFinish") %></td>
					<td><%=hmap.get("status")%></td>
				</tr>
				
				<%}%>
			</table>
		</div>
		<div class="pagingArea" id="pagingArea">
			<%if(currentPage >= maxPage){ %>
				<div class="moreSee1" id="moreSeeLast">더보기</div>
			<%} else{%>
				<div class="moreSee2" style="" onclick="location.href='<%=request.getContextPath()%>/selectList.seenr?currentPage=<%=currentPage+1%>'">더보기</div>
			<%} %>
		</div>
	</div>
	<script>
		$(function(){
			var pageSize = <%=currentPage%>;
			console.log(pageSize);
			
			$(".tableArea").css("height", 450*pageSize);
			
			$(".goRec").click(function(){
				var num = $(this).find("input[type=hidden]").val();
				
				
				
				location.href="<%=request.getContextPath()%>/recruit.so?type=1&RECID=" + num;
			});
		});
	</script>
	<%@ include file="../common/ourfooter.jsp" %>

</body>
</html>