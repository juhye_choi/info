<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*, person.model.vo.*"%>
<% 
	ArrayList<RecommendResume> list = (ArrayList<RecommendResume>) request.getAttribute("list");
	String Result = (String) request.getAttribute("Result");
	/* String count = (String) request.getAttribute("count"); */
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="/views/common/import.html"%>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
.outline {
	margin-right: auto;
	margin-left: auto;
	width: 80%;
	height: 800px;
	background: white;
	padding: 50px;
}

.headerArea {
	padding-top: 20px;
	padding-bottom: 20px;
}

.tableSection {
	padding-top: 20px;
}

.buttonStyle {
	margin-right: 5px !important;
	margin-left: 5px !important;
}

#logTable td:last-of-type {
	width: 180px;
}

h1 {
	margin-left: 11%;
}

.fields {
	padding-left: 20%;
}
div>p {
	color:grey;
	padding:3px;
	margin:0px;
	font-weight:bold;
}
#submit {
	background:#a333c8;
	font-size: 15px;
	color: white;
	height: 42px;
	width: 182px;
	border-radius: 15px; 
	position: relative;
}
</style>
</head>
<body>
	<%@ include file="../common/mainMenu.jsp" %>
	<div class="outline">
		<div class="headerArea">
			<h2 style="display:inline-block; margin:0;">추천 이력서 등록</h2>
			<% if(loginUser != null && Result == null) {%>
				<button class="ui button yellow Resumesubmit" style="width:100px; height:40px; border:none; display:inline-block; float:right; cursor:pointer; border-radius:10px;">신청하기</button>
			<% }else if(loginUser != null && Result.equals("ACC")) {%>
				<button class="Resumesubmit" style="width:100px; height:40px; border:none; display:inline-block; float:right; border-radius:10px; color:white; background:lightgray;" disabled='disabled'>등록중</button>
			<% }else if(loginUser != null && Result.equals("REQ")) {%>
				<button class="Resumesubmit" style="width:100px; height:40px; border:none; display:inline-block; float:right; border-radius:10px; color:white; background:lightgray;" disabled='disabled'>신청 심사중</button>
			<% }else if((loginUser != null && (Result.equals("END")) || (loginUser != null && Result.equals("DEN")))) { %>
				<button class="ui button yellow Resumesubmit" id="submit" style="width:100px; height:40px; border:none; display:inline-block; float:right; cursor:pointer; border-radius:10px; color:white;">신청하기</button>
			<% } %>
			<div style="width:730px; padding:25px;">
				<h3>추천이력서에 등록하시면, 채용의 기회가 증가합니다!</h3>
				<p>1.&nbsp;&nbsp;추천이력서를 신청하시면 심사 후 등록 혹은 반려됩니다.</p>
				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(반려될 경우, 사유를 확인하시면 다시 신청 가능합니다.)</p>
				<p>2.&nbsp;&nbsp;추천이력서 등록에 성공하시면 50포인트가 지급됩니다.</p>
				<p>3.&nbsp;&nbsp;1년에 4번(분기별) 심사 후 등록되며, 해당 분기가 끝나면 만료됩니다.</p>
			</div>
		</div>
		<hr>
		<div class="tableSection">
			<h2 style="margin:5px;">추천 이력서 등록내역</h2>
			<table class="ui sortable celled table" id="logTable" style="border-top:1px solid blue;">
				<thead align="center">
					<tr style="margin:10px;">
						<th>번호</th>
						<th>날짜</th>
						<th>카테고리</th>
						<th>사유</th>
						<th>기타</th>
					</tr>
				</thead>
				<tbody align="center">
					<% for(int i = 0; i < list.size(); i++) {%>
					<tr>
						<td><%= (i + 1) %></td>
						<td><%= list.get(i).getRefhidDate() %></td>
						<td>	
							<% if(list.get(i).getCategory().equals("REQ")) {%>
								추천이력서 등록 신청
							<% }else if(list.get(i).getCategory().equals("ACC")){%>
								추천이력서 등록에 성공!
							<% }else if(list.get(i).getCategory().equals("DEN")){ %>
								추천이력서 반려
							<% }else if(list.get(i).getCategory().equals("END")){ %>
								추천이력서 등록 만료
							<% }%>
						</td>
						<td>
							<% if(list.get(i).getCategory().equals("DEN")) { %>
								<%= list.get(i).getRefhisContent() %>
							<% } %>
						</td>
						<td>
							<% if(list.get(i).getCategory().equals("ACC")) { %>
								50포인트 지급
							<% } %>
						</td>
					</tr>
					<% } %>
				</tbody>
			</table>
		</div>
	</div>

	<div class="ui modal" style="width:350px; height:170px; text-align:center; vertical-align:middle; padding:25px;">
		<i class="close icon"></i>
		<div class="description">
			<div align="center">
				<h4>'추천 이력서'로 등록 신청하시겠습니까?</h4>
				<h4 style="margin:0px;">(관리자 승인 후 등록됩니다.)</h4>
			</div><br>
			<div align="center" style="margin">
				<button style="width:130px; height:40px; background:blue; color:white; font-size:15px; border:none; border-radius: 15px;" class="submit">신청</button>
				<button style="width:130px; height:40px; background:gray; color:white; font-size:15px; border:none; border-radius: 15px;" class="close">취소</button>
			</div>
		</div>
	</div>
	
	<script>
		$(function(){
			$("#logTable").tablesort();
			
			$(".close").click(function(){
				$('.ui.modal')
				  .modal('hide');
			});
			
			$(".Resumesubmit").click(function(){
				$('.ui.modal')
				  .modal('show');
			});
			$(".loginRequest").click(function(){
				alert("로그인이 필요한 화면입니다.");
			});
			$(".submit").click(function(){
				location.href="/info/insertRef.rg";
			});
		});
	</script>
	
	<%@ include file="../common/ourfooter.jsp" %>
</body>
</html>