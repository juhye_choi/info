<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, person.model.vo.*"%>
<%
	ArrayList<Attachment2> fileList = (ArrayList<Attachment2>) session.getAttribute("fileList");

%>
<style>
	.space{
		height:180px;
		background:white;
	}
	.basicInfo{
		height: 180px;
		width:70%;
		margin-right:auto;
		margin-left:auto;
		border-bottom:1px solid lightgray;
		padding:10px;
		padding-bottom:5px;
		padding-top:20px;
		position:fixed;
		background:white;
		z-index:80;
		
	}
	.basic1>img{
		width: 140px; height: 140px;
    	object-fit: cover;
   		object-position: top;
    	border-radius: 50%;
    	
	}
	.basic1{
		width:25%;
		height:100%;
		display:inline-block;
		text-align:right;
		overflow:hidden;
		
	}
	.basic23{
		display:inline-block;
		overflow:hidden;
		width:70%;
		height:100%;
	}
	.basic2{
		width:100%;
		height:60%;
		padding:10px;
	}
	.basic3{
		width:100%;
		height:30%;
		vertical-align:top;
		text-align:right;
	}
	.basic2>table{
		width:100%; text-align:center; margin-top:15px;	
	}
	.name{
		font-size:2.5em;
		font-weight:bold;
		margin:10px;
	}
	.sentence{
		color:gray;
		margin:10px;
	}
	.sentence:hover{
		cursor:pointer;
		text-decoration:underline;
	}
	.basicTable{
		width:600px;
	}
	
	@media screen and (max-width: 768px){
		.first{
			width:100%;
		}
		.detailInfo{
			height:1500px;
		}
		.projectInfo{
			display:block;
			width:100%;
			height:40%;
		}
		.eduInfo{
			display:block;
			width:100%;
			height:30%;
			overflow:scroll;
		}
		.eduInfo::-webkit-scrollbar{
			display:none;
		}
		.keywordInfo{
			display:block;
			width:100%;
			height:20%;
			overflow:scroll;
		}
		.keywordInfo::-webkit-scrollbar{
			display:none;
		}
		
		.sentence{
			display:none;
		}
		.name{
			font-size:2em;
		}
		.basic2>table{
			width:100%; text-align:center; margin-top:0;
		}
		.basic2 td:last-of-type{
			display:none;
		}
		#transPDF{
			display:none;
		}
		.basicTable{
			width:400px;
		}
		.basicInfo{
			width:100%;
		}
	}
</style>
		
	<!-- <h1 align="center">이력서</h1> -->
	<div class="basicInfo">
		<div class="basic1" id="photo">
			<%if(fileList.size() == 0) {%>
			<img class="personImg" src="<%=request.getContextPath()%>/images/profilePhoto.jpg"><br>
			<%} else { %>
			<img class="personImg" src=" <%=request.getContextPath()%>/thumbnail_uploadFiles/<%=fileList.get(0).getChangeName() %>"><br>
			
			<%} %>
		</div>
		<div class="basic23">
			<div class="basic3" id="buttonSection">
				<button class="ui purple button" id="finish" style="visibility:hidden;">작성완료</button>
				<button class="ui purple button" id="modify" style="display:none;">수정하기</button>
				<button class="ui purple basic button" id="transPDF" style="visibility:hidden;">PDF변환</button>
				<button class="ui purple basic button" id="cancel" style="display:none;">취소하기</button>
				<button class="ui black button" id="delete" style="display:none;">삭제하기</button>
			</div>
			<div class="basic2" id="contact">
				<form action="<%=request.getContextPath() %>/updateRtitle.re" method="post" id="rtitleForm">
				<input type="hidden" name="uno" id="unoInput">
				<span class="name" id="name">이름</span><span class="sentence" id="title"></span>
					<div class="ui input"><input type="text" id="titleInput" name="titleInput" style="height:30px; display:none" size="30"></div>
					<input type="button" class="ui button mini" id="titleModify" style="display:none;" value="수정">
					<input type="button" class="ui black button mini" id="titleModifyCancel" style="display:none;" value="취소">
				</form>
				<table class="basicTable">
					<tr>
						<td><i class="icon mail"></i><span id="emailAddress"></span></td>
						<td><i class="icon mobile alternate"></i><span id="phoneNumber"></span></td>
						<td><i class="icon bookmark"></i><span id="status">재직중</span></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div class="space"></div>
