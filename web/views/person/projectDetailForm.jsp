<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, keyword.model.vo.*"%>
<%
	ArrayList<Keyword> klist = (ArrayList<Keyword>) request.getAttribute("klist");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>이력서 작성</title>
<%@ include file="/views/common/import.html" %>
<style>
	.first{
		margin-right:auto;
		margin-left:auto;
		width:70%;
	}
	.detailInfo{
		margin:20px;
	}
	.pjt {
		color:gray;
		font-weight:bold;
		font-size:1.1em;
		height:50px;
		padding-top:20px;
	}
	label{
		font-weight:bold;
		font-size:1.2em;
	}
	.tableArea td{
		text-align:center;
	}
	.tableArea{
		border-spacing:2px 20px;
	}
	.contentLabel{
		vertical-align:top;
		padding-top:10px;
	}
	textarea{
		border:1px solid lightgray;
		border-radius:6px;
	}
	textarea:active{
		border:1px solid skyblue;
	}
	
	@media screen and (max-width: 768px){
		.first{
			width:100%;
		}
		.sentence{
			display:none;
		}
		.name{
			font-size:2em;
		}
		.basic2>table{
			width:100%; text-align:center; margin-top:0;
		}
		.basic2 td:last-of-type{
			display:none;
		}
	}
	
	.pjt-photo{
		border:1px solid lightgray;
		display:inline-block;
		vertical-align:middle;
		width:145px;
		height:100px;
		color:white;
		border-radius:10px;
		margin-right:5px;
		margin-left:5px;
	}
	.pjt-photo:hover{
		cursor:pointer;
		background:lightgray;
	}
	.keywordArea{
		height:46px;
		width:100%;
		padding:5px;
		overflow:hidden;
	}
	.kbtnArea{
		display:inline-block;
		margin:2px;
	}
	
	.more{
		color:gray;
		border:1px solid #e7e6e1;
		border-bottom-left-radius:8px;
		border-bottom-right-radius:8px;
	}
	.more:hover{
		cursor:pointer;
		color:black;
		/* background:#e7e6e1; */
	}
	
	#thumbnailPhoto, #contentPhoto1, #contentPhoto2, #contentPhoto3{
		width:145px;
		height:98px;
		border-radius:10px;
		border:1px doted black;
	}
	
</style>


</head>
<body>
	<%@ include file="../common/mainMenu.jsp" %>
	<div class="first">
		<div class="detailInfo">	
			<div class="pjt">프로젝트 경력기술서 추가</div>
			<hr>
			<form id="insertForm" action="<%=request.getContextPath() %>/insert.pjt" method="post" encType="multipart/form-data">
				<table class="tableArea" align="center">
					<tr>
						<td><label>프로젝트명 : </label></td>
						<td colspan="4"><div class="ui input"><input type="text" id="title" name="pjtName" size="80" placeholder="Project Title"></div></td>
					</tr>
					<tr>
						<td><label>내 역할 : </label></td>
						<td colspan="4"><div class="ui input"><input type="text" name="myRole" id="myRole" size="80" placeholder="My Role"></div></td>
					</tr>
					<tr>
						<td><label>참여기간 : </label></td>
						<td><div class="ui calendar" id="rangestart"><div class="ui input left icon">
								<i class="calendar icon"></i><input type="text" name="pstartDate" id="start" placeholder="Start Date" readonly></div></div></td>
						<td> ~ </td>
						<td><div class="ui calendar" id="rangeend"><div class="ui input left icon">
								<i class="calendar icon"></i><input type="text" name="pendDate" placeholder="End Date" readonly></div></div></td>
						<td>(기간 : )</td>
					</tr>
					<tr>
						<td class="contentLabel"><label>상세설명 : </label></td>
						<td colspan="4"><div class="ui form field" style="width:650px;"><textarea name="pContent" rows="20" style="resize:none;"></textarea></div></td>
					</tr>
					<tr>
						<td><label>관련링크 : </label></td>
						<td colspan="4"><div class="ui input"><input type="url" name="relatedLink" placeholder="http://" size="80"></div></td>
					</tr>
					<tr>
						<td><label>첨부파일 : </label></td>
						<td colspan="4"><input type="file" id="attachFile" name="attachFile" style="width:90%;" /></td>
					</tr>
					<tr>
						<td><label>관련사진 : </label></td>
						<td colspan="4">
							<div class="pjt-photo" id="thumbnailArea"><img id="thumbnailPhoto"></div>
							<div class="pjt-photo" id="contentArea1"><img id="contentPhoto1"></div>
							<div class="pjt-photo" id="contentArea2"><img id="contentPhoto2"></div>
							<div class="pjt-photo" id="contentArea3"><img id="contentPhoto3"></div>
							<input type="hidden" name="hiddenValue" id="hiddenValue">
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="4" id="fileArea">
							<input type="file" id="fileImg1" name="fileImg1" onchange="loadImg(this, 1);" accept="image/gif, image/jpeg, image/png">
							<input type="file" id="fileImg2" name="fileImg2" onchange="loadImg(this, 2);" accept="image/gif, image/jpeg, image/png">
							<input type="file" id="fileImg3" name="fileImg3" onchange="loadImg(this, 3);" accept="image/gif, image/jpeg, image/png">
							<input type="file" id="fileImg4" name="fileImg4" onchange="loadImg(this, 4);" accept="image/gif, image/jpeg, image/png">
						</td>
					</tr>
					<tr>
						<td style="vertical-align:top; padding-top:10px;"><label>키워드 추가 : </label></td>
						<td colspan="4">
							<div class="keywordArea" align="center">
							<%  int i = 1;
								for(Keyword k : klist){ %>
								<div class="kbtnArea">
								<label class="ui button kbtn">
								<input type="checkbox" name="keywords" value="<%=k.getKcode()%>" style="visibility:hidden; margin:0; padding:0; width:0;"><%=k.getKname() %></label>
								</div>
								<% if(i%6==0){%> <br> <% } i++;
								} %>
							</div>
							<div class="more"><i class="chevron down icon"></i></div>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="4" style="height:100px;">
							<button class="ui purple button" id="regist">등록하기</button>
							<button type="reset"class="ui basic purple button" id="cancel">등록취소</button>
						</td>
					</tr>
				</table>
			</form>
		</div><!-- detailInfo end -->
		
	</div>
	<%@ include file="../common/ourfooter.jsp" %>
	<script>
		var count = 0;
		var used = [false, false, false, false];
		
		
		
		function loadImg(value, num){
			if(value.files && value.files[0]){
				var reader = new FileReader();
				
				//console.log(value.files[0].size);
				fileNm = value.value;
				if (fileNm != "") {
				    var ext = fileNm.slice(fileNm.lastIndexOf(".") + 1).toLowerCase();
				    if (!(ext == "gif" || ext == "jpg" || ext == "png" || ext == "PNG" || ext == "JPG" || ext == "jpeg" || ext == "JPEG")) {
				        alert("이미지파일 (.jpg, .png, .gif ) 만 업로드 가능합니다.");
				        return false;
				    }
				}
				/* if(value.files[0].size >= 1024*1024*10){
					alert("최대 파일사이즈는 10Mbyte입니다.");
					return false;
				} */
				
				
				reader.onload = function(e){
					
					switch(num){
						case 1 : $("#thumbnailPhoto").attr("src", e.target.result); used[0] = true; break;
						case 2 : $("#contentPhoto1").attr("src", e.target.result); used[1] = true; break;
						case 3 : $("#contentPhoto2").attr("src", e.target.result); used[2] = true; break;
						case 4 : $("#contentPhoto3").attr("src", e.target.result); used[3] = true; break;
					}
				}
				reader.readAsDataURL(value.files[0]);
			}
		}
		$("#title").keydown(function(e){
			//console.log($(this).val());
			//console.log($(this).val().length);
			if($(this).val().length >= 50){
				alert("최대 글자수를 초과했습니다.");
			}
		});
		$("#myRole").keydown(function(e){
			//console.log($(this).val());
			//console.log($(this).val().length);
			if($(this).val().length >= 50){
				alert("최대 글자수를 초과했습니다.");
			}
		});
				
		
		
		$(function(){
			var more = 1;
			
			$("#fileArea").hide();
			//사진영역 클릭했을 때, 파일이 로드되는 것!
			$("#thumbnailArea").click(function(){
				$("#fileImg1").click();
			});
			$("#contentArea1").click(function(){
				$("#fileImg2").click();
			});
			$("#contentArea2").click(function(){
				$("#fileImg3").click();
			});
			$("#contentArea3").click(function(){
				$("#fileImg4").click();
			});
			
			$("input:checkBox[name='keywords']").click(function(){
				if($(':checkBox[name="keywords"]:checked').length > 10) {
					$(this).prop('checked', false);
					swal ("키워드 갯수 초과", "키워드는 최대 10개까지 가능합니다.", "error");
				} else if($(this).prop('checked') == true) {
					$(this).parent().addClass("orange");
				} else {
					$(this).parent().removeClass("orange");
				}
			});
			
			$('.more').click(function(){
				if(more == 1){
					$('.keywordArea').animate({
						'height':'500px',
						'duration':'1s'
					});
					$(this).html('<i class="chevron up icon"></i>');
					more = 0;
				} else{
					$('.keywordArea').animate({
						'height':'46px',
						'duration':'1s'
					})
					$(this).html('<i class="chevron down icon"></i>');
					more = 1;
					
				}
			});
			
			$("#regist").click(function(){
				for(var i=0; i<4; i++){
					if(used[i] == true){
						count++;
					}
				}
				$("#hiddenValue").val(count);
				console.log($("#hiddenValue").val());
				
				if($("#title").val() == ""){
					swal ({
						   title : "제목 빈칸은 불가합니다.",
						   icon : "error"
					}).then((value) => {
						$("#title").parent().addClass("error");
						$("#title").focus();
					});
					
					return false;
					
				} else if($("#myRole").val() == ""){
					swal ({
						   title : "내 역할 빈칸은 불가합니다.",
						   icon : "error"
					}).then((value) => {
						$("#myRole").parent().addClass("error");
						$("#myRole").focus();
					});
					return false;
					
				} else if($("#start").val() == ""){
					swal ({
						   title : "시작날짜 빈칸은 불가합니다.",
						   icon : "error"
					}).then((value) => {
						$("#start").parent().addClass("error");
						$("#start").focus();
					});
					return false;
					
				} else if($("#fileImg1").val()==""){
					alert("썸네일(대표사진)은 반드시 등록해야 합니다.");
					
					$("#thumbnailArea").css({
						"border":"1px solid #e25a5a",
						"background":"rgba(255,50,50,0.1)"
					});
					
					return false;
				} else if($(':checkBox[name="keywords"]:checked').length <= 0){
					alert("키워드를 한개 이상 선택해주세요.");
				    return false;
					
				}  else{
					
					if($("#fileImg1").val() == ""){
						$("#fileImg1").remove();
					} 
					if($("#fileImg2").val() == ""){
						$("#fileImg2").remove();
					}
					if($("#fileImg3").val() == ""){
						$("#fileImg3").remove();
					}
					if($("#fileImg4").val() == ""){
						$("#fileImg4").remove();
					} 
					if($("#attachFile").val() == ""){
						$("#attachFile").remove();
					}
					
					
					$("#insertForm").submit();
				}
				<%-- location.href="<%=request.getContextPath() %>/insert.pjt?count=" + count; --%>
			});
			
			$("#cancel").click(function(){
				location.href="<%=request.getContextPath()%>/selectOne.re";
			});
			
			$('#rangestart').calendar({
				type: 'date',
				formatter : {
					date: function(date, settings){
						if(!date) return '';
						var day = date.getDate();
						var month = date.getMonth() +1;
						var year = date.getFullYear();
						return year + '-' + month + '-' + day;
					}
				},
				endCalendar: $('#rangeend')
			});
			$('#rangeend').calendar({
				type: 'date',
				formatter : {
					date: function(date, settings){
						if(!date) return '';
						var day = date.getDate();
						var month = date.getMonth() +1;
						var year = date.getFullYear();
						return year + '-' + month + '-' + day;
					}
				},
				startCalendar: $('#rangestart')
			});
		});
	
	</script>
</body>
</html>