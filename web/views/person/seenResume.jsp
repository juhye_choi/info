<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>이력서</title>
<style>
	.first{
		margin-right:auto;
		margin-left:auto;
		width:80%;
	}
	.detailInfo{
		/* height:600px; */
		margin:10px;
		overflow:hidden;
		
	}
	.eduInfo{
		width:20%;
		height:100%;
		padding:10px;
		display:inline-block;
		background:#e7e6e1;
		overflow:hidden;
		vertical-align:top;
	}
	
	.projectInfo{
		width:65%;
		height:100%;
		padding:10px;
		display:inline-block;
		/* border:1px solid gray; */
		overflow:hidden;
		vertical-align:top;
	}
	.keywordInfo{
		width:14%;
		height:100%;
		padding:10px;
		display:inline-block;
		border:1px solid #e7e6e1;
		text-align:center;
		overflow:hidden;
		vertical-align:top;
		border-radius:10px;
	}
	.keyword{
		border:1px solid purple;
		border-radius:5px;
		margin:10px;
		margin-left:15px;
		margin-right:15px;
		color:purple;
	}
	h4 {
		color:gray;
	}
	
	.leftDetail{
		margin:10px;
		margin-bottom:30px;
	}
	
	@media screen and (max-width: 768px){
		.first{
			width:100%;
		}
		.detailInfo{
			height:1500px;
		}
		.projectInfo{
			display:block;
			width:100%;
			height:40%;
		}
		.eduInfo{
			display:block;
			width:100%;
			height:30%;
			overflow:scroll;
		}
		.eduInfo::-webkit-scrollbar{
			display:none;
		}
		.keywordInfo{
			display:block;
			width:100%;
			height:20%;
			overflow:scroll;
		}
		.keywordInfo::-webkit-scrollbar{
			display:none;
		}
		
		.sentence{
			display:none;
		}
		.name{
			font-size:2em;
		}
		.basic2>table{
			width:100%; text-align:center; margin-top:0;
		}
		.basic2 td:last-of-type{
			display:none;
		}
		#transPDF{
			display:none;
		}
	}
	
	.editBtn{
		float:right;
		background:#e7e6e1;
		border:1px solid #e7e6e1;
		cursor:pointer;
		border-radius:5px;
		width:30px;
	
	}
	.editBtn:hover{
		background-color:lightgray;
	}
	
	.pjtDetail{
		width:90%;
		height:100px;
		border:1px solid #e7e6e1;
		border-radius:10px;
		margin:20px;
		padding:10px;
	}
	
	.pjtDetail:hover{ background-color:#e7e6e1; cursor:pointer; }
	.date{ font-size:.8em; padding-left:10px; }
	.title{ font-weight:bold; }
	.explain{ font-size:.9em; color:gray; padding-left:10px; margin-bottom:10px; }
	
	.modalOuter{
		margin-left:auto;
		margin-right:auto;
		width:500px;
		height:200px;
		background:white;
		color:black;
		padding:20px;
		text-align:center;
	}
	
	.modalOuter2{
		margin-left:auto;
		margin-right:auto;
		width:500px;
		height:320px;
		background:white;
		color:black;
		padding:20px;
		text-align:center;
	}
	textarea{
		border:1px solid lightgray;
		border-radius:6px;
	}
	.buttonArea{
		padding:10px;
	}
	
</style>
<%@ include file="/views/common/import.html" %>
</head>
<body>
	<%@ include file="../common/mainMenu.jsp" %>
	<!-- <h1 align="center">이력서</h1> -->
	<div class="first">
		<%@ include file="resumeCommon.jsp" %>
		<div class="detailInfo">
			<div class="eduInfo">
				<div class="leftDetail">
					<h4>자기소개</h4>
					<div>안녕하세요.<br>
						저는 10년차 백앤드 개발자입니다.<br>
						어디서든 최선을 다하겠습니다!
					</div>
				</div>
				<div class="leftDetail">
					<h4>경력</h4>
					<div class="title">가가오뱅크(GaGao Bank)</div>
					<div class="date">2014-10-10 ~ 현재</div>
					<div class="explain">웹서버 백앤드 개발자 / 팀장</div>
					
					<div class="title">KH아이티</div>
					<div class="date">2012-10-10 ~ 2014-08-01</div>
					<div class="explain">프론트앤드 개발자 / PM</div>
					
					<div class="title">네이뻐 (Naybber)</div>
					<div class="date">2010-07-09 ~ 2012-10-01</div>
					<div class="explain">프론트앤드 개발자 / 사원</div>
				</div>
				<div class="leftDetail">
					<h4>학력</h4>
					<div class="title">유명대학교</div>
					<div class="date">2006-03-01 ~ 2010-08-14</div>
					<div class="explain">정보통신공학과 전공</div>
				</div>
				<div class="leftDetail">
					<h4>교육</h4>
					<div class="title">KH정보교육원</div>
					<div class="date">2010-01-01 ~ 2010-08-14</div>
					<div class="explain">JAVA 웹개발 과정</div>
				</div>
				<div class="leftDetail">
					<h4>자격증</h4>
					<div class="title">정보처리기사</div>
					<div class="date">2010.08.08</div>
				</div>
			</div>
			
			

			<div class="projectInfo">
				<h4>프로젝트 경력기술서</h4>
				<div class="pjtDetail" id="project1">프로젝트1</div>
				<div class="pjtDetail">프로젝트2</div>
				<div class="pjtDetail">프로젝트3</div>
				<div class="pjtDetail">프로젝트4</div>
			</div>
			

			<div class="keywordInfo">
				<h4>연관키워드</h4>
				<div class="keyword">JAVA</div>
				<div class="keyword">Eclipse</div>
				<div class="keyword">Oracle</div>
				<div class="keyword">JQL Developer</div>
				<div class="keyword">HTML5</div>
				<div class="keyword">CSS3</div>
				<div class="keyword">JavaScript</div>
				<div class="keyword">JQuery</div>
				<div class="keyword">JDBC</div>
				<div class="keyword">Servlet</div>
				<div class="keyword">JSP</div>
				
				
			</div>
		</div>
	</div> <!-- first Area End -->
	
	
	
	<%@ include file="../common/ourfooter.jsp" %>
</body>
</html>