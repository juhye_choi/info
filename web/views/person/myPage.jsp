<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="person.model.vo.*, java.util.*"%>
    
    <%
    
    ArrayList<Attachment2> fileList = (ArrayList<Attachment2>) session.getAttribute("fileList");
   	HashMap<String, Integer> ihmap = (HashMap<String, Integer>) request.getAttribute("ihmap");
    %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>마이페이지</title>
<%@ include file="/views/common/import.html" %>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<style>
	td{
		padding:10px;
		font-size:1.3em;
	}
	.firstposition {
		margin-left:auto;
		margin-right:auto;
		width:1000px;
		height:600px;
	}
	.secondposition {
		padding-right:200px;
		padding-left:200px;
		height:150px;
		text-align:right;
		/* border:1px solid black; */
	}
	.resume {
		width:320px;
		height:450px;
		text-align:center;
		border:1px solid gray;
		border-radius:20px;
		display:inline-block;
		overflow:hidden;
		vertical-align:top;
		margin-right:50px;
		margin-bottom:50px;
		margin-top:50px;
		padding:20px;
	}
	.siteInfo {
		width: 62%;
		height: 450px;
		display: inline-block;
		overflow:hidden;
		vertical-align:top;
		margin-top:50px;
	}
	.graphInfo {
		height: 60%;
		border-bottom:1px solid lightgray;
	}
	.checkInfo {
		height: 40%;
		padding: 20px;
	}
	
	.personImg {
		width: 160px; height: 160px;
    	object-fit: cover;
   		object-position: top;
    	border-radius: 50%;
    	margin-top:20px;
	}
	.prjNum {
		font-size:20px;
		margin-top:40px;
		margin-bottom:20px;
		padding-bottom:20px;
		border-bottom:1px solid lightgray;
	}
	.idTag{
		color:gray;
		margin:-10px;
	}
	
	.mybutton {
		width:120px;
		height:50px;
	}
	#donut_single{
		width: 180px;
   		height: 180px;
		text-align: center;
	}
	#check{
		font-size:0.9em;
	}
	.checkTable{
		width:600px;
	}
	@media screen and (max-width: 768px){
		.siteInfo{
			display:none;
		}
		.resume{
			margin-left:80px;
		}
		.secondposition{
			padding:100px;
		}
	}
</style>
<script>
</script>
</head>
<body>
	<%@ include file="../common/mainMenu.jsp" %>
	<%if(loginUser == null){
		request.setAttribute("msg", "로그인이 필요한 메뉴입니다");
		request.getRequestDispatcher("../common/needLogin.jsp").forward(request, response);
	} %>
	<div class="firstposition">
		<div class="resume">
			<%if(fileList.size() == 0) {%>
			<img class="personImg" src="<%=request.getContextPath()%>/images/profilePhoto.jpg"><br>
			<%} else { %>
			<img class="personImg" src=" <%=request.getContextPath()%>/thumbnail_uploadFiles/<%=fileList.get(0).getChangeName() %>"><br>
			
			<%} %>
			<% if(loginUser == null){ %>
			<h2>이름</h2>
			<%} else{%>
			<h2><%= loginUser.getUserName() %></h2>
			<%} %>
			<div class="idTag"><%=loginUser.getEmail() %></div>
			<div class="prjNum"><%=ihmap.get("pjtNum") %>개의 프로젝트</div>
			<button class="ui button big" style="margin-right:30px;" onclick="seeResume()">보 기</button>
			<button class="ui button big" onclick="goResume();">수 정</button>
		</div>
		<div class="siteInfo">
			<div class="graphInfo">
				<table align="center">
					<tr align="center" style="height: 180px;">
						<td style="width: 200px;"><div id="donut_single"></div></td>
						<td style="width: 200px;"><div id="donut_single2" style="width: 180px; height: 180px; padding:0;"></div></td>
						<td style="width: 200px;"><div id="donut_single3" style="width: 180px; height: 180px; padding:0;"></div></td>
					</tr>
					<tr align="center">
						<td id="completeRate"></td>
						<td id="pointCount">포인트 <%=ihmap.get("point") %>p</td>
						<td>뱃지수 <%=ihmap.get("refAcc") %>개</td>
					</tr>
				</table>
			</div>
			<div class="checkInfo">
				<h3>자신의 이력을 주기적으로 업데이트하여, 취직/이직에 성공하세요!</h3>
				<table align="center" class="checkTable">
					<tr>
						<td><input type="checkbox" name="checkinfo" id="c1" value="c1" disabled> <label for="c1" id="check1">기본정보 입력</label></td>
						<td><input type="checkbox" name="checkinfo" id="c2" value="c2" disabled> <label for="c2" id="check2">자기소개, 교육, 자격증</label></td>
						<td><input type="checkbox" name="checkinfo" id="c3" value="c3" disabled> <label for="c3" id="check3">내 경력이력 정보</label></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="checkinfo" id="c4" value="c4" disabled> <label for="c4" id="check4">프로젝트 이력 생성</label></td>
						<td><input type="checkbox" name="checkinfo" id="c5" value="c5" disabled> <label for="c5" id="check5">추천이력서 신청</label></td>
						<td><input type="checkbox" name="checkinfo" id="c6" value="c6" disabled> <label for="c6" id="check6">추천이력서 선정</label></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	
	
	<div class="secondposition">
		<button class="ui purple button mybutton" style="margin-right:30px; font-size:1em;" id="dcheck">출석 체크</button>
		<button class="ui purple basic button mybutton" style="font-size:1em;"onclick="pointCheck();">포인트결제</button>
	</div>



	<%@ include file="../common/ourfooter.jsp" %>

	<script>
		var count = 0;
		/* function dailyCheck() {
			alert("하루에 한번 출석체크 하시면 1포인트 적립됩니다.");
			$("#dcheck").attr("disabled", true);
			
			
		} */
		
		$(function(){
			var basicInfo = <%=ihmap.get("basic")%>;
			var eduInfo = <%=ihmap.get("eduNum")%>;
			var carInfo = <%=ihmap.get("carNum")%>;
			var pjtInfo = <%=ihmap.get("pjtNum")%>;
			var refReqInfo = <%=ihmap.get("refReq")%>;
			var refAccInfo = <%=ihmap.get("refAcc")%>;
			var dailyCheck = <%=ihmap.get("dailyCheck")%>;
			
			if(basicInfo > 0){
				$("#c1").prop("checked", true);
				$("#check1").css("color", "gray");
				count++;
			}
			if(eduInfo > 0){
				$("#c2").prop("checked", true);
				$("#check2").css("color", "gray");
				count++;
			}
			if(carInfo > 0){
				$("#c3").prop("checked", true);
				$("#check3").css("color", "gray");
				count++;
			}
			if(pjtInfo > 0){
				$("#c4").prop("checked", true);
				$("#check4").css("color", "gray");
				count++;
			}
			if(refReqInfo > 0){
				$("#c5").prop("checked", true);
				$("#check5").css("color", "gray");
				count++;
			}
			if(refAccInfo > 0){
				$("#c6").prop("checked", true);
				$("#check6").css("color", "gray");
			}
			
			if(dailyCheck >0){
				$("#dcheck").attr("disabled", true);
			}
			
			$("#completeRate").text("완성도 " + count/5*100 + "%");
			
			
			var con = 0;
			$("#dcheck").click(function(){
				//console.log("클릭!");
				
				alert("하루에 한번 출석체크 하시면 1포인트 적립됩니다.");
				$.ajax({
					url:"<%=request.getContextPath()%>/insertDaily.po",
					type:"get",
					success:function(data){
						console.log(data);
						if(data >0){
							$("#dcheck").attr("disabled", true);
							var oldpoint = <%=ihmap.get("point") %>+1;
							$("#pointCount").text("포인트"+ oldpoint +"p");
							location.href = "<%=request.getContextPath()%>/selectOne.po";
							
						} else{
							alert("출석체크에 실패했습니다.");
						}
					},
					error:function(request,status,error){
				        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				    }
				});
			});
		});

		function pointCheck() {
			location.href = "<%=request.getContextPath()%>/selectList.po";
		}
		
		$(function(){
			$("#donut_single").hide().transition({
				animation:'scale',
				duration: 800
			});
			$("#donut_single2").hide().transition({
				animation:'scale',
				duration: 400
			});
			$("#donut_single3").hide().transition({
				animation:'scale',
				duration: 1200
			});
		});
		
		function goResume(){
			window.location.href="<%=request.getContextPath()%>/selectOne.re";
		}
		
		function seeResume(){
			var num = <%=loginUser.getUno()%>;
			window.location.href="<%=request.getContextPath()%>/selectOther.re?otherUno=" + num;
		}
	</script>
	
	<script type="text/javascript">
		google.charts.load('current', {
			'packages' : [ 'corechart' ]
		});
		google.charts.setOnLoadCallback(drawChart);

		function drawChart() {

			var data = google.visualization.arrayToDataTable([
					[ '완성', '퍼센트' ],
					[ '완성도', count/5*100 ],
					[ '미완성', 100-count/5*100]
					]);
			var data2 = google.visualization.arrayToDataTable([
				[ '완성', '퍼센트' ],
				[ '포인트', <%=ihmap.get("point") %> ],
				[ '미완성', 50]
				]);
			var data3 = google.visualization.arrayToDataTable([
				[ '완성', '퍼센트' ],
				[ '뱃지수', <%=ihmap.get("refAcc") %> ],
				[ '미완성', 10]
				]);

			var options = {
				pieHole : 0.8,
				colors:[ '#f2f58b', 'lightgray'],
				legend : {
					position:"none"
				},
				tooltip: {
					trigger:"none"
				}
				
			};

			var chart = new google.visualization.PieChart(document.getElementById('donut_single'));
			var chart2 = new google.visualization.PieChart(document.getElementById('donut_single2'));
			var chart3 = new google.visualization.PieChart(document.getElementById('donut_single3'));
			chart.draw(data, options);
			chart2.draw(data2, options);
			chart3.draw(data3, options);
		}
		

	</script>
</body>
</html>







