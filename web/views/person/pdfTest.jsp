<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/js/html2canvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js"></script>
<style>
	.pdfOuter{
		margin-right:auto;
		margin-left:auto;
		margin-top:20px;
		/* width:620px;
		height:877px; */
		text-align:center;
		padding:50px;
		padding-right:80px;
		padding-left:80px;
		width:1240px;
		height:1754px;
		/* background:lightgray; */
	}
	.basicArea{
		width:100%;
		height:350px;
		/* border:1px solid gray; */
		text-align:left;
	}
	.photoArea{
		width:19%;
		height:70%;
		display:inline-block;
		/* border:1px dotted gray; */
		padding:10px;
		overflow:hidden;
		vertical-align:top;
	}
	.photoArea img{
		width:100%;
	}
	
	.tableArea{
		width:78%;
		height:100%;
		display:inline-block;
		overflow:hidden;
		vertical-align:top;
	}
	.basicArea table{
		width:100%;
		font-size:1.5em;
		border-spacing:20px;
	}
	.title{
		border-bottom:1px solid gray;
		padding:5px;
		font-size:2em;
		color:gray;
	}
	label{
		font-weight:bold;
	}
	.eduArea{
		width:100%;
		margin-top:50px;
		text-align:left;
		overflow:hidden;
		vertical-align:top;
	}
	.careerArea{
		width:100%;
		margin-top:50px;
		text-align:left;
		overflow:hidden;
		vertical-align:top;
	}
	.careerArea table{
		width:100%;
		font-size:1.5em;
		border-spacing:10px;
	}
	.careerArea tr{ height:50px; }
	
	.eduArea table{
		width:100%;
		font-size:1.5em;
		border-spacing:10px;
	}
	.eduArea tr{ height:50px; }
	
	.otherArea{
		width:100%;
		margin-top:50px;
		text-align:left;
		overflow:hidden;
		vertical-align:top;
	}
	.academy{
		margin-right:10px;
		width:60%;
		display:inline-block;
		overflow:hidden;
		vertical-align:top;
	}
	.skill{
		width:38%;
		display:inline-block;
		overflow:hidden;
		vertical-align:top;
	}
	.academy table{
		width:100%;
		font-size:1.3em;
		border-spacing:10px;
	}
	.academy tr{ height:30px; }
	
	.skill table{
		width:100%;
		font-size:1.5em;
		border-spacing:10px;
	}
	.skill tr{ height:30px; }
	
	.projectArea{
		width:100%;
		margin-top:50px;
		text-align:left;
		overflow:hidden;
		vertical-align:top;
	}
	.projectArea table{
		width:100%;
		font-size:1.5em;
		border-spacing:10px;
	}
</style>
</head>

<body align="center">
	<h1 align="center">PDF변환 페이지입니다.</h1>
	<button onclick="fnSaveAsPdf();">Save PDF</button>
	
	<div class="pdfOuter" id="capture">
		<h1 style="font-size:3em">이력서</h1>
		<div class="basicArea">
			<div class="title">기본 정보</div>
			<div class="photoArea"><img src="<%=request.getContextPath()%>/images/profilePhoto.jpg"></div>
			<div class="tableArea">
				<table>
					<tr>
						<td><label>이름</label></td>
						<td>홍길동</td>
						<td><label>생년월일</label></td>
						<td>1995.01.01</td>
					</tr>
					<tr>
						<td><label>E-mail</label></td>
						<td>testest@itduck.com</td>
						<td><label>전화번호</label></td>
						<td>010-1111-1234</td>
					</tr>
					<tr>
						<td><label>주소</label></td>
						<td colspan="2">서울시 서대문구 연희3동 141-34 동진빌라 8동 302호</td>
						<td>(우편번호: 12345)</td>
					</tr>
					<tr>
						<td><label>GitHub 주소</label></td>
						<td colspan="3">http://gitlab.com/juhye_choi/info</td>
					</tr>
				</table>	
			</div>
		</div>
		<div class="careerArea">
			<div class="title">경력 정보</div>
			<table>
				<thead>
					<tr>
						<th>경력기간</th>
						<th>회사명</th>
						<th>설명</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>2019-07-30 ~ 2019-12-03</td>
						<td>원할머니 코딩</td>
						<td>보쌈주문 로직 개발</td>
					</tr>
					<tr>
						<td>2018-06-10 ~ 2019-06-14</td>
						<td>우아한 자매들</td>
						<td>개발팀 총괄 / PM</td>
					</tr>
					<tr>
						<td>2012-03-02 ~ 2018-05-21</td>
						<td>가가오페이</td>
						<td>백앤드 개발자 / 팀장</td>
					</tr>
				</tbody>
			</table>
		</div> <!-- 경력정보 끝! -->
		
		<div class="eduArea">
			<div class="title">학력 정보</div>
			<table>
				<thead>
					<tr>
						<th>재학기간</th>
						<th>학교명</th>
						<th>학과</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>2010-03-01 ~ 2014-02-14</td>
						<td>유명한대학교</td>
						<td>정보통신공학과</td>
					</tr>
					<tr>
						<td>2007-03-01 ~ 2010-02-16</td>
						<td>OO고등학교</td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div> <!-- 학력정보 끝! -->
		<div class="otherArea">
			<div class="academy">
				<div class="title">교육 정보</div>
				<table>
					<thead>
						<tr>
							<th>교육기관</th>
							<th>기관명</th>
							<th>내용</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>2019-07-22 ~ 2019-02-14</td>
							<td>KH정보교육원</td>
							<td>자바(JAVA) 프레임워크 개발자 양성과정</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="skill">
				<div class="title">자격증</div>
				<table>
					<thead>
						<tr>
							<th>취득날짜</th>
							<th>자격증명</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>2019-08-30</td>
							<td>정보처리기사</td>
						</tr>
					</tbody>
				</table>
			</div>
				
		</div> <!-- OtherArea End -->
		<div class="projectArea">
			<div class="title">Skill Inventory</div>
			<table>
				<tr>
					<th>참여기간</th>
					<th>프로젝트명</th>
					<th>역할</th>
					<th>키워드</th>
				</tr>
				<tr>
					<td>2019-01-01</td>
					<td>개인 이력 저장 및 채용연계 사이트 개발</td>
					<td>개인 이력 저장 로직 / 팀장</td>
					<td>JAVA, Javascript, Servlet, 서비스</td>
				</tr>
			</table>
		</div>
		
	</div> <!-- Outer Area End -->
	
	<script>
		function fnSaveAsPdf(){
			html2canvas(document.getElementById('capture')).then(function(canvas){
				var imgData = canvas.toDataURL('image/png');
				var imgWidth = 210;
				var pageHeight = imgWidth * 1.414;
				var imgHeight = canvas.height * imgWidth / canvas.width;
				
				var doc = new jsPDF({
					'orientation' : 'p',
					'unit' : 'mm',
					'format' : 'a4'
				});
				
				doc.addImage(imgData, 'PNG', 0, 0, imgWidth, imgHeight);
				doc.save('sample_A4.pdf');
				console.log('Reached here?');
			});
			
		}
	</script>
</body>
</html>