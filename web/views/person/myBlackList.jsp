<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, resume.model.vo.*"%>
<%
	ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>>) request.getAttribute("list");
	
	MorePageInfo mpi = (MorePageInfo) request.getAttribute("mpi");
	int listCount = mpi.getListCount();
	int currentPage = mpi.getCurrentPage();
	int maxPage = mpi.getMaxPage();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>블랙리스트</title>
<%@ include file="../common/import.html" %>
	<style>
		.outline{
			width:1000px;
			/* height:650px; */
			margin-right:auto;
			margin-left:auto;
		}
		.title{
			margin:30px;
			margin-left:20px;
			font-size:1.8em;
		}
		.tableArea{
			text-align:center;
			width:100%;
			margin-top:20px;
			height:450px;
			/* border:1px solid black; */
		}
		table{
			width:85%;
		}
		.tableArea th{
			font-size:1.5em;
			border-bottom:1px solid #e7e6e1;
		}
		th, td{
			padding:10px;
			font-size:1.2em;
		}
		td:nth-of-type(3):hover{
			text-decoration:underline;
			cursor:pointer;
		}
		
		.pagingArea{
			text-align:center;
			padding:20px;
			margin-bottom:10px;
		}
		.moreSee1{
			height:40px;
			color:gray;
			border:1px solid lightgray;
			padding:10px;
		}
		.moreSee2{
			height:40px;
			color:gray;
			border:1px solid gray;
			padding:10px;
		}
		.moreSee2:hover{
			cursor:pointer;
			color:black;
		}
		
		.modalOuter{
			margin-left:auto;
			margin-right:auto;
			width:500px;
			height:280px;
			background:white;
			color:black;
			padding:20px;
			text-align:center;
		}
		.buttonArea{
			padding:10px;
		}
	</style>
</head>
<body>
	<%@ include file="../common/mainMenu.jsp" %>

	<div class="outline">
		<div class="title">나의 블랙리스트</div>

		<div class="tableArea">
			<table align="center" id="ingTable">
				<tr>
					<th>날짜</th>
					<th>기업명</th>
					<th>사유</th>
					<th>해지</th>
				</tr>
					<%for(HashMap<String, String> hmap : list){%>
				<tr>
					<td><%=hmap.get("pblDate") %></td>
					<td><%=hmap.get("cname") %></td>
					<td class="openModal"><div><%=hmap.get("reason") %></div>
					<input type="hidden" value="<%=hmap.get("pblId")%>"><input type="hidden" value="<%=hmap.get("pblHisId") %>">
					</td>
					<td><button class="ui button mini delete">차단해지</button></td>
				</tr>
					<%} %>
			</table>
		</div>
		<div class="pagingArea" id="pagingArea">
			<%if(currentPage >= maxPage){ %>
				<div class="moreSee1" id="moreSeeLast">더보기</div>
			<%} else{%>
				<div class="moreSee2" style="" onclick="location.href='<%=request.getContextPath()%>/selectList.seenr?currentPage=<%=currentPage+1%>'">더보기</div>
			<%} %>
		</div>
		
	</div>
	<div class="ui basic modal" id="modal">
		<div class="modalOuter">
			<h4>블랙리스트 수정</h4>
			<hr>
			<form action="<%=request.getContextPath() %>/update.pbl" method="post" id="updateForm">
				<table align="center" style="height:150px;">
					<tr>
						<td><label>회사명</label></td>
						<td><div class="ui input"><input name="blackName" id="blackName" type="text" size="20" readonly></div></td>
					</tr>
					<tr>
						<td><label>사유</label></td>
						<td><div class="ui input"><input name="blackReason" type="text" id="blackReason"></div>
							<input type="hidden" id="pblId" name="pblId">
						</td>
					</tr>
				</table>
				<div class="buttonArea">
					<button type="button" id="modifyBtn" class="ui purple button mini">수정</button>
					<button type="button" id="cancelBtn" class="ui button mini">취소</button>
				</div>
			</form>
		</div>
	</div> <!-- 수정 모달 끝 -->

	<script>
		$(function(){
			var pageSize = <%=currentPage%>;
			console.log(pageSize);
			
			$(".tableArea").css("height", 450*pageSize);
			
			
			$(".openModal").click(function(){
				$("#blackName").val($(this).parent().children().eq(1).text());
				$("#blackReason").val($(this).find("div").text());
				$("#pblId").val($(this).find("input[type=hidden]").eq(1).val()); //블랙리스트 히스토리 ID
				
				$("#modal").modal('show');
			});
			
			$("#modifyBtn").click(function(){ //블랙리스트 차단 내용 수정
				$("#updateForm").submit();
			});
			
			$("#cancelBtn").click(function(){
				$("#modal").modal('hide');
			});
			
			$(".delete").click(function(){ //블랙리스트 차단 해지 버튼! 
				var num = $(this).parent().parent().find("input[type=hidden]").eq(0).val(); //블랙리스트ID
				swal({
					  title: "블랙리스트 차단 해지시겠습니?",
					  text: "해지하시면, 해당 기업이 회원님의 이력서 열람이 가능합니다.",
					  icon: "warning",
					  buttons: true,
					  dangerMode: true,
					})
					.then((willDelete) => {
					  if (willDelete) {
						  location.href="<%=request.getContextPath()%>/delete.pbl?num=" + num;
					  }
					});
				
			});
		});
	
	</script>
	
	
	
	<%@ include file="../common/ourfooter.jsp" %>

</body>
</html>