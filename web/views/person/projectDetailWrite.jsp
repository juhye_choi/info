<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="resume.model.vo.*, java.util.*, keyword.model.vo.*"%>
<% //수정페이지
	ProjectCareer pc = (ProjectCareer) request.getAttribute("pc");
	ArrayList<Keyword> klist = (ArrayList<Keyword>) request.getAttribute("klist");

	if(pc == null){
		request.setAttribute("msg", "잘못된 접근입니다.");
		request.getRequestDispatcher("../common/needLogin.jsp").forward(request, response);
		
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>이력서 상세</title>
<style>
	.first{
		margin-right:auto;
		margin-left:auto;
		width:70%;
	}
	.detailInfo{
		margin:20px;
	}
	.pjt {
		color:gray;
		font-weight:bold;
		font-size:1.1em;
		height:50px;
		padding-top:20px;
	}
	label{
		font-weight:bold;
		font-size:1.2em;
	}
	.tableArea td{
		text-align:center;
	}
	.tableArea{
		border-spacing:2px 20px;
	}
	.contentLabel{
		vertical-align:top;
		padding-top:10px;
	}
	textarea{
		border:1px solid lightgray;
		border-radius:6px;
	}
	textarea:active{
		border:1px solid skyblue;
	}
	
	@media screen and (max-width: 768px){
		.first{
			width:100%;
		}
		.sentence{
			display:none;
		}
		.name{
			font-size:2em;
		}
		.basic2>table{
			width:100%; text-align:center; margin-top:0;
		}
		.basic2 td:last-of-type{
			display:none;
		}
	}
	
	.pjt-photo{
		border:1px solid lightgray;
		display:inline-block;
		vertical-align:middle;
		width:20%;
		height:100px;
		color:white;
		border-radius:10px;
		margin-right:10px;
		margin-left:10px;
		margin-bottom:20px;
		
	}
	.pjt-photo:hover{
		cursor:pointer;
		background:lightgray;
	}
	.keywordArea{
		height:46px;
		width:100%;
		padding:5px;
		overflow:hidden;
	}
	.kbtnArea{
		display:inline-block;
		margin:2px;
	}
	
	.more{
		color:gray;
		border:1px solid #e7e6e1;
		border-bottom-left-radius:8px;
		border-bottom-right-radius:8px;
	}
	.more:hover{
		cursor:pointer;
		color:black;
		/* background:#e7e6e1; */
	}
	
	.pjt-photo>img{
		width:145px;
		height:98px;
		border-radius:10px;
		border:1px doted black;
	}
</style>

<%@ include file="/views/common/import.html" %>
</head>
<body>
	<%@ include file="../common/mainMenu.jsp" %>
	<div class="first">
		<%@ include file="resumeCommon.jsp" %>
		<div class="detailInfo">
			<form id="updateForm" action="<%=request.getContextPath()%>/update.pjt" method="post" encType="multipart/form-data">
				<table class="tableArea" align="center">
					<tr>
						<td colspan="5" style="text-align:left"><div class="pjt">프로젝트 경력기술서 수정</div>
							<input type="hidden" value="<%=pc.getPjtId()%>" name="pjtId">
 						</td>
					</tr>
					<tr>
						<td><label>프로젝트명 : </label></td>
						<td colspan="4"><div class="ui input"><input type="text" name="pjtName" size="80" value="<%=pc.getPjtName()%>" placeholder="Project Title" id="pjtTitle"></div>
							<input type="hidden" name="pjtId" value="<%=pc.getPjtId()%>">
						</td>
					</tr>
					<tr>
						<td><label>내 역할 : </label></td>
						<td colspan="4"><div class="ui input"><input type="text" name="myRole" size="80" value="<%=pc.getMyRole()%>" placeholder="My Role"></div></td>
					</tr>
					<tr>
						<td><label>참여기간 : </label></td>
						<td><div class="ui calendar" id="rangestart"><div class="ui input left icon">
								<i class="calendar icon"></i><input type="text" name="pstartDate" id="start" placeholder="Start Date" value="<%=pc.getPjtStart()%>" readonly></div></div></td>
						<td> ~ </td>
						<td><div class="ui calendar" id="rangeend"><div class="ui input left icon">
								<i class="calendar icon"></i><input type="text" name="pendDate" placeholder="End Date" value="<%=pc.getPjtFinish()%>" readonly></div></div></td>
						<td>(기간 : )</td>
					</tr>
					<tr>
						<td class="contentLabel"><label>상세설명 : </label></td>
						<td colspan="4"><div class="ui form field" style="width:650px;"><textarea name="pContent" rows="20" style="resize:none;"><%=pc.getPjtContent() %></textarea></div></td>
					</tr>
					<tr>
						<td><label>관련링크 : </label></td>
						<%if(pc.getPjtLink() !=null){ %>
						<td colspan="4"><div class="ui input"><input type="url" name="relatedLink" placeholder="http://" size="80" value="<%=pc.getPjtLink()%>"></div></td>
						<%} else{ %>
						<td colspan="4"><div class="ui input"><input type="url" name="relatedLink" placeholder="http://" size="80" value=""></div></td>
						<%} %>
					</tr>
					<tr>
						<td><label>첨부파일 : </label></td>
						<td colspan="4">
							<div style="text-align:left; margin-left:30px;">
								<input type="button" value="첨부파일" id="attachBtn" onclick="$('#attachFile').click()">
							<%if(pc.getAttachmentFile().getOriginName() !=null){ %>
							<span><%=pc.getAttachmentFile().getOriginName() %></span></div>
							<%} else{ %> 
								<span>첨부파일이 없습니다.</span>
							<%} %>
							<input type="file" style="display:none" name="attachFile" id="attachFile" onchange="textchange()">
							</td>
					</tr>
					<tr>
						<td><label>관련사진 : </label></td>
						<td colspan="4">
							<% for(int i=0; i<pc.getAtlist().size(); i++){
								switch(i){
									case 0: %> <div class="pjt-photo" id="thumbnailArea">
												<img id="thumbnailPhoto" src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=pc.getAtlist().get(0).getChangeName()%>"></div> <%break;
									case 1: %> <div class="pjt-photo" id="contentArea1">
												<img id="contentPhoto1" src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=pc.getAtlist().get(1).getChangeName()%>"></div> <%break;
									case 2: %> <div class="pjt-photo" id="contentArea2">
												<img id="contentPhoto2" src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=pc.getAtlist().get(2).getChangeName()%>"></div> <%break;
									case 3: %> <div class="pjt-photo" id="contentArea3">
												<img id="contentPhoto3" src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=pc.getAtlist().get(3).getChangeName()%>"></div> <%break;
								}} %>
							<input type="hidden" name="count" id="hiddenValue">
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="4" id="fileArea">
							<input type="file" id="fileImg1" name="fileImg1" onchange="loadImg(this, 1);" accept="image/gif, image/jpeg, image/png">
							<input type="file" id="fileImg2" name="fileImg2" onchange="loadImg(this, 2);" accept="image/gif, image/jpeg, image/png">
							<input type="file" id="fileImg3" name="fileImg3" onchange="loadImg(this, 3);" accept="image/gif, image/jpeg, image/png">
							<input type="file" id="fileImg4" name="fileImg4" onchange="loadImg(this, 4);" accept="image/gif, image/jpeg, image/png">
						</td>
					</tr>
					<tr>
						<td style="vertical-align:top; padding-top:10px;"><label>키워드 추가 : </label></td>
						<td colspan="4">
							<div class="keywordArea">
								<% int i = 1;
								for(Keyword k : klist){ 
									boolean exist = false;
									for(int j=0; j<pc.getKlist().size(); j++){
										if(k.getKcode() == pc.getKlist().get(j).getKcode()){
											exist = true;
										}
									}
									if(exist == true){%>
										<div class="kbtnArea">
											<label class="ui orange button kbtn" style="" >
											<input type="checkbox" name="keywords" value="<%=k.getKcode()%>" style="visibility:hidden; margin:0; padding:0; width:0;" checked><%=k.getKname() %></label>
										</div>
										<%-- <div class="kbtnArea">
										<div class="ui button kbtn" style="background:orangered"><%=k.getKname() %></div>
										<input type="text" value="<%=k.getKcode()%>" style="display:none">
										<input type="hidden" name="keywords" value="<%=k.getKcode()%>">
										</div> --%>
									<%} else{%>	
										<div class="kbtnArea">
											<label class="ui button kbtn">
											<input type="checkbox" name="keywords" value="<%=k.getKcode()%>" style="visibility:hidden; margin:0; padding:0; width:0;"><%=k.getKname() %></label>
										</div>
								<% }if(i%6==0){%> <br> <% } i++;
								} %>
							</div>
							<div class="more"><i class="chevron down icon"></i></div>
						</td>
					</tr>
				</table>
			</form>
		</div><!-- detailInfo end -->
		
	</div>
	<%@ include file="../common/ourfooter.jsp" %>
	<script>
		var count = 0;
		var used = [false, false, false, false];
		
		function loadImg(value, num){
			if(value.files && value.files[0]){
				var reader = new FileReader();
				//console.log("눌림!");
				fileNm = value.value;
				if (fileNm != "") {
				    var ext = fileNm.slice(fileNm.lastIndexOf(".") + 1).toLowerCase();
				    if (!(ext == "gif" || ext == "jpg" || ext == "png" || ext == "PNG" || ext == "JPG" || ext == "jpeg" || ext == "JPEG")) {
				        alert("이미지파일 (.jpg, .png, .gif ) 만 업로드 가능합니다.");
				        return false;
				    }
				}
				
				reader.onload = function(e){
					
					switch(num){
						case 1 : $("#thumbnailPhoto").attr("src", e.target.result); used[0] = true; break;
						case 2 : $("#contentPhoto1").attr("src", e.target.result); used[1] = true; break;
						case 3 : $("#contentPhoto2").attr("src", e.target.result); used[2] = true; break;
						case 4 : $("#contentPhoto3").attr("src", e.target.result); used[3] = true; break;
					}
				}
				reader.readAsDataURL(value.files[0]);
			}
		}
		

		$("#title").keydown(function(e){
			//console.log($(this).val());
			//console.log($(this).val().length);
			if($(this).val().length >= 20){
				alert("최대 글자수를 초과했습니다.");
			}
		});
		$("#myRole").keydown(function(e){
			//console.log($(this).val());
			//console.log($(this).val().length);
			if($(this).val().length >= 20){
				alert("최대 글자수를 초과했습니다.");
			}
		});
				
		
		
		function textchange(){
			console.log($("#attachFile").val());
			$("#attachBtn").parent().find("span").text($("#attachFile").val().substring(12));
		}
		
		$(function(){
			var more = 1;
			
			$("#fileArea").hide();
			//사진영역 클릭했을 때, 파일이 로드되는 것!
			$("#thumbnailArea").click(function(){
				$("#fileImg1").click();
			});
			$("#contentArea1").click(function(){
				$("#fileImg2").click();
			});
			$("#contentArea2").click(function(){
				$("#fileImg3").click();
			});
			$("#contentArea3").click(function(){
				$("#fileImg4").click();
			});
			
			$("input:checkBox[name='keywords']").click(function(){
				if($(':checkBox[name="keywords"]:checked').length > 10) {
					$(this).prop('checked', false);
					swal ("키워드 갯수 초과", "키워드는 최대 10개까지 가능합니다.", "error");
				} else if($(this).prop('checked') == true) {
					$(this).parent().addClass("orange");
				} else {
					$(this).parent().removeClass("orange");
				}
			});
			
			$('.more').click(function(){
				if(more == 1){
					$('.keywordArea').animate({
						'height':'500px',
						'duration':'1s'
					});
					$(this).html('<i class="chevron up icon"></i>');
					more = 0;
				} else{
					$('.keywordArea').animate({
						'height':'46px',
						'duration':'1s'
					})
					$(this).html('<i class="chevron down icon"></i>');
					more = 1;
					
				}
			});

			$("#finish").css("visibility","visible");
			$("#cancel").css("display","inline-block");
			$("#delete").css("display", "inline-block");
			$("#transPDF").css("display","none");
			
			$("#cancel").click(function(){
				location.href="<%=request.getContextPath()%>/selectOne.re";
			});
			
			$("#finish").click(function(){
				for(var i=0; i<4; i++){
					if(used[i] == true){
						count++;
					}
				}
				
				if($("#pjtTitle").val() == ""){
					swal ({
						   title : "제목 빈칸은 불가합니다.",
						   icon : "error"
					}).then((value) => {
						$("#pjtTitle").parent().addClass("error");
						$("#pjtTitle").focus();
					});
					
					return false;
					
				} else if($("#myRole").val() == ""){
					swal ({
						   title : "내 역할 빈칸은 불가합니다.",
						   icon : "error"
					}).then((value) => {
						$("#myRole").parent().addClass("error");
						$("#myRole").focus();
					});
					
					return false;
					
				} else if($("#start").val() == ""){
					swal ({
						   title : "시작날짜 빈칸은 불가합니다.",
						   icon : "error"
					}).then((value) => {
						$("#start").parent().addClass("error");
						$("#start").focus();
					});
					return false;
					
				} else if($(':checkBox[name="keywords"]:checked').length <= 0){
					alert("키워드를 한개 이상 선택해주세요.");
				    return false;
					
				}  else{
					if($("#attachFile").val() == ""){
						$("#attachFile").remove();
					}

					$("#hiddenValue").val(count);
					$("#updateForm").submit();
				}
			});
			
			$("#delete").click(function(){
				var num = <%=pc.getPjtId()%>;
				var conf = confirm("한 번 삭제하시면 복구가 불가능합니다. 그래도 삭제하시겠습니까?");
				
				if(conf === true){
					location.href="<%=request.getContextPath()%>/delete.pjt?num=" + num;
				}
			});
			
			$('#rangestart').calendar({
				type: 'date',
				formatter : {
					date: function(date, settings){
						if(!date) return '';
						var day = date.getDate();
						var month = date.getMonth() +1;
						var year = date.getFullYear();
						return year + '-' + month + '-' + day;
					}
				},
				endCalendar: $('#rangeend')
			});
			$('#rangeend').calendar({
				type: 'date',
				formatter : {
					date: function(date, settings){
						if(!date) return '';
						var day = date.getDate();
						var month = date.getMonth() +1;
						var year = date.getFullYear();
						return year + '-' + month + '-' + day;
					}
				},
				startCalendar: $('#rangestart')
			});
			
			//기본정보 출력
			$("#name").text("<%=loginUser.getUserName()%>");
			$("#title").text("<%=pc.getrTitle()%>");
			$("#emailAddress").text("<%=loginUser.getEmail()%>");
			$("#phoneNumber").text("<%=loginUser.getPhone()%>");
		});
	
	</script>
</body>
</html>