<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="resume.model.vo.*, java.util.*, person.model.vo.*, member.model.vo.*"%>
<%
	Resume myResume = (Resume) request.getAttribute("resume");
	Member loginUser = (Member) session.getAttribute("loginUser");
	Person person = (Person)request.getAttribute("person");
	ArrayList<Attachment2> fileList = (ArrayList<Attachment2>) session.getAttribute("fileList");
	ArrayList<HashMap<String, String>> pdfklist = (ArrayList<HashMap<String, String>>) request.getAttribute("pdfklist");
	
	ArrayList<Education> elist = myResume.getElist();
	ArrayList<School> slist = myResume.getSlist();
	ArrayList<Certification> celist = myResume.getCelist();
	ArrayList<Career> calist = myResume.getCalist();
	ArrayList<ProjectCareer> plist = myResume.getPlist();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>이력서 내보내기</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/js/html2canvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js"></script>
<script src="<%= request.getContextPath() %>/sweetalert/docs/assets/sweetalert/sweetalert.min.js"></script>
<style>
	.pdfOuter{
		margin-right:auto;
		margin-left:auto;
		margin-top:20px;
		/* width:620px;
		height:877px; */
		text-align:center;
		padding:50px;
		padding-right:80px;
		padding-left:80px;
		width:1240px;
		height:1754px;
		/* background:lightgray; */
	}
	.basicArea{
		width:100%;
		height:350px;
		/* border:1px solid gray; */
		text-align:left;
	}
	.photoArea{
		width:19%;
		height:70%;
		display:inline-block;
		/* border:1px dotted gray; */
		padding:10px;
		overflow:hidden;
		vertical-align:top;
	}
	.photoArea img{
		width:100%;
	}
	
	.tableArea{
		width:78%;
		height:100%;
		display:inline-block;
		overflow:hidden;
		vertical-align:top;
	}
	.basicArea table{
		width:100%;
		font-size:1.5em;
		border-spacing:20px;
	}
	.title{
		border-bottom:1px solid gray;
		padding:5px;
		font-size:2em;
		color:gray;
	}
	label{
		font-weight:bold;
	}
	.eduArea{
		width:100%;
		margin-top:50px;
		text-align:left;
		overflow:hidden;
		vertical-align:top;
	}
	.careerArea{
		width:100%;
		margin-top:50px;
		text-align:left;
		overflow:hidden;
		vertical-align:top;
	}
	.careerArea table{
		width:100%;
		font-size:1.5em;
		border-spacing:10px;
	}
	.careerArea tr{ height:50px; }
	
	.eduArea table{
		width:100%;
		font-size:1.5em;
		border-spacing:10px;
	}
	.eduArea tr{ height:50px; }
	
	.otherArea{
		width:100%;
		margin-top:50px;
		text-align:left;
		overflow:hidden;
		vertical-align:top;
	}
	.academy{
		margin-right:10px;
		width:60%;
		display:inline-block;
		overflow:hidden;
		vertical-align:top;
	}
	.skill{
		width:38%;
		display:inline-block;
		overflow:hidden;
		vertical-align:top;
	}
	.academy table{
		width:100%;
		font-size:1.3em;
		border-spacing:10px;
	}
	.academy tr{ height:30px; }
	
	.skill table{
		width:100%;
		font-size:1.5em;
		border-spacing:10px;
	}
	.skill tr{ height:30px; }
	
	.projectArea{
		width:100%;
		margin-top:50px;
		text-align:left;
		overflow:hidden;
		vertical-align:top;
	}
	.projectArea table{
		width:100%;
		font-size:1.5em;
		border-spacing:10px;
	}
</style>
</head>
<body>
	
	<div class="pdfOuter" id="capture">
		<h1 style="font-size:3em">이력서</h1>
		<div class="basicArea">
			<div class="title">기본 정보</div>
			<div class="photoArea">
				<%if(fileList.size() == 0) {%>
				<img class="personImg" src="<%=request.getContextPath()%>/images/profilePhoto.jpg"><br>
				<%} else { %>
				<img class="personImg" src=" <%=request.getContextPath()%>/thumbnail_uploadFiles/<%=fileList.get(0).getChangeName() %>"><br>
			
				<%} %>

			</div>
			<div class="tableArea">
				<table>
					<tr>
						<td><label>이름</label></td>
						<td><%=loginUser.getUserName()%></td>
						<td><label>생년월일</label></td>
						<%if(person.getBirth() != null){ %>
							<td><%=person.getBirth() %></td>
						<%}else{ %>
							<td>-</td>
						<%} %>
					</tr>
					<tr>
						<td><label>E-mail</label></td>
						<td><%=loginUser.getEmail() %></td>
						<td><label>전화번호</label></td>
						<%if(loginUser.getPhone() != null) {%>
						<td><%=loginUser.getPhone() %></td>
						<%} else{ %>
						<td>-</td><%} %>
					</tr>
					<tr>
						<td><label>GitHub</label></td>
						<%if(person.getGithub() != null) {%>
						<td><%=person.getGithub() %></td>
						<%} else{ %>
						<td>-</td> <%}%>
						<td><label>Blog/사이트</label></td>
						<%if(person.getBlog() != null) {%>
						<td><%=person.getBlog() %></td>
						<%} else{ %>
						<td>-</td> <%}%>
					</tr>
				</table>	
			</div>
		</div>
		<%if(calist.size() != 0){ %>
		<div class="careerArea">
			<div class="title">경력 정보</div>
			<table>
				<thead>
					<tr>
						<th>경력기간</th>
						<th>회사명</th>
						<th>설명</th>
					</tr>
				</thead>
				<tbody>
					<% for(Career c : calist){ %>
						<tr>
						<%if(c.getCarFinishDate() != null) {%>
							<td><%=c.getCarStartDate() %>~<%=c.getCarFinishDate() %></td>
						<%} else{ %>
							<td><%=c.getCarStartDate() %>~now</td>
						<%} %>
							<td><%=c.getCarName() %></td>
						<%if(c.getCarContent() != null){ %>
							<td><%=c.getCarContent() %></td>
						<%} else{ %>
							<td>-</td>
						<%} %>
						</tr>
					<%} %>
				</tbody>
			</table>
		</div> <!-- 경력정보 끝! -->
		<%}
		if(slist.size() != 0){ %>
		<div class="eduArea">
			<div class="title">학력 정보</div>
			<table>
				<thead>
					<tr>
						<th>재학기간</th>
						<th>학교명</th>
						<th>학과</th>
					</tr>
				</thead>
				<tbody>
					<%for(School s : slist){ %>
						<tr>
						<%if(s.getGraduDate() != null) {%>
							<td><%=s.getEnterDate() %>~<%=s.getGraduDate() %></td>
						<%} else{ %>
							<td><%=s.getEnterDate() %>~now</td>
						<%} %>
							<td><%=s.getsName() %></td>
							<td><%=s.getMajor() %></td>
						</tr>
					<%} %>
				</tbody>
			</table>
		</div> <!-- 학력정보 끝! -->
		<%} 
		if(elist.size() != 0 || celist.size() != 0){ %>
		<div class="otherArea">
			<div class="academy">
				<div class="title">교육 정보</div>
				<table>
					<thead>
						<tr>
							<th>교육기관</th>
							<th>기관명</th>
							<th>내용</th>
						</tr>
					</thead>
					<tbody>
						<%if(elist.size() != 0){
							for(Education e: elist){%>
							<tr>
							<%if(e.getEuFinishDate() != null) {%>
								<td><%=e.getEuStartDate() %>~<%=e.getEuFinishDate() %></td>
							<%} else{ %>
								<td><%=e.getEuStartDate() %>~now</td>
							<%} %>
								<td><%=e.geteName() %></td>
								<td><%=e.getEuContent() %></td>
							</tr>
						<%}} %>
					</tbody>
				</table>
			</div>
			<div class="skill">
				<div class="title">자격증</div>
				<table>
					<thead>
						<tr>
							<th>취득날짜</th>
							<th>자격증명</th>
						</tr>
					</thead>
					<tbody>
						<%if(celist.size() != 0){ 
							for(Certification ce : celist){%>
							<tr>
								<td><%=ce.getCertifyDate() %></td>
								<td><%=ce.getCerName() %></td>
							</tr>
						<%}} %>
					</tbody>
				</table>
			</div>
				
		</div> <!-- OtherArea End -->
		<%} 
		if(plist.size() != 0){%>
		<div class="projectArea">
			<div class="title">Skill Inventory</div>
			<table>
				<tr>
					<th width="260px;">참여기간</th>
					<th>프로젝트명</th>
					<th>역할</th>
					<th style="width:500px;">키워드</th>
				</tr>
				<%for(ProjectCareer pc : plist){ %>
				<tr>
					<%if(pc.getPjtFinish() != null) {%>
					<td><%=pc.getPjtStart() %>~<%=pc.getPjtFinish() %></td>
					<%}else{ %>
					<td><%=pc.getPjtStart() %>~now</td> <%} %>
					<td><%=pc.getPjtName() %></td>
					<td><%=pc.getMyRole() %></td>
					<td><%for(HashMap<String, String> hmap : pdfklist){ 
							if(Integer.parseInt(hmap.get("pjtid")) == pc.getPjtId()){%>
							[<%=hmap.get("kname") %>] 
						<%}} %>
					</td>
				</tr>
				<%} %>
			</table>
		</div>
		<%} %>
		
	</div> <!-- Outer Area End -->
	<script>
	$(function(){
		swal({
			  title: "PDF변환",
			  text: "변환하신 파일을 저장하시겠습니까?",
			  icon: "warning",
			  buttons: true,
			  dangerMode: true,
			})
			.then((willDelete) => {
			  if (willDelete) {
				  html2canvas(document.getElementById('capture')).then(function(canvas){
						var imgData = canvas.toDataURL('image/png');
						var imgWidth = 210;
						var pageHeight = imgWidth * 1.414;
						var imgHeight = canvas.height * imgWidth / canvas.width;
						
						var doc = new jsPDF({
							'orientation' : 'p',
							'unit' : 'mm',
							'format' : 'a4'
						});
						
						doc.addImage(imgData, 'PNG', 0, 0, imgWidth, imgHeight);
						doc.save('<%=loginUser.getUserId()%>_Resume.pdf');
						//console.log('너의 하드에 저장!');
				});
			  } else {
			    swal("저장에 실패하였습니다!");
			    location.href="<%=request.getContextPath()%>/selectOne.re";
			  }
			});
		
		
	});
	</script>
</body>
</html>