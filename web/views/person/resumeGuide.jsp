<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% 
	String quarter = (String) request.getAttribute("quarter");
	String Result = (String) request.getAttribute("result");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>가이드라인</title>
<%@ include file="/views/common/import.html"%>
<style>
#stepName {
	font-weight: bold;
	font-size: 20px;
	margin-left: auto;
	margin-right: auto;
	width: 130px;
	}

#stepEx {
	font-weight: bold;
	font-size: 17px;
	margin-left: auto;
	margin-right: auto;
	display:inline-block;
	vertical-align: top;
}
#stepExSub{
	display:inline-block;
	color:gray; 
	font-weight:bold;
	font-size:17px;
}

#stepSubEx {
	font-weight: bold;
	font-size: 17px;
	margin-left: 10px;
	margin-right: auto;
	display:inline-block;
	color:rgb(231,76,60);
}

form {
	background: rgb(241, 218, 127);
	height: 130px;
	width: 130px;
	border-radius: 50%;
}
#submit {
	background:#a333c8;
	font-size: 15px;
	color: white;
	height: 42px;
	width: 182px;
	border-radius: 15px; 
	position: relative;
}

</style>
</head>
<body>
	<%@ include file="../common/mainMenu.jsp"%>
	<div style="height:60px;"></div>
		<div align="center" style="width:90%; margin-left:auto; margin-right:auto;">
			<div
				style="height:50px; display:inline-block; width:10%; font-size:20px; font-weight:bold;">가이드라인</div>
			<p align="left" style="display: inline-block; width:40%; color:gray; font-weight:bold; font-size:13px;">이력서 작성이 어렵다면, 가이드라인을 통해서 자신의
				이력서를 업그레이드 하세요!</p>
			<div style="width:30%; display: inline-block;">
				<% if (loginUser != null && Result == null) { %>
					<button value="추천이력서 등록" id="submit" class="Resumesubmit">추천이력서 신청</button>
				<% }else if(loginUser != null && Result.equals("ACC")) {%>
					<button value="추천이력서 등록" id="submit" style="background:lightgray;" disabled='disabled'>등록중</button>
				<% }else if((loginUser != null && Result.equals("REQ"))) {%>
					<button value="추천이력서 등록" id="submit" style="background:lightgray;" disabled='disabled'>신청 심사중</button>
				<% }else if((loginUser != null && (Result.equals("END")) || (loginUser != null && Result.equals("DEN")))) { %>
					<button value="추천이력서 등록" id="submit" class="Resumesubmit">추천이력서 등록</button>
				<% }else if(loginUser == null){ %>
					<button value="추천이력서 등록" id="submit" class="loginRequest">추천이력서 등록</button>
				<% } %>
			</div>
		<br>
		<br>
		<br>
		<div align="center">
			<table>
				<tr style="height:50px;" align="center">
					<td id="stepName" align="center">STEP1</td>
					<td></td>
					<td id="stepName" align="center" >STEP2</td>
					<td></td>
					<td id="stepName" align="center">STEP3</td>
					<td></td>
					<td id="stepName" align="center">STEP4</td>
					<td></td>
				</tr>
				<tr>
					<td><form action="">
							<p align="center" style="padding-top: 40%; font-weight:bold;">기본 정보 작성</p>
						</form></td>
					<td>
						<img alt="" src="<%= request.getContextPath()%>/images/step.PNG" style="height:64px; width:64px; margin: 20px;">
					</td>
					<td><form action="">
							<p align="center" style="padding-top: 40%; font-weight:bold;">경력기술서 작성</p>
						</form></td>
					<td>
						<img alt="" src="<%= request.getContextPath()%>/images/step.PNG" style="height:64px; width:64px; margin: 20px">
					</td>
					<td><form action="">
							<p align="center" style="padding-top: 40%; font-weight:bold;">추천 이력서 참고</p>
						</form></td>
					<td>
						<img alt="" src="<%= request.getContextPath()%>/images/step.PNG" style="height:64px; width:64px; margin: 20px">
					</td>
					<td><form action="">
							<p align="center" style="padding-top: 40%; font-weight:bold;">이력서 공개</p>
						</form></td>
						<td></td>
				</tr>
				
				<tr>
					<td><p style="width:145px; height:100px; padding-top:15px; color:gray; font-weight:bold;">자기소개, 경력, 학력/<br>교육, 자격증 정보를<br>작성하세요!</p></td>
					<td></td>
					<td><p style="width:145px; height:100px; padding-top:15px; color:gray; font-weight:bold;">경력기술서를 프로젝트<br>별로 작성하세요!</p></td>
					<td></td>
					<td><p style="width:145px; height:100px; padding-top:15px; color:gray; font-weight:bold;">추천이력서를 참고해서<br>본인의 이력서에 적용<br>해보세요!</p></td>
					<td></td>
					<td><p style="width:145px; height:100px; padding-top:15px; color:gray; font-weight:bold;">이력서를 공개 & 인기<br>이력서를 등록항 다른<br>사람들이 열람하도록<br>하세요!</p></td>
				</tr>
				<tr>
					<td colspan="2"><p style="color:orangered; width:220px; font-weight:bold;">[마이페이지 - 기본정보]</p></td>
					<td colspan="2"><p style="color:orangered; width:220px; font-weight:bold;">[마이페이지 - 포트폴리오]</p></td>
					<td colspan="2"><p style="color:orangered; width:220px; font-weight:bold;">[포트폴리오 - 다른 이력서]</p></td>
					<td colspan="2"><p style="color:orangered; width:220px; font-weight:bold;">[마이페이지 - 기본정보]</p></td>
				</tr>
			</table>
		</div>
		<br><br>
		<hr style="width:90%">
		<br><br>
		<div id="step1">
			<div style="width:10%; display:inline-block; overflow:hidden; vertical-align: top;" align="left">
				<h2>STEP1</h2>
				<img alt="" src="<%= request.getContextPath()%>/images/under.PNG" style="height:64px; width:64px; margin-top:20px;">
			</div>
			<div style="width:70%; display:inline-block; overflow:hidden;">
				<table>
					<tr>
						<td style="padding:20px;"><p align="left" id="stepEx">1.자기소개:</p><p id="stepExSub">본인의 현재 위치와 목표를<br>잘 나타낼 수  있는 소개를 작성하세요!</p></td>
						<td><img alt="" src="<%= request.getContextPath()%>/images/aboutMe.PNG" style="display:inline-block; height:100px; width:350px;"></td>
					</tr>	
					<tr>
						<td style="padding:20px;"><img alt="" src="<%= request.getContextPath()%>/images/career.PNG" style="display:inline-block; height:190px; width:350px;"></td>
						<td><p id="stepEx">2.경력이력:<p id="stepExSub">본인의 경력(직장, 프로젝트 등)에 대한<br>기간과 자신의 역활이 잘 나타나도록 작성하세요!</p></td>
						
					</tr>
					<tr>
						<td style="padding:20px;"><p align="left" id="stepEx">3.학력:</p><p id="stepExSub">본인의 학력에 대한<br>기관명, 기간, 전공 등을 작성하세요!</p></td>
						<td><img alt="" src="<%= request.getContextPath()%>/images/edu.PNG" style="display:inline-block; height:190px; width:350px;"></td>
					</tr>
					<tr>
						<td style="padding:20px;"><img alt="" src="" style="display:inline-block; height:190px; width:350px;"></td>
						<td><p id="stepEx">4.교육:</p><p id="stepExSub">본인이 교육을 받은 교육기관명, 기간 등에<br>대해서 작성하세요!</p></td>
					</tr>			
				</table>
			</div>
		</div>
		<br><br>
		<hr style="width:90%">
		<br><br>
		<div id="step2">
			<div style="width:10%; display:inline-block; overflow:hidden; vertical-align: top;" align="left">
				<h2>STEP2</h2>
				<img alt="" src="<%= request.getContextPath()%>/images/under.PNG" style="height:64px; width:64px; margin-top:20px;">
				<div style="height:80%"></div>
			</div>
			<div align="left" style="width:70%; display:inline-block; z-index:3">
			<p id="stepEx">경력기술서를 본인이 참여한 프로젝트 별로 작성하세요!</p>
				<div style="width:40%" align="left">
					<p id="stepExSub">프로젝트 이름과 기간, 상세내용을 입력합니다.</p>
					<p id="stepExSub">연관 링크 및 첨부파일을 추가하여 이력에 대한<br>
					신뢰도를 높일 수 있습니다.</p>
					<p id="stepExSub">키워드를 추가하시면,[나의 이력서]에 연관키워드로<br>
					본인의 키워드를 모아보실 수 있습니다.</p>
				</div>
			</div>
			<div style="display:inline-block; vertical-align: top; z-index:5" align="right">
				<img alt="" src="<%= request.getContextPath()%>/images/pg2.PNG" style="width:70%; margin-top:-150px; z-index:10; text-align:right;">
			</div>
		</div>
		<br><br>
		<hr style="width:90%">
		<br><br>
		<div id="step3">
			<div style="width:10%; display:inline-block; vertical-align: top;" align="left">
				<h2>STEP3</h2>
				<img alt="" src="<%= request.getContextPath()%>/images/under.PNG" style="height:64px; width:64px; margin-top:20px;">
			</div>
			<div align="left" style="width:70%; display:inline-block;">
				<p id="stepEx">잘 만들어진 이력서를 참고하여 본인의 이력서에 반영해보세요</p><br>
				<p id="stepSubEx">1) '추천이력서'를 열람</p><br>
				<p id="stepSubEx">2) 키워드 검색을 통해 연관 이력서를 열람</p>
				<br><br><br>
			</div>
			<img alt="" src="<%= request.getContextPath()%>/images/pg3.PNG" style="text-align:center; justify-content:center;">
		</div>
		<br><br>
		<hr style="width:90%">
		<br><br>
		<div id="step4">
			<div style="width:10%; display:inline-block; vertical-align: top;" align="left">
				<h2>STEP4</h2>
				<img alt="" src="<%= request.getContextPath()%>/images/under.PNG" style="height:64px; width:64px; margin-top:20px;">
			</div>
			<div align="left" style="width:70%; display:inline-block;">
				<p id="stepEx">이력서를 공개해서 채용의 기회를 높여보세요!</p><br>
				<p id="stepSubEx">1) '추천이력서'를 공개하시면, 페이지 상단 노출이 가능합니다.</p><br>
				<p id="stepSubEx">2) '주간 TOP10'에 선정되시면 소정의 혜택이 있습니다.</p><br>
			</div>
			<img alt="" src="<%= request.getContextPath()%>/images/pg4.PNG" style="text-align:center; justify-content:center;">
		</div>
		<br><br><br>
		<div>
			<h2 align="center">추천이력서로 신청하시겠습니까?</h2>
			<br><br>
				<% if (loginUser != null && Result == null) { %>
					<button value="추천이력서 등록" id="submit" class="Resumesubmit">추천이력서 신청</button>
				<% }else if(loginUser != null && Result.equals("ACC")) {%>
					<button value="추천이력서 등록" id="submit" style="background:lightgray;" disabled='disabled'>등록중</button>
				<% }else if((loginUser != null && Result.equals("REQ"))) {%>
					<button value="추천이력서 등록" id="submit" style="background:lightgray;" disabled='disabled'>신청 심사중</button>
				<% }else if((loginUser != null && (Result.equals("END")) || (loginUser != null && Result.equals("DEN")))) { %>
					<button value="추천이력서 등록" id="submit" class="Resumesubmit">추천이력서 등록</button>
				<% }else if(loginUser == null){ %>
					<button value="추천이력서 등록" id="submit" class="loginRequest">추천이력서 등록</button>
				<% } %>
			<div style="height:50px;"></div>
		</div>
	</div>
	
	<div class="ui modal" style="width:350px; height:170px; text-align:center; vertical-align:middle; padding:25px;">
		<i class="close icon"></i>
		<div class="description">
			<div align="center">
				<h4>'추천 이력서'로 등록 신청하시겠습니까?</h4>
				<h4 style="margin:0px;">(관리자 승인 후 등록됩니다.)</h4>
			</div><br>
			<div align="center" style="margin">
				<button style="width:130px; height:40px; background:blue; color:white; font-size:15px; border:none; border-radius: 10px;" class="submit">신청</button>
				<button style="width:130px; height:40px; background:gray; color:white; font-size:15px; border:none; border-radius: 10px;" class="close">취소</button>
			</div>
		</div>
	</div>
	
	<script>
		$(function(){
			$(".Resumesubmit").click(function(){
				$('.ui.modal')
				  .modal('show');
			});
			
			$(".close").click(function(){
				$('.ui.modal')
				  .modal('hide');
			})
			$(".submit").click(function(){
				location.href="/info/insertRef.rg";
			});
			$(".loginRequest").click(function(){
				alert("로그인이 필요한 화면입니다.");
			});
		});
	</script>
	<%@ include file="../common/ourfooter.jsp" %>
</body>
</html>