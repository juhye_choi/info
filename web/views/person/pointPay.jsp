<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, person.model.vo.PointHistory, keyword.model.vo.PageInfo"%>
<%
	ArrayList<PointHistory> polist = (ArrayList<PointHistory>) request.getAttribute("polist");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
	
	for(PointHistory ph : polist){
		if(ph.getCategory().equals("ATT")){
			ph.setCategory("적립");
		}else if(ph.getCategory().equals("USE")){
			ph.setCategory("사용");
			ph.setPoint(-ph.getPoint());
		}else if(ph.getCategory().equals("PAY")){
			ph.setCategory("결제");
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>포인트결제</title>
<%@ include file="/views/common/import.html" %>
<script type="text/javascript"src="https://cdn.iamport.kr/js/iamport.payment-1.1.5.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
	.first {
		margin-right:auto;
		margin-left:auto;
		margin-top:50px;
		width:800px;
		height:200px;
		border-bottom : 1px solid lightgray;
	}
	.second{
		margin-right:auto;
		margin-left:auto;
		width:800px;
		margin-top:25px;
	}
	.first-button {
		width:100%;
		text-align:right;
	}
	
	.paytable{
		margin-top:20px;
		margin-right:auto;
		margin-left:auto;
		width:700px;
	}
	.paytable td{
		padding:10px;
		font-size:1.2em;
	}
	.otherName:hover{
		text-decoration:underline;
		cursor:pointer;
	}
	.pagingArea{
		text-align:center;
		padding:20px;
		margin-bottom:20px;
		margin-top:10px;
	}
	.pagingbtn{
		height:30px;
		width:30px;
		display:inline-block;
		vertical-align:top;
		overflow:hidden;
		background:white;
		border:1px solid lightgray;
		border-radius:5px;
	}
	.pagingbtn:hover{
		cursor:pointer;
	}
	.pagingbtn:disabled{
		color:gray;
	}
</style>
</head>
<body>
	<%@ include file="../common/mainMenu.jsp" %>
	<div class="first">
		<h1>포인트 결제</h1>
		<div class="first-button">
			<button class="ui purple button" style="width: 100px;" onclick="payPoint()">결제하기</button>
			<button class="ui button" style="width: 100px;" onclick="location.href='<%=request.getContextPath()%>/selectOne.po'">취소</button>
		</div>
		<table class="paytable">
			<tr id="payMoney">
				<td><input type="radio" name="pay" id="point100" value="10000" checked><label for="point100"> 100꽥 / 10,000원</label></td>
				<td><input type="radio" name="pay" id="point50" value="5000"><label for="point50"> 50꽥 / 5,000원</label></td>
				<td><input type="radio" name="pay" id="point10" value="1000"><label for="point10"> 10꽥 / 1,000원</label></td>
				<td><input type="radio" name="pay" id="point5" value="500"><label for="point5"> 5꽥 / 500원</label></td>
			</tr>
		</table>
	</div>
	<div class="second">
		<h1>포인트 사용/적립 내역</h1>
		
		<table class="ui sortable celled table" id="logTable">
			<thead>
				<tr align="center">
					<th>구분</th>
					<th>날짜</th>
					<th>금액</th>
					<th>사용/적립내역</th>
					<th>꽥 포인트</th>
				</tr>
			</thead>
			<tbody>
				<% for(PointHistory ph : polist){ %>
					<tr align="center">
						<td><%=ph.getCategory() %></td>
						<td><%=ph.getPoDate() %></td>
						<%if(ph.getPayPoint() != 0){ %>
							<td><%=ph.getPayPoint() %> 원</td>
						<%} else{ %>
							<td>-</td>
						<%} 
						 if(ph.getPayName() != null){%>
						<td class="otherName"><%=ph.getPayName()%>
							<input id="otherUno" type="hidden" value="<%=ph.getPayUno()%>">
						</td>
						<%} else{ %>
							<td><%=ph.getPoContent() %></td>
						<%} %>
						<td><%=ph.getPoint() %></td>
					</tr>
				<%} %>
			</tbody>
		</table>
		
		
	<!-- 페이징 들어가야하는 영역 -->️
		<div class="pagingArea">
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.po?currentPage=1'"><<</button>
			<%if(currentPage <=1) {%>
				<button class="pagingbtn" disabled></button>
			<%} else{ %>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.po?currentPage=<%=currentPage-1%>'"><</button>
			<%} %>
			
			<%for(int p=startPage; p<=endPage; p++){
				if(p == currentPage){ %>
					<button class="pagingbtn" disabled><%=p %></button>
				<%} else{ %>
					<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.po?currentPage=<%=p%>'"><%=p %></button>
				<%}
			  } %>
			
			<%if(currentPage >= maxPage){ %>
				<button class="pagingbtn" disabled></button>
			<%} else {%>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.po?currentPage=<%=currentPage+1%>'">></button>
			<%} %>
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.po?currentPage=<%=maxPage%>'">>></button>
		</div>
	</div>
	
	<script>
		$(function(){
			$("#logTable").tablesort();	
			
			
			$(".otherName").click(function(){
				var otherUno = $(this).find("input[type=hidden]").val();
				location.href="<%=request.getContextPath()%>/selectOther.re?otherUno=" + otherUno;
			});
		});
		
		function payPoint(){
			var IMP = window.IMP;
			IMP.init('imp79349410');
			//console.log($("input[type=radio]:checked").val());
			var amount = $("input[type=radio]:checked").val();
			
			IMP.request_pay({
				pg : 'inicis', // version 1.1.0부터 지원.
				pay_method : 'card',
				merchant_uid : 'merchant_' + new Date().getTime(),
				name : '포인트결제',
				amount : amount, //판매 가격
				buyer_email : 'iamport@siot.do',
				buyer_name : '<%=loginUser.getUserName()%>',
				buyer_tel : '010-1234-5678',
				buyer_addr : '서울특별시 강남구 삼성동',
				buyer_postcode : '123-456',
				app_scheme : '<%=request.getContextPath()%>/index.jsp'
			}, function(rsp) {
				if (rsp.success) { //결제 성고했을때의 로직
					var msg = '결제가 완료되었습니다.';
					msg += '고유ID : ' + rsp.imp_uid;
					msg += '상점 거래ID : ' + rsp.merchant_uid;
					msg += '결제 금액 : ' + rsp.paid_amount;
					msg += '주문명 : ' + rsp.name;
					msg += '결제상태 : ' + rsp.status;
					msg += '카드 승인번호 : ' + rsp.apply_num; // 승인번호 (신용카드결제 한에서)
					msg += '주문자이름 : ' + rsp.buyer_name;
					msg += '주문자email : ' + rsp.buyer_email;
					msg += '주문자 연락처 : ' + rsp.buyer_tel;
					msg += '결제승인시각  : ' + rsp.paid_at;
					msg += '거래 매출전표 : ' + rsp.receipt_url;
					
					var result = { 
							payname:rsp.name,
							userName:rsp.buyer_name,
							uno:<%=loginUser.getUno()%>,
							amount:rsp.paid_amount
					};
					
					console.log(result);	
					
					$.ajax({
						url:"<%=request.getContextPath()%>/insertPay.po",
						data :result,
						type: "get",
						success:function(data){
							swal ({
								   title : "결제 완료되었습니다!",
								   icon : "success"
								}).then((value) => {
								   location.href="<%=request.getContextPath()%>/selectList.po";
								});
						},
						error:function(data){
							console.log("실패!");
						}
					});
				} else { //결제 실패시 로직
					swal ("에러내용 : " + rsp.error_msg, {
						   title : "결제에 실패하였습니다.",
						   icon : "error"
						}).then((value) => {
						   location.href="<%=request.getContextPath()%>/selectList.po";
						});
				}
			});
			
		}
	</script>
	
	
	
	<%@ include file="../common/ourfooter.jsp" %>
</body>
</html>










