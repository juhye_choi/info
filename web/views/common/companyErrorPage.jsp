<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String msg = (String) request.getAttribute("msg"); 
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>에러페이지</title>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>
	<script>
	var msg = "<%= msg %>";
	var path = "<%=request.getContextPath()%>/views/main/companyMain.jsp";
	
	swal ({
		   title : msg,
		   icon : "error"
		}).then((value) => {
		   location.href=path; 
		});
	
	</script>
</body>
</html>