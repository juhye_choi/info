<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript"src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript"src="https://cdn.iamport.kr/js/iamport.payment-1.1.5.js"></script>
<title>아임포트 테스트</title>
<style>
	#Test{
		margin-left: auto;
		margin-right:auto;
		width: 200px;
		border: 1px solid black;
		text-align: center;
	}
</style>
</head>
<body>
	<div style="height: 200px;"></div>
	<div id= "Test" onclick="importTest()">아임포트 테스트</div>
	<script>
		
		function importTest(){
			var IMP = window.IMP;
			IMP.init('imp79349410');

			IMP.request_pay({
				pg : 'inicis', // version 1.1.0부터 지원.
				pay_method : 'card',
				merchant_uid : 'merchant_' + new Date().getTime(),
				name : '주문명:결제테스트',
				amount : '100', //판매 가격
				buyer_email : 'iamport@siot.do',
				buyer_name : '구매자이름',
				buyer_tel : '010-1234-5678',
				buyer_addr : '서울특별시 강남구 삼성동',
				buyer_postcode : '123-456',
				app_scheme : '<%=request.getContextPath()%>/index.jsp'
			}, function(rsp) {
				if (rsp.success) { //결제 성고했을때의 로직
					var msg = '결제가 완료되었습니다.';
					msg += '고유ID : ' + rsp.imp_uid;
					msg += '상점 거래ID : ' + rsp.merchant_uid;
					msg += '결제 금액 : ' + rsp.paid_amount;
					msg += '주문명 : ' + rsp.name;
					msg += '결제상태 : ' + rsp.status;
					msg += '카드 승인번호 : ' + rsp.apply_num; // 승인번호 (신용카드결제 한에서)
					msg += '주문자이름 : ' + rsp.buyer_name;
					msg += '주문자email : ' + rsp.buyer_email;
					msg += '주문자 연락처 : ' + rsp.buyer_tel;
					msg += '결제승인시각  : ' + rsp.paid_at;
					msg += '거래 매출전표 : ' + rsp.receipt_url;
					
					var result = { 
							result1:rsp.name,
							result2:rsp.buyer_name,
							result3:rsp.paid_amount
					};
					
					console.log(result);	
					
					$.ajax({
						url:"<%=request.getContextPath()%>/testImport",
						data :result,
						type: "get",
						success:function(data){
							location.href = "<%=request.getContextPath()%>/index.jsp";
						},
						error:function(data){
							console.log("실패!");
						}
					});
				} else { //결제 실패시 로직
					var msg = '결제에 실패하였습니다.';
					msg += '에러내용 : ' + rsp.error_msg;
				}
				alert(msg);
			});
			
		}
		
	</script>
</body>
</html>