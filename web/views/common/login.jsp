<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
	
	<%
	
	ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) request.getAttribute("list");

	
	%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>로그인</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%= request.getContextPath() %>/semantic/semantic.min.css">
<script src="<%= request.getContextPath() %>/semantic/semantic.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
body {
	text-align: center;
	padding-top: 5%;
	margin: auto;
}

.memberChange {
	width: 230px;
	height: 60px;
	margin: 3px;
	background: white;
	border: 2px solid black;
	font-size: 17px;
}

.memberChange:hover {
	cursor: pointer;
}

#personal {
	background: #CC33FF;
	color: white;
}

.ui left icon input {
	margin: 50px;
}

table {
	margin-top: 50px;
}

input[type='submit'] {
	height: 100px;
	background: gray;
	color: white;
	border-radius: 10px;
}

#sign1 {
	margin-top: 15px;
}
</style>
</head>
<body>



	
	
	<table align="center">
	<tr>
	

	
	<%  if(list.size() == 0 || list == null) {%>
	<td style="width:20%"><img src="<%= request.getContextPath() %>/images/idAdver1.jpg" alt="메인로고" style="width:60%; height:490px;" ></td>
	<%}else if(list.size() == 1 && (int)list.get(0).get("pCode") == 8) {%>
		<td style="width:20%"><img src="<%= request.getContextPath() %>/attachment_uploadFiles/<%= list.get(0).get("changeName") %>" alt="메인로고" style="width:60%; height:490px;%" ></td>
	<%} else if(list.size() == 1 && (int)list.get(0).get("pCode") != 8) {%>
	<td style="width:20%"><img src="<%= request.getContextPath() %>/images/idAdver1.jpg" alt="메인로고" style="width:60%; height:490px;" ></td>
	<%} else if(list.size() == 2) { %>
	<td style="width:20%"><img src="<%= request.getContextPath() %>/attachment_uploadFiles/<%= list.get(0).get("changeName") %>" alt="메인로고" style="width:60%; height:490px;%" ></td>
	<%} %>

	
	
	<td style="width:50%; padding-bottom:200px;">
	<a href="<%= request.getContextPath() %>/index.jsp"><img
		src="<%= request.getContextPath() %>/images/logo.PNG" alt="메인로고"
		width="60%"></a>
	<br>
	<a href="<%= request.getContextPath() %>/loginBannerImg.me?type=1"><button
			class="memberChange" id="personal">개인</button></a>
	<a href="<%= request.getContextPath() %>/loginBannerImg.me?type=2"><button
			class="memberChange" id="enterprise">기업</button></a>
	<br>
	
	<div id=pLogin>

		<form id="pLoginForm" action="<%=request.getContextPath()%>/pLogin.me"
			method="post">


			<table align="center">
				<tr>
					<td>
						<div class="ui left icon input">
							<i class="user icon"></i> <input type="text" id="userId"
								name="userId" placeholder="userId" style="weight: 100px;">
						</div>

					</td>
					<td rowspan="2">
						<button type="submit" class="ui button" style="height: 80px;"
							onclick="pLogin();">로그인</button>
					</td>
				</tr>
				<tr>
					<td>

						<div class="ui left icon input">
							<i class="lock icon"></i> <input type="password" id="userPwd"
								name="userPwd" placeholder="Password">
						</div>

					</td>

				</tr>



			</table>


			<div id="sign1">

				<a href="views/person/personjoinForm.jsp">회원가입하기</a> <a
					href="views/person/personIdCheck.jsp" style="padding-left: 20px;">아이디</a>
				/ <a href="views/person/personPasswordCheck.jsp">비밀번호 찾기</a>


			</div>


		</form>


	</div></td>
	
	<%  if(list.size() == 0 || list == null) {%>
	<td style="width:20%"><img src="<%= request.getContextPath() %>/images/idAdver1.jpg" alt="메인로고" style="width:60%; height:490px;" ></td>
	<%}else if(list.size() == 1 && (int)list.get(0).get("pCode") == 9) {%>
		<td style="width:20%"><img src="<%= request.getContextPath() %>/attachment_uploadFiles/<%= list.get(0).get("changeName") %>" alt="메인로고" style="width:60%; height:490px;%" ></td>
	<%} else if(list.size() == 1 && (int)list.get(0).get("pCode") != 9) {%>
	<td style="width:20%"><img src="<%= request.getContextPath() %>/images/idAdver1.jpg" alt="메인로고" style="width:60%; height:490px;" ></td>
	<%} else if(list.size() == 2) { %>
	<td style="width:20%"><img src="<%= request.getContextPath() %>/attachment_uploadFiles/<%= list.get(1).get("changeName") %>" alt="메인로고" style="width:60%; height:490px;%" ></td>
	<%} %>
	
	

 
	
	
	
	</tr>
	
	
	</table>
	
	

	<script>
	
	

	function pLogin() {
		$("#pLoginForm").submit(); 
		
	}
	
	
	
	
	</script>

</body>
</html>