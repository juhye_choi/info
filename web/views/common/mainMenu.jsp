<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="member.model.vo.*"%>
 <%
 	Member loginUser = (Member) session.getAttribute("loginUser");
	 if(loginUser!= null && loginUser.getCategory().equals("C")){
		 	session.invalidate();
		}
 %>
    <link rel="stylesheet" href="<%= request.getContextPath()%>/css/menuStyle.css">
    <div style="height:50px; background:white;"></div>
    
    <div class="ui center aligned grid" style="position:fixed; top:0; left:0; right:0; margin:0px; background:white; z-index:99; height:50px; border-bottom:1px solid #e7e6e1;">
    	<div class="left floated left aligned four wide column">
			<a id="home"><img src="<%= request.getContextPath()%>/images/ITDuck.png" class="itduck"></a>
    	</div>
    	<div class="center aligned eight wide column">
			<a class="menu underline" href="<%= request.getContextPath()%>/selectOne.po" id="myPage">마이페이지</a>
			<a class="menu underline" href="<%= request.getContextPath()%>/searchResume.sr" id="resume">이력서</a>
			<a class="menu underline" href="<%= request.getContextPath()%>/recruit.prl" id="search">채용검색</a>
			<a class="menu underline" href="<%= request.getContextPath()%>/views/person/customerService.jsp" id="customer">고객센터</a>

		</div>
    	<div class="right floated right aligned four wide column">
    		<%if(loginUser == null || loginUser.getCategory().equals("C")){ %>
			<a class="menu" href="<%= request.getContextPath()%>/loginBannerImg.me?type=1" id="login">로그인</a>
			<%} else { %>
			<a class="menu" id="logout" style="padding:0; margin:0px; margin-right:10px;"><i class="user icon" style="height:100%"></i></a>
			<%} %>
			<a class="menu" href="<%= request.getContextPath()%>/views/main/companyMain.jsp" style="color:gray;" id="comp">기업회원</a>
			
		</div>
    </div>
    
	<!-- 마이페이지 서브메뉴 -->
	<div class="ui centered grid" style="display:none; height:50px; position:fixed; top:32px; left:0; right:0; background:white; opacity:0.6; z-index:98;" id="submenu1">
		<div class="ui secondary menu">
		<a class="item" href="<%= request.getContextPath()%>/views/person/personInformation.jsp">기본정보</a>
		<a class="item" href="<%=request.getContextPath()%>/selectOne.re" id="myResume">나의이력서</a>
		<a class="item" href="<%=request.getContextPath()%>/selectList.papp" id="applyLog">지원현황</a>
		<a class="item" href="<%=request.getContextPath() %>/selectList.seenr" id="watchingLog">내가본공고</a>
		<a class="item" href="<%=request.getContextPath()%>/selectList.pbl" id="myBlackList">블랙리스트</a>
		</div>
	</div>
	<!-- 이력서 서브메뉴 -->
	<div class="ui centered grid" style="display:none; height:50px; position:fixed; top:32px; left:0; right:0; background:white; opacity:0.6;  z-index:98;" id="submenu2">
		<div class="ui secondary menu">
		<a class="item" href="<%=request.getContextPath()%>/selectOne.re">나의이력서</a>
		<a class="item" href="<%= request.getContextPath()%>/searchResume.sr">이력서검색</a>
		<a class="item" href="<%= request.getContextPath()%>/selectResume.rg">가이드라인</a>
		<a class="item" href="<%= request.getContextPath()%>/selectRecommendResume.rh">추천이력서</a>
		</div>
	</div>
	
	<!-- 로그인 정보 서브메뉴-->
	<%if(loginUser != null){ %>
	<div class="loginInfo" style="" id="submenu3">
		<div class="ui vertical menu" style="width:120px; padding:0px;">
			<div class="loginMenu" style="">
				<%=loginUser.getUserName() %>님</div>
			<a class="item" href="<%=request.getContextPath()%>/selectOne.po">내 정보</a>
			<a class="item" href="<%=request.getContextPath()%>/selectOne.re">내 이력서</a>
			<a class="item" href="<%=request.getContextPath()%>/logout">로그아웃</a>

		</div>
	</div>
	<%} %>
	<!-- 로그인기능 완료하면 hide해야함!! -->
	<!-- <button class="circular ui olive button mini" 
		style="position:fixed; bottom:30px; right:20px; z-index:95; box-shadow: 5px 5px 10px 1px gray;" onclick="mgtFn();">관리자</button> -->
	
	<script>
		$(function() {
			$("#myPage").hover(function() {
				$("#submenu1").show();
				$("#submenu2").hide();
				$("#submenu3").hide();
			});
			$("#submenu1").mouseleave(function() {
				$(this).hide();
			});
	
			$("#resume").hover(function() {
				$("#submenu1").hide();
				$("#submenu2").show();
				$("#submenu3").hide();
			});
			$("#submenu2").mouseleave(function() {
				$(this).hide();
			});
			$("#logout").hover(function() {
				$(this).css("cursor", "pointer");
				$("#submenu1").hide();
				$("#submenu2").hide();
				$("#submenu3").show();
			});
			$("#submenu3").mouseleave(function() {
				$(this).hide();
			});
	
			$("#search, #customer").hover(function() {
				$("#submenu1").hide();
				$("#submenu2").hide();
			});
			
			$("#comp").hover(function(){
				$(this).css("color","black");
			}, function(){
				$(this).css("color","gray");
			});
			
			$("#home").hover(function(){
				$(this).css("cursor", "pointer");
			}).click(function(){
				window.location.href="<%= request.getContextPath()%>/index.jsp";
			});
		});
		
		function mgtFn(){
			window.location.href="<%= request.getContextPath()%>/views/main/adminMain.jsp";
		}
	</script>
	
	
	
	
	
	
