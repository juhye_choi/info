<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<style>
	.fcategory{
		margin:10px;
	}
	.fcategory:hover{
		cursor:pointer;
		text-decoration:underline;
	}
</style>
<footer style="height:200px; background:#b5b5b5; color:white; margin-top:100px;">
<div class="area1" style="padding:40px; text-align:center; font-size:1.3em;">
	<span class="fcategory">IT DUCK</span> | 
	<span class="fcategory">서비스 소개</span> | 
	<span class="fcategory" id="email">이메일 문의</span> | 
	<span class="fcategory" style="margin:10px;">자주 묻는 질문</span>
</div>
<div class="area2" style="width:100%; text-align:center; padding-top:10px;">
프로젝트명 : IT Duck (개인 이력관리 사이트)<br>
파덕, 세미프로젝트 팀장 : 최주혜 | 팀원 : 황규환 이도훈 문호승 박종현<br>
<i class="gitlab icon"></i>GitLab주소 : <a href="https://gitlab.com/juhye_choi/info" style="color:white">https://gitlab.com/juhye_choi/info</a> | @Itduck, Inc.<br>
</div>
<div id="joke" style="text-align:center; padding-top:5px; font-size:10px;">
<a href="https://vo.la/xStA" style="color:white">Designed by jjumentic <i class="paw icon"></i></a>
</div>
</footer>

<script>
	$(function(){
		$("#email").click(function(){
			location.href = "<%=request.getContextPath()%>/views/common/emailQ.jsp";
		});
	});

</script>