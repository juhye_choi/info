<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	int successCode = Integer.parseInt(request.getParameter("successCode"));
	System.out.println(successCode);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>
<script>
	var msg = "";
	var path = "";
	<% if(successCode == 1){%>
		msg = "경력을 삭제하였습니다.";
		path = "<%=request.getContextPath()%>/selectOne.re";
	<% } else if(successCode ==2){%>
		msg = "학력을 삭제하였습니다.";
		path = "<%=request.getContextPath()%>/selectOne.re";
	<% } else if(successCode == 3){ %>
		msg = "교육사항을 삭제하였습니다.";
		path = "<%=request.getContextPath()%>/selectOne.re";
	<% } else if(successCode == 4){%>
		msg = "자격증 사항을 삭제하였습니다.";
		path = "<%=request.getContextPath()%>/selectOne.re";
	<% } else if(successCode == 5){%>
		msg = "광고배너 사진 수정 완료.";
		path = "<%=request.getContextPath()%>/selectAdverAcc";
	<%} else if (successCode == 6){%>
		msg = "환불요청 완료.";
		path = "<%=request.getContextPath()%>/allPaymentList";
		console.log("success 들어왔냐?");
	<%} else if(successCode == 7){%>
		msg = "환불처리 완료.";
		path = "<%=request.getContextPath()%>/adminallPaymentList";
	<%}%>
	swal ({
		   title : msg,
		   icon : "success"
		}).then((value) => {
		   location.href=path; 
		});
	
	//location.href=path;
</script>
</body>
</html>