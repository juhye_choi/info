<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="member.model.vo.*"%>
 <%
 	
 	Member loginUser = (Member) session.getAttribute("loginUser");
	 if(loginUser != null && loginUser.getCategory().equals("P")){
			 session.invalidate();
		}
 %>
    <link rel="stylesheet" href="<%= request.getContextPath()%>/css/menuStyle.css">
    <div style="height:50px; background:white;"></div>
    
    <div class="ui center aligned grid" style="position:fixed; top:0; left:0; right:0; margin:0px; background:white; z-index:99; height:50px; border-bottom:1px solid #e7e6e1;">
    	<div class="left floated left aligned four wide column">
			<a class="" id="home"><img src="<%= request.getContextPath()%>/images/ITDuck.png" class="itduck"></a>
    	</div>
    	<div class="center aligned eight wide column">
	    	<a class="menu underline" href="<%= request.getContextPath()%>/intro.atc" id="comPage">기업페이지</a>
			<a class="menu underline" href="<%= request.getContextPath()%>/search.cr" id="search">인재검색</a>
			<a class="menu underline" href="<%= request.getContextPath()%>/recruitList" id="premium">프리미엄</a>
			<a class="menu underline" href="<%= request.getContextPath()%>/views/company/companyCustomerService.jsp" id="customer">고객센터</a>
		</div>
		<div class="right floated right aligned four wide column">
			<%if(loginUser == null || loginUser.getCategory().equals("P")){ %>
			<a class="menu" href="<%= request.getContextPath()%>/loginBannerImg.me?type=2" id="login">로그인</a>
			<%} else{ %>
			<a class="menu" id="logout" style="padding:0; margin:0px; margin-right:10px;"><i class="user icon" style="height:100%"></i></a>
			<%} %>
			<a class="menu" href="<%= request.getContextPath()%>/index.jsp" style="color:gray;" id="pers">개인회원</a>
		</div>
	</div>


		<!-- 기업페이지 서브메뉴 -->
	<div class="ui centered grid" style="display:none; height:50px; position:fixed; top:30px; left:0; right:0; background:white; opacity:0.6; z-index:98;" id="submenu1">
		<div class="ui secondary menu">   
		<a class="item" href="<%= request.getContextPath()%>/views/company/companyProfile.jsp" id="companyProfile">기본정보</a>
		<a class="item" href="<%= request.getContextPath()%>/selectList.rec" id="companyJobOpening">전체 채용공고</a>
		<a class="item" href="<%= request.getContextPath()%>/selectList.ke?type=3" id="jobPostingRegistration">공고등록</a>
		<a class="item" href="<%= request.getContextPath()%>/selectList.rec?type=1" id="applicantManagement">지원자관리</a>
		<a class="item" href="<%= request.getContextPath()%>/intro.atc" id="companyPage">기업정보</a>
		</div>
	</div>
	<!-- 이력서 서브메뉴 -->
	<div class="ui centered grid" style="display:none; height:50px; position:fixed; top:30px; left:0; right:0; background:white; opacity:0.6; z-index:98;" id="submenu2">
		<div class="ui secondary menu">
		<a class="item" href="<%= request.getContextPath()%>/recruitList" id = "myRecruitList">채용광고</a>
		<a class="item" href="<%= request.getContextPath()%>/selectViewCount">인재열람</a>
		<a class="item" href="<%= request.getContextPath()%>/testDatepicker">배너광고</a>
		<a class="item" href="<%= request.getContextPath()%>/allPaymentList">결제내역</a>
		<a class="item" href="<%= request.getContextPath()%>/usingPerson">이용내역</a>
		</div>
	</div>	
	<!-- 로그인 정보 서브메뉴-->
	<%if(loginUser != null){ %>
	<div class="loginInfo" id="submenu3">
		<div class="ui vertical menu" style="width:120px; padding:0px;">
			<div class="loginMenu" style="">
				<%=loginUser.getUserName() %>님</div>
			<a class="item">내 정보</a>
			<a class="item" href="<%=request.getContextPath()%>/logout">로그아웃</a>
		</div>
	</div>
	<%} %>

<script>
	$(function() {
		$("#comPage").hover(function() {
			$("#submenu1").show();
			$("#submenu2").hide();
			$("#submenu3").hide();
		});
		$("#submenu1").mouseleave(function() {
			$(this).hide();
		});

		$("#premium").hover(function() {
			$("#submenu1").hide();
			$("#submenu2").show();
			$("#submenu3").hide();
		});
		$("#submenu2").mouseleave(function() {
			$(this).hide();
		});
		$("#logout").hover(function(){
			$(this).css("cursor", "pointer");
			$("#submenu1").hide();
			$("#submenu2").hide();
			$("#submenu3").show();
		});
		$("#submenu3").mouseleave(function(){
			$(this).hide();
		});

		$("#search, #customer").hover(function() {
			$("#submenu1").hide();
			$("#submenu2").hide();
		});
		
		$("#pers").hover(function(){
			$(this).css("color","black");
		}, function(){
			$(this).css("color","gray");
		});
		
		$("#home").hover(function(){
			$(this).css("cursor","pointer");
		}).click(function(){
			window.location.href="<%= request.getContextPath()%>/views/main/companyMain.jsp";
		});

	});
	
</script>











