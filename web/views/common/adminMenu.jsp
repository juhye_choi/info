<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="member.model.vo.*"%>
<% Member loginUser =  (Member) session.getAttribute("loginUser"); 

	if(loginUser == null || !loginUser.getCategory().equals("A")){
		request.setAttribute("msg", "관리자가 아니시네요?!");
		request.getRequestDispatcher("/views/common/needLogin.jsp").forward(request, response);
		
	}else if(loginUser.getCategory().equals("A")){  %>
	
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/menuStyle.css">
	<div style="height: 50px; background: white;"></div>

	<div class="ui center aligned grid" style="position: fixed; top: 0; left: 0; right: 0; margin: 0px; background: white; z-index: 99; height: 50px; border-bottom: 1px solid #e7e6e1;">
	<div class="left floated left aligned four wide column">
		<a id="home"> <img src="<%=request.getContextPath()%>/images/ITDuck.png" class="itduck"></a>
	</div>
	<div class="center aligned eight wide column">
		<a class="menu underline" href="<%=request.getContextPath() %>/SelectPersonListPage.ad?currentPage=1" id="pCustom">개인회원 관리</a>
		<a class="menu underline" href="<%=request.getContextPath() %>/SelectCompanyListPage.ad?currentPage=1" id="cCustom">기업회원 관리</a>
		<a class="menu underline" href="<%=request.getContextPath() %>/declarationList.adl" id="declare">신고접수 관리</a>
		<a class="menu underline" href="<%=request.getContextPath() %>/adminallPaymentList" id="money">수익관리</a>
		<a class="menu underline" href="<%=request.getContextPath() %>/views/admin/csInfo.jsp" id="csPage">고객센터 관리</a>

	</div>
	<div class="right floated right aligned four wide column">
		<a class="menu" href="<%=request.getContextPath()%>/logout"
			style="color: gray;" id="comp">로그아웃</a>

	</div>
</div>

<!-- 개인회원 서브메뉴 -->
<div class="ui centered grid"
	style="display: none; height: 50px; position: fixed; top: 32px; left: 0; right: 0; background: white; opacity: 0.6; z-index: 98;"
	id="submenu1">
	<div class="ui secondary menu">
		<a class="item" href="<%=request.getContextPath() %>/SelectPersonListPage.ad?currentPage=1" id="pCustom">회원정보 조회</a>
		<a class="item" href="<%=request.getContextPath()%>/selectList.refall">추천이력서 관리</a>
		<a class="item" href="<%=request.getContextPath()%>/selectRefList.rhl">추천이력서 신청</a>
		<a class="item" href="<%=request.getContextPath()%>/selectListPage.ke?currentPage=1">키워드관리</a>
		<a class="item" style="visibility:hidden;">블랙리스트</a>
	</div>
</div>
<!-- 기업회원 서브메뉴 -->
<div class="ui centered grid"
	style="display: none; height: 50px; position: fixed; top: 32px; left: 0; right: 0; background: white; opacity: 0.6; z-index: 98;"
	id="submenu2">
	<div class="ui secondary menu">
		<a class="item" href="<%=request.getContextPath() %>/SelectCompanyListPage.ad?currentPage=1" id="cCustom">기업정보 조회</a>
		<a class="item" href="<%=request.getContextPath() %>/SelectCompanyApproval.ad?currentPage=1" >기업 가입승인</a>
		<a class="item" href="<%=request.getContextPath() %>/SelectCompanyRecruitList.ad?currentPage=1">공고게시물 관리</a>
	</div>
</div>

<!--  신고접수 서브메뉴 -->
<div class="ui centered grid"
	style="display: none; height: 50px; position: fixed; top: 32px; left: 0; right: 0; background: white; opacity: 0.6; z-index: 98;"
	id="submenu3">
	<div class="ui secondary menu">
		<a class="item" href="<%=request.getContextPath() %>/declarationList.adl">회원신고</a>
		<%-- <a class="item" href="<%=request.getContextPath() %>/views/admin/companyDeclaration.jsp">기업신고</a> --%>
		<a class="item" href="<%=request.getContextPath() %>/blackList.sab">블랙리스트</a>
		<a class="item" href="<%=request.getContextPath() %>/warningList.awl">경고리스트</a>
	</div>
</div>

<!-- 수익관리 서브메뉴 -->
<div class="ui centered grid"
	style="display: none; height: 50px; position: fixed; top: 32px; left: 0; right: 0; background: white; opacity: 0.6; z-index: 98;"
	id="submenu4">
	<div class="ui secondary menu">
		<a class="item" href="<%=request.getContextPath()%>/adminallPaymentList">정산내역</a>
		<a class="item" href="<%=request.getContextPath()%>/selectAdverAcc">광고배너 승인</a>
		<a class="item" href="<%=request.getContextPath()%>/views/admin/adverAdminManage.jsp">광고배너 관리</a>
		<a class="item" href="<%=request.getContextPath()%>/popRecruitListManage">적극 채용 공고 관리</a>
	</div>
</div>

<!-- 고객센터 서브메뉴 -->
<div class="ui centered grid"
	style="display: none; height: 50px; position: fixed; top: 32px; left: 0; right: 0; background: white; opacity: 0.6; z-index: 98;"
	id="submenu5">
	<div class="ui secondary menu">
		<a class="item"  href="<%=request.getContextPath() %>/views/admin/csInfo.jsp">고객센터 관리</a>
		<a class="item"  href="<%=request.getContextPath()%>/selectEmailList.ad?currentPage=1">이메일 문의 관리</a>
	</div>
</div>
 
<%
	}
%>
	<script>
		$(function() {
			$("#pCustom").hover(function() {
				$("#submenu1").show();
				$("#submenu2").hide();
				$("#submenu3").hide();
				$("#submenu4").hide();
				$("#submenu5").hide();
			});
			$("#submenu1").mouseleave(function() {
				$(this).hide();
			});
	
			$("#cCustom").hover(function() {
				$("#submenu1").hide();
				$("#submenu2").show();
				$("#submenu3").hide();
				$("#submenu4").hide();
				$("#submenu5").hide();
			});
			$("#submenu2").mouseleave(function() {
				$(this).hide();
			});
			
			$("#declare").hover(function() {
				$("#submenu1").hide();
				$("#submenu2").hide();
				$("#submenu3").show();
				$("#submenu4").hide();
				$("#submenu5").hide();
			});
			$("#submenu3").mouseleave(function() {
				$(this).hide();
			});
	
			$("#money").hover(function() {
				$("#submenu1").hide();
				$("#submenu2").hide();
				$("#submenu3").hide();
				$("#submenu4").show();
				$("#submenu5").hide();
			});
			$("#submenu4").mouseleave(function() {
				$(this).hide();
			});
			
			
			$("#csPage").hover(function() {
				$("#submenu1").hide();
				$("#submenu2").hide();
				$("#submenu3").hide();
				$("#submenu4").hide();
				$("#submenu5").show();
			});
		
			$("#submenu5").mouseleave(function() {
				$(this).hide();
			});
			
			$("#comp").hover(function(){
				$(this).css("color","black");
			}, function(){
				$(this).css("color","gray");
			});
			
			$("#home").hover(function(){
				$(this).css("cursor", "pointer");
			}).click(function(){
				window.location.href="<%= request.getContextPath()%>/selectCount.me";
			});
	
		});
	</script>
	
	
	
	
	
	
