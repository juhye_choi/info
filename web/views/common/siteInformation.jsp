<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="ui center aligned three column grid">
	<div class="column area" style="padding-top:50px;">
		<h2>개인 회원에게</h2>
		<p class="infoContents">이력서를 정기적 업데이트를 해보세요!<br>
		정기적인 이력관리로<br>
		자신의 가치를 향상시키고,<br>
		인기이력서에 도전해보세요!</p>
		<button class="huge ui orange basic button" id="site1">시작하기</button>
	</div>
	<div class="column area" style="padding-top:50px;">
		<h2>기업 회원에게</h2>
		<p class="infoContents">기업에 알맞은 인재를 찾아보세요!<br>
		원하는 인재를 키워드로 검색해서,<br>
		인재채용에 성공해보세요!</p>
		<button class="huge ui orange basic button" id="site2">시작하기</button>
	</div>
	<div class="column area" style="padding-top:50px;">
		<h2>IT개발자라면 누구나</h2>
		<p class="infoContents">경력을 이력으로 남길수있습니다.<br>
		경력 이력을 정기적으로 기록하시면,<br>
		언제든 사용가능합니다.<br>
		지금 시작해보세요!</p>
		<button class="huge ui orange basic button" id="site3">시작하기</button>
	</div>
</div>
