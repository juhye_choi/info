<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% String msg = (String) request.getAttribute("msg"); 
	int errorCode = 0;
	if(request.getParameter("errorCode") != null){
		errorCode = Integer.parseInt(request.getParameter("errorCode"));
	}

%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>에러페이지</title>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>
	<h1 align="center"><%= msg %></h1>
	
	
	<script>
	var msg = "<%= msg %>";
	var path = "";
	<%if(errorCode == 0){ %>
		path = "<%=request.getContextPath()%>/index.jsp";
	<%} else if(errorCode ==1){%>
		path= "<%=request.getContextPath()%>/selectOne.re";
	<%} else if(errorCode == 2){%>
		path="<%=request.getContextPath()%>/selectCount.me";
	<%} else if(errorCode == 3){%>
		path="<%=request.getContextPath()%>/selectList.ke?type=3";
	<%}%>
	
	swal ({
		   title : msg,
		   icon : "error"
		}).then((value) => {
		   location.href=path; 
		});
	
	</script>
</body>
</html>