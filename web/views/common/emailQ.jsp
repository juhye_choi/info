<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="member.model.vo.*"%>
    
    <%
    Member loginUser = (Member) request.getSession().getAttribute("loginUser");
    


    
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/js/html2canvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<%@ include file="/views/common/import.html" %>

<style>
body{
		text-align:center;
		padding-top:5%;
		margin:auto;
	}
	
	.memberChange{
		width:15%;
		height:40px;
		margin:3px;
		background:white;
		border:2px solid black;
		font-size:17px;
	}
	.memberChange:hover{
	cursor:pointer;
}
	
	#idCheck {
		background:#CC33FF;
		color:white;
	}
	
		#allDiv {
	margin-left: 13%;
	margin-right: 8%;
	margin-top: 25px;
}

		#div {
	margin-left: 10%;
	margin-right: 8%;
	margin-top: 50px;
}

		#div2 {
	margin-left: 2%;
	margin-right: 8%;
	margin-top: 20px;
}




td {
	vertical-align: top;
	padding-top: 10px;
}



</style>
</head>
<body>



<h1 style="margin-right: 65%;">이메일 문의</h1>





<div id="allDiv">

<hr>

<h4 style="text-align:left;">평일 09시에서 17시까지 문의하신 내용은 당일 답변해드립니다.<br>17시 이후에 문의하신 내용은 다음날에 답변, 주말에 문의하신 내용은 그 다음주 월요일에 답변해드립니다.</h4>

<br>

<form id="emailQForm" action="<%=request.getContextPath()%>/emailQ.me" method="post">

<table class="ui definition table" bolder="1" style="width:80%; margin:auto;">


  
  <tbody>
    <tr>
      <td style="text-align:center; padding-top:2%;">*이메일</td>
      <td><div class="ui input"><input type="email" style="width:300px;" id="email" name="email" value="<%= loginUser.getEmail()%>" readonly></div></td>
     
    </tr>
    
    <tr>
      <td style="text-align:center">*구분</td>
      <%if(loginUser.getCategory().equals("P")){ %>
       <td>
    <div class="ui radio checkbox"><input type="radio" id="person" name="cate" value="개인" checked disabled>
	<label for="person">개인</label></div>
						
	<div class="ui radio checkbox" style="padding-left:10px;"><input type="radio" id="company" name="cate" value="기업" disabled>
	<label for="company">기업</label></div>
						
	
      </td>
       <td><input type="hidden" name="cate1" id="cate1" value="1"></td>
      
      <%}else { %>
           <td>
    <div class="ui radio checkbox"><input type="radio" id="person" name="cate" value="개인" disabled>
	<label for="person">개인</label></div>
						
	<div class="ui radio checkbox" style="padding-left:10px;"><input type="radio" id="company" name="cate" value="기업" checked disabled>
	<label for="company">기업</label></div>
						
	
      </td>
      <td><input type="hidden" name="cate1" id="cate1" value="2"></td>
      
      <%} %>
     
     
    </tr>
    
      <tr>
      <td style="text-align:center; padding-top:2%;">*제목</td>
      <td><div class="ui input"><input type="text" id="title" name="title" style="width:300px;"></div></td>
     
    </tr>
    
     <tr>
      <td style="text-align:center; padding-top:7%;">*내용</td>
      <td><div class="ui input">
      <textarea id="content" name="content" style="height:150px; width:500px; resize:none;" placeholder="내용을 입력해주세요." ></textarea></div></td>
      
   
   
    <td><input type="hidden" id="uno" name="uno" value="<%= loginUser.getUno()%>"></td>  
    </tr>
    
   
    
   
   
   

    
   
    </tbody>



</table>

</form>

<div id="div2">
<%if(loginUser.getCategory().equals("P")) { %>
<a href="<%= request.getContextPath() %>/views/person/customerService.jsp"> <button class="ui secondary button">취소하기</button></a>
<%}else if(loginUser.getCategory().equals("C")) {%>
<a href="<%= request.getContextPath() %>/views/company/companyCustomerService.jsp"> <button class="ui secondary button">취소하기</button></a>
<%} %>
<input type="button"  onclick="submit();" class="ui button" value="문의하기">

</div>


</div>

<script>

function submit() {
	
	swal({
		  "title": "이메일 문의",
		  "text": "해당 내용으로 이메일 문의를 보내시겠습니까?",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		}).then((willDelete) => {
			
			 if (willDelete) {
				 $("#emailQForm").submit();
			 }else{
				 swal("이메일 문의 보내기가 실패했습니다!");
				 
				 
			 }
			
		});
	
	
}



</script>











</body>
</html>