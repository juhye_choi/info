<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%	
	int countCompany = 1384;
	int countPerson = 3872;
	int countReference = 2493;

	if((Integer)request.getAttribute("countCompany") != null || (Integer)request.getAttribute("countPerson") != null || (Integer)request.getAttribute("countReference") != null){
		countCompany = (Integer)request.getAttribute("countCompany");
		countPerson = (Integer)request.getAttribute("countPerson");
		countReference = (Integer)request.getAttribute("countReference");
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 페이지</title>
<%@ include file="/views/common/import.html" %>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<style>
	.counterArea{
		margin-right:auto;
		margin-left:auto;
		background:white;
		width:90%;
		padding:40px;
		border-radius:20px;
		text-align:center;
	}
	.graphArea{
		margin-top:50px;
		margin-right:auto;
		margin-left:auto;
		width:90%;
		height:50%;
		text-align:center;
		font-size:1.2em;
	}
	
    span {
        font-size: 66px;
        color: #555;
        margin-bottom: 40px;
        display: inline-block;
        font-weight: 400;
        text-align: center;
    }
    @media only screen and (max-width: 1024px) {
        span {
            font-size: 33px;
            margin-bottom: 200px;
        }
    }
    @media only screen and (max-width: 800px) {
        div > span {
            font-size: 66px;
            display: block;
            width: 100% !important;
            margin-bottom: 100px;
        }
        span {
            font-size: 66px;
        }
        code {
            margin-bottom: 100px;
        }
    }
    .label{
    	font-size:2em;
    	color:gray;
    }
  </style>
  <script>
    jQuery(document).ready(function() {
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
    });
  </script>
</head>
<body style="background:lightgray;">
	<%@ include file="../common/adminMenu.jsp" %>
	<h1 style="margin-left:100px; color:gray;">관리자페이지</h1>
	<div class="counterArea">
		<div>
	        <span class="label" style="display: inline-block; width: 32%">기업 회원</span>
	        <span class="label" style="display: inline-block; width: 32%">개인 회원</span>
	        <span class="label" style="display: inline-block; width: 32%">총 추천이력서</span>
	    </div>
		<div>
	        <span class="counter" style="display: inline-block; width: 32%; border-right:1px solid gray;"><%=countCompany %></span>
	        <span class="counter" style="display: inline-block; width: 32%; border-right:1px solid gray;"><%=countPerson %></span>
	        <span class="counter" style="display: inline-block; width: 32%"><%=countReference %></span>
	    </div>
    </div>
    <div class="graphArea">
	   <!--  !아래는 그래프 영역!<br> -->
	</div>
	
    <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="<%=request.getContextPath()%>/js/jquery.counterup.min.js"></script>
</body>
</html>



