<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
    
      
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>IT Duck</title>
<%@ include file="/views/common/import.html" %>
<style>
	img {
		display:block;
		margin-left:auto;
		margin-right:auto;
		width:100%;
	}
	.bridge{
		background-image: url("<%=request.getContextPath()%>/images/bridge.jpg");
		background-position: center;
		min-width:100%;
		background-size:cover;
		height:600px;
		padding-top:100px;

	}
	h1 {
		text-align:center;
		font-size:100px;
	}
	h3{
		text-align:center;
	}
</style>
</head>
<body>
	<%@ include file="../common/companyMenu.jsp" %>
	<!-- <img src="images/bridge.jpg"> -->
	<div class="bridge">


		<h3>개인과 기업을 잇다.</h3>
		<h1>IT DUCK</h1>
		<h3 style="color:red;">마스터파일 고치시면 혼날각오하세요.</h3>
		<h1>👿</h1>
		<h3>(기업회원 메인페이지)</h3>
	</div>
	
	<%@ include file="../common/siteInformation.jsp" %>



	<%@ include file="../common/ourfooter.jsp" %>	

	<script>
		$(function(){
			$("#logout").css("display","inline");
			$("#login").css("display","none");
		});
	</script>
</body>
</html>











