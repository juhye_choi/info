<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, member.model.vo.*, company.model.vo.*, keyword.model.vo.*,admin.model.vo.*"%>
    
    
    
    <%
    
    
    
    ArrayList<HashMap<String, Object>> list = ( ArrayList<HashMap<String, Object>>)request.getAttribute("list");
 
    
  
    
    for(int i = 0; i < list.size(); i++) { 
  		HashMap<String, Object> hmap = list.get(i);
  		
    }
    

 
   
    
    CompanyPageInfo pi = (CompanyPageInfo) request.getAttribute("pi");
    int listCount = pi.getListCount();
   	int currentPage = pi.getCurrentPage();
   	int maxPage = pi.getMaxPage();
   	int startPage = pi.getStartPage();
   	int endPage = pi.getEndPage(); 
     
    
    
    %>
    
    
   
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 페이지</title>
<%@ include file="/views/common/import.html" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
	.outline{
		margin-right:auto;
		margin-left:auto;
		width:80%;
		height:800px;   
		background:white;
		padding:50px;
	}
	.searchSection{
		padding-top:20px;
		padding-right:20%;
		padding-left:20%;
		padding-bottom:20px;
		border:1px solid lightgray;
		border-top:2px solid purple;
		text-align:center;
	}
	.tableSection{
		padding-top:20px;
	}
	
	.radioTable td{
		padding:10px;
		font-size:1.2em;
	}
	
	.buttonStyle {
		margin-right:5px !important;
		margin-left:5px !important;
	}
	
	#logTable td:last-of-type {
		width:180px;
	}
	
	h1{
		margin-left:11%;
	}
	
	.fields {
	
	padding-left:20%;
	
	
	}
	.pagingArea{
		text-align:center;
		padding:20px;
		margin-bottom:50px;
		margin-top:30px;
	}
	.pagingbtn{
		height:30px;
		width:30px;
		display:inline-block;
		vertical-align:top;
		overflow:hidden;
		background:white;
		border:1px solid lightgray;
		border-radius:5px;
	}
	.pagingbtn:hover{
		cursor:pointer;
	}
	.pagingbtn:disabled{
		color:gray;
	}
	
	
</style>
</head>
<body style="background:lightgray;">
	<%@ include file="../common/adminMenu.jsp" %>
	<h1>기업 가입승인</h1>
	
	<div class="outline">
		<div class="searchSection">
			<form class="ui form" action="" method="get">
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown">
							<option>회사명</option>
							<option>회원ID</option>
							
							<option>담당자이메일</option>
						</select>
					</div>
						<div class="ui icon input">
  						<input type="text" placeholder="Search..." disabled>
  						<i class="circular search link icon"></i>
					</div>
				</div>
				<table align="center" class="radioTable">
				
					<tr>
						<td colspan="5"><input type="submit" value="검색" class="ui purple button" style="width:150px;"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead align="center">
					<tr>
						<th>회원번호</th>
						<th>회사명</th>
						<th>회원ID</th>
						
						<th>담당자 이메일</th>
						
						<th>승인/삭제</th>
						<th>조회</th>
					</tr>
				</thead>
				<tbody align="center">
				<%for(int j = 0; j < list.size(); j++){ %>
					<tr>
						<td><%= list.get(j).get("uno") %></td>
						<td><%= list.get(j).get("name") %></td>
						<td><%=list.get(j).get("userId") %></td>
						<td><%= list.get(j).get("email") %></td>
						
						<td><input type="button" class="ui button mini buttonStyle" onclick="ok();" value="기업승인"><input type="button" class="ui red button mini buttonStyle" onclick="miniModal()" value="승인거부"></td>
						<td><input type="button" class="ui button mini buttonStyle" onclick="modal();"value="조회"></td>
						
						
						 <td style="display:none;"><input type="hidden" id="phone" value="<%= list.get(j).get("phone") %>" ></td> 
						<td style="display:none;"><input type="hidden" id="registNum" value="<%= list.get(j).get("registNum") %>" ></td>
						 <td style="display:none;"><input type="hidden"  id="owner" value="<%= list.get(j).get("owner") %>" ></td> 
					</tr>
					<%} %>
					
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="pagingArea">
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyApproval.ad?currentPage=1'"><<</button>
			<%if(currentPage <=1) {%>
				<button class="pagingbtn" disabled></button>
			<%} else{ %>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyApproval.ad?currentPage=<%=currentPage-1%>'"><</button>
			<%} %>
			
			<%for(int p=startPage; p<=endPage; p++){
				if(p == currentPage){ %>
					<button class="pagingbtn" disabled><%=p %></button>
				<%} else{ %>
					<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyApproval.ad?currentPage=<%=p%>'"><%=p %></button>
				<%}
			  } %>
			
			<%if(currentPage >= maxPage){ %>
				<button class="pagingbtn" disabled></button>
			<%} else {%>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyApproval.ad?currentPage=<%=currentPage+1%>'">></button>
			<%} %>
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyApproval.ad?currentPage=<%=maxPage%>'">>></button>
		</div>
	
	
	<div class="ui modal">
  <i class="close icon"></i>
  <div class="header">기업 상세조회</div>
 
    <div class="description">
      <table align="center" class="ui definition table" bolder="1" >

   <tbody>
     <tr>
      <td style="text-align:center; width:250px;">사업자 등록번호</td>
      <td id="registNum2"></td>
     
    </tr>
    
     <tr>
      <td style="text-align:center; width:250px;">회사명</td>
      <td id="name2"></td>
     
    </tr>
    
     <tr>
      <td style="text-align:center; width:250px;">대표자명</td>
      <td id="owner2"></td>
     
    </tr>
    
     <tr>
      <td style="text-align:center; width:200px;">연락처</td>
      <td id="phone2"></td>
     
    </tr> 
    </tbody> 



</table>
    </div>
    <div class="actions">
    <div class="ui black deny button" id="modalCancle">
      닫기
    </div>
    
  </div>
  </div>
  

	
	<script>
		$(function(){
			$("#logTable").tablesort();
			
			
$("#logTable input[type=button]").mouseenter(function(){
				
				$(this).parent().parent().css({"background":"skyblue","cursor":"pointer"})
				
			}).mouseout(function(){
				
				$(this).parent().parent().css({"background":"white"});
				
			}).click(function(){
				
				
					
var num = $(this).parent().parent().children().eq(0).text();
					
				if($(this).attr('value') == "기업승인") {
					
					var check =	confirm("해당기업을 승인하시겠습니까?");
					if(check == true) {
						
					
					 location.href = "<%=request.getContextPath()%>/ApprovalCompanyOne.ad?num=" + num;
					}
				
					
				}else if($(this).attr('value') == "승인거부") {
					
					var check =	confirm("해당기업을 승인거부하시겠습니까?");
	            	if(check == true) {
	            		 location.href = "<%=request.getContextPath()%>/deleteCompany.ad?num=" + num + "&type=1";	 
	            		
	            	}
					
					
				
					
				}
					
				
				
else if($(this).attr('value') == "조회") {
					
	
	            	
			var registNum = $(this).parent().parent().find("#registNum").val();
			var name = $(this).parent().parent().children().eq(1).text();
			 var owner =  $(this).parent().parent().find("#owner").val();
			var phone =  $(this).parent().parent().find("#phone").val();
			
			console.log(name);
			console.log(owner); 
			console.log(phone); 
			console.log(registNum);
			
			
			$("#registNum2").text(registNum);
			$("#name2").text(name);
			 $("#owner2").text(owner);
			$("#phone2").text(phone); 
					
			$('.ui.modal').modal('show');
					
				}
					
				
				
				
				
				
				
			})
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		});
		
		function modal() {
			
			
			
		}
		
		
		
	
	</script>
</body>
</html>





