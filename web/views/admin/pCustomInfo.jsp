<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, member.model.vo.*, person.model.vo.Person,keyword.model.vo.*"%>
    
   
    
    <%

    ArrayList<HashMap<String, Object>> list = ( ArrayList<HashMap<String, Object>>)request.getAttribute("list");
  
    for(int i = 0; i < list.size(); i++) { 
  		HashMap<String, Object> hmap = list.get(i);
    }
    
    PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
    %>
   
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 페이지</title>
<%@ include file="/views/common/import.html" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
	.outline{
		margin-right:auto;
		margin-left:auto;
		width:80%;
		height:800px;
		background:white;
		padding:50px;
	}
	.searchSection{
		padding-top:20px;
		padding-right:20%;
		padding-left:20%;
		padding-bottom:20px;
		border:1px solid lightgray;
		border-top:2px solid purple;
		text-align:center;
	}
	.tableSection{
		padding-top:20px;
	}
	
	.radioTable td{
		padding:10px;
		font-size:1.2em;
	}
	
	.buttonStyle {
		margin-right:5px !important;
		margin-left:5px !important;
	}
	
	#logTable td:last-of-type {
		width:180px;
	}
	
	h1{
		margin-left:11%;
	}
	
	.fields {
	
	padding-left:20%;
	
	
	}
	.eachArea{
		display:inline-block;
		margin:2px;
	}
	.pagingArea{
		text-align:center;
		padding:20px;
		margin-bottom:50px;
		margin-top:30px;
	}
	.pagingbtn{
		height:30px;
		width:30px;
		display:inline-block;
		vertical-align:top;
		overflow:hidden;
		background:white;
		border:1px solid lightgray;
		border-radius:5px;
	}
	.pagingbtn:hover{
		cursor:pointer;
	}
	.pagingbtn:disabled{
		color:gray;
	}
	
	
</style>
</head>
<body style="background:lightgray;">
	<%@ include file="../common/adminMenu.jsp" %>
	<h1>개인회원 정보조회</h1>
	
	<div class="outline">
		<div class="searchSection">
			<form class="ui form" action="" method="get">
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown" id="searchCategory">
							<option value="1">회원이름</option>
							<option value="2">회원ID</option>
							<option value="3">이메일</option>
							
						</select>
					</div>
						<div class="ui icon input">
  						<input type="text" id="searchText" placeholder="Search...">
  						<i class="circular search link icon"></i>
					</div>
				</div>
				<table align="center" class="radioTable">
				
					<tr>
						<td colspan="5"><input type="button" value="검색" class="ui purple button" style="width:150px;" onclick="searchKeyword('search')"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead align="center">
					<tr>
					<th>회원번호</th>
						<th>회원이름</th>
						<th>회원ID</th>
						<th>이메일</th>
						
						<th>현재 포인트</th>
						<th>조회/삭제처리</th>
					</tr>
				</thead>
				<tbody align="center">
					<%for(int j = 0; j < list.size(); j++){ %>
					
					<tr>
					<td><%= list.get(j).get("uno") %></td>
					<td><%= list.get(j).get("name") %></td>
					<td><%=list.get(j).get("userId") %></td>
					<td><%= list.get(j).get("email") %></td>
					
					<td><%= list.get(j).get("nowPoint") %></td>
					<td><input type="button" class="ui button mini buttonStyle" id="personSelect" value="회원조회"><input type="button" class="ui red button mini buttonStyle" id="personDelete" value="탈퇴"></td>
					</tr>
					
					<%} %>
					
					
				</tbody>
			</table>
		</div>
		<div class="pagingArea" id="pagingArea">
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectPersonListPage.ad?currentPage=1'"><<</button>
			<%if(currentPage <=1) {%>
				<button class="pagingbtn" disabled></button>
			<%} else{ %>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectPersonListPage.ad?currentPage=<%=currentPage-1%>'"><</button>
			<%} %>
			
			<%for(int p=startPage; p<=endPage; p++){
				if(p == currentPage){ %>
					<button class="pagingbtn" disabled><%=p %></button>
				<%} else{ %>
					<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectPersonListPage.ad?currentPage=<%=p%>'"><%=p %></button>
				<%}
			  } %>
			
			<%if(currentPage >= maxPage){ %>
				<button class="pagingbtn" disabled></button>
			<%} else {%>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectPersonListPage.ad?currentPage=<%=currentPage+1%>'">></button>
			<%} %>
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectPersonListPage.ad?currentPage=<%=maxPage%>'">>></button>
		</div>
	</div>
	
	<script>
	
	function searchKeyword(num){
		var searchCategory = $("#searchCategory").val();
		var searchText = "";
		var scurrentPage = 0;
		
		if(num == 'search'){
			scurrentPage = 1;
		} else{
			//console.log(num.innerText);
			if(num.innerText == '>>'){
				scurrentPage = $("#lastPage").val();
			} else{
				scurrentPage = num.innerText;
			}
		}
		
		if(searchCategory == 1){ //키워드명 검색
			searchText = $("#searchText").val();
		}else if(searchCategory == 2){ //키워드명 검색
			searchText = $("#searchText").val();
		}else if(searchCategory == 3){ //키워드명 검색
			searchText = $("#searchText").val();
		}
		
		if(searchText == ""){
			alert("검색어를 입력해주세요.");
		}else{
			
			$.ajax({
				
				url : "searchP.ad",
				data : {
					currentPage:scurrentPage,
					searchCategory:searchCategory,
					searchText:searchText,
					
				},
				type:"get",
				success:function(data){
					
					$tableBody = $("#logTable tbody");
					$pagingArea = $("#pagingArea").html('');
					
					$tableBody.html('');
					
					var member = data.Memberlist;
					
					var scurrentPage = data.scurrentPage;
					var slistCount = data.slistCount;
					var smaxPage = data.smaxPage;
					var sstartPage = data.sstartPage;
					var sendPage = data.sendPage;
					
					$.each(member, function(index, value){
						var $tr = $("<tr align='center'>");
						var $codeTd = $("<td>").text(value.uno);
						
						var res = decodeURIComponent(value.name);
						
						var $nameTd = $("<td>").text(res);
						var $userIdTd = $("<td>").text(value.userId);
						var $emailTd = $("<td>").text(value.email);
						var $nowPointTd = $("<td>").text(value.nowPoint);
						
					
					 	var $addModTd = $("<td>").append("<input type='button' class='ui button mini buttonStyle' id='personSelect' value='회원조회'><input type='button' class='ui red button mini buttonStyle' id='personDelete' value='탈퇴'>"); 
						
						
						$tr.append($codeTd);
						$tr.append($nameTd);
						$tr.append($userIdTd);
						$tr.append($emailTd);
						
						$tr.append($nowPointTd);
						 $tr.append($addModTd); 
						
						$tableBody.append($tr);
						
					});
					
					//페이징 처리!
					var $pageDiv = new Array();
					var $button = new Array();
					var $input2 = new Array();
					
					if(scurrentPage != 1){
						$pageDiv1 = $("<div class='eachArea'>");
						$button1 = $("<button class='pagingbtn' onclick='searchKeyword(1)'>").text('<<');
						$pageDiv1.append($button1);
						$pagingArea.append($pageDiv1);
					} 
					
					for(var p=sstartPage; p<=sendPage; p++){
						if(p==scurrentPage){
							$pageDiv[p] = $("<div class='eachArea'>");
							$button[p] = $("<button class='pagingbtn' disabled>").text(p);
								
						} else{
							$pageDiv[p] = $("<div class='eachArea'>");
							$button[p] = $("<button class='pagingbtn' onclick='searchKeyword(this)'>").text(p);
							//$input2[p] = $("<input type='hidden' id=''>").val(p);
						}
						$pageDiv[p].append($button[p]).append($input2[p]);
						$pagingArea.append($pageDiv[p]);
					}
					if(scurrentPage != sendPage){
						$pageDiv2 = $("<div class='eachArea'>");
						$button2 = $("<button class='pagingbtn' onclick='searchKeyword(this)'>").text('>>');
						$input3 = $("<input type='hidden' id='lastPage'>").val(sendPage);
						$pageDiv2.append($button2);
						$pageDiv2.append($input3);
						$pagingArea.append($pageDiv2);
					}
					
					
					$("#logTable input[type=button]").mouseenter(function(){
						
						$(this).parent().parent().css({"background":"skyblue","cursor":"pointer"})
						
					}).mouseout(function(){
						
						$(this).parent().parent().css({"background":"white"});
						
					}).click(function(){
						
						
							
		var num = $(this).parent().parent().children().eq(0).text();
							
						if($(this).attr('value') == "회원조회") {
							
							location.href = "<%=request.getContextPath()%>/SearchPersonOne.ad?num=" + num;
							console.log(num);
							
						}else if($(this).attr('value') == "탈퇴") {
							
							var check =	confirm("정말 강제로 탈퇴 시킵니까?");
			            	if(check == true) {
			            		location.href = "<%=request.getContextPath()%>/deletePerson.ad?num=" + num + "&type=2";	
			            		
			            	}
							
							
						
							
						}
							
						
						
						
							
						
						
						
						
						
						
					})
					
					
					
					
					
					
					
				},
				error:function(data){
					console.log("실패!");
				}
				
			})
			
			
			
			
		}
		
		
		
	}
	
	
		$(function(){
			
			
			
			$("#logTable input[type=button]").mouseenter(function(){
				
				$(this).parent().parent().css({"background":"skyblue","cursor":"pointer"})
				
			}).mouseout(function(){
				
				$(this).parent().parent().css({"background":"white"});
				
			}).click(function(){
				
				
					
var num = $(this).parent().parent().children().eq(0).text();
					
				if($(this).attr('value') == "회원조회") {
					
					location.href = "<%=request.getContextPath()%>/SearchPersonOne.ad?num=" + num;
					console.log(num);
					
				}else if($(this).attr('value') == "탈퇴") {
					
					var check =	confirm("정말 강제로 탈퇴 시킵니까?");
	            	if(check == true) {
	            		location.href = "<%=request.getContextPath()%>/deletePerson.ad?num=" + num + "&type=2";	
	            		
	            	}
					
					
				
					
				}
					
				
				
				
					
				
				
				
				
				
				
			})
			
			
			
			$("#logTable").tablesort();
			
			
			
			
		});
	</script>
</body>
</html>





