<%@page import="admin.model.vo.AllPayment"%>
<%@page import="java.util.ArrayList, keyword.model.vo.PageInfo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	ArrayList<AllPayment> list = (ArrayList<AllPayment>) request.getAttribute("list");
	
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
 	int endPage = pi.getEndPage();
 	
 	int totalPaymentCount=0;
 	int totalPaymentMoney=0;
 	int todayPaymentMoney=0;
 	
 	totalPaymentCount = (Integer)request.getAttribute("totalPaymentCount");
 	totalPaymentMoney = (Integer)request.getAttribute("totalPaymentMoney");
 	todayPaymentMoney = (Integer)request.getAttribute("todayPaymentMoney");
	
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/views/common/import.html"%>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<title>관리자페이지</title>
<style>
.outline {
	margin-right: auto;
	margin-left: auto;
	width: 80%;
	background: white;
	padding: 50px;
}

.searchSection {
	padding-top: 20px;
	padding-right: 20%;
	padding-left: 20%;
	padding-bottom: 20px;
	border: 1px solid lightgray;
	border-top: 2px solid purple;
	text-align: center;
}

.tableSection {
	padding-top: 20px;
}

.addArea {
	text-align: right;
	margin-top: 10px;
}

.tableSection td:nth-of-type(1) {
	width: 150px;
}

.tableSection td:nth-of-type(4) {
	width: 200px;
}

.radioTable td {
	padding: 10px;
	font-size: 1.2em;
}

.buttonStyle {
	margin-right: 5px !important;
	margin-left: 5px !important;
}

#logTable td:last-of-type {
	width: 180px;
}

h1 {
	margin-left: 11%;
}

.fields {
	padding-left: 20%;
}

.radioTable td {
	padding: 10px;
	margin-bottom: 10px;
	font-size: 1.2em;
}

table tr {
	text-align: center;
}

.pagingArea {
	text-align: center;
	padding: 20px;
	margin-bottom: 50px;
	margin-top: 30px;
}

.pagingbtn {
	height: 30px;
	width: 30px;
	display: inline-block;
	vertical-align: top;
	overflow: hidden;
	background: white;
	border: 1px solid lightgray;
	border-radius: 5px;
}

.pagingbtn:hover {
	cursor: pointer;
}

.pagingbtn:disabled {
	color: gray;
}
.counterArea{
		margin-right:auto;
		margin-left:auto;
		background:white;
		width:80%;
		padding:40px;
		border-radius:5px;
		text-align:center;
	}
	
    .counter{
    	font-size : 3em;
    	color:black;
    	margin-top: 30px;
    	display: inline-block;
    	font-weight: 1000px;
    	text-align: center;
    }
	.label{
    	font-size:2em;
    	color:black;
    	margin-bottom : 30px;
    	display: inline-block;
    	font-weight :1000px;
    	text-align: center;
    }
</style>
<script>
    jQuery(document).ready(function( $ ) {
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
    });
  </script>
</head>
<body>
<body style="background: lightgray;">
	<%@ include file="../common/adminMenu.jsp"%>
	<h1>정산내역 조회</h1>
	<div class="counterArea">
		<div>
		        <span class="label" style="display: inline-block; width: 32%">총 거래 수</span>
		        <span class="label" style="display: inline-block; width: 32%">총 매출</span>
		        <span class="label" style="display: inline-block; width: 32%">오늘 매출</span>
		</div>
		<div>
		        <span class="counter" style="display: inline-block; width: 32%; border-right:1px solid gray;"><%=totalPaymentCount%></span>
		        <span class="counter" style="display: inline-block; width: 32%; border-right:1px solid gray;"><%=totalPaymentMoney%></span>
		        <span class="counter" style="display: inline-block; width: 32%"><%=todayPaymentMoney%></span>
		</div>
	</div>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="<%=request.getContextPath()%>/js/jquery.counterup.min.js"></script>
    <Br><br>
	<div class="outline">
		<div class="searchSection">
			<div class="ui form">
			<table align="center" class="radioTable">
				<tr>
					<td><input type="radio" name="status" id="person" value="person" checked="checked"> <label for="conf">개인</label></td>
					<td><input type="radio" name="status" id="company" value="company"><label for="apply">기업</label></td>
					<td><input type="radio" name="status" id="all" value="all"><label for="all">전체</label></td>
				</tr>
			</table>
			<br>
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown" id = "selectdropdown">
							<option value = "1">구매자정보</option>
							<option value = "2">결제내용</option>
						</select>
					</div>
					<div class="ui icon input">
					<input type="text" placeholder="Search..." id = "searchText"><i class="circular search link icon"></i>
					</div>
				</div>
					<input type="button" value="검색" class="ui purple button" style="width: 150px;" onclick ="searchfunction('search')">
			</div>
		</div>
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead>
					<tr>
						<th>번호</th>
						<th>구분</th>
						<th>구매자정보</th>
						<th>내용</th>
						<th>금액</th>
						<th>결제일자</th>
						<th>환불여부</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<%for(int i=0; i<list.size(); i++) {%>
							<td><%=i+1 %></td>
							<td><!-- 개인 , 기업 구분 -->
								<%
									String Mcategory = "";
									if(list.get(i).getMcat().equals("C")){
										Mcategory = "기업";
									}else{
										Mcategory ="개인";
									}
								%>
								<%=Mcategory%>
							</td>
							<td><%=list.get(i).getUserid() %></td>
							<td><%=list.get(i).getPname() %></td>
							<td><%=list.get(i).getPmoney() %> 원</td>
							<td><%=list.get(i).getPDate() %></td>
							<td>
								<%if(list.get(i).getPcat() == 4){%>
									<button class="ui negative basic button backpayment">환불요청</button>
									<input type="hidden" value=<%=list.get(i).getPayid()%>>
								<%}else if(list.get(i).getPcat() == 5){%>
									<button class="ui orange basic button">환불완료</button>
								<%}%>
								
							</td>
							</tr>
						<%} %>
						
				</tbody>
			</table>
		</div>
		<div class="pagingArea" id ="pagingArea">
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/adminallPaymentList?currentPage=1'"><<</button>
			<%if(currentPage <=1) {%>
				<button class="pagingbtn" disabled></button>
			<%} else{ %>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/adminallPaymentList?currentPage=<%=currentPage-1%>'"><</button>
			<%} %>
			
			<%for(int p=startPage; p<=endPage; p++){
				if(p == currentPage){ %>
					<button class="pagingbtn" disabled><%=p %></button>
				<%} else{ %>
					<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/adminallPaymentList?currentPage=<%=p%>'"><%=p %></button>
				<%}
			  } %>
			
			<%if(currentPage >= maxPage){ %>
				<button class="pagingbtn" disabled></button>
			<%} else {%>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/adminallPaymentList?currentPage=<%=currentPage+1%>'">></button>
			<%} %>
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/adminallPaymentList?currentPage=<%=maxPage%>'">>></button>
		</div>
	</div>
	<script>
		$(function() {
			$("#logTable").tablesort();
		});
		
		
		
		function searchfunction(num){
			var searchRadio = $("input[name='status']:checked").val();
			var searchText = "";
			var searchSelect = $("#selectdropdown").val();
			var scurrentPage = 0;
			
			if(num == 'search'){
				scurrentPage = 1;
			}else{
				if(num.innerText == '>>'){
					scurrentPage = $("#lastPage").val();
				} else{
					scurrentPage = num.innerText;
				}
			}
			
			/* console.log(searchRadio);
			console.log(searchSelect); */
			
			if(searchSelect == 1 || searchSelect == 2 || searchSelect == 3 || searchSelect == 4 ){ 
				searchText = $("#searchText").val();
			} else{ //키워드 분류 검색
				searchText = "nothing";
			}
			if(searchText == ""){
				swal ({
					   title : "검색어를 입력해주세요",
					   icon : "error"
					}).then((value) => {
						$("#searchText").focus();
					});
			}else{
				$.ajax ({
					url : "searchAllPaymentList",
					data : {
						currentPage:scurrentPage,
						searchRadio:searchRadio,
						searchText:searchText,
						searchSelect:searchSelect
					},
					type :"get",
					success:function(data){
						console.log(data);
						
						$tableBody = $("#logTable tbody");
						$pagingArea = $("#pagingArea").html('');
						
						$tableBody.html('');
						
						var searchList = data.paymentList;
						
						var scurrentPage = data.scurrentPage;
						var slistCount = data.slistCount;
						var smaxPage = data.smaxPage;
						var sstartPage = data.sstartPage;
						var sendPage = data.sendPage;
						
						$.each(searchList , function(index , value){
							var $tr = $("<tr align='center'>");
							var mact = '';
							if(value.MCAT === 'C'){
								mact = '기업'
							}else if (value.MCAT === 'P'){
								mact = '개인'
							}
							var $number = $("<td>").text(index+1);
							var $mcatTd = $("<td>").text(mact);
							var $userIdTd = $("<td>").text(value.USERID);
							var res = decodeURIComponent(value.PNAME);
							res = replaceAll(res, "+" , " ");
							var $pnameTd = $("<td>").text(res);
							var $pmoneyTd = $("<td>").text(value.PMONEY);
							var $pdateTd = $("<td>").text(value.PDATE);
							
							$tr.append($number);
							$tr.append($mcatTd);
							$tr.append($userIdTd);
							$tr.append($pnameTd);
							$tr.append($pmoneyTd);
							$tr.append($pdateTd);
							
							$tableBody.append($tr);
						});
						// 페이징처리 
						var $pageDiv = new Array();
						var $button = new Array();
						var $input2 = new Array();
						
						if(scurrentPage != 1){
							$pageDiv1 = $("<div class='eachArea'>");
							$button1 = $("<button class='pagingbtn' onclick='searchfunction(1)'>").text('<<');
							$pageDiv1.append($button1);
							$pagingArea.append($pageDiv1);
						}
						
						for(var p=sstartPage; p<=sendPage; p++){
							if(p==scurrentPage){
								$pageDiv[p] = $("<div class='eachArea'>");
								$button[p] = $("<button class='pagingbtn' disabled>").text(p);
							}else{
								$pageDiv[p] = $("<div class='eachArea'>");
								$button[p] = $("<button class='pagingbtn' onclick='searchfunction(this)'>").text(p);
								
							}
							$pageDiv[p].append($button[p]).append($input2[p]);
							$pagingArea.append($pageDiv[p]);
						}
						if(scurrentPage != sendPage){
							$pageDiv2 = $("<div class='eachArea'>");
							$button2 = $("<button class='pagingbtn' onclick='searchfunction(this)'>").text('>>');
							$input3 = $("<input type='hidden' id='lastPage'>").val(sendPage);
							$pageDiv2.append($button2);
							$pageDiv2.append($input3);
							$pagingArea.append($pageDiv2);
						}
					},
					error:function(request,status,error){
				        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				    }
				})
			}
		}
		$(".backpayment").click(function() {
			var payid =  $(this).parent().find("input[type=hidden]").val();
			console.log(payid +"!!");
			if ( confirm("환불처리를 하시겠습니까??") ){
				location.href = "<%=request.getContextPath()%>/updateRefundcompleted?payid="+payid;
			}else{
				alert("취소되었습니다.");
			}
		});
		
		
		function replaceAll(sValue, param1, param2) {
			// replaceAll 이라는 변수 선언.
			return sValue.split(param1).join(param2);
		}
		
	
		
	</script>
</body>
</html>