<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, admin.model.vo.RefResume, keyword.model.vo.PageInfo"%>
<%
	ArrayList<RefResume> reflist = (ArrayList<RefResume>) request.getAttribute("reflist");
	
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();	
	%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 페이지</title>
<%@ include file="/views/common/import.html" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
	.outline{
		margin-right:auto;
		margin-left:auto;
		width:80%;
		height:950px;
		background:white;
		padding:50px;
	}
	.searchSection{
		padding-top:20px;
		padding-right:20%;
		padding-left:20%;
		padding-bottom:20px;
		border:1px solid lightgray;
		border-top:2px solid purple;
		text-align:center;
	}
	.tableSection{
		padding-top:20px;
		height:570px;
	}
	
	.radioTable td{
		padding:10px;
		font-size:1.2em;
	}
	
	.buttonStyle {
		margin-right:5px !important;
		margin-left:5px !important;
	}
	
	#logTable td:last-of-type {
		width:180px;
	}
	
	h1{
		margin-left:11%;
	}
	.pagingArea{
		text-align:center;
		padding:20px;
		margin-bottom:10px;
	}
	.eachArea{
		display:inline-block;
		margin:2px;
	}
	.pagingbtn{
		height:30px;
		width:30px;
		display:inline-block;
		vertical-align:top;
		overflow:hidden;
		background:white;
		border:1px solid lightgray;
		border-radius:5px;
	}
	.pagingbtn:hover{
		cursor:pointer;
	}
	.pagingbtn:disabled{
		color:gray;
	}
</style>
</head>
<body style="background:lightgray;">
	<%@ include file="../common/adminMenu.jsp" %>
	<h1>추천이력서 관리</h1>
	
	<div class="outline">
		<div class="searchSection">
			<form class="ui form" action="" method="get">
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown" id="searchCategory">
							<option value="1">아이디</option>
							<option value="2">해당분기</option>
						</select>
					</div>
					<div class="ten wide field">
						<div class="ui icon input fields" id="searchText"><i class="search icon"></i>
							<input type="text" placeholder="Search..."></div>
						<div class="fields" style="display:none;" id="searchQuarter">
							<div class="ten wide field">
								<div class="ui calendar" id="month_year_calendar">
									<div class="ui input left icon"><i class="calendar icon"></i><input type="text" placeholder="Date" readonly></div>
								</div>
							</div>
							<div class="six wide field">
								<input type="number" placeholder="분기" min="1" max="4">
							</div>
						</div>
					</div>
				</div>
				<table align="center" class="radioTable" id="radioTable">
					<tr>
						<td><input type="radio" name="status" id="conf" value="conf" checked="checked"> <label for="conf">등록/만료</label></td>
						<td><input type="radio" name="status" id="apply" value="apply"> <label for="apply">신청</label></td>
						<td><input type="radio" name="status" id="rej" value="rej"> <label for="rej">반려</label></td>
						<td><input type="radio" name="status" id="all" value="all"> <label for="all">전체</label></td>
					</tr>
					<tr>
						<td colspan="5"><input type="button" value="검색" class="ui purple button" style="width:150px;" onclick="searchRefResume('search');"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead>
					<tr align="center">
						<th style="width:50px;">구분</th>
						<th style="width:80px;">해당분기</th>
						<th style="width:150px;">날짜</th>
						<th style="width:120px;">회원아이디</th>
						<th style="width:130px;">회원이름</th>
						<th>내용</th>
						<th style="width:200px;">조회/등록취소</th>
					</tr>
				</thead>
				<tbody>	
					<% Calendar c = Calendar.getInstance(); String year = String.valueOf(c.get(Calendar.YEAR)); int qu = (int) Math.ceil((double)c.get(Calendar.MONTH)/3);
					for(RefResume r : reflist){ %>
						<tr align="center">
							<%if(r.getCategory().equals("ACC")){ 
								if(r.getQuarter().equals( year + "/" + String.valueOf(qu))){%>
									<td>등록</td>
								<%}else{ %>
									<td>만료</td>
								<%}
							}else if(r.getCategory().equals("REQ")){ %>
								<td>요청</td>
							<%}else if(r.getCategory().equals("DEN")){ %>
								<td>반려</td>
							<%}%>
							<td><%=r.getQuarter() %></td>
							<td><%=r.getRefHisDate() %></td>
							<td><%=r.getUserId() %><input type="hidden" value="<%=r.getUno()%>"></td>
							<td><%=r.getName() %></td>
							<%if(r.getRefHisContent() != null){ %>
							<td><%=r.getRefHisContent() %></td>
							<%} else{ %>
							<td></td> <%} 
							if(r.getQuarter().equals( year + "/" + String.valueOf(qu))){ %>
							<td><button class="ui button mini buttonStyle see"><i class="address card icon"></i>조회</button><button class="ui red button mini buttonStyle remove">강제취소</button>
							<%}else{ %>
							<td><button class="ui button mini buttonStyle see"><i class="address card icon"></i>조회</button><button class="ui red button mini buttonStyle" style="visibility:hidden;">강제취소</button>
							<%} %>
								<input type="hidden" value="<%=r.getRefId()%>">
							</td>
						</tr>
					<%} %>
				</tbody>
			</table>
		</div>
		<div class="pagingArea" id="pagingArea">
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.refall?currentPage=1'"><<</button>
			<%if(currentPage <=1) {%>
				<button class="pagingbtn" disabled></button>
			<%} else{ %>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.refall?currentPage=<%=currentPage-1%>'"><</button>
			<%} %>
			
			<%for(int p=startPage; p<=endPage; p++){
				if(p == currentPage){ %>
					<button class="pagingbtn" disabled><%=p %></button>
				<%} else{ %>
					<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.refall?currentPage=<%=p%>'"><%=p %></button>
				<%}
			  } %>
			
			<%if(currentPage >= maxPage){ %>
				<button class="pagingbtn" disabled></button>
			<%} else {%>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.refall?currentPage=<%=currentPage+1%>'">></button>
			<%} %>
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectList.refall?currentPage=<%=maxPage%>'">>></button>
		</div><!-- PagingArea End -->
	</div><!-- Outline End -->
	
	<script>
		$(function(){
			//$("#logTable").tablesort();
			
			$('#month_year_calendar')
			  .calendar({
			    type: 'year'
			  });
			
			
			$("#searchCategory").change(function(){
				//console.log($(this).val());
				if($(this).val() == '1'){
					$("#searchText").show();
					$("#searchQuarter").hide();
				} else{
					$("#searchText").hide();
					$("#searchQuarter").show();
				}
			});
			
			$(".buttonStyle.see").click(function(){
				//console.log("조회!");
				var uno = $(this).parent().parent().children().eq(3).find("input[type=hidden]").val();
				window.open('<%=request.getContextPath()%>/selectOne.mre?num=' +uno, "이력서", "location=0, resizable=no, menubar=no, status=no, toolbar=no");
				
			
			});
			$(".buttonStyle.remove").click(function(){
				//console.log("등록취소!");
				var refid = $(this).parent().find("input[type=hidden]").val();
				var con = confirm("이미 등록된 추천이력서입니다. 정말 취소하시겠습니까?");
				if(con==true){
					//console.log(con);					
					location.href = "<%=request.getContextPath()%>/delete.refall?num=" + refid;
				}
				
			});
			
		});
		function replaceAll(sValue, param1, param2) { // replaceAll 이라는 변수 선언.
			   return sValue.split(param1).join(param2);
		}
		function currentQuarter(){
			var d = new Date();
			
			var currquarter = d.getFullYear() + "/" + Math.ceil((d.getMonth()+1)/3);
			return currquarter;
		}
		
		function searchRefResume(num){
			var searchCategory = $("#searchCategory").val();
			var searchText = "";
			var searchQuarter1 = $("#searchQuarter").find("input[type=text]").val();
			var searchQuarter2 = $("#searchQuarter").find("input[type=number]").val();
			var status = $("#radioTable").find("input[type=radio]:checked").val();
			var scurrentPage = 0;
			
			//console.log(currentQuarter());
			$tableBody = $("#logTable tbody");

			if(num == 'search'){
				scurrentPage = 1;
				$tableBody.html("<div class='ui active inverted dimmer'><div class='ui loader'></div></div>");
			} else{
				if(num.innerText == '>>'){
					scurrentPage = $("#lastPage").val();
				} else{
					scurrentPage = num.innerText;
				}
			}
			
			if(searchCategory == 1){ //아이디 검색
				searchText = $("#searchText").find("input[type=text]").val();
			} else{ //해당분기 검색
				searchText = searchQuarter1 + "/" + searchQuarter2;
			}
			//console.log(searchText);
			//빈칸 검사 & 올바르지 않은 형식(분기) 검사 필요!
			
			
			$.ajax({
				url : "search.refall",
				data : {
					currentPage:scurrentPage,
					searchCategory : searchCategory,
					searchText : searchText,
					status:status
				},
				type:"get",
				success:function(data){
					//console.log(data);
					
					//$tableBody = $("#logTable tbody");
					$pagingArea = $("#pagingArea").html('');
					
					$tableBody.html('');
					
					var referenceList = data.refResumelist;

					var scurrentPage = data.scurrentPage;
					var slistCount = data.slistCount;
					var smaxPage = data.smaxPage;
					var sstartPage = data.sstartPage;
					var sendPage = data.sendPage;
					
					$.each(referenceList, function(index, value){
						var $tr = $("<tr align=center>");
						var statusKor = "";
						if(value.status == 'ACC'){
							if(value.quarter == currentQuarter()){
								statusKor = "등록";
							} else{
								statusKor = "만료";
							}
						} else if(value.status == 'REQ'){
							statusKor = "요청";
						} else if(value.status == 'DEN'){
							statusKor = "반려";
						}
						var $statusTd = $("<td>").text(statusKor);
						var $quarterTd = $("<td>").text(value.quarter);
						var $dateTd = $("<td>").text(value.refHisDate);
						var $userIdTd = $("<td>").text(value.userId);
						var $userNameTd = $("<td>").text(decodeURIComponent(value.name));
						if(value.content != null){
							var res = decodeURIComponent(value.content);
							res = replaceAll(res, "+", " ");
							var $contentTd = $("<td>").text(res);
						} else{
							var $contentTd = $("<td>").text(value.content);
						}
						if(value.status == "ACC" && value.quarter == currentQuarter()){
							var $seeReTd = $("<td>").append($input).append("<button class='ui button mini buttonStyle see2'>조회</button><button class='ui red button mini buttonStyle remove2'>강제취소</button>");	
						} else{
							var $seeReTd = $("<td>").append($input).append("<button class='ui button mini buttonStyle see2'>조회</button><button class='ui red button mini buttonStyle' style='visibility:hidden'>강제취소</button>");
						}
						var $input = $("<input type='hidden'>").val(value.refId);
						var $inputUno = $("<input type='hidden'>").val(value.uno);
						
						$seeReTd.append($input);
						$userIdTd.append($inputUno);
						$tr.append($statusTd);
						$tr.append($quarterTd);
						$tr.append($dateTd);
						$tr.append($userIdTd);
						$tr.append($userNameTd);
						$tr.append($contentTd);
						$tr.append($seeReTd);
						
						$tableBody.append($tr);
					});
					
					//페이징 처리!
					var $pageDiv = new Array();
					var $button = new Array();
					var $input2 = new Array();
					
					if(scurrentPage != 1){
						$pageDiv1 = $("<div class='eachArea'>");
						$button1 = $("<button class='pagingbtn' onclick='searchRefResume(1)'>").text('<<');
						$pageDiv1.append($button1);
						$pagingArea.append($pageDiv1);
					} 
					
					for(var p=sstartPage; p<=sendPage; p++){
						if(p==scurrentPage){
							$pageDiv[p] = $("<div class='eachArea'>");
							$button[p] = $("<button class='pagingbtn' disabled>").text(p);
								
						} else{
							$pageDiv[p] = $("<div class='eachArea'>");
							$button[p] = $("<button class='pagingbtn' onclick='searchRefResume(this)'>").text(p);
							//$input2[p] = $("<input type='hidden' id=''>").val(p);
						}
						$pageDiv[p].append($button[p]).append($input2[p]);
						$pagingArea.append($pageDiv[p]);
					} 
					
					
					if(scurrentPage != sendPage){
						$pageDiv2 = $("<div class='eachArea'>");
						$button2 = $("<button class='pagingbtn' onclick='searchRefResume(this)'>").text('>>');
						$input3 = $("<input type='hidden' id='lastPage'>").val(sendPage);
						$pageDiv2.append($button2);
						$pageDiv2.append($input3);
						$pagingArea.append($pageDiv2);
					}
					

					$(".buttonStyle.see2").click(function(){
						//console.log("조회!");
						var uno = $(this).parent().parent().children().eq(3).find("input[type=hidden]").val();
						<%-- location.href="<%=request.getContextPath()%>/selectOne.mre?num=" +uno; --%>
						window.open('<%=request.getContextPath()%>/selectOne.mre?num=' +uno, "이력서", "location=0, resizable=no, menubar=no, status=no, toolbar=no");

					});
					$(".buttonStyle.remove2").click(function(){
						//console.log("등록취소!");
						var refid = $(this).parent().find("input[type=hidden]").val();
						//console.log(refid);
						//아직 만들지말지 고민중!!
						var con = confirm("등록된 추천이력서입니다. 정말 취소하시겠습니까?");
						if(con==true){
							//console.log(con);					
							location.href = "<%=request.getContextPath()%>/delete.refall?num=" + refid;
						}
							
					});
				}
			});
			
			
		}
	</script>
</body>
</html>

















