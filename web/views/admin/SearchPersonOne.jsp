<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, java.text.SimpleDateFormat"%>
    
    
    
    <%
    
    ArrayList<HashMap<String, Object>> list = ( ArrayList<HashMap<String, Object>>)request.getAttribute("list");
    
    for(int i = 0; i < list.size(); i++) { 
  		HashMap<String, Object> hmap = list.get(i);
    }
    
    %>
<!DOCTYPE html>
<html>
<head>
<style>


	.outline{
		margin-right:auto;
		margin-left:auto;
		width:80%;
		height:1000px;
		background:white;
		padding:50px;
	}

#subDiv1 {

display:inline-block;
width:300px;


}

#subDiv0 {

display:inline-block;


}


.subTitle {
	color: gray;
	font-size: 15px;
	font-weight: bold;
	margin-left: 10px;
}

.explain {
	font-size: 20px;
	font-weight: bolder;
}

.explainDiv {
	width:150px;
	vertical-align:top;
}



.sort {
	margin : 30px;
}


#td-vertical {
	vertical-align: top;
	padding-top: 5px;
}



hr {
	margin-top: 10px;
}

.inline {

display:inline-block;
}

#profileImgArea {
	width:300px;
	height:300px;
}

#profileImg {
	 width:200px;
	 height:200px;
	 margin:50px;
	 border:1px solid black;
}



</style>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="/views/common/import.html" %>
</head>
<body style="background:lightgray;">
<%@ include file="../common/adminMenu.jsp" %>


<%if(list.size() != 0){ %>
	<h1 style="margin-left:10%;">개인회원 상세정보</h1>
<div class="outline">
	
	<label class="subTitle">개인정보</label>
	<hr>
	

	<div id="subDiv0" style="margin-bottom:100px;">
	
	<table class="sort">
	<tr>
	<td ><label class="explain">이름</label></td>
	<td style="padding-left:50px;"><div class="ui input"><input type="text" style="width:250px; " id="name" name="name" value= <%= list.get(0).get("name") %>  readonly></div></td>
	
	
	</tr>
	
	<%
	Date Birth = (Date)list.get(0).get("birth");
	
	String birthY="";
	String birthM = "";
	String birthD = "";
	
	if(Birth != null) {
	SimpleDateFormat afterFormat = new SimpleDateFormat("yyyyMMdd");
	String birth = afterFormat.format(Birth);
	
	 birthY = birth.substring(0,4);
	 birthM = birth.substring(4,6);
	 birthD = birth.substring(6,8);
	
	} else if(Birth == null){
		birthY = "2020";	
		 birthM = "02";
		 birthD = "14";
		
	} 
	
	

%> 
	
	
	
	
	<tr>
						<td style="padding-top:20px;"><label class="explain">생년월일</label></td>
						<td style="padding-left:50px; padding-top:20px;"><div class="ui input">
								<input type="text" maxlength="4" name="birth1" size="4" id="birth1" style="width:70px; margin-right:5px;" value= <%=birthY %> readonly>
							</div>/
							<div class="ui input"> 
								<input type="text" maxlength="2" name="birth2" size="2" id="birth2"  style="width:70px; margin-left:5px; margin-right:5px;" value= <%=birthM %> readonly>
							</div>/
							<div class="ui input">
								<input type="text" maxlength="2" name="birth3" size="2" id="birth3"  style="width:70px; margin-left:5px;" value= <%=birthD %> readonly>
							</div></td>
						<td></td>
					</tr>
	
	<tr >
	
	<%
	
	String[] tel =new String[3];
	String phone = (String)list.get(0).get("phone");
	
	if(phone == null) {
		tel[0]="010";
		tel[1]="0000";
		tel[2]="0000";
		
	}
	
	else if(phone.equals("--")) {
		tel[0]="010";
		tel[1]="0000";
		tel[2]="0000";
		
		
	}else {
		 tel = phone.split("-");	
		
	}
	
	
	%>
	
	<tr>
						<td style="padding-top:20px;"><label class="explain">연락처</label></td>
						<td style="padding-left:50px; padding-top:20px;"><div class="ui input">
								<input type="text" maxlength="3" name="tel1" size="2" id="tel1" style="width:70px; margin-right:5px; " value= <%= tel[0]%> readonly>
							</div>-
							<div class="ui input"> 
								<input type="text" maxlength="4" name="tel2" size="2" id="tel2"  style="width:70px; margin-left:5px; margin-right:5px;"value= <%= tel[1]%> readonly>
							</div>-
							<div class="ui input">
								<input type="text" maxlength="4" name="tel3" size="2" id="tel3"  style="width:70px; margin-left:5px;"value= <%= tel[2]%> readonly>
							</div></td>
						<td></td>
					</tr>
	
	 <tr >
	<td style="padding-top:20px;"><label class="explain">성별</label></td>
	

	
	 <%if((String)list.get(0).get("gender") == null){ %> 
	<td style="padding-left:50px; padding-top:20px; width:100px;"><div class="ui radio checkbox"><input type="radio" name="gender" id="M" value="M" disabled> <label for="M">남자</label></div>
	<div class="ui radio checkbox" style="padding-left:50px;"><input type="radio" name="gender" id="F" value="F" disabled> <label for="F">여자</label></div></td>
 <%}else if(list.get(0).get("gender").equals("F")){ %>
<td style="padding-left:50px; padding-top:20px; width:100px;"><div class="ui radio checkbox"><input type="radio" name="gender" id="M" value="M" disabled> <label for="M">남자</label></div>
	<div class="ui radio checkbox" style="padding-left:50px;"><input type="radio" name="gender" id="F" value="F" checked disabled> <label for="F">여자</label></div></td>


	<%} else if(list.get(0).get("gender").equals("M")){%>
	<td style="padding-left:50px; padding-top:20px; width:100px;"><div class="ui radio checkbox"><input type="radio" name="gender" id="M" value="M" checked disabled> <label for="M">남자</label></div>
	<div class="ui radio checkbox" style="padding-left:50px;"><input type="radio" name="gender" id="F" value="F" disabled> <label for="F">여자</label></div></td>
	
	<%} %> 
	</tr>
	
	
		
	</table>
	
	
	
	</div>
	
	<div id="subDiv1" style="margin-left:100px;">

	<table  style="padding-bottom:100px;" >
	<tr>
	<td width="300px" align="center">
					<div id="profileImgArea">
					 <%if(list.get(0).get("changeName") == null) {%> 
						<img id="profileImg" src="<%=request.getContextPath()%>/images/profilePhoto.jpg">
						 <%}else { %>
						<img id="profileImg" src="<%=request.getContextPath()%>/thumbnail_uploadFiles/<%=list.get(0).get("changeName") %>">
						<%} %> 
					</div>
					
				</td>
	
	</tr>
	
	

	<tr>
	
	</tr>
	
	</table>
	
	
	</div>
	<br>
	
	
	
	<div id="subDiv2" class="inline">
	<label class="subTitle">정보 공개</label>
	<hr style="width:95%">
	
	
	<table class="sort">
	
	<tr>
	<td ><label class="explain">구직상태</label></td>
	<td style="padding-left:50px;">
			
			 <%if(list.get(0).get("jobStatus")==null){ %>
			<select class="ui dropdown" name="JobStatus" id="JobStatus" style="width:250px;" disabled>
  				<option value="EDU" >재학중</option>
  				<option value="N">구직중</option>
  				<option value="Y">재직중</option>
  				
			</select>
			 <%} else if(list.get(0).get("jobStatus").equals("N")){ %>
			<select class="ui dropdown" name="JobStatus" id="JobStatus" style="width:250px;" disabled>
  				<option value="N">구직중</option>
  				<option value="EDU">재학중</option>
  				
  				<option value="Y">재직중</option>
  				
			</select>
			<%} else if(list.get(0).get("jobStatus").equals("Y")) {%>
			<select class="ui dropdown" name="JobStatus" id="JobStatus" style="width:250px;" disabled>
  				<option value="Y">재직중</option>
  				<option value="N">구직중</option>
  				<option value="EDU">재학중</option>
  				
  				
  				
			</select>
			
			<%} else if(list.get(0).get("jobStatus").equals("EDU")) {%>
			<select class="ui dropdown" name="JobStatus" id="JobStatus" style="width:250px;" disabled>
  				<option value="EDU">재학중</option>
  				<option value="N">구직중</option>
  				<option value="Y">재직중</option>
  				
			</select>
			
			
			<%} %> 
			
</td>


	<td></td>
	
	</tr>
	
	<tr>
	<td style="padding-top:20px;"><label class="explain">GitHub</label></td>
	<td style="padding-left:50px; padding-top:20px;"">
			<div class="ui labeled input">
  				<div class="ui label">
    				http://
  				</div>
  				<input type="text" name="github" id="github" value= <%= list.get(0).get("github") %>  readonly>
			</div>
	
	</td>
	
	</tr>
	
	<tr>
	<td style="padding-top:20px;"><label class="explain">블로그</label></td>
	<td style="padding-left:50px; padding-top:20px;"">
			<div class="ui labeled input">
  				<div class="ui label">
    				http://
  				</div>
  				<input type="text" name="blog" id="blog" value= <%= list.get(0).get("blog") %>  readonly>
			</div>
	
	</td>
	
	</tr>
	
	<tr>
	<td style="padding-top:20px;"><label class="explain">정보공개</label></td>
	<td style="padding-left:50px; padding-top:20px;">
			
			
	
	
	 <% if(list.get(0).get("infoOpen")==null){ %> 
	<select class="ui dropdown" id="publicInfo" name="publicInfo" style="width:250px;" disabled>
  				<option value="ALL">전부공개</option>
  				<option value="PART">일부공개</option>
  				<option value="NO">비공개</option>
			</select>
			
			 <%}else if(list.get(0).get("infoOpen").equals("PART")){ %>
			
			<select class="ui dropdown" id="publicInfo" name="publicInfo" style="width:250px;" disabled>
  				<option value="PART">일부공개</option>
  				<option value="ALL">전부공개</option>
  				
  				<option value="NO">비공개</option>
			</select>
			
			
			
			
			<%} else if(list.get(0).get("infoOpen").equals("NO")) { %>
			<select class="ui dropdown" id="publicInfo" name="publicInfo" style="width:250px;" disabled>
  				<option value="NO">비공개</option>
  				<option value="PART">일부공개</option>
  				<option value="ALL">전부공개</option>
  				
  				
			</select>
			
			<%} else if(list.get(0).get("infoOpen").equals("ALL")) {%>
			<select class="ui dropdown" id="publicInfo" name="publicInfo" style="width:250px;" disabled>
  				<option value="ALL">전부공개</option>
  				<option value="PART">일부공개</option>
  				<option value="NO">비공개</option>
			</select>
			
			
			<%} %>
			</td>
	
	</tr>
	
	<tr >
	<td style="padding-top:20px;"><label class="explain">KOSA 보유여부</label></td>
	
	
	<%if(list.get(0).get("kosaYn") ==null){ %> 
	<td style="padding-left:50px; padding-top:20px; width:100px;"><div class="ui radio checkbox"><input type="radio" name="kosa" id="Y" value="Y" disabled> <label for="Y">보유</label></div>
	<div class="ui radio checkbox" style="padding-left:50px;"><input type="radio" name="kosa" id="N" value="N"disabled > <label for="N">미보유</label></div></td>
 <%}else if(list.get(0).get("kosaYn").equals("N")){ %>
	<td style="padding-left:50px; padding-top:20px; width:100px;"><div class="ui radio checkbox"><input type="radio" name="kosa" id="Y" value="Y" disabled> <label for="Y">보유</label></div>
	<div class="ui radio checkbox" style="padding-left:50px;"><input type="radio" name="kosa" id="N" value="N" checked disabled> <label for="N">미보유</label></div></td>
	
	<%} else if(list.get(0).get("kosaYn").equals("Y")) {%>
	<td style="padding-left:50px; padding-top:20px; width:100px;"><div class="ui radio checkbox"><input type="radio" name="kosa" id="Y" value="Y" checked disabled> <label for="Y">보유</label></div>
	<div class="ui radio checkbox" style="padding-left:50px;"><input type="radio" name="kosa" id="N" value="N"disabled > <label for="N">미보유</label></div></td>
	
	<%} %>
	</tr>
	
	
	

	
	
		
	</table>
	</div>
	
	
	
	<div id="subDiv3" class="inline">
	
	
	<label class="subTitle">가입정보</label>
	<hr style="width:95%">
	<table class="sort">
	
	<tr>
	<td ><label class="explain">아이디</label></td>
	<td style="padding-left:50px;"><div class="ui input" ><input type="text" style="width:250px;" value= <%= list.get(0).get("userId") %>  readonly></div></td>
	<td></td>
	
	</tr>
	
	
	

	
	
		
	</table>
	</div>
	<div class="btn" align="center" style="margin-top:50px;">
				 <input type="button" class="ui secondary button" onclick="cancle();" value="목록으로 돌아가기">

				
			</div>

	

	</div>
	
	
	
	
	
			
			
	
	<%} else {
	
	
		request.setAttribute("msg", "추가정보가 없는 회원입니다.");
		request.getRequestDispatcher("../admin/needPersonInfo.jsp").forward(request, response);
	
	
	
	} %>
	

	
	<script>
	
	function cancle() {
		
		
		
		location.href="<%=request.getContextPath()%>/SelectPersonListPage.ad";
		
		
		
		
		
		
	}
	
	
	
	</script>
	 
	
	
	
	
	

</body>
</html>