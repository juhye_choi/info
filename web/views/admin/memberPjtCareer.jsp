<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="resume.model.vo.*, java.util.*, keyword.model.vo.*, member.model.vo.Member"%>
<%
	Member loginUser =  (Member) session.getAttribute("loginUser"); 
	
	if(loginUser == null || !loginUser.getCategory().equals("A")){
		request.setAttribute("msg", "관리자가 아니시네요?!");
		request.getRequestDispatcher("/views/common/needLogin.jsp").forward(request, response);
	}
	ProjectCareer pc = (ProjectCareer) request.getAttribute("pc");
	int diff = 0;
	if(pc == null){
		request.setAttribute("msg", "잘못된 접근입니다.");
		request.getRequestDispatcher("../common/needLogin.jsp").forward(request, response);
	} else{
		diff = (Integer) request.getAttribute("diffDays");
	}
	
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>이력서 상세</title>
<style>
	.first{
		margin-right:auto;
		margin-left:auto;
		width:70%;
	}
	
	.detailInfo{
		margin:10px;
	}
	.projectSection{
		width:80%;
		height:100%;
		display:inline-block;
		overflow:hidden;
		margin-right:5%;
	}

	.keywordSection{
		width:14%;
		height:100%;
		padding:10px;
		display:inline-block;
		border:1px solid #e7e6e1;
		text-align:center;
		overflow:hidden;
		border-radius:10px;
		vertical-align:top;
	}
	.keyword{
		border:1px solid purple;
		border-radius:5px;
		margin:10px;
		margin-left:15px;
		margin-right:15px;
		color:purple;
	}
	h4 {
		color:gray;
	}
	
	
	@media screen and (max-width: 768px){
		.first{
			width:100%;
		}
		.detailInfo{
			height:1500px;
		}
		.projectSection{
			display:block;
			width:100%;
			height:40%;
		}
		.keywordSection{
			display:block;
			width:100%;
			height:20%;
			overflow:scroll;
		}
		.keywordSection::-webkit-scrollbar{
			display:none;
		}
		.sentence{
			display:none;
		}
		.name{
			font-size:2em;
		}
		.basic2>table{
			width:100%; text-align:center; margin-top:0;
		}
		.basic2 td:last-of-type{
			display:none;
		}
		.section{
			display:block;
			width:100%;
			height:30%;
			overflow:scroll;
		}
		.section::-webkit-scrollbar{
			display:none;
		}
	}
	
	.title{
		padding:10px;
		margin:10px;
		text-align:center;
		font-size:2em;
		font-weight:bold;
	}
	.section{
		width:100%;
		border:1px solid #e7e6e1;
		border-radius:10px;
		margin-bottom:10px;
		padding:8px;
	}
	.thumbnail{
		/* background:lightgray; */
		height:180px;
		width:30%;
		display:inline-block;
		border-radius:10px;
		overflow:hidden;
	}
	.thumbnail>img{
		width:180px;
		height:180px;
	}
	
	.intro{
		border-radious:10px;
		display:inline-block;
		padding:10px;
		overflow:hidden;
		vertical-align:top;
	}
	.introDetail{
		padding-left:10px;
		font-size:1.2em;
	}
	
	.section2{
		width:100%;
		padding:10px;
		margin-bottom:10px;
	}
	
	.addSection{
		width:50%;
		font-weight:bold;
		margin:5px;
	}
	.pjtImg{
		display:inline-block;
		width:19%;
		text-align:center;
	}
	.pjtImg>img{
		width: 100px; height: 100px;
    	object-fit: cover;
   		object-position: top;
    	border-radius: 50%;
	}
	.attFile{ display:inline-block; }
	.attFile:hover{
		text-decoration:underline;
		cursor:pointer;
	}
	.modalOuter{
		margin-left:auto;
		margin-right:auto;
		text-align:center;
	}
	#ibtn:hover{
		cursor:pointer;
	}
</style>

<%@ include file="/views/common/import.html" %>
</head>
<body style="background:lightgray;">
	<%-- <%@ include file="../common/adminMenu.jsp" %> --%>
	<div class="first" style="background:white; padding:20px;">
		<div class="detailInfo">
			<div class="projectSection" id="pjtIntro">
				<input type="hidden" name="project" value="<%=pc.getPjtId()%>">
				<div class="title"><%=pc.getPjtName() %></div>
				<div class="section">
					<div class="thumbnail">
						<%if(pc.getAtlist().get(0).getChangeName() != null){ %>
						<img src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=pc.getAtlist().get(0).getChangeName()%>">
						<%} %>
					</div>
					<div class="intro">
						<h4>일정</h4>
						<div class="introDetail"><%=pc.getPjtStart() %> ~ <%=pc.getPjtFinish() %> (<%=diff %>일)</div>
						<h4>프로젝트 내 역할</h4>
						<div class="introDetail" style="white-space:pre-line;"><%=pc.getMyRole() %></div>
					</div>
				</div>
				<div class="section2">
					<h4>프로젝트 사진</h4>
					<%for(Attachment at : pc.getAtlist()){ 
						if(at.getChangeName() != null){%>
					<div class="pjtImg" onclick="seeImg(this);"><img src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=at.getChangeName()%>"></div>
					<%}} %>
				</div>
				<div class="section">
					<h4>프로젝트 세부내용</h4>
					<div class="pjtContent" style="white-space:pre-line;">
						<%=pc.getPjtContent() %>
					</div>
				</div>
				<div class="section">
					<div class="addSection">관련사이트 : <%=pc.getPjtLink() %></div>
					<div class="addSection">첨부파일 : 
						<div class="attFile" onclick="location.href='<%=request.getContextPath()%>/download.at?num=<%=pc.getAttachmentFile().getAtId() %>'">
							<%=pc.getAttachmentFile().getOriginName() %></div>
					</div>
				</div>
			</div>
			<div class="keywordSection">
				<h4>연관키워드</h4>
				<%for(Keyword k : pc.getKlist()){ %>
					<div class="keyword"><%=k.getKname() %></div>
				<%} %>
			</div>
		</div>
	</div>
	<%-- 모달 영역! --%>
	<div class="ui basic modal" id="introModal">
		<div class="modalOuter">
			<div class="buttonArea">
				<div id="ibtn" onclick="$('#introModal').modal('hide')" style="float:right;">X</div>
			</div>
			<div class="bigPhotoArea">
				<img id="modalImg" style="width:500px;" src="">
			</div>

		</div>
	</div> <!-- 사진 띄우는 모달 끝 -->
	<script>
		$(function(){
			$("#modify").click(function(){
				var num = $("#pjtIntro").find("input[type=hidden]").val();
				location.href="<%=request.getContextPath()%>/selectList.ke?type=2&num=" +num;
			});
			
			$("#cancel").click(function(){
				location.href="<%=request.getContextPath()%>/selectOne.re";
			});

			$("#modify").css("display","inline-block");
			$("#cancel").css("display","inline-block");
			$("#transPDF").css("display","none");
			

			//기본정보 출력
			$("#name").text("<%=loginUser.getUserName()%>");
			$("#title").text("<%=pc.getrTitle()%>");
			$("#emailAddress").text("<%=loginUser.getEmail()%>");
			$("#phoneNumber").text("<%=loginUser.getPhone()%>");

		});
		
		function seeImg(value){
			console.log($(value).find("img").attr("src"));
			$("#modalImg").attr("src", $(value).find("img").attr("src"));
			$("#introModal").modal('show');
		}
	
	
	</script>
</body>
</html>








