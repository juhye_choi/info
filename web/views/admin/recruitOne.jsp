<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="person.model.vo.*, keyword.model.vo.*, company.model.vo.*, java.util.*"%>
<%
	Recruit r = (Recruit) request.getAttribute("recruit");
	Company c = (Company) request.getAttribute("company");
 	int recType = r.getRectype();
 	int edu = r.getEducation();
 	int salWay = r.getSalary_way();
 	int apply = r.getHowto_apply();
 	String career = r.getCareer_period();
 	int recP = r.getRec_pcount();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><%=r.getRec_title() %></title>
<style>
#mainDiv {
	margin-left:auto;
	margin-right:auto;
	margin-top:70px;
	margin-bottom:100px;
	width:900px;
}

#leftDiv {
	width:600px;
}

#rightDiv1 {
	width:300px;
	padding:15px;
	border:1px solid lightgray;
	box-shadow: 0px 0px 2px 0.5px lightgray;
}

#rightDiv2 {
	margin-top:30px;
	width:300px;
}

aside {
	width: 340px;
    position: fixed;
    right: calc((100% - 1100px)/2);
}
.mySlides {
	display: none;
	overflow: hidden;
	width:600px;
	height:400px;
	line-height:400px;
}

img	{
 	vertical-align: middle;
}

.slideshow-container {
  width:600px;
  hegight:400px;
  position:relative;
  margin-left:auto;
  margin-right:auto;
}

.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
}

.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}
/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

table {
	border-collapse: collapse;
}

tr, td {
	padding-top:7px;
	padding-bottom:7px;
}

.tc {
	width:120px;
	font-size:15px;
	font-weight:bold;
	padding-right:20px;
}

pre {
	white-space: pre-line;
}

</style>
</head>
<%@ include file="/views/common/import.html" %>
<body>
	
	<div class="ui tiny modal">
		<i class="close icon"></i>
		<div class="header">지원정보</div>
		<table style="width:500px; margin-top:10px; margin-bottom:10px; margin-left:auto; margin-rigth:auto;">
			<tr>
				<td width="80px;"><h4>기업명</h4></td><td><%=c.getCompanyName() %></td>
			</tr>
			<tr>
				<td><h4>공고제목</h4></td><td><%=r.getRec_title() %></td>
			</tr>
		</table>
		
			<% if(apply == 1) { %>
			<form id="apply" action="<%=request.getContextPath()%>/apply.Rec" method="post" enctype="multipart/form-data">
		
		<input type="hidden" name="recid" value="<%=r.getRecid() %>">
		<table style="width:500px; margin-top:10px; margin-bottom:10px; margin-left:auto; margin-rigth:auto;">
			<tr>
				<td width="80px;"><h4>파일첨부</h4></td>
				<td>
					<div class="ui action input" id="fileDiv">
						<input type="file" accept=".pdf" class="ui hidden" name="addResume">
						<div class="ui icon button" id="fileBtn">
					    	<i class="attach icon"></i>
						</div>
					</div>
				</td>
			</tr>
		</table>
		</form>
	  	<div class="actions">
	 	    <div class="ui black deny button">취소</div>
	  		<div class="ui yellow button" onclick="sendApply()">지원</div>
	 	</div>
	    <% } else { %>
	    <table style="width:500px; margin-top:10px; margin-bottom:10px; margin-left:auto; margin-rigth:auto;">
			<tr>
				<td width="80px;"><h4>접수 이메일</h4></td><td><%=c.getEmail() %></td>
			</tr>
		</table>
	   	<div class="actions">
	  		<div class="ui black deny button">닫기</div>
	 	</div>
	    <% }%>
		
	    
	    
	</div>
	<div id="mainDiv">
		<aside>
			<header id="rightDiv1">
				<h3><%=r.getRec_title() %></h3>
				<hr>
				<table>
					<tr>
						<td class="tc">고용형태</td>
						<td><% if(recType == 1) { %>정규직<% }else if(recType == 2){ %>계약직<%}else if(recType == 3){ %>인턴<% }else{ %>기타<% } %></td>
					</tr>
					<tr>
						<td class="tc">채용경력</td>
						<td><% if(career.equals("NEW")){ %>신입<% } else if(career.equals("OLD")){ %>경력<% } else { %>경력무관<% } %></td>
					</tr>
					<tr>
						<td class="tc">학력제한</td>
						<td><% if(edu == 1) { %>고등학교 졸업<% } else if(edu == 2) { %>대학교 졸업(2년제)<% } else if(edu == 3) { %>대학교 졸업(4년제)<% } else { %>대학원 졸업<% } %></td>
					</tr>
					<tr>
						<td class="tc">회사대표</td>
						<td>
							<% if(c.getOwner() != null) { %>
								<%=c.getOwner() %>
							<% } else { %>
								미입력
							<% } %>
						</td>
					</tr>
					<tr>
						<td class="tc">인사담당자</td>
						<td>
							<% if(c.getHrManager() != null) { %>
								<%=c.getHrManager() %>
							<% } else { %>
								미입력
							<% } %>
						</td>
					</tr>
					<tr>
						<td class="tc">담당자 연락처</td>
						<td>
							<% if(c.getPhone() != null) { %>
								<%=c.getPhone() %>
							<% } else { %>
								미입력
							<% } %>
						</td>
					</tr>					
				</table>
				<hr>
				<table>
					<tr>
						<td class="tc">모집인원</td>
						<td><% if(recP == 1){ %>O명<% } else if(recP == 2){ %>OO명<% } else { %>OOO명<% } %></td>
					</tr>
					<tr>
					<% if(r.getHowto_apply() == 1) { %>
						<td class="tc">현재 지원자</td>
						<td><%=r.getApplicant() %> 명</td>
					<% } else { %>
						<td colspan="2">이메일 접수</td>
					<% } %>
					</tr>
				</table>
				<hr>
				<table>
					<tr>
						<td class="tc">마감일</td>
						<td><%=r.getRec_finish() %></td>
					</tr>
				</table>
			</header>
			
		</aside>
		<div id=leftDiv>
			<% if(c.getAtlist().size() > 0) {%>
			<div class="slideshow-container">
				<div class="mySlides fade">
				 	<img src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=c.getAtlist().get(0).getChangeName() %>" style="width:100%;">
				</div>
				<% if(c.getAtlist().size() > 1) { %>
				<div class="mySlides fade">
					<img src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=c.getAtlist().get(1).getChangeName() %>" style="width:100%;">
				</div>
				<% }
					if(c.getAtlist().size() > 2) {
				%>
				<div class="mySlides fade">
					<img src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=c.getAtlist().get(2).getChangeName() %>" style="width:100%;">
				</div>
				<% }
					if(c.getAtlist().size() > 3) {
				%>
				<div class="mySlides fade">
					<img src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=c.getAtlist().get(3).getChangeName() %>" style="width:100%;">
				</div>
				<% } %>
				<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
				<a class="next" onclick="plusSlides(1)">&#10095;</a>
				
			</div>
			<br>	
			<div style="text-align:center">
				<span class="dot" onclick="currentSlide(1)"></span> 
				<% if(c.getAtlist().size() > 1) { %>
				<span class="dot" onclick="currentSlide(2)"></span>
				<% }
					if(c.getAtlist().size() > 2) {
				%>
				<span class="dot" onclick="currentSlide(3)"></span>
				<% }
					if(c.getAtlist().size() > 3) {
				%>
				<span class="dot" onclick="currentSlide(4)"></span> 
				<% } %>
			</div>
			<script>
			var slideIndex = 1;
			showSlides(slideIndex);
				
			function plusSlides(n) {
				showSlides(slideIndex += n);
			}
			
			function currentSlide(n) {
				showSlides(slideIndex = n);
			}
				
			function showSlides(n) {
				var i;
				var slides = document.getElementsByClassName("mySlides");
				var dots = document.getElementsByClassName("dot");
				
				if (n > slides.length) {
					slideIndex = 1
				};    
				if (n < 1) {
					slideIndex = slides.length
				};
				for (i = 0; i < slides.length; i++) {
				    	slides[i].style.display = "none";  
			  	};
			 	for (i = 0; i < dots.length; i++) {
			    	dots[i].className = dots[i].className.replace(" active", "");
				};
				slides[slideIndex-1].style.display = "block";  
				dots[slideIndex-1].className += " active";
			}
			</script>
			<% } %>
			
			
			<h2><%=r.getRec_title() %></h2>
			<label style="font-size:15px; color:gray; margin-left:5px;"><%=c.getCompanyName() %></label>
			<hr>
			
			<% if(r.getKlist() != null) { 
				for(Keyword k : r.getKlist()) { %>
					<label class="ui orange tag label" style="margin-right:6px">&nbsp;<%=k.getKname()%></label>
			<% } 
			} %>	
			
			<% if(c.getIntro() != null){ %>
			<pre><%=c.getIntro() %></pre>
			<% } %>
			
			
			<% if(r.getJob_position() != null){ %>
			<h4 class="ui secondary header">주요업무</h4>
			<pre><%=r.getJob_position() %></pre>
			<% } %>
			
			<% if(r.getEligibility() != null){ %>
			<h4 class="ui secondary header">자격요건</h4>
			<pre><%=r.getEligibility() %></pre>
			<% } %>

			<% if(r.getSpe_condition() != null){ %>
			<h4 class="ui secondary header">우대사항</h4>
			<pre><%=r.getSpe_condition() %></pre>
			<% } %>
			
			<% if(r.getBenefits_welfare() != null){ %>
			<h4 class="ui secondary header">혜택 및 복지</h4>
			<pre><%=r.getBenefits_welfare() %></pre>
			<% } %>
			
			<% if(r.getRecruitAttachment().getOriginName() != null){ %>
			<h4 class="ui secondary header">전용 양식 파일</h4>
			<label for="l2"><%=r.getRecruitAttachment().getOriginName() %></label><button>다운로드</button>
			<% } %>
			
			<hr style="margin-top:50px; margin-bottom:20px">
			
			<div><label id="wa" class="tc">근무지역</label><label for="wa"><%=r.getWork_area() %></label></div>
			
			<div id="map" style="width:600px; height:300px; margin-top:20px; margin-bottom:20px;"></div>
			
			<% if(c.getBirth() != null){ %>
			<div style="margin-top:20px; margin-bottom:20px;"><label id="birth" class="tc">회사 설립일</label><label for="birth"><%=c.getBirth() %></label></div>

			<% } %>
			</div>
			
			
			<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=158fc96dbe3058f905dcd07613a77957&libraries=services,clusterer,drawing"></script>
			<script>
			var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
		    mapOption = {
		        center: new kakao.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
		        level: 3 // 지도의 확대 레벨
		    };  

			// 지도를 생성합니다    
			var map = new kakao.maps.Map(mapContainer, mapOption); 
	
			// 주소-좌표 변환 객체를 생성합니다
			var geocoder = new kakao.maps.services.Geocoder();
	
			// 주소로 좌표를 검색합니다
			geocoder.addressSearch('<%=r.getWork_area() %>', function(result, status) {
	
			    // 정상적으로 검색이 완료됐으면 
			     if (status === kakao.maps.services.Status.OK) {
	
			        var coords = new kakao.maps.LatLng(result[0].y, result[0].x);
	
			        // 결과값으로 받은 위치를 마커로 표시합니다
			        var marker = new kakao.maps.Marker({
			            map: map,
			            position: coords
			        });
	
			        // 인포윈도우로 장소에 대한 설명을 표시합니다
			        var infowindow = new kakao.maps.InfoWindow({
			            content: '<div style="width:150px;text-align:center;padding:6px 0;"><%=c.getCompanyName() %></div>'
			        });
			        infowindow.open(map, marker);
	
			        // 지도의 중심을 결과값으로 받은 위치로 이동시킵니다
			        map.setCenter(coords);
			    } 
			});    
			</script>
			
		</div>			<!-- leftDiv End -->
	
	<script>
	var slideIndex = 1;
	showSlides(slideIndex);
		
	function plusSlides(n) {
		showSlides(slideIndex += n);
	}
	
	function currentSlide(n) {
		showSlides(slideIndex = n);
	}
		
	function showSlides(n) {
		var i;
		var slides = document.getElementsByClassName("mySlides");
		var dots = document.getElementsByClassName("dot");
		
		if (n > slides.length) {
			slideIndex = 1
		};    
		if (n < 1) {
			slideIndex = slides.length
		};
		for (i = 0; i < slides.length; i++) {
		    	slides[i].style.display = "none";  
	  	};
	 	for (i = 0; i < dots.length; i++) {
	    	dots[i].className = dots[i].className.replace(" active", "");
		};
		slides[slideIndex-1].style.display = "block";  
		dots[slideIndex-1].className += " active";
	}
	
	function support(){
		$('.ui.tiny.modal')
		  .modal('show')
		;
	}
	
	function needLogin(){
		swal ({title : "로그인이 필요한 서비스입니다!",icon : "error"});
	}
	
	function sendApply(){
		$("#apply").submit();
	}
	</script>
</body>
</html>