<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="keyword.model.vo.*, java.util.*"%>
<%
	ArrayList<Keyword> klist = (ArrayList<Keyword>) request.getAttribute("klist");
	
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>키워드관리</title>
<%@ include file="/views/common/import.html" %>
<style>
	.outline{
		margin-right:auto;
		margin-left:auto;
		width:80%;
		height:900px;
		background:white;
		padding:50px;
	}
	.searchSection{
		padding-top:20px;
		padding-right:20%;
		padding-left:20%;
		padding-bottom:20px;
		border:1px solid lightgray;
		border-top:2px solid purple;
		text-align:center;
	}
	.addArea{
		text-align:right;
		margin-top:10px;
	}
	.tableSection{
		padding-top:5px;
	}
	
	.tableSection td:nth-of-type(1){
		width:150px;
	}
	
	.tableSection td:nth-of-type(4){
		width:200px;
	}
	
	.buttonStyle {
		margin-right:5px !important;
		margin-left:5px !important;
	}
	
	h1{
		margin-left:11%;
	}
	.pagingArea{
		text-align:center;
		padding:20px;
		margin-bottom:10px;
	}
	.eachArea{
		display:inline-block;
		margin:2px;
	}
	.pagingbtn{
		height:30px;
		width:30px;
		display:inline-block;
		vertical-align:top;
		overflow:hidden;
		background:white;
		border:1px solid lightgray;
		border-radius:5px;
	}
	.pagingbtn:hover{
		cursor:pointer;
	}
	.pagingbtn:disabled{
		color:gray;
	}
	.modalOuter{
		margin-left:auto;
		margin-right:auto;
		width:400px;
		height:280px;
		background:white;
		color:black;
		padding:20px;
		text-align:center;
	}
</style>
</head>
<body style="background:lightgray;">
	<%@ include file="../common/adminMenu.jsp" %>
	<h1>키워드 관리</h1>

	<div class="outline">
		<div class="searchSection">
			<div class="ui form">
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown" id="searchCategory">
							<option value="1">키워드명</option>
							<option value="2">키워드분류</option>
						</select>
					</div>
					<div class="ten wide field">
						<input type="text" id="searchText">
						<select class="ui fluid dropdown" name="scategory" id="scategory" style="display:none;">
								<option value="1">언어</option>
								<option value="2">모바일/OS</option>
								<option value="3">웹개발</option>
								<option value="4">프레임워크</option>
								<option value="5">라이브러리</option>
								<option value="6">DB</option>
								<option value="7">업무분야</option>
								<option value="8">Tool</option>
							</select>
					</div>
				</div>
			<input type="button" value="검색" class="ui purple button" style="width:150px;" onclick="searchKeyword('search')">
			</div>
		</div>
		
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead>
					<tr align="center">
						<th style="width:80px;">키워드코드</th>
						<th>키워드명</th>
						<th style="width:100px;">분류</th>
						<th>삭제/수정</th>
					</tr>
				</thead>
				<tbody>
					<%for(Keyword k : klist){ %>
						<tr align="center">
							<td><%=k.getKcode() %></td>
							<td><%=k.getKname() %></td>
							<%switch(k.getCategory()){
								case "1" : %> <td>언어</td> <% break;
							 	case "2" : %> <td>모바일/OS</td> <% break;
							 	case "3" : %> <td>웹개발</td> <% break;
							 	case "4" : %> <td>프레임워크</td> <% break;
							 	case "5" : %> <td>라이브러리</td> <% break;
							 	case "6" : %> <td>DB</td> <% break;
							 	case "7" : %> <td>업무분야</td> <%break;
							 	case "8" : %> <td>Tool</td> <%break;
							 	default : %> <td></td> <%break;
							  } %>
							 						
							<td>
							<input type="hidden" value="<%=k.getCategory()%>">
							<button class="ui button mini buttonStyle delete">삭제</button>
							<button class="ui red button mini buttonStyle modify">수정</button></td>
						</tr>
					<%} %>
				</tbody>
			</table>
		</div><!-- table Section End -->
		<div class="addArea"><button class="ui purple basic button mini" id="addBtn">추가하기</button></div>
		<div class="pagingArea" id="pagingArea">
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectListPage.ke?currentPage=1'"><<</button>
			<%if(currentPage <=1) {%>
				<button class="pagingbtn" disabled></button>
			<%} else{ %>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectListPage.ke?currentPage=<%=currentPage-1%>'"><</button>
			<%} %>
			
			<%for(int p=startPage; p<=endPage; p++){
				if(p == currentPage){ %>
					<button class="pagingbtn" disabled><%=p %></button>
				<%} else{ %>
					<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectListPage.ke?currentPage=<%=p%>'"><%=p %></button>
				<%}
			  } %>
			
			<%if(currentPage >= maxPage){ %>
				<button class="pagingbtn" disabled></button>
			<%} else {%>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectListPage.ke?currentPage=<%=currentPage+1%>'">></button>
			<%} %>
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectListPage.ke?currentPage=<%=maxPage%>'">>></button>
		</div><!-- PagingArea End -->
	</div><!-- Outline End -->
	
	<div class="ui basic modal" id="addModal">
		<div class="modalOuter">
			<h4>키워드 수정</h4>
			<hr>
			<form action="<%=request.getContextPath() %>/insert.ke" method="post" id="insertKeywordForm">
				<table align="center" style="height:150px;">
					<tr>
						<td><label>키워드명</label></td>
						<td><div class="ui input"><input name="kname" id="kname" type="text" size="20"></div></td>
					</tr>
					<tr>
						<td><label>카테고리</label></td>
						<td><select class="ui fluid dropdown" name="kcategory" id="kcategory">
								<option value="1">언어</option>
								<option value="2">모바일/OS</option>
								<option value="3">웹개발</option>
								<option value="4">프레임워크</option>
								<option value="5">라이브러리</option>
								<option value="6">DB</option>
								<option value="7">업무분야</option>
								<option value="8">Tool</option>
							</select>
						</td>
					</tr>
				</table>
				<div class="buttonArea">
					<input type="hidden" id="Mkcode" name="mkcode">
					<button type="submit" id="Madd" class="ui purple button mini">추가</button>
					<button type="submit" id="Mmodify" class="ui purple button mini" style="display:none">수정</button>
					<button type="reset" id="Mcancel" class="ui button mini">취소</button>
				</div>
			</form>
		</div>
	</div> <!-- 추가하기 모달 끝 -->
	
</body>
	<script>
		function replaceAll(sValue, param1, param2) { // replaceAll 이라는 변수 선언.

		   return sValue.split(param1).join(param2);

		}
		function searchKeyword(num){
			var searchCategory = $("#searchCategory").val();
			var searchText = "";
			var scategory = $("#scategory").val();
			var scurrentPage = 0;
			$tableBody = $("#logTable tbody");
			
			if(num == 'search'){
				scurrentPage = 1;
				$tableBody.html("<div class='ui active inverted dimmer'><div class='ui loader'></div></div>");
			} else{
				//console.log(num.innerText);
				if(num.innerText == '>>'){
					scurrentPage = $("#lastPage").val();
				} else{
					scurrentPage = num.innerText;
				}
			}
			
			if(searchCategory == 1){ //키워드명 검색
				searchText = $("#searchText").val();
			} else{ //키워드 분류 검색
				searchText = "nothing";
			}
			
			if(searchText == ""){
				swal ({
					   title : "검색어를 입력해주세요",
					   icon : "error"
					}).then((value) => {
						$("#searchText").focus();
					});
			} else{
				
				$.ajax({
					url : "search.ke",
					data : {
						currentPage:scurrentPage,
						searchCategory:searchCategory,
						searchText:searchText,
						scategory:scategory
					},
					type:"get",
					success:function(data){
						//console.log(data);
						
						//$tableBody = $("#logTable tbody");
						$pagingArea = $("#pagingArea").html('');
						
						$tableBody.html('');
						
						var keyword = data.keywordlist;
						
						var scurrentPage = data.scurrentPage;
						var slistCount = data.slistCount;
						var smaxPage = data.smaxPage;
						var sstartPage = data.sstartPage;
						var sendPage = data.sendPage;
						
						
						$.each(keyword, function(index, value){
							var $tr = $("<tr align='center'>");
							var $codeTd = $("<td>").text(value.kcode);
							var res = decodeURIComponent(value.kname); //decodeURIComponent로 디코딩 해서 받아온 후,
							//res = replaceAll(res, "+", " "); // 받아온 값을 replaceAll 이라는 함수를 통해 바꿔준다.
							var $nameTd = $("<td>").text(res);
							
							
							switch(value.category){
								case 1 : var $categoryTd = $("<td>").text("언어"); break;
								case 2 : var $categoryTd = $("<td>").text("모바일/OS"); break;
								case 3 : var $categoryTd = $("<td>").text("웹개발"); break;
								case 4 : var $categoryTd = $("<td>").text("프레임워크"); break;
								case 5 : var $categoryTd = $("<td>").text("라이브러리"); break;
								case 6 : var $categoryTd = $("<td>").text("DB"); break;
								case 7 : var $categoryTd = $("<td>").text("업무분야"); break;
								case 8 : var $categoryTd = $("<td>").text("Tool"); break;
								default : var $categoryTd = $("<td>");
							}
							var $input = $("<input type='hidden'>").val(value.category);
							//console.log($input);
							var $addModTd = $("<td>").append($input).append("<button class='ui button mini buttonStyle delete2'>삭제</button><button class='ui red button mini buttonStyle modify2'>수정</button>");
							$tr.append($codeTd);
							$tr.append($nameTd);
							$tr.append($categoryTd);
							$tr.append($addModTd);
							
							$tableBody.append($tr);
							
						});
						
						//페이징 처리!
						var $pageDiv = new Array();
						var $button = new Array();
						var $input2 = new Array();
						
						if(scurrentPage != 1){
							$pageDiv1 = $("<div class='eachArea'>");
							$button1 = $("<button class='pagingbtn' onclick='searchKeyword(1)'>").text('<<');
							$pageDiv1.append($button1);
							$pagingArea.append($pageDiv1);
						} 
						
						for(var p=sstartPage; p<=sendPage; p++){
							if(p==scurrentPage){
								$pageDiv[p] = $("<div class='eachArea'>");
								$button[p] = $("<button class='pagingbtn' disabled>").text(p);
									
							} else{
								$pageDiv[p] = $("<div class='eachArea'>");
								$button[p] = $("<button class='pagingbtn' onclick='searchKeyword(this)'>").text(p);
								//$input2[p] = $("<input type='hidden' id=''>").val(p);
							}
							$pageDiv[p].append($button[p]).append($input2[p]);
							$pagingArea.append($pageDiv[p]);
						} 
						
						
						if(scurrentPage != sendPage){
							$pageDiv2 = $("<div class='eachArea'>");
							$button2 = $("<button class='pagingbtn' onclick='searchKeyword(this)'>").text('>>');
							$input3 = $("<input type='hidden' id='lastPage'>").val(sendPage);
							$pageDiv2.append($button2);
							$pageDiv2.append($input3);
							$pagingArea.append($pageDiv2);
						}
						
						
							$(".buttonStyle.delete2").click(function(){
								var con = confirm("해당키워드를 삭제하시겠습니까?");
								if(con === true){
									var num = $(this).parent().parent().children().eq(0).text();
									location.href="<%=request.getContextPath()%>/delete.ke?num=" + num;
								}
							});
							$(".buttonStyle.modify2").click(function(){
								var kcode = $(this).parent().parent().children().eq(0).text();
								var kname = $(this).parent().parent().children().eq(1).text();
								var category = $(this).parent().find("input[type=hidden]").val();
								$("#kname").val(kname);
								$("#kcategory").val(category);
								$("#Mkcode").val(kcode);
								
								$("#Mmodify").css("display", "inline");
								$("#Madd").css("display", "none");
								
								
								$("#addModal").modal('show');
							});
							
							//$("#pagingArea").hide();
					},
					error:function(data){
						console.log("실패!");
					}
				})
				
			}
		}
	
	
		$(function(){
			//$("#logTable").tablesort();
			
			$("#addBtn").click(function(){
				$("#Mkcode").val(0);
				$("#Mmodify").css("display", "none");
				$("#Madd").css("display", "inline");
				$("#addModal").modal('show');
			});
			
			$("#Madd").click(function(){
				$("#insertKeywordForm").submit();
			});
			$("#Mcancel").click(function(){
				$("#addModal").modal('hide');
			});
			
			$(".buttonStyle.delete").click(function(){
				//console.log("삭제!");
				var con = confirm("해당키워드를 삭제하시겠습니까?");
				if(con === true){
					var num = $(this).parent().parent().children().eq(0).text();
					location.href="<%=request.getContextPath()%>/delete.ke?num=" + num;
				}
			});
			$(".buttonStyle.modify").click(function(){
				var kcode = $(this).parent().parent().children().eq(0).text();
				var kname = $(this).parent().parent().children().eq(1).text();
				var category = $(this).parent().find("input[type=hidden]").val();
				$("#kname").val(kname);
				$("#kcategory").val(category);
				$("#Mkcode").val(kcode);
				
				$("#Mmodify").css("display", "inline");
				$("#Madd").css("display", "none");
				
				
				$("#addModal").modal('show');
			});
			$("#Mnodify").click(function(){
				$("#insertKeywordForm").submit();
			});
			
			$("#searchCategory").change(function(){
				if($(this).val() == 1){
					$("#scategory").css("display", "none");
					$("#searchText").css("display", "inline");
				} else{
					$("#scategory").css("display", "inline");
					$("#searchText").css("display", "none");
					
				}
			});
			
		});
	</script>
</html>