<%@page import="keyword.model.vo.PageInfo"%>
<%@page import="admin.model.vo.AllPopRecruit"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	ArrayList<AllPopRecruit> list = (ArrayList<AllPopRecruit>) request.getAttribute("list");

	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
 	int endPage = pi.getEndPage();
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 페이지</title>
<%@ include file="/views/common/import.html"%>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
.outline {
	margin-right: auto;
	margin-left: auto;
	width: 80%;
	background: white;
	padding: 50px;
}

.searchSection {
	padding-top: 20px;
	padding-right: 20%;
	padding-left: 20%;
	padding-bottom: 20px;
	border: 1px solid lightgray;
	border-top: 2px solid purple;
	text-align: center;
}

.tableSection {
	padding-top: 20px;
}

.radioTable td {
	padding: 10px;
	font-size: 1.2em;
}

.buttonStyle {
	margin-right: 5px !important;
	margin-left: 5px !important;
}

#logTable td:last-of-type {
	width: 180px;
}

h1 {
	margin-left: 11%;
}

.search {
	margin-left: auto;
	margin-right: auto;
	width: 80%;
}

#searchlabel {
	width: 100px;
	heigth: 30px;
	padding-left: 10px;
	overflow: hidden;
}
	.pagingArea {
	text-align: center;
	padding: 20px;
	margin-bottom: 50px;
	margin-top: 30px;
}

.pagingbtn {
	height: 30px;
	width: 30px;
	display: inline-block;
	vertical-align: top;
	overflow: hidden;
	background: white;
	border: 1px solid lightgray;
	border-radius: 5px;
}

.pagingbtn:hover {
	cursor: pointer;
}

.pagingbtn:disabled {
	color: gray;
}
</style>
</head>
<body style="background: lightgray;">
	<%@ include file="../common/adminMenu.jsp"%>
	<h1>적극채용공고 관리</h1>

	<div class="outline">
		<div class="searchSection">
			<form class="ui form" action="" method="get">
				<div class="fields">
				</div>
				<table align="center" class="radioTable">
					<tr>
						<td><input type="radio" name="status" id="all" value="all" checked="checked"> <label for="all">전체</label></td>
						<td><input type="radio" name="status" id="ing" value="ing">
							<label for="ing">이용중</label></td>
						<td><input type="radio" name="status" id="waiting" value="waiting"> <label for="waiting">대기중</label></td>
						<td><input type="radio" name="status" id="end" value="end">
							<label for="end">이용종료</label></td>
					</tr>
					<tr>
						<td colspan="5"><input type="submit" value="검색" class="ui purple button" style="width: 150px;"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead>
					<tr align="center">
						<th>구분</th>
						<th>결제일자</th>
						<th>광고기간</th>
						<th>기업이름</th>
						<th>결제금액</th>
						<th>보기</th>
					</tr>
				</thead>
				<tbody>
				<%for(int i=0; i<list.size(); i++){ %>
					<tr align="center">
					<%if(list.get(i).getStatus()==1){ %>
						<td>이용중</td>
					<%}else if(list.get(i).getStatus()==2) {%>
						<td>대기중</td>
					<%}else { %>
						<td>종료</td>
					<%} %>
						<td><%=list.get(i).getPayDate() %></td>
						<td><%=list.get(i).getSDate()%> ~ <%=list.get(i).getEDate() %></td>
						<td>㈜ &nbsp;<%=list.get(i).getName() %></td>
						<td><%=list.get(i).getPMoney() %></td>
						<td>
							<button class="ui button mini buttonStyle goRecruit">보기</button>
							<input type="hidden" value =<%=list.get(i).getRecid()%>>
						</td>
					</tr>
				<%} %>
				</tbody>
			</table>
		</div>
			<!-- 페이징시작 -->
			<div class="pagingArea" id ="pagingArea">
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/popRecruitListManage?currentPage=1'"><<</button>
			<%if(currentPage <=1) {%>
				<button class="pagingbtn" disabled></button>
			<%} else{ %>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/popRecruitListManage?currentPage=<%=currentPage-1%>'"><</button>
			<%} %>
			
			<%for(int p=startPage; p<=endPage; p++){
				if(p == currentPage){ %>
					<button class="pagingbtn" disabled><%=p %></button>
				<%} else{ %>
					<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/popRecruitListManage?currentPage=<%=p%>'"><%=p %></button>
				<%}
			  } %>
			
			<%if(currentPage >= maxPage){ %>
				<button class="pagingbtn" disabled></button>
			<%} else {%>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/popRecruitListManage?currentPage=<%=currentPage+1%>'">></button>
			<%} %>
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/popRecruitListManage?currentPage=<%=maxPage%>'">>></button>
			</div>
		<!-- 페이징끝 -->
	</div>
	<script>
		$(".goRecruit").click(function(){
			var recid = $(this).parent().find("input[type=hidden]").val();
			console.log(recid);
			location.href = "<%=request.getContextPath()%>/recruit.so?RECID="+recid+"&type=2";
		})
	</script>

</body>
</html>





