<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*, declaration.model.vo.*"%>
<% 
	ArrayList<DeclarationList> list = (ArrayList<DeclarationList>) request.getAttribute("list");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 페이지</title>
<%@ include file="/views/common/import.html"%>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
.outline {
	margin-right: auto;
	margin-left: auto;
	width: 80%;
	height: 800px;
	background: white;
	padding: 50px;
}

.searchSection {
	padding-top: 20px;
	padding-right: 20%;
	padding-left: 20%;
	padding-bottom: 20px;
	border: 1px solid lightgray;
	border-top: 2px solid purple;
	text-align: center;
}

.tableSection {
	padding-top: 20px;
}

.radioTable td {
	padding: 10px;
	font-size: 1.2em;
}

.buttonStyle {
	margin-right: 5px !important;
	margin-left: 5px !important;
}

#logTable td:last-of-type {
	width: 180px;
}

h1 {
	margin-left: 11%;
}

.fields {
	padding-left: 20%;
}
#contentDetail:hover {
	cursor:pointer;
}
.AllContent {
	cursor:pointer;
	border: none;
	margin:15px;
	background:white;
	font-size: 15px;
	text-decoration: underline;
}
.content {
	background:white;
	border:none;
	font-size:15px;
}
.content:hover {
	cursor:pointer;
	text-decoration: underline;
}
</style>
</head>
<body style="background: lightgray;">
	<%@ include file="../common/adminMenu.jsp"%>
	<h1>회원 신고</h1>

	<div class="outline">
		<div class="searchSection">
			<form class="ui form" action="" method="get">
				
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown">
							<option>신고된 회원 ID</option>
							<option>접수자 ID</option>
						</select>
					</div>
					<div class="ui icon input">
						<input type="text" placeholder="Search..." disabled> <i class="circular search link icon"></i>
					</div>
				</div>
				<div class="radioArea" style="padding:10px; font-size:1.2em;">
					<div style="display:inline-block; width:100px; margin-left:10px; margin-right:10px;">
						<input type="radio" id="searchListAny" name="radioBtn" value="anyContent" checked><label for="searchListAny"> 미처리</label>
					</div>
					<div style="display:inline-block; width:100px; margin-left:10px; margin-right:10px;">
						<input type="radio" id="searchListAny2" name="radioBtn" value="anyContent2"><label for="searchListAny2"> 처리완료</label>
					</div>
					<div style="display:inline-block; width:100px; margin-left:10px; margin-right:10px;">
						<input type="radio" id="searchListAll" name="radioBtn" value="allContent"><label for="searchListAll"> 전체</label>
					</div>
				</div>
				<table align="center" class="radioTable">
					<tr>
						<td colspan="5"><input type="button" id="searchBtn" value="검색" class="ui purple button" style="width: 150px;"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead align="center">
					<tr align="center">
						<th>신고일자</th>
						<th>신고된 회원 ID</th>
						<th>접수자 ID</th>
						<th>접수자 Email</th>
						<th>신고 내역</th>
						<th>처리 내역</th>
					</tr>
				</thead>
				<tbody>
					<% for(int i = 0; i < list.size(); i++) { %>
						<tr align="center">
							<td><%= list.get(i).getDecDate() %></td>
							<td><%= list.get(i).getrUserId() %></td>
							<td><%= list.get(i).getgUserid() %></td>
							<td><%= list.get(i).getEmail() %></td>
							<td>
								<input type="hidden" value="<%=list.get(i).getContent()%>">
								<input type="button" class="content" value="내용보기">
							</td>
							<td>
								<input type="hidden" value="<%=list.get(i).getdId()%>">
								<% if(list.get(i).getCategory() == 1) { %>
									<button class="ui red button mini buttonStyle decide">처리하기</button>
								<% }else if(list.get(i).getCategory() == 2) { %>
									<button class="ui button mini buttonStyle" disabled>처리완료</button>
								<% }else { %>
									<button class="ui button mini buttonStyle" disabled>반려</button>
								<% } %>
							</td>
						</tr>
					<% } %>
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="ui modal v1" style="width:350px; height:170px; text-align:center; vertical-align:middle; padding:20px;">
		<i class="close icon"></i>
		<div class="description">
			<div align="center">
				<h3 style="height:25px; margin-top:10px;">신고 내용에 대해 경고처리 하시겠습니까?</h3>
			</div><br>
			<div align="center">
				<form action="<%=request.getContextPath()%>/decPersonApp.adl" id="confirmForm">
					<input type="hidden" name="decid" id="mdecid">
					<div class="ui input" style="margin-bottom:10px;">
						<input type="text" id="content" name="content" size="30"></div>
					<input type="hidden" name="type" id="type">
					<input type="button" class="ui red button" style="width:100px;" id="confirm" value="승인">
					<input type="button" class="ui blue button" style="width:100px;" id="confirm2" value="반려">
				</form>
			</div>
		</div>
	</div> <!-- 처리하기 모달 끝 -->
	
	<div class="ui modal v2" style="width:350px; height:170px; text-align:center; vertical-align:middle; padding:20px;">
		<i class="close icon"></i>
		<div class="description">
			<div align="center">
				<h3>신고 접수 내용 : </h3>
				<div style="height:50px; margin:10px; padding-top:10px; border:1px solid lightgray; border-radius:5px;" id="declarationContent"></div>
			</div>
			<div align="center">
				<button class="ui blue button close2" style="width:300px;">OK</button>
			</div>
		</div>
	</div> <!-- 신고내역 모달 끝 -->
	
	<script>
	
	$(function(){
		$("#logTable").tablesort();
		
		$(".decide").click(function(){
			$("#mdecid").val($(this).parent().find("input[type=hidden]").val());
			
			$(".modal.v1").modal('show');
		});
		
		$("#confirm").click(function(){
			//var num = $(this).parent().find("input[type=hidden]").val();
			if($("#content").val() == ""){
				alert("신고승인 내용을 입력해주세요.");
			} else{
				$("#type").val(1);
				$("#confirmForm").submit();
			}
			
			
		});
		$("#confirm2").click(function(){
			if($("#content").val() == ""){
				alert("신고반려 내용을 입력해주세요.");
			} else{
				$("#type").val(2);
				$("#confirmForm").submit();
			}
		});
		
		$(".content").click(function(){
			var content = $(this).parent().find("input[type=hidden]").val();
			$("#declarationContent").text(content);
			$('.ui.modal.v2').modal('show');
		});
		
		
		
		$(".close1").click(function(){
			$('.ui.modal.v1')
			  .modal('hide');
		});
		
		$(".close2").click(function(){
			$('.ui.modal.v2')
			  .modal('hide');
		});
		
		$(".close3").click(function(){
			$('.ui.modal.v4')
			  .modal('hide');
		});
		
		$(".companion").click(function(){
			$('.ui.modal.v4').modal('show');
			$('.ui.modal.v3').modal('hide');
		});
		
		/* $("input[type=radio]").click(function(){
			
			if($("input[name=radioBtn]:checked").val() === 'AllContent'){
				$('.hidden').show();
				$('.AnyContent').show();
				$('.AllContent').hide();
			}else {
				$('.hidden').hide();
				$('.AllContent').show();
				$('.AnyContent').hide();
			}
		}); */
		
		$("#searchBtn").click(function(){
			//console.log($("input[type=radio]:checked").val());
			var category = $("input[type=radio]:checked").val();
			var type = 0;
			//console.log(category);
			if(category == "allContent"){
				type = 1;
			} else if(category == "anyContent2"){
				type = 2;
			} else{
				type = 0;
			}
			$tableBody = $("#logTable tbody");
			$tableBody.html("<div class='ui active inverted dimmer'><div class='ui loader'></div></div>");

			$.ajax({
				url : "search.adl",
				data : {
					type : type
				},
				type : "get",
				success:function(data){
					//console.log(data);
					//$tableBody = $("#logTable tbody");
					//$pagingArea = $("#pagingArea").html('');
					
					$tableBody.html('');
					
					$.each(data, function(index, value){
						var $tr = $("<tr align=center>");
						
						
						if(value.category==1){
							$hiddenInput2 = $("<input type='hidden'>").val(value.decId);
							$buttonTd = $("<button class='ui red button mini buttonStyle decide'>처리하기</button>");
							$statusTd = $("<td>").append($hiddenInput2).append($buttonTd);
							
						} else if(value.category==2){
							$statusTd = $("<td>").text("승인완료").css("color","blue");
						} else{
							$statusTd = $("<td>").text("반려완료").css("color","red");
						}
						
						$dateTd = $("<td>").text(value.decDate);
						$rUserIdTd = $("<td>").text(value.rUserId);
						$gUserIdTd = $("<td>").text(value.gUserId);
						$emailTd = $("<td>").text(value.email);
						var res = decodeURIComponent(value.content);
						res = replaceAll(res, "+", " ");
						$hiddenInput=$("<input type='hidden'>").val(decodeURIComponent(res));
						$inputTd = $("<td>").append($hiddenInput).append("<input type='button' class='content' value='내용보기'>");
						
						
						$tr.append($dateTd);
						$tr.append($rUserIdTd);
						$tr.append($gUserIdTd);
						$tr.append($emailTd);
						$tr.append($inputTd);
						$tr.append($statusTd);
						
						$tableBody.append($tr);

					});
					
					$(".content").click(function(){
						var content = $(this).parent().find("input[type=hidden]").val();
						$("#declarationContent").text(content);
						$('.ui.modal.v2').modal('show');
					});
					
					$(".decide").click(function(){
						$("#mdecid").val($(this).parent().find("input[type=hidden]").val());
						
						$(".modal.v1").modal('show');
					});
					
				},
				error:function(data){
					console.log("Error!");
				}
			});
			
			
			
		});
		
	});
	

	function replaceAll(sValue, param1, param2) { // replaceAll 이라는 변수 선언.
		   return sValue.split(param1).join(param2);
	}
		/* 
		function modal(bid) {
			Bid = bid;
			$('.ui.modal.v1')
			  .modal('show');
		} */
		
		function Content(content) {
			
		}
		
		/* function select() {
			$('.ui.modal.v3').modal('show');
			$('.ui.modal.v2').modal('hide');
		} */
		
		function approval() {
			
		}
		
		function companion() {
			
		}
	</script>
</body>
</html>