<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, keyword.model.vo.*, email.model.vo.*"%>
    
    <%
    ArrayList<Email> list = (ArrayList<Email>)request.getAttribute("list");
    
    
    PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
    
    %>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="/views/common/import.html" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>

<style>
.outline{
		margin-right:auto;
		margin-left:auto;
		width:80%;
		height:800px;
		background:white;
		padding:50px;
	}
	.searchSection{
		padding-top:20px;
		padding-right:20%;
		padding-left:20%;
		padding-bottom:20px;
		border:1px solid lightgray;
		border-top:2px solid purple;
		text-align:center;
	}
	.tableSection{
		padding-top:20px;
	}
	
	.radioTable td{
		padding:10px;
		font-size:1.2em;
	}
	
	.buttonStyle {
		margin-right:5px !important;
		margin-left:5px !important;
	}
	
	#logTable td:last-of-type {
		width:180px;
	}
	
	h1{
		margin-left:11%;
	}
	
	.fields {
	
	padding-left:20%;
	
	
	}
	.eachArea{
		display:inline-block;
		margin:2px;
	}
	.pagingArea{
		text-align:center;
		padding:20px;
		margin-bottom:50px;
		margin-top:30px;
	}
	.pagingbtn{
		height:30px;
		width:30px;
		display:inline-block;
		vertical-align:top;
		overflow:hidden;
		background:white;
		border:1px solid lightgray;
		border-radius:5px;
	}
	.pagingbtn:hover{
		cursor:pointer;
	}
	.pagingbtn:disabled{
		color:gray;
	}
	#content:focus {
	
	outline:none;
	
	}
	
	



</style>


</head>
<body style="background:lightgray;">
<%@ include file="../common/adminMenu.jsp" %>
<h1>이메일 문의 관리</h1>

<div class="outline">
<div class="searchSection">
			<form class="ui form" action="" method="get">
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown" id="searchCategory">
							<option value="1">제목</option>
							<option value="2">내용</option>
							<option value="3">이메일</option>
							
						</select>
					</div>
						<div class="ui icon input">
  						<input type="text" id="searchText" placeholder="Search..." disabled>
  						<i class="circular search link icon"></i>
					</div>
				</div>
				<table align="center" class="radioTable">
				
					<tr>
						<td colspan="5"><input type="button" value="검색" class="ui purple button" style="width:150px;" onclick="searchKeyword('search')"></td>
					</tr>
				</table>
			</form>
		</div>
<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead align="center">
					<tr>
					<th>문의번호</th>
					<th style="display:none">회원번호</th>
					<th  style="display:none">내용</th>
						<th>제목</th>
						
						<th>문의날짜</th>
						
						<th>회원구분</th>
						<th>이메일</th>
						<th>처리여부</th>
					</tr>
				</thead>
				<tbody align="center">
					<%for(int j = 0; j < list.size(); j++){ %>
					
					<tr>
					
					<td><%= list.get(j).getQid() %></td>
					<td style="display:none"><%= list.get(j).getUno() %></td>
					<td style="display:none"><%= list.get(j).getQcontent() %></td>
					<td><%= list.get(j).getQtitle()%></td>
					
					<td><%= list.get(j).getQdate()        %></td>
					<%if(list.get(j).getCategory() == 1) { %>
					<td>개인회원</td>
					<%} else { %>
					<td>기업회원</td>
					<%} %>
					<td><%= list.get(j).getQemail() %></td>
					
					<td><input type="button" class="ui button mini buttonStyle" id="emailSend" value="처리"></td>
					</tr>
					
					<%} %>
					
					
				</tbody>
			</table>
		</div>
		<div class="pagingArea" id="pagingArea">
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectEmailList.ad?currentPage=1'"><<</button>
			<%if(currentPage <=1) {%>
				<button class="pagingbtn" disabled></button>
			<%} else{ %>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectEmailList.ad?currentPage=<%=currentPage-1%>'"><</button>
			<%} %>
			
			<%for(int p=startPage; p<=endPage; p++){
				if(p == currentPage){ %>
					<button class="pagingbtn" disabled><%=p %></button>
				<%} else{ %>
					<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectEmailList.ad?currentPage=<%=p%>'"><%=p %></button>
				<%}
			  } %>
			
			<%if(currentPage >= maxPage){ %>
				<button class="pagingbtn" disabled></button>
			<%} else {%>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectEmailList.ad?currentPage=<%=currentPage+1%>'">></button>
			<%} %>
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectEmailList.ad?currentPage=<%=maxPage%>'">>></button>
		

</div>
<div class="ui modal" style="height:500px; width:550px;">
  <i class="close icon"></i>
  <div class="header">
    이메일 문의 처리
  </div>
  <div class="image content">
   
    <div class="description">
      <div class="ui header" style="white-space:pre-line; ">질문내용</div>
       <textarea id="content" name="content" style="height:200px; width:500px; resize:none; border: none;" readonly ></textarea>
      
    </div>
  </div>
 
  <div class="actions" >
   <div style="padding-right:90%;  height:200px; width:200px; ">
  <textarea style="height:150px; width:500px; resize:none; " id="textarea2"> </textarea>
  
  </div>
    <div class="ui black deny button">
      취소
    </div>
    <div class="ui positive right labeled icon button" id="submit">
     문의 답변

      <i class="checkmark icon"></i>
    </div>
  </div>
</div>


</body>

<script>


$("#logTable input[type=button]").mouseenter(function(){
	
	$(this).parent().parent().css({"background":"skyblue","cursor":"pointer"})
	
}).mouseout(function(){
	
	$(this).parent().parent().css({"background":"white"});
	
}).click(function(){
	var num = $(this).parent().parent().children().eq(1).text();
	var content = $(this).parent().parent().children().eq(2).text();
	
	if($(this).attr('id') == "emailSend") {
		$("#content").html(content).css("white-space","pre-line");	
		$('.ui.modal').modal('show');
		$("#textarea2").focus();
		
		
		<%-- location.href = "<%=request.getContextPath()%>/SearchPersonOne.ad?num=" + num; --%>
	
		
	}
});

$("#submit").click(function(){
	

	var email = $("#logTable input[type=button]").parent().parent().children().eq(6).text();
	var answer = $("#textarea2").val();
	var num = $("#logTable input[type=button]").parent().parent().children().eq(0).text();
	
	
	location.href = "<%=request.getContextPath()%>/EmailSend.ad?answer=" + answer + "&email=" + email + "&num=" + num;
	
	
})








</script>
</html>