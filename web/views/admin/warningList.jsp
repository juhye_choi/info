<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="/views/common/import.html" %>
<style>
	.outline{
			width:1000px;
			height:650px;
			margin-right:auto;
			margin-left:auto;
	}
	.mainBtn{
			width:30%;
			height:50px;
			margin:-2px;
			padding-top:12px;
			background:#e7e6e1;
			border:1px solid black;
			display:inline-block;
			text-align:center;
			vertical-align:middle;
			font-size:1.2em;
	}
	.mainBtn:hover{
			cursor:pointer;
	}
	
	.title{
			margin:30px;
			margin-left:20px;
			font-size:1.8em;
	}	
	.searchArea{
		padding-top:20px;
		padding-right:20%;
		padding-left:20%;
		padding-bottom:20px;
		border:1px solid lightgray;
		border-top:2px solid purple;
		text-align:center;
	}
	.radioTable td{
		padding:10px;
		font-size:1.2em;
	}
	.fields {
		padding-left:20%;
	}	
</style>
</head>
<body>
<%@ include file="../common/adminMenu.jsp" %>
	<div>
		<div class="title">지원현황</div>
	
		<div class="searchArea">
			<form class="ui form" action="" method="get">
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown">
							<option>ID</option>
							<option>이름</option>
							<option>EMAIL</option>
							<option>경고 처리 획수</option>
						</select>
					</div>
					<div class="ui icon input">
  						<input type="text" placeholder="Search">
  						<i class="circular search link icon"></i>
					</div>
				</div>
				<table align="center" class="radioTable">
					<tr>
						<td colspan="5"><input type="submit" value="검색" class="ui purple button" style="width:150px;"></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</body>
</html>