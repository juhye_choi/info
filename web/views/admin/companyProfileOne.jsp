<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="company.model.vo.*"%>
    
    <%
    Company c = (Company)request.getAttribute("company");
%>
<!DOCTYPE html>
<html>
<head>
<style>
.outline{
		margin-right:auto;
		margin-left:auto;
		width:80%;
		height:1000px;
		background:white;
		padding:50px;
	}
	#logoImgArea {
	width:300px;
	height:300px;
}

#logoImg {
	 width:200px;
	 height:200px;
	 margin:50px;
	 border:1px solid black;
}



td {
	padding-top: 30px;
	font-size:15px;
	font-weight:bold;
}

</style>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="/views/common/import.html" %>
</head>
<body style="background:lightgray;">
<%@ include file="../common/adminMenu.jsp" %>
<h1 style="margin-left:10%;">개인회원 상세정보</h1>
<form id="modifyForm" action="<%=request.getContextPath()%>/updateCompany.me" method="post" encType="multipart/form-data">
	<div class="outline">
		
		<table align="center" style="border-top:1px solid gray;">
			<tr>
				<td width="800px">
					<table>
						<tr>
							<td width="130px">아이디</td>
							<td width="370px"><div class="ui fluid input"><input type="text" value="<%=loginUser.getUserId()%>" disabled></div></td>
						</tr>
						<tr>
							<td>비밀번호</td>
							<td><div class="ui fluid input"><input type="password" placeholder="********" readonly disabled></div></td>
							
						</tr>
						<tr>
							<td>인사담당자</td>
							<td>
								<% if(c.getHrManager() != null){ %>
								<div class="ui fluid input"><input type="text" id="hrName" name="hrName" value="<%=c.getHrManager()%>" disabled></div>
								<% } else { %>
								<div class="ui fluid input"><input type="text" id="hrName" name="hrName" value="미입력" disabled></div>
								<% } %>
							</td>
						</tr>
						<tr>
							<td>담당자 연락처</td>
							<td><div class="ui fluid input"><input type="text" id="phone" name="phone" value="<%= c.getPhone() %>" disabled></div></td>
						</tr>
						<tr>
							<td>담당자 이메일</td>
							<td><div class="ui fluid input"><input type="text" id="email" name="email" value="<%= loginUser.getEmail() %>" disabled></div></td>
							 <td><input type="hidden" name="uno1" id="uno1" value="<%= loginUser.getUno() %>"></td>
						</tr>
					</table>
				</td>
				<td width="300px" align="center">
					<div id="logoImgArea">
						<% if(c.getLogo() != null) { %>
							<img src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=c.getLogo() %>" id="logoImg">
						<% } else { %>
							<img src="<%=request.getContextPath()%>/images/profilePhoto.jpg" id="logoImg">
						<% } %>
					</div>
					
				</td>
			</tr>
		</table>
		
		
		<div align="center" style="padding-top:50px;">
			<input type="button" class="ui secondary button" onclick="cancle();" value="목록으로 돌아가기">
		</div>
	</div>
	</form>
	

</body>
<script>
	
	function cancle() {
		
		
		
		location.href="<%=request.getContextPath()%>/SelectCompanyListPage.ad?currentPage=1";
		
		
		
		
		
		
	}
	
	
	
	</script>
</html>