<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*, declaration.model.vo.*"%>
<% ArrayList<DeclarationList> list = (ArrayList<DeclarationList>) request.getAttribute("list");%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 페이지</title>
<%@ include file="/views/common/import.html"%>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
.outline {
	margin-right: auto;
	margin-left: auto;
	width: 80%;
	height: 800px;
	background: white;
	padding: 50px;
}

.searchSection {
	padding-top: 20px;
	padding-right: 20%;
	padding-left: 20%;
	padding-bottom: 20px;
	border: 1px solid lightgray;
	border-top: 2px solid purple;
	text-align: center;
}

.mainBtn {
	width: 30%;
	height: 50px;
	margin: -2px;
	padding-top: 12px;
	background: #e7e6e1;
	border: 1px solid black;
	display: inline-block;
	text-align: center;
	vertical-align: middle;
	font-size: 1.2em;
}

.mainBtn:hover {
	cursor: pointer;
}

.title {
	margin: 30px;
	margin-left: 20px;
	font-size: 1.8em;
}

.tableSection {
	padding-top: 20px;
}

.radioTable td {
	padding: 10px;
	font-size: 1.2em;
}

.buttonStyle {
	margin-right: 5px !important;
	margin-left: 5px !important;
}

#logTable td:last-of-type {
	width: 180px;
}

h1 {
	margin-left: 11%;
}

.fields {
	padding-left: 20%;
}
</style>
</head>
<body style="background: lightgray;">
	<%@ include file="../common/adminMenu.jsp"%>
	<h1>회원 경고 리스트</h1>
	<div class="outline">
		<div class="searchSection">
			<form class="ui form" action="" method="get">
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown">
							<option>ID</option>
							<option>이름</option>
							<option>Email</option>
						</select>
					</div>
					<div class="ui icon input">
						<input type="text" placeholder="Search..." disabled="disabled"> <i
							class="circular search link icon"></i>
					</div>
				</div>
				<table align="center" class="radioTable">

					<tr>
						<td colspan="5"><input type="submit" value="검색"
							class="ui purple button" style="width: 150px;"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead align="center">
					<tr>
						<th>번호</th>
						<th>ID</th>
						<th>이름</th>
						<th>Email</th>
						<th>신고 처리 횟수</th>
					</tr>
				</thead>
				<tbody align="center">
					<% for(int i = 0; i < list.size(); i++) { %>
						<tr>
							<td><%= i + 1 %></td>
							<td><%= list.get(i).getgUserid() %></td>
							<td><%= list.get(i).getgName() %></td>
							<td><%= list.get(i).getEmail() %></td>
							<td><%= list.get(i).getCount() %></td>
						</tr>
					<% } %>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>