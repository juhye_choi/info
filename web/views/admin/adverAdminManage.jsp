<%@page import="admin.model.vo.AdverAcceptUpdate"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/views/common/import.html"%>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> <!-- datePicker//js,css file -->

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><!-- Bootstrap js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script><!-- Moment js -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/lightpick.css"> <!--Lightpick CSS-->
<script src="<%=request.getContextPath()%>/js/lightpick.js"></script><!--Lightpick JS--> 

<title>관리자 페이지</title>
<style>
.outline {
	margin-right: auto;
	margin-left: auto;
	width: 80%;
	height: 800px;
	background: white;
	padding: 50px;
}

.searchSection {
	padding-top: 20px;
	padding-right: 20%;
	padding-left: 20%;
	padding-bottom: 20px;
	border: 1px solid lightgray;
	border-top: 2px solid purple;
	text-align: center;
}

h1 {
	margin-left: 11%;
}
.sectionOne {
	width: 55%;
	height: 500px;
	padding-top: 150px;
	padding-left: 100px;
	display: inline-block;
	overflow: hidden;
}
.sectionTwo{
	width: 40%;
	height: 500px;
	padding-top: 100px;
	padding-left : 60px;
	display: inline-block;
	overflow: hidden;
}
.select{
	display:inline-block;
	width:40%;
	height:400px;
	overflow: hidden;
}
#select{
	width: 150px;
	heigth:80px;
	margin-bottom: 50px;
	margin-left:20px;
	font-size: 1.5em;
	font-weight:bold;
}
#option{
	margin-bottom:30px;
	margin-left:20px;
}
#photo{
	width:300px;
	height:350px;
	border: 1px solid black;
	margin-bottom: 30px;
	text-align: center;
}
#photo img{
	max-width: 100%;
    max-height: 100%;
}
#button{
	width: 100px;
	margin-left: 30px;
}
</style>
</head>
<body style="background: lightgray;">
	<%@ include file="../common/adminMenu.jsp"%>
	<h1>광고 배너 관리</h1>
	<div class="outline">
		
			<div class="sectionOne">
				<div class="select">
					<div id = "select">광고 배너 선택</div>
					<div id = "select">기업 이름</div>
					<div id = "select">기간</div>
					<div id = "select">결제 금액</div>
				</div>
				<div class="select">
<!-- 광고배너 선택 옵션 --><div id ="option">
						<select class="ui dropdown" >
							<option value="0" selected="selected">포트폴리오 배너</option>
							<option value="1">로그인 좌측 배너</option>
							<option value="2">로그인 우측 배너</option>
						</select>
					</div>
					
<!-- 기업이름 입력 -->	<div class="ui input" id ="option"><input type="text" placeholder="Search..." ></div>
					
<!-- 기간 -->			<div id ="option" >
						<input style="width: 180px; height: 30px; text-align: center;" type="text" id="demo-5" class="form-control form-control-sm" >
					</div>
					
<!-- 결제금액입력창 -->	<div class="ui input" id ="option"><input type="text" placeholder="000,000원"></div>
					
				</div>
			</div>
			<div class="sectionTwo"> 
				<div id = "photo">
					<img id = "updateImg"  alt="" src="">
				</div>
				<form action="upload" id="uploadForm" method="post" enctype="multipart/form-data">
					<input type="file" id = "fileData">		
				</form>
				<br>	
			</div>
			<div style="height: 50px;"></div>
			<div style="text-align: center;"> 
			<button class="ui primary button updateAdver"  id = "button">확인</button>
			<button class="ui button" id = "button" onclick= "reset();">취소</button>
			</div>
		
	</div>
	<script>
		var picker = new Lightpick({
			field : document.getElementById('demo-5'),
			singleDate : false,
			/* numberOfColumns : 3,
			numberOfMonths : 1, */
			onSelect : function(start, end) {
				var str = ''
				str += start ? start.format('YYYY.MM.DD') + ' ~ ' : '';
				str += end ? end.format('YYYY.MM.DD') : '...';
				console.log(str);
				//document.getElementById('result-5').innerHTML = str;
			}
		});
		
		
		
		
		
		$(function(){
			/* 사진변경시 띄어주는 function */
			$('#fileData').change(function(){
			    setImageFromFile(this, '#updateImg');
			});
			function setImageFromFile(input, expression) {
			    if (input.files && input.files[0]) {
			        var reader = new FileReader();
			        reader.onload = function (e) {
			            $(expression).attr('src', e.target.result);
			        }
			        reader.readAsDataURL(input.files[0]);
			    }
			}/* 사진변경시 띄어주는 function end */
		});
		
		
		
		
		
		
		
		function reset(){
			location.href="<%=request.getContextPath()%>/selectAdverAcc";
		}
	</script>
</body>
</html>