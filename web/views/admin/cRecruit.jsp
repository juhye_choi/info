<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,keyword.model.vo.* ,member.model.vo.*, person.model.vo.RecruitInfo"%>
    
    <%
    
    ArrayList<HashMap<String, Object>> list = ( ArrayList<HashMap<String, Object>>)request.getAttribute("list");
    
    for(int i = 0; i < list.size(); i++) { 
  		HashMap<String, Object> hmap = list.get(i);
    }
    
    PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 페이지</title>
<%@ include file="/views/common/import.html" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
	.outline{
		margin-right:auto;
		margin-left:auto;
		width:80%;
		height:800px;   
		background:white;
		padding:50px;
	}
	.searchSection{
		padding-top:20px;
		padding-right:20%;
		padding-left:20%;
		padding-bottom:20px;
		border:1px solid lightgray;
		border-top:2px solid purple;
		text-align:center;
	}
	.tableSection{
		padding-top:20px;
	}
	
	.radioTable td{
		padding:10px;
		font-size:1.2em;
	}
	
	.buttonStyle {
		margin-right:5px !important;
		margin-left:5px !important;
	}
	
	#logTable td:last-of-type {
		width:180px;
	}
	
	h1{
		margin-left:11%;
	}
	
	.fields {
	
	padding-left:20%;
	
	
	}
	.pagingArea{
		text-align:center;
		padding:20px;
		margin-bottom:50px;
		margin-top:30px;
	}
	.pagingbtn{
		height:30px;
		width:30px;
		display:inline-block;
		vertical-align:top;
		overflow:hidden;
		background:white;
		border:1px solid lightgray;
		border-radius:5px;
	}
	.pagingbtn:hover{
		cursor:pointer;
	}
	.pagingbtn:disabled{
		color:gray;
	}
	
	
</style>
</head>
<body style="background:lightgray;">
	<%@ include file="../common/adminMenu.jsp" %>
	<h1>공고게시물 관리</h1>
	
	<div class="outline">
		<div class="searchSection">
			<form class="ui form" action="" method="get">
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown">
							<option>회사명</option>
							<option>회원ID</option>
							<option>게시물 공고 날짜</option>
						</select>
					</div>
						<div class="ui icon input">
  						<input type="text" placeholder="Search..." disabled>
  						<i class="circular search link icon"></i>
					</div>
				</div>
				<table align="center" class="radioTable">
				
					<tr>
						<td colspan="5"><input type="button" value="검색" class="ui purple button" style="width:150px;" ></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead align="center">
					<tr>
					<th>공고번호</th>
						<th>회사명</th>
						<th>회원ID</th>
						<th>담당자 이메일</th>
						<th>접수 시작일</th>
						<th>접수 종료일</th>
						<th width="200px;">조회/삭제</th>
					</tr>
					
					
				</thead>
				<tbody align="center">
				<%for(int j = 0; j < list.size(); j++){ %>
				<tr>
					<td ><%= list.get(j).get("recId") %></td>
					<td><%= list.get(j).get("name") %></td>
					<td><%=list.get(j).get("userId") %></td>
					<td><%= list.get(j).get("email") %></td>
					
					<td><%= list.get(j).get("recStart") %></td>
					<td><%= list.get(j).get("recFinish") %></td>
					<td><button class="ui button mini buttonStyle" id="recruitSelect" ><i class="search icon"></i>조회</button><button class="ui red button mini buttonStyle" id="recruitDelete" ><i class="trash alternate icon"></i>삭제</button></td>
					</tr>
				
				
				<%} %>
			
					
				</tbody>
			</table>
		</div>
		<div class="pagingArea" id="pagingArea">
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyRecruitList.ad?currentPage=1'"><<</button>
			<%if(currentPage <=1) {%>
				<button class="pagingbtn" disabled></button>
			<%} else{ %>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyRecruitList.ad?currentPage=<%=currentPage-1%>'"><</button>
			<%} %>
			
			<%for(int p=startPage; p<=endPage; p++){
				if(p == currentPage){ %>
					<button class="pagingbtn" disabled><%=p %></button>
				<%} else{ %>
					<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyRecruitList.ad?currentPage=<%=p%>'"><%=p %></button>
				<%}
			  } %>
			
			<%if(currentPage >= maxPage){ %>
				<button class="pagingbtn" disabled></button>
			<%} else {%>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyRecruitList.ad?currentPage=<%=currentPage+1%>'">></button>
			<%} %>
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyRecruitList.ad?currentPage=<%=maxPage%>'">>></button>
		</div>
	</div>
</body>

<script>

$(function(){
	
	
	
	$("#logTable button").mouseenter(function(){
		
		$(this).parent().parent().css({"background":"skyblue","cursor":"pointer"})
		
	}).mouseout(function(){
		
		$(this).parent().parent().css({"background":"white"});
		
	}).click(function(){
		
		
		
		var RECID = $(this).parent().parent().children().eq(0).text();
							
						if($(this).attr('id') == "recruitSelect") {
							
							 window.open('<%=request.getContextPath()%>/recruit.so?RECID=' +RECID +'&type=2', "채용공고", "location=0, resizable=no, menubar=no, status=no, toolbar=no"); 
							
						}else if($(this).attr('id') == "recruitDelete") {
							
							var check =	confirm("정말 강제로 탈퇴 시킵니까?");
			            	if(check == true) {
			            		
			            		location.href="<%=request.getContextPath()%>/DeleteRecruit.ad?RECID=" +RECID +"&type=2";
			            		
			            	
			            		
			            	}
							
							
						
							
						}
							
						
						
						
							
						
						
						
						
						
						
					})

	
	
	
		

			
		
			
			
		
			
		
			
		
		
		
			
		
		
		
		
		
		
	})
	
	

	
	
	
	



</script>
</html>





