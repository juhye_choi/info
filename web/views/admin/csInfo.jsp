<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 페이지</title>

<style>

.outline{
		margin-right:auto;
		margin-left:auto;
		width:80%;
		height:800px;
		background:white;
		padding:50px;
	}
	.searchSection{
		padding-top:20px;
		padding-right:20%;
		padding-left:20%;
		padding-bottom:20px;
		border:1px solid lightgray;
		border-top:2px solid purple;
		text-align:center;
	}
	.tableSection{
		padding-top:20px;
	}
	
	.radioTable td{
		padding:10px;
		font-size:1.2em;
	}
	
	.buttonStyle {
		margin-right:5px !important;
		margin-left:5px !important;
	}
	
	#logTable td:last-of-type {
		width:180px;
	}
	
	h1{
		margin-left:11%;
	}
	
	.fields {
	
	padding-left:20%;
	
	
	}
	

</style>
<%@ include file="/views/common/import.html" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body style="background:lightgray;">
	<%@ include file="../common/adminMenu.jsp" %>
	
	<h1 style="margin-left:10%;">개인/기업회원 FAQ</h1>
	
	<div class="outline">
		<div class="searchSection">
			<form class="ui form" action="" method="get">
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown">
							<option>제목</option>
							<option>내용</option>
							
						</select>
					</div>
						<div class="ui icon input">
  						<input type="text" placeholder="Search..." disabled>
  						<i class="circular search link icon"></i>
					</div>
				</div>
				<table align="center" class="radioTable">
					<tr>
					<td><input type="radio" name="category" id="all" value="all" checked="checked"> <label for="all">전체</label></td>
						<td><input type="radio" name="category" id="person" value="person" checked="checked"> <label for="person">개인</label></td>
						<td><input type="radio" name="category" id="company" value="company"> <label for="company">기업</label></td>
						
					</tr>
					<tr>
						<td colspan="5"><input type="submit" value="검색" class="ui purple button" style="width:150px;"></td>
					</tr>
				</table>
			</form>
		</div>
		
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead align="center">
					<tr>
						<th>등록번호</th>
						<th>분류</th>
						<th>제목</th>
						
						<th>작성자</th>
						<th>등록날짜</th>
						
						
					</tr>
				</thead>
				<tbody align="center">
					<tr>
					<td>1</td>
					<td>개인</td>
						<td>이력서 등록방법</td>
						
						<td>admin</td>
						<td>2018.09.01</td>
						
						
						
					</tr>
					<tr>
					<td>2</td>
					<td>개인</td>
						<td>개인정보 보호정책</td>
						
						<td>admin	</td>
						<td>2018.09.01	</td>
						
						
					
					</tr>
					<tr>
					<td>3</td>
					<td>기업</td>
						<td>아이디/비밀번호 찾기</td>
						
						<td>admin</td>
						<td>2017.04.30</td>
						
						
						
					</tr>
					<tr>
					<td>4</td>
					<td>개인</td>
						<td>개인회원 탈퇴방법</td>
						
						<td>  admin
</td>
						<td>2019.11.03	</td>
						
						
						
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	
	
	
	
	
	
	<script>
	
	$(function(){
		
		$("#logTable").tablesort();
		
	})
	
	$(function(){
		
		
		$("#logTable td").mouseenter(function(){
			$(this).parent().css({"background":"skyblue", "cursor":"pointer"});
		}).mouseout(function(){
			$(this).parent().css({"background":"white"})
		}).click(function(){
			console.log($(this).parent().children("input").val());
			var num = $(this).parent().children("input").val();
			
			location.href="<%=request.getContextPath()%>/views/admin/csInfoDetail.jsp";
			
			/*hidden 으로 서블릿에 보내줘야댐 구현할때!!!!!!!!  */
			<%-- location.href="<%=request.getContextPath()%>/selectOne.bo?num=" + num; --%>
			
			
			
		});
		
	});
		
	</script>
	
	
	
	
	

</body>
</html>