<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, declaration.model.vo.*"%>
<% 
	ArrayList<DeclarationList> plist = (ArrayList<DeclarationList>) request.getAttribute("plist");
	ArrayList<DeclarationList> clist = (ArrayList<DeclarationList>) request.getAttribute("clist");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 페이지</title>
<%@ include file="/views/common/import.html"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
	.outline{
		margin-right:auto;
		margin-left:auto;
		width:80%;
		height:800px;
		background:white;
		padding:50px;
	}
	.searchSection{
		padding-top:20px;
		padding-right:20%;
		padding-left:20%;
		padding-bottom:20px;
		border:1px solid lightgray;
		border-top:2px solid purple;
		text-align:center;
	}
	.tableSection{
		padding-top:20px;
	}
	
	.radioTable td{
		padding:10px;
		font-size:1.2em;
	}
	
	.buttonStyle {
		margin-right:5px !important;
		margin-left:5px !important;
	}
	
	#pLogTable td:last-of-type {
		width:180px;
	}
	
	#cLogTable td:last-of-type {
		width:180px;
	}
	
	h1{
		margin-left:11%;
	}
	
	.fields {
		padding-left:20%;
		margin-bottm:0px;
	}
	#companyArea {
		display:none;
	}
</style>
</head>
<body style="background:lightgray;">
	<%@ include file="../common/adminMenu.jsp" %>
	<h1>블랙리스트</h1>
	
	<div class="outline">
		<div class="searchSection" id="personArea">
			<form class="ui form" action="" method="get">
				<div class="radioArea" style="padding:10px; font-size:18px;">
					<div style="display:inline-block; width:100px; margin-left:50px; margin-right:50px;">
						<input type="radio" name="radioBtn" value="person" checked><label for="person">개인 회원</label>
					</div>
					<div style="display:inline-block; width:100px; margin-left:50px; margin-right:50px;">
						<input type="radio" name="radioBtn" value="company"><label for="company">기업 회원</label>
					</div>
				</div>
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown">
							<option>이름</option>
							<option>회원 ID</option>
							<option>이메일</option>
						</select>
					</div>
						<div class="ui icon input">
  						<input type="text" placeholder="Search..." disabled="disabled">
  						<i class="circular search link icon"></i>
					</div>
				</div>
				<table align="center" class="radioTable">
				
					<tr>
						<td colspan="5"><input type="submit" value="검색" class="ui purple button" style="width:150px;"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="searchSection" id="companyArea">
			<form class="ui form" action="" method="get">
				<div class="radioArea" style="padding:10px; font-size:18px;">
					<div style="display:inline-block; width:100px; margin-left:50px; margin-right:50px;">
						<input type="radio" name="radioBtn" value="person"><label for="person">개인 회원</label>
					</div>
					<div style="display:inline-block; width:100px; margin-left:50px; margin-right:50px;">
						<input type="radio" name="radioBtn" value="company" checked><label for="company">기업 회원</label>
					</div>
				</div>
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown">
							<option>회사명</option>
							<option>회원 ID</option>
							<option>사업자등록번호</option>
						</select>
					</div>
						<div class="ui icon input">
  						<input type="text" placeholder="Search..." disabled="disabled">
  						<i class="circular search link icon"></i>
					</div>
				</div>
				<table align="center" class="radioTable">
				
					<tr>
						<td colspan="5"><input type="submit" value="검색" class="ui purple button" style="width:150px;"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="tableSection" id="personTableArea">
			<table class="ui selectable celled table" id="pLogTable">
				<thead align="center">
					<tr>
						<th>번호</th>
						<th>구분</th>
						<th>이름</th>
						<th>이메일</th>
						<th>등록 날짜</th>
						<th>등록사유 / 등록 취소 사유</th>
					</tr>
				</thead>
				<tbody align="center">
					<% for(int i = 0; i < plist.size(); i++) { %>
					<tr>
						<td>
							<%= i + 1 %>
						</td>
						<td>
							<% if(plist.get(i).getCategory() == 1) { %>
								개인
							<% } %>
						</td>
						<td>
							<%= plist.get(i).getrName() %>
						</td>
						<td>
							<%= plist.get(i).getEmail() %>
						</td>
						<td>
							<%= plist.get(i).getDecDate() %>
						</td>
						<td>
							<%= plist.get(i).getContent() %>
						</td>
					</tr>
					<% } %>
				</tbody>
			</table>
		</div>
		<div class="tableSection" id="companyTableArea" style="display:none">
			<table class="ui selectable celled table" id="cLogTable">
				<thead align="center">
					<tr>
						<th>번호</th>
						<th>구분</th>
						<th>회사명</th>
						<th>사업자등록번호</th>
						<th>등록 날짜</th>
						<th>등록사유 / 등록 취소 사유</th>
					</tr>
				</thead>
				<tbody align="center">
					<% for(int j = 0; j < clist.size(); j++) { %>
						<tr>
							<td><%= j + 1 %></td>
							<td>
								<% if(clist.get(j).getCategory() == 2) { %>
									기업
								<% } %>
							</td>
							<td><%= clist.get(j).getrName() %></td>
							<td><%= clist.get(j).getcNum() %></td>
							<td><%= clist.get(j).getDecDate() %></td>
							<td><%= clist.get(j).getContent() %></td>
						</tr>
					<% } %>
				</tbody>
			</table>
		</div>
		<div id="btnArea" align="right" style="margin:10px; margin-top:30px;">
			<button id="plusBtn" value="plus" class="ui gray button">추가</button>
			<button id="deleteBtn" value="delete" class="ui gray button" >등록 취소</button>
		</div>
	</div>
	
	<div class="ui modal v1" style="width:280px; vertical-align:middle; padding:20px;">
		<i class="close icon" align="right"></i>
		<div class="description">
			<table style="margin:10px;">
				<tr>
					<td><label style="font-weight:bold;">구분</label></td>
				</tr>
				<tr>
					<td style="padding-bottom:20px;">
						<select style="width:220px; height:40px;" id="pc">
							<option>개인</option>
							<option>기업</option>
						</select>
						<br>
					</td>
				</tr>
				<tr>
					<td><label style="font-weight:bold;"> 회원 아이디  </label> <br></td>
				</tr>
				<tr>
					<td align="center" style="padding-bottom:20px;">
						<input type="text" name="name" id="plusId" style="border:none; width:220px; border-bottm-color:1px solid rgb(91, 191, 202); "> <br>
					</td>
				</tr>
				<tr>
					<td>
						<label style="font-weight:bold;" id="inCEmail"> 이메일 </label> 
						<label style="font-weight:bold; display:none;" id="inCNum"> 사업자 등록번호 </label>
					<br></td>
				</tr>
				<tr>
					<td align="center" style="padding-bottom:20px;">
						<input type="email" name="name" id="plusEmail" style="border:none; width:220px; border-bottm-color:1px solid rgb(91, 191, 202); "> <br>
					</td>
				</tr>
				<tr>
					<td><label style="font-weight:bold; color:rgb(91, 191, 202);">등록사유</label> <br></td>
				</tr>
				<tr>
					<td align="center"><input type="text" name="name" id="plusContent" style="width: 100%; height:60px; white-space:pre-line; border:none; border-bottm-color:2px solid rgba(91, 191, 202);"></td>
				</tr>
			</table>
			<div style="margin:10px; margin-left:auto; margin-right:auto;">
				<hr>
				<button id="plusSubmit" style="position:relative; width:40%; height:20px; font-size:15px; margin:10px; border:none; background:white; cursor:pointer; color:rgb(91, 191, 202)">등록하기</button>
				<button style="position:relative; width:40%; height:20px; font-size:15px; margin:10px; border:none; background:white; cursor:pointer;" class="close">취소하기</button>
			</div>
		</div>
	</div>
	
	<div class="ui modal v2" style="width:280px; vertical-align:middle; padding:20px;">
		<i class="close icon" align="right"></i>
		<div class="description">
			<table style="margin:10px;">
				<tr>
					<td><label style="font-weight:bold;">구분</label></td>
				</tr>
				<tr>
					<td style="padding-bottom:20px;">
						<select style="width:220px; height:40px;" id="pcD" disabled="disabled">
							<option value="person" >개인</option>
							<option value="company">기업</option>
						</select>
						<br>
					</td>
				</tr>
				<tr>
					<td>
						<label style="font-weight:bold;" id="personName"> 이름  </label> 
						<label style="font-weight:bold; display:none;" id="companyName"> 회사명 </label><br>
					</td>
				</tr>
				<tr>
					<td align="center" style="padding-bottom:20px;">
						<input type="text" id="PName" name="name" style="border:none; width:220px; border-bottm-color:1px solid rgb(91, 191, 202);" disabled="disabled"> 
						<input type="text" id="CName" name="name" style="border:none; width:220px; border-bottm-color:1px solid rgb(91, 191, 202); display:none;" disabled="disabled"> <br>
					</td>
				</tr>
				<tr>
					<td>
						<label style="font-weight:bold;" id="inDCEmail"> 이메일 </label> 
						<label style="font-weight:bold; display:none;" id="inDCNum"> 사업자 등록번호 </label><br>
					</td>
				</tr>
				<tr>
					<td align="center" style="padding-bottom:20px;">
						<input type="text" id="PID" name="name" style="border:none; width:220px; border-bottm-color:1px solid rgb(91, 191, 202);" disabled="disabled"> 
						<input type="text" id="CID" name="name" style="border:none; width:220px; border-bottm-color:1px solid rgb(91, 191, 202); display:none;" disabled="disabled"> <br>
					</td>
				</tr>
			</table>
			<div style="margin:10px; margin-left:auto; margin-right:auto;">
				<hr>
				<button id="delSubmit" style="position:relative; width:40%; height:20px; font-size:15px; margin:10px; border:none; background:white; cursor:pointer; color:rgb(91, 191, 202)">등록 취소</button>
				<button style="position:relative; width:40%; height:20px; font-size:15px; margin:10px; border:none; background:white; cursor:pointer;" class="close1">취소하기</button>
			</div>
		</div>
	</div>
	
	<script>
	var Type = null;
	var checkValue = null;
	var Name = null;
	
		$(function(){
			$("#cLogTable").tablesort();
			$("#pLogTable").tablesort();
			
			$("input[type=radio]").click(function(){
				$(this).prop("checked", true)
				
				if($(this).val() === 'company'){
					$("#personArea").hide();
					$("#companyArea").show();
					$("#personTableArea").hide();
					$("#companyTableArea").show();
				} else{
					$("#personArea").show();
					$("#companyArea").hide();
					$("#personTableArea").show();
					$("#companyTableArea").hide();
				}
			
			});
			
			$("#plusBtn").click(function(){
				$('.ui.modal.v1').modal('show');
			});
	
			$("#plusSubmit").click(function(){
				var plusContent = $("#plusContent").val();
				var plusId = $("#plusId").val();
				var email = $("#plusEmail").val();
				var pcCheck = $("#pc option:selected").text();
				
				$.ajax({
					url : "/info/checkPCId.apc",
					type : "post",
					
					data : {
						EmailorNum : email,
						id : plusId,
						category : pcCheck
					},
					succes : function(data) {
						
					},
					error : function() {
						console.log("실패");
					}
				});
				if(pcCheck === "기업") {
						if(plusContent != null && plusId != null) {
							if(email != null) {
								location.href="<%=request.getContextPath()%>/blackSubmit.ppb?plusContent="+plusContent+"&plusId="+plusId+"&email="+email; 
							}else {
								alert("모두 작성한 후 등록가능합니다");
							}
						}else {
							alert("모두 작성한 후 등록가능합니다");
						}
				}
			});
				
			$("#deleteBtn").click(function(){
				if(Type == "개인") {
					$("#pcD>option[value=person]").prop("selected", true);
					$("#inDCNum").hide();
					$("#inDCEmail").show();
					$("#companyName").hide();
					$("#personName").show();
				}else if(Type == "기업"){
					$("#pcD>option[value=company]").prop("selected", true);
					$("#inDCNum").show();
					$("#inDCEmail").hide();
					$("#personName").hide();
					$("#companyName").show();
				}
				$("#PID").val(checkValue);
				$("#CID").val(checkValue);
				$("#PName").val(Name);
				$("#CName").val(Name);
				$('.ui.modal.v2').modal('show');
			});
			
				$("#pc").change(function(){
					var pcCheck = $("#pc option:selected").text(); 
					
					if(pcCheck === "개인") {
						$("#inCNum").hide();
						$("#inCEmail").show();
						
					}else if(pcCheck === "기업") {
						$("#inCEmail").hide();
						$("#inCNum").show();
					}
				});
				
				$("#pcD").change(function(){
					var pcCheck = $("#pc option:selected").text(); 
					
					if(pcCheck === "개인") {
						$("#inDCNum").hide();
						$("#inDCEmail").show();
						$("#PID").show();
						$("#CID").hide();
					}else if(pcCheck === "기업") {
						$("#inDCEmail").hide();
						$("#inDCNum").show();
						$("#PID").hide();
						$("#CID").show();
					}
				});
			
			$("#delSubmit").click(function(){
				var delEmail = $("#PID").val();
				var delCNum = $("#CID").val();
				
				if(delEmail != null || delCNum != null) {
					location.href="<%= request.getContextPath() %>/cancelBlack.acb?delEmail="+delEmail+"&delCNum="+delCNum;
				}
			});
			
			$(".close").click(function(){
				$('.ui.modal.v1').modal('hide');
			});
			
			$(".close1").click(function(){
				$('.ui.modal.v2').modal('hide');
			});
			
			$("#pLogTable tr").click(function(){
				var tdArr = new Array();
				var td = $(this).children();
				
				td.each(function(i) {
					tdArr.push(td.eq(i).text());
				});
				var type = td.eq(1).text();
				var name = td.eq(2).text();
				var email = td.eq(3).text();
				email = $.trim(email);
				type = $.trim(type);
				name = $.trim(name);
				Type = type;
				Name = name;
				checkValue = email;
			});
			
			$("#cLogTable tr").click(function(){
				var tdArr = new Array();
				var td = $(this).children();
				
				td.each(function(i) {
					tdArr.push(td.eq(i).text());
				});
				var type = td.eq(1).text();
				var name = td.eq(2).text();
				var cNum = td.eq(3).text();
				type = $.trim(type);
				cNum = $.trim(cNum);
				name = $.trim(name);
				Type = type;
				Name = name;
				checkValue = cNum;
			});
		});
	
	</script>
</body>
</html>