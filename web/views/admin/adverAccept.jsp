<%@page import="admin.model.vo.AdverAccept"%>
<%@page import="java.util.ArrayList, keyword.model.vo.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	ArrayList<AdverAccept> list = (ArrayList<AdverAccept>) request.getAttribute("list");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/views/common/import.html"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<title>관리자페이지</title>
<style>
.outline {
	margin-right: auto;
	margin-left: auto;
	width: 80%;
	height: 800px;
	background: white;
	padding: 50px;
}

.searchSection {
	padding-top: 20px;
	padding-right: 20%;
	padding-left: 20%;
	padding-bottom: 20px;
	border: 1px solid lightgray;
	border-top: 2px solid purple;
	text-align: center;
}

.tableSection {
	padding-top: 20px;
}

.radioTable td {
	padding: 10px;
	font-size: 1.2em;
}

.buttonStyle {
	margin-right: 5px !important;
	margin-left: 5px !important;
}

#logTable td:last-of-type {
	width: 180px;
}

h1 {
	margin-left: 11%;
}

.fields {
	padding-left: 20%;
}

.radioTable td {
	padding: 10px;
	margin-bottom: 10px;
	font-size: 1.2em;
}

#bluebtn {
	width: 50px;
	height: 30px;
	background: blue;
	color: white;
	font-weight: bold;
	display: inline-block;
	padding-top: 5px;
	opacity: 0.5;
}

#redbtn {
	width: 50px;
	height: 30px;
	background: red;
	color: white;
	font-weight: bold;
	display: inline-block;
	padding-top: 5px;
	opacity: 0.5;
}

.modalOuter {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
}

.pagingArea {
	text-align: center;
	padding: 20px;
	margin-bottom: 50px;
	margin-top: 30px;
}

.pagingbtn {
	height: 30px;
	width: 30px;
	display: inline-block;
	vertical-align: top;
	overflow: hidden;
	background: white;
	border: 1px solid lightgray;
	border-radius: 5px;
}

.pagingbtn:hover {
	cursor: pointer;
}

.pagingbtn:disabled {
	color: gray;
}
</style>
</head>
<body>
<body style="background: lightgray;">
	<%@ include file="../common/adminMenu.jsp"%>
	<h1>광고 배너 승인</h1>
	<div class="outline">
		<div class="searchSection">
		<div class="ui form">
			
			<br>
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown" id = "searchCategory">
							<option value="1">기업이름</option>
							<option value="2">요청광고배너</option>
							<option value="3">구분</option>
						</select>
					</div>
					<div class="ten wide field">
						<input type="text" id="searchText" disabled="disabled">
						<select class="ui fluid dropdown" name="scategory" id="scategory" style="display:none;">
								<option value="1">요청</option>
								<option value="2">승인</option>
								<option value="3">환불</option>
						</select>
					</div>
				</div>
				<input type="submit" value="검색" class="ui purple button" style="width: 150px;">
				</div>
		</div>
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead align="center">
					<tr align="center">
						<th>번호</th>
						<th>결제일자</th>
						<th>광고기간</th>
						<th>기업이름</th>
						<th>요청광고배너</th>
						<th>결제금액</th>
						<th>파일보기</th>
						<th>보기</th>
					</tr>
				</thead>
				<tbody align="center">
					<%for(int i=0; i<list.size(); i++){ %>
					<tr align="center">
						<td><%=i+1%></td>
						<td><%=list.get(i).getPDate()%></td>
						<td><%=list.get(i).getSDate() %> ~ <%=list.get(i).getEDate() %></td>
						<td><%=list.get(i).getCName()%></td>
						<td><%=list.get(i).getPName() %></td>
						<td><%=list.get(i).getPMoney() %> 원</td>
						<td>
							<button  id="clickFile" class="ui primary basic button clickFile" style="cursor: pointer;">File</button>
							<input type="hidden" value="<%=list.get(i).getPayid()%>">
						</td>
						<%if(list.get(i).getPhCategory() == 1) {%>
							<!-- 요청  , 승인하시겠습니까? modal 확인 -> 결제 상세이력 승인insert //결제 id 필요-->
							<!-- 요청  , 거절하시겠습니까? modal 확인 -> 결제 상세이력  환불insert //결제 id 필요-->
							<td>
								<button onclick="yes(<%=list.get(i).getPayid()%>)"class="ui primary basic button"  style="cursor: pointer;">Y</button>
								<button onclick="no(<%=list.get(i).getPayid()%>);" class="ui negative basic button" style="cursor: pointer;">N</button>
								<input type="hidden" value="<%=list.get(i).getPayid()%>">
							</td>
						<%}else if(list.get(i).getPhCategory()==2){ %>
							<!-- 승인 날짜 , 수정 버튼 modal-->
							<td>
								<button  class="ui primary basic button checkReason"  style="cursor: pointer;">Y</button>
								<input type="hidden" value="<%=list.get(i).getPayid()%>">
							</td>
						<%}else {%>
							<!-- 거절한 이유 확인 modal -->
							<td>
								<button class="ui negative basic button noReason" style="cursor: pointer;">N</button>
								<input type="hidden" value="<%=list.get(i).getPayid()%>">
							</td>
						<%} %>
					</tr>
					<%} %>
				</tbody>
			</table>
		</div>
		<!-- 페이징시작 -->
			<div class="pagingArea" id ="pagingArea">
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectAdverAcc?currentPage=1'"><<</button>
			<%if(currentPage <=1) {%>
				<button class="pagingbtn" disabled></button>
			<%} else{ %>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectAdverAcc?currentPage=<%=currentPage-1%>'"><</button>
			<%} %>
			
			<%for(int p=startPage; p<=endPage; p++){
				if(p == currentPage){ %>
					<button class="pagingbtn" disabled><%=p %></button>
				<%} else{ %>
					<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectAdverAcc?currentPage=<%=p%>'"><%=p %></button>
				<%}
			  } %>
			
			<%if(currentPage >= maxPage){ %>
				<button class="pagingbtn" disabled></button>
			<%} else {%>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectAdverAcc?currentPage=<%=currentPage+1%>'">></button>
			<%} %>
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/selectAdverAcc?currentPage=<%=maxPage%>'">>></button>
		</div>
		<!-- 페이징끝 -->
		</div><!-- 사진 모달 start -->
			 <div class="ui basic modal" id="introModal">
				<div class="modalOuter">
					<div class="buttonArea">
						<div id="ibtn" onclick="$('#introModal').modal('hide')" style="float:right; cursor: pointer;">X</div>
					</div>
				<div class="bigPhotoArea">
					<img id="modalImg" style="width:300px;" src="">
				</div>
			</div>
		</div> <!-- 사진 띄우는 모달 끝 -->
		
		
		
		<!-- 승인/거절 중 YES 선택 -->
		<div class="mini ui modal yes">
	  		<div class="header" style="text-align: center;">해당 광고를 승인하시겠습니까?</div>
	  		<div class="content">
	  			<div class="actions" style="text-align: center;">
	  			<input id = "yes" type="hidden" style="display: none">
				    <div class="ui black deny button">
				      	No
				    </div>
				    <div class="ui positive right labeled icon button YesAdver">
				      	Yes
				      <i class="checkmark icon"></i>
				    </div>
				  </div>
	 		</div>
		</div>
		<!-- 승인/거절 중 NO 선택 -->
		<div class="mini ui modal no">
	  		<div class="header" style="text-align: center;">해당 광고를 거절하시겠습니까?</div>
	  		<div class="content">
	  			<div class="actions" style="text-align: center;">
	  			<ul style="text-align: left; margin-left: 15px;">
	  				<li>거절 사유</li>
	  			</ul>
	  			<div><textarea cols ="30" rows ="10" id ="noTextArea"></textarea></div>
	  			<br>
				<br>
				    <div class="ui black deny button">
				      	No
				    </div>
				    <div class="ui positive right labeled icon button NoAdver">
				      	Yes
				      <i class="checkmark icon"></i>
				    </div>
				  </div>
	 		</div>
		</div>
		
		<!-- check모달 -->
		<div class="mini ui modal check" id="checkModal">
	  		<div class="header" style="text-align: center;">승인 광고 확인</div>
	  		<div class="content">
	  			<div class="actions" style="text-align: center;">
	  			<h4 class="ui secondary header">광고 img</h4>
	  			<img id="checkImg" style="width:200px;" src="">
	  			<br><br><br>
				    <div class="ui black deny button">
				      	확인
				    </div>
				    <div class="ui positive right labeled icon button checkAdver">
				      	수정
				      <i class="checkmark icon"></i>
				    </div>
				  </div>
	 		</div>
		</div>
		<!-- 환불 이유modal -->
		<div class="mini ui modal noReasonModal" id = "noReasonModal">
	  		<div class="header" style="text-align: center;">거절 사유 확인</div>
	  		<div class="content">
	  			<div class="actions" style="text-align: center;">
	  			<div><textarea cols ="30" rows ="3" id ="noReasonArea" readonly="readonly"></textarea></div>
	  			<br>
				    <div class="ui positive right labeled icon button noReasonAdver">
				      	확인
				      <i class="checkmark icon"></i>
				    </div>
				  </div>
	 		</div>
		</div>
		
		
		
		
		
	<script>
		var Payid;
		/* 광고 배너 승인 */
		function yes(payid){
			Payid=payid;
			$('.mini.modal.yes')
			  .modal('show');
		}
		$(".YesAdver").click(function() {
			//console.log("PayId : " +Payid);
			location.href = "<%=request.getContextPath()%>/insertYesAdver?payid="+Payid;
		});
		
		/* 광고 배너 거절 */
		function no(payid){
			Payid=payid;
			$('.mini.modal.no')
			  .modal('show');
		}
		$(".NoAdver").click(function() {
			//console.log("PayId : " +Payid);
			var text = $("#noTextArea").val();
			if(text == ""){
				alert("거절사유를 입력해주세요")
			}else{
				console.log(text);
				location.href = "<%=request.getContextPath()%>/insertNoAdver?payid="+Payid+"&text="+text;
			}
		});
		
		
		/* 광고배너 체크 */
		
		$(".checkReason").click(function(){
			var payid = $(this).parent().find("input[type=hidden]").val();
			var result = {
					payid : payid
			}
			Payid=payid;
			$.ajax({
				url : "pictureShow",
				data : result,
				type : "POST",
				success : function(data) {
					$("#checkImg").attr("src", data);
					$("#checkModal").modal('show');
				}
			})
		})
		/* 수정버튼 클릭 */
		$(".checkAdver").click(function(){
			console.log(Payid);
			location.href = "<%=request.getContextPath()%>/updateAdverImg?payid="+Payid;
		})
		
		
		$(".noReason").click(function(){
			var payid = $(this).parent().find("input[type=hidden]").val();
			var result = {
					payid : payid
			}
			Payid = payid;
			$.ajax({
				url : "selectNoAdverReason",
				data : result,
				type : "POST",
				success : function(data) {
					console.log(data);
					$("#noReasonArea").val(data);
					$("#noReasonModal").modal('show');
				}
				,error:function(request,status,error){
			        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			    }
			})
		})
		
		
		
		
		
		
		
		//fileButton 클릭시 사진 보여주기
		$(".clickFile").click(function() {
			var payid = $(this).parent().find("input[type=hidden]").val();
			var result = {
				payid : payid
			}
			$.ajax({
				url : "pictureShow",
				data : result,
				type : "POST",
				success : function(data) {
					$("#modalImg").attr("src", data);
					$("#introModal").modal('show');
				}
			})
		});
		
	
		
		
		/* TableSort && category css */
		$(function() {
			$("#logTable").tablesort();

			$("#searchCategory").change(function() {
				console.log($(this).val())
				if ($(this).val() == 3) {
					$("#scategory").css("display", "inline");
					$("#searchText").css("display", "none");
				} else {
					$("#scategory").css("display", "none");
					$("#searchText").css("display", "inline");

				}
			});
		});
	</script>
</body>
</html>





