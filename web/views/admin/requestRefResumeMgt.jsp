<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, person.model.vo.*"%>
<%
	ArrayList<RecommendResume> list = (ArrayList<RecommendResume>) request.getAttribute("list");
	//ArrayList<RecommendResume> detailList = (ArrayList<RecommendResume>) request.getAttribute("detailList");
	/* ArrayList<Member> mlist = null;
	for(int i = 0; i < list.size(); i++) {
		mlist = list.get(i).getMemberList();
	} */
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 페이지</title>
<%@ include file="/views/common/import.html" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
	.outline{
		margin-right:auto;
		margin-left:auto;
		width:80%;
		height:800px;
		background:white;
		padding:50px;
	}
	.searchSection{
		padding-top:20px;
		padding-right:20%;
		padding-left:20%;
		padding-bottom:20px;
		border:1px solid lightgray;
		border-top:2px solid purple;
		text-align:center;
	}
	.tableSection{
		padding-top:20px;
	}
	
	.buttonStyle {
		margin-right:5px !important;
		margin-left:5px !important;
	}
	
	h1{
		margin-left:11%;
	}
	
	.modalOuter{
		margin-left:auto;
		margin-right:auto;
		width:500px;
		height:220px;
		background:white;
		color:black;
		padding:20px;
		text-align:center;
	}
	
	textarea{
		border:1px solid lightgray;
		border-radius:6px;
		margin:15px;
	}
</style>
</head>
<body style="background:lightgray;">
	<%@ include file="../common/adminMenu.jsp" %>
	<h1>추천이력서 신청</h1>
	
	<div class="outline">
		<div class="searchSection">
			<form class="ui form" action="" method="get">
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown">
							<option>이름</option>
							<option>회원아이디</option>
							<option>신청날짜</option>
						</select>
					</div>
					<div class="ten wide field">
						<div class="ui disabled icon input"><i class="search icon"></i>
 							 <input type="text" placeholder="Search..."></div>
					</div>
				</div>
				<input type="submit" value="검색" class="ui purple button" style="width:150px;">
			</form>
		</div>
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead>
					<tr align="center">
						<th>신청날짜</th>
						<th>신청분기</th>
						<th>회원번호</th>
						<th>회원아이디</th>
						<th>회원이름</th>
						<th>이력서조회</th>
						<th>신청/반려</th>
					</tr>
				</thead>
				<tbody>
				<%if(list != null){ 
					for(int i=0; i<list.size(); i++){ %>
					<tr align="center">
						<td><%=list.get(i).getRefhidDate() %></td>
						<td><%=list.get(i).getQuarter() %></td>
						<td><%=list.get(i).getMember().getUno() %></td>
						<td><%=list.get(i).getMember().getUserId() %></td>
						<td><%=list.get(i).getMember().getUserName() %></td>
						<td><button class="ui button mini seeResume"style="width:80px;"><i class="address card icon"></i>조회</button>
							<input type="hidden" value="<%=list.get(i).getMember().getUno() %>">
						</td>
						<td><button class="ui blue button mini confirm">승인</button><button class="ui black button mini deny">반려</button>
							<input type="hidden" value="<%=list.get(i).getRefId()%>">
						</td>
					</tr>
				<%}} %>
				</tbody>
			</table>
		</div>
	</div>
	<%-- 모달 영역! --%>
	<div class="ui basic modal" id="deniModal">
		<div class="modalOuter">
			<h4>반려사유를 작성해주세요.</h4>
			<hr>
			<form action="<%=request.getContextPath() %>/updateRefCom.rr" method="post" id="deniForm">
				<textarea cols="60" rows="5" style="resize:none;" name="content"></textarea>
				<input type="hidden" id="selectRefId" name="selectRefId">
				<div class="buttonArea">
					<button type="submit" id="ok" class="ui purple button mini">제출</button>
					<button type="reset" id="cancel" class="ui button mini">취소</button>
				</div>
			</form>
		</div>
	</div> <!-- 신청반려 모달 끝 -->

	<script>
	var uno = 0;
	
		$(function(){
			$(".confirm").click(function(){
				
				swal({
					  title: "추천이력서 등록",
					  text: "추천이력서 신청을 승인하시겠습니까?",
					  icon: "warning",
					  buttons: true,
					  dangerMode: true,
					})
					.then((value) => {
					  if (value) {
						 var num = $(this).parent().find("input[type=hidden]").val();
						location.href = "<%=request.getContextPath()%>/update.ref?refid=" + num;
					  } else {
					    swal("승인을 보류하였습니다.");
					  }
					});
			});
			$(".deny").click(function(){
				var num = $(this).parent().find("input[type=hidden]").val();
				$("#selectRefId").val(num);
				$("#deniModal").modal('show');
			});
			$("#cancel").click(function(){
				$("#deniModal").modal('hide');
			});
			
			$(".seeResume").click(function(){
				var num = $(this).parent().find("input[type=hidden]").val();
				window.open('<%=request.getContextPath()%>/selectOne.mre?num=' +num, "이력서", "location=0, resizable=no, menubar=no, status=no, toolbar=no");
				
				<%-- location.href="<%=request.getContextPath()%>/selectOne.mre?num=" +num; --%>

			});
			
			
			
			$("#logTable").tablesort();
			
			
		});
	</script>
</body>
</html>





