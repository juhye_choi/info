<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 페이지</title>
<%@ include file="/views/common/import.html"%>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
.outline {
	margin-right: auto;
	margin-left: auto;
	width: 80%;
	height: 800px;
	background: white;
	padding: 50px;
}

.searchSection {
	padding-top: 20px;
	padding-right: 20%;
	padding-left: 20%;
	padding-bottom: 20px;
	border: 1px solid lightgray;
	border-top: 2px solid purple;
	text-align: center;
}

.tableSection {
	padding-top: 20px;
}

.radioTable td {
	padding: 10px;
	font-size: 1.2em;
}
	
.radioTable td{
	padding:10px;
	font-size:1.2em;
}

.buttonStyle {
	margin-right: 5px !important;
	margin-left: 5px !important;
}

#logTable td:last-of-type {
	width: 180px;
}

h1 {
	margin-left: 11%;
}

.fields {
	padding-left: 20%;
}
#contentDetail:hover {
	cursor:pointer;
}
.AllContent, .AnyContent {
	cursor:pointer;
	border: none;
	margin:15px;
	background:white;
	font-size: 15px;
	text-decoration: underline;
}
</style>
</head>
<body style="background: lightgray;">
	<%@ include file="../common/adminMenu.jsp"%>
	<h1>기업 신고</h1>

	<div class="outline">
		<div class="searchSection">
			<form class="ui form" action="" method="get">
				<div class="radioArea" style="padding:10px; font-size:18px;">
					<div style="display:inline-block; width:100px; margin-left:50px; margin-right:50px;">
						<input type="radio" id="searchListAny" name="radioBtn" value="AnyContent" checked><label for="searchListAny">일부 보기</label>
					</div>
					<div style="display:inline-block; width:100px; margin-left:50px; margin-right:50px;">
						<input type="radio" id="searchListAll" name="radioBtn" value="AllContent"><label for="searchListAll">모두 보기</label>
					</div>
				</div>
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown">
							<option>일자</option>
							<option>신고된 기업 ID</option>
							<option>접수자 ID</option>
							<option>처리여부</option>
						</select>
					</div>
					<div class="ui icon input">
						<input type="text" placeholder="Search..."> <i
							class="circular search link icon"></i>
					</div>
				</div>
				<table align="center" class="radioTable">

					<tr>
						<td colspan="5"><input type="submit" value="검색"
							class="ui purple button" style="width: 150px;"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead align="center">
					<tr>
						<th>번호</th>
						<th>일자</th>
						<th>신고된 기업 ID</th>
						<th>접수자 ID</th>
						<th>접수자 Email</th>
						<th>신고 내역</th>
						<th>처리 내역</th>
					</tr>
				</thead>
				<tbody align="center">
					<tr>
						<td>1</td>
						<td>2018.09.01</td>
						<td>(주)뀨화니</td>
						<td>hwan123</td>
						<td>wan@gmail.com</td>
						<td><p id="contentDetail" style="text-decoration:underline" onclick="modal2();">내용보기</p></td>
						<td><button class="ui red button mini buttonStyle" onclick="modal();">처리하기</button></td>
					</tr>
					<tr>
						<td>2</td>
						<td>2018.09.01</td>
						<td>(주)연두</td>
						<td>yg123</td>
						<td>yh@naver.com</td>
						<td><p id="contentDetail" style="text-decoration:underline" onclick="modal2();">내용보기</p></td>
						<td><button class="ui red button mini buttonStyle" onclick="modal();">처리하기</button></td>
					</tr>
					<tr>
						<td>3</td>
						<td>2017.04.30</td>
						<td>(주)도후훙</td>
						<td>dohuhung1</td>
						<td>huhung@gmail.com</td>
						<td><p id="contentDetail" style="text-decoration:underline" onclick="modal2();">내용보기</p></td>
						<td><button class="ui button mini buttonStyle">처리완료</button></td>
					</tr>
					<tr class="hidden" style="display:none">
						<td>3</td>
						<td>2017.04.27</td>
						<td>(주)도후훙</td>
						<td>dohuhung1</td>
						<td>huhung@gmail.com</td>
						<td><p id="contentDetail" style="text-decoration:underline">내용보기</p></td>
						<td><button class="ui button mini buttonStyle">처리하기</button></td>
					</tr>
					<tr>
						<td>4</td>
						<td>2019.11.03</td>
						<td>(주)쭈헤이</td>
						<td>jjuhey</td>
						<td>juhey @gmail.com</td>
						<td><p id="contentDetail" style="text-decoration:underline" onclick="modal2();">내용보기</p></td>
						<td><button class="ui button mini buttonStyle">처리완료</button></td>
					</tr>
					<tr class="hidden" style="display:none">
						<td>4</td>
						<td>2019.11.01</td>
						<td>(주)쭈헤이</td>
						<td>jjuhey</td>
						<td>juhey @gmail.com</td>
						<td><p id="contentDetail" style="text-decoration:underline;">내용보기</p></td>
						<td><button class="ui button mini buttonStyle">처리하기</button></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="ui modal v1" style="width:350px; height:170px; text-align:center; vertical-align:middle; padding:20px;">
		<i class="close icon"></i>
		<div class="description">
			<div align="center">
				<h3>신고 내용에 대해</h3>
				<h3>경고 처리를 하시겠습니까?</h3>
			</div><br>
			<div align="center">
				<button style="width:140px; height:40px; background:red; color:white; font-size:15px; border:none;">Yes</button>
				<button style="width:140px; height:40px; background:blue; color:white; font-size:15px; border:none;" class="close1">No</button>
			</div>
		</div>
	</div>
	
	<div class="ui modal v2" style="width:350px; height:170px; text-align:center; vertical-align:middle; padding:20px;">
		<i class="close icon"></i>
		<div class="description">
			<div align="center">
				<h3>신고 접수 내용 : </h3>
				<h3>연봉 정보가 허위입니다.</h3>
			</div><br>
			<div align="center">
				<button style="width:280px; height:40px; background:blue; color:white; font-size:15px; border:none;" class="close2">OK</button>
			</div>
		</div>
	</div>
	<script>
		$(function(){
			$("#logTable").tablesort();
			
			$(".close1").click(function(){
				$('.ui.modal.v1')
				  .modal('hide');
			});
			
			$(".close2").click(function(){
				$('.ui.modal.v2')
				  .modal('hide');
			});
			
			$("input[type=radio]").click(function(){
				
				if($("input[name=radioBtn]:checked").val() === 'AllContent'){
					$('.hidden').show();
					$('.AnyContent').show();
					$('.AllContent').hide();
				}else {
					$('.hidden').hide();
					$('.AllContent').show();
					$('.AnyContent').hide();
				}
			});
		});
		
		function modal() {
			$('.ui.modal.v1')
			  .modal('show');
		}
		
		function modal2() {
			$('.ui.modal.v2')
			  .modal('show');
		}
	</script>
</body>
</html>