<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, resume.model.vo.*, person.model.vo.*, member.model.vo.Member"%>
<%		
	Member loginUser =  (Member) session.getAttribute("loginUser"); 
	
		if(loginUser == null || !loginUser.getCategory().equals("A")){
			request.setAttribute("msg", "관리자가 아니시네요?!");
			request.getRequestDispatcher("/views/common/needLogin.jsp").forward(request, response);
		}
		Resume resume = (Resume) request.getAttribute("resume");
		HashMap<String, String> personInfo = (HashMap<String, String>) request.getAttribute("personInfo");
		ArrayList<Attachment2> atlist = (ArrayList<Attachment2>) request.getAttribute("atlist");
		String changeName = "";
		if(atlist.size() != 0){
			changeName = atlist.get(0).getChangeName();			
		}
		
		ArrayList<Education> elist = resume.getElist();
		ArrayList<School> slist = resume.getSlist();
		ArrayList<Certification> celist = resume.getCelist();
		ArrayList<Career> calist = resume.getCalist();
		ArrayList<ProjectCareer> plist = resume.getPlist();
		ArrayList<HashMap<String, String>> khlist = resume.getKhlist();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>이력서</title>
<style>
	.first{
		margin-right:auto;
		margin-left:auto;
		width:70%;
		padding-bottom:30px;
	}
	
	.space{
		height:180px;
		background:white;
	}
	.basicInfo{
		height:180px;
		width:70%;
		margin-right:auto;
		margin-left:auto;
		border-bottom:1px solid lightgray;
		padding:10px;
		padding-bottom:5px;
		position:fixed;
		background:white;
		z-index:80;
		
	}
	.basic1>img{
		width: 160px; height: 160px;
    	object-fit: cover;
   		object-position: top;
    	border-radius: 50%;
    	
	}
	.basic1{
		width:25%;
		height:100%;
		display:inline-block;
		text-align:right;
		overflow:hidden;
		
	}
	.basic23{
		display:inline-block;
		overflow:hidden;
		width:70%;
		height:100%;
	}
	.basic2{
		width:100%;
		height:60%;
		padding:10px;
	}
	.basic3{
		width:100%;
		height:30%;
		vertical-align:top;
		text-align:right;
	}
	.basic2>table{
		width:100%; text-align:center; margin-top:15px;	
	}
	.name{
		font-size:2.5em;
		font-weight:bold;
		margin:10px;
	}
	.sentence{
		color:gray;
		margin:10px;
	}
	.sentence:hover{
		cursor:pointer;
		text-decoration:underline;
	}
	.basicTable{
		width:600px;
	}
	.detailInfo{
		/* height:600px; */
		margin:10px;
		overflow:hidden;
		
	}
	.eduInfo{
		width:20%;
		height:100%;
		padding:10px;
		display:inline-block;
		background:#e7e6e1;
		overflow:hidden;
		vertical-align:top;
	}
	
	.projectInfo{
		width:65%;
		height:100%;
		padding:10px;
		display:inline-block;
		/* border:1px solid gray; */
		overflow:hidden;
		vertical-align:top;
	}
	.keywordInfo{
		width:14%;
		height:360px;
		padding:10px;
		display:inline-block;
		border:1px solid #e7e6e1;
		text-align:center;
		overflow:hidden;
		vertical-align:top;
		border-radius:10px;
	}
	.keywordArea{
		height:30px;
		margin-left:10px;
	}
	.keyword{
		/* display:inline-block; */
		border:1px solid purple;
		border-radius:5px;
		color:purple;
		width:90%;
	}
	.keywordCnt{
		/* display:inline-block; */
		border-radius:10px;
		background:#e75a5a;
		width:18px;
		color:white;
		position:relative;
		top:11px;
		left:80%;
	}
	
	h4 {
		color:gray;
	}
	
	.leftDetail{
		margin:10px;
		margin-bottom:30px;
	}
	
	@media screen and (max-width: 768px){
		.first{
			width:100%;
		}
		.detailInfo{
			height:1500px;
		}
		.projectInfo{
			display:block;
			width:100%;
			height:40%;
		}
		.eduInfo{
			display:block;
			width:100%;
			height:30%;
			overflow:scroll;
		}
		.eduInfo::-webkit-scrollbar{
			display:none;
		}
		.keywordInfo{
			display:block;
			width:100%;
			height:20%;
			overflow:scroll;
		}
		.keywordInfo::-webkit-scrollbar{
			display:none;
		}
		
		.sentence{
			display:none;
		}
		.name{
			font-size:2em;
		}
		.basic2>table{
			width:100%; text-align:center; margin-top:0;
		}
		.basic2 td:last-of-type{
			display:none;
		}
		#transPDF{
			display:none;
		}
	}
	
	.editBtn{
		float:right;
		background:#e7e6e1;
		border:1px solid #e7e6e1;
		cursor:pointer;
		border-radius:5px;
		width:30px;
	
	}
	.editBtn:hover{
		background-color:lightgray;
	}
	
	.pjtDetail{
		width:95%;
		height:100px;
		border:1px solid #e7e6e1;
		border-radius:10px;
		margin:20px;
		padding:5px;
	}
	.pjtAdd{
		width:95%;
		height:40px;
		border:1px solid #e7e6e1;
		border-radius:10px;
		margin:20px;
		text-align:center;
		padding-top:8px;
		color:white;
		background: lightgray;
	}
	
	.pjtDetail:hover, .pjtAdd:hover{ cursor:pointer; box-shadow: 3px 3px 20px #dddddd; }
	.date{ font-size:.8em; padding-left:10px; }
	.title{ font-weight:bold; }
	.explain{ font-size:.9em; color:gray; padding-left:10px; margin-bottom:10px; }
	
	.modalOuter{
		margin-left:auto;
		margin-right:auto;
		width:500px;
		height:200px;
		background:white;
		color:black;
		padding:20px;
		text-align:center;
	}
	
	.modalOuter2{
		margin-left:auto;
		margin-right:auto;
		width:500px;
		height:320px;
		background:white;
		color:black;
		padding:20px;
		text-align:center;
	}
	textarea{
		border:1px solid lightgray;
		border-radius:6px;
	}
	.buttonArea{
		padding:10px;
	}
	.pdfTable{
		width:400px;
		height:200px;
	}
	.pintro{
		display:inline-block;
		overflow:hidden;
		vertical-align:top;
	}
	.ptitle{
		padding-left:2px;
		font-size:1.2em;
		font-weight:bold;

	}
	.pdate{
		color:gray;
		padding:5px;

	}
	.pcontent{
		color:lightgray;
		padding-left:5px;
		overflow:hidden;
	}
	.pimg{
		width:88px;
		height:88px;
		border-radius:5px;
		border:1px solid gray;
		display:inline-block;
		overflow:hidden;
	}
	
</style>
<%@ include file="/views/common/import.html" %>
</head>
<body style="background:lightgray;">
	<%-- <%@ include file="../common/adminMenu.jsp" %> --%>
	<div class="first" style="background:white;">
		<div class="basicInfo">
			<div class="basic1" id="photo">
				<% if(changeName.equals("")) { %>
					<img src="<%=request.getContextPath()%>/images/profilePhoto.jpg">
				<% }else { %>
					<img src="<%= request.getContextPath()%>/thumbnail_uploadFiles/<%= changeName%>">
				<% } %>
			</div>
			<div class="basic23">
				<div class="basic3" id="buttonSection"></div>
				<div class="basic2" id="contact">
					<span class="name" id="name"><%=personInfo.get("name") %></span>
					<span class="sentence" id="title"></span>
					<table class="basicTable">
						<tr>
							<td><i class="icon mail"></i><span id="emailAddress"><%=personInfo.get("email")%></span></td>
							<td><i class="icon mobile alternate"></i><span id="phoneNumber"><%=personInfo.get("phone") %></span></td>
							<td><i class="icon bookmark"></i><span id="status"><%=personInfo.get("jobStatus")%></span></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="space"></div>
		<div class="detailInfo">
			<div class="eduInfo">
				<div class="leftDetail">
					<h4>자기소개</h4>
					<%if(resume.getrIntro() != null){ %>
					<div><%=resume.getrIntro() %></div>
					<%} else{ %>
					<div>-</div>
					<%} %>
				</div>
				<div class="leftDetail">
						<h4>경력</h4>
					<%if(calist != null){
						for(Career c : calist){%>
							<div>
								<input type="hidden" name="career" value="<%=c.getCarId()%>" />
								<div id="title" class="title mtitle"><%=c.getCarName() %></div>
								<%if(c.getCarFinishDate() != null){ %>
									<div id="date" class="date"><%=c.getCarStartDate() %> ~ <%=c.getCarFinishDate()%></div>
								<%} else{ %>
									<div id="date" class="date"><%=c.getCarStartDate() %> ~ now</div>
								<%} 
								if(c.getCarContent() != null){ %>
									<div id="explain" class="explain"><%=c.getCarContent() %></div>
								<%} else{%>
									<div id="explain" class="explain">-</div>
								<%} %>
							</div>
					<%}} %>
				</div>
				<div class="leftDetail">
						<h4>학력</h4>
					<%if(slist != null){
						for(School s : slist){ %>
						<div>
							<input type="hidden" name="school" value="<%=s.getSid()%>" />
							<div id="title" class="title stitle"><%=s.getsName() %></div>
							<%if(s.getGraduDate() != null){ %>
								<div id="date" class="date"><%=s.getEnterDate() %> ~ <%=s.getGraduDate() %></div>
							<%} else{ %>
								<div id="date" class="date"><%=s.getEnterDate() %> ~ now</div>
							<%}
							if(s.getMajor() != null){%>
								<div id="explain" class="explain"><%=s.getMajor() %></div>
							<%} else{%>
								<div id="explain" class="explain">-</div>
							<%} %>
						</div>
					<%}} %>
				</div>
				<div class="leftDetail">
						<h4>교육</h4>
					<%if(elist != null){
						for(Education e : elist){%>
						<div>
							<input type="hidden" name="education" value="<%=e.getEid()%>" />
							<%if(e.getEuFinishDate() != null){ %>
								<div id="date" class="date"><%=e.getEuStartDate() %> ~ <%=e.getEuFinishDate() %></div>
							<%} else{%>
								<div id="date" class="date"><%=e.getEuStartDate() %> ~ now</div>
							<%} %>
							<div id="explain" class="explain"><%=e.getEuContent() %></div>
						</div>
					<%}} %>
				</div>
				<div class="leftDetail">
					<h4>자격증</h4>
					<%if(celist != null){
					for(Certification ce : celist){ %>
					<div>
						<input type="hidden" name="certification" value="<%=ce.getcerId()%>"/>
						<div class="title ctitle"><%=ce.getCerName() %></div>
						<div id="date" class="date"><%=ce.getCertifyDate() %></div>
					</div>
					<%}} %>
				</div>
			</div>
			
			<div class="projectInfo">
				<h4>프로젝트 경력기술서</h4>
				<%if(plist != null){ 
					for(ProjectCareer pc : plist){%>
						<div class="pjtDetail" id="project1">
						<input type="hidden" name="project" value="<%=pc.getPjtId()%>">
						<%if(pc.getAtlist().get(0).getChangeName() != null){ %>
						<div class="pimg"><img src="<%=request.getContextPath()%>/attachment_uploadFiles/<%=(pc.getAtlist()).get(0).getChangeName()%>" width="88px" height="88px"></div>
						<%} else{ %>
						<div class="pimg"></div>
						<%} %>
						<div class="pintro">
							<div class="ptitle"><%=pc.getPjtName() %></div>
							<div class="pdate"><%=pc.getPjtStart() %> ~ <%=pc.getPjtFinish()%> </div>
							<div class="pcontent"><%=pc.getMyRole() %></div>
						</div>
					</div>
				<%}} %>
			</div>
			
			<div class="keywordInfo">
				<h4 style="margin:0px;">연관키워드</h4>
				<%if(khlist != null){
					int cnt = 10;
					if(cnt > khlist.size()){
						cnt = khlist.size();
					}
					
					for(int i=0; i < cnt; i++){%>
					<div class="keywordArea">
						<div class="keywordCnt"><%=khlist.get(i).get("count") %></div>
						<div class="keyword"><%=khlist.get(i).get("kname")%></div>
					</div>
				<%}} %>
				
			</div> <!-- 키워드 영역 끝! -->
		</div>
	</div> <!-- first Area End -->
	<script>
		$(function(){
			$(".pjtDetail").click(function(){
				var num = $(this).find("input[type=hidden]").val();
				location.href="<%=request.getContextPath()%>/selectOne.pjt?type=3&num=" + num;
			});
		});
	</script>
	
</body>
</html>