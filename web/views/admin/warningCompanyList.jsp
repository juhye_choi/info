<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 페이지</title>
<%@ include file="/views/common/import.html"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
.outline {
	margin-right: auto;
	margin-left: auto;
	width: 80%;
	height: 800px;
	background: white;
	padding: 50px;
}

.searchSection {
	padding-top: 20px;
	padding-right: 20%;
	padding-left: 20%;
	padding-bottom: 20px;
	border: 1px solid lightgray;
	border-top: 2px solid purple;
	text-align: center;
}

.mainBtn {
	width: 30%;
	height: 50px;
	margin: -2px;
	padding-top: 12px;
	background: #e7e6e1;
	border: 1px solid black;
	display: inline-block;
	text-align: center;
	vertical-align: middle;
	font-size: 1.2em;
}

.mainBtn:hover {
	cursor: pointer;
}

.title {
	margin: 30px;
	margin-left: 20px;
	font-size: 1.8em;
}

.tableSection {
	padding-top: 20px;
}

.radioTable td {
	padding: 10px;
	font-size: 1.2em;
}

.buttonStyle {
	margin-right: 5px !important;
	margin-left: 5px !important;
}

#logTable td:last-of-type {
	width: 180px;
}

h1 {
	margin-left: 11%;
}

.fields {
	padding-left: 20%;
}
</style>
</head>
<body style="background: lightgray;">
	<%@ include file="../common/adminMenu.jsp"%>
	<h1>경고 리스트</h1>

	<div class="outline">
		<div class="btnArea">
			<div class="mainBtn" id="person">개인</div>
			<div class="mainBtn" id="company">기업</div>
		</div>
		<div class="searchSection">
			<form class="ui form" action="" method="get">
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown">
							<option>ID</option>
							<option>이름</option>
							<option>사업자 등록번호</option>
							<option>날짜</option>
						</select>
					</div>
					<div class="ui icon input">
						<input type="text" placeholder="Search..."> <i
							class="circular search link icon"></i>
					</div>
				</div>
				<table align="center" class="radioTable">

					<tr>
						<td colspan="5"><input type="submit" value="검색"
							class="ui purple button" style="width: 150px;"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead align="center">
					<tr>
						<th>번호</th>
						<th>일자</th>
						<th>ID</th>
						<th>회사명</th>
						<th>사업자 번호</th>
						<th>신고 내용</th>
					</tr>
				</thead>
				<tbody align="center">
					<tr>
						<td>1</td>
						<td>2018.09.01</td>
						<td>hwan123</td>
						<td>(주)뀨화니</td>
						<td>506-20-31524</td>
						<td></td>
					</tr>
					<tr>
						<td>2</td>
						<td>2018.09.01</td>
						<td>yg123</td>
						<td>(주)연두</td>
						<td>501-15-32452</td>
						<td></td>
					</tr>
					<tr>
						<td>1</td>
						<td>2017.04.30</td>
						<td>dohuhung1</td>
						<td>(주)도후훙</td>
						<td>207-10-57852</td>
						<td></td>
					</tr>
					<tr>
						<td>3</td>
						<td>2019.11.03</td>
						<td>jjuhey</td>
						<td>(주)쭈헤이</td>
						<td>157-48-25846</td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<script>
		$(function(){
			$("#logTable").tablesort();
			
			$("#company").css({"border-bottom":"white", "background":"white"});
			$("#person").css({"color":"gray", "border":"1px solid gray"});
			
			$("#person").click(function(){
				location.href="warningPersonList.jsp";
			})
		});
	</script>
</body>
</html>