<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*, company.model.vo.*"%>
    
    <%
    
    ArrayList<HashMap<String, Object>> list = ( ArrayList<HashMap<String, Object>>)request.getAttribute("list");
    
    for(int i = 0; i < list.size(); i++) { 
  		HashMap<String, Object> hmap = list.get(i);
    }
    
    CPageInfo pi = (CPageInfo) request.getAttribute("pi");
    int ClistCount = pi.getClistCount();
   	int currentPage = pi.getCurrentPage();
   	int maxPage = pi.getMaxPage();
   	int startPage = pi.getStartPage();
   	int endPage = pi.getEndPage();
    
    
    
    
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 페이지</title>
<%@ include file="/views/common/import.html" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tablesort.js"></script>
<style>
	.outline{
		margin-right:auto;
		margin-left:auto;
		width:80%;
		height:800px;
		background:white;
		padding:50px;
	}
	.searchSection{
		padding-top:20px;
		padding-right:20%;
		padding-left:20%;
		padding-bottom:20px;
		border:1px solid lightgray;
		border-top:2px solid purple;
		text-align:center;
	}
	.tableSection{
		padding-top:20px;
	}
	
	.radioTable td{
		padding:10px;
		font-size:1.2em;
	}
	
	.buttonStyle {
		margin-right:5px !important;
		margin-left:5px !important;
	}
	
	#logTable td:last-of-type {
		width:180px;
	}
	
	h1{
		margin-left:11%;
	}
	
	.fields {
	
	padding-left:20%;
	
	
	}
	.pagingArea{
		text-align:center;
		padding:20px;
		margin-bottom:50px;
		margin-top:30px;
	}
	.pagingbtn{
		height:30px;
		width:30px;
		display:inline-block;
		vertical-align:top;
		overflow:hidden;
		background:white;
		border:1px solid lightgray;
		border-radius:5px;
	}
	.pagingbtn:hover{
		cursor:pointer;
	}
	.pagingbtn:disabled{
		color:gray;
	}
	
</style>
</head>
<body style="background:lightgray;">
	<%@ include file="../common/adminMenu.jsp" %>
	<h1>기업회원 정보조회</h1>
	
	<div class="outline">
		<div class="searchSection">
			<form class="ui form" action="" method="get">
				<div class="fields">
					<div class="six wide field">
						<select class="ui fluid dropdown">
							<option>회사명</option>
							<option>회원ID</option>
							<option>가입날짜</option>
							<option>담당자이메일</option>
						</select>
					</div>
						<div class="ui icon input">
  						<input type="text" placeholder="Search..." disabled>
  						<i class="circular search link icon"></i>
					</div>
				</div>
				<table align="center" class="radioTable">
				
					<tr>
						<td colspan="5"><input type="submit" value="검색" class="ui purple button" style="width:150px;"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="tableSection">
			<table class="ui sortable celled table" id="logTable">
				<thead align="center">
					<tr>
					<th>회원번호</th>
						<th>회사명</th>
						<th>회원ID</th>
						
						<th>담당자 이메일</th>
						<th>남은 인재열람건수</th>
						<th>조회/삭제처리</th>
					</tr>
				</thead>
				<tbody align="center">
					<%for(int j = 0; j < list.size(); j++){ %>
					<tr>
					<td><%= list.get(j).get("uno") %></td>
					<td><%= list.get(j).get("name") %></td>
					<td><%=list.get(j).get("userId") %></td>
					<td><%= list.get(j).get("email") %></td>
					
					<td><%= list.get(j).get("viewCount") %></td>
					<td><input type="button" class="ui button mini buttonStyle" id="personSelect" value="회원조회"><input type="button" class="ui red button mini buttonStyle" id="personDelete" value="탈퇴"></td>
					</tr>
					
					<%} %>
				</tbody>
			</table>
		</div>
		<div class="pagingArea">
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyListPage.ad?currentPage=1'"><<</button>
			<%if(currentPage <=1) {%>
				<button class="pagingbtn" disabled></button>
			<%} else{ %>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyListPage.ad?currentPage=<%=currentPage-1%>'"><</button>
			<%} %>
			
			<%for(int p=startPage; p<=endPage; p++){
				if(p == currentPage){ %>
					<button class="pagingbtn" disabled><%=p %></button>
				<%} else{ %>
					<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyListPage.ad?currentPage=<%=p%>'"><%=p %></button>
				<%}
			  } %>
			
			<%if(currentPage >= maxPage){ %>
				<button class="pagingbtn" disabled></button>
			<%} else {%>
				<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyListPage.ad?currentPage=<%=currentPage+1%>'">></button>
			<%} %>
			<button class="pagingbtn" onclick="location.href='<%=request.getContextPath()%>/SelectCompanyListPage.ad?currentPage=<%=maxPage%>'">>></button>
		</div>
	</div>
	
	<script>
		$(function(){
			$("#logTable").tablesort();
			
			
$("#logTable input[type=button]").mouseenter(function(){
				
				$(this).parent().parent().css({"background":"skyblue","cursor":"pointer"})
				
			}).mouseout(function(){
				
				$(this).parent().parent().css({"background":"white"});
				
			}).click(function(){
				
				
					
var num = $(this).parent().parent().children().eq(0).text();
					
				if($(this).attr('value') == "회원조회") {
					
					
					
					location.href = "<%=request.getContextPath()%>/CompanyInfo2.me?num=" + num;	
					
				}else if($(this).attr('value') == "탈퇴") {
					
					var check =	confirm("정말 강제로 탈퇴 시킵니까?");
	            	if(check == true) {
	            		location.href = "<%=request.getContextPath()%>/deleteCompany.ad?num=" + num + "&type=2";	
	            		
	            	}
					
					
				
					
				}
					
				
				
				
					
				
				
				
				
				
				
			})
			
			
			
			
			
			
			
			
			
			
			
			
			
		});
	</script>
</body>
</html>





