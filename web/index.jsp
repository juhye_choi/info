<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>IT Duck</title>
<%-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/semantic/semantic.min.css">
<script src="<%= request.getContextPath() %>/semantic/semantic.min.js"></script> --%>
<%@ include file="/views/common/import.html" %>

<style>
	img {
		display:block;
		margin-left:auto;
		margin-right:auto;
		width:100%;
	}
	.bridge{
		background-image: url("<%=request.getContextPath()%>/images/bridge.jpg");
		background-position: center;
		min-width:100%;
		background-size:cover;
		height:600px;
		padding-top:100px;
	}
	h1 {
		text-align:center;
		font-size:100px;
	}
	h3{
		text-align:center;
	}
	
	.contents{
		width:100%;
		height:350px;
		margin-right:auto;
		margin-left:auto;
		text-align:center;
	}
	.banner {
		position: relative;
		width: 1700px;
		height: 350px;
		margin:0 auto; 
		padding:0; 
		overflow: hidden;
	}
	.banner ul {
		position: absolute; 
		margin: 0px; 
		padding:0; 
		list-style: none; 
	}
	.nextBtn {
		position: absolute;
		z-index:50;
		right:100px;
		bottom:30px;
		opacity:0.4;
	}
	.banner ul li {
		float: left; 
		width: 1700px; 
		height: 350x; 
		margin:0; 
		padding:0;
	}
	.banner img{
		width:inherit;
		max-width:100%;
		height:auto;
	}
	
	.middleArea{
		width:800px;
		height:50px;
		margin:10px;
		margin-top:20px;
		margin-bottom:40px;
		margin-right:auto;
		margin-left:auto;
		border:1px solid lightgray;
		border-radius:10px;
		padding-left:50px;
		padding-right:50px;
	}
</style>
</head>
<body>
	<%@ include file="views/common/mainMenu.jsp" %>
	<% if(loginUser == null || !loginUser.getCategory().equals("P")){ %>
		<div class="bridge">
		
			<h3><span style="color:orangered;">개인</span>과 기업을 잇다.</h3>
			<h1>IT DUCK</h1>
			<h3 style="color:orangered; visibility:hidden;">마스터파일 고치시면 혼날각오하세요.</h3>
			<h1 style="visibility:hidden;">😡</h1>
			<h3 style="visibility:hidden;">(개인회원 메인페이지)</h3>
			<button class="ui yellow button huge" id="startNow" 
				style="float:right; margin-top:-50px; margin-right:100px; border-radius:50px; width:200px; height:80px;">START NOW</button>
		</div>
	<%} else{ %>
	<div class="contents">
		<div class="banner">
			<ul>
				<li><img src="images/mainImg5.png"></li>
				<li><img src="images/mainImg1.png"></li>
				<li><img src="images/mainImg2.png"></li>
				<li><img src="images/mainImg3.png"></li>
			</ul>
		<button class="circular ui icon button nextBtn" id="nextBtn"><i class="icon chevron right"></i></button>
		</div>
	<%} %>
	</div>
	<%if(loginUser != null && loginUser.getCategory().equals("P")){ %>
		<div class="middleArea">
			<div class="ui third header" style="height:40px; margin-top:11px;">
				<i class="pen nib icon" style="display:inline-block; overflow:hidden;"></i>
				<div class="content" style="display:inline-block; overflow:hidden;">
					<span style="color:purple"><%=loginUser.getUserName() %>님!</span> 자주 방문하셔서, 이력을 관리해보세요!</div>
				<button class="ui purple button mini" style="float:right; display:inline-block; overflow:hidden;">이력서관리</button>
			</div>
		</div>
	<%} else{ %>
		<div class="middleArea" style="visibility:hidden"></div>
	<%} %>

		<%@ include file="views/common/siteInformation.jsp" %>
	<%@ include file="views/common/ourfooter.jsp" %>	
	
	<script>
	var width = "";
	var height = "";
	var $banner = "";
	var $banner = $(".banner").find("ul");
	
	$(document).ready(function() {
		
		$("#site1").click(function(){
			location.href='<%=request.getContextPath()%>/selectOne.po';
		});
		$("#site2").click(function(){
			location.href='<%=request.getContextPath()%>/index.jsp';
		});
		$("#site3").click(function(){
			location.href='<%=request.getContextPath()%>/selectOne.re';
		});
		
		width = window.innerWidth;
		height = width*350/1700;

		$banner.children().css("width", width);
		$banner.children().css("height", height);
		$(".banner").css("width", width);
		$(".banner").css("height", height);
		
		
		var $bannerWidth = $banner.children().outerWidth();//이미지의 폭
		var $bannerHeight = $banner.children().outerHeight(); // 높이
		var $length = $banner.children().length;//이미지의 갯수
		var rollingId;
		
		//console.log(window.innerWidth);
		
		
		
		//정해진 초마다 함수 실행
		rollingId = setInterval(function() { 
			rollingStart(); 
			}, 5000);//다음 이미지로 롤링 애니메이션 할 시간차
    		
		function rollingStart() {
			$banner.children().css("width", width);
			$banner.children().css("height", height);
			$(".banner").css("width", width);
			$(".banner").css("height", height);
			
			$banner.css("width", $bannerWidth * $length + "px");
			$banner.css("height", $bannerHeight + "px");
			//alert(bannerHeight);
			//배너의 좌측 위치를 옮겨 준다.
			$banner.animate({left: - $bannerWidth + "px"}, 1500, function() { //숫자는 롤링 진행되는 시간이다.
				//첫번째 이미지를 마지막 끝에 복사(이동이 아니라 복사)해서 추가한다.
				$(this).append("<li>" + $(this).find("li:first").html() + "</li>");
				//뒤로 복사된 첫번재 이미지는 필요 없으니 삭제한다.
				$(this).find("li:first").remove();
				//다음 움직임을 위해서 배너 좌측의 위치값을 초기화 한다.
				$(this).css("left", 0);
				//이 과정을 반복하면서 계속 롤링하는 배너를 만들 수 있다.
			});
		}
		$("#nextBtn").click(function(){
			rollingStart();
		});
		
		$("#startNow").click(function(){
			location.href = "<%=request.getContextPath()%>/views/person/personjoinForm.jsp";
		});
		
	});
	
	$(window).resize(function(){

		width = window.innerWidth;
		height = width*350/1700;

		$banner.children().css("width", width);
		$banner.children().css("height", height);
		$(".banner").css("width", width);
		$(".banner").css("height", height);
	});
</script>
</body>
</html>











